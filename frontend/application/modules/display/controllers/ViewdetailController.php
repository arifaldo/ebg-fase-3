<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';

class display_ViewdetailController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	protected $_bankName;

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
		$this->_transferStatus 	= $conf["transfer"]["status"];
	}
	
	function string_between_two_string($str, $starting_word, $ending_word)
	{
		$subtring_start = strpos($str, $starting_word);
		//Adding the strating index of the strating word to 
		//its length would give its ending index
		$subtring_start += strlen($starting_word);  
		//Length of our d sub string
		$size = strpos($str, $ending_word, $subtring_start) - $subtring_start;  
		// Return the substring from the index substring_start of length size 
		return substr($str, $subtring_start, $size);  
	}

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$filter 			= new Application_Filtering();

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;


		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($filter->filter($this->_getParam('payReff'), "PS_NUMBER"));
		$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);
		//var_dump($PS_NUMBER);die;

		$pdf 				= $filter->filter($this->_getParam('pdf'), "BUTTON");
		$cancelfuturedatebtn = $filter->filter($this->_getParam('cancelfuturedate'), "BUTTON");
		$backBtn			= $filter->filter($this->_getParam('back'), "BUTTON");
		$this->_paymentRef 	= $PS_NUMBER;

		$select = $this->_db->select()
			->from('T_PSLIP', array('*'))
			->where("PS_NUMBER = '" . $PS_NUMBER . "' AND CUST_ID = '" . $this->_custIdLogin . "'");

		// echo $select->__toString();
		// die;


		//echo $select;
		$cekPsNumber = $this->_db->fetchRow($select);

		$pslip = $cekPsNumber;
		
		$psnumber = $PS_NUMBER;
		$getPaymentDetail 	= new display_Model_Paymentreport();
		$history = $getPaymentDetail->getHistory($psnumber);
		$historyaprove = $getPaymentDetail->getHistoryAprove($psnumber);
//var_dump($history);die;
		$detail = $getPaymentDetail->getPaymentDetailReport($psnumber);
		//var_dump($detail);die;
		if(empty($detail[0]['SOURCE_ACCT_BANK_CODE'])){
			$this->view->masterBankName = $this->_bankName;
		}
		$this->view->detail = $detail[0];
		// echo '<pre>';
		// var_dump($pslip);die;
		//cari policy
		$policyBoundary = $this->findPolicyBoundary($pslip['PS_TYPE'], $pslip['PS_TOTAL_AMOUNT']);

		$checkBoundary = $this->validatebtn($pslip['PS_TYPE'], $pslip['PS_TOTAL_AMOUNT'], $pslip['PS_CCY'], $pslip['PS_NUMBER']);

		//cek privilage
		$custidlike = '%' . $this->_custIdLogin . '%';

		//select user with reviewer privilage
		$selectReviewer = $this->_db->select()
			->from(array('P' => 'M_FPRIVI_USER'))
			->from(array('U' => 'M_USER'))
			->where("P.FPRIVI_ID = 'RVPV' AND P.FUSER_ID LIKE ?", (string) $custidlike)
			->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
			->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

		$userReviewer = $this->_db->fetchAll($selectReviewer);

		$reviewerList = array();
		foreach ($userReviewer as $row) {
			$userIdReviewer = explode($this->_custIdLogin, $row['FUSER_ID']);

			//get user name
			$selectReviewerName = $this->_db->select()
				->from('M_USER')
				->where("USER_ID = ?", (string) $userIdReviewer[1]);

			$userReviewerName = $this->_db->fetchAll($selectReviewerName);

			array_push($reviewerList, $userReviewerName[0]['USER_FULLNAME']);
		}

		//function utk munculin button dengan policy grup jika belum ada yg approve
		if ($pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '5' || $pslip['PS_TYPE'] == '11') {
			$PS_TYPE = '18';
		} else {
			$PS_TYPE = $pslip['PS_TYPE'];
		}

		$approverUserList = $this->findUserBoundary($PS_TYPE, $pslip['PS_TOTAL_AMOUNT']);


		//select user with releaser privilage
		$selectReleaser = $this->_db->select()
			->from(array('P' => 'M_FPRIVI_USER'))
			->from(array('U' => 'M_USER'))
			->where("P.FPRIVI_ID = 'PRLP' AND P.FUSER_ID LIKE ?", (string) $custidlike)
			->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
			->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

		$userReleaser = $this->_db->fetchAll($selectReleaser);

		$releaserList = array();
		foreach ($userReleaser as $row) {
			$userIdReleaser = explode($this->_custIdLogin, $row['FUSER_ID']);

			//get user name
			$selectReleaserName = $this->_db->select()
				->from('M_USER')
				->where("USER_ID = ?", (string) $userIdReleaser[1]);
			//echo $selectReleaserName;
			$userReleaserName = $this->_db->fetchAll($selectReleaserName);

			array_push($releaserList, $userReleaserName[0]['USER_FULLNAME']);
		}

		$selectdatachange = $this->_db->select()
			  ->from('T_GLOBAL_CHANGES');
		$selectdatachange -> where("CUST_ID = ".$this->_db->quote($this->_custIdLogin)." OR CUST_ID ='BANK'");
		// $selectdatachange -> where("CHANGES_STATUS = ? ",'WA');
		$selectdatachange -> where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS ='RR'");
		// $selectdatachange -> where("CHANGES_STATUS = ? ",'RR');
		$selectdatachange -> where("COMPANY_CODE = ? ",$this->_custIdLogin);
		$selectdatachange -> where("DISPLAY_TABLENAME = ? ",'Approver Matrix');

		// echo $selectdatachange;die();
		 $selectdatachange1 = $selectdatachange->query()->FetchAll();

		 // echo "<pre>";
		 // var_dump($selectdatachange1);
		 // die();
		
		$error_msg2 = false;
		if(!empty($selectdatachange1)){
		  $this->view->validbtn = false;
		  $error_msg2 = true;
		  $this->view->error_msg2  = 'Cannot authorize, waiting for approval matrix changes.';
		}

		//cek ada privilage reviewer atau approver
		$selectpriv = $this->_db->select()
			->from(array('M_CUSTOMER'))
			->where("CUST_ID = ?", $this->_custIdLogin);

		$userpriv = $this->_db->fetchAll($selectpriv);

		if ($userpriv[0]['CUST_REVIEW'] != 1) {
			$cust_reviewer = 0;
		} else {
			$cust_reviewer = 1;
		}

		if ($userpriv[0]['CUST_APPROVER'] != 1) {
			$cust_approver = 0;
		} else {
			$cust_approver = 1;
		}

		if(!empty($pslip['PS_PERIODIC'])){
		$liststats = array(1,15,5,2,21,22);
		$selectlistPS = $this->_db->select()->from(array('P' => 'T_PSLIP'), array('A.*'))
				->joinLeft(array('A' => 'T_PSLIP_HISTORY'), 'P.PS_NUMBER = A.PS_NUMBER', array())
			->where("P.PS_PERIODIC = ?", $pslip['PS_PERIODIC'])
			->where("A.HISTORY_STATUS IN (?)", $liststats);
			//echo $selectlistPS;
		$listPeriodic = $this->_db->fetchAll($selectlistPS);
		//echo '<pre>';
		//var_dump($listPeriodic);
		}else{
			$listPeriodic = array();
		}
		
		if($pslip['PS_TYPE'] != '38'){
		$selectHistory	= $this->_db->select()
			->from('T_PSLIP_HISTORY')
			->where("PS_NUMBER = ?", $PS_NUMBER);

		$history = $this->_db->fetchAll($selectHistory);
		}else{
			$selecttransaction = $this->_db->select()
				->from(array('A' => 'T_TRANSACTION'), array('A.TRA_MESSAGE'))
				->where("A.PS_NUMBER = ?", $PS_NUMBER);
			$bgnumb = $this->_db->fetchOne($selecttransaction);
			//var_dump($bgnumb);die;
			
			$selectHistory	= $this->_db->select()
			->from('T_BANK_GUARANTEE_HISTORY')
			->where("BG_REG_NUMBER = ?", $bgnumb);

		$history = $this->_db->fetchAll($selectHistory);
			
		}
		//echo $selectHistory;
		$history = array_merge($listPeriodic,$history); 
		
		//echo '<pre>';
		//var_dump($history);
		//die;
		
		foreach ($history as $row) {
			//if maker done
			if ($row['HISTORY_STATUS'] == 1) {
				$currentStatus = 1;
				$makerStatus = 'active';
				$makerIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
					$reviewerOngoing = '';
					$approverOngoing = '';
					$releaserOngoing = 'ongoing';
				} else {
					$reviewerOngoing = 'ongoing';
					$approverOngoing = '';
					$releaserOngoing = '';
				}

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				$custEmail 	  = $customer[0]['USER_EMAIL'];
				$custPhone	  = $customer[0]['USER_PHONE'];

				$makerApprovedBy = $custFullname;

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

				$align = 'align="center"';
				$marginRight = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginRight = 'style="margin-right: 15px;"';
				}

				$this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
			}
			//if reviewer done
			if ($row['HISTORY_STATUS'] == 15) {
				$currentStatus = 15;
				$makerStatus = 'active';
				$reviewStatus = 'active';
				$reviewIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'ongoing';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$reviewerApprovedBy = $custFullname;

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->reviewerApprovedBy = '<div align="center" class="textTheme">' . $efdate . '<br>' . $custFullname . '</div>';
			}
			//if approver done
			if ($row['HISTORY_STATUS'] == 2 ) {
				$currentStatus = 2;
				$makerStatus = 'active';
				$approveStatus = '';
				$reviewStatus = 'active';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'active';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				//tampung data user yang sudah approve
				$userid[] = $custlogin;

				$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
			}
			
			
			/*
			if ($row['HISTORY_STATUS'] == 22 && $PS_NUMBER == $row['PS_NUMBER']) {
				//die;
				$currentStatus = 5;
				$makerStatus = 'active';
				$approveStatus = 'active';
				$reviewStatus = 'active';
				$releaseStatus = 'active';
				$releaseIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = '';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$releaserApprovedBy = $custFullname;

				$align = 'align="center"';
				$marginLeft = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginLeft = 'style="margin-left: 15px;"';
				}
				
				if($custFullname == ''){
					$custFullname = 'System';
				}

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
				//var_dump($this->view->releaserApprovedBy);die;
				//break;
			}*/
			
			
			//if releaser done
			if ($row['HISTORY_STATUS'] == 5 ) {
				$currentStatus = 5;
				$makerStatus = 'active';
				$approveStatus = 'active';
				$reviewStatus = 'active';
				$releaseStatus = 'active';
				$releaseIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = '';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];
				if($custlogin != null){
				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];
				// $custEmail 	  = $customer[0]['USER_EMAIL'];
				// $custPhone	  = $customer[0]['USER_PHONE'];

				$releaserApprovedBy = $custFullname;
				}
				$align = 'align="center"';
				$marginLeft = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginLeft = 'style="margin-left: 15px;"';
				}
				
				if($custFullname == ''){
					$custFullname = 'System';
				}

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';

				//break;
			}
			
			

			//if rejected
			if($row['HISTORY_STATUS'] == 4){

				$custlogin = $row['USER_LOGIN'];

				$selectCust	= $this->_db->select()
					->from('M_USER')
					->where("USER_ID = ?", $custlogin);

				$customer = $this->_db->fetchAll($selectCust);

				$custFullname = $customer[0]['USER_FULLNAME'];

				if ($custlogin == 'System') {
					$custFullname = 'System';
				}

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

				//if current status 1 = maker, then 4 = rejected, show rejected icon on review icon
				if ($currentStatus == 1) {
					$reviewStatus = 'rejected';
					$reviewIcon = '<i class="fas fa-check"></i>';

					$makerOngoing = '';
					$reviewerOngoing = 'ongoing';
					$approverOngoing = '';
					$releaserOngoing = '';

					$this->view->reviewerApprovedBy = '<div align="center" class="textTheme">' . $efdate . '<br>' . $custFullname . '</div>';
				}
				//if current status 15 = review, then 4 = rejected, show rejected icon on approver icon
				else if($currentStatus == 15){
					$approveStatus = 'rejected';
					$approveIcon = '<i class="fa fa-times"></i>';

					$makerOngoing = '';
					$reviewerOngoing = '';
					$approverOngoing = 'ongoing';
					$releaserOngoing = '';
 
					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $custFullname . '</div>';
				}

				else if($currentStatus == 2){
					$rejected = true;
					$rejectedDate = $efdate;
					$rejectedBy = $custFullname;
				}

				//if currentstatus = releaser then next ada rejected, maka itu rejected by system, gausah ditampilkan
			}
			//var_dump($row['HISTORY_STATUS']);
			//var_dump($PS_NUMBER); 
			//var_dump($row['PS_NUMBER']);
			//echo '--- end  ---';
			if ($row['HISTORY_STATUS'] == 22 && $PS_NUMBER == $row['PS_NUMBER']) {
				$expstr = explode('Recreate from Trx ID: ',$row['PS_REASON']);
				//var_dump($PS_NUMBER.'01');
				//var_dump($expstr['1']);die;
				//if($expstr['1']==$PS_NUMBER.'01'){ 
				//die;
				$currentStatus = 22;
				$makerStatus = 'active';
				$approveStatus = 'active';
				$reviewStatus = 'active';
				$releaseStatus = 'active';
				$recurrentStatus = 'active';
				$recurrentIcon = '<i class="fas fa-check"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = '';
				$releaserOngoing = '';

				$custlogin = $row['USER_LOGIN'];


				$align = 'align="center"';
				$marginLeft = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginLeft = 'style="margin-left: 15px;"';
				}
				
				//if($custFullname == ''){
					$custFullname = 'System';
				//}

				$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
				$this->view->recurenctBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';

				//break;
				//}
			}
		}

		//approvernamecircle jika sudah ada yang approve
		if (!empty($userid)) {

			$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

			$flipAlphabet = array_flip($alphabet);

			$approvedNameList = array();
			$i = 0;
			
			foreach ($userid as $key => $value) {

				//select utk nama dan email
				$selectusername = $this->_db->select()
					->from(array('M_USER'), array(
						'*'
					))
					->where("USER_ID = ?", (string) $value);

				$username = $this->_db->fetchAll($selectusername);

				//select utk cek user berada di grup apa
				$selectusergroup	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array(
						'*'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.USER_ID 	= ?", (string) $value);

				$usergroup = $this->_db->fetchAll($selectusergroup);

				$groupuserid = $usergroup[0]['GROUP_USER_ID'];
				$groupusername = $usergroup[0]['USER_ID'];
				$groupuseridexplode = explode("_", $groupuserid);

				if ($groupuseridexplode[0] == "S") {
					$usergroupid = "SG";
					$approveIcon = '<i class="fas fa-check"></i>';
				} else {
					$usergroupid = $alphabet[$groupuseridexplode[2]];
				}

				// $tempuserid = "";
				// foreach ($approverNameCircle as $row => $data) {
				// 	foreach ($data as $keys => $val) {
				// 		if ($keys == $usergroupid) {
				// 			if (preg_match("/active/", $val)) {
				// 				continue;
				// 			}else{
				// 				if ($groupuserid == $tempuserid) {
				// 					continue;
				// 				}else{
				// 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
				// 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
				// 				}
				// 				$tempuserid = $groupuserid;
				// 			}
				// 		}
				// 	}
				// }

				array_push($approvedNameList, $username[0]['USER_FULLNAME']);

				$efdate = $approveEfDate[$i];

				$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

				$i++;
			}
			//var_dump($approverApprovedBy);
			//kalau sudah approve semua
			if (!$checkBoundary) {
				$approveStatus = 'active';
				$approverOngoing = '';
				$approveIcon = '<i class="fas fa-check"></i>';
				$releaserOngoing = 'ongoing';
			}
			
			//if udah approve semua and next = 4(rejected), maka rejected pd tahap releaser
			if (!$checkBoundary && $rejected) {
				$releaseStatus = 'rejected';
				$releaseIcon = '<i class="fa fa-times"></i>';

				$align = 'align="center"';
				$marginLeft = '';
				if ($cust_reviewer == 0 && $cust_approver == 0) {
					$align = '';
					$marginLeft = 'style="margin-left: 15px;"';
				}
				$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $rejectedDate . '<br><span ' . $marginLeft . '>' . $rejectedBy . '</span></div>';
			}
			// if current status 2 = approver and next = 4 (rejected), then ada yg reject pd tahapn approver
			else if($rejected){
				$approveStatus = 'rejected';
				$approveIcon = '<i class="fa fa-times"></i>';

				$makerOngoing = '';
				$reviewerOngoing = '';
				$approverOngoing = 'ongoing';
				$releaserOngoing = '';

				$approverApprovedBy[] = '<div align="center" class="textDanger">' . $efdate . '<br>' . $rejectedBy . '</div>';
			}
		}

		$this->view->approverApprovedBy = $approverApprovedBy;

		$selectsuperuser = $this->_db->select()
			->from(array('C' => 'T_APPROVAL'))
			->where("C.PS_NUMBER 	= ?", $PS_NUMBER)
			->where("C.GROUP 	= 'SG'");

		$superuser = $this->_db->fetchAll($selectsuperuser);
	//var_dump($approverApprovedBy);
		if (!empty($superuser)) {
			$userid = $superuser[0]['USER_ID'];

			//select utk nama dan email
			$selectusername = $this->_db->select()
				->from(array('M_USER'), array(
					'*'
				))
				->where("USER_ID = ?", (string) $userid);

			$username = $this->_db->fetchAll($selectusername);


			$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

			$approveStatus = 'active';
			$approverOngoing = '';
			$approveIcon = '<i class="fas fa-check"></i>';
			$releaserOngoing = 'ongoing';

			//if udah superuser approve and next = 4(rejected), maka rejected pd tahap releaser
			if ($rejected) {
				$releaseStatus = 'rejected';
				$releaseIcon = '<i class="fa fa-times"></i>';
			}
		}

		$approverApprovedBy = array_unique($approverApprovedBy);
		$this->view->approverApprovedBy = $approverApprovedBy;
		// echo "<pre>";
		// var_dump($pslip);die;

		//if active no hovertext
		if ($makerStatus == 'active') {
			//define circle
			$makerNameCircle = '<img src="/assets/themes/assets/newlayout/img/maker.PNG"> <br/><button id="makerCircle" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . '" style="cursor: auto;" disabled>' . $makerIcon . '</button>';
		}
		else{
			//define circle
			$makerNameCircle = '<img src="/assets/themes/assets/newlayout/img/maker.PNG"> <br/><button id="makerCircle" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '
					<span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">' . $makerApprovedBy . '</p></span>
				</button>';
		}

		
		// echo $makerNameCircle;die;
		// var_dump($makerNameCircle);die;

		foreach ($reviewerList as $key => $value) {

			$textColor = '';
			if ($value == $reviewerApprovedBy) {
				$textColor = 'text-white-50';
			}

			$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
		}

		//if active / rejected no hovertext
		if ($reviewStatus == 'active' || $reviewStatus == 'rejected') {
			$reviewerNameCircle = '<img src="/assets/themes/assets/newlayout/img/reviewer.png"> <br/><button class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' " style="cursor: auto;" disabled>'.$reviewIcon.'</button>';
		}
		else{
			$reviewerNameCircle = '<img src="/assets/themes/assets/newlayout/img/reviewer.png"> <br/><button class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '
					<span class="hovertextcontent" style="text-align: center;">' . $reviewerListView . '</span></button>';
		}
		

		$groupNameList = $approverUserList['GROUP_NAME'];
		//echo '<pre>';
		//var_dump($approverUserList);die;
		unset($approverUserList['GROUP_NAME']);
		
		if ($approverUserList != '') {
			foreach ($approverUserList as $key => $value) {
				$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
				$i = 1;
				foreach ($value as $key2 => $value2) {

					$textColor = '';
					if (in_array($value2, $approvedNameList)) {
						$textColor = 'text-white-50';
					}

					if ($i == count($value)) {
						$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
					} else {
						$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
					}
					$i++;
				}
			}
		} else {
			$approverListdata = 'There is no Approver User';
		}

		$spandata = '';
		if (!empty($approverListdata) && !$error_msg2) {
			$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
		}
		//var_dump($pslip['PS_STATUS']);
		if($pslip['PS_STATUS']=='1'){
			$approveStatus = '';
			$reviewStatus = 'active';

			$approverOngoing = '';
		} else if ($pslip['PS_STATUS']=='2'){
			$approveStatus = 'active';
			$reviewStatus = 'active';

		}else{
			$approveStatus = '';
			$reviewStatus = '';

		}

		
		//if active / rejected no hovertext
		if ($approveStatus == 'active' || $approveStatus == 'rejected') {
			$spandata = '';
			$approverNameCircle = '<img src="/assets/themes/assets/newlayout/img/approver.png"> <br/><button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' " disabled style="cursor: auto;">' . $approveIcon . ' ' . $spandata  . '</button>';
		}
		else{
			$approverNameCircle = '<img src="/assets/themes/assets/newlayout/img/approver.png"> <br/><button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . ' ' . $spandata  . '</button>';
		}
		
		foreach ($releaserList as $key => $value) {

			$textColor = '';
			if ($value == $releaserApprovedBy) {
				$textColor = 'text-white-50';
			}

			$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
		}
		//var_dump($releaserListView);

		if ($recurrentStatus == 'active') {
			$releaseStatus = 'active';
			$releaseIcon = '<i class="fas fa-check"></i>';
			$recurrentNameCircle = '<img src="/assets/themes/assets/newlayout/img/recurrent.png" style="width:80px"> <br/><button id="recurrentCircle" class="btnCircleGroup  ' . $recurrentStatus . ' ' . $releaserOngoing . '" style="cursor: auto;" disabled>' . $recurrentIcon . '</button>';
		}
		else{
			$recurrentNameCircle = '<img src="/assets/themes/assets/newlayout/img/recurrent.png" style="width:80px"> <br/><button id="recurrentCircle" class="btnCircleGroup  ' . $recurrentStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $recurrentIcon;
			
		}
		
		//if active / rejected no hovertext
		if ($releaseStatus == 'active' || $releaseStatus == 'rejected') {

			$releaserNameCircle = '<img src="/assets/themes/assets/newlayout/img/releaser.png"> <br/><button id="releaserCircle" class="btnCircleGroup  ' . $releaseStatus . ' ' . $releaserOngoing . '" style="cursor: auto;" disabled>' . $releaseIcon . '</button>';
		}
		else{
			$releaserNameCircle = '<img src="/assets/themes/assets/newlayout/img/releaser.png"> <br/><button id="releaserCircle" class="btnCircleGroup  ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '
					<span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span></button>';
			
		}
		
		
		
		
		

		$this->view->cust_reviewer = $cust_reviewer;
		$this->view->cust_approver = $cust_approver;
		
		if($error_msg2  ){
			$policyBoundary = '';
		}
		
		
		$this->view->policyBoundary = $policyBoundary;
		$this->view->makerNameCircle = $makerNameCircle;
		$this->view->reviewerNameCircle = $reviewerNameCircle;
		$this->view->approverNameCircle = $approverNameCircle;
		$this->view->releaserNameCircle = $releaserNameCircle;
		$this->view->recurrentNameCircle = $recurrentNameCircle;

		$this->view->makerStatus = $makerStatus;
		$this->view->approveStatus = $approveStatus;
		$this->view->reviewStatus = $reviewStatus;
		$this->view->releaseStatus = $releaseStatus;
		$this->view->recurrentStatus = $recurrentStatus;

		if (!$cekPsNumber) {

			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Invalid Payment Ref#");
			$this->_redirect('/display/viewpayment/index');
		} else {
			// var_dump($cancelfuturedatebtn);
			if ($cancelfuturedatebtn) {
				$Payment = new Payment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
				$Payment->cancelFutureDate();

				$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/payReff/' . $this->_paymentRef);
				$this->_redirect('/notification/success');
			}

			$paramPayment = array(
				"WA" 				=> false,
				"ACCOUNT_LIST" 	=> $this->_accountList,
				"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
			);

			// get payment query
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$select   = $CustUser->getPaymentOutgoing($paramPayment);
			$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);
			//echo $select;die;
			$pslip = $this->_db->fetchRow($select);

			 //echo "<pre>";
			 //var_dump($pslip);
			 //die();

			$PSSTATUS  = $pslip["PS_STATUS"];
			$PSNUMBER  = $pslip["payReff"];
			$payStatus = $pslip["payStatus"];

			$this->view->AccArr = $CustUser->getAccounts();
			$this->view->AccArrDomestic = $CustUser->getAccounts(array("CCY_IN" => array("IDR")));

			// Set variables needed in view
			$settings = new Application_Settings();
			$ccyList  = $settings->setCurrencyRegistered();

			$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
			$lldRelationshipArr = $settings->getLLDDOMRelationship();
			$lldCategoryArr = $settings->getLLDDOMCategory();

			$model = new purchasing_Model_Purchasing();

			$purposeArr = $model->getTranspurpose();

			$purposeList = array('' => '-- Select Transaction Purpose --');
			foreach ($purposeArr as $key => $value) {
				$purposeList[$value['CODE']] = $value['DESCRIPTION'];
			}

			$this->view->TransPurposeArr = $purposeList;

			$selectuser = $this->_db->select()
				->from(array('A' => 'M_USER'));
			$selectuser->where("A.CUST_ID = " . $this->_db->quote($this->_custIdLogin));
			$this->view->dataact = $selectuser->query()->fetchAll();

			$selectccy = $this->_db->select()
				->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
			$this->view->dataccy = $selectccy->query()->fetchAll();

			$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
			$caseTransStatus = "(CASE A.TRA_STATUS ";
			foreach($transstatusarr as $key=>$val)
			{
				$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
			}
			$caseTransStatus .= " ELSE 'N/A' END)";

			$selecttransaction = $this->_db->select()
				->from(array('A' => 'T_TRANSACTION'), array('A.*','TRANSFER_STATUS' => $caseTransStatus))
				->where("A.PS_NUMBER = ?", $PSNUMBER);
			//echo $selecttransaction;die;
			$this->view->datatrx = $dataTranscation = $selecttransaction->query()->fetchAll();
			$transStatus = $dataTranscation[0]['TRANSFER_STATUS'];

			// print_r($this->view->datatrx);die();


			$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
			$ccyList  			= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'

			//for skn rtgs
			$lldTypeArr  		= $settings->getLLDDOMType();
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
			$lldRelationshipArr = $settings->getLLDDOMRelationship();
			$lldPurposeArr 		= $settings->getLLDDOMPurpose();
			$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
			$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
			$residentArr 		= array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
			$citizenshipArr		= array("W" => "WNI", "N" => "WNA");

			$this->view->residentArr = $residentArr;
			$this->view->citizenshipArr = $citizenshipArr;
			$this->view->lldCategoryArr = $lldCategoryArr;
			$this->view->lldBeneIdentifArr = $lldBeneIdentifArr;
			$this->view->TransactorArr = $lldRelationshipArr;
			$this->view->IdentyArr = $lldIdenticalArr;


			$anyValue = '-- ' . $this->language->_('Select City') . ' --';
			$select = $this->_db->select()
				->from(array('A' => 'M_CITY'), array('*'));
			$select->order('CITY_NAME ASC');
			$arr = $this->_db->fetchall($select);
			$cityCodeArr 			= array('' => $anyValue);
			$cityCodeArr 			+= Application_Helper_Array::listArray($arr, 'CITY_CODE', 'CITY_NAME');
			$this->view->cityCodeArr 	= $cityCodeArr;

			if ($this->view->hasPrivilege('MTSP')) {
				$usetemp = 1;
			} else {
				$usetemp = 0;
			}

			$this->view->usetemplate = $usetemp;

			if ($PSSTATUS == 5) {

				// COMPLETED WITH (_) TRANSACTION (S) FAILED
				// TRA_STATUS FAILED (4)

				$select = $this->_db->select()
					->from('T_TRANSACTION', array('countfailed' => 'count(TRANSACTION_ID)'))
					->where("TRA_STATUS = '4' AND PS_NUMBER = ?", $PSNUMBER);
				$countFailed = $this->_db->fetchOne($select);

				if ($countFailed == 0) 	$payStatus = $payStatus;
				else 					$payStatus = 'Completed with ' . $countFailed . ' Failed Transaction(s)';
			} else $payStatus = $payStatus;

			//ambil dari slip_detail - begin
			$getPaymentDetail 	= new display_Model_Paymentreport();
			$pslipdetail = $getPaymentDetail->getPslipDetail($PS_NUMBER);

			$htmldataDetailDetail = '';

			foreach ($pslipdetail as $pslipdetaillist) {
				if ($pslipdetaillist['PS_FIELDTYPE'] == 1) {
					$value = Application_Helper_General::displayMoney($pslipdetaillist['PS_FIELDVALUE']);
				} elseif ($pslipdetaillist['PS_FIELDTYPE'] == 2) {
					//$value = Application_Helper_General::convertDate($pslipdetaillist['PS_FIELDVALUE'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					$value = $pslipdetaillist['PS_FIELDVALUE'];
				} elseif ($pslipdetaillist['PS_FIELDTYPE'] == 3) {
					$value = $pslipdetaillist['PS_FIELDVALUE'];
				} else {
					$value = '';
				}

				$htmldataDetailDetail .= '
				<tr>
					<td class="">' . $this->language->_($pslipdetaillist['PS_FIELDNAME']) . '</td>
					<td class="">' . $this->language->_($value) . '</td>
				</tr>
				';
			}
			// print_r($htmldataDetailDetail);die;
			if (isset($htmldataDetailDetail)) $this->view->templateDetail = $htmldataDetailDetail;
			//ambil dari slip_detail - end
			$this->_tableMstleft[0]["label"] = $this->language->_('Payment Type');
			$this->_tableMstleft[1]["label"] = $this->language->_('Payment Ref') . "#";
			$this->_tableMstleft[2]["label"] = $this->language->_('Payment Subject');
			$this->_tableMstleft[3]["label"] = $this->language->_('Trans#');
			//echo '<pre>';
			//var_dump($pslip);die;
			if ($pslip['PS_TYPE'] == 19 && $pslip['numtrx'] > 1) {
				$this->_tableMstleft[5]["label"] = ' ';
			}else if($pslip['PS_CATEGORY'] == 'BULK PAYMENT'){
				$this->_tableMstleft[5]["label"] = ' ';
			}else{
				$this->_tableMstleft[5]["label"] = $this->language->_('Message');
			}
			// $this->_tableMstleft[5]["label"] = $this->language->_('Additional Message');

			$tra_type1 = $trfType[$pslip['TRANSFER_TYPE']];

			$tra_type2	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");
			$tra_type3 = $tra_type2[$pslip['TRANSFER_TYPE']];
                    
            if ($pslip['PS_TYPE'] == 19) {
                 $payType = 'CP Same Bank Remains';
            }else if ($pslip['PS_TYPE'] == 20) {
                 $payType = 'CP Same Bank Maintains';
            }else if ($pslip['PS_TYPE'] == 23) {
                 $payType = 'CP Others Remains - '.$tra_type3;
            }else if ($pslip['PS_TYPE'] == 21) {
                 $payType = 'MM - '.$tra_type3;
            }else if($pslip['PS_TYPE'] == '25' || $pslip['PS_TYPE'] == '4' || $pslip['PS_TYPE'] == '26' || $pslip['PS_TYPE'] == '28' || $pslip['PS_TYPE'] == '3' || $pslip['PS_TYPE'] == '27'){
				$payType = $pslip['payType'];
			}else {
                $payType = $pslip['payType'] . ' - ' . $tra_type1;
            }
			if($pslip['paySubj'] == ''){
				$pslip['paySubj'] = 'no subject';
			}
			$this->_tableMstleft[0]["value"] = $payType;
			$this->_tableMstleft[1]["value"] = $PS_NUMBER;
			$this->_tableMstleft[2]["value"] = $pslip['paySubj'];
			// $buttondownload = '';
			if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" || ($pslip["PS_CATEGORY"] == "SWEEP PAYMENT" ) && $pslip['numtrx'] > 1) {
				// download trx bulk file
				$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $pslip['PS_NUMBER']), null, true);

				$buttondownload = $this->view->formButton('download', $this->language->_('download'), array('class' => 'btnwhite hov ml-1', 'onclick' => "window.location = '" . $downloadURL . "';"));
			}
			$this->_tableMstleft[3]["value"] = $pslip['numtrx'] . " " . $buttondownload;
			
			// $this->_tableMstleft[3]["value"] = $pslip[''];
			// $this->_tableMstleft[4]["value"] = $pslip['paySubj'];

			if ( $pslip['PS_CATEGORY'] == 'OPEN TRANSFER') {
			if(!empty($pslip['BENEFICIARY_ID'])){
			 $this->getBenefData($pslip['BENEFICIARY_ID']);
			}else{
				$citizenshipArr	= array("W" => "WNI", "N" => "WNA");
				$residentArr = array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);

				$benefData = $pslip;
				//var_dump($benefData['BENEFICIARY_RESIDENT']);die;
				//echo '<pre>';
				//var_dump($detail[0]);die;
				$tableMst[0]["label"] = $this->language->_('Beneficiary Bank');
				$tableMst[1]["label"] = $this->language->_('City');
				$tableMst[2]["label"] = $this->language->_('Beneficiary Account');
				$tableMst[3]["label"] = $this->language->_('Beneficiary Name');
				$tableMst[4]["label"] = $this->language->_('Beneficiary Alias Name');
				$tableMst[5]["label"] = $this->language->_('Beneficiary Address 1');
				$tableMst[6]["label"] = $this->language->_('Beneficiary Address 2');
				$tableMst[7]["label"] = $this->language->_('Beneficiary Email');
				$tableMst[8]["label"] = $this->language->_('Citizenship');
				$tableMst[9]["label"] = $this->language->_('Nationality');
				$tableMst[10]["label"] = $this->language->_('Beneficiary Category');
				$tableMst[11]["label"] = $this->language->_('Beneficiary ID Type');
				$tableMst[12]["label"] = $this->language->_('Beneficiary ID Number');

				if (!empty($benefData['BENEF_ACCT_BANK_CODE'])) {
                      $benefBankName = Application_Helper_General::getBankTableName($benefData['BENEF_ACCT_BANK_CODE']);
                 }
                else{
                       $benefBankName = $this->masterBankName;
                }
				$tableMst[0]["value"] = $benefBankName;
				$tableMst[1]["value"] = empty($benefData['BANK_CITY']) ? '-' : $benefData['BANK_CITY'];
				$tableMst[2]["value"] = $benefData['BENEFICIARY_ACCOUNT'];
				$tableMst[3]["value"] = $benefData['BENEFICIARY_ACCOUNT_NAME'];
				$tableMst[4]["value"] = empty($benefData['BENEFICIARY_ALIAS_NAME']) ? '-' : $benefData['BENEFICIARY_ALIAS_NAME'];
				$tableMst[5]["value"] = empty($benefData['BENEFICIARY_ADDRESS']) ? '-' : $benefData['BENEFICIARY_ADDRESS'];
				$tableMst[6]["value"] = empty($benefData['BENEFICIARY_ADDRESS2']) ? '-' : $benefData['BENEFICIARY_ADDRESS2'];
				$tableMst[7]["value"] = empty($benefData['BENEFICIARY_EMAIL']) ? '-' : $benefData['BENEFICIARY_EMAIL'];
				$tableMst[8]["value"] = empty($benefData['BENEFICIARY_RESIDENT']) ? '-' : $residentArr[$benefData['BENEFICIARY_RESIDENT']];
				$tableMst[9]["value"] = empty($benefData['BENEFICIARY_CITIZENSHIP']) ? '-' : $citizenshipArr[$benefData['BENEFICIARY_CITIZENSHIP']];
				$tableMst[10]["value"] = empty($LLD_CATEGORY_POST) ? '-' : $LLD_CATEGORY_POST;
				$tableMst[11]["value"] = empty($benefData['BENEFICIARY_ID_TYPE']) ? '-' : $benefData['BENEFICIARY_ID_TYPE'];
				$tableMst[12]["value"] = empty($benefData['BENEFICIARY_ID_NUMBER']) ? '-' : $benefData['BENEFICIARY_ID_NUMBER'];

				$this->view->tableBene 		= $tableMst;
			}
		}



		$benefDt = $pslip;

		$tableMstr[0]["label"] = $this->language->_('Beneficiary Bank');
		$tableMstr[1]["label"] = $this->language->_('City');
		$tableMstr[2]["label"] = $this->language->_('Beneficiary Account');
		$tableMstr[3]["label"] = $this->language->_('Beneficiary Name');
		$tableMstr[4]["label"] = $this->language->_('Beneficiary Alias Name');
		$tableMstr[5]["label"] = $this->language->_('Beneficiary Address 1');
		$tableMstr[6]["label"] = $this->language->_('Beneficiary Address 2');
		$tableMstr[7]["label"] = $this->language->_('Beneficiary Email');
		$tableMstr[8]["label"] = $this->language->_('Citizenship');
		$tableMstr[9]["label"] = $this->language->_('Nationality');
		$tableMstr[10]["label"] = $this->language->_('Beneficiary Category');
		$tableMstr[11]["label"] = $this->language->_('Beneficiary ID Type');
		$tableMstr[12]["label"] = $this->language->_('Beneficiary ID Number');

		$tableMstr[0]["value"] = empty($benefDt['BANK_NAME']) ? $this->_bankName : $benefDt['BANK_NAME'];
		$tableMstr[1]["value"] = empty($benefDt['BANK_CITY']) ? '-' : $benefDt['BANK_CITY'];
		$tableMstr[2]["value"] = $benefDt['BENEFICIARY_ACCOUNT'];
		$tableMstr[3]["value"] = $benefDt['BENEFICIARY_NAME'];
		$tableMstr[4]["value"] = empty($benefDt['BENEFICIARY_ALIAS']) ? '-' : $benefDt['BENEFICIARY_ALIAS'];
		$tableMstr[5]["value"] = empty($benefDt['BENEFICIARY_ADDRESS']) ? '-' : $benefDt['BENEFICIARY_ADDRESS'];
		$tableMstr[6]["value"] = empty($benefDt['BENEFICIARY_ADDRESS2']) ? '-' : $benefDt['BENEFICIARY_ADDRESS2'];
		$tableMstr[7]["value"] = empty($benefDt['BENEFICIARY_EMAIL']) ? '-' : $benefDt['BENEFICIARY_EMAIL'];
		$tableMstr[8]["value"] = empty($benefDt['BENEFICIARY_RESIDENT']) ? '-' : $residentArr[$benefDt['BENEFICIARY_RESIDENT']];
		$tableMstr[9]["value"] = empty($benefDt['BENEFICIARY_CITIZENSHIP']) ? '-' : $citizenshipArr[$benefDt['BENEFICIARY_CITIZENSHIP']];
		$tableMstr[10]["value"] = empty($LLD_CATEGORY_POST) ? '-' : $LLD_CATEGORY_POST;
		$tableMstr[11]["value"] = empty($benefDt['BENEFICIARY_ID_TYPE']) ? '-' : $benefDt['BENEFICIARY_ID_TYPE'];
		$tableMstr[12]["value"] = empty($benefDt['BENEFICIARY_ID_NUMBER']) ? '-' : $benefDt['BENEFICIARY_ID_NUMBER'];


		$this->view->tableBnf 		= $tableMstr;

		//var_dump($pslip);die;


			$tableTransSum[0]["label"] = $this->language->_('Amount');
			$tableTransSum[1]["label"] = $this->language->_('Total Transfer Fee');
			$tableTransSum[2]["label"] = $this->language->_('Total');

			$tableTransSum[0]["value"] = $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']);
			$tableTransSum[1]["value"] = $pslip['TRANSFER_FEE'] == '0' ? '-' : $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['TRANSFER_FEE']);
			$tableTransSum[2]["value"] = '<h6 style="color: red;"><b>' . $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney(($pslip['TRA_AMOUNT'] + $pslip['TRANSFER_FEE'] )) . '</b></h6>';
			$this->view->tableTransSum 		= $tableTransSum;

			// if (($pslip['PS_CATEGORY'] == 'SINGLE PAYMENT' || $pslip['PS_CATEGORY'] == 'OPEN TRANSFER')) {
			// 	if ($pslip['TRANSFER_TYPE'] == 0) {
			// 		if ($pslip['SOURCE_ACCT_BANK_CODE'] == $pslip['BENEF_ACCT_BANK_CODE']) {
			// 			$payType = $pslip['payType'] . ' - ' . 'PB';
			// 		} else {
			// 			$payType = $pslip['payType'] . ' - ' . 'ONLINE';
			// 		}
			// 	} else if ($pslip['TRANSFER_TYPE'] == 1) {
			// 		$payType = $pslip['payType'];
			// 	} else if ($pslip['TRANSFER_TYPE'] == 2) {
			// 		$payType = $pslip['payType'];
			// 	} else {
			// 		$payType = $pslip['payType'];
			// 	}
			// } else if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

			// 	if ($pslip['PS_TYPE'] == '19') {
			// 		$poolingType = 'Remains';
			// 	} else {
			// 		$poolingType = 'Maintains';
			// 	}

			// 	$payType = $pslip['payType'] . ' - ' . $poolingType;
			// } else {
			// 	$payType = $pslip['payType'];
			// }

			$select  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'), array('P.*', 'A.*'))
				->joinLeft(array('A' => 'T_PERIODIC'), 'P.PS_PERIODIC = A.PS_PERIODIC', array())
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array('*'))
				->joinLeft(array('C' => 'T_PSLIP_DETAIL'), 'P.PS_NUMBER = C.PS_NUMBER', array('*'))
				->where('P.PS_NUMBER = ?', $PS_NUMBER);

			$cekPsNumber = $this->_db->fetchRow($select);
			$PS_EVERY_PERIODIC_UOM = $cekPsNumber['PS_EVERY_PERIODIC_UOM'];
			$PS_PERIODIC_NUMBER = $cekPsNumber['PS_PERIODIC_NUMBER'];
			$PS_EVERY_PERIODIC_NEW = $cekPsNumber['PS_EVERY_PERIODIC'];

			if ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 1) {
				$select = $this->_db->select()
					->from(array('C' => 'T_PERIODIC_DAY', array('C.DAY_ID')))
					->where("C.PERIODIC_ID  = ?", $cekPsNumber["PS_PERIODIC"]);

				$PERIODIC_DAY = $this->_db->fetchAll($select);
				$days = array();
				foreach ($PERIODIC_DAY as $key) {
					$days[] = (int) $key['DAY_ID'];
				}
				$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
				$dateNow = date("Y-m-d H:i");
				$result = $date;
				foreach ($days as $day) {
					if ($dateNow > $result) {
						$date = $dateNow;
						$dayofweek = date('N', strtotime($date));
						$result = date('Y-m-d', strtotime(($day - $dayofweek) . ' day', strtotime($date)));
						$result = $result . ' ' . $cekPsNumber["PS_EFTIME"];
					}
				}

				if ($result < $dateNow) {
					$date = date("Y-m-d", strtotime("+1 week", strtotime($date)));
					$dayofweek = date('N', strtotime($date));
					$result = date('Y-m-d H:i', strtotime(($days[0] - $dayofweek) . ' day', strtotime($date)));
				}
				$date = $result;
				$date = strtotime($date);
				if (date('Y-m-d', $date) == date('Y-m-d')) {
					$nextExecute = $this->language->_('Today') . ' @' . date('H:i', $date);
				} else {
					$nextExecute = date('d M Y @H:i', $date);
				}
			} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 2 || $cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 3) {
				$date = $cekPsNumber["PS_PERIODIC_NEXTDATE"];
				$nextExecute = $date;

			} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 5) {
				$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
				$dateNow = date("Y-m-d H:i");

				if ($dateNow > $date) {
					$date = $dateNow;
				}
				$dayofweek = date('N', strtotime($date));
				$result = date('Y-m-d', strtotime(($cekPsNumber['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
				$result = $result . ' ' . $cekPsNumber["PS_EFTIME"];
				if ($result < $dateNow) {
					$date = date("Y-m-d", strtotime("+1 week"));
					$result = date('Y-m-d', strtotime(($cekPsNumber['PS_EVERY_PERIODIC'] - $dayofweek) . ' day', strtotime($date)));
				}
				$nextExecute = date("Y-m-d", strtotime($result));
			} elseif ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] == 6) {
				$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
				$dateNow = date("Y-m-d H:i");

				if ($date < $dateNow) {
					$nextExecute = date("Y-m-d", strtotime("+1 month", strtotime($date)));
				}
			}

			// if ($cekPsNumber["PS_EVERY_PERIODIC_UOM"] != 1) {
			// 	$date = $cekPsNumber["PS_EFDATE"] . ' ' . $cekPsNumber["PS_EFTIME"];
			// 	// var_dump($date);
			// 	$date = strtotime($date);
			// 	if (date('Y-m-d', $date) == date('Y-m-d')) {
			// 		$nextExecute = $this->language->_('Today') . ' @' . date('H:i', $date);
			// 	} else {
			// 		$nextExecute = date('d M Y @H:i', $date);
			// 	}
			// }

			// var_dump($nextExecute);
			// die();
			
			$nextpaymentdate = substr(Application_Helper_General::convertDate($nextExecute, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
			$this->view->billerid = $cekPsNumber['BILLER_ORDER_ID'];
		if($cekPsNumber['PS_TYPE'] == '17'){
			$billertype = 'Payment';
		}else if($cekPsNumber['PS_TYPE'] == '16'){
			$billertype = 'Purchase';
		}
		$this->view->billertype = $billertype;

		if($cekPsNumber['PS_TYPE'] == '17' || $cekPsNumber['PS_TYPE'] == '16' ){
			
			$biller  = $this->_db->select()
							->from(array('D' => 'T_PSLIP_DETAIL'), array('*'))
							->where('D.PS_NUMBER = ?', $PS_NUMBER)
							->query()->fetchAll();
		}

		foreach($biller as $key => $val){
			if($val['PS_FIELDNAME'] == 'Type Of Transaction'){
				$billtype = $val['PS_FIELDVALUE'];
			}else if($val['PS_FIELDNAME'] == 'Service Provider'){
				$billname = $val['PS_FIELDVALUE'];
			}else if($val['PS_FIELDNAME'] == 'Customer Name'){
				$billcust = $val['PS_FIELDVALUE'];
			}
		}

		//$this->view->billertype = $billtype;
		if(empty($billname)){
			$str = $this->string_between_two_string($cekPsNumber['TRA_MESSAGE'],'ID Pelanggan = ',' no');
			$billname = $str;
			
		}
		$logdata = json_decode($cekPsNumber['LOG'], true); 
			if($logdata['Module'] == 'Phone'){
				$custname = $logdata['CUSTOMER_NAME'];
				$this->view->billercust = $custname;
			}
			$billtype = $logdata['TypeOfTrans'];
	//var_dump($billername);die;
		if($pslip['PS_BILLER_ID'] == '1158'){
			$billtype = 'Tax';
			$billname = 'MPN G3';
		}
		$custname = $logdata['CUSTOMER_NAME'];
		$this->view->billercust = $custname;
		$this->view->billertype = $billtype;
		$this->view->billername = $billname;
			
			
			$arrdayongoing = array(
					'0' => 'sun',
					'1' => 'mon',
					'2' => 'tue',
					'3' => 'wed',
					'4' => 'thu',
					'5' => 'fry',
					'6' => 'sat'

				);

			$PeriodIdSweepIn = $pslip['PS_PERIODIC'];
			if($PeriodIdSweepIn){
			$selectday	= $this->_db->select()
				->from(array('TTS' => 'T_PERIODIC_DAY'))
				->where('TTS.PERIODIC_ID = ?', $PeriodIdSweepIn);
			
			$report_day = $this->_db->fetchAll($selectday);
			
			//var_dump($report_day);
			if (!empty($report_day)) {
							foreach ($report_day as $key => $value) {
								//var_dump($value['DAY_ID']); 
								${'pooling_' . $arrdayongoing[$value['DAY_ID']]} = $value['LIMIT_AMOUNT'];
							}
			}
			//var_dump($arrdayongoing);
			//var_dump($pooling_sun);
			$this->view->pooling_sun = $pooling_sun;
			$this->view->pooling_mon = $pooling_mon;
			$this->view->pooling_tue = $pooling_tue;
			$this->view->pooling_wed = $pooling_wed;
			$this->view->pooling_thu = $pooling_thu;
			$this->view->pooling_fry = $pooling_fry;
			$this->view->pooling_sat = $pooling_sat;

			$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
			$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
			$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
			$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
			$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
			$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
			$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);
			
			
			$selectperiodic	= $this->_db->select()
				->from(array('TTS' => 'T_PERIODIC'))
				->where('TTS.PS_PERIODIC = ?', $PeriodIdSweepIn);			
			$selectperiodic = $this->_db->fetchRow($selectperiodic);
			$nowdate = date('Y-m-d');
			
			}

			$this->_tableMstright[0]["label"] = $this->language->_('Frequently');
			//echo '<pre>';
			//var_dump($pslip);
			//if sweep 
			if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

				if  (empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[2]["label"] = $this->language->_('Payment Date');
					$this->_tableMstright[2]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					$repeat = '1x';
				}else{
					if ($PS_EVERY_PERIODIC_UOM == '3' || $PS_EVERY_PERIODIC_UOM == '6') {
						$this->_tableMstright[1]["label"] = $this->language->_('Repeat on date');
					}else{
						$this->_tableMstright[1]["label"] = $this->language->_('Repeat on');
					}
					
					$this->_tableMstright[2]["label"] = $this->language->_('Start From');
				}

				$this->_tableMstright[3]["label"] = $this->language->_('Time');

				

				if (!empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[4]["label"] = $this->language->_('Last Updated Date');
					$this->_tableMstright[4]["value"] = substr(Application_Helper_General::convertDate($pslip['PS_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					$this->_tableMstright[5]["label"] = $this->language->_('Payment Date');
					$this->_tableMstright[5]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
				}
				
				if  (empty($pslip['PS_PERIODIC'])) {
					// $this->_tableMstright[2]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					// $repeat = '1x';
					// $repeatOn = '-';

					// $efDate = date_create($pslip['PS_EFDATE']);
					// $newEfDate = date_format($efDate, "d M Y");

					// $startFrom = $newEfDate;
				}
				else{
					
					$select	= $this->_db->select()
							->from(array('P'	 		=> 'T_PSLIP'))
							->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
							->joinLeft(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
							->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
							->GROUP('D.DAY_ID');

					$periodicData = $this->_db->fetchAll($select);
					
					$arrday = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);
					
					if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 1) {

						$repeat = 'Daily';
						$repeatOnPdf = '<div class="input-group">';
						foreach ($periodicData as $key => $value) {
						$repeatOnPdf .= substr($arrday[$value['DAY_ID']], 0, 1).'   ';
						}
						//echo '<pre>'; 
						//var_dump($periodicData);die;
						$repeatOnPdf .= '</div>';
						
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						foreach ($periodicData as $key => $value) {
						$labelcheck = 'check'.$value['DAY_ID'];
						${$labelcheck} = 'checked';
						
						}
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 2 || $periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 5) {
						$repeat = 'Weekly';
						$repeatOnPdf = $arrday[$periodicData[0]['PS_EVERY_PERIODIC']];
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						
						$labelcheck = 'check'.$periodicData[0]['PS_EVERY_PERIODIC'];
						${$labelcheck} = 'checked';
 
						 foreach ($periodicData as $key => $value) {
						 $labelcheck = 'check'.$value['DAY_ID'];
						 ${$labelcheck} = 'checked';
						
						 }
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else {
						$repeat = 'Monthly';
						$repeatOn = $periodicData[0]['PS_EVERY_PERIODIC'];
						$repeatOnPdf = $periodicData[0]['PS_EVERY_PERIODIC'];
					}

					$startDate = date_create($periodicData[0]['PS_PERIODIC_STARTDATE']);
					$newStartDate = date_format($startDate, "d M Y");

					$endDate = date_create($periodicData[0]['PS_PERIODIC_ENDDATE']);
					$newEndDate = date_format($endDate, "d M Y");

					$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;

					// $arrday = array(
					// 	'0' => 'Sunday',
					// 	'1' => 'Monday',
					// 	'2' => 'Tuesday',
					// 	'3' => 'Wednesday',
					// 	'4' => 'Thursday',
					// 	'5' => 'Friday',
					// 	'6' => 'Saturday'
					// );

					// if ($pslip['PS_TYPE'] == '19') {
					// 	//remain
					// 	$amountlabel = $this->language->_('Remain on source');

					// } else if ($pslip['PS_TYPE'] == '20') {
					// 	$amountlabel = $this->language->_('Maintain Beneficiary');

					// 	$maintainsData = '';

					// 	foreach ($periodicData as $key => $value) {
					// 		$maintainsData .= substr($arrday[$value['DAY_ID']], 0, 3) . ' IDR ' . Application_Helper_General::displayMoney($value['LIMIT_AMOUNT']) . '<br>';
					// 	}

					// 	$content = "<div class='text-center'>".$maintainsData."</div>";

					// 	// $amount = '<button type="button" class="btngrad hov" data-html="true" tabindex="0" data-toggle="popover" data-trigger="focus" title="Maintains Config" data-content="'.$content.'">View</button>';
						
					// }
					$this->_tableMstright[1]["value"] = $repeatOn;
					$this->_tableMstright[2]["value"] = $startFrom;
				}

				$AESMYSQL = new Crypt_AESMYSQL();
				$encrypted_pwd = $AESMYSQL->encrypt($PS_NUMBER, $rand);
				$encreff = urlencode($encrypted_pwd);

				if (($pslip['PS_STATUS'] == 5  || $pslip['PS_STATUS'] == 7 || $pslip['PS_STATUS'] == 14) && $pslip['PS_EFDATE'] >= $nowdate && $selectperiodic['PS_PERIODIC_ENDDATE'] >= $nowdate) {
					$this->_tableMstright[0]["value"] = ($repeat == '1x') ? $repeat : $repeat .' (FreqRef# <a href="/paymentworkflow/ongoing/viewdetail/payReff/'.$encreff.'">'.$PS_PERIODIC_NUMBER.'</a>)';
				}else{
					$this->_tableMstright[0]["value"] = ($repeat == '1x') ? $repeat : $repeat;
				}

				if ($pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '23' || $pslip['PS_TYPE'] == '30' ) {
					$amountlabel = $this->language->_('Remain on source');
				} else if ($pslip['PS_TYPE'] == '20'  ) {
					$amountlabel = $this->language->_('Maintain Beneficiary');					
				}else if($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15'){
					//var_dump($pslip);
					if($pslip['PS_SWEEP_TYPE'] == '1'){
							$amountlabel = $this->language->_('Remain on source');
					}else{
							$amountlabel = $this->language->_('Maintain Beneficiary');					
					}
				}

				
				
				$amount1 	 = '<span>' . $pslip['ccy'] .' '.Application_Helper_General::displayMoney($pslip['PS_MIN_AMOUNT']).'</span>';

				$eftime = date('h:i A', strtotime($pslip['PS_EFTIME']));
				$this->_tableMstright[3]["value"] = $eftime;
				$this->_tableMstright[6]["label"] = $amountlabel;
				if ($pslip['PS_TXCOUNT'] > 1) {
					$amount= '-';
					$pdfamount = '-';
					$counttrans = $pslip['numtrx']."<strong class='formreq' style='color:red;'>&nbsp;*</strong>";
				}else{
					$counttrans = $pslip['numtrx'];
					if ($pslip["PS_TYPE"] == '20' && $PS_EVERY_PERIODIC_UOM == '1') {
						$amount = ' - <button type="button" class="btngrad hov" class="toogleModal text-primary" data-toggle="modal" data-target="#poolingConfirmModal">View Config</button>';
						$pdfamount = '-'."<strong class='formreq' style='color:red;'>&nbsp;*</strong>";
					}else{
						$amount = '<span>' . $pslip['ccy'] . ' '.Application_Helper_General::displayMoney($pslip['TRA_REMAIN']).'</span>';
						$pdfamount = '<span>' . $pslip['ccy'] . ' '.Application_Helper_General::displayMoney($pslip['TRA_REMAIN']).'</span>';
					}
				}
				$this->_tableMstright[6]["value"] = $amount;
				$this->_tableMstright[7]["label"] = 'Minimum Transfer';
				$this->_tableMstright[7]["value"] = $amount1;

			if ($pslip['TRANSFER_TYPE'] == '1' && ($pslip['PS_TYPE'] == '23' || $pslip['PS_TYPE'] == '30' )) {
					$this->_tableMstright[8]["label"] = $this->language->_('If sweep exceed RTGS');
					$this->_tableMstright[8]["value"] = 'Split if amount exceed RTGS limit';
			}else if($pslip['TRANSFER_TYPE'] == '2' && ($pslip['PS_TYPE'] == '23' || $pslip['PS_TYPE'] == '30' )){
					$this->_tableMstright[8]["label"] = $this->language->_('If sweep exceeds SKN');
					$this->_tableMstright[8]["value"] = 'Cancel if amount exceed SKN limit';
				}
				
				$this->_tableMstright[9]["label"] = $this->language->_('Status');
				$this->_tableMstright[9]["value"] = $payStatus.' - '.$transStatus;
			}
			else{

				if  (empty($pslip['PS_PERIODIC'])) {
					$repeat = '1x';
					$this->_tableMstright[1]["label"] = $this->language->_('Payment Date');
					$this->_tableMstright[1]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
					$this->_tableMstright[2]["label"] = $this->language->_(' ');
					$this->_tableMstright[2]["value"] = '';
				}
				else{
					// $select	= $this->_db->select()
					// 		->from(array('S' => 'T_PERIODIC'))
					// 		->where("S.PS_PERIODIC = ?", (string) $pslip['PS_PERIODIC']);

					// $periodicData = $this->_db->fetchAll($select);

					// if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] === null) {
					// 	$repeat = '1x';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '1') {
					// 	$repeat = 'Daily';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '2') {
					// 	$repeat = 'Weekly';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '3') {
					// 	$repeat = 'Monthly';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '4') {
					// 	$repeat = 'Yearly';
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '5') {
					// 	$periodicEveryArr = array(
					// 		'1' => $this->language->_('Monday'),
					// 		'2' => $this->language->_('Tuesday'),
					// 		'3' => $this->language->_('Wednesday'),
					// 		'4' => $this->language->_('Thursday'),
					// 		'5' => $this->language->_('Friday'),
					// 		'6' => $this->language->_('Saturday'),
					// 		'7' => $this->language->_('Sunday'),
					// 	);
					// 	$repeat = $this->language->_('Every Day of ') . $periodicEveryArr[$periodicData[0]['PS_EVERY_PERIODIC']];
					// } elseif ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == '6') {
					// 	$repeat = $this->language->_('Every Date of ') . $periodicData[0]['PS_EVERY_PERIODIC'];
					// } else {
					// 	$repeat = '-';
					// }

					

					$select1	= $this->_db->select()
							->from(array('P'	 		=> 'T_PSLIP'))
							->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
							->joinLeft(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
							->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
							->GROUP('D.DAY_ID');

					$periodicData1 = $this->_db->fetchAll($select1);
					//echo '<pre>';
					//var_dump($periodicData1);die; 
					$arrday = array(
						'0' => 'Sunday',
						'1' => 'Monday',
						'2' => 'Tuesday',
						'3' => 'Wednesday',
						'4' => 'Thursday',
						'5' => 'Friday',
						'6' => 'Saturday'
					);
					if ($periodicData1[0]['PS_EVERY_PERIODIC_UOM'] == 1) {

						$repeat = 'Daily';
						$repeatOnPdf = '<div class="input-group">';
						foreach ($periodicData1 as $key => $value) {
							$repeatOnPdf .= substr($arrday[$value['DAY_ID']], 0, 1).'   ';
						}
						$repeatOnPdf .= '</div>';
						
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						foreach ($periodicData1 as $key => $value) {
						$labelcheck = 'check'.$value['DAY_ID'];
						//var_dump($labelcheck);
						${$labelcheck} = 'checked';
						
						}
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;">
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" checked />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						//echo $repeatOn;				
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
						
					} else if ($periodicData1[0]['PS_EVERY_PERIODIC_UOM'] == 2 || $periodicData1[0]['PS_EVERY_PERIODIC_UOM'] == 5) {
						$repeat = 'Weekly';
						$repeatOnPdf = $arrday[$periodicData1[0]['PS_EVERY_PERIODIC']];
						$repeatOn = '<div class="input-group">';
						
						$check0 = '';
						$check1 = '';
						$check2 = '';
						$check3 = '';
						$check4 = '';
						$check5 = '';
						$check6 = '';
						
						$labelcheck = 'check'.$periodicData1[0]['PS_EVERY_PERIODIC'];
						${$labelcheck} = 'checked';

						 foreach ($periodicData1 as $key => $value) {
						 $labelcheck = 'check'.$value['DAY_ID'];
						 ${$labelcheck} = 'checked';
						
						 }
						
						$repeatOn .= '<div class="roundChk" style="margin-left: 0px;>
											<input type="checkbox" class="checkboxday" id="checkbox6" value="0" name="report_day[]" disabled="disabled" '.$check0.' />
											<label for="checkbox6" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox" value="1" name="report_day[]" disabled="disabled" '.$check1.' />
											<label for="checkbox" style="height: 25px!important;">M</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox1" value="2" name="report_day[]" disabled="disabled" '.$check2.' />
											<label for="checkbox1" style="height: 25px!important;">T</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox2" value="3" name="report_day[]" disabled="disabled" '.$check3.' />
											<label for="checkbox2" style="height: 25px!important;">W</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox3" value="4" name="report_day[]" disabled="disabled" '.$check4.' />
											<label for="checkbox3" style="height: 25px!important;">T</label>
										</div>';
										
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox4" value="5" name="report_day[]" disabled="disabled" '.$check5.' />
											<label for="checkbox4" style="height: 25px!important;">F</label>
										</div>';
						$repeatOn .= '<div class="roundChk">
											<input type="checkbox" class="checkboxday" id="checkbox5" value="6" name="report_day[]" disabled="disabled" '.$check6.' />
											<label for="checkbox5" style="height: 25px!important;">S</label>
										</div>';
						$repeatOn .= '</div>';
					} else {
						$repeat = 'Monthly';
						$repeatOn = $PS_EVERY_PERIODIC_NEW;
						$repeatOnPdf = $PS_EVERY_PERIODIC_NEW;
					}

					$startDate = date_create($periodicData1[0]['PS_PERIODIC_STARTDATE']);
					$newStartDate = date_format($startDate, "d M Y");

					$endDate = date_create($periodicData1[0]['PS_PERIODIC_ENDDATE']);
					$newEndDate = date_format($endDate, "d M Y");

					$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;

					if ($PS_EVERY_PERIODIC_UOM == '3') {
						$this->_tableMstright[3]["label"] = $this->language->_('Repeat on date');
					}else{
						$this->_tableMstright[3]["label"] = $this->language->_('Repeat on');
					}

					$this->_tableMstright[3]["value"] = $repeatOn;
					$this->_tableMstright[4]["label"] = $this->language->_('Start From');
					$this->_tableMstright[4]["value"] = $startFrom;
					$this->_tableMstright[5]["label"] = $this->language->_('Last Updated Date');
					$this->_tableMstright[5]["value"] = $pslip['PS_UPDATED'];
				}

				$AESMYSQL = new Crypt_AESMYSQL();
				$encrypted_pwd = $AESMYSQL->encrypt($PS_NUMBER, $rand);
				$encreff = urlencode($encrypted_pwd);
			//echo '<pre>';
			//var_dump($pslip);die;
				if (($pslip['PS_STATUS'] == 5  || $pslip['PS_STATUS'] == 7 || $pslip['PS_STATUS'] == 14) && $pslip['PS_EFDATE'] >= $nowdate && $selectperiodic['PS_PERIODIC_ENDDATE'] >= $nowdate) {
					$this->_tableMstright[0]["value"] = ($repeat == '1x') ? $repeat : $repeat .' (FreqRef# <a href="/paymentworkflow/ongoing/viewdetail/payReff/'.$encreff.'">'.$PS_PERIODIC_NUMBER.'</a>)';
				}else{
					$this->_tableMstright[0]["value"] = ($repeat == '1x') ? $repeat : $repeat;
				}

				if (!empty($pslip['PS_PERIODIC'])) {
					$this->_tableMstright[6]["label"] = $this->language->_('Payment Date');
					$this->_tableMstright[6]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
				}
				
				$this->_tableMstright[7]["label"] = $this->language->_('Amount');
				if($pslip['PS_TYPE'] == '25' || $pslip['PS_TYPE'] == '11' || $pslip['PS_TYPE'] == '4'){
					$this->_tableMstright[7]["value"] = '<span style="color: red;"><b>' .$pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['PS_TOTAL_AMOUNT']).'</b></span>';
				}else{
					$this->_tableMstright[7]["value"] = '<span style="color: red;"><b>' .$pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']).'</b></span>';
				}
				$this->_tableMstright[8]["label"] = $this->language->_('Status');
				$this->_tableMstright[8]["value"] = $payStatus.' - '.$transStatus;

			}
			
			$this->view->pdfpaytype = $payType;
			$this->view->pdfpsnumber = $PS_NUMBER;
			$this->view->pdfpaysubject= $pslip['paySubj'];
			$this->view->pdftrans= $counttrans;
			$this->view->pdftramessage = $pslip['TRA_MESSAGE'];

			$this->view->PS_EVERY_PERIODIC_UOM = $PS_EVERY_PERIODIC_UOM;
			$this->view->pdfnextpaymentdate = $nextpaymentdate;
			$this->view->pdfrepeaton = $repeatOnPdf;
			$this->view->pdftime = $eftime;
			$this->view->pdfstartfrom = $startFrom;
			$this->view->pdftraremain = $pdfamount;
			$this->view->pdfminamount = '<span>' . $pslip['ccy'] .' '.Application_Helper_General::displayMoney($pslip['PS_MIN_AMOUNT']).'</span>';

			$this->view->pdffrequently = $repeat;
			$this->view->pdfpaymentdate = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
			$this->view->pdftotalamount = '<span style="color: red;"><b>' .$pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['amount']).'</b></span>';
			$this->view->pdfstatus = $payStatus.' - '.$transStatus;

			// if ($pslip['PS_TYPE'] == '19') {
			// 	//remain
			// 	$amountlabel = $this->language->_('Remain on source');
			// 	$amount 	 = 'IDR n/a';
			// } else if ($pslip['PS_TYPE'] == '20') {
			// 	$amountlabel = $this->language->_('Maintains beneficiary');
			// 	$amount 	 = 'IDR (as config)';
			// } else {
			// 	$amountlabel = $this->language->_('Total Amount');
			// 	$amount 	 = $pslip['ccy'] . " " . Application_Helper_General::displayMoney($pslip['amount']);
			// }
			// $this->_tableMstright[2]["label"] = $amountlabel;
			// $this->_tableMstright[2]["value"] = $amount;
			// $this->_tableMstright[3]["label"] = $this->language->_('Payment Status');
			// $this->_tableMstright[3]["value"] = $payStatus;
			// $this->_tableMstright[4]["label"] = $this->language->_('Transaction Status');
			// $this->_tableMstright[4]["value"] = $transStatus;

			// $this->_tableMstleft[1]["label"] = $this->language->_('Payment Subject');
			// $this->_tableMstleft[2]["label"] = $this->language->_('Payment Date');
			// $this->_tableMstleft[3]["label"] = $this->language->_('Message');
			// $this->_tableMstleft[4]["label"] = $this->language->_('Additional Message');


			// View Data
			$this->_tableMst[0]["label"] = $this->language->_('Payment Ref') . "#";
			$this->_tableMst[1]["label"] = $this->language->_('Created Date');
			$this->_tableMst[2]["label"] = $this->language->_('Updated Date');
			$this->_tableMst[3]["label"] = $this->language->_('Payment Date');
			$this->_tableMst[4]["label"] = $this->language->_('Payment Subject');
			$this->_tableMst[5]["label"] = $this->language->_('Master Account');
			$this->_tableMst[6]["label"] = $this->language->_('Payment Type');
			$this->_tableMst[7]["label"] = $this->language->_('Payment Status');
			$this->_tableMst[8]["label"] = $this->language->_('Source Bank Name');
			
			$this->_tableMst[0]["value"] = $PS_NUMBER;
			$this->_tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			$this->_tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			$this->_tableMst[3]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);
			$this->_tableMst[4]["value"] = $pslip['paySubj'];
			$this->_tableMst[5]["value"] = "";
			$this->_tableMst[6]["value"] = $pslip['payType'];
			$this->_tableMst[7]["value"] = $payStatus;

			/*	pending future date = 7 ;
				display button Cancel Future Date, if payment status 'Pending Future Date'
			*/
			//echo "pstype: ";
			//echo $pslip["PS_STATUS"];
			$selectBank	= $this->_db->select()
				->from(
					array('B' => 'M_BANK_TABLE'),
					array(
						'BANK_CODE'			=> 'B.BANK_CODE',
						'BANK_NAME'			=> 'B.BANK_NAME'
					)
				);

			$bankList = $this->_db->fetchAll($selectBank);

			foreach ($bankList as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			if (empty($pslip['SOURCE_ACCT_BANK_CODE'])) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$pslip['SOURCE_ACCT_BANK_CODE']];
			}

			$this->_tableMst[8]["value"] = $bankname;

			if ($pslip['PS_STATUS'] == 7 && $this->view->hasPrivilege('CFDT')) {
				$this->view->paypendingfuture = true;
			} // privillege cancel future date.

			//View File dihidden untuk PAYROLL
			if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && !$pdf && $pslip["PS_TYPE"] != "11") {
				// download trx bulk file
				$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
				$this->_tableMst[9]["label"] = $this->language->_('View File');
				$this->_tableMst[9]["value"] = $this->view->formButton('download', $this->language->_('download'), array('class' => 'btngreen hov', 'onclick' => "window.location = '" . $downloadURL . "';"));
			}

			// separate credit and debet view
			if (
				// $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] ||
				$pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]
			) {
				// die('hee');
				$this->debet($pslip);
			} elseif ($pslip["PS_BILLER_ID"] == '1156' || $pslip["PS_BILLER_ID"] == '1158') {
				// echo 'ss';die;
				$this->etax($pslip);
			} else {
				// die('sene');

				$this->credit($pslip);
			}
			//var_dump($this->_tableMstleft);die;
			//Zend_Debug::dump($pslip["PS_TYPE"]); die;
			// && $pslip['PS_STATUS'] != 5  && != 6 Hannichr2010
			// View Transfer to di hidden untuk PAYROLL
			if ($pslip["PS_CATEGORY"] == "BULK PAYMENT" && $pslip['PS_STATUS'] != 5 && $pslip['PS_STATUS'] != 6 || $pslip['PS_TYPE'] == 11) {
				// $this->view->fields			 = array();
				// $this->view->tableDtl 		 = array();
				// $this->view->TITLE_DTL		 = "";
			}

			/*
				Status payment :
					1. PayStatus (5) completed + TransStatus (3) success
					2. PayStatus (4) rejected
					2. PayStatus (6) transferfailed + TransStatus (4) Failed


			*/
			$txtMessage = '';
			if ($pslip['PS_STATUS'] == 4) {
				$txtMessage 				= $this->language->_('Payment Rejected');
				$this->view->flagstatus 	= true;
			}
			if (($pslip['PS_STATUS'] == 5) || ($pslip['PS_STATUS'] == 7)) {
				$custId 	= $this->_custIdLogin;

				$UUID = $this->_db->select()
					->from(
						array('P'		=> 'T_PSLIP'),
						array('uuid'	=> 'UUID')
					)
					->where('PS_NUMBER = ' . $this->_db->quote($PS_NUMBER) . ' AND CUST_ID = ?', $custId);

				$UUID = $this->_db->fetchOne($UUID);
				//->query()
				//->fetchOne();

				$daterelease = $this->_db->select()
					->from(
						array('P'		=> 'T_PSLIP_HISTORY'),
						array('date'	=> 'DATE_TIME')
					)
					->where('PS_NUMBER = ' . $this->_db->quote($PS_NUMBER) . ' AND CUST_ID = ?', $custId);

				$daterelease = $this->_db->fetchOne($daterelease);
				//date("d/m/Y H:i:s")
				if ($daterelease) {
					$daterelease = Application_Helper_General::convertDate($daterelease, $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
					//$daterelease = date("d/m/Y H:i:s",$daterelease);
				} else $daterelease = '';

				//->query()
				//->fetchOne();
				$txtMessage = "
					<p><h4><u>" . $this->language->_('Release ' . $pslip["payStatus"]) . "</u></h4></p>
					<p><h5>" . $this->language->_('Release Date') . ": <b>" . $daterelease . " </b></h5></p>";

				$this->view->flagstatus 	= true;

				//<p><h5>Trace ID: <b>".$UUID."</b></h5></p>
			}
			if ($pslip['PS_STATUS'] == 6) {
				$txtMessage 				= $this->language->_('Transfer Failed');
				$this->view->flagstatus 	= true;
			}
			if ($pslip['PS_STATUS'] == 9) {
				$txtMessage 				= $this->language->_('Payment Exception');
				$this->view->flagstatus 	= true;
			}

			$this->view->txtMessage = $txtMessage;

			$this->view->pslip 				= $pslip;
			$this->view->PS_NUMBER 			= $PS_NUMBER;
			$this->view->tableMst 			= $this->_tableMst;

			// echo '<pre>';
			// print_r($pslip);die;
			
			if(!empty($pslip) && ($pslip['PS_TYPE'] == '33' || $pslip['PS_TYPE'] == '35'))
			{
				$datacard = $this->_db->fetchRow(
				$this->_db->select()
								   ->from(array('T' => 'T_DEBITCARD'))
								   ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
								   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('REG_NUMBER','VA_NAME','VA_NUMBER','DEBIT_TYPE','DEBIT_ATM','DEBIT_EDC'))
								   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
								   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
								   
								   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=T.DEBIT_NUMBER',array('USER_ID'))
								   ->joinleft(array('G' => 'M_USER'),'F.USER_ID=G.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
								   ->joinLeft(array('B' => 'T_DEBIT_BALANCE'),'B.CUST_ID = T.CUST_ID AND B.ACCT_NO = T.DEBIT_NUMBER',array('BALANCE',
						'rs_datetime' => 'DATE'))
								   ->where('T.DEBIT_NUMBER = ?', $pslip['accsrc'])
								   ->group('T.DEBIT_NUMBER')				 
					 //->order('B.GROUP_NAME DESC')
					 //->order('A.ORDER_NO ASC')
				);
				
				if($datacard['DEBIT_TYPE'] == '1')
				{
					$datacard['DEBIT_TYPE'] = 'Corporate';
				}
				else
				{
					$datacard['DEBIT_TYPE'] = 'Card Holder';
				}
				
				if($datacard['DEBIT_ATM'] == '1')
				{
					$datacard['DEBIT_ATM'] = 'On';
				}
				else
				{
					$datacard['DEBIT_ATM'] = 'Off';
				}
				
				if($datacard['DEBIT_EDC'] == '1')
				{
					$datacard['DEBIT_EDC'] = 'On';
				}
				else
				{
					$datacard['DEBIT_EDC'] = 'Off';
				}
				
				if(!empty($datacard['USER_FULLNAME']))
				{
					$datacard['DEBIT_NAME'] = $datacard['USER_FULLNAME'];
				}
				else
				{
					$datacard['DEBIT_NAME'] = $datacard['CUST_NAME'];
				}	
				
				if(!empty($datacard['USER_EMAIL']))
				{
					$datacard['DEBIT_EMAIL'] = $datacard['USER_EMAIL'];
				}
				else
				{
					$datacard['DEBIT_EMAIL'] = $datacard['CUST_EMAIL'];
				}
					
				$this->view->datacard = $datacard;
			}




			//var_dump($pslip['PS_CATEGORY']);die;
			if($pslip['PS_CATEGORY'] == 'BULK PAYMENT' || $pslip['PS_TYPE'] == '4'){
				
				unset($this->_tableMstleft[5]);
			}	
			$this->view->tableMstleft 			= $this->_tableMstleft;
			$this->view->tableMstright 			= $this->_tableMstright;



			$this->view->totalTrx 			= $pslip["numtrx"];
			$persenLabel = $pslip["BALANCE_TYPE"] == '2' ? ' %' : '';
			// 			print_r($pslip);die;
			//	if(!empty($persenLabel)){
			//		$this->view->totalAmt 			= $pslip["TRA_AMOUNT"];
			//}else
			if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
				//die;
				$this->view->totalAmt			= $pslip['PS_REMAIN'];
			} else {
				//print_r($pslip);die;
				if ($pslip['PS_TYPE'] == '3' && $pslip['acbenef_ccy'] == 'USD' && $pslip['accsrc_ccy'] == 'USD') {
					$this->view->totalAmt 			= $pslip["ccy"] . ' ' . $pslip["EQUIVALEN_IDR"];
					$this->view->ps_ccy 			= $pslip["ccy"];
				} else if ($pslip['PS_TYPE'] == '3') {
					//$this->view->totalAmt 			= 'IDR '.$pslip["amount"].' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
					$this->view->totalAmt 			= 'IDR ' . Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']);
					$this->view->ps_ccy 			= 'IDR';
				} else {
					$this->view->totalAmt 			= $pslip["ccy"] . ' ' . $pslip["amount"];
					$this->view->ps_ccy 			= $pslip["ccy"];
				}
				/*	if($pslip['ccy']!='IDR'){
					$this->view->totalAmt 			= $pslip['ccy'].' '.$pslip["amount"].' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
				}else{
					$this->view->totalAmt 			= $pslip['ccy'].' '.$pslip["amount"];
				}
			*/
			}
			//print_r($pslip);die;
			if ($pslip['PS_TYPE'] == '3' || ($pslip['PS_TYPE'] == '1')) {
				$this->view->totaltext 			= true;
			} else {
				$this->view->totaltext 			= false;
			}
			$this->view->pdf 				= ($pdf) ? true : false;

			if ($pdf) {

				$Payment = new 	Payment($pslip['PS_NUMBER']);
				$listHistory =  $Payment->getHistory();
				$this->view->paymentHistory =  $listHistory;
				$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;

				$selectUser = $this->_db->select()
				->from(array('A' => 'M_USER'), array('A.USER_ID','A.USER_FULLNAME'))
				->where('A.USER_STATUS != 3')
				->where('A.CUST_ID = ?', $this->_custIdLogin);

				$selectUserList = $selectUser->query()->fetchAll();

				foreach ($selectUserList as $key => $value) {
					$newUserList[$value['USER_ID']] = $value['USER_FULLNAME'];
				}

				$this->view->dataUser = $newUserList;
				$this->view->custName = $this->_custNameLogin; 

				$pstatusDisp = array(
					17 => 'Waiting for Review',
					1 => 'Waiting for Approval',
					2 => 'Waiting for Release'
				);

				$this->view->pdfstatusdisp 	= $pstatusDisp[$pslip['PS_STATUS']];

				$setting = new Settings();
				$master_bank_app_name = $setting->getSetting('master_bank_app_name');
				$master_bank_name = $setting->getSetting('master_bank_name');
				$this->view->master_bank_app_name = $master_bank_app_name;
				$this->view->master_bank_name = $master_bank_name;

				$frontendOptions = array(
					'lifetime' => 86400,
					'automatic_serialization' => true
				);
				$backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
				$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

				$cacheID = 'USERLIST';

				$userlist = $cache->load($cacheID);
				$this->view->pdfUserlist 	= $userlist;

				$tempdir = APPLICATION_PATH . "/../public/QRImages/";

				$teks_qrcode    = "Membuat QR Code dengan PHP";
				$namafile       = $pslip['PS_NUMBER'] . ".png";
				$quality        = "H";
				$ukuran         = 5;
				$padding        = 1;
				$qrImage        = $tempdir . $namafile;
				QRCode::png($teks_qrcode, $tempdir . $namafile, $quality, $ukuran, $padding);

				$outputHTML = "<tr><td>" . $this->view->render('/viewdetail/indexpdf.phtml') . "</td></tr>";
				// echo $outputHTML;die; 
				$this->_helper->download->newPdf(null, null, null, $master_bank_app_name . ' - ' . $master_bank_name , $outputHTML);
			}

			Application_Helper_General::writeLog('DARC', 'Detail View Payment');
		}
	}

	function moneyAliasFormatter($n)
	{
		// first strip any formatting;
		return str_replace('.00', '', Application_Helper_General::displayMoney($n));
		// $n = (0+str_replace(",", "", $n));

		// // is this a number?
		// if (!is_numeric($n)) return false;

		// // now filter it;
		// if ($n > 1000000000000) return round(($n/1000000000000), 2).' T';
		// elseif ($n > 1000000000) return round(($n/1000000000), 2).' B';
		// elseif ($n > 1000000) return round(($n/1000000), 2).' M';
		// elseif ($n > 1000) return $n;

		// return number_format($n);
	}

	public function findPolicyBoundary($transfertype, $amount)
	{
	//var_dump($transfertype);
	if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);
		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}
		 //echo $selectuser;
		$datauser = $this->_db->fetchAll($selectuser);
		//var_dump($datauser);
		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{

		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);	
		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		// echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}
		// $groupuser[] = '';
		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}
			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		// $selectGroupName	= $this->_db->select()
		// 		->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
		// 		->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin. '%');

		// 	$groupNameList = $this->_db->fetchAll($selectGroupName);

		// 	array_unique($groupNameList[0]);

		// 	$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}

		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);

					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	private function debet($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"], $this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
		foreach ($arrTransferStatus as $key => $val) {
			$caseTraStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseTraStatus .= " END)";

		$this->_tableMst[5]["label"] = $this->language->_('Beneficiary Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_bankname"], $pslip["acbenef_alias"]);

		// Table Detail Header
		$fields = array(
			"ACCTSRC_NAME"		=> $this->language->_('Source Account Name'),
			"ACCTSRC"			=> $this->language->_('Source Account'),
			"ACCSRC_ALIAS" => $this->language->_('Source Account Alias'),
			"ACBENEF_ALIAS"			=> $this->language->_('Beneficiary Alias'),
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			"ACCTSRC_CCY"  	   	=> $this->language->_('CCY'),
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			"TRA_STATUS"  	   	=> $this->language->_('Transaction Status'),
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);

		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
					'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
					'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),
					'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
					'ACCTSRC_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																				 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME)
																			END"),
					//'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
					'TRA_AMOUNT'				=> 'TT.TRA_AMOUNT',
					'TRA_ADDMESSAGE'				=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
					'TRA_REFNO'					=> 'TT.TRA_REFNO',
					'TRA_STATUS'				=> $caseTraStatus
				)
			)
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);

		$selectBank	= $this->_db->select()
			->from(
				array('B' => 'M_BANK_TABLE'),
				array(
					'BANK_CODE'			=> 'B.BANK_CODE',
					'BANK_NAME'			=> 'B.BANK_NAME'
				)
			);

		$bankList = $this->_db->fetchAll($selectBank);

		foreach ($bankList as $key => $value) {
			$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		$tableDtl = array();
		if (count($pslipTrx) <= 50) {
			foreach ($pslipTrx as $p => $pTrx) {

				// create table detail data
				$psCategory = $pslip['PS_CATEGORY'];
				foreach ($fields as $key => $field) {
					$value = $pTrx[$key];

					if ($key == "TRA_AMOUNT") {
						$value = Application_Helper_General::displayMoney($value);
					}


					$value = ($value == "") ? "-" : $value;

					$tableDtl[$p][$key] = $value;
				}

				if (!empty($pslip['BANK_CODE'])) {
					$bankcode = $pslip['BANK_CODE'];
				} else {
					$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
				}

				if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
					$bankname = '-';
				} else if (empty($bankcode)) {
					$bankname = $this->_bankName;
				} else {
					$bankname = $bankNameArr[$bankcode];
				}

				$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;
				$tableDtl[$p]['BENEFICIARY_ID'] = $pTrx['ACBENEF_ID'];
			}

			$this->view->fields			 = $fields;
			$this->view->tableDtl 		 = $tableDtl;
		}

		$this->view->TITLE_MST		 = $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 = $this->language->_('Transfer From');


		if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {
			$this->view->TITLE_MST		 = $this->language->_('Sweep To');
			$this->view->TITLE_DTL		 = $this->language->_('Sweep From');
		}
	}

	private function credit($pslip)
	{
		//die('her');
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"], $this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
		foreach ($arrTransferStatus as $key => $val) {
			$caseTraStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseTraStatus .= " END)";
		if(empty($pslip["accsrc_bankname"])){
		$selectcustact = $this->_db->select()->from(array('T' => 'M_CUSTOMER_ACCT'), array('*'))
						->where("T.ACCT_NO =?", $pslip["accsrc"]); 
						//->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						//->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;
					$custact = $this->_db->fetchRow($selectcustact);
		 $pslip["accsrc_bankname"] = $custact['ACCT_NAME'];			
		}
		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		// Table Detail Header
		$fields = array(
			"ACBENEF_NAME"		=> $this->language->_('Beneficiary Account Name'),
			"ACBENEF"			=> $this->language->_('Beneficiary Account'),

			//		"BENEFICIARY_CATEGORY"			=> $this->language->_('Beneficiary Category'),
			//		"BENEFICIARY_ID_TYPE"			=> $this->language->_('Beneficiary Identification Type'),
			//		"BENEFICIARY_ID_NUMBER"			=> $this->language->_('Beneficiary Identification Number'),
			//		"BENEFICIARY_CITY_CODE"			=> $this->language->_('Beneficiary City'),

			"BENEFICIARY_ID_NUMBER"			=> $this->language->_('Beneficiary NRC'),
			"BENEFICIARY_MOBILE_PHONE_NUMBER"			=> $this->language->_('Beneficiary Phone'),
			"ACBENEF_ALIAS"			=> $this->language->_('Beneficiary Alias'),
			"ACCSRC_ALIAS" => $this->language->_('Source Account Alias'),
			/* "TRA_MESSAGE" 		=> $this->language->_('Message'),
						"TRA_REFNO"  	   	=> $this->language->_('Additional Message'),
						"TRANSFER_TYPE"	   	=> $this->language->_('Transfer Type'),
						"PS_CCY"  	   	=> $this->language->_('CCY'),
						"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
						//"TRANSFER_FEE"  	=> 'Transfer Charge',
						"BANK_NAME"  	   	=> $this->language->_('Bank'),
						"TRA_STATUS" 		=> $this->language->_('Transaction Status'),
						*/
			"TRA_MESSAGE" 		=> $this->language->_('Message'),
			"TRA_ADDMESSAGE"  	   	=> $this->language->_('Additional Message'),
			"TRANSFER_TYPE"	   	=> $this->language->_('Transfer Type'),
			"PS_CCY"  	   	=> $this->language->_('CCY'),
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),

			"RATE"  	   	=> $this->language->_('Rate'),
			"TRANSFER_FEE"  	   	=> $this->language->_('Transfer Fee'),
			"FULL_AMOUNT_FEE"  	=> $this->language->_('Full Amount Fee'),
			"PROVISION_FEE"  	=> $this->language->_('Provision Fee'),
			"TOTAL"  	=> $this->language->_('Total'),
			"BANK_NAME"  	   	=> $this->language->_('Beneficiary Bank'),
			"BENEFICIARY_BANK_ADDRESS1"  	   	=> $this->language->_('Beneficiary Bank Address'),
			"BENEFICIARY_BANK_CITY"  	   	=> $this->language->_('Beneficiary Bank City'),
			"BENEFICIARY_BANK_COUNTRY"  	   	=> $this->language->_('Country'),
			"NOSTRO_NAME"  	   	=> $this->language->_('Nostro Bank'),
			"TRA_STATUS" 		=> $this->language->_('Transaction Status'),
			"BENEF_ACCT_BANK_CODE" => $this->language->_('Beneficiary Bank Name')
		);
		//print_r($pslip);die;
		if ($pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME)"),
						'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
						'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'C.PS_CCY', 'C.EQUIVALENT_AMOUNT_IDR', 'C.PS_TYPE',
						'BENEFICIARY_CATEGORY'				=> 'TT.BENEFICIARY_CATEGORY',
						'BENEFICIARY_ID_TYPE'				=> 'TT.BENEFICIARY_ID_TYPE',
						'BENEFICIARY_ID_NUMBER'				=> 'TT.BENEFICIARY_ID_NUMBER',
						'BENEFICIARY_CITY_CODE'				=> 'TT.BENEFICIARY_CITY_CODE',
						'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',

						'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
						'TRA_REFNO'				=> 'TT.TRA_REFNO',

						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House(Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House(Buy)'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT', 'TT.SOURCE_ACCOUNT_CCY',
						'C.CUST_ID',
						#'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $this->_bankName . "'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'TRA_STATUS' 			=> $caseTraStatus
					)
				)
				->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
				->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
				//->joinLeft	(   array('D' => 'T_PERIODIC_DETAIL'), 'C.PS_PERIODIC = D.PS_PERIODIC', array() )
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		} else {

			$app = Zend_Registry::get('config');
			$appBankname = $app['app']['bankname'];

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
						'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME)"),
						'ACBENEF_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ALIAS_NAME)"),
						'ACCSRC_ALIAS'			=> new Zend_Db_Expr("
																			CONCAT(TT.SOURCE_ACCOUNT_ALIAS_NAME)"),

						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'C.PS_TXCOUNT',
						'C.PS_CCY', 'C.PS_TYPE',
						'C.CUST_ID',
						'TT.EQUIVALENT_AMOUNT_IDR',
						'BENEFICIARY_CATEGORY'				=> 'TT.BENEFICIARY_CATEGORY',
						'BENEFICIARY_ID_TYPE'				=> 'TT.BENEFICIARY_ID_TYPE',
						'BENEFICIARY_ID_NUMBER'				=> 'TT.BENEFICIARY_ID_NUMBER',
						'BENEFICIARY_CITY_CODE'				=> 'TT.BENEFICIARY_CITY_CODE',
						'TT.BENEFICIARY_MOBILE_PHONE_NUMBER',
						'TT.RATE', 'TT.SOURCE_ACCOUNT_CCY',
						'TT.PROVISION_FEE',
						'TT.NOSTRO_NAME',
						'TT.BENEFICIARY_BANK_ADDRESS1',
						'TT.BENEFICIARY_BANK_CITY',
						'TT.FULL_AMOUNT_FEE',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

						'TRA_REFNO'				=> 'TT.TRA_REFNO',
						'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
						'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House(Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House(Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE 'N/A'
																			END"),
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
						'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
						#'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'TT.SP2D_NO',
						'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'TRA_STATUS' 			=> $caseTraStatus
					)
				)
				->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
				->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);
		}
		//echo $select;die;
		$pslipTrx = $this->_db->fetchAll($select);
		// print_r($pslipTrx);die;
		$tableDtl = array();
		// if(count($pslipTrx) <= 50){
		$selectBank	= $this->_db->select()
			->from(
				array('B' => 'M_BANK_TABLE'),
				array(
					'BANK_CODE'			=> 'B.BANK_CODE',
					'BANK_NAME'			=> 'B.BANK_NAME'
				)
			);

		$bankList = $this->_db->fetchAll($selectBank);

		foreach ($bankList as $key => $value) {
			$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		foreach ($pslipTrx as $p => $pTrx) {

			if ($pTrx['PS_TYPE'] == 19 && $pTrx['PS_TXCOUNT'] > 1 || $pTrx['PS_TYPE'] == 11) {
				$this->_tableMstleft[5]["value"] = '';
			}else if($pTrx['PS_CATEGORY'] == 'BULK PAYMENT'){
				$this->_tableMstleft[5]["value"] = '';
			}else if($pTrx['PS_TYPE'] == 29){
				$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadfile', 'payReff' => $pslip['PS_NUMBER']), null, true);
				$this->_tableMstleft[5]["value"] = 'SP2D no: '.$pTrx['SP2D_NO'].'<a href="'.$downloadURL.'" ><input type="button" class="btnwhite" value="Download" > </a>';
				//$pTrx['TRA_MESSAGE'];
				//$this->view->message = 'SP2D no: '.$pslipTrx['0']['SP2D_NO'].'<input type="button" ';
			}else{
				
				//$this->_tableMstleft[5]["value"] = $pTrx['TRA_MESSAGE'];
				if($pTrx['PS_TYPE'] == '38'){
					$this->_tableMstleft[5]["value"] = $pTrx['TRA_MESSAGE'].'&nbsp;&nbsp;<a href="/eform/bgdetail/index/bgnumb/'.$pTrx['TRA_MESSAGE'].'"><button class="btnwhite hov">Detail</button></a>';
				}else{
					$this->_tableMstleft[5]["value"] = $pTrx['TRA_MESSAGE'];	
				}
			}
			
			$this->view->pdftramessage = $pTrx['TRA_MESSAGE'];

			if ($pslip['PS_TYPE'] == '17' && $pslip['PS_BILLER_ID'] == '1156') {
				$this->_tableMstleft[5]["value"] = '';
			}
			
		if($pslip['PS_TYPE'] == '4'){
			$this->_tableMstleft[5]["value"] = '';
		}
			// $this->_tableMstleft[5]["value"] = $pTrx['TRA_ADDMESSAGE']; 

			// if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {
			// 	$this->_tableMstright[6]["value"] = $pTrx['TRA_STATUS'];
			// }
			// else{
			// 	$this->_tableMstright[3]["value"] = $pTrx['TRA_STATUS'];
			// }

			// $this->_tableMstleft[4]["value"] = $pTrx['TRA_ADDMESSAGE'];
			$psCategory = $pslip['PS_CATEGORY'];
			//print_r($fields);die;
			// create table detail data

			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];
				// print_r($key);die;
				if ($key == 'ACBENEF' && (trim($pTrx['TRANSFER_TYPE']) == 'FA' || trim($pTrx['TRANSFER_TYPE']) == 'No FA')) {
					$value = '-';
					// die;
				}
				if ($key == "BENEFICIARY_CATEGORY") {
					$LLD_CATEGORY = $value;
					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}
					$value = $LLD_CATEGORY_POST;
				}
				if ($key == "BENEFICIARY_CITY_CODE") {
					$CITY_CODE = $value;
					$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
					$select = $this->_db->select()
						->from(array('A' => 'M_CITY'), array('*'));
					$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
					$arr = $this->_db->fetchall($select);
					$value = $arr[0]['CITY_NAME'];
				}

				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['PS_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15' || $pslip['PS_TYPE'] == '19' || $pslip['PS_TYPE'] == '20') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						//print_r($pTrx);die;
						if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD' && $pslip['PS_TYPE'] == '1') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							//$value = $pTrx['PS_CCY'].' '.Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']).' (IDR '.Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']).')';
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']);
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}

				if ($key == 'RATE') {
					$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
				}
				//print_r($pTrx);die;
				if ($key == "FULL_AMOUNT_FEE") {
					if ($pTrx['TRANSFER_TYPE'] == 'FA') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
					} elseif ($pTrx['TRANSFER_TYPE'] == 'No FA') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '4')
							->where("T.CUST_ID =?", $pTrx['CUST_ID'])
							// ->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
						;
					}



					// echo $selecttrfFA;
					// print_r($pTrx);die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);

					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);

					//print_r($value);die;
				}


				if ($key == 'PROVISION_FEE') {
					//print_r($pTrx);die;
					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrfpro;die;
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['ACBENEF_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != 'N/A') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}
				
				if(empty($pTrx['SOURCE_ACCOUNT_CCY'])){
					$pTrx['SOURCE_ACCOUNT_CCY']='IDR';
				}
				
				if ($key == "TRANSFER_FEE") {
					//print_r($value);die;
					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;

					$trffee = $this->_db->fetchRow($selecttrffee);
					if ($pTrx['TRANSFER_FEE'] != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRANSFER_FEE']);
					} else {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' 0.00';
					}
				}
 
				if ($key == "BENEF_ACCT_BANK_CODE") {
					if (!empty($pslip['BANK_CODE'])) {
						$bankcode = $pslip['BANK_CODE'];
					}else if (!empty($pslip['BENEF_ACCT_BANK_CODE'])) {
						$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
					} else {
						$bankcode = $pslip['CLR_CODE'];
					}

					if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
						$bankname = '-';
					}else if (!empty($pslip['BENEFICIARY_BANK_NAME'])) {
						$bankname = $pslip['BENEFICIARY_BANK_NAME'];
					} else if (empty($bankcode)) {
						$bankname = $this->_bankName;
					} else {
						$bankname = $bankNameArr[$bankcode];
					}

					$value = $bankname;
				}

				//print_r($value);die;
				$value = ($value == "") ? "-" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
				$tableDtl[$p]['BENEFICIARY_ID'] = $pTrx['ACBENEF_ID'];
			}
		}

		if($pTrx['PS_TYPE'] == 11){
			unset($this->_tableMstleft[5]);
		}
		// }
		// echo "<pre>";
		// print_r($fields);
		 //print_r($tableDtl);die;
		if($pslipTrx['0']['PS_TYPE'] == '27' || $pslipTrx['0']['PS_TYPE'] == '28'){
		$selectclient = $this->_db->select()
                    ->from(array('B' => 'T_DIRECTDEBIT'),array('CLIENT_REFF', 'CLIENT_NAME'))
                    ->where("B.DEBIT_ACCT = ?", $tableDtl[0]['ACBENEF'])
                    ->query()->fetchAll();

			
			$this->view->clientReff			= $selectclient[0]['CLIENT_REFF'];
			$this->view->clientName			= $selectclient[0]['CLIENT_NAME'];
			$this->view->cust_approver      = 0;
		}
			$this->view->fields			 = $fields;
			$this->view->tableDtl 		 = $tableDtl;
			
		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;

		if (count($tableDtl) >= 50) {
			$this->view->countTable = true;
		} else {
			$this->view->countTable = false;
		}
		$this->view->TITLE_MST		 	= $this->language->_('Transfer To');
		$this->view->TITLE_DTL		 	= $this->language->_('Transfer From');

		if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {
			$this->view->TITLE_MST		 = $this->language->_('Sweep To');
			$this->view->TITLE_DTL		 = $this->language->_('Sweep From');
		}
	}

	private function etax($pslip)
	{
		// echo '<pre>';
		// var_dump($pslip);die;
		$PS_NUMBER = $this->_paymentRef;
		$dataEtax = json_decode($pslip['LOG']);
		$this->view->dataEtax = $dataEtax;


		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		if ($pslip['PS_BILLER_ID'] == '1158') {
			$this->_tableMst[4]["value"] = $dataEtax->paymentSubject;
			//table detail
			$compulsory = array('0' => 'No', '1' => 'Yes');
			$taxType = array('0' => 'NPWP', '1' => 'Non NPWP');
			$identity = array(
				'1' => 'KTP',
				'2' => 'NPWP',
				'3' => 'SIM',
				'4' => 'PASPOR',
				'5' => 'KITAS'
			);
			// roki
			$month = array(
				$this->language->_('January'),
				$this->language->_('February'),
				$this->language->_('March'),
				$this->language->_('April'),
				$this->language->_('May'),
				$this->language->_('June'),
				$this->language->_('July'),
				$this->language->_('August'),
				$this->language->_('September'),
				$this->language->_('October'),
				$this->language->_('November'),
				$this->language->_('December')
			);
			$map_code = array();
			$depositType = array();
			$select = $this->_db->select()
				->from(array('M_MAP_CODE'), array('MAP_CODE', 'MAP_NAME'))
				->query()->fetchAll();

			foreach ($select as $key) {
				$map_code[$key['MAP_CODE']] = $key['MAP_NAME'] . " (" . $key['MAP_CODE'] . ")";
			}
			if (isset($dataEtax->akuncode)) {
				$select = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_MAP_CODE'), array('DEPOSIT_CODE', 'DEPOSIT_NAME'))
						->where('MAP_CODE = ?', $dataEtax->akuncode)
				);

				foreach ($select as $key) {
					$depositType[$key['DEPOSIT_CODE']] = $key['DEPOSIT_NAME'] . " (" . $key['DEPOSIT_CODE'] . ")";
				}
			}

			$tableDetail[0]["label"] = $this->language->_('Billing ID').' / '.$this->language->_('Code');
			 $tableDetail[0]["value"] = '2020032300014203';

			 $tableDetail[1]["label"] = $this->language->_('MAP Code');
			 $tableDetail[1]["value"] = $map_code[$dataEtax->akuncode];
			 
			 $tableDetail[2]["label"] = $this->language->_('KJS');
			 $tableDetail[2]["value"] = $depositType[$dataEtax->deposittype];
			 
			 $tableDetail[3]["label"] = $this->language->_('NPWP Number');
			 $tableDetail[3]["value"] = $dataEtax->asessableNpwp;

			 $tableDetail[4]["label"] = $this->language->_("Taxpayer's Name");
			 $tableDetail[4]["value"] = $dataEtax->asessablename;

			 $tableDetail[5]["label"] = $this->language->_("Taxpayer's Address");
			 $tableDetail[5]["value"] = $dataEtax->asessableaddress;

			 $tableDetail[6]["label"] = $this->language->_("Taxpayer's City");
			 $tableDetail[6]["value"] = $dataEtax->asessablecity;
			
			 $tableDetail[7]["label"] = $this->language->_('SK Number');
			 $tableDetail[7]["value"] = $dataEtax->skNumber;
			 
			 $tableDetail[8]["label"] = '';
			 $tableDetail[8]["value"] = $this->language->_('Format : Serial No / SKP Type / Tax Year / KPP Code / Release Year');
			 
			 
 			 $tableDetail[9]["label"] = $this->language->_('Tax Object Number (NOP)');
			 $tableDetail[9]["value"] = $dataEtax->taxobjectnumber;
			 
			 $tableDetail[10]["label"] = $this->language->_("Taxayer's ID eg(KTP,SIM) ");
			 $tableDetail[10]["value"] = $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;

			$tableDetail[11]["label"] = $this->language->_("Depositor's NPWP Number");
			 $tableDetail[11]["value"] = $dataEtax->payerNpwp;
			 
			 $tableDetail[12]["label"] = $this->language->_("Depositor's Name");
			 $tableDetail[12]["value"] = $dataEtax->payername;
			 
			 $tableDetail[13]["label"] = '';
			 $tableDetail[13]["value"] = '';

			 $tableDetail[14]["label"] = $this->language->_("Tax Method Period");
			 $tableDetail[14]["value"] = 'bold';
			 
			$tableDetail[15]["label"] = $this->language->_("Month");
			 $tableDetail[15]["value"] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2];
			 
			 $tableDetail[16]["label"] = $this->language->_("Year");
			 $tableDetail[16]["value"] = $dataEtax->periodic;


			// $tableDetail[0]["label"] = $this->language->_('Billing Code');
			// $tableDetail[0]["value"] = '2020032300014203';

			// $tableDetail[1]["label"] = $this->language->_('Amount');
			// $tableDetail[1]["value"] = 'IDR ' . $dataEtax->amount;

			// $tableDetail[2]["label"] = $this->language->_('Payer NPWP');
			// $tableDetail[2]["value"] = $dataEtax->payerNpwp;

			// $tableDetail[3]["label"] = $this->language->_('Payer Name');
			// $tableDetail[3]["value"] = $dataEtax->payername;

			// $tableDetail[4]["label"] = $this->language->_('Compulsory');
			// $tableDetail[4]["value"] = $compulsory[$dataEtax->chargetype] . ' ' . ($dataEtax->chargetype == '1' ? ' - (' . $dataEtax->chargeid . ')' : '');

			// $tableDetail[5]["label"] = $this->language->_('Asessable NPWP');
			// $tableDetail[5]["value"] = $dataEtax->asessableNpwp;

			// $tableDetail[6]["label"] = $this->language->_('Asessable Name');
			// $tableDetail[6]["value"] = $dataEtax->asessablename;

			// $tableDetail[7]["label"] = $this->language->_('Asessable Address');
			// $tableDetail[7]["value"] = $dataEtax->asessableaddress;

			// $tableDetail[8]["label"] = $this->language->_('Asessable City');
			// $tableDetail[8]["value"] = $dataEtax->asessablecity;

			// $tableDetail[9]["label"] = $this->language->_('Asessable Identity');
			// $tableDetail[9]["value"] = $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;

			// $tableDetail[10]["label"] = $this->language->_('Map / Akun Code');
			// $tableDetail[10]["value"] = $map_code[$dataEtax->akuncode];

			// $tableDetail[11]["label"] = $this->language->_('Deposit Type');
			// $tableDetail[11]["value"] = $depositType[$dataEtax->deposittype];

			// $tableDetail[12]["label"] = $this->language->_('Tax Object Number (NOP)');
			// $tableDetail[12]["value"] = $dataEtax->taxobjectnumber;

			// $tableDetail[13]["label"] = $this->language->_('SK Number');
			// $tableDetail[13]["value"] = $dataEtax->skNumber;

			// $tableDetail[14]["label"] = $this->language->_('Remark');
			// $tableDetail[14]["value"] = $dataEtax->remark;

			// $tableDetail[15]["label"] = $this->language->_('Tax Period  Payment');
			// $tableDetail[15]["value"] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2] . ' ' . $dataEtax->periodic;
		} elseif ($pslip['PS_BILLER_ID'] == '1156') {
			// unset($this->_tableMst[4]); // remove array key payment subject

			// $tableDetail[0]["label"] = $this->language->_('Billing Code');
			// $tableDetail[0]["value"] = $dataEtax->orderId;

			// $tableDetail[1]["label"] = $this->language->_('Amount');
			// $tableDetail[1]["value"] = 'IDR ' . Application_Helper_General::displayMoney($dataEtax->amount);

			// $billingPrefix = substr($dataEtax->orderId, 0, 1);

			// if ($billingPrefix == '0' || $billingPrefix == '1' || $billingPrefix == '2' || $billingPrefix == '3') {

			// 	$tableDetail[2]["label"] = $this->language->_('NPWP');
			// 	$tableDetail[2]["value"] = $dataEtax->dataUi->npwp;

			// 	$tableDetail[3]["label"] = $this->language->_('Customer Name');
			// 	$tableDetail[3]["value"] = $dataEtax->dataUi->customer_name;

			// 	$tableDetail[4]["label"] = $this->language->_('Customer Address');
			// 	$tableDetail[4]["value"] = $dataEtax->dataUi->customer_address;

			// 	$tableDetail[5]["label"] = $this->language->_('Map / Akun Code');
			// 	$tableDetail[5]["value"] = $dataEtax->dataUi->account_map;

			// 	$tableDetail[6]["label"] = $this->language->_('Deposit Type');
			// 	$tableDetail[6]["value"] = $dataEtax->dataUi->type;

			// 	$tableDetail[7]["label"] = $this->language->_('Tax Object Number (NOP)');
			// 	$tableDetail[7]["value"] = $dataEtax->dataUi->NOP;

			// 	$tableDetail[8]["label"] = $this->language->_('SK Number');
			// 	$tableDetail[8]["value"] = $dataEtax->dataUi->sk_number;

			// 	$tableDetail[9]["label"] = $this->language->_('Tax Period Payment');
			// 	$tableDetail[9]["value"] = $dataEtax->dataUi->period;
			// } elseif ($billingPrefix == '4' || $billingPrefix == '5' || $billingPrefix == '6') {

			// 	$tableDetail[2]["label"] = $this->language->_('Customer Name');
			// 	$tableDetail[2]["value"] = $dataEtax->dataUi->customer_name;

			// 	$tableDetail[3]["label"] = $this->language->_('ID Type Customer');
			// 	$tableDetail[3]["value"] = $dataEtax->dataUi->customer_id;

			// 	$tableDetail[4]["label"] = $this->language->_('Document Type');
			// 	$tableDetail[4]["value"] = $dataEtax->dataUi->document_type;

			// 	$tableDetail[5]["label"] = $this->language->_('Document Number');
			// 	$tableDetail[5]["value"] = $dataEtax->dataUi->document_number;

			// 	$tableDetail[6]["label"] = $this->language->_('Document Date');
			// 	$tableDetail[6]["value"] = $dataEtax->dataUi->document_date;

			// 	$tableDetail[7]["label"] = $this->language->_('KPBC Code');
			// 	$tableDetail[7]["value"] = $dataEtax->dataUi->kppbc_code;
			// } elseif ($billingPrefix == '7' || $billingPrefix == '8' || $billingPrefix == '9') {

			// 	$tableDetail[2]["label"] = $this->language->_('Customer Name');
			// 	$tableDetail[2]["value"] = $dataEtax->dataUi->customer_name;

			// 	$tableDetail[3]["label"] = $this->language->_('K/L');
			// 	$tableDetail[3]["value"] = $dataEtax->dataUi->k_l;

			// 	$tableDetail[4]["label"] = $this->language->_('Echelon Unit 1');
			// 	$tableDetail[4]["value"] = $dataEtax->dataUi->eselon_unit;

			// 	$tableDetail[5]["label"] = $this->language->_('Code Unit');
			// 	$tableDetail[5]["value"] = $dataEtax->dataUi->work_unit;
			// }
			// //adons Type of Tax First row
			// $typeOfTax = $this->_db->fetchRow(
			// 	$this->_db->select()
			// 		->from(array('M_SERVICE_PROVIDER'), array('PROVIDER_NAME'))
			// 		->where('PROVIDER_ID = ?', $pslip['PS_BILLER_ID'])
			// );
			// $tableTypeofTax[0]["label"] = $this->language->_('Type of Tax');
			// $tableTypeofTax[0]["value"] = $typeOfTax['PROVIDER_NAME'];

			// $tableDetail = array_merge($tableTypeofTax, $tableDetail);
		}
		 $this->view->tableDetail = $tableDetail;
		/////
		$fields = array(
			"operatorNama" 	=> $this->language->_('Type Of Tax'),
			"orderId" 		=> $this->language->_('Billing Code'),
			"TRA_AMOUNT"	=> $this->language->_('Amount'),						
		);

		if ($dataEtax->taxType == 'DJP'){
			$fields['npwp']				= $this->language->_('NPWP');
			$fields['customer_name']	= $this->language->_('Customer Name');
			$fields['customer_address']	= $this->language->_('Customer Address');
			$fields['account_map']		= $this->language->_('Map / Akun Code');
			$fields['type']				= $this->language->_('Deposit Type');
			$fields['nop']				= $this->language->_('Tax Object Number (NOP)');
			$fields['sk_number']		= $this->language->_('SK Number');
			$fields['period']			= $this->language->_('Tax Period Payment');
		}elseif ($dataEtax->taxType == 'DJBC'){
			$fields['customer_name']	= $this->language->_('Customer Name');
			$fields['customer_id']		= $this->language->_('ID Type Customer');
			$fields['document_type']	= $this->language->_('Document Type');
			$fields['document_number']	= $this->language->_('Document Number');
			$fields['document_date']	= $this->language->_('Document Date');
			$fields['kppbc_code']		= $this->language->_('KPBC Code');
		}elseif ($dataEtax->taxType == 'DJA'){
			$fields['customer_name']	= $this->language->_('Customer Name');
			$fields['k_l']				= $this->language->_('K/L');
			$fields['eselon_unit']		= $this->language->_('Echelon Unit 1');
			$fields['work_unit']		= $this->language->_('Code Unit');			
		}

		// Table Detail Header
		// $fields = array(
		// 	"orderId" 		=> $this->language->_('Billing Code'),
		// 	"TRA_AMOUNT"		=> $this->language->_('Amount'),
		// 	"payerNpwp"		=> $this->language->_('Payer NPWP'),
		// 	"payername"		=> $this->language->_('Payer Name'),
		// 	"compulsory"		=> $this->language->_('Compulsory'),
		// 	"asessableNpwp"		=> $this->language->_('Asessable NPWP'),
		// 	"asessablename"			=> $this->language->_('Asessable Name'),
		// 	"asessableaddress"		=> $this->language->_('Asessable Address'),			
		// 	"asessablecity" 	=> $this->language->_('Asessable City'),			 
		// 	"asessableidentity"	=> $this->language->_('Asessable Identity'),
		// 	"map_code"			=> $this->language->_('Map / Akun Code'),
		// 	"depositType"		=> $this->language->_('Deposit Type'),						
		// 	"taxobjectnumber"	=> $this->language->_('Tax Object Number (NOP'),			
		// 	"skNumber"			=> $this->language->_('SK Number'),			
		// 	"remark"			=> $this->language->_('Remark'),			
		// 	"tax_period_payment"	=> $this->language->_('Tax Period  Payment'),			
		// );		
		
		$fieldsdetail = array(
			"TRA_AMOUNT"  	   	=> $this->language->_('Amount'),
			"TOTAL_CHARGES"  	=> $this->language->_('Fee'),
			"PS_TOTAL_AMOUNT"	=> $this->language->_('Total Amount')
	   );

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];
		
		$ACCTSRC_arr = array();
		$ACCTSRC_arr[$pslip["accsrc"]]["CCY"] 		= $pslip["accsrc_ccy"];
		$ACCTSRC_arr[$pslip["accsrc"]]["AMOUNT"] 	= $pslip["amount"];

		$tableDtl = array();

		$app = Zend_Registry::get('config');
		$appBankname = $app['app']['bankname'];

		$selectTrx	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
					'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
					'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
					'TRA_REFNO'				=> 'TT.TRA_REFNO',
					'BENEFICIARY_BANK_CITY',
					'BENEFICIARY_ALIAS_NAME',
					'BENEFICIARY_ADDRESS',
					'BENEFICIARY_ADDRESS2',
					'BENEFICIARY_ID_TYPE',
					'BENEFICIARY_ID_NUMBER',
					'BENEFICIARY_CITIZENSHIP' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CITIZENSHIP = 'W' THEN 'WNI'
																				 WHEN TT.BENEFICIARY_CITIZENSHIP = 'R' THEN 'WNA'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_RESIDENT' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_RESIDENT = 'R' THEN 'Residence'
																				 WHEN TT.BENEFICIARY_RESIDENT = 'W' THEN 'Non REsidence'
																				 ELSE '-'
																			END"),
					'BENEFICIARY_CATEGORY' => new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CATEGORY = '1' THEN 'Individual'
																				 WHEN TT.BENEFICIARY_CATEGORY = '2' THEN 'Government'
																				 WHEN TT.BENEFICIARY_CATEGORY = '3' THEN 'Bank'
																				 WHEN TT.BENEFICIARY_CATEGORY = '4' THEN 'Non Bank Financial Institution'
																				 WHEN TT.BENEFICIARY_CATEGORY = '5' THEN 'Company'
																				 WHEN TT.BENEFICIARY_CATEGORY = '6' THEN 'Other'
																				 ELSE '-'
																			END"),

					'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
					'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'In House (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'In House (Buy)'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE '-'
																			END"),
					'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
					'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					'TOTAL_CHARGES'			=> 'TT.TOTAL_CHARGES',
					'TT.RATE',
					'TT.PROVISION_FEE',
					'TT.NOSTRO_NAME',
					'TT.FULL_AMOUNT_FEE',
					'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',					
					'LLD_TRANSACTION_PURPOSE'	=> 'TT.LLD_TRANSACTION_PURPOSE',
					'LLD_DESC'				=> 'TT.LLD_DESC',
					'C.CUST_ID', 'TT.SOURCE_ACCOUNT_CCY',
					'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
					'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
					'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																			 WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																			 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
					'ACBENEF_ISAPPROVE'		=> new Zend_Db_Expr("
																			CASE WHEN C.PS_STATUS = '1' THEN 'Waiting Approval'
																				 WHEN C.PS_STATUS = '2' THEN 'Approved'
																				 ELSE '-'
																			END"),
				)
			)
			->joinLeft(array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
			->joinLeft(array('C' => 'T_PSLIP'), 'TT.PS_NUMBER = C.PS_NUMBER', array())
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo $selectTrx;die;
		$pslipTrx = $this->_db->fetchAll($selectTrx);
		
		// $this->view->message = $pslipTrx['0']['TRA_MESSAGE'];
		// $this->view->addmessage = $pslipTrx['0']['TRA_ADDMESSAGE'];
		// echo "<pre>";
		// 	print_r($pslipTrx);die;

		$frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BANKTABLE';
        
        $bankNameArr = $cache->load($cacheID);
		//var_dump($select_int);
        if(empty($bankNameArr)){
	        	$selectbank = $this->_db->select()
				->from(array('C' => 'M_BANK_TABLE'), array('*'));

			$databank = $this->_db->fetchAll($selectbank);

			foreach ($databank as $key => $value) {
				$bankNameArr[$value['BANK_CODE']] = $value['BANK_NAME'];
			}
			
			$cache->save($bankNameArr,$cacheID);
        }


		foreach ($pslipTrx as $p => $pTrx) {
			// Create array bene for validation
			if (!empty($pTrx["ACBENEF_ID"])) {
				$ACBENEF_IDarr[$pTrx["ACBENEF_ID"]]["CCY"] = $pTrx["ACBENEF_CCY"];
			}

			$trfType = $pTrx["TRANSFER_TYPE"];

			$psCategory = $pslip['PS_CATEGORY'];

			// create table detail data
			foreach ($fields as $key => $field) {
				$value = $pTrx[$key];
				
				if ($key == 'ACBENEF' && ($pTrx['TRANSFER_TYPE'] == '10' || $pTrx['TRANSFER_TYPE'] == '9')) {
					$value = '';
				}
				if ($key == "TRA_AMOUNT" || $key == "TRA_REMAIN" || $key == "PROVISION_FEE") {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {

						if ($pTrx['PS_CCY'] != 'IDR') {
							$value = Application_Helper_General::displayMoney($value);
						} else {

							$value = Application_Helper_General::displayMoney($value);
						}
					}
				}

				if ($key == "TRANSFER_FEE" && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {

					$selecttrffee = $this->_db->select()->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '3')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_ACCOUNT_CCY']);
					//echo $selecttrffee;
					$trffee = $this->_db->fetchRow($selecttrffee);

					if ($value != '') {
						$value = $trffee['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					}
				}
				if ($key == "FULL_AMOUNT_FEE" && !empty($pTrx['ACBENEF_CCY'])) {

					$selecttrfFA = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '4')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['ACBENEF_CCY']);
					// echo $selecttrfFA;die;
					$trfFA = $this->_db->fetchRow($selecttrfFA);
					if ($pTrx['TRANSFER_TYPE'] == '10') {
						$pTrx['FULL_AMOUNT_FEE'] = 0;
					} else if ($pTrx['TRANSFER_TYPE'] == '9') {
						$selecttrfFA = $this->_db->select()
							->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
							->where("T.CHARGE_TYPE =?", '6')
							->where("T.CUST_ID =?", $pTrx['CUST_ID']);
						// echo $selecttrfFA;die;
						$trfFA = $this->_db->fetchRow($selecttrfFA);
					}
					$value = $trfFA['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['FULL_AMOUNT_FEE']);
				}

				if ($key == 'PROVISION_FEE' && !empty($pTrx['SOURCE_ACCOUNT_CCY'])) {
					$selecttrfpro = $this->_db->select()
						->from(array('T' => 'M_CHARGES_REMITTANCE'), array('*'))
						->where("T.CHARGE_TYPE =?", '5')
						->where("T.CUST_ID =?", $pTrx['CUST_ID'])
						->where("T.CHARGE_CCY =?", $pTrx['SOURCE_ACCOUNT_CCY']);
					$trfpro = $this->_db->fetchRow($selecttrfpro);
					$value = $trfpro['CHARGE_AMOUNT_CCY'] . ' ' . Application_Helper_General::displayMoney($value);
					//print_r($value);die;
				}

				if ($key == 'TOTAL') {
					if ($pslip['PS_TYPE'] == '14' || $pslip['PS_TYPE'] == '15') {
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
					} else {
						if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD' && $pTrx['TRANSFER_TYPE_disp'] == 'PB') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						} else if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
							$value = 'USD ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else if ($pTrx['PS_CCY'] == 'USD') {
							$value = $pTrx['PS_CCY'] . ' ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']) . ' (IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']) . ')';
						} else if ($pTrx['EQUIVALEN_IDR'] != '0.00') {
							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['EQUIVALEN_IDR']);
						} else {

							$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
						}
					}
				}
				//print_r($pTrx);die;
				if ($key == 'RATE') {
					if ($pTrx['ACBENEF_CCY'] == 'USD' && $pTrx['SOURCE_ACCOUNT_CCY'] == 'USD') {
						$value = 'IDR 0.00';
					} else if ($pTrx['RATE'] != '-') {
						$value = 'IDR ' . Application_Helper_General::displayMoney($pTrx['RATE']);
					} else {
						$value = 'IDR 0.00';
					}
				}

				if ($key == "TRANSFER_TYPE_disp") {
					if ($value == 'PB') {
						$value = 'In House';
					}
				}

				if ($key == "BENEFICIARY_ID_NUMBER") {
					$value = $pTrx['BENEFICIARY_ID_TYPE'].' '.$pTrx['BENEFICIARY_ID_NUMBER'];
				}

				if ($key == "SP2D_DATE" || $key == "SPM_DATE") {
					$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);
				}

				if ($key == "LLD_DESC") {
					$value = nl2br($value);
				}

				$value = ($value == "") ? "-" : $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if (!empty($pslip['BANK_CODE'])) {
				$bankcode = $pslip['BANK_CODE'];
			} else {
				$bankcode = $pslip['BENEF_ACCT_BANK_CODE'];
			}

			if ($psCategory == 'Payment' || $psCategory == 'Purchase') {
				$bankname = '-';
			} else if (empty($bankcode)) {
				$bankname = $this->_bankName;
			} else {
				$bankname = $bankNameArr[$bankcode];
			}

			$tableDtl[$p]['BENEF_ACCT_BANK_CODE'] = $bankname;

			$tableDtl[$p]['operatorNama']		= $dataEtax->operatorNama;
			$tableDtl[$p]['orderId']			= $dataEtax->orderId;

			//DJP
			$tableDtl[$p]['npwp']				= $dataEtax->dataUi->npwp;
			$tableDtl[$p]['customer_name']		= $dataEtax->dataUi->customer_name;
			$tableDtl[$p]['customer_address']	= $dataEtax->dataUi->customer_address;			
			$tableDtl[$p]['account_map']		= $dataEtax->dataUi->account_map;
			$tableDtl[$p]['type']				= $dataEtax->dataUi->type;
			$tableDtl[$p]['nop']				= $dataEtax->dataUi->NOP;
			$tableDtl[$p]['sk_number']			= $dataEtax->dataUi->sk_number;
			$tableDtl[$p]['period']				= $dataEtax->dataUi->period;
			
			//DJBC
			$tableDtl[$p]['customer_name']		= $dataEtax->dataUi->customer_name;
			$tableDtl[$p]['customer_id']		= $dataEtax->dataUi->customer_id;
			$tableDtl[$p]['document_type']		= $dataEtax->dataUi->document_type;
			$tableDtl[$p]['document_number']	= $dataEtax->dataUi->document_number;
			$tableDtl[$p]['document_date']		= $dataEtax->dataUi->document_date;
			$tableDtl[$p]['kppbc_code']			= $dataEtax->dataUi->kppbc_code;

			//DJA
			$tableDtl[$p]['customer_name']		= $dataEtax->dataUi->customer_name;
			$tableDtl[$p]['k_l']				= $dataEtax->dataUi->k_l;
			$tableDtl[$p]['eselon_unit']		= $dataEtax->dataUi->eselon_unit;
			$tableDtl[$p]['work_unit']			= $dataEtax->dataUi->work_unit;

			// $tableDtl[$p]['payerNpwp']			= $dataEtax->payerNpwp;
			// $tableDtl[$p]['payername']			= $dataEtax->payername;			
			// $tableDtl[$p]['asessableNpwp']		= $dataEtax->asessableNpwp;
			// $tableDtl[$p]['asessablename']		= $dataEtax->asessablename;
			// $tableDtl[$p]['asessableaddress']	= $dataEtax->asessableaddress;
			// $tableDtl[$p]['asessablecity']		= $dataEtax->asessablecity;
			// $tableDtl[$p]['taxobjectnumber']		= $dataEtax->taxobjectnumber;
			// $tableDtl[$p]['skNumber']			= $dataEtax->skNumber;
			// $tableDtl[$p]['remark']				= $dataEtax->remark;
			// $tableDtl[$p]['compulsory']			= $compulsory[$dataEtax->chargetype] . ' ' . ($dataEtax->chargetype == '1' ? ' - (' . $dataEtax->chargeid . ')' : '');
			// $tableDtl[$p]['asessableidentity']	= $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;
			// $tableDtl[$p]['map_code'] 			= $map_code[$dataEtax->akuncode];
			// $tableDtl[$p]['depositType'] 		= $depositType[$dataEtax->deposittype];
			// $tableDtl[$p]['tax_period_payment'] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2] . ' ' . $dataEtax->periodic;
			
			$tableDtl[$p]['TRA_AMOUNT']  	= Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);
			$tableDtl[$p]['TOTAL_CHARGES'] 	=  Application_Helper_General::displayMoney($pTrx['TOTAL_CHARGES']);
			$totalAmount = $pTrx['TRA_AMOUNT'] - $pTrx['TOTAL_CHARGES'];
			$tableDtl[$p]['PS_TOTAL_AMOUNT'] 	= Application_Helper_General::displayMoney($totalAmount);			
		}

		// Start - check if user may approve payment
		Zend_Registry::set('MAKER_LIMIT', array());
		$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $PS_NUMBER);
		$paramApprove = array(
			"FROM" 				=> "D",						// D: by Detail, M: by Multiple
			"PS_AMOUNT" 			=> $pslip["amount"],		// used in getApprovalBoundary()
			"PS_CCY" 				=> $pslip["ccy"],			// used in getApprovalBoundary()
			"ACCTSRCarr" 			=> $ACCTSRC_arr,
			"ACBENEF_IDarr" 		=> $ACBENEF_IDarr,
			"_approveBeneficiary" => $this->_hasPriviApproveBene,
			"_approvePayment" 	=> $this->_hasPriviApprovePayment,
		);

		$validate->checkApprove($paramApprove);

		$this->view->valError 	 = $validate->isError();
		$this->view->valErrorMsg = $validate->getErrorMsg();
		$this->view->boundary 	 = $validate->getValidateInfo();
		// End - check if user check may approve payment

		$config = Zend_Registry::get('config');
		$paystatusarr = array_combine(array_values($config['payment']['status']['code']),array_values($config['payment']['status']['desc']));
			//var_dump();die;
  		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE '-' END)";


		$select	= $this->_db->select()
			->from(
				array('TP' => 'T_PSLIP'),
				array(
					'REF_ID'					=> 'TP.REFF_ID',
					'PS_PERIODIC'					=> 'TP.PS_PERIODIC',
					'EFTIME'					=> 'TP.PS_EFTIME',
					'REMAIN'					=> 'TP.PS_REMAIN',
					'STATUS'					=> $casePayStatus,
					'TRANS'						=> 'TP.PS_TXCOUNT'
				)
			)
			->where('TP.PS_NUMBER = ?', $PS_NUMBER);
			//echo $select;die;
		$pslipTrx = $this->_db->fetchRow($select);
		
		if($pslipTrx['TRANS'] > 1){
			//$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$downloadURL = $this->view->url(array('module' => 'display', 'controller' => 'viewdetail', 'action' => 'downloadtrx', 'csv' => '1', 'payReff' => $PS_NUMBER), null, true);
			$this->view->downloadurl = $downloadURL;
			$this->view->trans = $pslipTrx['TRANS'];
		}
		$this->view->paystatus = $pslipTrx['STATUS'].' - Untransfered';
		$this->view->futuretrx = true;
			$this->view->frequen = '1x';
		$this->view->frequen 		= '1x';
		$this->view->fields 		= $fields;
		$this->view->fieldsdetail	= $fieldsdetail;
		$this->view->tableDtl 		= $tableDtl;
		$this->view->TITLE_MST		= $this->language->_('Transfer From');
		$this->view->TITLE_DTL		= $this->language->_('Transaction');
	}

	private function etaxold($pslip)
	{
		$PS_NUMBER = $this->_paymentRef;
		$getPaymentDetail 	= new display_Model_Paymentreport();
		$detail = $getPaymentDetail->getPaymentDetail($pslip["TRANSACTION_ID"]);
		$dataEtax = json_decode($detail[0]['LOG']);
		// echo '<pre>';
		 //var_dump($detail);die; 
		$detailPslip = $getPaymentDetail->getPslipDetail($pslip["PS_NUMBER"]);
		foreach ($detailPslip as $key => $value) {
			if ($value['PS_FIELDNAME'] == 'NTB/NTP'){
				$ntb = $value['PS_FIELDVALUE'];
			}
			if ($value['PS_FIELDNAME'] == 'NTPN'){
				$ntpn = $value['PS_FIELDVALUE'];
			}
			if ($value['PS_FIELDNAME'] == 'STAN'){
				$stan = $value['PS_FIELDVALUE'];
			}
		}

		//$this->_tableMstleft[1]["value"] = $dataEtax->paymentSubject;
		$this->_tableMst[5]["label"] = $this->language->_('Source Account');
		$this->_tableMst[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], $pslip["accsrc_alias"]);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$ACCTSRC_arr = array();
		if ($pslip['PS_BILLER_ID'] == '1158') {
			//table detail
			$compulsory = array('0' => 'No', '1' => 'Yes');
			$taxType = array('0' => 'NPWP', '1' => 'Non NPWP');
			$identity = array(
				'1' => 'KTP',
				'2' => 'NPWP',
				'3' => 'SIM',
				'4' => 'PASPOR',
				'5' => 'KITAS'
			);
			// roki
			$month = array(
				$this->language->_('January'),
				$this->language->_('February'),
				$this->language->_('March'),
				$this->language->_('April'),
				$this->language->_('May'),
				$this->language->_('June'),
				$this->language->_('July'),
				$this->language->_('August'),
				$this->language->_('September'),
				$this->language->_('October'),
				$this->language->_('November'),
				$this->language->_('December')
			);
			$map_code = array();
			$depositType = array();
			$select = $this->_db->select()
				->from(array('M_MAP_CODE'), array('MAP_CODE', 'MAP_NAME'))
				->query()->fetchAll();

			foreach ($select as $key) {
				$map_code[$key['MAP_CODE']] = $key['MAP_NAME'] . " (" . $key['MAP_CODE'] . ")";
			}
			if (isset($dataEtax->akuncode)) {
				$select = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_MAP_CODE'), array('DEPOSIT_CODE', 'DEPOSIT_NAME'))
						->where('MAP_CODE = ?', $dataEtax->akuncode)
				);

				foreach ($select as $key) {
					$depositType[$key['DEPOSIT_CODE']] = $key['DEPOSIT_NAME'] . " (" . $key['DEPOSIT_CODE'] . ")";
				}
			}
		
			$tableMst[0]["label"] = $this->language->_('Payment Ref#');
			$tableMst[0]["value"] = $pslip['PS_NUMBER'];

			$tableMst[1]["label"] = $this->language->_('Created Date');
			$tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

			$tableMst[2]["label"] = $this->language->_('Updated Date');
			$tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

			$tableMst[3]["label"] = $this->language->_('Payment Subject');
			$tableMst[3]["value"] = $dataEtax->paymentSubject;

			$tableMst[4]["label"] = $this->language->_('Source Account');
			$tableMst[4]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], "");

			$tableMst[5]["label"] = $this->language->_('Payment Status');
			$tableMst[5]["value"] = $pslip["payStatus"];

			$tableMst[6]["label"] = $this->language->_('Billing Code');
			$tableMst[6]["value"] = '2020032300014203';

			$tableMst[7]["label"] = $this->language->_('Amount');
			$tableMst[7]["value"] = 'IDR ' . $dataEtax->amount;

			$tableMst[8]["label"] = $this->language->_('Payer NPWP');
			$tableMst[8]["value"] = $dataEtax->payerNpwp;

			$tableMst[9]["label"] = $this->language->_('Payer Name');
			$tableMst[9]["value"] = $dataEtax->payername;

			$tableMst[10]["label"] = $this->language->_('Compulsory');
			$tableMst[10]["value"] = $compulsory[$dataEtax->chargetype] . ' ' . ($dataEtax->chargetype == '1' ? ' - (' . $dataEtax->chargeid . ')' : '');

			$tableMst[11]["label"] = $this->language->_('Asessable NPWP');
			$tableMst[11]["value"] = $dataEtax->asessableNpwp;

			$tableMst[12]["label"] = $this->language->_('Asessable Name');
			$tableMst[12]["value"] = $dataEtax->asessablename;

			$tableMst[13]["label"] = $this->language->_('Asessable Address');
			$tableMst[13]["value"] = $dataEtax->asessableaddress;

			$tableMst[14]["label"] = $this->language->_('Asessable City');
			$tableMst[14]["value"] = $dataEtax->asessablecity;

			$tableMst[15]["label"] = $this->language->_('Asessable Identity');
			$tableMst[15]["value"] = $identity[$dataEtax->asessableidentity1] . ' - ' .  $dataEtax->asessableidentity2;

			$tableMst[16]["label"] = $this->language->_('Map / Akun Code');
			$tableMst[16]["value"] = $map_code[$dataEtax->akuncode];

			$tableMst[17]["label"] = $this->language->_('Deposit Type');
			$tableMst[17]["value"] = $depositType[$dataEtax->deposittype];

			$tableMst[18]["label"] = $this->language->_('Tax Object Number (NOP)');
			$tableMst[18]["value"] = $dataEtax->taxobjectnumber;

			$tableMst[19]["label"] = $this->language->_('SK Number');
			$tableMst[19]["value"] = $dataEtax->skNumber;

			$tableMst[20]["label"] = $this->language->_('Remark');
			$tableMst[20]["value"] = $dataEtax->remark;

			$tableMst[21]["label"] = $this->language->_('Tax Period  Payment');
			$tableMst[21]["value"] = $month[$dataEtax->month1] . ' ' . $this->language->_('to') . ' ' . $month[$dataEtax->month2] . ' ' . $dataEtax->periodic;

			$tableMst[22]["label"] = $this->language->_('NTB / NTP');
			$tableMst[22]["value"] = $ntb;

			$tableMst[23]["label"] = $this->language->_('NTPN');
			$tableMst[23]["value"] = $ntpn;

			$tableMst[24]["label"] = $this->language->_('STAN');
			$tableMst[24]["value"] = $stan;

			$tableMst[25]["label"] = $this->language->_('Transaction Status');
			$tableMst[25]["value"] = $detail[0]["TRANSFER_STATUS"];
		} elseif ($pslip['PS_BILLER_ID'] == '1156') {
			//adons Type of Tax First row
			$typeOfTax = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('M_SERVICE_PROVIDER'), array('PROVIDER_NAME'))
					->where('PROVIDER_ID = ?', $pslip['PS_BILLER_ID'])
			);
			$tableMst[0]["label"] = $this->language->_('Payment Ref#');
			$tableMst[0]["value"] = $pslip['PS_NUMBER'];

			$tableMst[1]["label"] = $this->language->_('Created Date');
			$tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

			$tableMst[2]["label"] = $this->language->_('Updated Date');
			$tableMst[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

			$tableMst[3]["label"] = $this->language->_('Source Account');
			$tableMst[3]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], "");

			$tableMst[4]["label"] = $this->language->_('Payment Status');
			$tableMst[4]["value"] = $pslip["payStatus"];

			$tableMst[5]["label"] = $this->language->_('Type of Tax');
			$tableMst[5]["value"] = $typeOfTax['PROVIDER_NAME'];

			$tableMst[6]["label"] = $this->language->_('Billing Code');
			$tableMst[6]["value"] = $dataEtax->orderId;

			$tableMst[7]["label"] = $this->language->_('Amount');
			$tableMst[7]["value"] = 'IDR ' . Application_Helper_General::displayMoney($dataEtax->amount);

			$billingPrefix = substr($dataEtax->orderId, 0, 1);

			if ($billingPrefix == '0' || $billingPrefix == '1' || $billingPrefix == '2' || $billingPrefix == '3') {
				$tableMst[8]["label"] = $this->language->_('NPWP');
				$tableMst[8]["value"] = $dataEtax->dataUi->npwp;

				$tableMst[9]["label"] = $this->language->_('Customer Name');
				$tableMst[9]["value"] = $dataEtax->dataUi->customer_name;

				$tableMst[10]["label"] = $this->language->_('Customer Address');
				$tableMst[10]["value"] = $dataEtax->dataUi->customer_address;

				$tableMst[11]["label"] = $this->language->_('Map / Akun Code');
				$tableMst[11]["value"] = $dataEtax->dataUi->account_map;

				$tableMst[12]["label"] = $this->language->_('Deposit Type');
				$tableMst[12]["value"] = $dataEtax->dataUi->type;

				$tableMst[13]["label"] = $this->language->_('Tax Object Number (NOP)');
				$tableMst[13]["value"] = $dataEtax->dataUi->NOP;

				$tableMst[14]["label"] = $this->language->_('SK Number');
				$tableMst[14]["value"] = $dataEtax->dataUi->sk_number;

				$tableMst[15]["label"] = $this->language->_('Tax Period Payment');
				$tableMst[15]["value"] = $dataEtax->dataUi->period;

				$tableMst[16]["label"] = $this->language->_('NTB / NTP');
				$tableMst[16]["value"] = $ntb;

				$tableMst[17]["label"] = $this->language->_('NTPN');
				$tableMst[17]["value"] = $ntpn;

				$tableMst[18]["label"] = $this->language->_('STAN');
				$tableMst[18]["value"] = $stan;

				$tableMst[19]["label"] = $this->language->_('Transaction Status');
				$tableMst[19]["value"] = $detail[0]["TRANSFER_STATUS"];
			} elseif ($billingPrefix == '4' || $billingPrefix == '5' || $billingPrefix == '6') {

				$tableMst[8]["label"] = $this->language->_('Customer Name');
				$tableMst[8]["value"] = $dataEtax->dataUi->customer_name;

				$tableMst[9]["label"] = $this->language->_('ID Type Customer');
				$tableMst[9]["value"] = $dataEtax->dataUi->customer_id;

				$tableMst[10]["label"] = $this->language->_('Document Type');
				$tableMst[10]["value"] = $dataEtax->dataUi->document_type;

				$tableMst[11]["label"] = $this->language->_('Document Number');
				$tableMst[11]["value"] = $dataEtax->dataUi->document_number;

				$tableMst[12]["label"] = $this->language->_('Document Date');
				$tableMst[12]["value"] = $dataEtax->dataUi->document_date;

				$tableMst[13]["label"] = $this->language->_('KPBC Code');
				$tableMst[13]["value"] = $dataEtax->dataUi->kppbc_code;

				$tableMst[14]["label"] = $this->language->_('NTB / NTP');
				$tableMst[14]["value"] = $ntb;

				$tableMst[15]["label"] = $this->language->_('NTPN');
				$tableMst[15]["value"] = $ntpn;

				$tableMst[16]["label"] = $this->language->_('STAN');
				$tableMst[16]["value"] = $stan;

				$tableMst[17]["label"] = $this->language->_('Transaction Status');
				$tableMst[17]["value"] = $detail[0]["TRANSFER_STATUS"];
			} elseif ($billingPrefix == '7' || $billingPrefix == '8' || $billingPrefix == '9') {

				$tableMst[8]["label"] = $this->language->_('Customer Name');
				$tableMst[8]["value"] = $dataEtax->dataUi->customer_name;

				$tableMst[9]["label"] = $this->language->_('K/L');
				$tableMst[9]["value"] = $dataEtax->dataUi->k_l;

				$tableMst[10]["label"] = $this->language->_('Echelon Unit 1');
				$tableMst[10]["value"] = $dataEtax->dataUi->eselon_unit;

				$tableMst[11]["label"] = $this->language->_('Code Unit');
				$tableMst[11]["value"] = $dataEtax->dataUi->work_unit;

				$tableMst[12]["label"] = $this->language->_('NTB / NTP');
				$tableMst[12]["value"] = $ntb;

				$tableMst[13]["label"] = $this->language->_('NTPN');
				$tableMst[13]["value"] = $ntpn;

				$tableMst[14]["label"] = $this->language->_('STAN');
				$tableMst[14]["value"] = $stan;

				$tableMst[15]["label"] = $this->language->_('Transaction Status');
				$tableMst[15]["value"] = $detail[0]["TRANSFER_STATUS"];
			}
		}
		//var_dump($tableMst);die;
		return $tableMst;
		/////
	}

	private function sp2d($pslip)
	{
		$getPaymentDetail 	= new display_Model_Paymentreport();
		$detail = $getPaymentDetail->getPaymentDetail($pslip["PS_NUMBER"]);

		$ACBENEF_IDarr = array();
		$ACBENEF_IDarr[$pslip["acbenef_id"]]["CCY"] = $pslip["acbenef_ccy"];

		$tableMst1[0]["label"] = $this->language->_('Payment Ref#');
		$tableMst1[0]["value"] = $pslip['PS_NUMBER'];

		$tableMst1[1]["label"] = $this->language->_('Created Date');
		$tableMst1[1]["value"] = Application_Helper_General::convertDate($pslip['created'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

		$tableMst1[2]["label"] = $this->language->_('Updated Date');
		$tableMst1[2]["value"] = Application_Helper_General::convertDate($pslip['updated'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

		$tableMst1[3]["label"] = $this->language->_('Payment Date');
		$tableMst1[3]["value"] = substr(Application_Helper_General::convertDate($pslip['efdate'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat), 0, 11);

		$tableMst1[4]["label"] = $this->language->_('Payment Subject');
		$tableMst1[4]["value"] = $pslip['PS_SUBJECT'];

		$tableMst1[5]["label"] = $this->language->_('Source Account');
		$tableMst1[5]["value"] = Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_bankname"], "");

		$tableMst1[6]["label"] = $this->language->_('Payment Type');
		$tableMst1[6]["value"] = $pslip['payType'];

		$tableMst1[7]["label"] = $this->language->_('Payment Status');
		$tableMst1[7]["value"] = $pslip["payStatus"];

		$tableMst[0]["label"] = $this->language->_('No SP2D');
		$tableMst[0]["value"] = $pslip['SP2D_NO'];

		$tableMst[1]["label"] = $this->language->_('SP2D Date');
		$tableMst[1]["value"] = Application_Helper_General::convertDate($pslip['SP2D_DATE'], $this->_dateViewFormat);

		$tableMst[2]["label"] = $this->language->_('No SPM');
		$tableMst[2]["value"] = $pslip['SPM_NO'];

		$tableMst[3]["label"] = $this->language->_('SPM Date');
		$tableMst[3]["value"] = Application_Helper_General::convertDate($pslip['SPM_DATE'], $this->_dateViewFormat);

		$tableMst[4]["label"] = $this->language->_('SKPD Name');
		$tableMst[4]["value"] = $pslip['SKPD_NAME'];

		$tableMst[5]["label"] = $this->language->_('SPP Type');
		$tableMst[5]["value"] = $pslip['SPP_TYPE'];

		$tableMst[6]["label"] = $this->language->_('Beneficiary Account Name');
		$tableMst[6]["value"] = $pslip['BENEFICIARY_ACCOUNT_NAME'];

		$tableMst[7]["label"] = $this->language->_('Beneficiary Account Number');
		$tableMst[7]["value"] = $pslip['BENEFICIARY_ACCOUNT'];

		$tableMst[8]["label"] = $this->language->_('Beneficiary Identity');
		$tableMst[8]["value"] = $pslip['BENEFICIARY_ID_TYPE'] . ' ' . $pslip['BENEFICIARY_ID_NUMBER'];

		$tableMst[9]["label"] = $this->language->_('Transaction Purpose');
		$tableMst[9]["value"] = $pslip['LLD_TRANSACTION_PURPOSE'];

		$tableMst[10]["label"] = $this->language->_('Description');
		$tableMst[10]["value"] = nl2br($pslip['LLD_DESC']);

		$tableMst[11]["label"] = $this->language->_('Actual Amount');
		$tableMst[11]["value"] = 'IDR ' . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']);

		$tableMst[12]["label"] = $this->language->_('Deduction Amount');
		$tableMst[12]["value"] = 'IDR ' . Application_Helper_General::displayMoney($pslip['TOTAL_CHARGES']);

		$tableMst[13]["label"] = $this->language->_('Total Amount');
		$tableMst[13]["value"] = 'IDR ' . Application_Helper_General::displayMoney($pslip['PS_TOTAL_AMOUNT']);

		$tableMst[14]["label"] = $this->language->_('Transaction Status');
		$tableMst[14]["value"] = $detail[0]["TRANSFER_STATUS"];

		$tableMst = array_merge($tableMst1, $tableMst);
		return $tableMst;
		/////
	}

	public function downloadtrxAction()
	{
		$PS_NUMBER 			= trim(strip_tags($this->_getParam('payReff')));

		$paramPayment = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPayment($paramPayment);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);
		
		if (!empty($pslip)) {
			// separate credit and debet view
			if (
				// $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || removed globar var ?
				$pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"]
			)
				$this->downloadTrxDebet($PS_NUMBER);
			else
				$this->downloadTrxCredit($PS_NUMBER, $pslip['payType'], $pslip['PS_TYPE']);
		} else {
			$data = array();
			$data[0][0] = "Invalid Payment Number";
			$this->_helper->download->csv(array(), $data, null, $PS_NUMBER);
		}
	}

	protected function downloadTrxCredit($PS_NUMBER, $PS_TYPE, $paymenttype)
	{
		//		"BENEFICIARY ACC","BENEFICIARY NAME","CCY","AMOUNT","MESSAGE","ADDITIONAL MESSAGE","EMAIL ADDRESS","TRANSFER TYPE","BANK CODE"," BANK NAME ","BANK CITY","CITIZENSHIP"
		//		"2003019114","Account Name","USD","3,000.50","MSG USD WITHIN","ADDITIONAL MSG","admin@yahoo.com","PB","","",""
		//		"2003019126","Baharudin","IDR","3,500.50","MSG IDR SKN","PAYMENT DESC 2004","admin2@yahoo.com","SKN","00023234","BNI, Ambon","Thamrin","R"
		//		"2003019120","Antony","IDR","1000","MSG IDR RTGS","PAYMENT DESC 2005","admin2@yahoo.com","RTGS","00023235","BCA, SBY","Address 12","NR"

		$caseTransferType = Application_Helper_General::caseArray($this->_transfertype);

		 //print_r($PS_TYPE);die;

		if ($PS_TYPE == 'Payroll' || $PS_TYPE == 'Payroll Other') {
			$header = array($this->language->_('BENEFICIARY ACC'), $this->language->_('CCY'), $this->language->_('MESSAGE'));

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						//'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE'
					)
				)
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);
			$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
		} else if ($PS_TYPE == 'Multi Credit - Import') {
			$header = array($this->language->_('DESTINATION ACC'), $this->language->_('CCY'), $this->language->_('AMOUNT'), $this->language->_('MESSAGE'), $this->language->_('TRANSFER TYPE'), $this->language->_('CLEARING CODE'));

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						'TRA_TYPE'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '5' THEN 'ONLINE'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'SELL'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'BUY'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 ELSE ''
																			END"),
						'CLR_CODE'				=> 'TT.CLR_CODE',

					)
				)
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);
			$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
		}else if(strtoupper($PS_TYPE) == strtoupper('Bulk Biller')){
			$header = array($this->language->_('Biller'), $this->language->_('Order Id'), $this->language->_('Amount'));

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'BILLER_ID'				=> 'TT.BILLER_ID',
						'BILLER_ORDER_ID'			=> 'TT.BILLER_ORDER_ID',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT'
						//'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE'
					)
				)
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);
			$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
		} else {

			$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
			$caseTransStatus = "(CASE TT.TRA_STATUS ";
			foreach($transstatusarr as $key=>$val)
			{
				$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
			}
			$caseTransStatus .= " ELSE 'N/A' END)";

			$select	= $this->_db->select()
				->from(
					array('TT' => 'T_TRANSACTION'),
					array(
						'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
						'ACBENEF_NAME'			=> 'TT.BENEFICIARY_ACCOUNT_NAME',
						'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
						'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
						'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
						//'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

						'TRA_REFNO'				=> 'TT.TRA_REFNO',
						'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
						'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("
																			CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
						'BANK_CODE'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 WHEN TT.TRANSFER_TYPE = '1' THEN TT.CLR_CODE
																				 WHEN TT.TRANSFER_TYPE = '2' THEN TT.CLR_CODE
																				 ELSE TT.SWIFT_CODE
																			END"),
						'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
						'BANK_CITY'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN ''
																				 ELSE TT.BENEFICIARY_BANK_CITY
																			END"),
						'CITIZENSHIP'				=> new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CITIZENSHIP = 'W' THEN 'WNI'
																				 ELSE 'WNA'
																			END"),
						'RESIDENT_STATUS'				=> new Zend_Db_Expr("
																			CASE WHEN TT.BENEFICIARY_CITIZENSHIP = 'R' THEN 'Resident'
																				 ELSE 'Non Resident'
																			END"),
						//'CITIZENSHIP'			=> 'TT.BENEFICIARY_CITIZENSHIP',
						//'RESIDENT_STATUS'			=> 'TT.BENEFICIARY_RESIDENT',
						'PS_CREATED'			=> 'C.PS_CREATED',
						'REFF_ID'				=> 'C.REFF_ID',
						'PS_TXCOUNT'			=> 'C.PS_TXCOUNT',
						'PS_CCY'				=> 'C.PS_CCY',
						'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
						'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
						'TRA_STATUS'			=> $caseTransStatus,
					)
				)
				->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
				->where('TT.PS_NUMBER = ?', $PS_NUMBER);

			$data = $this->_db->fetchAll($select);

			if ($paymenttype == '19') {

				$PS_CREATED = explode('-',$data[0]['PS_CREATED']);
				$PSCREATED  = explode(' ',$PS_CREATED[2]);
				
		      	$headerData = array($this->language->_('SWEEP FROM ACCT'), $this->language->_('REMAIN BALANCE'), $this->language->_('MESSAGE'), $this->language->_('CUST REF'));
		      	
		      	foreach ($data as $p => $pTrx)
				{
					$paramTrx = array(  
					          "0" => $pTrx['SOURCE_ACCOUNT'],
					          "1" => $pTrx['TRA_REMAIN'],
					          "2" => $pTrx['TRA_MESSAGE'],
					          "3" => $pTrx['REFF_ID'],
					        );

					$newData[] = $paramTrx;
				}
				
			}else{

				$headerData = array($this->language->_('BENEFICIARY ACC'), $this->language->_('BENEFICIARY NAME'), $this->language->_('CCY'), $this->language->_('AMOUNT'), $this->language->_('MESSAGE'), $this->language->_('EMAIL ADDRESS'), $this->language->_('TRANSFER TYPE'), $this->language->_('BANK CODE'), $this->language->_('BANK NAME'), $this->language->_('BANK CITY'), $this->language->_('CITIZENSHIP'), $this->language->_('RESIDENT STATUS'));
				foreach($data as $key => $val){
					unset($data[$key]['TRA_REFNO']);
					unset($data[$key]['PS_CREATED']);
					unset($data[$key]['REFF_ID']);
					unset($data[$key]['PS_TXCOUNT']);
					unset($data[$key]['PS_CCY']);
					unset($data[$key]['TRA_REMAIN']);
					unset($data[$key]['SOURCE_ACCOUNT']);
					unset($data[$key]['TRA_STATUS']);
					
				}
				$newData = $data;

			}
			

			$this->_helper->download->csv($headerData, $newData, null, $PS_NUMBER);
		}
		//$header = array("BENEFICIARY ACC","BENEFICIARY NAME","CCY","AMOUNT","MESSAGE","ADDITIONAL MESSAGE","EMAIL ADDRESS","TRANSFER TYPE","BANK CODE"," BANK NAME" ,"BANK CITY","CITIZENSHIP","RESIDENT STATUS");

	}

	protected function downloadTrxDebet($PS_NUMBER)
	{
		//		ACCT SOURCE,AMOUNT,MESSAGE,ADDITIONAL MESSAGE
		//		"0000000000000000","350,000.90","MESSAGE","ADDITIONAL MESSAGE"

		$header = array("ACCT SOURCE", "AMOUNT", "MESSAGE");
		$select	= $this->_db->select()
			->from(
				array('TT' => 'T_TRANSACTION'),
				array(
					'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
					'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
					'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
					//	'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',

					//	'TRA_REFNO'				=> 'TT.TRA_REFNO',
				)
			)
			->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		$this->_helper->download->csv($header, $data, null, $PS_NUMBER);
	}

	public function downloadfileAction(){
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$select = $this->_db->select()
							->distinct()
							->from(	
									array('TFS'=>'T_PSLIP'),
									array(
											'PS_FILE'			=>'TFS.PS_FILE'
										))
							->where('TFS.CUST_ID =?',$this->_custIdLogin);
			$PS_NUMBER      = $this->_getParam('payReff');
			$select->where('PS_NUMBER =?',$PS_NUMBER);
			$data = $this->_db->fetchRow($select);
			$attahmentDestination = UPLOAD_PATH . '/document/temp/';
			//strlower
			//var_dump($data);die;
			$this->_helper->download->file(strtolower($data['PS_FILE']),$attahmentDestination.strtolower($data['PS_FILE']));
	}

	public function templateaddAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$cross = $this->_getParam('cross');
		$transpose = $this->_getParam('transpose');
		$exrate = $this->_getParam('exrate');
		$refnum = $this->_getParam('refnum');
		$custnum = $this->_getParam('custnum');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "1",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_CROSS' 			=> $cross,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_EXCHANGE' 		=> $exrate,
			'TEMP_REFNUM' 			=> $refnum,
			'TEMP_CUSTREF' 			=> $custnum,
			'TEMP_LOCKED'			=> $locked,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function templateadddomesticAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$tratype = $this->_getParam('tratype');
		$bene = $this->_getParam('bene');
		$beneaddress = $this->_getParam('beneaddress');
		$citizenship = $this->_getParam('citizenship');
		$nationality = $this->_getParam('nationality');
		$benecategory = $this->_getParam('benecategory');
		$beneidtype = $this->_getParam('beneidtype');
		$beneidnum = $this->_getParam('beneidnum');
		$bank = $this->_getParam('bank');
		$city = $this->_getParam('city');
		$email_domestic = $this->_getParam('email_domestic');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');


		try {
			$this->_db->beginTransaction();
			$insertArr = array(
				'T_NAME' 			=> $template,
				'T_CREATEDBY' 		=> $this->_userIdLogin,
				'T_CREATED' 		=> new Zend_Db_Expr("now()"),
				'T_GROUP'			=> $this->_custIdLogin,
				'T_TARGET'			=> $target,
				'T_TYPE'			=> '2',
			);

			$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);



			$lastId = $this->_db->lastInsertId();

			if (!empty($bene)) {
				$select = $this->_db->select()
					->from(array('A' => 'M_BENEFICIARY'), array('*'));
				$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
				$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
				$benedata = $this->_db->fetchall($select);
				$benename = $benedata['0']['BENEFICIARY_NAME'];
			} else {
				$benedata = array();
				$benename = '';
			}

			// print_r($insertArr);die;

			$insertData = array(
				'TEMP_ID' 				=> $lastId,
				'TEMP_SUBJECT' 			=> $subject,
				'TEMP_SOURCE' 			=> $source,
				'TEMP_TRATYPE' 			=> $tratype,
				'TEMP_BENE' 			=> $bene,
				'TEMP_BENE_NAME' 		=> $benename,
				'TEMP_BENEADDRESS' 		=> $beneaddress,
				'TEMP_CITIZENSHIP' 		=> $citizenship,
				'TEMP_NATIONALITY' 		=> $nationality,
				'TEMP_BENCATEGORY' 		=> $benecategory,
				'TEMP_BENEIDTYPE' 		=> $beneidtype,
				'TEMP_BENEIDNUM' 		=> $beneidnum,
				'TEMP_BANK' 			=> $bank,
				'TEMP_CITY' 			=> $city,
				'TEMP_EMAIL_DOMESTIC' 	=> $email_domestic,

				'TEMP_AMOUNT' 			=> $amount,
				'TEMP_NOTIF' 			=> $notif,
				'TEMP_EMAIL' 			=> $email,
				'TEMP_SMS' 				=> $sms,
				'TEMP_LOCKED'			=> $locked,
			);

			$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollback();
			Zend_Debug::Dump($e->getMessages());
		}

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function templateaddremittanceAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();


		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$benename2 = $this->_getParam('benename');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$chargetype = $this->_getParam('chargetype');
		$transpose = $this->_getParam('transpose');
		$emaildomestic = $this->_getParam('emaildomestic');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');
		$messagetext = $this->_getParam('messagetext');
		$identity = $this->_getParam('identity');
		$transactor = $this->_getParam('transactor');
		$ccy = $this->_getParam('ccy');
		$bankname = $this->_getParam('bankname');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "3",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_CHARGE_TYPE' 		=> $chargetype,
			'TEMP_MESSAGE' 		=> $messagetext,
			'TEMP_EMAIL_DOMESTIC' 	=> $emaildomestic,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_IDENTY' 			=> $identity,
			'TEMP_RELATIONSHIP'		=> $transactor,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_LOCKED'			=> $locked,
			'TEMP_BANK'				=> $bankname,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}
	}


	public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		// die;


		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				//->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype);
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);

		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		// echo $selectuser;
		$datauser = $this->_db->fetchAll($selectuser);
		// print_r($datauser);die;
		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_USER'), array(
				'*'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);

		// print_r($usergroup);die();


		$this->view->boundarydata = $datauser;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {
				$group = explode('_', $value['GROUP_USER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				// print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);


					foreach ($list as $row => $data) {
						if ($data == $groupalfa) {
							$cek = true;
							// die('ter');
							break;
						}
					}
				}
			}
			if ($group[0] == 'S') {
				$cek = true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		}


		if ($cek) {




			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);

			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);

			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);
			// print_r($list);die;
			// var_dump($command)
			$thendata = explode('THEN', $command);
			// print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);
			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {
					foreach ($secondlist as $row => $thenval) {
						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			// var_dump($thendata);
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
					// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}
			// var_dump($grouplist);
			// // print_r($group);die;
			// // echo $thengroup;die;
			// echo '<pre>';
			// var_dump($thengroup);
			// var_dump($thendata);die;
			// print_r($thendata);echo '<br/>';die('here');
			if ($thengroup == true) {

				// echo $oriCommand;die;
				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;

					$indno = $i;
					// echo $oriCommand;echo '<br>';
					for ($a = $cthen - $indno; $a >= 1; --$a) {

						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}


					// print_r($oriCommand);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					// print_r($oriCommand);echo '<br>';
					// print_r($list);echo '<br>';

					$result = $this->generate($oriCommand, $list, $param, $psnumb);
					// print_r($i);

					// echo 'result-';print_r($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);


							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											return true;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						// echo $oriCommand;die;
						$result = $this->generate($oriCommand, $list, $param, $psnumb);
						// echo $result;echo '<br/>';die;
						if (!$result) {
							// die;
							foreach ($grouplist as $key => $valg) {
								// print_r($valg);die;
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							}
						} else {
							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						// die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);
						// print_r($secondlist);die;
						// print_r($grouplist);die;
						// print_r($thendata[$i]);die;
						// return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {
										return false;
									}
								}
							}
						}
						// $secondresult = $this->generate($thendata[$i-1],$list,$param,$psnumb);
						// print_r($thendata[$i-1]);
						// die;
						// print_r($thendata);die;
						foreach ($grouplist as $key => $valg) {
							// print_r($valg);
							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								// die('here');
								return true;
							} else if (trim($valg) == trim($thendata[$i - 1])) {
								$cekgroup = false;
								// die('here');
								return false;
							}
						}
						if (!$cekgroup) {
							// die('here');
							return true;
						}
					}
					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {
				// var_dump($groupalfa);die;
				foreach ($thendata as $ky => $vlue) {
					if ($vlue == $groupalfa) {
						return true;
					}
				}
				return false;
			}
			// die;


			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_APPROVAL'), array(
							'USER_ID'
						))
						->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.PS_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
					// echo $selectapprover;die;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			// print_r($approver);die;



			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$numb = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $numb . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			// print_r($phpCommand);die;
			$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			// die('here2');
			// var_dump ($result);die;
			return $result;
		} else {
			// die('here');
			return true;
		}
	}


	public function generate($command, $list, $param, $psnumb)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();
		// print_r($list);die;  	
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.PS_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;die;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		// print_r($approver);die;


		// print_r($approver);
		foreach ($param as $url) {
			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$numb = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $numb . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}

		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		// print_r($phpCommand);echo '<br/>';die;
		// var_dump($phpCommand);die;
		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			// var_dump($result);die;
			if ($result) {
				// var_dump($result);die;
				return false;
			} else {
				return true;
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;
	}

	public function summarydetailAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		$filter 			= new Application_Filtering();
		$PS_NUMBER 			= $filter->filter($this->_getParam('payReff'), "PS_NUMBER");

		$paramSQL = array(
			"WA" 				=> false,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);


		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getPaymentDetail($paramSQL);
		$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);
		if(!empty($this->_getParam('trxid'))){
				$select->where('T.TRANSACTION_ID = ?', (string) $this->_getParam('trxid'));
		}
		//echo $select;
		$pslip = $this->_db->fetchRow($select);
		//echo '<pre>';
		//var_dump($pslip);die;

		// $tableMst[0]["label"] = $this->language->_('Payment Type');
		// $tableMst[1]["label"] = $this->language->_('#Trans');
		// $tableMst[2]["label"] = $this->language->_('Recurring');
		// // var_dump($pslip);die;
		// if (($pslip['PS_CATEGORY'] == 'SINGLE PAYMENT' || $pslip['PS_CATEGORY'] == 'OPEN TRANSFER')) {
		// 	if ($pslip['TRANSFER_TYPE'] == 0) {
		// 		if ($pslip['SOURCE_ACCT_BANK_CODE'] == $pslip['BENEF_ACCT_BANK_CODE']) {
		// 			$payType = $pslip['payType'] . ' - ' . 'PB';
		// 		} else {
		// 			$payType = $pslip['payType'] . ' - ' . 'ONLINE';
		// 		}
		// 	} else if ($pslip['TRANSFER_TYPE'] == 1) {
		// 		$payType = $pslip['payType'] . ' - ' . 'SKN';
		// 	} else if ($pslip['TRANSFER_TYPE'] == 2) {
		// 		$payType = $pslip['payType'] . ' - ' . 'RTGS';
		// 	} else {
		// 		$payType = $pslip['payType'];
		// 	}
		// } else if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {

		// 	if ($pslip['PS_TYPE'] == '19') {
		// 		$poolingType = 'Remains';
		// 	} else {
		// 		$poolingType = 'Maintains';
		// 	}

		// 	$payType = $pslip['payType'] . ' - ' . $poolingType;
		// } else {
		// 	$payType = $pslip['payType'];
		// }

		// if (!empty($pslip['PERIODIC'])) {
		// 	$recurring = $this->language->_('Yes');
		// } else {
		// 	$recurring = $this->language->_('No');
		// }

		// $tableMst[0]["value"] = $payType;
		// $tableMst[1]["value"] = $pslip['PS_TXCOUNT'];
		// $tableMst[2]["value"] = $recurring;

		// if ($pslip['PS_CATEGORY'] == 'SWEEP PAYMENT') {



		// 	$arrday = array(
		// 		'0' => 'Sunday',
		// 		'1' => 'Monday',
		// 		'2' => 'Tuesday',
		// 		'3' => 'Wednesday',
		// 		'4' => 'Thursday',
		// 		'5' => 'Friday',
		// 		'6' => 'Saturday'
		// 	);



		// 	$select	= $this->_db->select()
		// 		->from(array('P'	 		=> 'T_PSLIP'))
		// 		->join(array('S' => 'T_PERIODIC'), 'P.PS_PERIODIC = S.PS_PERIODIC', array('S.*'))
		// 		->join(array('D' => 'T_PERIODIC_DAY'), 'D.PERIODIC_ID = S.PS_PERIODIC', array('D.DAY_ID', 'D.LIMIT_AMOUNT'))
		// 		->where("P.PS_NUMBER = ?", (string) $PS_NUMBER)
		// 		->GROUP('D.DAY_ID');


		// 	$periodicData = $this->_db->fetchAll($select);

		// 	$tableMst[3]["label"] = $this->language->_('Repeat');
		// 	$tableMst[4]["label"] = $this->language->_('Repeat On');
		// 	$tableMst[5]["label"] = $this->language->_('Start From');
		// 	$tableMst[6]["label"] = $this->language->_('Time');

		// 	if ($pslip['PS_TYPE'] == 19) {
		// 		$tableMst[7]["label"] = $this->language->_('Remains on Source');

		// 		$remainsOrMaintains = '<h6 class="text-danger"><b>' . $pslip['ccy'] . ' n/a </b></h6>';
		// 	} else {
		// 		$tableMst[7]["label"] = $this->language->_('Maintains Destination');

		// 		$remainsOrMaintains = '';
		// 		foreach ($periodicData as $key => $value) {
		// 			$remainsOrMaintains .= substr($arrday[$value['DAY_ID']], 0, 3) . ' IDR ' . Application_Helper_General::displayMoney($value['LIMIT_AMOUNT']) . '<br>';
		// 		}
		// 	}

		// 	if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 1) {


		// 		if (empty($pslip['PS_PERIODIC'])) {
		// 			$repeat = 'No';
		// 			$repeatOn = '-';
		// 		} else {
		// 			$repeat = 'Daily';
		// 			$repeatOn = '<div class="input-group">';
		// 			foreach ($periodicData as $key => $value) {


		// 				$repeatOn .= '<div class="roundChk" style="margin-left: -4%;">
		// 								<input type="checkbox" disabled class="checkboxday" id="checkbox' . $value['DAY_ID'] . '" value="' . $value['DAY_ID'] . '" checked />
		// 								<label for="checkbox' . $value['DAY_ID'] . '">' . substr($arrday[$value['DAY_ID']], 0, 1) . '</label>
		// 							</div>';
		// 			}
		// 			$repeatOn .= '</div>';
		// 		}
		// 	} else if ($periodicData[0]['PS_EVERY_PERIODIC_UOM'] == 2) {
		// 		if (empty($pslip['PS_PERIODIC'])) {
		// 			$repeat = 'No';
		// 			$repeatOn = '-';
		// 		} else {
		// 			$repeat = 'Weekly';
		// 			$repeatOn = $arrday2[$pslip['PS_EVERY_PERIODIC']];
		// 		}
		// 	} else {
		// 		if (empty($pslip['PS_PERIODIC'])) {
		// 			$repeat = 'No';
		// 			$repeatOn = '-';
		// 		} else {
		// 			$repeat = 'Monthly';
		// 			$repeatOn = $pslip['PS_EVERY_PERIODIC'] . ' of every month';
		// 		}
		// 	}

		// 	if (empty($pslip['PS_PERIODIC'])) {
		// 		$efDate = date_create($pslip['PS_EFDATE']);
		// 		$newEfDate = date_format($efDate, "d M Y");

		// 		$startFrom = $newEfDate;
		// 	} else {

		// 		$startDate = date_create($periodicData[0]['PS_PERIODIC_STARTDATE']);
		// 		$newStartDate = date_format($startDate, "d M Y");

		// 		$endDate = date_create($periodicData[0]['PS_PERIODIC_ENDDATE']);
		// 		$newEndDate = date_format($endDate, "d M Y");

		// 		$startFrom = $newStartDate . '  <span class="text-black-50">to</span>  ' . $newEndDate;
		// 	}

		// 	$tableMst[3]["value"] = $repeat;
		// 	$tableMst[4]["value"] = $repeatOn;
		// 	$tableMst[5]["value"] = $startFrom;
		// 	$tableMst[6]["value"] = $pslip['PS_EFTIME'];
		// 	$tableMst[7]["value"] = $remainsOrMaintains;
		// } else {
		// 	$tableMst[3]["label"] = $this->language->_('Total Amount');
		// 	$tableMst[4]["label"] = $this->language->_('Total Transfer Fee');
		// 	$tableMst[5]["label"] = $this->language->_('Total Payment');


		// 	$tableMst[3]["value"] = $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['PS_TOTAL_AMOUNT']);
		// 	$tableMst[4]["value"] = Application_Helper_General::displayMoney($pslip['FULL_AMOUNT_FEE'] == '0' ? '-' : $pslip['ccy'] . ' ' . $pslip['FULL_AMOUNT_FEE']);
		// 	$tableMst[5]["value"] = '<h6 class="text-danger"><b>' . $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney(($pslip['PS_TOTAL_AMOUNT'] + $pslip['FULL_AMOUNT_FEE'])) . '</b></h6>';
		// }

		if ($pslip["PS_TYPE"] == '19' || $pslip["PS_TYPE"] == '20') {
			
			if ($pslip["PS_TYPE"] == '19') {
				$tableMst[0]["label"] = $this->language->_('Remains on source');
			}else{
				$tableMst[0]["label"] = $this->language->_('Maintains on beneficiary');	
			}
			
			$tableMst[1]["label"] = $this->language->_('Minimum Transfer Amount');

			$tableMst[0]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['TRA_REMAIN']);
			$tableMst[1]["value"] = 'IDR '.Application_Helper_General::displayMoney($pslip['PS_MIN_AMOUNT']);

			$this->view->tableMst 		= $tableMst;
			$this->view->pslip = $pslip["PS_TYPE"];

		}else{

			$tableMst[0]["label"] = $this->language->_('Amount');
			$tableMst[1]["label"] = $this->language->_('Total Transfer Fee');
			$tableMst[2]["label"] = $this->language->_('Total');

			$tableMst[0]["value"] = $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']);
			$tableMst[1]["value"] = $pslip['TRANSFER_FEE'] == '0' ? '-' : $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney($pslip['TRANSFER_FEE']);
			$tableMst[2]["value"] = '<h6 style="color: red;"><b>' . $pslip['ccy'] . ' ' . Application_Helper_General::displayMoney(($pslip['TRA_AMOUNT'] + $pslip['TRANSFER_FEE'] )) . '</b></h6>';

			if (($pslip["PS_BILLER_ID"] == '1156' || $pslip["PS_BILLER_ID"] == '1158') && $pslip["PS_TYPE"] == '17') {
				$tableMst = $this->etax($pslip);
			} elseif ($pslip["PS_TYPE"] == '18') {
				$tableMst = $this->sp2d($pslip);
			}

			$this->view->tableMst 		= $tableMst;

		}
		
	}

	public function benefdetailAction()
	{
		$this->_helper->layout()->setLayout('newpopup');
		$filter 			= new Application_Filtering();
		$benefId 		= $filter->filter($this->_getParam('benef'), "BENEFICIARY_ID");
		$psnumber 		= $filter->filter($this->_getParam('pslip'), "PS_NUMBER");
		//var_dump($benefId);die;
		///SIKD
		if(!empty($psnumber)){
			
			$select = $this->_db->select()
			->from('T_TRANSACTION', array('*'))
			->where("PS_NUMBER = ?", $psnumber); 

		$addData = $this->_db->fetchRow($select);
			
			if(!empty($addData)){
				$this->view->sp2d = true;
				if(!empty($addData['ADDITIONAL_DATA'])){
					$data = json_decode($addData['ADDITIONAL_DATA'],true);
					//echo '<pre>';
					//var_dump($data);die;
					$no = 0;
					if(!empty($data['source']['pajak'])){
						foreach($data['source']['pajak'] as $key => $val){
							$taxarr = array_keys($val);
							foreach($taxarr as $keyt => $valt){
								$label = lcfirst(str_replace("_"," ",$valt));
								$tableMst[$keyt]["label"] = $this->language->_($label);
								if($val[$valt]=='1'){$value = 'Ya';}else{ $value = $val[$valt];}
								$tableMst[$keyt]["value"] = $value;
								$no++;
							}
						}
						
					}
					
					if(!empty($data['source']['potongan'])){
						foreach($data['source']['potongan'] as $key => $val){
							
								$label = lcfirst(str_replace("_"," ",$val['nama_potongan']));
								$tableMst[$no]["label"] = $this->language->_($label);
								//if($val[$valt]=='1'){$value = 'Ya';}else{ $value = $val['nominal'];}
								$tableMst[$no]["value"] = $val['nominal'];
							$no++;
						}
						
					}
					//echo '<pre>';
					
					if(!empty($data['data'])){
						//var_dump($data['data']);die;
						//foreach($data['data'] as $key => $val){
							
							$taxarr = array_keys($data['data']);
							
							foreach($taxarr as $keyt => $valt){
								$label = lcfirst(str_replace("_"," ",$valt));
								$tableMst[$no]["label"] = $this->language->_($label);
								if($data['data'][$valt]=='1'){$value = 'Ya';}else{ $value = $data['data'][$valt];}
								$tableMst[$no]["value"] = $value;
								$no++;
							}
						//}
						
					}
					
					
					//die;
				}
				
			}
			
			
			
			
		}else{
		$select = $this->_db->select()
			->from('M_BENEFICIARY', array('*'))
			->where("BENEFICIARY_ID = ?", $benefId); 

		$benefData = $this->_db->fetchRow($select);

		$citizenshipArr	= array("W" => "WNI", "N" => "WNA");
		$residentArr = array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);

		$settings 			= new Application_Settings();
		$LLD_array 			= array();
		$LLD_DESC_arrayCat 	= array();
		$lldTypeArr  		= $settings->getLLDDOMType();

		$LLD_CATEGORY_POST = '';
		if (!empty($benefData['BENEFICIARY_CATEGORY'])) {
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$LLD_array["CT"] 	= $benefData['BENEFICIARY_CATEGORY'];
			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$benefData['BENEFICIARY_CATEGORY']];
		} 

		$tableMst[0]["label"] = $this->language->_('Beneficiary Bank');
		$tableMst[1]["label"] = $this->language->_('City');
		$tableMst[2]["label"] = $this->language->_('Beneficiary Account');
		$tableMst[3]["label"] = $this->language->_('Beneficiary Name');
		$tableMst[4]["label"] = $this->language->_('Beneficiary Alias Name');
		$tableMst[5]["label"] = $this->language->_('Beneficiary Address 1');
		$tableMst[6]["label"] = $this->language->_('Beneficiary Address 2');
		$tableMst[7]["label"] = $this->language->_('Beneficiary Email');
		$tableMst[8]["label"] = $this->language->_('Citizenship');
		$tableMst[9]["label"] = $this->language->_('Nationality');
		$tableMst[10]["label"] = $this->language->_('Beneficiary Category');
		$tableMst[11]["label"] = $this->language->_('Beneficiary ID Type');
		$tableMst[12]["label"] = $this->language->_('Beneficiary ID Number');

		$tableMst[0]["value"] = empty($benefData['BANK_NAME']) ? $this->_bankName : $benefData['BANK_NAME'];
		$tableMst[1]["value"] = empty($benefData['BANK_CITY']) ? '-' : $benefData['BANK_CITY'];
		$tableMst[2]["value"] = $benefData['BENEFICIARY_ACCOUNT'];
		$tableMst[3]["value"] = $benefData['BENEFICIARY_NAME'];
		$tableMst[4]["value"] = empty($benefData['BENEFICIARY_ALIAS']) ? '-' : $benefData['BENEFICIARY_ALIAS'];
		$tableMst[5]["value"] = empty($benefData['BENEFICIARY_ADDRESS']) ? '-' : $benefData['BENEFICIARY_ADDRESS'];
		$tableMst[6]["value"] = empty($benefData['BENEFICIARY_ADDRESS2']) ? '-' : $benefData['BENEFICIARY_ADDRESS2'];
		$tableMst[7]["value"] = empty($benefData['BENEFICIARY_EMAIL']) ? '-' : $benefData['BENEFICIARY_EMAIL'];
		$tableMst[8]["value"] = empty($benefData['BENEFICIARY_RESIDENT']) ? '-' : $residentArr[$benefData['BENEFICIARY_RESIDENT']];
		$tableMst[9]["value"] = empty($benefData['BENEFICIARY_CITIZENSHIP']) ? '-' : $citizenshipArr[$benefData['BENEFICIARY_CITIZENSHIP']];
		$tableMst[10]["value"] = empty($LLD_CATEGORY_POST) ? '-' : $LLD_CATEGORY_POST;
		$tableMst[11]["value"] = empty($benefData['BENEFICIARY_ID_TYPE']) ? '-' : $benefData['BENEFICIARY_ID_TYPE'];
		$tableMst[12]["value"] = empty($benefData['BENEFICIARY_ID_NUMBER']) ? '-' : $benefData['BENEFICIARY_ID_NUMBER'];
		
		}
		
		
		
		$this->view->tableMst 		= $tableMst;
	}
}
