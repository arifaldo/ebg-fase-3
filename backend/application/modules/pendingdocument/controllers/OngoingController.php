<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgreport_OngoingController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function detailAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $bg = $this->_getParam('bg');

    $sessToken  = new Zend_Session_Namespace('Tokenenc');
    $password   = $sessToken->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $decryption = urldecode($bg);
    $decryption_bg = $AESMYSQL->decrypt($decryption, $password);

    $conf = Zend_Registry::get('config');

    $select = $this->_db->select()
      ->from(
        array('TBG' => 'TEMP_BANK_GUARANTEE'),
        array('*')
      )
      ->joinLeft(
        array('MB' => 'M_BRANCH'),
        'MB.BRANCH_CODE = TBG.BG_BRANCH',
        array('BRANCH_NAME')
      )
      ->joinLeft(
        array('MC' => 'M_CUSTOMER'),
        'MC.CUST_ID = TBG.CUST_ID',
        array(
          'CUST_ID',
          'CUST_NAME',
          'CUST_NPWP',
          'CUST_ADDRESS',
          'CUST_CITY',
          'CUST_FAX',
          'CUST_CONTACT',
          'CUST_PHONE',
        )
      )
      ->joinLeft(
        array('MCST' => 'M_CUSTOMER'),
        'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
        array(
          "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
        )
      )
      ->joinLeft(
        array('INSURANCE' => 'M_CUSTOMER'),
        'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
        array(
          "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
        )
      )
      ->joinLeft(
        array('MCL' => 'M_CITYLIST'),
        'MCL.CITY_CODE = MC.CUST_CITY',
        array('CITY_NAME')
      )
      ->joinLeft(
        array('TBGD' => 'TEMP_BANK_GUARANTEE_DETAIL'),
        'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
        array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
      )
      ->joinLeft(
        array('TBGS' => 'TEMP_BANK_GUARANTEE_SPLIT'),
        'TBGS.BG_NUMBER = TBG.BG_REG_NUMBER',
        [
          "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
        ]
      )
      ->where('TBG.BG_REG_NUMBER = ?', $decryption_bg);
    //->where('TBG.CUST_ID = ?', $this->_custIdLogin);
    //die();
    $auth = Zend_Auth::getInstance()->getIdentity();
    if ($auth->userHeadQuarter == "NO") {
      $select->where('MB.ID = ?', $auth->userBranchId); // Add Bahri
    }
    $data = $this->_db->fetchRow($select);

    switch ($data["CHANGE_TYPE"]) {
      case '0':
        $this->view->suggestion_type = "New";
        break;
      case '1':
        $this->view->suggestion_type = "Amendment Changes";
        break;
      case '2':
        $this->view->suggestion_type = "Amendment Draft";
        break;
    }

    //echo '<pre>';print_r($data);
    $conf = Zend_Registry::get('config');
    // BG TYPE
    $bgType         = $conf["bg"]["type"]["desc"];
    $bgCode         = $conf["bg"]["type"]["code"];

    $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

    $this->view->arrbgType = $arrbgType;

    //BG Counter Guarantee Type
    $bgcgType         = $conf["bgcg"]["type"]["desc"];
    $bgcgCode         = $conf["bgcg"]["type"]["code"];

    $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

    $this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

      $bgdatasplit = $this->_db->select()
        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
        ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
        ->query()->fetchAll();

      $this->view->fullmember = $bgdatasplit;
    }


    $sqlbgdetail = $this->_db->select()
      ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
      //->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"]);
    //  die('a');
    $bgdatadetail = $sqlbgdetail
      ->query()->fetchAll();


    if (!empty($bgdatadetail)) {
      foreach ($bgdatadetail as $key => $value) {

        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
          if ($value['PS_FIELDNAME'] == 'Insurance Name') {
            $this->view->insuranceName =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
            $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount') {
            $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
            $this->view->paDateStart =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
            $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
          }
        } else {

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
            $this->view->owner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
            $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
            $this->view->owner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
            $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
            $this->view->owner3 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
            $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
          }
        }
      }
    }

    $get_linefacility = $this->_db->select()
      ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
      ->where("CUST_ID = ?", $data["CUST_ID"])
      ->query()->fetchAll();

    $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
    $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

    $this->view->linefacility = $get_linefacility[0];

    $this->view->acct = $data['FEE_CHARGE_TO'];

    $conf = Zend_Registry::get('config');
    $this->view->bankname = $conf['app']['bankname'];

    $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
    $AccArr = $CustomerUser->getAccountsBG($param);
    //var_dump($AccArr);die;

    if (!empty($AccArr)) {
      $this->view->src_name = $AccArr['0']['ACCT_NAME'];
    }

    $get_cash_collateral = $this->_db->select()
      ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
      ->where("CUST_ID = ?", "GLOBAL")
      ->where("CHARGES_TYPE = ?", "10")
      ->query()->fetchAll();

    $this->view->cash_collateral = $get_cash_collateral[0];

    $bgpublishType     = $conf["bgpublish"]["type"]["desc"];
    $bgpublishCode     = $conf["bgpublish"]["type"]["code"];

    $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

    $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

    $arrBankFormat = array(
      1 => 'Bank Standard',
      2 => 'Special Format (with bank approval)'
    );

    $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
    $this->view->bankFormatNumber = $data['BG_FORMAT'];

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Billingual',
    );

    $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

    $checkOthersAttachment = $this->_db->select()
      ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
      ->where("BG_REG_NUMBER = '268312345601F4201'")
      ->order('A.INDEX ASC')
      ->query()->fetchAll();

    if (count($checkOthersAttachment) > 0) {
      $this->view->othersAttachment = $checkOthersAttachment;
    }

    // Get data T_BANK_GUARANTEE_HISTORY
    $select = $this->_db->select()
      ->from(
        array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
        array('*')
      )
      ->where('TBGH.BG_REG_NUMBER = ?', $decryption_bg)
      ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
    $dataHistory = $this->_db->fetchAll($select);

    // Get data TEMP_BANK_GUARANTEE_SPLIT
    $select = $this->_db->select()
      ->from(
        array('TBGS' => 'TEMP_BANK_GUARANTEE_SPLIT'),
        array('*')
      )
      ->where('TBGS.BG_REG_NUMBER = ?', $decryption_bg);
    $dataAccSplit = $this->_db->fetchAll($select);

    $config = Zend_Registry::get('config');

    $docTypeCode = $config["bgdoc"]["type"]["code"];
    $docTypeDesc = $config["bgdoc"]["type"]["desc"];
    $docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

    $statusCode = $config["bg"]["status"]["code"];
    $statusDesc = $config["bg"]["status"]["desc"];
    $statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

    $historyStatusCode = $config["history"]["status"]["code"];
    $historyStatusDesc = $config["history"]["status"]["desc"];
    $historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

    $counterTypeCode = $config["bgcg"]["type"]["code"];
    $counterTypeDesc = $config["bgcg"]["type"]["desc"];
    $counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

    //$config    		= Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    // $arrStatus = array(
    //   '7'  => 'Canceled',
    //   '20' => 'On Risk',
    //   '21' => 'Off Risk',
    //   '22' => 'Claimed On Process',
    //   '23' => 'Claimed'
    // );

    $this->view->arrStatus = $arrStatus;




    $this->view->data               = $data;
    //Zend_Debug::dump($data);
    $this->view->dataHistory        = $dataHistory;
    $this->view->dataAccSplit       = $dataAccSplit;
    $this->view->docTypeArr         = $docTypeArr;
    $this->view->statusArr          = $statusArr;
    $this->view->historyStatusArr   = $historyStatusArr;
    $this->view->counterTypeArr     = $counterTypeArr;

    $download = $this->_getParam('download');
    if ($download == 1) {
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';
      $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
    }
  }

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $csv = $this->_getParam('csv');
    $pdf = $this->_getParam('pdf');

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                   'label' => $this->language->_('Alias Name'),
                                   'sortable' => true),*/
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg No# /  BG No#'),
      ),
      'subject'     => array(
        'field'    => 'BG_SUBJECT',
        'label'    => $this->language->_('Subject'),
      ),
      'principal'     => array(
        'field'    => 'CUST_NAME',
        'label'    => $this->language->_('Principal Name'),
      ),
      'obligee'     => array(
        'field'    => 'RECIPIENT_NAME',
        'label'    => $this->language->_('Obligee Name'),
      ),
      'branch'     => array(
        'field'    => 'BRANCH_NAME',
        'label'    => $this->language->_('Bank Branch'),
      ),
      'countertype'     => array(
        'field'    => 'COUNTERTYPE',
        'label'    => $this->language->_('Counter Type'),
      ),
      'changetype'     => array(
        'field'    => 'CHANGETYPE',
        'label'    => $this->language->_('Change Type'),
      ),
      'status'     => array(
        'field'    => 'STATUS',
        'label'    => $this->language->_('Status'),
      )



    );

    $filterlist = array('BG_SUBJECT', 'BG_STATUS', 'BG_NUMBER', 'BG_REG_NUMBER');

    $this->view->filterlist = $filterlist;

    $filterArr = array(
      'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
      'BG_STATUS'  => array('StringTrim', 'StripTags'),
      'BG_NUMBER'   => array('StringTrim', 'StripTags'),
      'BG_REG_NUMBER'   => array('StringTrim', 'StripTags')
    );

    // print_r($filterArr);die;
    // if POST value not null, get post, else get param ,"SOURCE_ACCOUNT_NAME","BENEFICIARY_ACCOUNT","BENEFICIARY_ACCOUNT_NAME","TRANSACTION_ID","PS_CCY"
    $dataParam = array('BG_SUBJECT', 'BG_STATUS', 'BG_NUMBER', 'BG_REG_NUMBER');
    $dataParamValue = array();
    foreach ($dataParam as $dtParam) {

      // print_r($dtParam);die;
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            if (!empty($dataParamValue[$dtParam])) {
              $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
            }
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }

      // $dataPost = $this->_request->getPost($dtParam);
      // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
    }


    $options = array('allowEmpty' => true);
    $validators = array(
      'BG_SUBJECT'        => array(),
      'BG_STATUS'        => array(),
      // 'SOURCE_ACCOUNT'     => array(array('InArray', array('haystack' => array_keys($optarrAccount)))),
      'BG_NUMBER'     => array(),
      'BG_REG_NUMBER'     => array()

    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

    // print_r($fCreatedEnd);die;


    $subject    = $zf_filter->getEscaped('BG_SUBJECT');
    $status    = $zf_filter->getEscaped('BG_STATUS');
    $fbgnumb    = $zf_filter->getEscaped('BG_NUMBER');
    $fbgregnumb     = $zf_filter->getEscaped('BG_REG_NUMBER');




    if ($filter == NULL && $clearfilter != 1) {
      //      $fUpdatedStart  = date("d/m/Y");
      //      $fUpdatedEnd  = date("d/m/Y");

      // echo "f0 c1";
    } else {
    }

    //$whereIn = [7, 20, 21, 22, 23];
    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrChangeType = array(
      '0' => 'New',
      //'2' => 'Indirect',
      '1' => 'Amendment Changes',
      '2' => 'Amendment Draft'
    );

    $this->view->arrChangeType = $arrChangeType;

    $arrWarrantyType = array(
      '1' => 'Full Cover',
      //'2' => 'Indirect',
      '2' => 'Line Facility',
      '3' => 'Insurance'
    );

    $this->view->arrWarrantyType = $arrWarrantyType;


    //print 
    $caseStatus = "(CASE BG_STATUS ";
    foreach ($arrStatus as $key => $val) {
      $caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseStatus .= " END)";

    $caseCounter = "(CASE COUNTER_WARRANTY_TYPE ";
    foreach ($arrWarrantyType as $key => $val) {
      $caseCounter .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseCounter .= " END)";

    $caseType = "(CASE CHANGE_TYPE ";
    foreach ($arrChangeType as $key => $val) {
      $caseType .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseType .= " END)";


    $select = $this->_db->select()
      ->from(
        array('A' => 'TEMP_BANK_GUARANTEE'),
        array(
          'A.BG_REG_NUMBER',
          'A.BG_SUBJECT',
          'C.CUST_NAME',
          'A.RECIPIENT_NAME',
          'B.BRANCH_NAME',
          'COUNTERTYPE' => $caseCounter,
          'CHANGETYPE' => $caseType,
          'STATUS' => $caseStatus,
          'A.BG_INSURANCE_CODE',
          'A.COUNTER_WARRANTY_TYPE',
          'A.CHANGE_TYPE',
          'A.BG_STATUS'
        )
      )
      ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
      ->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = A.CUST_ID', array('CUST_NAME'))
      //  ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_STATUS NOT IN (?)', '15', '16');
    //->order('BG_CREATED DESC');


    if ($status) {
      $select->where("BG_STATUS = ? ", $status);
    }

    if ($subject) {
      $subjectArr = explode(',', $subject);
      //  $select->where("UPPER(BG_SUBJECT)  like ?",'%'.$subjectArr.'%' );

      $select->where("BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
    }

    if ($fbgnumb)
    // {  $select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and SOURCE_ACCOUNT = ?) > 0", (string)$fAcctsrc);   }
    { //$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fAcctsrc.'%'));  
      $fAcctsrcArr = explode(',', $fbgnumb);
      $select->where("BG_NUMBER  LIKE ?", '%' . $fAcctsrcArr . '%');
    } // ACCSRC ?????

    if ($fbgregnumb) { //$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBenefsrc.'%'));  
      $fBenefsrcArr = explode(',', $fbgregnumb);
      $select->where("BG_REG_NUMBER  LIKE ?", '%' . $fBenefsrcArr . '%');
    }



    if ($csv || $pdf || $this->_request->getParam('print')) {

      $arr = $this->_db->fetchAll($select);

      foreach ($arr as $key => $val) {

        unset($arr[$key]['BG_INSURANCE_CODE']);
        unset($arr[$key]['COUNTER_WARRANTY_TYPE']);
        unset($arr[$key]['CHANGE_TYPE']);
        unset($arr[$key]['BG_STATUS']);
      }

      $header = Application_Helper_Array::simpleArray($fields, 'label');
      if ($csv) {

        $this->_helper->download->csv($header, $arr, null, $this->language->_('Customer List BG Ongoing'));
        Application_Helper_General::writeLog('RBGR', 'Download CSV Customer List BG Ongoing');
      } else if ($pdf) {
        $this->_helper->download->pdf($header, $arr, null, $this->language->_('Customer List BG Ongoing'));
        Application_Helper_General::writeLog('RBGR', 'Download PDF Customer List BG Ongoing');
      } else if ($this->_request->getParam('print') == 1) {

        $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Customer List BG Ongoing', 'data_header' => $fields));
      }
    }

    $select = $select->query()->fetchAll();
    $this->paging($select);

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    // $arrStatus = array('1' => 'Waiting for review',
    //                     '2' => 'Waiting for approve',
    //                     '3' => 'Waiting to release',
    //                     '4' => 'Waiting for bank approval',
    //                     '5' => 'Issued',
    //                     '6' => 'Expired',
    //                     '7' => 'Canceled',
    //                     '8' => 'Claimed by applicant',
    //                     '9' => 'Claimed by recipient',
    //                     '10' => 'Request Repair',
    //                     '11' => 'Reject',
    //                   );





    $arrType = array(
      1 => 'Standard',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;



    if (!empty($dataParamValue)) {

      $this->view->createdStart = $dataParamValue['BG_CREATED'];
      $this->view->createdEnd = $dataParamValue['BG_CREATED_END'];
      $this->view->efdateStart = $dataParamValue['BG_DATEFROM'];
      $this->view->efdateEnd = $dataParamValue['BG_DATEFROM_END'];



      unset($dataParamValue['BG_DATEFROM_END']);
      unset($dataParamValue['BG_CREATED_END']);

      //var_dump($dataParamValue);
      foreach ($dataParamValue as $key => $value) {
        //foreach($value as $ss => $vs){

        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[] = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[] = $key;
          $whereval[] = $value;
        }

        //}


      }
      //var_dump($wherecol);
      //var_dump($whereval);die;

      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }
  }
}
