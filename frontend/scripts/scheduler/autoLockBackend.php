<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

$db = Zend_Db_Table::getDefaultAdapter();

function userIdle($timeout_login)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	
	$userNoLogin = $db->select()
				  ->from('M_BUSER')
				  ->where('TIMESTAMPDIFF(day, date(BUSER_LASTLOGIN), date(now()) ) > ?', $timeout_login)
				  ->where('BUSER_STATUS != ?', '2')
				  ->query()
				  ->fetchall()
	;
	
	forcelock($userNoLogin,$timeout_login); #temporary deactive
}

function forcelock($userNoLogin,$timeout_login)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	
	try{
		$db->beginTransaction();
		
		$res = null;
		$string ='';
		foreach($userNoLogin as $row)
		{
			unset($where);
			$buserid = $row['BUSER_ID'];
			$lockreason = "User Suspended. No login after ".$timeout_login." day(s). Last login was at ".$row['BUSER_LASTLOGIN'];	
			$update = array(
				'BUSER_ISLOGIN'  	=> '0',
				'BUSER_STATUS'		=> '2',
				'BUSER_LOCKREASON'	=> $lockreason);		
			$where[] = "BUSER_ID = '".$buserid."'";
			$where[] = "BUSER_STATUS != '3'";
			$where[] = "BUSER_STATUS != '2'";
			$where[] = "BUSER_ID != 'SUPERADMIN01'";
			$res += $db->update('M_BUSER', $update, $where);
			$string .= "$buserid , "; 
		}	
		$db->commit();	
		Application_Helper_General::cronLog('autoLockBackend', $string, '1');	   
	}
	catch(Exception $e) 
	{
		$db->rollBack();
		$string = $e->getMessage();
		Application_Helper_General::cronLog('autoLockBackend', $string, '1');	   
	}
}

function autolock()
{
	$set = new Settings();
	$timeout_login = $set->getSetting('b_locknologinafter');
	userIdle($timeout_login);
}
autolock();
	
?>