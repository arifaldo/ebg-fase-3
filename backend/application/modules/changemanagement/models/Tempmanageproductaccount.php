<?php

class Changemanagement_Model_Tempmanageproductaccount extends Changemanagement_Model_Tempchanges {
	protected $_moduleId = 'BGR';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		
		//query from TEMP_ROOT_COMM
		$TEMP_PRODUCT = $this->dbObj->select()
									 ->from('TEMP_PRODUCT')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($TEMP_PRODUCT)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Product Type)';
			return false;
        }
        

        

		//insert to master table M_BACKEND GROUP
		$insertArr = array_diff_key($TEMP_PRODUCT,array('TEMP_ID'=>'','CHANGES_ID'=>''));

		
		$insertArr['LAST_APPROVED'] = new Zend_Db_Expr('now()');
		// $insertArr['CREATEDBY'] = $actor;
		$insertArr['LAST_APPROVEDBY'] = $actor;
		$M_PRODUCT = $this->dbObj->insert('M_PRODUCT',$insertArr);
        
		$jsonusernew = json_encode($M_PRODUCT);
     

       //$fullDesc = "Approve Changes Id : ".$this->_changeId." Suggest Data : ".$jsonusernew."  Data Name : ".$insertArr['PRODUCT_ID'];
       //Application_Helper_General::writeLog('PAIM', $fullDesc);
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor=NULL) {
		
		$TEMP_PRODUCT = $this->dbObj->select()
									 ->from('TEMP_PRODUCT')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);
									 
		if(!count($TEMP_PRODUCT)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Product Type)';
			return false;
        }
        
		//insert to master table M_BACKEND GROUP
		$insertArr = array_diff_key($TEMP_PRODUCT,array('TEMP_ID'=>'','CHANGES_ID'=>'','BGROUP_ID'=>''));
		 //var_dump($insertArr); exit();
		$M_PRODUCT = $this->dbObj->select()
									 ->from('M_PRODUCT')
									 ->where('PRODUCT_ID = ?',$TEMP_PRODUCT['PRODUCT_ID'])
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		$insertArr['LAST_APPROVED'] = new Zend_Db_Expr('now()');
		$insertArr['LAST_APPROVEDBY'] = $actor;
		$whereArr = array('PRODUCT_ID = ?'=>$TEMP_PRODUCT['PRODUCT_ID']);
		$updateM_PRODUCT = $this->dbObj->UPDATE('M_PRODUCT',$insertArr,$whereArr);
		

		$old_M_PRODUCT = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('M_PRODUCT')
									 ->where('PRODUCT_ID = ?',$TEMP_PRODUCT['PRODUCT_ID'])
									 );
		
	       $jsonOLD = json_encode($old_M_PRODUCT);
	      
	       $jsonNEW = json_encode($updateM_PRODUCT);

	      // $fullDesc = "Approve Changes Id : ".$this->_changeId." Old Data : ".$jsonOLD." Suggest Data : ".$jsonNEW."  Data Name : ".$insertArr['PRODUCT_ID'];
	      // Application_Helper_General::writeLog('PAIM', $fullDesc);
			//var_dump($fullDesc);die; 
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate() {
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate() {
		
	}
	
	public function approveSuspend($actor=NULL) {
		$backendGroup = $this->dbObj->select()
									 ->from('TEMP_BGROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($backendGroup)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group)';
			return false;
        }
        
        $whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendGroup,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$backendGroupOld = $this->dbObj->select()
									 ->from('M_BGROUP')
									 ->where('BGROUP_ID = ?',$backendGroup['BGROUP_ID'])
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);
									 
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BGROUP',$updateArr,$whereArr);
		
			$jsonuserold = json_encode($backendGroupOld);
	       
	       $jsonusernew = json_encode($backendGroupPrivilege);
	       

	       $fullDesc = "Approve Changes Id : ".$this->_changeId." Old Data : ".$jsonuserold."  Suggest Data : ".$jsonusernew."   Data Name : ".$insertArr['BGROUP_ID'];
	       Application_Helper_General::writeLog('CHCA', $fullDesc);

		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveUnsuspend($actor=NULL) {
		$backendGroup = $this->dbObj->select()
									 ->from('TEMP_BGROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($backendGroup)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group)';
			return false;
        }
        
        $whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendGroup,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BGROUP',$updateArr,$whereArr);
		
		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveDelete($actor=NULL) {
		$TEMP_PRODUCT = $this->dbObj->select()
									 ->from('TEMP_PRODUCT')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($TEMP_PRODUCT)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Product Type)';
			return false;
        }
        $whereArr = array('PRODUCT_ID = ?'=>$TEMP_PRODUCT['PRODUCT_ID']);
		//update to master table M_BACKEND GROUP
		/*$updateArr = array_diff_key($TEMP_PRODUCT,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$updateArr['LAST_APPROVED'] = new Zend_Db_Expr('now()');
		$updateArr['LAST_APPROVEDBY'] = $actor;
		*/
		$deleteM_PRODUCT = $this->dbObj->delete('M_PRODUCT',$whereArr);
		
		 $jsonOLD = json_encode($TEMP_PRODUCT);

	       //$fullDesc = "Approve Changes Id : ".$this->_changeId." Old Data : ".$jsonOLD." Data Name : ".$TEMP_PRODUCT['PRODUCT_ID'];
	       //Application_Helper_General::writeLog('PAIM', $fullDesc);
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

		$backendGroup = $this->dbObj->delete('TEMP_PRODUCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		$TEMP_PRODUCT = $this->dbObj->delete('TEMP_PRODUCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteSuspend() {
		$backendGroup = $this->dbObj->delete('TEMP_PRODUCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	public function deleteUnsuspend() {
		$backendGroup = $this->dbObj->delete('TEMP_PRODUCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	
	public function deleteDelete() {
		$backendGroup = $this->dbObj->delete('TEMP_PRODUCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;	
	}
}