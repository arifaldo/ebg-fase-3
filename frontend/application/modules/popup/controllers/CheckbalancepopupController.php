<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'General/SystemBalance.php';

class Popup_CheckbalancepopupController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti
	
	public function initController(){	
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction(){
		$this->view->custId = $this->_custIdLogin;
		$fields = array(
						'acctno'  		=> array(	'field' 	=> 'ACCT_NO',
													'label' 	=> $this->language->_('Account Number'),
													'sortable' 	=> true),
						'bene_name'  	=> array(	'field' 	=> 'ACCT_NAME',
													'label' 	=> $this->language->_('Account Name'),
													'sortable' 	=> true),
						'acct_name'  	=> array(	'field' 	=> 'ACCT_NAME',
													'label' 	=> $this->language->_('Account Name'),
													'sortable' 	=> true),
						'ccy'   		=> array(	'field'    => 'CCY_ID',
													'label'    	=> $this->language->_('CCY'),
													'sortable' 	=> true),
						'effective'   	=> array(	'field'    => '',
													'label'    => $this->language->_('Available Balance'),
													'sortable' => true),
						'status'   		=> array(	'field'    => '',
													'label'    => $this->language->_('Account Status'),
													'sortable' => true),
				);
		
		/*
			ACCOUNT_BANK_NAME 	= $systemBalance->getCoreAccountName();
			EFFECTIVEBALANCE 	= $systemBalance->getEffectiveBalance();
			
		*/
		
		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$resultaccounts = $CustomerUser->getAccounts();
		
		// $date = date("d/m/Y  H:i:s ");
		$date = date("Y-d-m H:i:s");
		
		
		if(is_array($resultaccounts)){
			foreach ($resultaccounts as $key => $row){
				
				$data[$key]['ACCT_NO'] 			= $row["ACCT_NO"];
				$data[$key]['ACCT_ALIAS_NAME'] 	= $row["ACCT_ALIAS_NAME"];
				$data[$key]['CCY_ID'] 			= $row["CCY_ID"];
				
				$systemBalance = new SystemBalance($this->_custIdLogin,$row["ACCT_NO"],Application_Helper_General::getCurrNum($row["CCY_ID"]));
				$rr = $systemBalance->checkBalance();
					
				$data[$key]['ACCOUNT_BANK_NAME'] = $systemBalance->getCoreAccountName();
				$data[$key]['EFFECTIVE_BALANCE'] = Application_Helper_General::displayMoney($systemBalance->getEffectiveBalance());
				$data[$key]['MESSAGE'] 			 = Application_Helper_General::convertDate($date, $this->view->displayDateTimeFormat);
				
			}
			
		}
		
		$this->view->nowDate	= Application_Helper_General::convertDate($date, $this->view->displayDateTimeFormat);
		$this->view->resultdata = $data;
	}
}
