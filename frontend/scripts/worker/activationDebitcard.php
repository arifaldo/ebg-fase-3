<?php
//die('here');
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'CMD/Payment.php';
require_once 'General/Settings.php';

	
	
	//$thisClass = $this;
	$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
		
		$datapers = array();
		$logName = 'Service_SMS::sendSMS';
		$pathFile = null;
		$printToDisplay = true;
		$logFileType = "ALL";
		
		
		
	
		$loggerService = SGO_Extendedmodule_Loggerservice_Service::getInstance();
		$logger = $loggerService->getLogger($logName, $pathFile, $printToDisplay, $logFileType);
		//var_dump($logger);
		//var_dump($queueService);die;
		$sms = array();
		$callback = function($sms) use ($logger,$queueService) {
			
			
			
			
			$request = json_decode($sms->body, true);
		 	
			//$Payment = new Payment($request['PS_NUMBER'], $request['CUST_ID'], 'System');
			
			$row = $request['card_no'];
			$acct_no = $request['acct_no'];
			$user_id = $request['user_id'];
			//var_dump($emailList); 
			//var_dump($sendTransfer);
			//var_dump($transactionData);die;


    	$Settings = new Settings();
		$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
		$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');

		$startSend = date("Y-m-d H:i:s");
        $config = Zend_Registry::get('config');
        $fromName = $templateEmailMasterBankAppName;
        $config = Zend_Registry::get('config');
         $username = $config['email']['username'];
          

		$dbObj =  Zend_Db_Table::getDefaultAdapter();
								$clientUser  =  new SGO_Soap_ClientUser();
								$requestva = array(
											'sender_id' => 'D360',
											'order_id' => trim($acct_no),
											'remark1' => null,
											'remark2' => 'ESPAY',
											'remark3' => null
											
										);
										$success = $clientUser->callapi('generateva',$requestva);
										$result  = $clientUser->getResult();
										$resultArr = json_decode(json_encode($result), true);
										$vanumb = '';
										if(!empty($resultArr['va_list'])){
											foreach($resultArr['va_list'] as $ky => $vl){
												if($vl['bank_code'] == '037'){
													$vanumb = $vl['va_number'];
												}
												$inpArr = array(
													'REG_NUMBER' => trim($row),
													'VA_NUMBER' => $vl['va_number'],
													'BANK_CODE' => $vl['bank_code'],
													'FEE' => $vl['fee']
												);
												$customerins = $dbObj->insert('T_VA_DEBIT',$inpArr);
											}
											
										}
								
								if($vanumb == ''){
									$db = Zend_Db_Table::getDefaultAdapter();
									$vacheck = $db->fetchAll($dbObj->select()
												->from(array('T' => 'T_VA_DEBIT'))
												->where('T.BANK_CODE = ?','037')
												->where('T.REG_NUMBER = ?', trim($row))
												);
									if(!empty($vacheck)){
										$vanumb = $vacheck['0']['VA_NUMBER'];
									}				
								}
								
								$requestactivation = array();
								$requestactivation['card_no'] = trim($row);
								$requestactivation['acct_no'] = $vanumb;
								$number = rand(0,99999999);
								$trace = str_pad($number,8,0,STR_PAD_LEFT);
								$requestactivation['trace'] = trim($trace);
								
								
								
								$success = $clientUser->callapi('activationcard',$requestactivation);
								$result  = $clientUser->getResult();
								if($result->ResponseCode == '00' || $result->ResponseCode == '0000'){
									
										$request = array();
										$cust_type = 'CORPORATE';
										$request['header']['sender_id'] = 'D360';
										$request['header']['signature'] = '';
										$request['data']['customer_type'] = $cust_type;
										$request['data']['bank_code'] = '999';
										$request['data']['cif'] = trim($acct_no);
										$request['data']['account_type'] = '10';
										$request['data']['account_number'] = trim($acct_no);
										$request['data']['account_currency'] = '000';
										$request['data']['account_name'] = 'ESPAY';
										$request['data']['email'] = NULL;
										$request['data']['phone_number'] = NULL;
										$request['data']['birthdate'] = NULL;
										$request['data']['address'] = NULL;
										$request['data']['city'] = NULL;
										$request['data']['state'] = NULL;
										$clientUser  =  new SGO_Soap_ClientUser();
										
										$success = $clientUser->callapi('registeraccount',$request);
										$result  = $clientUser->getResult();
										var_dump($result->header->response_code);
										if($result->header->response_code == '00' || $result->header->response_code == '0000'){
										
										$var['DEBIT_NUMBER'] = trim($row);
										$var['ACCT_NO'] = trim($acct_no);
										$var['DEBIT_STATUS'] = '3';
										
										$var['DEBIT_ACTIVATEDBY'] = $user_id;
										$var['DEBIT_ACTIVATED'] = new Zend_Db_Expr('now()');
										$var['IS_ACTIVATED'] = 1;
										
										// echo "<pre>";
										// print_r($var);die;
										$dbObj->insert("T_DEBITCARD",$var);
										
										
										
										
										$requestpin = array();
										$requestpin['card_no'] = trim($row);
										$first_pin = $Settings->getSetting('first_pin');
										$requestpin['first_pin'] = trim($first_pin);
										$number = rand(0,99999999);
										$trace = str_pad($number,8,0,STR_PAD_LEFT);
										$requestpin['trace'] = trim($trace);
										
										
										$success = $clientUser->callapi('firstpin',$requestpin);
										$result  = $clientUser->getResult();
									
									}
								
								}
		
			 
		}; 

	                
		
		//$queueConsumer = '';
		//var_dump($queueConsumer);
		$queueConsumer = $queueService->getQueueByProfileName("DEBITCARD_ACTIVATION");
		
		try {
			$queueConsumer->waitAndProcess($callback);
		} catch (Exception $e) {
			$param = [];
			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e->getMessage(),NULL,FALSE),$param);
			SGO_Helper_Supervisor::doExitByExitCodeName("RESTART");
		}

?>
