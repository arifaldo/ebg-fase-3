<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Mobile_DeactiveController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
	public function indexAction()
	{
	   $this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
		$model = new mobile_Model_Mobile();
		
		$filter = $this->_getParam('filter');
		// print_r($filter);die;
		if($filter == 'DEACTIVATE'){
			$auth = Zend_Auth::getInstance()->getIdentity();
    	  	$this->_userIdLogin   = $auth->userIdLogin;
    	  	$this->_custIdLogin   = $auth->custIdLogin;

			$where['CUST_ID = ?'] = $this->_custIdLogin;
			$where['USER_ID = ?'] = $this->_custIdLogin;
			$updateArr = array('USER_ISACTIVED' => 0);
			$this->_db->update('M_USER',$updateArr,$where);
		}

		if($this->_request->isPost() )
		{
			$this->_redirect('/mobile/deactivate/id/1');
		}
		//validate parameters before passing to view and query
		

		//get filtering param
		
		
		
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
	
		$select = $model->getData($fParam,$sortBy,$sortDir,$filter);		
		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['ID']);
		}
		//--------konfigurasicsv dan pdf---------
	    
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}
}