<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgreport_IndexController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $filter_clear     = $this->_getParam('clearfilter');

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        $fields = array(
            'regno'     => array('field'    => 'BG_NUMBER',
                                  'label'    => $this->language->_('BG No# / Subject'),
                                ), 
            'applicant'     => array('field'    => 'BG_Applicant',
                                  'label'    => $this->language->_('Applicant'),
                                ),
            'idccy'     => array('field'    => 'CCYID',
                                  'label'    => $this->language->_('CCY '),
                                ),
            'bgamount'  => array('field'    => 'BG_AMOUNT',
                                  'label'    => $this->language->_('BG Amount'),
                                ),
            'startdate' => array('field' => 'TIME_PERIOD_START',
                                   'label' => $this->language->_('Date From'),
                                ),
            'enddate'   => array('field'    => 'TIME_PERIOD_END',
                                  'label'    => $this->language->_('Date To'),
                                ),
        );
    
    //  $selectbg = $this->_db->select()
    //                          ->from(array('A' => 'T_BANK_GUARANTEE'),array('*'))
    //                          ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
    //                          ->query()->fetchAll();

    $filterlist = array('BG_NUMBER','BG_SUBJECT','BG_PERIOD');

    $this->view->filterlist = $filterlist;
              
    $page    = $this->_getParam('page');        
    
    $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    
    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
    
    $this->view->currentPage = $page;
    $this->view->sortBy      = $sortBy;
    $this->view->sortDir     = $sortDir;

    $select2 = $this->_db->select()
                          ->from(array('BG' => 'T_BANK_GUARANTEE'),array('*'))
                          ->join(array('C' => 'M_CUSTOMER'), 'BG.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'))
                          ->where('BG.BG_INSURANCE_CODE ='.$this->_db->quote((string)$this->_custIdLogin))
                          ->order('BG_UPDATED DESC');
                                 
    $conf = Zend_Registry::get('config');

    $this->view->bankname = $conf['app']['bankname'];
    
      $config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];
		
		$arrStatus = array_combine(array_values($BgCode),array_values($BgType));
         
        $this->view->arrStatus = $arrStatus;
    
    $arrType = array(
          1 => 'Standart',
          2 => 'Custom'
    );
    
    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;

    $filterArr = array(
                      'BG_NUMBER'   => array('StripTags','StringTrim','StringToUpper'),                                               
                      'BG_SUBJECT'      => array('StripTags','StringTrim','StringToUpper')
                          );
                            
        $dataParam = array("BG_NUMBER","BG_SUBJECT","BG_PERIOD");
        $dataParamValue = array();

        foreach ($dataParam as $dtParam)
        {          
          if(!empty($this->_request->getParam('wherecol'))){
            $dataval = $this->_request->getParam('whereval');
              foreach ($this->_request->getParam('wherecol') as $key => $value) {
                if($dtParam==$value){
                  $dataParamValue[$dtParam] = $dataval[$key];
                }
              }

          }
        }

        if(!empty($this->_request->getParam('efdate'))){
          $efdatearr = $this->_request->getParam('efdate');
          $dataParamValue['BG_PERIOD'] = $efdatearr[0];
          $dataParamValue['BG_PERIOD_END'] = $efdatearr[1];
        }
                          
        $validator = array(                  
         'BG_NUMBER'  => array(),  
         'BG_SUBJECT'      => array(),              
         'BG_PERIOD'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
         'BG_PERIOD_END'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          );
       
        $zf_filter   = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);

        if ($zf_filter->isValid()) {
          $filter     = TRUE;
        }
        
         
        $bgNubmer  = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));   
        $bgSubject  = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));     
        $datefrom    = html_entity_decode($zf_filter->getEscaped('BG_PERIOD'));
        $dateto    = html_entity_decode($zf_filter->getEscaped('BG_PERIOD_END'));
       
       
        if($filter == null)
        { 
          $datefrom = (date("d/m/Y"));
          $dateto = (date("d/m/Y"));
          $this->view->fDateFrom  = (date("d/m/Y"));
          $this->view->fDateTo  = (date("d/m/Y"));
        }
        
        if($filter_clear == '1'){
          $this->view->fDateFrom  = '';
          $this->view->fDateTo  = '';
          $datefrom = '';
          $dateto = '';
          
        }
      
        if($filter == null || $filter ==TRUE)
        {
          $this->view->fDateFrom = $datefrom;
          $this->view->fDateTo = $dateto;  
          if(!empty($datefrom))
          {
            $FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
            $datefrom  = $FormatDate->toString($this->_dateDBFormat); 
          }
                
          if(!empty($dateto))
          {
              $FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
              $dateto    = $FormatDate->toString($this->_dateDBFormat);
          }
      
          if(!empty($datefrom) && empty($dateto))
              $select2->where("BG.TIME_PERIOD_START >= ".$this->_db->quote($datefrom));
                
          if(empty($datefrom) && !empty($dateto))
              $select2->where("BG.TIME_PERIOD_END <= ".$this->_db->quote($dateto));
                  
          if(!empty($datefrom) && !empty($dateto))
              $select2->where("BG.TIME_PERIOD_START >= ".$this->_db->quote($datefrom)." and BG.TIME_PERIOD_END <= ".$this->_db->quote($dateto));
        }

      
        if($filter == TRUE)
        {   
          
          if($bgNubmer!=null)
          {         
            $this->view->bgNubmer = $bgNubmer;
            $select2->where("BG.BG_NUMBER LIKE ".$this->_db->quote('%'.$bgNubmer.'%'));
          }
          
          if($bgSubject != null)
          {
             $this->view->bgSubject = $bgSubject;
               $select2->where("BG.BG_SUBJECT LIKE ".$this->_db->quote('%'.$bgSubject.'%'));
          }
        }

        $select2->order($sortBy.' '.$sortDir);
		//echo $select2;die;
      
        $txt = $this->_getParam('txt');
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
        if($txt || $csv || $pdf || $this->_request->getParam('print'))
        {
          $arr = $this->_db->fetchAll($select2);
          foreach ($arr as $key => $value)
          {
            unset($arr[$key]['CUST_ID']);
            //echo $key;
            $arr[$key]["PS_REQUESTED"] = Application_Helper_General::convertDate($value["PS_REQUESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

            $arr[$key]["SOURCE_ACCOUNT"] = $arr[$key]["SOURCE_ACCOUNT"]." [".$arr[$key]["SOURCE_ACCOUNT_CCY"]."] / ".$arr[$key]["SOURCE_ACCOUNT_NAME"];
            unset($arr[$key]["SOURCE_ACCOUNT_CCY"]);
            unset($arr[$key]["SOURCE_ACCOUNT_NAME"]);

            $arrProductType = array('1'=>'Cheque','2'=>'Bilyet Giro');
            $arr[$key]["PRODUCT_TYPE"] = $arrProductType[$arr[$key]["PRODUCT_TYPE"]];

            unset($arr[$key]['PS_APPROVEBY']);

            $requestby = $arr[$key]['PS_REQUESTBY'];
            unset($arr[$key]['PS_REQUESTBY']);

            $arr[$key]["ADMIN_FEE"] = "IDR ".Application_Helper_General::displayMoney($arr[$key]["ADMIN_FEE"]);
            unset($arr[$key]["SUGGEST_STATUS"]);

            $arr[$key]["PS_REQUESTBY"] = $requestby;
            
          }
		  
		   if($txt)
          {
            Application_Helper_General::writeLog('DTRX','Download TXT Transaction Report');
			$arrTxt = $this->_db->fetchAll($select2);
			
			
			$txtData = array();
			foreach($arrTxt as $ky => $vl){
				$selectcity = $this->_db->select()
                          ->from(array('M_CITY'),array('*'))
						  ->where('CITY_NAME = ?',$vl['RECIPIENT_CITY']);
				$city = $this->_db->fetchRow($selectcity);
				if(empty($city)){
					$citycode = $city['CITY_CODE'];
				}else{
					$citycode = '0300';
				}
						  
				$txtData[$ky]['FLAG'] = 'D';
				$txtData[$ky]['BG_NUMBERA'] = $vl['BG_NUMBER'].'A';
				$txtData[$ky]['BG_NUMBER'] = $vl['BG_NUMBER'];
				$txtData[$ky]['CUST_NAME'] = $vl['CUST_NAME'];
				$txtData[$ky]['SEGMENT'] = 'F05';
				$txtData[$ky]['AGUNAN'] = '1';
				$txtData[$ky]['KODEAGUNAN'] = '252';
				$txtData[$ky]['PERINGKATAGUNAN'] = '';
				$txtData[$ky]['KODEPERINGKAT'] = '';
				$txtData[$ky]['JENISPENGIKAT'] = '99';
				$txtData[$ky]['TGLPENGIKAT'] = str_replace('-','',$vl['TIME_PERIOD_START']);
				$txtData[$ky]['RECIPIENT_NAME'] = $vl['RECIPIENT_NAME'];
				$txtData[$ky]['GUARANTEE_TRANSACTION'] = $vl['GUARANTEE_TRANSACTION'];
				$txtData[$ky]['RECIPIENT_ADDRES'] = $vl['RECIPIENT_ADDRES'];
				$txtData[$ky]['CITY_CODE'] = $$citycode;
				$txtData[$ky]['BG_AMOUNT'] = $vl['BG_AMOUNT'];
				$txtData[$ky]['BG_AMOUNTLK'] = $vl['BG_AMOUNT'];
				$txtData[$ky]['DATELK'] = str_replace('-','',$vl['TIME_PERIOD_START']);
				$txtData[$ky]['NILAIPENILAI'] = '';
				$txtData[$ky]['NAMAPENILAI'] = '';
				$txtData[$ky]['DATEPENILAI'] = '';
				$txtData[$ky]['PARIPASUSTATS'] = 'T';
				$txtData[$ky]['PARIPASUPECENT'] = '';
				$txtData[$ky]['STATUSKREDIT'] = 'T';
				$txtData[$ky]['STATUSINSURANCE'] = 'Y';
				$txtData[$ky]['DESC'] = '';
				$txtData[$ky]['KACAB'] = '70';
				$txtData[$ky]['OPERASIDATA'] = 'C';
				
				
			}
			
            $this->_helper->download->txtblank(null,$txtData,null,'Bank Guarantee List');
          }
          
		  
    
          $header = Application_Helper_Array::simpleArray($fields, 'label');
          if($csv)
          {
            Application_Helper_General::writeLog('DTRX','Download CSV Transaction Report');
            $this->_helper->download->csv($header,$arr,null,'Executed Transaction');
          }
          
          if($pdf)
          {
            Application_Helper_General::writeLog('DTRX','Download PDF Transaction Report');
            $this->_helper->download->pdf($header,$arr,null,'Executed Transaction');
          }
          if($this->_request->getParam('print') == 1){
            
                      $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields));
              }
      }
      else
      {
        Application_Helper_General::writeLog('DTRX','View Transaction Report');
      }
  
        $this->view->fields = $fields;
        $this->view->filter = $filter;
        $this->paging($select2);

        if(!empty($dataParamValue)){
          
				$this->view->efdateStart = $dataParamValue['BG_PERIOD'];
				$this->view->efdateEnd = $dataParamValue['BG_PERIOD_END'];

          foreach ($dataParamValue as $key => $value) {
            $duparr = explode(',',$value);
            if(!empty($duparr)){
              
              foreach($duparr as $ss => $vs){
                $wherecol[]	= $key;
                $whereval[] = $vs;
              }
            }else{
                $wherecol[]	= $key;
                $whereval[] = $value;
            }
          }
          $this->view->wherecol     = $wherecol;
          $this->view->whereval     = $whereval;
        }
  
  }

    

}

