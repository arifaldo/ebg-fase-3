<?php
Class bankforexlimit_Model_Bankforexlimit {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getData($fParam,$sortBy = null,$sortDir = null,$filter = null)
    {
		$select = $this->_db->select()
					        ->from(array('A' => 'M_FOREX'),array('CCY','BUY_AMOUNT','SELL_AMOUNT','CREATED','CREATEDBY','UPDATED','UPDATEDBY')); 
		
		// if($filter == 'Set Filter')
		
		if( !empty($sortBy) && !empty($sortDir) )
			$select->order($sortBy.' '.$sortDir);
       return $this->_db->fetchall($select);
    }
    
    public function getCCYlist()
    {
    	$data = $this->_db->select()->distinct()
    	->from(array('M_MINAMT_CCY'),array('CCY_ID'))
    	->order('CCY_ID ASC')
    	-> query() ->fetchAll();
    
    	return $data;
    }
  
    public function getDetail($CCY_ID)
    {
		$data = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_FOREX')) 
									 ->where("CCY=?", $CCY_ID)
							);
		
       return $data;
    }
  
    public function insertData($content)
    {
		$this->_db->insert("M_FOREX",$content);
    }
  
    public function updateData($PRODUCT_ID,$content)
    {
		$whereArr  = array('CCY = ?'=>$PRODUCT_ID);
		$this->_db->update('M_FOREX',$content,$whereArr);
    }
  
    public function deleteData($PRODUCT_ID)
    {
//		$this->_db->delete('M_PRODUCT_TYPE','PRODUCT_ID = ?', $PRODUCT_ID);
		$this->_db->delete('M_FOREX','CCY = '.$this->_db->quote($PRODUCT_ID));
    }
}