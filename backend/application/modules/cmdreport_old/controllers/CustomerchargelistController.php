<?php
require_once 'Zend/Controller/Action.php';

//class Cmdreport_customeruserlistController extends Application_Main
class Cmdreport_customerchargelistController extends customer_Model_Customer
{	
	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{			
		
		$custArr  = Application_Helper_Array::listArray($this->getAllCustomer(),'CUST_ID','CUST_ID');
		$this->view->custArr = $custArr;
		
		$fields1 = array(
							'custid'  		=> array	(	
															'field' => 'custid',
															'label' => 'Company Code / Company Name',
															'sortable' => true
														),
							'acctno'  		=> array	(
															'field' => 'acctno',
															'label' => 'Account Number',
															'sortable' => true
														),
							'chargetype'  	=> array	(
															'field' => 'chargetype',
															'label' => 'Charge Type',
															'sortable' => true
														),
						);
		
		$fieldsbetween = array(
			
							'trafrom'  	=> array	(
															'field' => 'trafrom',
															'label' => 'From',
															'sortable' => true
														),
							'trato'  	=> array	(
															'field' => 'trato',
															'label' => 'To',
															'sortable' => true
														)
						);
		
		$fields2 = array(
							'ccy'  			=> array	(
															'field' => 'ccy',
															'label' => 'Currency',
															'sortable' => true
														),
							'chargeamount'  => array	(
															'field' => 'chargeamount',
															'label' => 'Charge Amount',
															'sortable' => true
														)
						);
						
		$ff = $fields1 + $fieldsbetween;
		$fieldsall = $ff + $fields2;
		
		$fields = $fields1 + $fields2;
		
		// echo "<pre>";
		// print_r($fields);
		// print_r($fieldsall);
		// die;
		
		//validasi page, jika input page bukan angka               
		$page 	= $this->_getParam('page');
		$csv 	= $this->_getParam('csv');
		$pdf 	= $this->_getParam('pdf');
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$filterArr = array('COMPANYID'     	=> array('StripTags','StringTrim','StringToUpper'),
						   'COMPANYNAME'   	=> array('StripTags','StringTrim','StringToUpper'),
						   'ACCOUNTSOURCE'  => array('StripTags','StringTrim','StringToUpper')
						  );
		
		// if POST value not null, get post, else get param
		$dataParam = array("COMPANYID","COMPANYNAME","ACCOUNTSOURCE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						'COMPANYID' 	=> array(array('InArray', array('haystack' => array_keys($custArr)))),
						'COMPANYNAME' 	=> array(),	
						'ACCOUNTSOURCE' => array(),								
						);
		
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter 	= $this->_getParam('filter');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');

		// $this->view->currentPage = $page;
		
		$fCOMPANYID  	= html_entity_decode($zf_filter->getEscaped('COMPANYID'));
		$fCOMPANYNAME  	= html_entity_decode($zf_filter->getEscaped('COMPANYNAME'));
		$fACCOUNTSOURCE = html_entity_decode($zf_filter->getEscaped('ACCOUNTSOURCE'));
		
		// Select data from M_CHARGES_MONTHLY
		/*
			'chargetype'=> new Zend_Db_Expr("CASE 	MONTHLYFEE_TYPE 
													WHEN '3' THEN 'Transaction per Account' 
													WHEN '4' THEN 'Transaction per Company'	
											END"),
		*/
	
		$selectchargesmontly = $this->_db->select()
									->from(		array(	'C' =>'M_CHARGES_MONTHLY'), array())
									->joinLeft(	array(	'M'	=>'M_CUSTOMER'),'C.CUST_ID = M.CUST_ID AND C.MONTHLYFEE_TYPE = M.CUST_MONTHLYFEE_TYPE', array())
									->joinLeft(	array(	'N'				=>'M_CUSTOMER_ACCT'),'C.CHARGES_ACCT_NO = N.ACCT_NO ',
												array(	'custid'		=>'M.CUST_ID',
														'custname'		=>'M.CUST_NAME',
														'acctno'		=>'C.CHARGES_ACCT_NO',
														'chargetype'	=> new Zend_Db_Expr("CASE 	C.MONTHLYFEE_TYPE 
																									WHEN '3' THEN 'Transaction per Account' 
																									WHEN '4' THEN 'Transaction per Company'	
																							END"),																								
														'trafrom'		=>'C.TRA_FROM',
														'trato'			=>'C.TRA_TO',
														
														'chargeamount'	=>'C.AMOUNT',
														'ccy'			=>'N.CCY_ID'));
		
		// Select data from M_ADMINFEE_MONTHLY
		/*
			'chargetype'=> new Zend_Db_Expr("CASE 	MONTHLYFEE_TYPE 
													WHEN '1' THEN 'Administration fee per Account' 
													WHEN '2' THEN 'Administration fee per company'	
											END"),
		*/
		
		$selectadminfee = $this->_db->select()
									->from(		array(	'C' =>'M_ADMFEE_MONTHLY'), array())
									->joinLeft(	array(	'M'	=>'M_CUSTOMER'),'C.CUST_ID = M.CUST_ID AND C.MONTHLYFEE_TYPE = M.CUST_MONTHLYFEE_TYPE', array())
									->joinLeft(	array(	'N'	=>'M_CUSTOMER_ACCT'),'C.CHARGES_ACCT_NO = N.ACCT_NO ',
												array(	'custid'		=>'M.CUST_ID',
														'custname'		=>'M.CUST_NAME',
														'acctno'		=>'C.CHARGES_ACCT_NO',
														'chargetype'	=> new Zend_Db_Expr("CASE 	C.MONTHLYFEE_TYPE 
																							WHEN '1' THEN 'Administration fee per Account' 
																							WHEN '2' THEN 'Administration fee per company'	
																						END"),
														'trafrom'		=>	new Zend_Db_Expr("''"),
														'trato'			=>	new Zend_Db_Expr("''"),
														'chargeamount'	=>'C.AMOUNT',
														'ccy'			=>'N.CCY_ID'));
		
		// Select data from M_CHARGES_OTHER
		/*
			'chargetype'=> new Zend_Db_Expr("CASE 	CHARGES_TYPE 
													WHEN '1' THEN 'RTGS' 
													WHEN '2' THEN 'SKN'	
													WHEN '3' THEN 'Transfer Fee'	
													WHEN '4' THEN 'Full Amount Fee'	
													WHEN '5' THEN 'Provision Fee'	
											END"),
		*/
	
		$selectother = $this->_db->select()
								->from(		array(	'C'=>'M_CHARGES_OTHER'),array())
								->joinLeft(	array(	'M'=>'M_CUSTOMER'),'C.CUST_ID = M.CUST_ID',
											array(	'custid'		=>'M.CUST_ID',
													'custname'		=>'M.CUST_NAME',
													'acctno'		=>	new Zend_Db_Expr("''"),								
													'chargetype'	=> new Zend_Db_Expr("CASE 	C.CHARGES_TYPE 
																						WHEN '1' THEN 'RTGS' 
																						WHEN '2' THEN 'SKN'	
																						WHEN '3' THEN 'Transfer Fee'	
																						WHEN '4' THEN 'Full Amount Fee'	
																						WHEN '5' THEN 'Provision Fee'	
																					END"),
													'trafrom'		=>	new Zend_Db_Expr("''"),
													'trato'			=>	new Zend_Db_Expr("''"),								
													'chargeamount'	=>'C.CHARGES_AMT',
													'ccy'			=>'C.CHARGES_CCY',
													));
		
		// Select data from M_CHARGES_WITHIN
		/*
			'chargetype'=> new Zend_Db_Expr("CASE 	BUSINESS_TYPE 
													WHEN '1' THEN 'Inhouse' 
													WHEN '2' THEN 'Disbursement'	
													WHEN '3' THEN 'Settlement'	
													
											END"),
			
			
			
		*/
	
		$selectwithin = $this->_db->select()
								->from(		array(	'C'=>'M_CHARGES_WITHIN'),array())
								->joinLeft(	array(	'M'=>'M_CUSTOMER'),'C.CUST_ID = M.CUST_ID',array())
								->joinLeft(	array(	'N'=>'M_CUSTOMER_ACCT'),'C.ACCT_NO = N.ACCT_NO',
											array(	'custid'		=>'M.CUST_ID',
													'custname'		=>'M.CUST_NAME',
													'acctno'		=>'C.ACCT_NO',
													'chargetype'	=> new Zend_Db_Expr("CASE 	C.BUSINESS_TYPE 
																						WHEN '1' THEN 'Inhouse' 
																						WHEN '2' THEN 'Disbursement Within'	
																						WHEN '3' THEN 'Settlement'	
																					END"),
													'trafrom'		=>	new Zend_Db_Expr("''"),
													'trato'			=>	new Zend_Db_Expr("''"),	
													'chargeamount'	=>'C.AMOUNT',
													'ccy'			=>'N.CCY_ID')
											);
		
		// Select data from M_CHARGES_PARTNER		
		// $selectpartner = $this->_db->select()
									// ->from( array(	'C'	=>'M_CHARGES_PARTNER'),array())
									// ->joinLeft(	array('M'=>'M_CUSTOMER'),'C.CUST_ID = M.CUST_ID',
												// array(	'custid'		=>'M.CUST_ID',
														// 'custname'		=>'M.CUST_NAME',
														// 'acctno'		=>new Zend_Db_Expr("''"),				
														// 'chargetype'	=>'C.FEATURE_ID',
														// 'trafrom'		=>	new Zend_Db_Expr("''"),
														// 'trato'			=>	new Zend_Db_Expr("''"),	
														// 'chargeamount'	=>'C.CHARGES_AMT',
														// 'ccy'			=>'C.CHARGES_CCY'
														
														
														// ));
		
		
		
		$selectchargesmontlyString 	= $selectchargesmontly	->__toString();
		$selectadminfeeString 		= $selectadminfee		->__toString();
		$selectotherString 			= $selectother			->__toString();
		$selectwithinString 		= $selectwithin			->__toString();
		// $selectpartnerString 		= $selectpartner		->__toString();
		
		$unionquery = $this->_db->select()
								->union(array($selectchargesmontlyString,$selectadminfeeString,$selectotherString,$selectwithinString));
		
		$select = $this->_db->select()
							->from(($unionquery),array('*'));
		
		if($fCOMPANYID)		$select->where('custid = '.$this->_db->quote(strtoupper($fCOMPANYID))); 
		if($fCOMPANYNAME)	$select->where('custname LIKE '.$this->_db->quote(strtoupper('%'.$fCOMPANYNAME.'%')));
		if($fACCOUNTSOURCE)	$select->where('acctno LIKE '.$this->_db->quote(strtoupper('%'.$fACCOUNTSOURCE.'%')));
		
		//echo "<pre>";
		//echo $selectwithin->__toString();
		//echo "<br />";
		//echo $selectpartner->__toString(); die;
		//echo $select->__toString(); die;
		
		$select->order($sortBy.' '.$sortDir);
		$dataSQL 	= $this->_db->fetchAll($select);
		$dataCount 	= count($dataSQL); // utk validasi max row csv
		
		if(!$csv && !$pdf){
		
			$this->paging($dataSQL);
			$dataSQL = $this->view->paginator;
		}
		else{
			
			$header = array('Company Code / Company Name','Account Number','Charge Type',
						'Number of Transfer from','Number of Transfer to','Currency','Charge Amount'
						);
		}
		
		if(!empty($dataSQL) && count($dataSQL) > 0){
			foreach ($dataSQL as $d => $dt){	
			
				foreach ($fieldsall as $key => $field){
					
					$value = $dt[$key];
					if ($key == "chargeamount" && !$csv){	$value = Application_Helper_General::displayMoney($value); }
					else if ($key == "custid"){ $value = $dt['custid'].' / '.$dt['custname'];	}				
					
					else if ($key == "trato" && !$csv){
						if($dt['trato'] != ""){ $value = $dt['trato']; }
						else{ $value = "-"; }
					}
					else if ($key == "trafrom" && !$csv){
						if($dt['trafrom'] != ""){ $value = $dt['trafrom']; }
						else{ $value = "-"; }
					}
					
					$value = ($value == "" && !$csv)? "&nbsp;": $value;
					
					$data[$d][$key] = $value;
				}
			
			}
		}
		else{ $data = array(); }
		
		//Zend_Debug::Dump($data);
		
		if($csv)
		{	
			$this->_helper->download->csv($header,$data,null,'Customer Charge List');  
			Application_Helper_General::writeLog('CCRL','Download CSV Customer Charge List report');
		
		}
		elseif($pdf)
		{
			$this->_helper->download->pdf($header,$data,null,'Charge Charge List');  
			Application_Helper_General::writeLog('CCRL','Download PDF Customer Charge List report');
		
		}
		else
		{	
			$stringParam = array('COMPANYID'		=> $fCOMPANYID,
								 'COMPANYNAME'		=> $fCOMPANYNAME,
								 'ACCOUNTSOURCE'	=> $fACCOUNTSOURCE,
							    );	
							    
			$this->view->COMPANYID 		= $fCOMPANYID;
			$this->view->COMPANYNAME 	= $fCOMPANYNAME;
			$this->view->ACCOUNTSOURCE 	= $fACCOUNTSOURCE;
			
			$this->view->data 			= $data;
			$this->view->fields1 		= $fields1;
			$this->view->fields2		= $fields2;
			$this->view->filter 		= $filter;
	        $this->view->sortDir 		= $sortDir;
	        $this->view->sortBy 		= $sortBy;
	        $this->view->dataCount 		= $dataCount;
			
			$this->view->custArr 		= $custArr;
			
			Application_Helper_General::writeLog('CCRL','View Customer Charge List Report');
			
		}
	}

}
