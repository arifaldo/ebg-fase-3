<?php
/**
 * Temprootcommunity model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempbgapprovalmatrix extends Changemanagement_Model_Tempchanges {
	protected $_moduleId = 'AUM';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {

		$boundary = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_APP_BGBOUNDARY')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
		
		// Zend_Debug::dump($boundary);die;
		
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		/*$select = $this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);	*/			 
		
		$boundary = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_APP_BGBOUNDARY')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
		
		// Zend_Debug::dump($boundary);die;
		
		if($boundary)
		{
			$whereArr2 = array('1=1');
			$delete2 = $this->dbObj->delete('M_APP_BGBOUNDARY_GROUP',$whereArr2);
			$whereArr1 = array('1=1');
			$delete1 = $this->dbObj->delete('M_APP_BGBOUNDARY',$whereArr1);

			$whereArr2 = array('1=1');
			$delete3 = $this->dbObj->delete('M_APP_GROUP_BUSER',$whereArr2);
			
			// Zend_Debug::dump($delete2.' '.$delete1.' '.$custid);die;
			// echo "<pre>";
			// print_r($boundary);die;
			foreach($boundary as $list)
			{
				// print_r($list);die;
				$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>'','BOUNDARY_ID'=>''));
	       		$insertArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
				$insertArr['CREATED'] = new Zend_Db_Expr('now()');
				$insertArr['CREATEDBY'] = $actor;
				// INsert To M_APP_BOUNDARY
					$insert = $this->dbObj->insert('M_APP_BGBOUNDARY',$insertArr);	
				$lastId = $this->dbObj->query('SELECT @@identity AS LASTID')->fetch(Zend_Db::FETCH_ASSOC);
				$last = $lastId['LASTID'];
				
				$query = $this->dbObj->select()
									 ->from('TEMP_APP_BGBOUNDARY_GROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->where('ROW_INDEX = ?',$list['ROW_INDEX']);
// Zend_Debug::dump($boundary);
// Zend_Debug::dump($query->__toString());
// Zend_Debug::dump($list);

				// print_r($query);die;
									 // echo $query;die;
				$group = $this->dbObj->fetchAll($query);
				// Zend_Debug::dump($group);die;
				foreach($group as $grouplist)
				{
					// Zend_Debug::dump($last);die;
					$groupinsertArr = array_diff_key($grouplist,array('TEMP_ID'=>'','CHANGES_ID'=>'','BOUNDARY_ID'=>''));
					$groupinsertArr['BOUNDARY_ID'] = $last;
					try {
						// die('here');
						// echo '<pre>';
						// print_r($groupinsertArr);
						$groupinsert = $this->dbObj->insert('M_APP_BGBOUNDARY_GROUP',$groupinsertArr);	
					} catch (Exception $e) {
						// print_r($e);die;
					}
					
				}
			}


				


		}

		$queryg = $this->dbObj->select()
									 ->from('TEMP_APP_GROUP_BUSER')
									 ->where('CHANGES_ID = ?',$this->_changeId);
									 // ->where('ROW_INDEX = ?',$list['ROW_INDEX']);
// Zend_Debug::dump($boundary);
// Zend_Debug::dump($query->__toString());
// Zend_Debug::dump($list);


				$groupuser = $this->dbObj->fetchAll($queryg);
				// Zend_Debug::dump($groupuser);die;
				foreach($groupuser as $grouplist)
				{
					// Zend_Debug::dump($last);die;
					$groupinsertArr = array_diff_key($grouplist,array('TEMP_ID'=>'','CHANGES_ID'=>''));
					// $groupinsertArr['BOUNDARY_ID'] = $last;
					try {
						// die('here');
						// echo '<pre>';
						// print_r($groupinsertArr);
						$groupinsert = $this->dbObj->insert('M_APP_GROUP_BUSER',$groupinsertArr);	
					} catch (Exception $e) {
						// print_r($e);
					}
					
				}
		// die('12');
		$deleteChanges  = $this->deleteEdit();
		// if(!$deleteChanges)
			// return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate() {
		
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate() {
		
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		// die('here');
		$delete = $this->dbObj->delete('TEMP_APP_BGBOUNDARY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$groupdelete = $this->dbObj->delete('TEMP_APP_BGBOUNDARY_GROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		$groupdelete = $this->dbObj->delete('TEMP_APP_GROUP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteDelete() {
		//will never be called
	}
}