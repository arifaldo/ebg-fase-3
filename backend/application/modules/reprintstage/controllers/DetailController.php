<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class Reprintstage_detailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');

        $stamp_fee = $settings->getSetting('stamp_fee');
        $this->view->stamp_fee = $stamp_fee;
        $adm_fee = $settings->getSetting('adm_fee');
        $this->view->adm_fee = $adm_fee;

        $this->view->systemType = $system_type;
        $this->view->ProvFee = 2000000;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;


        // $numb = $this->_getParam('bgnumb');

        // decrypt numb
        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token     = $rand;
        $this->view->token = $sessionNamespace->token;

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;

        $this->view->bg_reg_number_encrypt = urlencode($this->_getParam('bgnumb'));

        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        $this->view->encrypt_numb = urlencode($this->_getParam('bgnumb'));

        // others attachment

        $checkOthersAttachment = $this->_db->select()
            ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
            ->where("BG_REG_NUMBER = '$BG_NUMBER'")
            ->order('A.INDEX ASC')
            ->query()->fetchAll();

        if (count($checkOthersAttachment) > 0) {
            $this->view->othersAttachment = $checkOthersAttachment;
        }

        // end others attachment

        if (!empty($BG_NUMBER)) {

            $verifyData = $this->_db->select()
                ->from(array('A' => 'TEMP_BGVERIFY_DETAIL'), array('*'))
                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();
            //var_dump($verifyData);die;
            if (!empty($verifyData)) {
                foreach ($verifyData as $vl) {
                    //if()
                    $label = 'check' . $vl['INDEX'];
                    //var_dump($label);
                    $this->view->$label = true;
                    $labeldate = 'verify' . $vl['INDEX'];
                    $verifydate = Application_Helper_General::convertDate($vl['VERIFIED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                    $this->view->$labeldate = $verifydate . ' by ' . $vl['VERIFIEDBY'];
                }
            }

            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.SP_OBLIGEE_CODE = B.CUST_ID', array('B.CUST_NAME'))
                ->joinLeft(["PRINSIPAL" => "M_CUSTOMER"], "PRINSIPAL.CUST_ID = A.CUST_ID", [
                    "PRINSIPAL" => "PRINSIPAL.CUST_NAME"
                ])
                ->joinLeft(["BG_BRANCH" => "M_BRANCH"], "BG_BRANCH.BRANCH_CODE = A.BG_BRANCH", [
                    "BG_BRANCH_NAME" => "BG_BRANCH.BRANCH_NAME"
                ])
                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->where('A.BG_STATUS = ?', 15)
                ->query()->fetchAll();

            $getTabBankDetail = $this->_db->select()
                ->from("T_BANK_GUARANTEE_DETAIL")
                ->where("BG_REG_NUMBER = ?", $bgdata[0]["BG_REG_NUMBER"])
                ->query()->fetchAll();

            $getCurrency = array_search("Currency", array_column($getTabBankDetail, "PS_FIELDNAME"));
            $bgdata[0]["CURRENCY"] = $getTabBankDetail[$getCurrency]["PS_FIELDVALUE"];

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))

                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();

            $principleData = [];
            if (count($bgdatadetail) > 0) {
                foreach ($bgdatadetail as $key => $value) {
                    $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
                }

                $this->view->principleData = $principleData;
            }
            //$datas = $this->_request->getParams();
            //echo '<pre>';
            //var_dump($bgdata);
            //die;


            if (!empty($bgdata)) {

                $get_cash_collateral = $this->_db->select()
                    ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
                    ->where("CUST_ID = ?", "GLOBAL")
                    ->where("CHARGES_TYPE = ?", "10")
                    ->query()->fetchAll();

                $this->view->cash_collateral = $get_cash_collateral[0];

                $this->view->data = $bgdata[0];
                $data = $bgdata[0];

                $attahmentDestination = UPLOAD_PATH . '/document/submit/';

                $get_pdf = file_get_contents($attahmentDestination . $data["DOCUMENT_ID"] . ".pdf");
                // $get_total_pages = preg_match_all("/\/Page\W/", $get_pdf, $dummy);

                $get_total_pages = strstr($get_pdf, "Count");
                $get_total_pages = substr($get_total_pages, 0, 7);
                $get_total_pages = intval(explode(" ", $get_total_pages)[1]);

                $this->view->total_halaman_pdf = $get_total_pages;

                $getbuserlogin = $this->_db->select()
                    ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH', "BUSER_ID"))
                    ->joinLeft(["BR" => "M_BRANCH"], "A.BUSER_BRANCH = BR.ID")
                    ->where('A.BUSER_ID = ?', $this->_userIdLogin);
                $buser = $this->_db->fetchRow($getbuserlogin);

                $getbuserreleaser = $this->_db->select()
                    ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH', "BUSER_ID"))
                    ->joinLeft(["BR" => "M_BRANCH"], "A.BUSER_BRANCH = BR.ID")
                    // ->where('A.BUSER_ID = ?', $data["BG_UPDATEDBY"]);
                    ->where('BR.BRANCH_CODE = ?', $data["BG_BRANCH"]);
                $buserreleaser = $this->_db->fetchRow($getbuserreleaser);

                $changereleaser = false;
                $onlyprint = false;
                if ($buser["BRANCH_CODE"] == $buserreleaser["BRANCH_CODE"]) {
                    $changereleaser = true;
                } else {
                    $onlyprint = true;
                }
                $this->view->changereleaser = $changereleaser;
                $this->view->onlyprint = $onlyprint;

                $getBranchReleaser = $this->_db->select()
                    ->from(["MBSR" => "M_BUSER"])
                    ->joinLeft(["MB" => "M_BRANCH"], "MBSR.BUSER_BRANCH = MB.ID")
                    // ->where("MBSR.BUSER_ID = ?", $data["BG_UPDATEDBY"])
                    ->where("MB.BRANCH_CODE = ?", $data["BG_BRANCH"])
                    ->query()->fetchAll();

                $this->view->branchReleaser = $getBranchReleaser[0]["BRANCH_NAME"];
                $this->view->branchReleaserCode = $getBranchReleaser[0]["BRANCH_CODE"];

                if (!empty($data["BG_BRANCH_PUBLISHER"])) {
                    $getBranchPublisher = $this->_db->select()
                        ->from("M_BRANCH")
                        ->where("BRANCH_CODE = ?", $data["BG_BRANCH_PUBLISHER"])
                        ->query()->fetchAll();

                    $this->view->branchPublisher = $getBranchPublisher[0]["BRANCH_NAME"];
                    $this->view->branchPublisherCode = $getBranchPublisher[0]["BRANCH_CODE"];
                } else {
                    $getBranchPublisher = $this->_db->select()
                        ->from(["MBSR" => "M_BUSER"])
                        ->joinLeft(["MB" => "M_BRANCH"], "MBSR.BUSER_BRANCH = MB.ID")
                        // ->where("MBSR.BUSER_ID = ?", $data["BG_UPDATEDBY"])
                        ->where("MB.BRANCH_CODE = ?", $data["BG_BRANCH"])
                        ->query()->fetchAll();

                    $this->view->branchPublisher = $getBranchPublisher[0]["BRANCH_NAME"];
                    $this->view->branchPublisherCode = $getBranchPublisher[0]["BRANCH_CODE"];
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                } else {
                    $data["BG_NUMBER"] = substr($data["BG_NUMBER"], 0, 3) . "-" . substr($data["BG_NUMBER"], 3, 6) . "-" . substr($data["BG_NUMBER"], 9, 5) . "-" . substr($data["BG_NUMBER"], 14);
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }

                $this->view->BG_NUMBER = $data['BG_NUMBER'];

                $configCompletion = Zend_Registry::get('config');
                $getCompletionCode = $configCompletion["bg"]["complation"]["code"];
                $getCompletionDesc = $configCompletion["bg"]["complation"]["desc"];

                $getCompletion = array_combine(array_values($getCompletionCode), array_values($getCompletionDesc));

                $this->view->completionStatus = $getCompletion;

                if ($this->_request->isPost()) {
                    $setuju = $this->_request->getParam("setuju");
                    $tolak = $this->_request->getParam("tolak");
                    $kirim = $this->_request->getParam("kirim");

                    if ($setuju) {
                        $this->_db->update("T_BANK_GUARANTEE", [
                            "REPRINT" => "2"
                        ], [
                            "BG_REG_NUMBER = ?" => $BG_NUMBER
                        ]);

                        Application_Helper_General::writeLog('AARG', 'Terima Permintaan Cetak Ulang [' . $data["BG_NUMBER"] . ']');
                    } else if ($tolak) {
                        $reject_notes = $this->_request->getPost("PS_REASON_REJECT");
                        $this->_db->update("T_BANK_GUARANTEE", [
                            "REPRINT" => "3",
                            "REJECT_REPRINT_NOTES" => $reject_notes,
                        ], [
                            "BG_REG_NUMBER = ?" => $BG_NUMBER
                        ]);

                        Application_Helper_General::writeLog('AARG', 'Tolak Permintaan Cetak Ulang [' . $data["BG_NUMBER"] . '] | Note : ' . $reject_notes . '');
                    } else if ($kirim) {
                        $this->_db->update("T_BANK_GUARANTEE", [
                            "BG_BRANCH_PUBLISHER" => $this->_request->getParam("save_bg_publisher")
                        ], [
                            "BG_REG_NUMBER = ?" => $BG_NUMBER
                        ]);
                    } else {
                        $this->_db->update("T_BANK_GUARANTEE", [
                            "BG_BRANCH_PUBLISHER" => $this->_request->getParam("save_bg_publisher"),
                            "REPRINT" => "4"
                        ], [
                            "BG_REG_NUMBER = ?" => $BG_NUMBER
                        ]);
                    }

                    $this->setbackURL('/' . $this->_request->getModuleName() . '/list/');
                    return $this->_redirect('/notification/success/index');
                }
            }
        }
    }


    public function validateapproval($policy, $list, $bgnumb)
    {
        die('a');
        $command = ' ' . $policy . ' ';
        $command = strtoupper($command);

        $cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
        var_dump($cleanCommand);
        $commandspli = explode('THEN', $cleanCommand);
        $commandnew = '';
        foreach ($commandspli as $ky => $valky) {
            if ($commandspli[$ky + 1] != '') {
                $commandnew .= '(' . $valky . ') THEN ';
            } else {
                $commandnew .= '(' . $valky . ')';
            }
        }

        //transform to php logical operator syntak
        $translate = array(
            'AND' => '&&',
            'OR' => '||',
            'THEN' => '&&',
            'A' => '$A',
            'B' => '$B',
            'C' => '$C',
            'D' => '$D',
            'E' => '$E',
            'F' => '$F',
            'G' => '$G',
            'H' => '$H',
            'I' => '$I',
            'J' => '$J',
            'K' => '$K',
            'L' => '$L',
            'M' => '$M',
            'N' => '$N',
            'O' => '$O',
            'P' => '$P',
            'Q' => '$Q',
            'R' => '$R',
            // 'S' => '$S',
            'T' => '$T',
            'U' => '$U',
            'V' => '$V',
            'W' => '$W',
            'X' => '$X',
            'Y' => '$Y',
            'Z' => '$Z',
            'SG' => '$SG',
        );

        $phpCommand =  strtr($commandnew, $translate);

        $param = array(
            '0' => '$A',
            '1' => '$B',
            '2' => '$C',
            '3' => '$D',
            '4' => '$E',
            '5' => '$F',
            '6' => '$G',
            '7' => '$H',
            '8' => '$I',
            '9' => '$J',
            '10' => '$K',
            '11' => '$L',
            '12' => '$M',
            '13' => '$N',
            '14' => '$O',
            '15' => '$P',
            '16' => '$Q',
            '17' => '$R',
            // '18' => '$S',
            '19' => '$T',
            '20' => '$U',
            '21' => '$V',
            '22' => '$W',
            '23' => '$X',
            '24' => '$Y',
            '25' => '$Z',
            '26' => '$SG',
        );
        // print_r($phpCommand);die;
        // function str_replace_first($from, $to, $content,$row)
        // {
        //     $from = '/'.preg_quote($from, '/').'/';
        //     return preg_replace($from, $to, $content, $row);
        // }

        $command = str_replace('(', '', $policy);
        $command = str_replace(')', '', $command);
        $list = explode(' ', $command);
        // print_r($list);die;
        $approver = array();
        foreach ($list as $key => $value) {
            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))

                    ->where("C.REG_NUMBER = ?", (string) $bgnumb)
                    ->where("C.GROUP = ?", (string) $value);
                // echo $selectapprover;die;
                $usergroup = $this->_db->fetchAll($selectapprover);
                // print_r($usergroup);
                $approver[$value] = $usergroup;
            }
        }
        // print_r($approver);die;
        foreach ($param as $url) {
            if (strpos($phpCommand, $url) !== FALSE) {
                $ta = substr_count($phpCommand, $url);
                // print_r($list);die;

                if (!empty($approver)) {
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                            foreach ($approver[$value] as $row => $val) {
                                if (!empty($val)) {
                                    $values = 'G' . $value;
                                    ${$values}[$row + 1] = true;
                                }
                            }
                        }
                    }
                }
                for ($i = 1; $i <= $ta; $i++) {
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                            $values = 'G' . $value;
                            if (empty(${$values}[$i])) {
                                ${$values}[$i] = false;
                            }
                        }
                    }

                    $BG_NUMBER = $i;
                    $label = str_replace('$', '$G', $url);

                    $replace = $label . '[' . $BG_NUMBER . ']';

                    $alf = str_replace('$', '', $url);
                    $values = 'G' . $alf;

                    if (${$values}[$i] == $replace) {
                        $phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
                        //	 print_r($phpCommand);
                    } else {
                        $phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
                        //	 print_r($phpCommand);
                    }
                }
            }
        }
        $keywords = preg_split("/[\s,]+/", $cleanCommand);
        $result =  false;
        //print_r($phpCommand);
        eval('$result = ' . "$phpCommand;");
        //var_dump ($result);die;
        return $result;
    }



    public function findPolicyBoundary($transfertype, $amount)
    {


        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BGBOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        //echo $selectuser;die;
        $datauser = $this->_db->fetchAll($selectuser);

        return $datauser[0]['POLICY'];
    }

    //return tombol jika blm ada yg approve
    public function findUserBoundary($transfertype, $amount)
    {



        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        //echo $selectuser;die();
        $datauser = $this->_db->fetchAll($selectuser);

        $command = str_replace('(', '', $datauser[0]['POLICY']);
        $command = str_replace(')', '', $command);
        $command = $command . ' SG';
        $list = explode(' ', $command);

        $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

        $flipAlphabet = array_flip($alphabet);

        foreach ($list as $row => $data) {
            foreach ($alphabet as $key => $value) {
                if ($data == $value) {
                    $groupuser[] = $flipAlphabet[$data];
                }
            }
        }

        $uniqueGroupUser = array_unique($groupuser);

        foreach ($uniqueGroupUser as $key => $value) {
            if ($value == '27') {
                $selectGroupName    = $this->_db->select()
                    ->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
                    ->where("C.GROUP_BUSER_ID LIKE ?", '%S_%');
            } else {
                $selectGroupName    = $this->_db->select()
                    ->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
                    ->where("C.GROUP_BUSER_ID LIKE ?", '%_' . $value . '%');
            }

            $groupNameList = $this->_db->fetchAll($selectGroupName);

            array_unique($groupNameList[0]);

            $uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
        }

        foreach ($uniqueGroupName as $row => $data) {
            foreach ($alphabet as $key => $value) {
                if ($row == $key) {
                    $newUniqueGroupName[$value] = $data;
                }
            }
        }

        foreach ($groupuser as $key => $value) {

            //if special group
            if ($value == 27) {
                $likecondition = "S_%";
            } else {
                $likecondition = "%_" . $value . "%";
            }

            $selectgroup = $this->_db->select()
                ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
                    'BUSER_ID'
                ))

                ->where("C.GROUP_BUSER_ID LIKE ?", (string) $likecondition);

            $group_user = $this->_db->fetchAll($selectgroup);

            $groups[][$alphabet[$value]] = $group_user;
        }
        //	echo '<pre>';
        //var_dump($groups);
        $tempGroup = array();
        foreach ($groups as $key => $value) {

            foreach ($value as $data => $values) {

                foreach ($values as $row => $val) {
                    $userid = $val['BUSER_ID'];

                    $selectusername = $this->_db->select()
                        ->from(array('M_BUSER'), array(
                            '*'
                        ))

                        ->where("BUSER_ID = ?", (string) $userid);
                    //echo $selectusername;echo ' ';
                    $username = $this->_db->fetchAll($selectusername);

                    if (!in_array($data, $tempGroup)) {
                        $userlist[$data][] = $username[0]['BUSER_NAME'];
                    }
                }

                array_push($tempGroup, $data);

                // $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
                // 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
            }
        }

        $userlist['GROUP_NAME'] = $newUniqueGroupName;

        return $userlist;
    }




    public function validatebtn($transfertype, $amount, $ccy, $psnumb)
    {
        //die;


        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BGBOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))

            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        // echo $selectuser;

        $datauser = $this->_db->fetchAll($selectuser);
        if (empty($datauser)) {

            return true;
        }

        $selectusergroup    = $this->_db->select()
            ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
                '*'
            ))

            ->where("C.BUSER_ID 	= ?", (string) $this->_userIdLogin);

        $usergroup = $this->_db->fetchAll($selectusergroup);




        $this->view->boundarydata = $datauser;
        //var_dump($usergroup);die;
        // print_r($this->view->boundarydata);die;
        if (!empty($usergroup)) {
            $cek = false;

            foreach ($usergroup as $key => $value) {
                $group = explode('_', $value['GROUP_BUSER_ID']);
                $alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
                $groupalfa = $alphabet[(int) $group[2]];
                // print_r($groupalfa);echo '-';
                $usergroup[$key]['GROUP'] = $groupalfa;
                //var_dump($usergroup);
                // print_r($datauser);die;
                foreach ($datauser as $nub => $val) {
                    $command = str_replace('(', '', $val['POLICY']);
                    $command = str_replace(')', '', $command);
                    $list = explode(' ', $command);

                    //var_dump($list);
                    foreach ($list as $row => $data) {

                        if ($data == $groupalfa) {
                            $cek = true;
                            // die('ter');
                            break;
                        }
                    }
                }
            }
            //die;



            if ($group[0] == 'S') {
                $cek = true;
            }
            // echo $cek;
            // print_r($cek);die;
            if (!$cek) {
                // die('here');
                return false;
            }
        }
        $tempusergroup = $usergroup;

        if ($cek) {




            $command = ' ' . $datauser['0']['POLICY'] . ' ';
            $command = strtoupper($command);

            $cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

            //transform to php logical operator syntak
            $translate = array(
                'AND' => '&&',
                'OR' => '||',
                'THEN' => 'THEN$',
                'A' => '$A',
                'B' => '$B',
                'C' => '$C',
                'D' => '$D',
                'E' => '$E',
                'F' => '$F',
                'G' => '$G',
                'H' => '$H',
                'I' => '$I',
                'J' => '$J',
                'K' => '$K',
                'L' => '$L',
                'M' => '$M',
                'N' => '$N',
                'O' => '$O',
                'P' => '$P',
                'Q' => '$Q',
                'R' => '$R',
                // 'S' => '$S',
                'T' => '$T',
                'U' => '$U',
                'V' => '$V',
                'W' => '$W',
                'X' => '$X',
                'Y' => '$Y',
                'Z' => '$Z',
                'SG' => '$SG',
            );

            $phpCommand =  strtr($cleanCommand, $translate);
            //var_dump($phpCommand);die;
            $param = array(
                '0' => '$A',
                '1' => '$B',
                '2' => '$C',
                '3' => '$D',
                '4' => '$E',
                '5' => '$F',
                '6' => '$G',
                '7' => '$H',
                '8' => '$I',
                '9' => '$J',
                '10' => '$K',
                '11' => '$L',
                '12' => '$M',
                '13' => '$N',
                '14' => '$O',
                '15' => '$P',
                '16' => '$Q',
                '17' => '$R',
                // '18' => '$S',
                '19' => '$T',
                '20' => '$U',
                '21' => '$V',
                '22' => '$W',
                '23' => '$X',
                '24' => '$Y',
                '25' => '$Z',
                '26' => '$SG',
            );
            // print_r($phpCommand);die;
            function str_replace_first($from, $to, $content, $row)
            {
                $from = '/' . preg_quote($from, '/') . '/';
                return preg_replace($from, $to, $content, $row);
            }

            $command = str_replace('(', ' ', $val['POLICY']);
            $command = str_replace(')', ' ', $command);
            $list = explode(' ', $command);
            // print_r($list);die;
            // var_dump($command)

            $thendata = explode('THEN', $command);
            // print_r($thendata);echo '<br>';die;
            $cthen = count($thendata);
            // print_r($thendata);die;
            $secondcommand = str_replace('(', '', trim($thendata[0]));
            $secondcommand = str_replace(')', '', $secondcommand);
            $secondcommand = str_replace('AND', '', $secondcommand);
            $secondcommand = str_replace('OR', '', $secondcommand);
            $secondlist = explode(' ', $secondcommand);
            // print_r($secondlist);die;
            // print_r($grouplist);die;
            // print_r($thendata[$i]);die;
            // return true;
            if (!empty($secondlist)) {
                foreach ($usergroup as $key => $value) {
                    foreach ($secondlist as $row => $thenval) {
                        // print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
                        if (trim($value['GROUP']) == trim($thenval)) {
                            $thengroup = true;
                            $grouplist[] = trim($thenval);
                            //die('here');
                        }
                    }
                }
            }

            //var_dump($cthen);
            if ($cthen >= 2) {
                foreach ($usergroup as $key => $value) {
                    // print_r($value);
                    foreach ($thendata as $row => $thenval) {
                        // echo '|';print_r($thenval);echo '==';
                        // print_r($value['GROUP']);echo '|';echo '<br/>';
                        // $thengroup = true;
                        $newsecondcommand = str_replace('(', '', trim($thenval));
                        $newsecondcommand = str_replace(')', '', $newsecondcommand);
                        $newsecondcommand = str_replace('AND', '', $newsecondcommand);
                        $newsecondcommand = str_replace('OR', '', $newsecondcommand);
                        $newsecondlist = explode(' ', $newsecondcommand);
                        //var_dump($newsecondcommand);
                        if (in_array(trim($value['GROUP']), $newsecondlist)) {
                            //if (trim($value['GROUP']) == trim($thenval)) {
                            $thengroup = true;
                            $grouplist[] = trim($thenval);
                            //die('here');
                        }
                    }
                }
            }
            //var_dump($grouplist);die;
            //var_dump($thengroup);die;
            // var_dump($thengroup);die;
            // // print_r($group);die;
            // // echo $thengroup;die;
            //echo '<pre>';
            //var_dump($thengroup);
            //var_dump($thendata);
            //print_r($thendata);echo '<br/>';die('here');
            if ($thengroup == true) {


                for ($i = 1; $i <= $cthen; ++$i) {
                    $oriCommand = $phpCommand;
                    //echo $oriCommand;die;
                    $indno = $i;
                    //echo $oriCommand;echo '<br>';

                    for ($a = $cthen - $indno; $a >= 1; --$a) {

                        if ($i > 1) {
                            $replace = 'THEN$ $' . trim($thendata[$a + 1]);
                        } else {
                            $replace = 'THEN$ $' . trim($thendata[$a]);
                        }

                        $oriCommand = str_replace($replace, "", $oriCommand);
                    }


                    //print_r($thendata);echo '<br>';die();

                    //die;
                    // if($i == 3){
                    // echo 'command : ';echo $oriCommand;echo '<br/>';
                    // }
                    //print_r($oriCommand);echo '<br>';
                    //print_r($list);echo '<br>';die;


                    $result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
                    // print_r($i);
                    //var_dump($result);
                    //echo 'result-';var_dump($result);echo '-';die;
                    if ($result) {
                        // die;
                        // echo $thendata[$i+1];die('eere');
                        // print_r($i);
                        // print_r($thendata);die;



                        $replace = 'THEN$ $' . trim($thendata[$i + 1]);
                        // var_dump($replace);die;
                        // print_r($i);
                        if (!empty($thendata[$i + 1])) {
                            //die;
                            $oriCommand = str_replace($replace, "", $phpCommand);
                        } else {
                            // die;
                            $thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
                            $thirdcommand = str_replace(')', '', $thirdcommand);
                            $thirdcommand = str_replace('AND', '', $thirdcommand);
                            $thirdcommand = str_replace('OR', '', $thirdcommand);
                            $thirdlist = explode(' ', $thirdcommand);
                            //						var_dump($secondlist);
                            //						var_dump($grouplist);
                            //							die;

                            if (!empty($secondlist)) {
                                foreach ($grouplist as $key => $valg) {
                                    foreach ($secondlist as $row => $value) {
                                        if ($value == $valg) {
                                            //echo 'sini';
                                            return false;
                                        }
                                    }
                                }
                            }
                            $oriCommand = $phpCommand;
                        }
                        // print_r($thendata[$i]);die;
                        // if($i == 3){
                        // echo $oriCommand;die;
                        // echo '<br/>';	
                        // }
                        // print_r($i);
                        //echo $oriCommand;
                        $result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
                        //var_dump($result);echo '<br/>';
                        if (!$result) {
                            // die;
                            //print_r($groupalfa);
                            //print_r($thendata[$i]);

                            if ($groupalfa == trim($thendata[$i])) {
                                //echo 'heer';
                                return true;
                            } else {

                                //return true;
                            }
                            /*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
                        } else {

                            //return false;

                            // $result = $this->generate($phpCommand,$list,$param,$psnumb);
                            // print_r($phpCommand);
                            // if($result){}
                            // die('here');
                        }
                        // var_dump($result);
                        // die;


                    } else {
                        //die('here');
                        $secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
                        $secondcommand = str_replace(')', '', $secondcommand);
                        $secondcommand = str_replace('AND', '', $secondcommand);
                        $secondcommand = str_replace('OR', '', $secondcommand);
                        $secondlist = explode(' ', $secondcommand);
                        //var_dump($i);
                        // var_dump($thendata);
                        //print_r($grouplist);
                        //die;
                        //if($groupalfa == $gro)
                        $approver = array();
                        $countlist = array_count_values($list);
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                                $selectapprover    = $this->_db->select()
                                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                                        'USER_ID'
                                    ))

                                    // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                                    ->where("C.REG_NUMBER = ?", (string) $psnumb)
                                    ->where("C.GROUP = ?", (string) $value);
                                //	 echo $selectapprover;
                                $usergroup = $this->_db->fetchAll($selectapprover);
                                // print_r($usergroup);
                                $approver[$value] = $usergroup;
                                if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
                                    //var_dump($countlist[$value]);
                                    //var_dump($tempusergroup['0']['GROUP']);  
                                    //die('gere');
                                    return false;
                                }
                            }
                        }

                        //$array = array(1, "hello", 1, "world", "hello");


                        //echo '<pre>';
                        //var_dump($list);
                        //var_dump($approver);
                        //die;
                        //var_dump($secondlist[0]);die;
                        //foreach($approver as $app => $valpp){
                        if (!empty($thendata[$i])) {
                            if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
                                //die('gere');
                                return false;
                            }
                        }

                        //echo '<pre>';
                        //var_dump($thendata);
                        //var_dump($grouplist);
                        //var_dump($approver);
                        //var_dump($groupalfa);
                        //die;
                        foreach ($grouplist as $key => $vg) {
                            $newgroupalpa = str_replace('AND', '', $vg);
                            $newgroupalpa = str_replace('OR', '', $newgroupalpa);
                            $groupsecondlist = explode(' ', $newgroupalpa);
                            //var_dump($newgroupalpa);
                            //&& empty($approver[$groupalfa])
                            if (in_array($groupalfa, $groupsecondlist)) {
                                // echo 'gere';die;
                                return true;
                                //die('ge');
                            }

                            if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
                                // echo 'gere';die;
                                return true;
                                //die('ge');
                            }
                        }

                        // var_dump($approver[$groupalfa]);
                        // var_dump($grouplist);
                        //var_dump($groupalfa);die;

                        //echo 'ger';die;
                        //print_r($secondlist);die;
                        //	 return true;
                        if (!empty($secondlist)) {
                            foreach ($grouplist as $key => $valg) {
                                foreach ($secondlist as $row => $value) {
                                    if ($value == $valg) {

                                        if (empty($thendata[1])) {
                                            //die;
                                            return true;
                                        }
                                        //else{
                                        //	return false;
                                        //	}
                                    }
                                }
                            }
                        }

                        //	echo 'here';die;
                        $secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);
                        // var_dump($secondresult);
                        //print_r($thendata[$i-1]);
                        //die;
                        //if()
                        //	 echo '<pre>';
                        //var_dump($grouplist);die;
                        foreach ($grouplist as $key => $valgroup) {
                            //print_r($valgroup);
                            //var_dump($thendata[$i]); 


                            if (trim($valg) == trim($thendata[$i])) {
                                $cekgroup = false;
                                //die('here');
                                if ($secondresult) {
                                    return false;
                                } else {
                                    return true;
                                }

                                //break;
                            }

                            //else if (trim($valg) == trim($thendata[$i - 1])) {
                            //		$cekgroup = false;
                            // die('here');
                            //	return false;
                            //	}
                        }
                        //die;
                        //if (!$cekgroup) {
                        // die('here');
                        //return false;
                        //}
                    }

                    //		    echo '<br/>';
                    //	${$command} = $oriCommand;
                }
            } else if (!empty($thendata) && $thengroup == false) {

                //var_dump($groupalfa)die;
                foreach ($thendata as $ky => $vlue) {
                    $newsecondcommand = str_replace('(', '', trim($vlue));
                    $newsecondcommand = str_replace(')', '', $newsecondcommand);
                    $newsecondcommand = str_replace('AND', '', $newsecondcommand);
                    $newsecondcommand = str_replace('OR', '', $newsecondcommand);
                    $newsecondlist = explode(' ', $newsecondcommand);
                    if ($newsecondlist['0'] == $groupalfa) {
                        return true;
                    }
                }
                return false;
            }




            $approver = array();
            // print_r($list);die;  	
            foreach ($list as $key => $value) {
                if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                    $selectapprover    = $this->_db->select()
                        ->from(array('C' => 'T_BGAPPROVAL'), array(
                            'USER_ID'
                        ))

                        // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                        ->where("C.REG_NUMBER = ?", (string) $psnumb)
                        ->where("C.GROUP = ?", (string) $value);
                    //	 echo $selectapprover;
                    $usergroup = $this->_db->fetchAll($selectapprover);
                    // print_r($usergroup);
                    $approver[$value] = $usergroup;
                }
            }
            //die;




            // print_r($phpCommand);die;
            foreach ($param as $url) {
                if (strpos($phpCommand, $url) !== FALSE) {
                    $ta = substr_count($phpCommand, $url);
                    // print_r($list);die;

                    if (!empty($approver)) {
                        // print_r($approver);die;
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                                foreach ($approver[$value] as $row => $val) {
                                    // print_r($approver);die;
                                    if (!empty($val)) {
                                        $values = 'G' . $value;
                                        ${$values}[$row + 1] = true;
                                        // print_r($B);
                                    }

                                    // print_r($val);
                                }
                            }
                        }
                    }

                    for ($i = 1; $i <= $ta; $i++) {
                        // print_r($list);die;
                        foreach ($list as $key => $value) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                                $values = 'G' . $value;
                                // print_r(${$values});
                                if (empty(${$values}[$i])) {
                                    ${$values}[$i] = false;
                                }
                                // if(${$value}[$i])
                            }
                        }
                        // print_r($phpCommand);die;
                        $BG_NUMBER = $i;
                        $label = str_replace('$', '$G', $url);

                        $replace = $label . '[' . $BG_NUMBER . ']';

                        $alf = str_replace('$', '', $url);
                        $values = 'G' . $alf;
                        // print_r($values);die;
                        if (${$values}[$i] == $replace) {
                            $phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
                            // print_r($phpCommand);
                        } else {
                            $phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
                            // print_r($phpCommand);die;
                        }
                        // }
                        // }

                    }
                    // print_r($GB);die;

                }
            }

            $keywords = preg_split("/[\s,]+/", $cleanCommand);
            $result =  false;
            $thendata = explode('THEN$', $phpCommand);
            //var_dump($thendata);die;
            if (!empty($thendata['1'])) {
                $phpCommand = '';
                foreach ($thendata as $tkey => $tval) {
                    $phpCommand .= '(';
                    $phpCommand .= $tval . ')';
                    if (!empty($thendata[$tkey + 1])) {
                        $phpCommand .= ' && ';
                    }
                }
            } else {

                $phpCommand = str_replace('THEN$', '&&', $phpCommand);
            }
            //var_dump($phpCommand);
            if (!empty($phpCommand)) {
                eval('$result = ' . "$phpCommand;");
            } else {
                return false;
            }
            //var_dump($result);die;
            if (!$result) {

                return true;
            }
            // die('here2');
            //var_dump ($result);die;
            //return $result;
        } else {
            // die('here');
            return true;
        }
    }



    public function generate($command, $list, $param, $psnumb, $group, $thengroup)
    {

        $phpCommand = $command;

        // echo $command;die;

        $approver = array();

        $count_list = array_count_values($list);
        //print_r($count_list);
        foreach ($list as $key => $value) {
            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))

                    // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                    ->where("C.REG_NUMBER = ?", (string) $psnumb)
                    ->where("C.GROUP = ?", (string) $value);
                // echo $selectapprover;
                $usergroup = $this->_db->fetchAll($selectapprover);
                // print_r($usergroup);
                $approver[$value] = $usergroup;
            }
        }
        //var_dump($param);die;
        //var_dump($group);
        foreach ($approver as $appval) {
            $totaldata = count($approver[$group]);
            $totalgroup = $count_list[$group];
            //var_dump($totaldata);
            //var_dump($totalgroup);
            if ($totalgroup == $totaldata && $totalgroup != 0) {

                return false;
            }
        } //die;
        //die;





        foreach ($param as $url) {

            if (strpos($phpCommand, $url) !== FALSE) {
                $ta = substr_count($phpCommand, $url);
                // print_r($list);die;

                if (!empty($approver)) {
                    // print_r($approver);die;
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                            foreach ($approver[$value] as $row => $val) {
                                // print_r($approver);die;
                                if (!empty($val)) {
                                    $values = 'G' . $value;
                                    ${$values}[$row + 1] = true;
                                    // print_r($B);
                                }

                                // print_r($val);
                            }
                        }
                    }
                }


                // print_r($approver);die;

                for ($i = 1; $i <= $ta; $i++) {

                    foreach ($list as $key => $value) {
                        if (!empty($value)) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                                $values = 'G' . $value;
                                // print_r(${$values});
                                if (empty(${$values}[$i])) {
                                    ${$values}[$i] = false;
                                }
                                // if(${$value}[$i])
                            }
                        }
                    }


                    $BG_NUMBER = $i;
                    $label = str_replace('$', '$G', $url);
                    // print_r($phpCommand);die('here');
                    $replace = $label . '[' . $BG_NUMBER . ']';

                    $alf = str_replace('$', '', $url);
                    $values = 'G' . $alf;

                    if (${$values}[$i] == $replace) {
                        $phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
                        // print_r($phpCommand);
                    } else {
                        $phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
                        // print_r($phpCommand);die;
                    }
                    // }
                    // }

                }
                // print_r($GB);die;

            }
        }

        $keywords = preg_split("/[\s,]+/", $cleanCommand);
        $result =  false;
        $phpCommand = str_replace('THEN$', '&&', $phpCommand);
        //print_r($phpCommand);echo '<br/>';

        if (!empty($phpCommand)) {
            eval('$result = ' . "$phpCommand;");
            //var_dump($thengroup);
            // var_dump($result);

            if ($result) {
                //var_dump($thengroup);die;
                if ($thengroup) {
                    return true;
                } else {
                    return false;
                }
            } else {

                if ($thengroup) {
                    return false;
                } else {

                    return true;
                }
            }
            // return $result;
        } else {
            return false;
        }

        // var_dump ($result);die;




    }

    public function getbgpublisherAction()
    {
        $this->_helper->layout()->setLayout('popup');
        $getAllBranch = $this->_db->select()
            ->from("M_BRANCH", ["*"])
            ->where("HEADQUARTER = 'NO'")
            ->query()->fetchAll();

        $this->paging($getAllBranch);
    }

    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function Terbilangen($nilai)
    {
        $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 20 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 100) {
            return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilangen($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilangen($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function generateTransactionID($branch, $bgtype)
    {

        $currentDate = date("ymd");
        $seqNumber   = mt_rand(1111, 9999);
        $trxId = strval($branch) . strval($currentDate) . strval($bgtype) . strval($seqNumber);

        return $trxId;
    }

    public function showfileAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // decrypt numb

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;

        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgregnumber'));

        $bg_reg_number = $AESMYSQL->decrypt($BG_NUMBER, $password);

        $file = $this->_request->getParam("file");

        $get_detail = $this->_db->select()
            ->from("T_BANK_GUARANTEE_DETAIL")
            ->where("BG_REG_NUMBER = ?", $bg_reg_number)
            ->query()->fetchAll();

        switch ($file) {
            case '1':
                $get_keys = array_keys(array_column($get_detail, "PS_FIELDNAME"), "form_permintaan_cetak_ulang");
                $get_keys = $get_keys[count($get_keys) - 1];
                $get_file_name = $get_detail[$get_keys]["PS_FIELDVALUE"];
                break;
            case '2':
                $get_keys = array_keys(array_column($get_detail, "PS_FIELDNAME"), "lampiran_bukti_dokumen");
                $get_keys = $get_keys[count($get_keys) - 1];
                $get_file_name = $get_detail[$get_keys]["PS_FIELDVALUE"];
                break;
            case '3':
                $get_keys = array_keys(array_column($get_detail, "PS_FIELDNAME"), "bukti_bayar");
                $get_keys = $get_keys[count($get_keys) - 1];
                $get_file_name = $get_detail[$get_keys]["PS_FIELDVALUE"];
                break;
        }

        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=" . $get_file_name);
        // @readfile('/app/bg/library/data/uploads/document/submit/' . $get_file_name);
        @readfile(UPLOAD_PATH . '/document/submit/' . $get_file_name);
    }
}
