<?php

require_once 'Zend/Controller/Action.php';

class setbilleronboardcharges_EditController extends Application_Main
{
	public function indexAction() 
	{
		$model = new setbilleronboardcharges_Model_Setbilleronboardcharges();
		$modelBiller = new biller_Model_Biller();
		
		$providerid = $this->_getParam('providerid');
		$ccy = $this->_getParam('ccy');
		
		$provider = $modelBiller->getDetailProvider($providerid);
		$this->view->providerData 	 = $provider;
		
		$this->view->billerAcctList = $model->getBillerAcctList($providerid);
		
		$submit = $this->_getParam('submit');
		
		$cek = $model->isRequestChange($provider['PROVIDER_CODE']);
			
		if(!$cek)
		{
			if($submit)
			{
				$error = 0;
					
				if($error == 0)
				{
					$this->_db->beginTransaction();
					try
					{
						Application_Helper_General::writeLog('UBCC','Update E-Commerce charges '. $provider['PROVIDER_CODE']);
						$charges_amt = $this->_getParam('charges_amt');
						$charges_ccy = $this->_getParam('charges_ccy');
						$charges_to = $this->_getParam('charges_to');
						$biller_acct = $this->_getParam('biller_acct');
						
						$info = 'E-Commerce Charges';
						$info2 = 'Update E-Commerce Charges';
						
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_PROVIDER','TEMP_CHARGES_PROVIDER',$provider['PROVIDER_CODE'],$provider['PROVIDER_NAME'],$provider['PROVIDER_CODE']);
						
						foreach($charges_ccy as $ccy)
						{
							if($charges_amt[$ccy] == null)
								$amt = '0.00';
							else
								$amt = Application_Helper_General::convertDisplayMoney($charges_amt[$ccy]);
							
							if($charges_to[$ccy] == 1) // jika payer
								$charges_acct = null;
							else if($charges_to[$ccy] == 2) // jika biller
								$charges_acct = $biller_acct[$ccy];
							
							$data= array(
											'CHANGES_ID' 			=> $change_id,
											'PROVIDER_ID' 			=> $providerid,
											'BANK_CODE' 			=> '008', // hardcode
											'CHARGES_CCY' 			=> $ccy,
											'CHARGES_AMT' 			=> $amt,
											'CHARGES_TO' 			=> $charges_to[$ccy],
											'CHARGES_ACCT' 			=> $charges_acct,
											'CHARGES_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
											'CHARGES_SUGGESTEDBY' 	=> $this->_userIdLogin
										);
			
							$model->insertTemp($data);	
						}
		
						
						
						if($error == 0)
						{	
							$this->_db->commit();
							$this->setbackURL('/setbilleronboardcharges/index');
							$this->_redirect('/notification/submited/index');
						}
						
					}
					catch(Exception $e)
					{
						Zend_Debug::dump('rollback'); die;
						$this->_db->rollBack();
					}
				}		
			}
		}
		if($cek)
		{			
			$docErr = "*No changes allowed for this record while awaiting approval for previous change";	
			$this->view->error = $docErr;
			$this->view->changestatus = "disabled";
		}	
	}
}
