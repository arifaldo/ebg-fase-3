<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';

class Changemanagement_Model_Tempapikeymanagement extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = '';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null){

		$apikey = $this->dbObj->select()
						  ->from('TEMP_APIKEY')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetchAll();
						  // print_r($user);die;
		if(!count($apikey)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Apikey) 1';
			return false;
        }

        $custid = $apikey[0]['CUST_ID'];
        $tempdata['bankcode'] = $apikey[0]['BANK_CODE'];


        $tempdata['data'] = array();
        foreach ($apikey as $key => $value) {
        	$tempdata['data'][$value['FIELD']] =  $value['VALUE'];
        }

        $field = array_keys($tempdata['data']);
		$value = array_values($tempdata['data']);

        $selectapikey = $this->dbObj->select()
		 				->from(array('A' => 'M_APIKEY'))
		 				->order('A.APIKEY_ID DESC')
		 				->limit(1);

		$apikey = $this->dbObj->fetchAll($selectapikey);

		if (!empty($apikey)) {
			
			$id = $apikey[0]['APIKEY_ID'] + 1;
		}else{

			$id = 1;
		}

		$request = array();
		foreach ($field as $key => $val) {
			$request[$val] = $value[$key]; 
		}

        //call api
		$clientUser  =  new SGO_Soap_ClientUser();
		
		$auth = Zend_Auth::getInstance()->getIdentity();
		$request['corporate_id_channel'] = $custid;
		$request['corporate_name'] = $custid;
		$request['bankcode'] = $tempdata['bankcode'];
		
		$success = $clientUser->callapi('registerapi',$request,'b2b/account/register');
		
		if($clientUser->isTimedOut()){
			$returnStruct = array(
					'ResponseCode'	=>'XT',
					'ResponseDesc'	=>'Service Timeout',					
					'Cif'			=>'',
					'AccountList'	=> '',
			);

			$this->_errorCode = '82';
			$this->_errorMsg = 'API Timeout(apikey)';
			return false;
		}else{
			$result  = $clientUser->getResult();

			$paramlog = array(
				                    'DIGI_USER' => $this->_userIdLogin,
				                    'DIGI_CUST'	=> $this->_custIdLogin,
				                    'DIGI_BANK' => $request['BANK_CODE'],
				                    'DIGI_ACCOUNT' => $request['account_number'],
				                    'DIGI_ERROR_CODE' => $result->error_code,
				                    'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
				                    'DIGI_SERVICE'	=> 1
					        );

					        $this->_helper->ServiceLog->serviceLog($paramlog);
			// print_r($result);die;
			if($result->error_code == '0000'){
				try{

					for($i=0; $i<count($tempdata['data']); $i++){

						$content = array(
							'APIKEY_ID' 	 => $id,
							'BANK_CODE' 	 => $tempdata['bankcode'],
							'CUST_ID' 		 => $custid,
							'FIELD' 	 	 => $field[$i],
							'VALUE' 	 	 => $value[$i]
				       	);

						$this->dbObj->insert('M_APIKEY',$content);
					}

					$where['CHANGES_ID = ?'] = $this->_changeId;
    				$this->dbObj->delete('TEMP_APIKEY',$where);

					$insertkey['ID'] = $id;
					$insertkey['SENDER_ID']  = $result->eoa_credentials->sender_id;
					$insertkey['AUTH_USER']  = $result->eoa_credentials->auth_username;
					$insertkey['AUTH_PASS']  = $result->eoa_credentials->auth_password;
					$insertkey['SIGNATURE_KEY']  = $result->eoa_credentials->signature_key;
					$insertkey['ALLOW_IP']  = $result->eoa_credentials->allowed_ip;
					
					$this->dbObj->insert('M_APICREDENTIAL',$insertkey);
					$result = $this->dbObj->fetchRow(
						$this->dbObj->select()
							->from(array('C' => 'M_USER'))
							->where("USER_ID = ".$this->dbObj->quote(Zend_Registry::get('userLogin')))
					);

					if($result['USER_HOME_CONTENT'] == ''){
						$updateArr = array(
										'USER_HOME_CONTENT' => '1'
									);

					}else{

						$strdata = $result['USER_HOME_CONTENT'].',1';

						$updateArr = array(
										'USER_HOME_CONTENT' => $strdata
									);
					}
					

					$updateWhere['USER_ID = ?'] = (string)Zend_Registry::get('userLogin');
					$this->dbObj->update('M_USER',$updateArr,$updateWhere);

					//clear cache if account added
					$frontendOptions = array ('lifetime' => 600,
                                  'automatic_serialization' => true );
			        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
			        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
			        $cacheID = 'BAL'.$custid;

			        $cache->remove($cacheID);

				}catch(Exception $e){
					$this->_errorCode = '82';
					$this->_errorMsg = $e;
					return false;
				}

			}else{
				$this->_errorCode = '82';
				$this->_errorMsg = 'Query failed(apikey) 2';
				return false;
			}

			return true;
		}
	}

	//-----------------------------------------sisanya masih bawaan tempusermodel------------------------------------
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  	->from('TEMP_USER')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetch(ZenddbObj::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }

		//Validation, user can only be activated if this user's customer is active
		$customer = $this->dbObj->select()
							  	 ->from('M_CUSTOMER')
							  	 ->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	 ->query()
							  	 ->fetch(ZenddbObj::FETCH_ASSOC);

		if($customer['CUST_STATUS'] == 'I') {
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		
        //query from M_USER
		$m_user = $this->dbObj->select()
						  	  ->from('M_USER')
						  	  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  	  ->where('CUST_ID = ?',(string)$user['CUST_ID'])
						  	  ->query()
						  	  ->fetch(ZenddbObj::FETCH_ASSOC);
		if(!count($m_user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }		
        	
						  	
		//update record
		$updateArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','USER_ID'=>'','USER_STATUS'=>'','USER_PASSWORD'=>'',
												'USER_LASTLOGIN'=>'',
												'USER_LASTACTIVITY'=>'',
												'USER_LOCKREASON'=>'',
												'USER_RRESET'=>'',
												'USER_FAILEDTOKEN'=>'',
												'USER_ISNEW'=>'',
												'USER_CLEARTEXT_PWD'=>'',
												'USER_ISEMAILPWD'=>'',
												'USER_LASTCHANGEPWD'=>'',
												'USER_RPWD_ISBYBO'=>'',
												'USER_RPWD_ISBYUSER'=>'',
												'USER_RPWD_ISEMAILED'=>'',
												'USER_RPWD_ISPRINTED'=>'',
												'FGROUP_ID'=>'',
												'USER_ISEMAIL_TRAN'=>'',
												'USER_UPDATED'=>'',
												'USER_UPDATEDBY'=>'',
												'USER_HASH'=>'',
												'SESSION_HASH'=>'',
		                                        'USER_RCHANGE'=>'','USER_ISREQUIRE_CHANGEPWD'=>'','USER_ISLOGIN'=>'','USER_ISLOCKED'=>'','USER_FAILEDATTEMPT'=>'','USER_CREATED'=>'','USER_CREATEDBY'=>''));
		$updateArr['USER_UPDATED']     = new ZenddbObj_Expr('now()');
		$updateArr['USER_UPDATEDBY']   = $actor;
		
		$whereArr = array('CUST_ID = ?'=>$user['CUST_ID'],
						  'USER_ID = ?'=>$user['USER_ID']);
		
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
		
		//delete privilege master jika ada
		$fuser_id = $user['CUST_ID'] . $user['USER_ID'];
	    $privilegeMaster = $this->dbObj->select()
						               ->from('M_FPRIVI_USER')
						               ->where('FUSER_ID ='.$this->dbObj->quote($fuser_id))
						               ->query()->fetchAll();
						               
		if(count($privilegeMaster) > 0)
		{
		   $this->dbObj->delete('M_FPRIVI_USER',array('FUSER_ID = ?'=>$fuser_id));
		}
		
		//insert privilege baru
	    $privilege = $this->dbObj->select()
						          ->from('TEMP_FPRIVI_USER')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		if(count($privilege) > 0)
		{
		   foreach($privilege as $privi)
		   {
		      $insertPrivi = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
              $this->dbObj->insert('M_FPRIVI_USER',$insertPrivi);
		   }
        }


        $custacct = $this->dbObj->select()
						          ->from('TEMP_CUSTOMER_ACCT')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		if(count($custacct) > 0)
		{
		   foreach($custacct as $privi)
		   {
		      $insertPrivi = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
              $this->dbObj->insert('M_CUSTOMER_ACCT',$insertPrivi);
		   }
        }


        $makerlimit = $this->dbObj->select()
						          ->from('TEMP_MAKERLIMIT')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		if(count($makerlimit) > 0)
		{
		   foreach($makerlimit as $privi)
		   {
		      $insertPrivi = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
              $this->dbObj->insert('M_MAKERLIMIT',$insertPrivi);
		   }
        }
        
        $deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges) return false;
		
		
		//integrate ke core (TOKEN), delete token lama dan add token baru	
        //proses dilakukan jika token baru berbeda dengan token lama, jika sama maka tidak diproses
        if($user['TOKEN_ID'])
        {
	        if($m_user['TOKEN_ID'] != $user['TOKEN_ID'])
			{  
			    $resultRemove = array();
			
			    //jika token id di master user ada isinya maka diremove, jika tidak ada tidak usah di remove
			    if(!empty($m_user['TOKEN_ID']))
			    {
				    //REMOVE TOKEN
				    $dataRemove   = new Service_Token($m_user['CUST_ID'],$m_user['USER_ID'],$m_user['TOKEN_ID']);
					$param['condition'] = 1;
					$resultRemove = $dataRemove->removePairToken($param);
			        //$resultRemove['ResponseCode'] = '00';
			        
			        if($resultRemove['ResponseCode'] != '00')  //00 = success
			        {
			            $this->_errorMsg = $resultRemove['ResponseDesc'];
			            return false;
			        }
			    }
			    else $resultRemove['ResponseCode'] = '00';
				
				if($resultRemove['ResponseCode'] == '00')
				{    
			        //ADD TOKEN		
					$param = array();
					$param['TokenSerial'] 	= $user['TOKEN_ID'];
					$param['UserName'] 		= $user['USER_FULLNAME'];
					
					$param['CustAddress'] 	= $customer['CUST_ADDRESS'];
					$param['CustCity'] 		= $customer['CUST_CITY'];
					$param['CustName'] 		= $customer['CUST_NAME'];
					$param['CustZipCode'] 	= $customer['CUST_ZIP'];
					$dataAdd =  new Service_Token($user['CUST_ID'],$user['USER_ID']);
			
					$resultAdd = $dataAdd->addPairToken($param);	
			        //$resultAdd['ResponseCode'] = '00';
			        
					/*echo $user['CUST_ID'] . '  '.$user['USER_ID'] . '  '.$user['TOKEN_ID']; 
					Zend_Debug::dump($resultAdd); die;*/
					
			        if($resultAdd['ResponseCode'] != '00')  //00 = success
			        {
			            $this->_errorMsg = $resultAdd['ResponseDesc'];
			            return false;
			        }
			        //END integrate ke core (TOKEN)	
				}
				else
				{
				    $this->_errorMsg = $resultRemove['ResponseDesc'];
		            return false;
				}
			}//END if($m_user['TOKEN_ID'] != $user['TOKEN_ID'])
        }
        else if($m_user['TOKEN_ID'])
		{ 
		     //REMOVE TOKEN
				$dataRemove   = new Service_Token($user['CUST_ID'], $user['USER_ID'], $m_user['TOKEN_ID']);
				$param['condition'] = 1;
				$resultRemove = $dataRemove->removePairToken($param);
		     //$resultRemove['ResponseCode'] = '00';
		     
		     //Zend_Debug::dump($resultRemove); die;
				
			 if($resultRemove['ResponseCode'] != '00')
			 {
		        $this->_errorMsg = $resultRemove['ResponseDesc'];
	            return false;
			 }
		}
        
		
		
		
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(ZenddbObj::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp User)';
			return false;
        }
        
        
	    //query from M_USER
		$m_user = $this->dbObj->select()
						  ->from('M_USER')
						  ->where('CUST_ID = ?',(string)$user['CUST_ID'])
						  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  ->query()
						  ->fetch(ZenddbObj::FETCH_ASSOC);
		if(!count($m_user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Master User)';
			return false;
        }
        

		//Validation, user can only be activated if this user's customer is active
		$customer = $this->dbObj->select()
							    ->from('M_CUSTOMER')
							  	->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	->query()
							  	->fetch(ZenddbObj::FETCH_ASSOC);
		if($customer['CUST_STATUS'] == 'I') {
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		
		
		$updateArr = array('USER_STATUS' => 1);
		$updateArr['USER_SUGGESTED']   = $user['USER_SUGGESTED'];
		$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
		$updateArr['USER_UPDATED']     = new ZenddbObj_Expr('now()');
		$updateArr['USER_UPDATEDBY']   = $actor;
		
		
		/*//jika new user pertama x, maka ada tambahan yaitu : request change password dan update requeset unlock user
		if($m_user['USER_ISNEW'] == 1)
		{
		    $customerUser = new CustomerUser($user['CUST_ID'],$user['USER_ID']);
		    $customerUser->requestUnlockResetPassword(2);    //2 = print
		    $customerUser->approveRequestResetPassword();
		}*/
		
		
	
		$whereArr = array('CUST_ID = ?'=>(string)$user['CUST_ID'],
						  'USER_ID = ?'=>(string)$user['USER_ID']);
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;
		
			
			
			
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(ZenddbObj::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }
        
		//Validation, user can only be activated if this user's customer is active
		/*$customer = $this->dbObj->select()
							  	 ->from('M_CUSTOMER')
							  	 ->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	 ->query()
							  	 ->fetch(ZenddbObj::FETCH_ASSOC);

		if($customer['CUST_STATUS'] == 'I') {
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		*/
        
		//update user status			  
		//$updateArr = array('USER_STATUS' => 'I', 'USER_HASTOKEN' => 'N');
		
		$updateArr = array('USER_STATUS' => '2');
		$updateArr['USER_SUGGESTED']   = $user['USER_SUGGESTED'];
		$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
		$updateArr['USER_UPDATED']     = new ZenddbObj_Expr('now()');
		$updateArr['USER_UPDATEDBY']   = $actor;
		
		
		
		$whereArr = array('CUST_ID = ?'=>(string)$user['CUST_ID'],
						  'USER_ID = ?'=>(string)$user['USER_ID']);
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}


/**
 * Returns an encrypted & utf8-encoded
 */
	function encrypt($pure_string, $encryption_key) {
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
	    return $encrypted_string;
	}

	/**
	 * Returns decrypted original string
	 */
	function decrypt($encrypted_string, $encryption_key) {
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
	    return $decrypted_string;
	}
	
	public function approveDelete($actor = null) 
	{
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(ZenddbObj::FETCH_ASSOC);
						  
		if(!count($user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }
        
        //query from M_USER
		$m_user = $this->dbObj->select()
						  	  ->from('M_USER')
						  	  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  	  ->where('CUST_ID = ?',(string)$user['CUST_ID'])
						  	  ->query()
						  	  ->fetch(ZenddbObj::FETCH_ASSOC);
		if(!count($m_user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }								 

        
		//Validation, user can only be activated if this user's customer is active
		$customer = $this->dbObj->select()
							    ->from('M_CUSTOMER')
							  	->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	->query()
							  	->fetch(ZenddbObj::FETCH_ASSOC);
							  	
		if($customer['CUST_STATUS'] == 'I') 
		{
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		
		
		$updateArr['USER_STATUS']      = 3;
		$updateArr['USER_ISLOGIN']     = 0;
		$updateArr['USER_ISLOCKED']    = 0;
		$updateArr['USER_FAILEDATTEMPT'] = 0;
		$updateArr['USER_LOCKREASON']    = '';
		$updateArr['USER_SUGGESTED']   = $user['USER_SUGGESTED'];
		$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
		$updateArr['USER_UPDATED']     = new ZenddbObj_Expr('now()');
		$updateArr['USER_UPDATEDBY']   = $actor;
		
		
		$whereArr = array('CUST_ID = ?'=>(string)$user['CUST_ID'],
						  'USER_ID = ?'=>(string)$user['USER_ID']);
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		
		if(!(boolean)$userupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
	    //integrate ke core (TOKEN), delete token lama dan add token baru	
	    //diremove jika hanya token di M_USER ada isinya, jika tidak ada maka tidak usah dihapus
	    if($m_user['TOKEN_ID'])
	    {
	        //REMOVE TOKEN
			$data   = new Service_Token($user['CUST_ID'], $user['USER_ID'], $m_user['TOKEN_ID']);
			$param['condition'] = 1;
			$result = $data->removePairToken($param);
	        //$result['ResponseCode'] = '00';
			
			if($result['ResponseCode'] != '00')    //00 = 'success'
			{
		        $this->_errorMsg = $result['ResponseDesc'];
	            return false;
			}
	    }
	    
	    $subject = "STATUS USER ACCOUNT INFORMATION";
	    $setting = new Settings();
	    $templateEmailMasterBankName = $setting->getSetting('master_bank_name');
	    $templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
	    $templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
	    $template = $setting->getSetting('ftemplate_addusernotification');
	    // 		$subject = "CMD - New User";
	    
	    $data = $this->dbObj->select()
	    ->from('TEMP_USER')
	    ->where('CHANGES_ID = ?',$this->_changeId)
	    ->query()
	    ->fetch(ZenddbObj::FETCH_ASSOC);
	    // 		print_r($data);die;
	    $datacreated = $this->dbObj->select()
	    ->from('M_USER')
	    ->where('USER_ID = ?',$data['USER_SUGGESTEDBY'])
	    ->where('CUST_ID = ?',$data['CUST_ID'])
	    ->query()
	    ->fetch(ZenddbObj::FETCH_ASSOC);
	    // 		print_r($datacreated);die;
	    
	    
	    $template = str_ireplace('[[user_fullname]]',$data['USER_FULLNAME'],$template);
	    $template = str_ireplace('[[comp_accid]]',$data['CUST_ID'],$template);
	    
	    $template = str_ireplace('[[buser_name]]',$data['USER_FULLNAME'],$template);
	    $template = str_ireplace('[[user_login]]',$data['USER_ID'],$template);
	    $template = str_ireplace('[[user_email]]',$data['USER_EMAIL'],$template);
	    $template = str_ireplace('[[user_status]]','Rejected',$template);
	    
	    $template = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$template);
	    $template = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$template);
	    $template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);
	    
	    $email = $datacreated['USER_EMAIL'];
	    if($email!=''){
	    	$result = Application_Helper_Email::sendEmail($email,$subject,$template);
            }

        $deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges) return false;
		
		return true;
	}
	
    /**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
		
		
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_APIKEY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
             
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		$this->dbObj->delete('TEMP_CUSTOMER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		$this->dbObj->delete('TEMP_MAKERLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		$this->dbObj->delete('TEMP_FPRIVI_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteDelete() 
	{
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
		
	
	
	}
	
	
	
    public function approveDeactivate(){

	}

	public function approveActivate(){

	}
	
    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}
	
	
	
	
}