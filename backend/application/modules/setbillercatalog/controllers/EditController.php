<?php

require_once 'Zend/Controller/Action.php';

class billeraccount_EditController extends Application_Main
{
	public function indexAction() 
	{
		// min max untuk provider Code and Name Length
  		$min = 4;
		$max = 10;
		$model = new biller_Model_Biller();
		$providerID = $this->_getParam('providerID');
		$model 			 = new biller_Model_Biller();
		if(empty($providerID))
		{
			header("location: {$this->view->backURL}");exit();
		}
		$providerData 	 = $model->getDetailProvider($providerID);
		if(empty($providerData))
		{
			header("location: {$this->view->backURL}");exit();
		}
		else
		{
			$this->view->providerData = $providerData;
			$this->view->providerID = $providerID;
			
			$submit = $this->_getParam('submit');
			$cek = $model->isRequestChange($providerID);
			if(!$cek)
			{
				if($submit)
				{
					$error = 0;
					if($error == 0)
					{
						$this->_db->beginTransaction();
						try
						{
							$providerID 	 = $this->_getParam('providerID');
							$providerName 	 = $this->_getParam('providerName');
							  
							$info = 'Service Provider';
							$info2 = 'Update Biller';
							
							$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_SERVICE_PROVIDER','TEMP_SERVICE_PROVIDER',$providerData['PROVIDER_CODE'],$providerData['PROVIDER_NAME'],$providerData['PROVIDER_CODE']);
							
							unset($providerData['CHARGES']);
							$data= $providerData+array(
																				'CHANGES_ID' 			=> $change_id,
																				'PROVIDER_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
																				'PROVIDER_SUGGESTEDBY' 	=> $this->_userIdLogin
																			);
							$data['PROVIDER_NAME']	= $providerName;

							$getServiceType 	 = $model->getServiceType();
							if(!empty($getServiceType))
							{
								foreach($getServiceType as $value)
								{
									$arrServiceType[trim($value['SERVICE_NAME'])] = $value['SERVICE_ID'];
								}
							}

							$arrProviderType = array('Payment' => 1,'Purchase' =>2);
							$data['PROVIDER_TYPE']	= $arrProviderType[$providerData['PROVIDER_TYPE']];
							$data['SERVICE_TYPE']		= $arrServiceType[trim($providerData['SERVICE_TYPE'])];
							$data['PROVIDER_STATUS']	= 1;// approved

							$model->insertTemp($data);
							Application_Helper_General::writeLog('UBIL','Update biller');
							
							if($error == 0)
							{
								$this->_db->commit();
								$this->setbackURL('/biller');
								$this->_redirect('/notification/submited/index');
							}
						}
						catch(Exception $e)
						{
							$this->_db->rollBack();
						}
					}		
				}
			}
			else
			{
				$docErr = "*No changes allowed for this record while awaiting approval for previous change";	
				$this->view->error = $docErr;
				$this->view->changestatus = "disabled";
			}
		}
	}
}
