<?php

require_once 'Zend/Controller/Action.php';

class Remittancebank_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
	    //pengaturan url untuk button back
			$this->_helper->layout()->setLayout('newlayout');
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}


    	$this->view->report_msg = array();

    	$selectcountry = $this->_db->select()
					->from('M_COUNTRY')
					->query()->fetchAll();
			// 	    $this->view->countryArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME'));

		if(!$this->_request->isPost())
		{
			$bank_code = $this->_getParam('bank_code');

			if($bank_code)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BANK_REMITTANCE'))
									 ->where("bank_code=?", $bank_code)
							                   );
			  if($resultdata)
			  {
			        $this->view->bank_code      = $resultdata['bank_code'];
					$this->view->bank_name      = $resultdata['bank_name'];
					$this->view->bank_address1  = $resultdata['bank_address1'];
					$this->view->bank_address2  = $resultdata['bank_address2'];
					$this->view->city_name   	= $resultdata['city_name'];
					$this->view->country_code   	= $resultdata['country_code'];
					$this->view->pob_number   	= $resultdata['pob_number'];
				    $this->view->countryArr = $selectcountry;
			  }
			}
			else
			{
			   $error_remark = 'Bank Code not found';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   //$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			   $this->_redirect('/'.$this->_request->getModuleName().'/index');
			}
		}
		else
		{
			$filters = array(
							 'bank_code' => array('StringTrim','StripTags','StringToUpper'),
							 'bank_name' => array('StringTrim','StripTags','StringToUpper'),
							 'bank_address1' => array('StringTrim','StripTags','StringToUpper'),
							 'bank_address2' => array('StringTrim','StripTags','StringToUpper'),
							 'city_name'=> array('StringTrim','StripTags','StringToUpper'),
							 'country_code'=> array('StringTrim','StripTags','StringToUpper'),
							 'pob_number' => array('StringTrim','StripTags','StringToUpper')
							);

			$validators = array(
								'bank_code' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>12)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 12 chars)'),
			                                                             )
													),
								'bank_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>105)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 105 chars)'),
																         )
														),
								'bank_address1'      => array('allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>140)),
													 'messages' => array(
																         $this->language->_('Data too long (max 140 chars)'),
																         )
														),
								'bank_address2'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>140)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 140 chars)'),
																         )
														),
								'city_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 35 chars)'),
																         )
														),
								'country_code'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>3)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 3 chars)'),
																         )
														),
								'pob_number'      => array('allowEmpty'=>true,//'NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																		//$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 35 chars)'),
																         )
														)
							   );

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				$content = array(
								'bank_name' 	 => $zf_filter_input->bank_name,
								'bank_address1' => $zf_filter_input->bank_address1,
								'bank_address2'	 => $zf_filter_input->bank_address2,
 								'city_name' 	 => $zf_filter_input->city_name,
 								'pob_number' => $zf_filter_input->pob_number,
 								'country_code' 	 => $zf_filter_input->country_code,
 								'updated'	=> date('Y-m-d H:i:s'),
 								'updatedby' => $this->_userIdLogin
						       );

				try
				{
				    //-----insert--------------
					$this->_db->beginTransaction();

					$whereArr  = array('bank_code = ?'=>$zf_filter_input->bank_code);
					$this->_db->update('M_BANK_REMITTANCE',$content,$whereArr);

					$this->_db->commit();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					Application_Helper_General::writeLog('RBUD','Edit Remittance Bank. Bank Code : ['.$zf_filter_input->bank_code.']');
					$this->view->success = true;
					$this->view->report_msg = array();

					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

				    foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);
					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$this->view->countryArr = $selectcountry;

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
	}

}
