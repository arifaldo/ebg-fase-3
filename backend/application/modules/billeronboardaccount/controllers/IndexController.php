<?php


require_once 'Zend/Controller/Action.php';


class billeronboardaccount_IndexController extends Application_Main
{
	public function indexAction() 
	{
		$model 			 = new billeronboard_Model_Billeronboard();
		$billerCharges = new setbilleronboardcharges_Model_Setbilleronboardcharges();
		$providerArr 	 = $billerCharges->getServiceProviderAcc();
    	$options = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));
		unset($options[3]);
		array_unshift($options,'---'.$this->language->_('Any Value').'---');
		foreach($options as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		
		//$this->view->options = $options;
		$this->view->options = $optpayStatusRaw;
		$this->view->var = $providerArr;	
		
		$fields = array	(
							'Biller Code'  			=> array	(
																	'field' => 'PROVIDER_CODE',
																	'label' => $this->language->_('E-Commerce Code'),
																	'sortable' => true
																),
							'Biller Name'  			=> array	(
																	'field' => 'PROVIDER_NAME',
																	'label' => $this->language->_('E-Commerce Name'),
																	'sortable' => true
																),
							'Account Number'  			=> array	(
																	'field' => 'ACCT_NO',
																	'label' => $this->language->_('Account Number'),
																	'sortable' => true
																),
							/*'Account Name'  			=> array	(
																	'field' => 'ACCT_NAME',
																	'label' => $this->language->_('Account Name'),
																	'sortable' => true
																),*/
							/*'Account CIF'  		=> array	(
																	'field' => 'ACCT_CIF',
																	'label' => $this->language->_('Account CIF'),
																	'sortable' => true
																),*/
							'Currency'  		=> array	(
																	'field' => 'CCY_ID',
																	'label' => $this->language->_('Currency'),
																	'sortable' => true
																),
							/*'Account Type'  		=> array	(
																	'field' => 'ACCT_TYPE',
																	'label' => $this->language->_('Account Type'),
																	'sortable' => true
																),*/
							'Type'  		=> array	(
																	'field' => 'ACCT_DESC',
																	'label' => $this->language->_('Type'),
																	'sortable' => true
																),
							'Status'  		=> array	(
																	'field' => 'ACCT_STATUS',
																	'label' => 'Status',
																	'sortable' => true
																),
							'Latest Suggestion'  		=> array	(
																	'field' => 'ACCT_SUGGESTED',
																	'label' => $this->language->_('Latest Suggestion'),
																	'sortable' => true
																),
							'Latest Suggester'  		=> array	(
																	'field' => 'ACCT_SUGGESTEDBY',
																	'label' => $this->language->_('Suggester'),
																	'sortable' => true
																),
							'Latest Approval'  		=> array	(
																	'field' => 'ACCT_UPDATED',
																	'label' => $this->language->_('Latest Approval'),
																	'sortable' => true
																),
							'Latest Approver'  		=> array	(
																	'field' => 'ACCT_UPDATEDBY',
																	'label' => $this->language->_('Approver'),
																	'sortable' => true
																),
						);
						
		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'billerCode'    	=> array('StripTags','StringTrim'),
	                       'billerName'  	=> array('StripTags','StringTrim','StringToUpper'),
	                       'status'  	=> array('StripTags','StringTrim'),
						   'suggestor'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'approver'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'fDateFrom'  	=> array('StripTags','StringTrim'),
						   'ufDateFrom'  	=> array('StripTags','StringTrim'),
						   'fDateTo'  		=> array('StripTags','StringTrim'),
						   'ufDateTo'  		=> array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array('filter' 		=> array(),
	                       'billerCode'    	=> array(),
	                       'billerName'  	=> array(),
	                       'status'  	=> array(),
						   'suggestor'  	=> array(),
						   'approver'  	=> array(),
						   'fDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'ufDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'ufDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );
	                      
		$fParam = array();	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $billerCode = $fParam['billerCode'] = html_entity_decode($zf_filter->getEscaped('billerCode'));
		$billerName = $fParam['billerName'] = html_entity_decode($zf_filter->getEscaped('billerName'));
		$status = $fParam['status'] = html_entity_decode($zf_filter->getEscaped('status'));
		$suggestor 	= $fParam['suggestor'] 	= html_entity_decode($zf_filter->getEscaped('suggestor'));
		$approver 	= $fParam['approver'] 	= html_entity_decode($zf_filter->getEscaped('approver'));
		$datefrom 	= $fParam['datefrom'] 	= html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$udatefrom 	= $fParam['udatefrom'] 	= html_entity_decode($zf_filter->getEscaped('ufDateFrom'));
		$dateto 	= $fParam['dateto'] 	= html_entity_decode($zf_filter->getEscaped('fDateTo'));
		$udateto 	= $fParam['udateto'] 	= html_entity_decode($zf_filter->getEscaped('ufDateTo'));
		
		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');
		
		$page = (Zend_Validate::is($page,'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $fParam['sortBy'] = $sortBy;
		$this->view->sortDir     = $fParam['sortDir'] = $sortDir;

		if($filter == $this->language->_('Set Filter'))
		{
			$this->view->fDateTo    = $dateto;
			$this->view->ufDateTo    = $udateto;
			$this->view->fDateFrom  = $datefrom;
			$this->view->ufDateFrom  = $udatefrom;
			 
			if(!empty($datefrom))
			{
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$fParam['datefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($dateto))
			{
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
			}
			if(!empty($udatefrom))
			{
				$FormatDate = new Zend_Date($udatefrom, $this->_dateDisplayFormat);
				$fParam['udatefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($udateto))
			{
				$FormatDate = new Zend_Date($udateto, $this->_dateDisplayFormat);
				$fParam['udateto']    = $FormatDate->toString($this->_dateDBFormat);
			}

		    if($billerCode)
		    {
	       		$this->view->billerCode = $billerCode;
		    }
		    
			if($billerName)
			{
	       		$this->view->billerName = $billerName;
			}
			
			if($status)
			{
	       		$this->view->status = $status;
			}
			
			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
		    }
			if($approver)
		    {
	       		$this->view->approver = $approver;
		    }
		}
		else
		{
			$fParam['billerCode'] = '';
			$fParam['billerName'] = '';
			$fParam['status'] = '';
			$fParam['suggestor'] 	= '';
			$fParam['approver'] 	= '';
			$fParam['datefrom'] 	= '';
			$fParam['udatefrom'] 	= '';
			$fParam['dateto'] 	= '';
			$fParam['udateto'] = '';
		}

		$data = $model->getBillerAcc($fParam,$sortBy,$sortDir,$filter);
		
    	$this->paging($data);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	if($csv || $pdf || $this->_request->getParam('print'))
    	{
    		$arr = $data;
    		
    		foreach($arr as $key=>$value)
			{
				unset($arr[$key]["PROVIDER_ID"]);
				unset($arr[$key]["ACCT_CIF"]);
				unset($arr[$key]["ACCT_TYPE"]);
				$arr[$key]["ACCT_UPDATED"] = Application_Helper_General::convertDate($value["ACCT_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$arr[$key]["ACCT_SUGGESTED"] = Application_Helper_General::convertDate($value["ACCT_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
    		// Zend_Debug::dump($arr);die;
	    	if($csv)
			{
				Application_Helper_General::writeLog('VEPA','Download CSV E-Commerce Account');
				$this->_helper->download->csv(array_keys($fields),$arr,null,'E-Commerce Account List');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('VEPA','Download PDF E-Commerce Account');
				$this->_helper->download->pdf(array_keys($fields),$arr,null,'E-Commerce Account List');
			}
    		if($this->_request->getParam('print') == 1){
				$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'E-Commerce Account List', 'data_header' => $fields));
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('VEPA','View E-Commerce Account');
    	}
	}
}