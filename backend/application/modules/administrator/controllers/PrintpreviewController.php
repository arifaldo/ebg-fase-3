<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class Administrator_PrintpreviewController extends Application_Main
{
  public function indexAction() 
  { 
  	$userid = $this->_getParam('userid');
  	$select = $this->_db->select()
			->from(array('A' => 'M_BUSER'),array('BUSER_NAME','BUSER_ID','BUSER_CLEARTEXT_PASSWORD','BUSER_EMAIL','BUSER_ISNEW'));
	$select->where("BUSER_ID LIKE ".$this->_db->quote($userid));
 	$result = $this->_db->fetchRow($select);
 	$password = substr(base64_decode($result['BUSER_CLEARTEXT_PASSWORD']),4, -4);
 	//Zend_Debug::dump($result); die;
  	$setting = new Settings();
  	
  	$templateEmailMasterBankAddress = $setting->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $setting->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $setting->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $setting->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
	$templateEmailMasterBankEmail1 = $setting->getSetting('master_bank_email1');
	$templateEmailMasterBankFax = $setting->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $setting->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $setting->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $setting->getSetting('master_bank_website');
		
	if($result['BUSER_ISNEW'] == 1)
	{
		$template = $setting->getSetting('btemplate_newuser');
	}
	else
	{
		$template = $setting->getSetting('btemplate_resetpwd');
	}

	$template = str_ireplace('[[buser_name]]',$result['BUSER_NAME'],$template);
 	$template = str_ireplace('[[buser_login]]',$result['BUSER_ID'],$template);
 	$template = str_ireplace('[[buser_cleartext_password]]',$password,$template);
 	$template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);
	$this->view->template = $template;
	
	$updatesugStatus['BUSER_RPASSWORD_ISPRINTED'] = 1;
  	$wheresugStatus['BUSER_ID = ?'] = $userid;
  	$query = $this->_db->update ( "M_BUSER", $updatesugStatus, $wheresugStatus );
	//Zend_Debug::dump($template); die;
	
  		if(!$this->_request->isPost()){
				Application_Helper_General::writeLog('ADRL','Print Password Backend User Account  (' .$userid. ')');
		}
		
  }
}