<?php
//die('here');

use function PHPSTORM_META\type;

require_once(realpath(dirname(__FILE__) . '/zf-cli.php'));
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'CMD/Payment.php';
require_once 'General/Settings.php';
//require_once 'SGO/Extendedmodule/Sendgrid/sendgrid-php.php';


//$thisClass = $this;
$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();

$datapers = array();
$logName = 'Service_SMS::sendSMS';
$pathFile = null;
$printToDisplay = true;
$logFileType = "ALL";




$loggerService = SGO_Extendedmodule_Loggerservice_Service::getInstance();
$logger = $loggerService->getLogger($logName, $pathFile, $printToDisplay, $logFileType);
//var_dump($logger);
//var_dump($queueService);die;
$sms = array();
$callback = function ($sms) use ($logger, $queueService) {




	$request = json_decode($sms->body, true);

	//$Payment = new Payment($request['PS_NUMBER'], $request['CUST_ID'], 'System');

	$subject = $request['subject'];
	$to = $request['to'];
	$message = $request['message'];
	$log = $request['log'];
	//var_dump($request);die;
	//var_dump($emailList); 
	//var_dump($sendTransfer);
	//var_dump($transactionData);die;


	$Settings = new Settings();
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');

	$startSend = date("Y-m-d H:i:s");
	$config = Zend_Registry::get('config');
	$fromName = $templateEmailMasterBankAppName;
	$config = Zend_Registry::get('config');
	$username = $config['email']['username'];
	//$bankName = $config['app']['bankname'];
	//$appName = $config['app']['name'];
	$bankName = $templateEmailMasterBankName;
	$appName = $templateEmailMasterBankAppName;


	// $subject = "$bankName $appName - ".$subject;
	$subject = $subject;


	// $log = $transport->getConnection()->getLog();
	$dbObj =  Zend_Db_Table::getDefaultAdapter();




	//$email = new \SendGrid\Mail\Mail();

	$toArr = explode(";", $to);
	if (empty($toArr[1])) {
		$toArr = explode(",", $to);
	}
	foreach ($toArr as $to) {
		//var_dump($configdata['email']['smtpserver']);
		//var_dump($config);
		$Settings = new Settings();
		$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
		$config = Zend_Registry::get('config');
		$smtpServer = $config['email']['smtpserver'];
		$smtpPort	= $config['email']['smtpport'];
		$username = $config['email']['username'];
		// $user = $config['email']['user'];
		$password = $config['email']['password'];
		$fromName = $templateEmailMasterBankAppName;

		//var_dump($smtpServer);
		$transport = new Zend_Mail_Transport_Smtp($smtpServer, $config);
		$mail = new Zend_Mail();
		$to = trim($to);
		$mail->setFrom($username, $fromName);
		$mail->setSubject($subject);
		$mail->addTo($to, '');
		$mail->SMTPAuth = true;
		// $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
		//$mail->addContent(
		//   "text/html", $message
		//);
		$mail->setBodyHtml($message);
		// $enc = getenv('SENDGRID_API_KEY');
		$config = Zend_Registry::get('config');
		//$key = $configdata['email']['sendkey'];
		// var_dump($enc);die;
		// $sendgrid = new \SendGrid('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');
		//$sendgrid = new \SendGrid($key);
		//var_dump($message);die;
		try {
			$emailStatus = 1;
			//$response = $sendgrid->send($email);
			//var_dump($to);die;
			$mail->send($transport);
			// $mail->send();

			// print $response->statusCode() . "\n";
			// Zend_Debug::dump($response->headers());
			// print $response->body() . "\n";


		} catch (Exception $e) {
			//var_dump($e);die;
			echo 'Caught exception: ' . $e->getMessage() . "\n";
			$emailStatus = 0; //die;
			//var_dump($e);die;
			// echo '1231231';
		}
		//die('herte');

		$log = $transport->getConnection()->getLog();

		$dataInsert = [
			// 'EMAIL_DATE' => new Zend_Db_Expr('now()'),
			'EMAIL_DATE' => date('d-m-Y H:i:s'),
			'EMAIL_SENDER' => $username . ' ' . $fromName,
			'EMAIL_RECEIVER' => $to,
			'EMAIL_CONTENT' => $message,
			'EMAIL_STATUS' => $emailStatus,
			'EMAIL_TYPE' => $startSend . 'SMTP',
			'SMTP_RESPONSE' =>  str_replace(['\n', '\r'], '', $log)
		];

		$today = date("Ymd");
		$nametext = $today . '.txt';
		$path_file = LIBRARY_PATH . '/data/logs/email/';

		$dirname = dirname($path_file . $nametext);

		if (!is_dir($dirname)) {
			mkdir($dirname, 0755, true);
		}

		$fp = fopen($path_file . $nametext, 'a'); //opens file in append mode
		fwrite($fp, json_encode($dataInsert));
		fwrite($fp, PHP_EOL);
		fclose($fp);

		shell_exec('chown nginx ' . $path_file);
		shell_exec('chown nginx ' . $path_file . $nametext);

		// $dbObj->insert('T_EMAIL_LOG', $dataInsert);
	}
};



//$queueConsumer = '';

$queueConsumer = $queueService->getQueueByProfileName("SEND_EMAIL");

try {
	$queueConsumer->waitAndProcess($callback);
} catch (Exception $e) {
	var_dump($e);
	$param = [];
	Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e->getMessage(), NULL, FALSE), $param);
	SGO_Helper_Supervisor::doExitByExitCodeName("RESTART");
}
