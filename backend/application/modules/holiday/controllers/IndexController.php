<?php

class Holiday_IndexController extends Application_Main {
	
	protected $_moduleDB  = 'HTB';
	
	public function indexAction(){
		$this->_helper->layout()->setLayout('newlayout');
		

		//get year data from table
//		if($this->_request->isPost()){
//			
//			if($this->_getParam('year')){
//				$year = $this->_getParam('year');
//			}else{
//				$year = date("Y");
//			}
//			Zend_Debug::Dump($this->_getAllParams()); die;
//			if($this->_getParam('year')){
//				
//			}
//			}else{
//				$year = date("Y");
//		}

			if($this->_getParam('year')){
				$year = $this->_getParam('year');
			}else{
				$year = date("Y");
				$this->_setParam('year',$year);
			}

			if($this->_getParam('yearccy')){
				$yearccy = $this->_getParam('yearccy');
			}else{
				$yearccy = date("Y");
				$this->_setParam('yearccy',$yearccy);
			}
			
			$filters = array('year' => array('StringTrim','StripTags'));
			
			$validators = array('year' => array(	'NotEmpty',
													'Digits',
													array('Db_NoRecordExists', array('table' => 'TEMP_HOLIDAY', 'field' => 'HOLIDAY_YEAR')),
													'messages' => array(
														'Cannot be empty',
														'Invalid format',
														'No changes allowed for this record while awaiting approval for previous change.')
												));
			
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				if($this->_request->isPost() && $this->view->hasPrivilege('HDUD')){

					$this->view->isedited=0;
					if($this->_getParam("m")=='holidaysave')
					{	
						$this->_db->beginTransaction();
						$holiday_date_arr=explode('~|~',$this->_getParam("dates"));
						$holiday_desc_arr=explode('~|~',$this->_getParam("datesdesc"));
						$year = $this->_getParam("year");

						// holiday ccy
						$holidayccy_date_arr=explode('~|~',$this->_getParam("datesccy"));
						$holidayccy_desc_arr=explode('~|~',$this->_getParam("datesdescccy"));
						$holidayccy_ccyid_arr=explode('~|~',$this->_getParam("ccyid"));

						// var_dump($holidayccy_desc_arr);
						// var_dump($holidayccy_ccyid_arr);die;


						if(is_array($holiday_date_arr) || is_array($holidayccy_date_arr))
						{
							  // insert ke T_GLOBAL_CHANGES
							try{
								$info = "";
								$change_id = $this->suggestionWaitingApproval('Holiday Setting',$info,$this->_changeType['code']['edit'],null,'M_HOLIDAY','TEMP_HOLIDAY','',$year);
								$i=0;
								if($holiday_date_arr[0] !=null)
								{
									foreach($holiday_date_arr as $holidate) 
									{
										$data = array(	'CHANGES_ID' => $change_id,
															'HOLIDAY_YEAR' => $year,
															'HOLIDAY_DATE' => $holidate,
															'HOLIDAY_DESCRIPTION' => $holiday_desc_arr[$i],
														);
										$this->_db->insert('TEMP_HOLIDAY',$data);
										$i++;
									}
								}

								$j=0;
								if($holidayccy_date_arr[0] !=null)
								{
									foreach($holidayccy_date_arr as $holidateccy) 
									{
										$data = array(	'CHANGES_ID' => $change_id,
															'HOLIDAY_YEAR' => $year,
															'HOLIDAY_DATE' => $holidateccy,
															'HOLIDAY_DESCRIPTION' => $holidayccy_desc_arr[$j],
															'HOLIDAY_CCY' => $holidayccy_ccyid_arr[$j],
														);
														// var_dump($data);die;
										$this->_db->insert('TEMP_HOLIDAY',$data);
										$j++;
									}
								}
								Application_Helper_General::writeLog('HDUD','Update Holiday Setting');
								$this->_db->commit();						    
								$this->view->success = true;
								$msg = 'Holiday changes saved';
								$this->setbackURL('/holiday');
								$this->_redirect('/notification/submited/index');
								$this->view->report_msg = $msg;
							}
							catch(Exception $e) 
							{
								$this->_db->rollBack();					
							}
						}
					}

					// if($this->_getParam("mccy")=='holidaysave')
					// {	
					// 	$this->_db->beginTransaction();
					// 	$holiday_date_arr=explode('~|~',$this->_getParam("datesccy"));
					// 	$holiday_desc_arr=explode('~|~',$this->_getParam("datesdescccy"));
					// 	$holiday_ccyid_arr=explode('~|~',$this->_getParam("ccyid"));
					// 	$year = $this->_getParam("yearccy");

					// 	// var_dump($holiday_date_arr);
					// 	// var_dump($holiday_desc_arr);
					// 	// var_dump($holiday_ccyid_arr);
					// 	// die;

					// 	if(is_array($holiday_date_arr))
					// 	{
					// 		  // insert ke T_GLOBAL_CHANGES
					// 		try{
					// 			$info = "";
					// 			$change_id = $this->suggestionWaitingApproval('Holiday Setting',$info,$this->_changeType['code']['edit'],null,'M_HOLIDAY','TEMP_HOLIDAY','',$year);
					// 			$i=0;
					// 			if($holiday_date_arr[0] !=null)
					// 			{
					// 				foreach($holiday_date_arr as $holidate) 
					// 				{
					// 					$data = array(	'CHANGES_ID' => $change_id,
					// 										'HOLIDAY_YEAR' => $year,
					// 										'HOLIDAY_DATE' => $holidate,
					// 										'HOLIDAY_DESCRIPTION' => $holiday_desc_arr[$i],
					// 										'HOLIDAY_CCY' => $holiday_ccyid_arr[$i],
					// 									);
					// 					$this->_db->insert('TEMP_HOLIDAY',$data);
					// 					$i++;
					// 				}
					// 			}
					// 			Application_Helper_General::writeLog('HDUD','Update Holiday Setting');
					// 			$this->_db->commit();						    
					// 			$this->view->success = true;
					// 			$msg = 'Holiday changes saved';
					// 			$this->setbackURL('/holiday');
					// 			$this->_redirect('/notification/submited/index');
					// 			$this->view->report_msg = $msg;
					// 		}
					// 		catch(Exception $e) 
					// 		{
					// 			$this->_db->rollBack();					
					// 		}
					// 	}
					// }


				}else{
				$year = date("Y");
				$yearccy = date("Y");
				}
			}
			else
			{
				$this->view->isedited=1;
				$this->view->error = true;
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
			}

		$this->view->year =  $year;
		$this->view->yearccy =  $yearccy;
		
		$sqlCheckTemp=
		"SELECT *
		FROM TEMP_HOLIDAY";
		$listTemp = $this->_db->fetchAll($sqlCheckTemp,'');
		
		$sqlCheckTempChange=
		"SELECT *
		FROM T_GLOBAL_CHANGES WHERE DISPLAY_TABLENAME = 'Holiday Setting' AND CHANGES_STATUS = 'WA' ";
		$listTempChange = $this->_db->fetchAll($sqlCheckTempChange,'');
		
		
		if(!empty($listTemp) || !empty($listTempChange)){
		    $tempcheck = true;
		}else{
		    $tempcheck = false;
		}
		
		$this->view->tempcek = $tempcheck;
		
		$sqlQueryHoliday=
		"SELECT *,
		DAY(HOLIDAY_DATE) AS day,
		MONTH(HOLIDAY_DATE) AS month,
		YEAR(HOLIDAY_DATE) AS year
		FROM M_HOLIDAY WHERE HOLIDAY_YEAR= ?";
		$listHoliday = $this->_db->fetchAll($sqlQueryHoliday,$year);
		$holiday = array();
		foreach($listHoliday as $key => $val){
			if($val['HOLIDAY_CCY'] != NULL){
				continue;
			}
			$holiday[$key] = $val;
		}
		$this->view->listHoliday = $holiday;
		// var_dump($holiday);
	
		$hccy = array();
		foreach($listHoliday as $keys => $valu){
			if($valu['HOLIDAY_CCY'] == NULL){
				continue;
			}
			$hccy[$keys] = $valu;
		}
		// var_dump($hccy);
		$this->view->listHolidayccy = $hccy;
		
		// $select2 = $this->_db->select()
        //                   ->from(array('BG' => 'M_CURRENCY'),array('*'))
		// 				  ->query()->fetchAll();

		// echo "<pre>";
		// var_dump($select2);

		$getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;
		// var_dump($getCcy);

		if(!$this->_request->isPost()){
			Application_Helper_General::writeLog('HDLS','View Holiday Setting');
		}
	}
}

?>
