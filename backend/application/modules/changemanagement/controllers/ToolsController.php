<?php
require_once 'Zend/Controller/Action.php';

class Changemanagement_ToolsController extends Application_Main
{

	public function messageAction()
	{
		$changes_id = $this->_getParam('changes_id');


		$caseWhenRequestChangeList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN 'Request Repaired'
		WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN 'Unread Suggestion'
		WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN 'Read Suggestion'
		WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN 'Repaired Suggestion'
		END
		";

		$select = $this->_db->select()
			->from(
				array('G' => 'T_GLOBAL_CHANGES'),
				array(
					'*',
					'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)
				)
			)
			->where('G.CHANGES_ID= ?', $changes_id)
			->query()->fetch();

		$this->view->module_name = $select['MODULE'];
		$this->view->changes_id  = $changes_id;
		$this->view->dataChanges = $select;
		$this->view->refreshParentScript = false;

		$this->view->createdby = $select['CREATED_BY'];
		$this->view->created = $select['CREATED'];
		$this->view->grantedby = '';
		$this->view->granted = '';
		$this->view->rejectedby = '';
		$changesTypeCodeArr   = array_flip($this->_suggestType['code']);
		$this->view->suggestionType   = ucfirst($changesTypeCodeArr[$select['CHANGES_TYPE']]);
		if ($select['CHANGES_TYPE'] == 'F') {
			$this->view->suggestionType   = ucfirst("Freeze");
		}
		if ($select['CHANGES_TYPE'] == 'K') {
			$this->view->suggestionType   = ucfirst("Unfreeze");
		}
		if ($select['CHANGES_TYPE'] == 'T') {
			$this->view->suggestionType   = ucfirst("Terminate");
		}
		$this->view->status = $select['suggestion_status'];
		if ($select['suggestion_status'] != 'Repaired Suggestion') {
			$this->view->hidenote = 'none';
		}
		$this->view->repairNote = $select['CHANGES_REASON'];
		$success = true;

		$this->view->rejected = '';
		if ($this->_request->isPost()) {
			$action  = $this->_getParam('submit');
			$note =  $this->_getParam('detailnote');
			$gcmodel = new Changemanagement_Model_Globalchanges($changes_id);
			if ($action == $this->language->_('Approve')) {
				$updated = $gcmodel->approve($this->_userIdLogin, $note);
				$message = $this->language->_('Suggestion Granted');
				$this->view->grantedby = $this->_userIdLogin;
				$this->view->granted = $select['LASTUPDATED'];
				$this->view->status = 'Granted Suggestion';
			} elseif ($action == $this->language->_('Reject')) {
				$updated = $gcmodel->reject($this->_userIdLogin, $note);
				$message = $this->language->_('Suggestion Rejected');

				$this->view->rejectedby = $this->_userIdLogin;
				$this->view->rejected = $select['LASTUPDATED'];
				$this->view->status = 'Rejected Suggestion';
			} elseif ($action == $this->language->_('Request Repair')) {
				$updated = $gcmodel->requestRepair($this->_userIdLogin, $note);
				$message = $this->language->_('Suggestion Repair Requested');
				$this->view->status = 'Request Repair';
			}

			//jika proses gagal maka cetak errornya :
			if (!$updated) {
				if ($gcmodel->getErrorMessage() != "Query failed(Customer)") {
					$message = $gcmodel->getErrorMessage();
					$success = false;
				}
			}

			$this->view->message = $message;
			$this->view->success = $success;
		} else {
			$this->_setReadSuggestion($changes_id);
			$this->view->message = '';
			$this->view->refreshParentScript == true;
		}
	}

	public function indexAction()
	{
		$changes_id = $this->_getParam('changes_id');
		$select = $this->_db->select()
			->from(array('d' => 'T_GLOBAL_CHANGES'))
			->where('d.CHANGES_ID= ?', $changes_id)
			->query()->fetch();




		//echo '<pre>';
		//var_dump($select);die;
		$liststatus = array('S', 'U', 'L');
		$this->view->hidenote = true;
		if (in_array($select['CHANGES_TYPE'], $liststatus, true)) {
			$this->view->hidenote = false;
		}

		$this->view->module_name = $select['MODULE'];

		$isAuthorizeAppove =  false;
		$isAuthorizeReject =  false;
		$isAuthorizeRequestRepair =  false;
		if (!empty($this->_priviId)) {
			$changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
			$isAuthorizeAppove = $changeModulePrivilegeObj->isAuthorizeApprove($select['MODULE']);
			$isAuthorizeReject = $changeModulePrivilegeObj->isAuthorizeReject($select['MODULE']);
			$isAuthorizeRequestRepair = $changeModulePrivilegeObj->isAuthorizeRequestRepair($select['MODULE']);
			$isAuthorizeRepair = $changeModulePrivilegeObj->isAuthorizeRepair($select['MODULE']);
		}

		$this->view->changes_id  = $changes_id;
		$this->view->dataChanges = $select;
		$this->view->visibleButtonApprove = $isAuthorizeAppove;
		$this->view->visibleButtonReject = $isAuthorizeReject;
		$this->view->visibleButtonRequestRepair = $isAuthorizeRequestRepair;
		$this->view->visibleButtonRepair = $isAuthorizeRepair;

		$this->view->refreshParentScript = false;
		if ($this->_request->isPost()) {
			$action  = $this->_getParam('submit');
			$note =  $this->_getParam('detailnote');
			if ($action == $this->language->_('Approve')) {

				//check kondisi apabila modulnya backend griup dan perubahannya SUSPEN / DELETE user login grup tersebut akan terlogout
				if ($select['MODULE'] == 'backendgroup' && $select['CHANGES_TYPE'] == "S") {
					$data2  = [
						'BUSER_ISLOGIN'	=> 0,
					];
					$where2 = ['BGROUP_ID = ?' => $select['KEY_FIELD']];
					$this->_db->update('M_BUSER', $data2, $where2);
				} else if ($select['MODULE'] == 'backendgroup' && $select['CHANGES_TYPE'] == "L") {
					$data2  = [
						'BUSER_ISLOGIN'	=> 0,
					];
					$where2 = ['BGROUP_ID = ?' => $select['KEY_FIELD']];
					$this->_db->update('M_BUSER', $data2, $where2);
				}

				$message = $this->language->_('Suggestion Granted');
			} elseif ($action == $this->language->_('Reject')) {
				$message = $this->language->_('Suggestion Rejected');
			} elseif ($action == $this->language->_('Request Repair')) {
				$message = $this->language->_('Suggestion Repair Requested');
			}
			$this->view->message = $message;
			$this->view->refreshParentScript = true;
		}

		if ($select['CHANGES_STATUS'] == 'AP') {
			$this->view->message = '';
		}
	}

	private function _setReadSuggestion($changesId)
	{
		$data = $this->_db->update('T_GLOBAL_CHANGES', array('READ_STATUS' => 1), $this->_db->quoteInto('CHANGES_ID = ?', $changesId));
	}
}
