<?php

Class useropenlimit_Model_Userlimit
{

  public function __construct()
  {
    $this->_db = Zend_Db_Table::getDefaultAdapter();
  }

  public function getUser($fParam,$sorting)
  {
    $select = $this->_db->select()
                           ->from(array('M'=>'M_MAKERLIMIT'),array('*'))
                           ->joinLeft(array('MU'=>'M_USER'),'MU.USER_ID=M.USER_LOGIN AND MU.CUST_ID=M.CUST_ID',array('MU.USER_FULLNAME'))
                           ->joinLeft(  array(  'D' => 'M_BANK_TABLE' ),'M.BANK_CODE = D.BANK_CODE',array('D.BANK_NAME'))
						   ->joinLeft(  array(  'CS' => 'M_CUSTOMER_ACCT' ),'M.ACCT_NO = CS.ACCT_NO',array('ACTNAME' => 'CS.ACCT_NAME','CS.ACCT_ALIAS_NAME'))
               ->where('M.MAXLIMIT != ?','0.00')
                           ->where('UPPER(M.CUST_ID)='.$this->_db->quote((string)$fParam['cust_id']))
                           ->order('M.SUGGESTED DESC');
                           // ->where('USER_STATUS = 1');

    if(isSet($fParam['userid']))
    { 
      // $select->where("UPPER(USER_ID) LIKE ".$this->_db->quote('%'.$fParam['userid'].'%'));
      $select->where("UPPER(USER_ID)  in (?)", $fParam['userid'] ); 
    }

    if(isSet($fParam['bankname']))
    { 
      // $select->where("M.BANK_CODE LIKE ".$this->_db->quote('%'.$fParam['bankname'].'%'));
      $select->where("M.BANK_CODE  in (?)", $fParam['bankname'] ); 
    }

    if(isSet($fParam['uname']))
    { 
      // $select->where("UPPER(USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['uname'].'%')); 
      $select->where("UPPER(USER_FULLNAME)  in (?)", $fParam['uname'] );
    }
    
    if(isSet($fParam['acctnum']))
    { 
      // $select->where("ACCT_NO LIKE ".$this->_db->quote('%'.$fParam['acctnum'].'%'));
      $select->where("ACCT_NO  in (?)", $fParam['acctnum'] ); 
    }

    if(isSet($fParam['suggeststart']))
    {
      $select->where('date(USER_SUGGESTED) >= ?', $fParam['suggeststart']);
    }
    
    if(isSet($fParam['suggestend']))
    {
      $select->where('date(USER_SUGGESTED) <= ?', $fParam['suggestend']);
    }
    
    if(isSet($fParam['approvestart']))
    {
      $select->where('date(USER_UPDATED) >= ?', $fParam['approvestart']);
    }
    
    if(isSet($fParam['approveend']))
    {
      $select->where('date(USER_UPDATED) <= ?', $fParam['approveend']);
    }

    if($sorting)
      $select->order($sorting);
	//echo $select;
    return $this->_db->fetchAll($select);
  }
  
}




