<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgrequest_DetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');
		$this->view->systemType = $system_type;

		$conf = Zend_Registry::get('config');
		$this->_bankName = $conf['app']['bankname'];
		$this->view->masterbankname = $this->_bankName;


		// $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

		$bgRegNumb = $this->_getParam('bg_regnumb');

		if (!empty($bgRegNumb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $bgRegNumb)
				->query()->fetchAll();

			$RandBgnumber = $this->generateBGNumber($bgdata['0']['BG_BRANCH'], $bgdata['0']['USAGE_PURPOSE']);
			$this->view->RandBgnumber = $RandBgnumber;

			$bgdatadetail = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $bgRegNumb)
				->query()->fetchAll();


			$dataselect = $bgdata['0'];
			$selectcomp = $this->_db->select()
				->from(array('A' => 'M_CUSTOMER'), array('*'))
				->where('A.CUST_ID = ?', $dataselect['CUST_ID'])
				->query()->fetchAll();

			$this->view->compinfo = $selectcomp[0];

			if (!empty($bgdata)) {

				$data = $bgdata['0'];

				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					//var_dump($selectbranch[0]['BRANCH_NAME']);die;
					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);


				$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

				$this->view->arrStatus = $arrStatus;

				$arrBankFormat = array(
					1 => 'Bank Standard',
					2 => 'Special Format (with bank approval)'
				);

				$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

				if ($data['BG_STATUS'] == '7' || $data['BG_STATUS'] == '11' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$selectQuery  = "SELECT
                      a.USER_LOGIN,
                      b.`USER_FULLNAME` AS u_name,
                      c.`BUSER_NAME` AS b_name,
                      a.DATE_TIME,
                      a.BG_REASON,
                      a.HISTORY_STATUS,
                      a.BG_REG_NUMBER
                      
                      
                    FROM
                      T_BANK_GUARANTEE_HISTORY AS a
                      LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                      LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                    WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS IN (10) GROUP BY HISTORY_ID ORDER BY DATE_TIME";
					if ($data['BG_STATUS'] == '10') {
						$result =  $this->_db->fetchAll($selectQuery);
					} else {
						$result = array();
					}
					if (!empty($result)) {
						$this->view->reqrepair = true;
						$data['REASON'] = $result['0']['BG_REASON'];
						$this->view->username = ' (By ' . $result['0']['u_name'] . '' . $result['0']['b_name'] . ')';
					}
					//var_dump($result);die;


					$this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
				}

				$arrLang = array(
					1 => 'Indonesian',
					2 => 'English',
					3 => 'Billingual',
				);

				$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

				$arrWaranty = array(
					1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
					2 => 'Bank Guarantee Line Facility',
					3 => 'Insurance'

				);

				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

				if (!empty($data['USAGE_PURPOSE'])) {
					$data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
					foreach ($data['USAGE_PURPOSE'] as $key => $val) {
						$str = 'checkp' . $val;
						//var_dump($str);
						$this->view->$str =  'checked';
					}
				}

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];

				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->comment = $data['GUARANTEE_TRANSACTION'];

				$this->view->fileName = $data['FILE'];
				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];
				$this->view->status = $data['BG_STATUS'];

				$CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);
				$param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
				// $param = array('CCY_IN' => 'IDR');
				$AccArr = $CustomerUser->getAccounts($param);
				// var_dump($AccArr);

				if (!empty($AccArr)) {
					$this->view->src_name = $AccArr['0']['ACCT_NAME'];
				}

				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Currency') {
								$this->view->Currency =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}


				$conf = Zend_Registry::get('config');
				$this->view->bankname = $conf['app']['bankname'];



				$download = $this->_getParam('download');

				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				$approve            = $this->_getParam('approve');
				$reject             = $this->_getParam('reject');
				$repair             = $this->_getParam('repair');

				if ($reject) {
					$rejectNotes = $this->_getParam('rejectNotes');

					try {
						$this->_db->beginTransaction();

						$dataReject =  array(
							'BG_STATUS' 		=> '11',
							'BG_REJECT_NOTES'	=> $rejectNotes,
							'BG_UPDATED' 		=> date('Y-m-d H:i:s'),
							'BG_UPDATEDBY' 		=> $this->_userIdLogin
						);

						$where = array('BG_REG_NUMBER = ?' => $bgRegNumb);
						$result = $this->_db->update('T_BANK_GUARANTEE', $dataReject, $where);
						$this->_db->commit();
						// Application_Helper_General::writeLog('RJLO','Reject Bank Guarantee '.$bgRegNumb);	



						// ---------------------------------- EMAIL NOTIF -----------------------------------------------

						$bankguarantee = $this->_db->select()
							->from('T_BANK_GUARANTEE')
							->where('BG_REG_NUMBER = ?', $bgRegNumb)
							->query()
							->fetch(Zend_Db::FETCH_ASSOC);


						$customer = $this->_db->select()
							->from('M_CUSTOMER')
							->where('CUST_ID = ?', $bankguarantee['CUST_ID'])
							->query()
							->fetch(Zend_Db::FETCH_ASSOC);


						// ------------- BG HISTORY --------------
						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'         => $bankguarantee['BG_REG_NUMBER'],
							'CUST_ID'           => $bankguarantee['CUST_ID'],
							'USER_LOGIN'        => $this->_userIdLogin,
							'HISTORY_STATUS'    => 11,
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
						//--------------- END BG HISTORY ---------------------

						$setting 	= new Settings();
						$template						= $setting->getSetting('femailtemplate_rejectbgnotification', '');
						$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name', '');
						$templateEmailMasterBankAppName 	= $setting->getSetting('master_bank_app_name', '');
						$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp', '');
						$templateEmailMasterBankWapp 	= $setting->getSetting('master_bank_wapp', '');
						$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email', '');
						$templateEmailMasterBankEmail1 	= $setting->getSetting('master_bank_email1', '');
						$date 							= date("d M Y H:i:s");
						$config    		= Zend_Registry::get('config');
						$bgstatus 	= $config["bg"]["status"]["desc"];
						$bgstatusCode 	= $config["bg"]["status"]["code"];
						$bgstatusarr = array_combine(array_values($bgstatusCode), array_values($bgstatus));

						$config    		= Zend_Registry::get('config');
						$BgType 		= $config["bg"]["status"]["desc"];
						$BgCode 		= $config["bg"]["status"]["code"];

						$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

						//var_dump($template);die('heere');

						$amount = number_format($bankguarantee['BG_AMOUNT'], 2, ',', '.');
						$translate = array(
							'[[user_name]]'                 => $customer['CUST_NAME'],
							'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
							'[[master_bank_app_name]]' 		=> $templateEmailMasterBankAppName,
							'[[MASTER_BANK_TELP]]' 			=> $templateEmailMasterBankTelp,
							'[[MASTER_BANK_EMAIL]]' 		=> $templateEmailMasterBankEmail,
							'[[MASTER_BANK_WAPP]]' 			=> $templateEmailMasterBankWapp,
							'[[MASTER_BANK_EMAIL1]]' 		=> $templateEmailMasterBankEmail1,
							'[[cust_name]]'					=> $customer['CUST_NAME'],
							'[[bg_number]]'					=> $bankguarantee['BG_NUMBER'],
							'[[account_no]]'	 			=> $bankguarantee['ACCT_NO'],
							'[[subject]]' 					=> $bankguarantee['BG_SUBJECT'],
							'[[amount]]' 					=> 'IDR ' . $amount,
							'[[status]]' 					=> $arrStatus[$bankguarantee['BG_STATUS']],
							'[[startdate]]' 					=> $bankguarantee['TIME_PERIOD_START'],
							'[[duedate]]' 					=> $bankguarantee['TIME_PERIOD_END'],
						);

						$subject = 'BG NEW Notification';
						$useremail = $customer['CUST_EMAIL'];
						$mailContent = strtr($template, $translate);
						//var_dump($useremail);
						//var_dump($mailContent);die;
						Application_Helper_Email::sendEmail($useremail, $subject, $mailContent);
						// ---------------------------------- END EMAIL NOTIF -------------------------------------------



					} catch (Exception $e) {
						$this->_db->rollBack();
					}




					$this->setbackURL('/' . $this->_request->getModuleName());
					$this->_redirect('/notification/success');
				}

				if ($approve) {

					$bgNumber = $this->_getParam('bg_number');

					$attahmentDestination 	= UPLOAD_PATH . '/document/bg/';
					$adapter 				= new Zend_File_Transfer_Adapter_Http();

					$sourceFileName = $adapter->getFileName();

					if ($sourceFileName == null) {
						$sourceFileName = null;
						$fileType = null;


						$setting 	= new Settings();
						$escrowbg						= $setting->getSetting('escrow_bg', '');
						$param = array();

						$PS_CCY = 'IDR';
						$PS_EFDATE = date('Y-m-d');
						$param['PS_SUBJECT'] 					= $data['BG_SUBJECT'];
						$param['PS_CCY'] 						= 'IDR';
						$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);

						if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
							$param['TRANSFER_FEE'] 					= 0;
							$param['TRA_MESSAGE'] 					= $data['BG_REG_NUMBER'];
							$param['TRA_REFNO'] 					= NULL;
							$param['SOURCE_ACCOUNT'] 				= $data['COUNTER_WARRANTY_ACCT_NO'];
							$param['SOURCE_ACCOUNT_NAME'] 			= $data['COUNTER_WARRANTY_ACCT_NAME'];
							$param['BENEFICIARY_ACCOUNT'] 			= $escrowbg;
							$param['TRA_AMOUNT'] 					= $data['BG_AMOUNT'];
							$param['BENEFICIARY_ACCOUNT_CCY'] 		= 'IDR';
							$param['BENEFICIARY_ACCOUNT_NAME'] 		= 'ESCROW BG';
							$param['BENEFICIARY_ALIAS_NAME'] 		= NULL;
							$param['BENEFICIARY_EMAIL'] 			= NULL;
							$param['_addBeneficiary'] 				= $this->view->hasPrivilege('BADA');
							$param['_beneLinkage'] 					= $this->view->hasPrivilege('BLBU');
							$param['_priviCreate'] 					= 'CRSP';
							$param['sourceAccountType'] 			= '10';

							$param['RATE'] 							= '0';
							$param['RATE_BUY'] 						= '0';
							$param['BOOKRATE'] 						= '0';
							$param['BOOKRATE_BUY'] 					= '0';
							$param['TRA_AMOUNTEQ'] = '0';


							$SinglePayment = new SinglePayment($PS_NUMBER, $data['CUST_ID'], $data['BG_CREATEDBY']);
							$result = $SinglePayment->createPaymentWithinBG($param);
							$setting 	= new Settings();
							$chagesbg						= $setting->getSetting('charges_bg', '');

							$param['TRANSFER_FEE'] 					= 0;
							$param['TRA_MESSAGE'] 					= $data['BG_REG_NUMBER'];
							$param['TRA_REFNO'] 					= NULL;
							$param['SOURCE_ACCOUNT'] 				= $data['FEE_CHARGE_TO'];
							//$param['SOURCE_ACCOUNT_NAME'] 			= $data['COUNTER_WARRANTY_ACCT_NAME'];
							$param['BENEFICIARY_ACCOUNT'] 			= $escrowbg;
							$param['TRA_AMOUNT'] 					= $chagesbg;
							$param['BENEFICIARY_ACCOUNT_CCY'] 		= 'IDR';
							$param['BENEFICIARY_ACCOUNT_NAME'] 		= 'ESCROW BG';
							$param['BENEFICIARY_ALIAS_NAME'] 		= NULL;
							$param['BENEFICIARY_EMAIL'] 			= NULL;
							$param['_addBeneficiary'] 				= $this->view->hasPrivilege('BADA');
							$param['_beneLinkage'] 					= $this->view->hasPrivilege('BLBU');
							$param['_priviCreate'] 					= 'CRSP';
							$param['sourceAccountType'] 			= '10';
							$param['TRA_MESSAGE']					= $result;
							$param['RATE'] 							= '0';
							$param['RATE_BUY'] 						= '0';
							$param['BOOKRATE'] 						= '0';
							$param['BOOKRATE_BUY'] 					= '0';
							$param['TRA_AMOUNTEQ'] = '0';


							$SinglePayment = new SinglePayment($PS_NUMBER, $data['CUST_ID'], $data['BG_CREATEDBY']);
							$result = $SinglePayment->createPaymentWithinBG($param);
						} else {

							$setting 	= new Settings();
							$chagesbg						= $setting->getSetting('charges_bg', '');


							$param['TRANSFER_FEE'] 					= 0;
							$param['TRA_MESSAGE'] 					= $data['BG_REG_NUMBER'];
							$param['TRA_REFNO'] 					= NULL;
							$param['SOURCE_ACCOUNT'] 				= $data['FEE_CHARGE_TO'];
							//$param['SOURCE_ACCOUNT_NAME'] 			= $data['COUNTER_WARRANTY_ACCT_NAME'];
							$param['BENEFICIARY_ACCOUNT'] 			= $escrowbg;
							$param['TRA_AMOUNT'] 					= $chagesbg;
							$param['BENEFICIARY_ACCOUNT_CCY'] 		= 'IDR';
							$param['BENEFICIARY_ACCOUNT_NAME'] 		= 'ESCROW BG';
							$param['BENEFICIARY_ALIAS_NAME'] 		= NULL;
							$param['BENEFICIARY_EMAIL'] 			= NULL;
							$param['_addBeneficiary'] 				= $this->view->hasPrivilege('BADA');
							$param['_beneLinkage'] 					= $this->view->hasPrivilege('BLBU');
							$param['_priviCreate'] 					= 'CRSP';
							$param['sourceAccountType'] 			= '10';

							$param['RATE'] 							= '0';
							$param['RATE_BUY'] 						= '0';
							$param['BOOKRATE'] 						= '0';
							$param['BOOKRATE_BUY'] 					= '0';
							$param['TRA_AMOUNTEQ'] = '0';


							$SinglePayment = new SinglePayment($PS_NUMBER, $data['CUST_ID'], $data['BG_CREATEDBY']);
							$result = $SinglePayment->createPaymentWithinBG($param);
						}

						try {
							$this->_db->beginTransaction();
							$number = rand(0, 999999999);
							$acct = str_pad($number, 9, 0, STR_PAD_LEFT);
							$dataApprove =  array(
								'BG_STATUS' 		=> '5',
								'BG_NUMBER'			=> $bgNumber,
								'ACCT_NO'			=> $acct,
								'BG_APPROVE_DOC'	=> $sourceFileName,
								'BG_UPDATED' 		=> date('Y-m-d H:i:s'),
								'BG_UPDATEDBY' 		=> $this->_userIdLogin
							);

							$where = array('BG_REG_NUMBER = ?' => $bgRegNumb);
							$result = $this->_db->update('T_BANK_GUARANTEE', $dataApprove, $where);
							// Application_Helper_General::writeLog('RJLO','Approve Bank Guarantee '.$bgRegNumb);	

							$this->_db->commit();
						} catch (Exception $e) {
							$this->_db->rollBack();
						}


						// ---------------------------------- EMAIL NOTIF -----------------------------------------------

						$bankguarantee = $this->_db->select()
							->from('T_BANK_GUARANTEE')
							->where('BG_REG_NUMBER = ?', $bgRegNumb)
							->query()
							->fetch(Zend_Db::FETCH_ASSOC);


						$customer = $this->_db->select()
							->from('M_CUSTOMER')
							->where('CUST_ID = ?', $bankguarantee['CUST_ID'])
							->query()
							->fetch(Zend_Db::FETCH_ASSOC);

						// ------------- BG HISTORY --------------
						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'         => $bankguarantee['BG_REG_NUMBER'],
							'CUST_ID'           => $bankguarantee['CUST_ID'],
							'USER_LOGIN'        => $this->_userIdLogin,
							'HISTORY_STATUS'    => 5,
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
						//--------------- END BG HISTORY ---------------------


						$setting 	= new Settings();
						$template						= $setting->getSetting('femailtemplate_newbg', '');
						$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name', '');
						$templateEmailMasterBankAppName 	= $setting->getSetting('master_bank_app_name', '');
						$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp', '');
						$templateEmailMasterBankWapp 	= $setting->getSetting('master_bank_wapp', '');
						$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email', '');
						$templateEmailMasterBankEmail1 	= $setting->getSetting('master_bank_email1', '');
						$date 							= date("d M Y H:i:s");
						$config    		= Zend_Registry::get('config');
						$bgstatus 	= $config["bg"]["status"]["desc"];
						$bgstatusCode 	= $config["bg"]["status"]["code"];
						$arrStatus = array_combine(array_values($bgstatusCode), array_values($bgstatus));



						//var_dump($template);die('heere');

						$amount = number_format($bankguarantee['BG_AMOUNT'], 2, ',', '.');
						$translate = array(
							'[[user_name]]'                 => $customer['CUST_NAME'],
							'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
							'[[master_bank_app_name]]' 		=> $templateEmailMasterBankAppName,
							'[[MASTER_BANK_TELP]]' 			=> $templateEmailMasterBankTelp,
							'[[MASTER_BANK_EMAIL]]' 		=> $templateEmailMasterBankEmail,
							'[[MASTER_BANK_WAPP]]' 			=> $templateEmailMasterBankWapp,
							'[[MASTER_BANK_EMAIL1]]' 		=> $templateEmailMasterBankEmail1,
							'[[cust_name]]'					=> $customer['CUST_NAME'],
							'[[bg_number]]'					=> $bankguarantee['BG_NUMBER'],
							'[[account_no]]'	 			=> $bankguarantee['ACCT_NO'],
							'[[subject]]' 					=> $bankguarantee['BG_SUBJECT'],
							'[[amount]]' 					=> 'IDR ' . $amount,
							'[[status]]' 					=> $arrStatus[$bankguarantee['BG_STATUS']],
							'[[startdate]]' 					=> $bankguarantee['TIME_PERIOD_START'],
							'[[duedate]]' 					=> $bankguarantee['TIME_PERIOD_END'],
						);

						$subject = 'BG NEW Notification';
						$useremail = $customer['CUST_EMAIL'];
						$mailContent = strtr($template, $translate);
						//var_dump($useremail);
						//var_dump($mailContent);die;
						Application_Helper_Email::sendEmail($useremail, $subject, $mailContent);
						// ---------------------------------- END EMAIL NOTIF ------------------------------------------- 

						$this->setbackURL('/' . $this->_request->getModuleName());
						$this->_redirect('/notification/success');
					} else {
						$sourceFileName = substr(basename($adapter->getFileName()), 0);
						if ($_FILES["uploadSource"]["type"]) {
							$adapter->setDestination($attahmentDestination);
							$maxFileSize 			= $this->getSetting('Fe_attachment_maxbyte');
							$fileType = $adapter->getMimeType();
							$size = $_FILES["uploadSource"]["size"];
						} else {
							$fileType = null;
							$size = null;
						}







						$fileExt 				= "pdf";
						$fileTypeMessage = explode('/', $fileType);
						$fileType =  $fileTypeMessage[1];
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
						// $extensionValidator->setMessage("Extension file must be *.pdf");

						$maxFileSize = "1024000";
						$size = number_format($size);

						$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
						$sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

						$adapter->setValidators(array($extensionValidator, $sizeValidator));

						if ($adapter->isValid()) {
							// $newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
							// $adapter->addFilter('Rename', $newFileName);
							if ($adapter->receive()) {

								$setting 	= new Settings();
								$escrowbg						= $setting->getSetting('escrow_bg', '');
								$param = array();

								$PS_CCY = 'IDR';
								$PS_EFDATE = date('Y-m-d');
								$param['PS_SUBJECT'] 					= $data['BG_SUBJECT'];
								$param['PS_CCY'] 						= 'IDR';
								$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);

								if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
									$param['TRANSFER_FEE'] 					= 0;
									$param['TRA_MESSAGE'] 					= NULL;
									$param['TRA_REFNO'] 					= NULL;
									$param['SOURCE_ACCOUNT'] 				= $data['COUNTER_WARRANTY_ACCT_NO'];
									$param['SOURCE_ACCOUNT_NAME'] 			= $data['COUNTER_WARRANTY_ACCT_NAME'];
									$param['BENEFICIARY_ACCOUNT'] 			= $escrowbg;
									$param['TRA_AMOUNT'] 					= $data['BG_AMOUNT'];
									$param['BENEFICIARY_ACCOUNT_CCY'] 		= 'IDR';
									$param['BENEFICIARY_ACCOUNT_NAME'] 		= 'ESCROW BG';
									$param['BENEFICIARY_ALIAS_NAME'] 		= NULL;
									$param['BENEFICIARY_EMAIL'] 			= NULL;
									$param['_addBeneficiary'] 				= $this->view->hasPrivilege('BADA');
									$param['_beneLinkage'] 					= $this->view->hasPrivilege('BLBU');
									$param['_priviCreate'] 					= 'CRSP';
									$param['sourceAccountType'] 			= '10';
									$param['TRA_MESSAGE']					= $data['BG_REG_NUMBER'];
									$param['RATE'] 							= '0';
									$param['RATE_BUY'] 						= '0';
									$param['BOOKRATE'] 						= '0';
									$param['BOOKRATE_BUY'] 					= '0';
									$param['TRA_AMOUNTEQ'] = '0';


									$SinglePayment = new SinglePayment($PS_NUMBER, $data['CUST_ID'], $data['BG_CREATEDBY']);
									$result = $SinglePayment->createPaymentWithinBG($param);
									$setting 	= new Settings();
									$chagesbg						= $setting->getSetting('charges_bg', '');

									$param['TRANSFER_FEE'] 					= 0;
									$param['TRA_MESSAGE'] 					= NULL;
									$param['TRA_REFNO'] 					= NULL;
									$param['SOURCE_ACCOUNT'] 				= $data['FEE_CHARGE_TO'];
									//$param['SOURCE_ACCOUNT_NAME'] 			= $data['COUNTER_WARRANTY_ACCT_NAME'];
									$param['BENEFICIARY_ACCOUNT'] 			= $escrowbg;
									$param['TRA_AMOUNT'] 					= $chagesbg;
									$param['BENEFICIARY_ACCOUNT_CCY'] 		= 'IDR';
									$param['BENEFICIARY_ACCOUNT_NAME'] 		= 'ESCROW BG';
									$param['BENEFICIARY_ALIAS_NAME'] 		= NULL;
									$param['BENEFICIARY_EMAIL'] 			= NULL;
									$param['_addBeneficiary'] 				= $this->view->hasPrivilege('BADA');
									$param['_beneLinkage'] 					= $this->view->hasPrivilege('BLBU');
									$param['_priviCreate'] 					= 'CRSP';
									$param['sourceAccountType'] 			= '10';
									$param['TRA_MESSAGE']					= $data['BG_REG_NUMBER'];
									$param['RATE'] 							= '0';
									$param['RATE_BUY'] 						= '0';
									$param['BOOKRATE'] 						= '0';
									$param['BOOKRATE_BUY'] 					= '0';
									$param['TRA_AMOUNTEQ'] = '0';


									$SinglePayment = new SinglePayment($PS_NUMBER, $data['CUST_ID'], $data['BG_CREATEDBY']);
									$result = $SinglePayment->createPaymentWithinBG($param);
								} else {
									$setting 	= new Settings();
									$chagesbg						= $setting->getSetting('charges_bg', '');
									$param['TRANSFER_FEE'] 					= 0;
									$param['TRA_MESSAGE']					= $data['BG_REG_NUMBER'];
									$param['TRA_REFNO'] 					= NULL;
									$param['SOURCE_ACCOUNT'] 				= $data['FEE_CHARGE_TO'];
									//$param['SOURCE_ACCOUNT_NAME'] 			= $data['COUNTER_WARRANTY_ACCT_NAME'];
									$param['BENEFICIARY_ACCOUNT'] 			= $escrowbg;
									$param['TRA_AMOUNT'] 					= $chagesbg;
									$param['BENEFICIARY_ACCOUNT_CCY'] 		= 'IDR';
									$param['BENEFICIARY_ACCOUNT_NAME'] 		= 'ESCROW BG';
									$param['BENEFICIARY_ALIAS_NAME'] 		= NULL;
									$param['BENEFICIARY_EMAIL'] 			= NULL;
									$param['_addBeneficiary'] 				= $this->view->hasPrivilege('BADA');
									$param['_beneLinkage'] 					= $this->view->hasPrivilege('BLBU');
									$param['_priviCreate'] 					= 'CRSP';
									$param['sourceAccountType'] 			= '10';

									$param['RATE'] 							= '0';
									$param['RATE_BUY'] 						= '0';
									$param['BOOKRATE'] 						= '0';
									$param['BOOKRATE_BUY'] 					= '0';
									$param['TRA_AMOUNTEQ'] = '0';


									$SinglePayment = new SinglePayment($PS_NUMBER, $data['CUST_ID'], $data['BG_CREATEDBY']);
									$result = $SinglePayment->createPaymentWithinBG($param);
								}

								try {
									$this->_db->beginTransaction();
									$number = rand(0, 999999999);
									$acct = str_pad($number, 9, 0, STR_PAD_LEFT);
									$dataApprove =  array(
										'BG_STATUS' 		=> '5',
										'BG_NUMBER'			=> $bgNumber,
										'ACCT_NO'			=> $acct,
										'BG_APPROVE_DOC'	=> $sourceFileName,
										'BG_UPDATED' 		=> date('Y-m-d H:i:s'),
										'BG_UPDATEDBY' 		=> $this->_userIdLogin
									);

									$where = array('BG_REG_NUMBER = ?' => $bgRegNumb);
									$result = $this->_db->update('T_BANK_GUARANTEE', $dataApprove, $where);
									// Application_Helper_General::writeLog('RJLO','Approve Bank Guarantee '.$bgRegNumb);	

									$this->_db->commit();
								} catch (Exception $e) {
									$this->_db->rollBack();
								}


								// ---------------------------------- EMAIL NOTIF -----------------------------------------------

								$bankguarantee = $this->_db->select()
									->from('T_BANK_GUARANTEE')
									->where('BG_REG_NUMBER = ?', $bgRegNumb)
									->query()
									->fetch(Zend_Db::FETCH_ASSOC);


								$customer = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $bankguarantee['CUST_ID'])
									->query()
									->fetch(Zend_Db::FETCH_ASSOC);

								// ------------- BG HISTORY --------------
								$historyInsert = array(
									'DATE_TIME'         => new Zend_Db_Expr("now()"),
									'BG_REG_NUMBER'         => $bankguarantee['BG_REG_NUMBER'],
									'CUST_ID'           => $bankguarantee['CUST_ID'],
									'USER_LOGIN'        => $this->_userIdLogin,
									'HISTORY_STATUS'    => 5,
								);

								$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
								//--------------- END BG HISTORY ---------------------


								$setting 	= new Settings();
								$template						= $setting->getSetting('femailtemplate_newbg', '');
								$templateEmailMasterBankName 	= $setting->getSetting('master_bank_name', '');
								$templateEmailMasterBankAppName 	= $setting->getSetting('master_bank_app_name', '');
								$templateEmailMasterBankTelp 	= $setting->getSetting('master_bank_telp', '');
								$templateEmailMasterBankWapp 	= $setting->getSetting('master_bank_wapp', '');
								$templateEmailMasterBankEmail 	= $setting->getSetting('master_bank_email', '');
								$templateEmailMasterBankEmail1 	= $setting->getSetting('master_bank_email1', '');
								$date 							= date("d M Y H:i:s");
								$config    		= Zend_Registry::get('config');
								$bgstatus 	= $config["bg"]["status"]["desc"];
								$bgstatusCode 	= $config["bg"]["status"]["code"];
								$arrStatus = array_combine(array_values($bgstatusCode), array_values($bgstatus));



								//var_dump($template);die('heere');

								$amount = number_format($bankguarantee['BG_AMOUNT'], 2, ',', '.');
								$translate = array(
									'[[user_name]]'                 => $customer['CUST_NAME'],
									'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
									'[[master_bank_app_name]]' 		=> $templateEmailMasterBankAppName,
									'[[MASTER_BANK_TELP]]' 			=> $templateEmailMasterBankTelp,
									'[[MASTER_BANK_EMAIL]]' 		=> $templateEmailMasterBankEmail,
									'[[MASTER_BANK_WAPP]]' 			=> $templateEmailMasterBankWapp,
									'[[MASTER_BANK_EMAIL1]]' 		=> $templateEmailMasterBankEmail1,
									'[[cust_name]]'					=> $customer['CUST_NAME'],
									'[[bg_number]]'					=> $bankguarantee['BG_NUMBER'],
									'[[account_no]]'	 			=> $bankguarantee['ACCT_NO'],
									'[[subject]]' 					=> $bankguarantee['BG_SUBJECT'],
									'[[amount]]' 					=> 'IDR ' . $amount,
									'[[status]]' 					=> $arrStatus[$bankguarantee['BG_STATUS']],
									'[[startdate]]' 					=> $bankguarantee['TIME_PERIOD_START'],
									'[[duedate]]' 					=> $bankguarantee['TIME_PERIOD_END'],
								);

								$subject = 'BG NEW Notification';
								$useremail = $customer['CUST_EMAIL'];
								$mailContent = strtr($template, $translate);
								//var_dump($useremail);
								//var_dump($mailContent);die;
								Application_Helper_Email::sendEmail($useremail, $subject, $mailContent);
								// ---------------------------------- END EMAIL NOTIF -------------------------------------------



								$this->setbackURL('/' . $this->_request->getModuleName());
								$this->_redirect('/notification/success');
							}
						} else {
							$errors = array($adapter->getMessages());
							var_dump($errors);
							die;
						}
					}
				}

				if ($repair) {
					// die('here');
					$data = array('BG_STATUS' => '7');
					$where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
					$this->_db->update('T_BANK_GUARANTEE', $data, $where);

					$notes = $this->_getParam('PS_REASON_REPAIR');
					$historyInsert = array(
						'DATE_TIME'         => new Zend_Db_Expr("now()"),
						'BG_REG_NUMBER'         => $BG_NUMBER,
						'CUST_ID'           => $this->_custIdLogin,
						'USER_LOGIN'        => $this->_userIdLogin,
						'HISTORY_STATUS'    => 10,
						'BG_REASON'         => $notes,
					);

					$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

					$this->setbackURL('/' . $this->_request->getModuleName() . '/index/');
					$this->_redirect('/notification/success/index');
				}
			}
		}

		$back = $this->_getParam('back');
		if ($back) {
			$this->_redirect('/bgrequest');
		}
	}


	public function generateBGNumber($branch, $bgtype)
	{
		$currentDate = date("Ymd");
		// $seqNumber	 = $this->getPaymentCounter($forTransaction);
		$number = rand(0, 9999);
		$seqNumber = str_pad($number, 4, 0, STR_PAD_LEFT);
		//$seqNumber	 = strtoupper(Application_Helper_General::str_rand(8));
		$checkDigit  = '';
		//$checkDigit  = strtoupper(substr(MD5($currentDate.$seqNumber),-3));
		// $checkDigit  = strtoupper(substr(preg_replace("/[^a-zA-Z0-9]/", "", uniqid()),-3));
		//$paymentID   = $currentDate.$seqNumber.$checkDigit;
		$paymentID   = $branch . $currentDate . $bgtype . $seqNumber;

		return $paymentID;
	}
}
