<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class releasepayment_ReleasepaymentreportlistController extends Application_Main
{
  public function indexAction() 
  { 
		
  		$typearr = array_combine(array_values($this->_releasetype['code']),array_values($this->_releasetype['desc']));
  		$statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
  		
    	//Zend_Debug::dump($this->_masterglobalstatus); die;
		 
    	$select = $this->_db->select()->distinct()
					->from(array('M_CUSTOMER'),array('CUST_ID'))
					->where("CUST_STATUS != '3'")
					->order('CUST_ID ASC')
				 	-> query() ->fetchAll();
		//Zend_Debug::dump($typearr); die;
		$this->view->var=$select;
    	
    	$fields = array('Company Code'   	=> array('field'    => 'custid',
	                                        		 'label'    => 'Company Code',
	                                        		 'sortable' => true),
    
                    	'Company Name'		=> array('field'    => 'custname',
	                                       			 'label'    => 'Company Name',
	                                        		 'sortable' => true),
    
    					'Release Date'		=> array('field'  => 'release',
	                                        		 'label'    => 'Release Date',
	                                        		 'sortable' => true),
    
                   		'Payment Ref#'     	=> array('field'    => 'payref',
	                                        		 'label'    => 'Payment Ref#',
	                                        		 'sortable' => true),
    	
    					'Release Type'     	=> array('field'    => 'releasetype',
	                                        		 'label'    => 'Release Type',
	                                        		 'sortable' => true),
    	
    					'Release User'     	=> array('field'    => 'releaseuser',
	                                        		 'label'    => 'Release User',
	                                        		 'sortable' => true),
    	
    					'Release Status'    => array('field'    => 'releasestatus',
	                                        		 'label'    => 'Release Status',
	                                        		 'sortable' => true),
    	
    					'Description'     	=> array('field'    => 'description',
	                                        		 'label'    => 'Description',
	                                        		 'sortable' => true),
                   		);
                 
    	$page = $this->_getParam('page');
    	$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
    
    	$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
   		$sortBy  = $this->_getParam('sortby');
    	$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    	$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

    	$filterArr = array(	'filter' 	=> array('StripTags','StringTrim'),
                       		'custid'   	=> array('StripTags','StringTrim','StringToUpper'),
                       		'custname' 	=> array('StripTags','StringToUpper'),
                       		'payref'	=> array('StripTags','StringTrim'),
    						'desc'		=> array('StripTags','StringToUpper'),
    						'status'	=> array('StripTags','StringTrim'),
    						'fDateFrom'	=> array('StripTags','StringTrim'),
    						'fDateTo'	=> array('StripTags','StringTrim'),
                      		);
                      		
		$validator = array(	'filter' 	=> array(),
                       		'custid'   	=> array(),
                       		'custname' 	=> array(),
                       		'payref'	=> array(),
    						'desc'		=> array(),
    						'status'	=> array(),
    						'fDateFrom'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    						'fDateTo'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                      		);
                      
    	$zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
    	
    	$filter 	= $zf_filter->getEscaped('filter');
    	$custid     = html_entity_decode($zf_filter->getEscaped('custid'));
		$custname   = html_entity_decode($zf_filter->getEscaped('custname'));
		$payref     = html_entity_decode($zf_filter->getEscaped('payref'));
		$desc   	= html_entity_decode($zf_filter->getEscaped('desc'));
		$status   	= html_entity_decode($zf_filter->getEscaped('status'));
		$datefrom   = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$dateto   	= html_entity_decode($zf_filter->getEscaped('fDateTo'));
		
    	$this->view->currentPage = $page;
    	$this->view->sortBy = $sortBy;
    	$this->view->sortDir = $sortDir;
    	
    	$caseStatus = "(CASE D.TRA_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";
  			
  		$caseType = "(CASE E.RELEASE_TYPE ";
  		foreach($typearr as $key=>$val)
  		{
   			$caseType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseType .= " END)";
    
    
      	$selectunion1 = $this->_db->select()->distinct()
					        ->from(array('B' => 'T_PSLIP_HISTORY'),array())
					        ->join(array('C' => 'M_CUSTOMER'), 'B.CUST_ID = C.CUST_ID',array())
					        ->join(array('D' => 'T_TRANSACTION'), 'B.PS_NUMBER = D.PS_NUMBER',array())
					        ->join(array('E' => 'T_PSLIP'), 'D.PS_NUMBER = E.PS_NUMBER',array( 	'custid'		=>	'C.CUST_ID',
						        																'custname'		=>	'C.CUST_NAME',
						        																'release'		=>	'B.DATE_TIME', 
						        																'payref'		=>	'B.PS_NUMBER',
						        																'releasetype'	=>	$caseType,
						        																'releaseuser'	=>	'B.USER_LOGIN',
						        																'releasestatus'	=>	$caseStatus,
						        																'description' 	=>	'B.PS_REASON',
					        																	//'transid'		=>	'D.TRANSACTION_ID',
	        																					'statuscode'	=>	'D.TRA_STATUS',));
		$selectunion1 -> where("B.HISTORY_STATUS = '5'");
		$selectunion1 -> where("D.TRA_STATUS = '3' OR D.TRA_STATUS = '4'");
		/*echo $selectunion1;die;
		$result = $this->_db->fetchAll($selectunion1);
		
		Zend_Debug::dump($result);die;*/
		$selectunion1 -> where("D.TRA_STATUS = '3' OR D.TRA_STATUS = '4'");

//		$selectstring1 = $selectunion1->__toString();
		
//		$unionquery = $this->_db->select()
//								->union(array($selectstring1,$selectstring2));
//		$select2 = $this->_db->select()
//					        ->from(($unionquery),array('*'));
		$select2 = $selectunion1;	        
					         
		//$result = $this->_db->fetchAll($select2);						
		//Zend_Debug::dump($result);
		
  		if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}

  		if($filter == null || $filter == 'Set Filter')
		{
		 	$this->view->fDateTo    = $dateto;
		 	$this->view->fDateFrom  = $datefrom;
		//Zend_Debug::dump($custid); die;
		 
			if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    	if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
					//Zend_Debug::dump($dateto); die;	
	            }
		
			if(!empty($datefrom) && empty($dateto))
	            $select2->where("CONVERT(DATE, release) >= ".$this->_db->quote($datefrom));
	            
	   		if(empty($datefrom) && !empty($dateto))
	            $select2->where("CONVERT(DATE, release) <= ".$this->_db->quote($dateto));
	            
	    	if(!empty($datefrom) && !empty($dateto))
	            $select2->where("CONVERT(DATE, release) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}
		
        if($filter=='Set Filter')
        {
			if($custid){
	       		$this->view->custid = $custid;
	       		$select2->where("custid LIKE ".$this->_db->quote($custid));}
	       		
        	if($custname){
	       		$this->view->custname = $custname;
	       		$select2->where("custname LIKE ".$this->_db->quote('%'.$custname.'%'));}
	       		
        	if($payref){
	       		$this->view->payref = $payref;
	       		$select2->where("payref LIKE ".$this->_db->quote('%'.$payref.'%'));}
	       		
        	if($desc){
	       		$this->view->desc = $desc;
	       		$select2->where("description LIKE ".$this->_db->quote('%'.$desc.'%'));}
	       		
        	if($status){
	       		$this->view->status = $status;
	       		$select2->where("statuscode LIKE ".$this->_db->quote($status));}
        }
        
		$select2->order($sortBy.' '.$sortDir);
		$arr = $this->_db->fetchAll($select2);   
		//$this->paging($select2);
		$this->paging($arr,30);
    	//die;
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;
		//Zend_Debug::dump($arr);
		//echo $select2; die;
		//echo $select2; die;
    
    	if($csv || $pdf)
    	{
	    	if($csv)
			{
				Application_Helper_General::writeLog('PAYR','Download CSV Release Payment Report');
				foreach ($arr as $key => $value)
				{
					//echo $key; 
					$arr[$key]["release"]= Application_Helper_General::convertDate($value["release"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
					unset($arr[$key]["statuscode"]);
					unset($arr[$key]["transid"]);
				}
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv(array('Company Code','Company Name','Release Date','Payment Ref#','Release Type','Release User','Release Status','Description'),$arr,null,'Release Payment Report List');
			}
			                                                                                                                                            
			if($pdf)
			{
				Application_Helper_General::writeLog('PAYR','Download PDF Release Payment Report');
				foreach ($arr as $key => $value)
				{
					//echo $key; 
					$arr[$key]["release"]= Application_Helper_General::convertDate($value["release"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
					unset($arr[$key]["statuscode"]);
					unset($arr[$key]["transid"]);
				}
				$this->_helper->download->pdf(array('Company Code','Company Name','Release Date','Payment Ref#','Release Type','Release User','Release Status','Description'),$arr,null,'Release Payment Report List');
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('PAYR','View Release Payment Report');
    	}
	}
}
