<?php
require_once 'Zend/Controller/Action.php';
class user_MyactivityreportsController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$select = $this->_db->select()->distinct()
						->from(array('A' => 'M_USER'),array('USER_ID', 'USER_FULLNAME', 'CUST_ID'))
						->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						->order('USER_FULLNAME ASC')
				 		->query()->fetchAll();
		
		$this->view->var=$select;
		
		$activity = $this->_db->select()->distinct()
								->from(array('A' => 'M_FPRIVILEGE'),array('FPRIVI_ID', 'FPRIVI_DESC'))
								->order('FPRIVI_DESC ASC')
				 				->query()->fetchAll();
		
		$login = array('FPRIVI_ID'=>'FLGN','FPRIVI_DESC'=>'Login');
		$logout = array('FPRIVI_ID'=>'FLGT','FPRIVI_DESC'=>'Logout');
		$changepass = array('FPRIVI_ID'=>'CHMP','FPRIVI_DESC'=>'Change My Password');
		//$resetpass = array('FPRIVI_ID'=>'RFPW','FPRIVI_DESC'=>'Reset Forgot Password');
		array_unshift($activity,$changepass);
		array_unshift($activity,$logout);
		array_unshift($activity,$login);
		//array_unshift($activity,$resetpass);
		//unset($resetpass['RFPW']);
				 				
		$activityarr = Application_Helper_Array::listArray($activity,'FPRIVI_ID','FPRIVI_DESC');
		asort($activityarr);
		//Zend_Debug::dump($activityarr);die;
		$this->view->activity=$activityarr;
		
		$userlist=Application_Helper_Array::listArray($select,'USER_ID','USER_FULLNAME');
		$this->view->userlist = $userlist;
		
		
		//Zend_Debug::dump($activity);die;
		
		$fields = array(
						'LOG_DATE'      	=> array('field' => 'LOG_DATE',
											      'label' => $this->language->_('Date/Time'),
											      'sortable' => true),
						'FPRIVI_DESC'  	=> array('field' => 'FPRIVI_DESC',
											      'label' => $this->language->_('Activity'),
											      'sortable' => true),
						'ACTION_FULLDESC'     => array('field' => 'ACTION_FULLDESC',
											      'label' => $this->language->_('Description'),
											      'sortable' => true)
				      );
				      
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','LOG_DATE');
		$sortDir = $this->_getParam('sortdir','desc');
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$filterArr = array('filter' 			=> array('StripTags','StringTrim'),
						   'SEARCH_ACTIVITY'    => array('StripTags'),
						   'fDateFrom' 			=> array('StripTags','StringTrim'),
						   'fDateTo' 			=> array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array('filter' 			=> array(),
						   'SEARCH_ACTIVITY'    => array(),
						   'fDateFrom' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );
	                      
	    $zf_filter 	= new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter 	= $zf_filter->getEscaped('filter');
	    $active 	= html_entity_decode($zf_filter->getEscaped('SEARCH_ACTIVITY'));
		$datefrom 	= html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$dateto 	= html_entity_decode($zf_filter->getEscaped('fDateTo'));
		
		$select2 = $this->_db->select()
					        ->from(array('B' => 'T_FACTIVITY'),array())
					        ->joinleft(array('C' => 'M_FPRIVILEGE'),'B.ACTION_DESC = C.FPRIVI_ID',array('B.LOG_DATE',
						        																		'B.USER_ID',
						        																		'FPRIVI_DESC' => new Zend_Db_Expr("(CASE B.ACTION_DESC 
																							        	  								   	WHEN 'FLGN' THEN 'Login' 
																							        	  									WHEN 'FLGT' THEN 'Logout' 
																							        	  									WHEN 'CHMP' THEN 'Change My Password' 
																							        	  									WHEN 'RFPW' THEN 'Reset Forgot Password' 
																							        	  									ELSE C.FPRIVI_DESC 
																							        	  									END)"),
						        																		'B.ACTION_FULLDESC'));
		$select2->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$select2->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
		
		if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}
		
		if($filter == null || $filter == TRUE)
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
					//Zend_Debug::dump($dateto); die;	
	            }
		
		
		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(B.LOG_DATE) >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.LOG_DATE) <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.LOG_DATE) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}
		
		if($filter ==TRUE)
		{
		//if($active) $select2->where("ACTION_DESC LIKE ".$active);
		if($active)
		{
			$this->view->active = $active;
			if( ($active != 'FLGN' && $active !='FLGT' && $active != 'CHMP'))
			{
			   $select2->where("C.FPRIVI_ID LIKE ".$this->_db->quote($active));
			}
			else
			{
				$select2->where("B.ACTION_DESC LIKE ".$this->_db->quote($active));
			}
		}

		//if($user) $select2->where("USER_ID LIKE ".$user);
		}
		$select2->order($sortBy.' '.$sortDir);

		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$arr = $this->_db->fetchAll($select2);
			//new - begin
			$data_val = array();
			$i=0;
			foreach ($arr as $data) {
				$data['FPRIVI_DESC'] = $this->language->_($data['FPRIVI_DESC']);
				array_push($data_val, $data);
				//$data_val[$i++] = $data;
			}
			//new - end
			//die;
			
			foreach ($arr as $key => $value)
			{
				//echo $key; 
				$arr[$key]["LOG_DATE"]= Application_Helper_General::convertDate($value["LOG_DATE"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
			if($csv)
			{
				$this->_helper->download->csv(array($this->language->_('Date/Time'),$this->language->_('User ID'),$this->language->_('Activity'),$this->language->_('Description')),$data_val,null,$this->language->_('My Activity'));
			}
			
			if($pdf) 
			{
				$this->_helper->download->pdf(array($this->language->_('Date/Time'),$this->language->_('User ID'),$this->language->_('Activity'),$this->language->_('Description')),$data_val,null,$this->language->_('My Activity'));
			}
			if($this->_request->getParam('print') == 1){
                $data = $arr;//$this->_dbObj->fetchAll($select);
                $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'My Activity', 'data_header' => $fields));
            }
		}
		
		//Zend_Debug::dump($arr); die;    
		$this->paging($select2);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}
}
?>
