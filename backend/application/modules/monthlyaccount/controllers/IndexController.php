<?php


require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';


class monthlyaccount_IndexController extends Application_Main
{
	public function indexAction() 
	{						
		$this->_helper->layout()->setLayout('newlayout');
		
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $custid = $AESMYSQL->decrypt($this->_getParam('custid'), $password);
	    $custid = (Zend_Validate::is($custid,'Alnum') && Zend_Validate::is($custid,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $custid : null;
		
		$selectcust = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$selectcust -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $selectcust->query()->FetchAll();
		$this->view->result = $result;
		
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		/*$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));*/
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
// 		$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;
		
		foreach($resultlist as $defaultacct)
		{
			$acctno = $defaultacct['ACCT_NO'];
			$cekacct = 'cek'.$acctno;
			$this->view->$cekacct = $defaultacct['ACCT_NO'];
		}
		
		$monthlyfeestatus = $result[0]["CUST_MONTHLYFEE_STATUS"];
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = $this->language->_('Disabled');
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = $this->language->_('Enabled');
		}
			
		$custname = $result[0]['CUST_NAME'];
		
		foreach($resultunion as $row)
		{	
			$acctno = $row['ACCT_NO'];
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
			$select4 -> where("MONTHLYFEE_TYPE = '3'");
			$result4 = $select4->query()->FetchAll();

			$i = 0;
			$x = 0;
			if($select4)
			{
				foreach($result4 as $row2)
				{	
					$idfrom = $row2['ACCT_NO'].'from'.$i;
   					$idto = $row2['ACCT_NO'].'to'.$i;
   					$idamt = $row2['ACCT_NO'].'amount'.$i;
					$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
					$cekacct = 'cek'.$acctno;
					
					if($row2['TRA_FROM'] != 0 && $row2['TRA_TO'] != 0)
					{
						$this->view->$idfrom = $row2['TRA_FROM'];
						$this->view->$idto = $row2['TRA_TO'];
					}
					
					$this->view->$idamt = $amt;
					$this->view->$cekacct = $row2['CHARGES_ACCT_NO'];
					$i++;
				}
			}
			$x++;
		}	
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
		if(!$submit)
		{
			Application_Helper_General::writeLog('CHUD','View Update Monthly transaction account charges page ('.$custid.')');
		}
		
		$cek = $this->_db->select()
							->from('TEMP_ADMFEE_MONTHLY');
		$cek -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek = $cek->query()->FetchAll();
		
		$cek2 = $this->_db->select()
							->from('TEMP_CHARGES_MONTHLY');
		$cek2 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek2 = $cek2->query()->FetchAll();
		//Zend_Debug::dump($submit); die;
		if(!$cek && !$cek2)
		{
			if($submit)
			{
				Application_Helper_General::writeLog('CHUD','Updating Monthly transaction account charges ('.$custid.')');
					$error = 0;
					$arraycekisi = array();
					foreach($resultunion as $row)
					{
						$cekisi = 0;
						$cekisi2 = 0;
						$cekisiamt = 0;
						$acctno = $row['ACCT_NO'];
						$cekacct = 'cek'.$acctno;
						$rowmax = 0;
						
						for($i=0;$i<5;$i++)
						{
						$z = 0;
						$idamt = $row['ACCT_NO'].'amount'.$i;
						$idfrom = $row['ACCT_NO'].'from'.$i;
   						$idto = $row['ACCT_NO'].'to'.$i;
   						$err2 = $row['ACCT_NO'].'err2'.$i;
   						//Zend_Debug::dump($idamt); die;
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						$temp_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
						$this->view->$idamt = $cekamt;
						
						$cekfrom = $this->_getParam($idfrom);
						$cek_angka2 = (Zend_Validate::is($cekfrom,'Digits')) ? true : false;
						$this->view->$idfrom = $cekfrom;
						
						$cekto = $this->_getParam($idto);
						$cek_angka3 = (Zend_Validate::is($cekto,'Digits')) ? true : false;
						$this->view->$idto = $cekto;
						
						$cekcekacct = $this->_getParam($acctno);
   						$cek_angka4 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   						$this->view->$cekacct = $cekcekacct;
   						//Zend_Debug::dump($cekto); die;
   						
   						$selectcekcur = $this->_db->select()
					        			->from (($selectprk),array('*'));
						$selectcekcur -> where("ACCT_NO LIKE ".$this->_db->quote($cekcekacct));
						$cekcur = $this->_db->fetchRow($selectcekcur);
   								
						if($cek_angka && $cek_angka2 && $cek_angka3)
						{
							//Zend_Debug::dump($cekto); die;
							if ($cekfrom >= $cekto && $cekfrom != '0' && $cekto != '0')
							{
								$errid = 'err'.$idfrom;
								$err = $this->language->_('From Range has to be less than To Range');
								$this->view->$errid = $err;
								$error++;
								$z++;
								//die;
							}
							
							if ($cekfrom == '0' && $cekto != '0')
							{
								$errid = 'err'.$idfrom;
								$err = $this->language->_('Range value can not start from 0');
								$this->view->$errid = $err;
								$error++;
								$z++;
								//die;
							}
							
							for($j=$i;$j>=0;$j--)
							{
								if($i != $j)
								{
									$fromerror = false;
									$toerror = false;
									$idfromcek = $row['ACCT_NO'].'from'.$j;
   									$idtocek = $row['ACCT_NO'].'to'.$j;
   									$fromcek = $this->_getParam($idfromcek);
   									$tocek = $this->_getParam($idtocek);
   									
   									if($fromcek && $tocek)
   									{
   										if($fromcek <= $cekfrom && $cekfrom <= $tocek)
   										{
   											$fromerror = true;
   											//echo $cekfrom;die;
   										}
   									
										if($fromcek <= $cekto && $cekto <= $tocek)
   										{
   											$toerror = true;
   										}
   									
   										if($fromerror == true || $toerror == true)
   										{
   											$errid = 'err'.$idto.$j;
											$err = $this->language->_('Value overlap not allowed (overlap with : ').$fromcek.'-'.$tocek.')';
											$this->view->$errid = $err;
											$this->view->$err2 = true;
											//echo $err2;die;
											//Zend_Debug::dump($fromcek.' <= '.$cekto.' - '.$cekto.' <= '.$tocek);
											//Zend_Debug::dump($errid);
											$error++;
											$z++;
											
   										}
   									}	
								}
							}
						}
   						
						if($cek_angka2 && $cek_angka3 && $cekfrom != '0' && $cekto != '0')
							{
								if($cekamt == null || $cekamt == '0.00')
								{
									$errid = 'err'.$idamt;
									$err = $this->language->_('Amount value has to be set for this range');
									$this->view->$errid = $err;
									$error++;
									$z++;
									//die;
								}
							}
						
						if($cek_angka == false && $cekamt != null)
						{
							$errid = 'err'.$idamt;
							$err = $this->language->_('Amount value has to be numeric');
							$this->view->$errid = $err;
							$error++;
							$z++;
							//die;
						}
							
						if($cekcekacct)
						{
								if($cekcur['CCY_ID'] != $row['CCY_ID'])
								{
									$errid = 'err'.$cekacct;
									$err = $this->language->_('Account currency must be same');
									$this->view->$errid = $err;
									$error++;
								}
						}
						
						if($cek_angka && $cekamt != '0.00')
							{
								if($cek_angka2 == false || $cek_angka3 == false)
								{
									$errid = 'err'.$idfrom;
									$err = $this->language->_('From and To value has to be numeric');
									$this->view->$errid = $err;
									$error++;
									$z++;
									//die;
								}
								
								if($cekfrom == null || $cekto == null)
								{
									$errid = 'err'.$idfrom;
									$err = $this->language->_('From and To Range should not be empty while the amount is filled');
									$this->view->$errid = $err;
									$error++;
									$z++;
								}
							}
						
							if(!$cekfrom && !$cekto && ($amt == null || $amt == '0.00'))
							{
								$cekisi2++;
							}
							
							if($z > 0)
							{
								$rowmax++;
							}
   						
						$cekisi++;
						
						}
						
						if($cekisi == $cekisi2)
						{
							$temparray = array($acctno);
							$arraycekisi = array_merge($arraycekisi,$temparray);
						}
						if($cekisi != $cekisi2)
						{
							if(!$cekcekacct)
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account should be choosen when amount is filled');
								$this->view->$errid = $err;
								$error++;
								//die;
							}
							
							if($cekcekacct)
							{
								if($cek_angka4 == false)
								{
									$errid = 'err'.$cekacct;
									$err = $this->language->_('Account value has to be numeric');
									$this->view->$errid = $err;
									$error++;
								}
							}
						}
						
						$rowmaxid = 'rowmax'.$row['ACCT_NO'];
						$this->view->$rowmaxid = $rowmax;
					}
					//die;
					//Zend_Debug::dump($arraycekisi); die;
				if($error == 0)
				{
					$this->_db->beginTransaction();
					//die;
					try
					{
				    //Zend_Debug::dump($resultunion); die;
					$info = 'Charges';
					$info2 = $this->language->_('Set Charges Monthly per Account');
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_MONTHLY','TEMP_CHARGES_MONTHLY',$custid,$custname,$custid);
					
					foreach($resultunion as $row)
					{
						for($i=0;$i<6;$i++)
						{
							$idamt = $row['ACCT_NO'].'amount'.$i;
							$idfrom = $row['ACCT_NO'].'from'.$i;
   							$idto = $row['ACCT_NO'].'to'.$i;
   						
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
							$cekfrom = $this->_getParam($idfrom);
						
							$cekto = $this->_getParam($idto);
							$cekcekacct = $this->_getParam($row['ACCT_NO']);
							if($cekfrom && $cekto && $cekamt)
							{
								$data= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'TRA_FROM' 			=> $cekfrom,
										'TRA_TO' 			=> $cekto,
										'AMOUNT' 			=> $amt,
										'MONTHLYFEE_TYPE' 	=> '3',
										'CHARGES_ACCT_NO'	=> $cekcekacct
									);
								$this->_db->insert('TEMP_CHARGES_MONTHLY',$data);										
								//echo $row['ACCT_NO'].' aaa<br />';
								//Zend_Debug::dump($data);
							}
						}				
					}
					
					if($arraycekisi)
					{
						foreach($arraycekisi as $array)
						{				
							$cekcekacct = $this->_getParam($array);
								
							$data2= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $array,
										'TRA_FROM' 			=> '0',
										'TRA_TO' 			=> '0',
										'AMOUNT' 			=> '0.00',
										'MONTHLYFEE_TYPE' 	=> '3',
										'CHARGES_ACCT_NO'	=> $cekcekacct
										);
							$this->_db->insert('TEMP_CHARGES_MONTHLY',$data2);				
						}
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/setcompanycharges/detail/index/custid/'.$this->_getParam('custid'));
						$this->_redirect('/notification/submited/index');
					}
				}
				
				catch(Exception $e)
				{
					die;
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}			
			}
		}
		if($cek || $cek2)
		{			
			$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";	
			$this->view->error = $docErr;
			$this->view->changestatus = $this->language->_("disabled");
		}
		Application_Helper_General::writeLog('CHUD','Update Company charges');
	}
}
