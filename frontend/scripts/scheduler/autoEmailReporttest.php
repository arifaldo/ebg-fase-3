<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';

$config     = Zend_Registry::get('config');
// $config     = Zend_Registry::get('config');
     $bankName    = $config['app']['bankname'];
     $appName     = $config['app']['name'];
     $appBankCode   = $config['app']['bankcode'];

  $Settings = new Settings();
  $templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
  $templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
  $templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
  $templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
  $templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
  $templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
  $templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
  $templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
  $templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
  $templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
  $templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
  
$template = file_get_contents(LIBRARY_PATH.'/email template/emailreport.html');
$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
// print_r($template);die;
$transFT   = array( '[[APP_NAME]]'      => $templateEmailMasterBankAppName,
          '[[BANK_NAME]]'     => $templateEmailMasterBankName,
          '[[APP_URL]]'       => $templateEmailMasterBankAppUrl                         
          );

$FOOTER_ID = strtr($FOOTER_ID, $transFT);
$FOOTER_EN = strtr($FOOTER_EN, $transFT);

$db = Zend_Db_Table::getDefaultAdapter();

$mailedTo = 'No Email Sent';

// $settings  = new Settings();


  
//  SEND FILE STATUS = 0
$data = array();
$data_content = '';
$status = 0;
$counter = 0;




 $data =  $db->fetchAll(
            $db->select()->distinct()
               ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))

               //->WHERE('A.REPORT_SCHEDULE = ?','monthly') 
               //->WHERE('A.REPORT_EMAIL != ?','')    
            //->WHERE('A.REPORT_DATE = DAY(NOW())')    
            ->WHERE('A.REPORT_FILESTATUS = 0')    
           ->where('A.ID = ?','168')  
               ->order('A.ID DESC')
            );

$receiver = '';
$subject = '';
$tableHeader = $string = '';
 //echo $data;die;
 //var_dump($data);die;
foreach ($data as $key => $value) {
  // $listReceiver = $Settings->getSetting('email_exception');
  $receiver = explode(";", $value['REPORT_EMAIL']);
  $subject = 'Scheduler Report '.$value['REPORT_NAME'];
  // print_r($receiver);
  $selectcolomn = $db->select()
                            ->from('T_REPORT_COLOMN', array('COLM_NAME','COLM_FIELD','COLM_TYPE'))
                            ->where('COLM_REPORD_ID = ?', $value['ID']);

        $ColomnList = $db->fetchAll($selectcolomn);

        $CustomerUser   = new CustomerUser($value['REPORT_CUST'], $value['REPORT_CREATEDBY']);
        
        $arrAccount     = $CustomerUser->getAccounts();
        $acc  = Application_Helper_Array::simpleArray($arrAccount, "ACCT_NO");
         $paramPayment = array("WA"              => false,
                              "ACCOUNT_LIST"    => $acc,
                              "_beneLinkage"    => 0,
                             );
        
        // get payment query
        $select   = $CustomerUser->getPayment($paramPayment);
        // $selectdata = $db->select()->from(array('A' => $select),
        //         array(
        //             'A.*'
        //         ));
        // echo $selectdata;die;
                 if(!empty($value['REPORT_LIMIT'])){
            $select->limit($value['REPORT_LIMIT']);
        }
        if(!empty($value['REPORT_WHERE'])){
            $whereArr = explode(';', $value['REPORT_WHERE']);
            foreach ($whereArr as $key => $valuew) {
                // print_r($whereArr);die;
                $searchkey = ( explode( ' ', $valuew ));
                if($searchkey['0'] == 'PS_EFDATE' || $searchkey['0'] == 'PS_CREATED' || $searchkey['0'] == 'PS_UPDATED'){
                    $var = $searchkey['2'];
                    $searchkey['2'] = date("Y-m-d", strtotime($var) );
                    $valuew = 'DATE('.$searchkey['0'].') '.$searchkey['1'].' "'.$searchkey['2'].'"';
                }
        
        if($valuew != ''){
                $select->where($valuew);
        }
            }
            
        }

        // if(!empty($value['REPORT_SCHEDULE'])){
        //     // $sortby = $reportdata['REPORT_SORT_DESC'];
        //     if($value['REPORT_SCHEDULE']=='monthly'){
        //         $select->where('MONTH(PS_CREATED) = ?' , 'MONTH(NOW())');
        //         $select->where('YEAR(PS_CREATED) = ?' , 'YEAR(NOW())');

        //     }
        //     // $sortdesdata = explode(',', $reportdata['REPORT_SORT_DESC']);
        //     // $sortdirdesc = 'DESC';
        //     //  if(!empty($sortdesdata)){
        //     //     foreach ($sortdesdata as $key => $value) {
        //     //             // 4
        //     //             $select->order($value.' DESC');
        //     //     }
        //     // }
        // }
        if(!empty($reportdata['REPORT_DATA'])){
                    if($reportdata['REPORT_DATA'] == '1'){
                        $select->where('MONTH(PS_CREATED) = MONTH(NOW())');
                    }else if($reportdata['REPORT_DATA'] == '2'){
                        $select->where('DATE(PS_CREATED) BETWEEN '.date('Y-m-01', strtotime('-1 MONTH')).' AND DATE_SUB(CURDATE(),INTERVAL EXTRACT(DAY FROM NOW()) DAY)');
                    }else if($reportdata['REPORT_DATA'] == '3'){
                        $select->where('DATE(PS_CREATED) BETWEEN '.date('Y-m-01', strtotime('-3 MONTH')).' AND DATE_SUB( CURDATE( ) ,INTERVAL 3 MONTH )');
                    }else if($reportdata['REPORT_DATA'] == '4'){
                        $select->where('DATE(PS_CREATED) BETWEEN '.date('Y-m-01', strtotime('-6 MONTH')).' AND DATE_SUB( CURDATE( ) ,INTERVAL 6 MONTH )');
                    }else if($reportdata['REPORT_DATA'] == '5'){
                        $select->where('YEAR(PS_CREATED) = YEAR(NOW())');
                    }

                } 
        // echo $select;die;
        // $selectdata = $selectdata->order('TRA_AMOUNT DESC');
        // $selectdata = $selectdata->order('PS_NUMBER DESC');

        // $selectdata->order($sortby.' '.$sortdir);
        if(!empty($value['REPORT_SORT_ASC'])){

            $sortascdata = explode(',', $value['REPORT_SORT_ASC']);
                    // print_r($sortascdata);die;
            if(!empty($sortascdata)){
                // die;
                foreach ($sortascdata as $key => $values) {
                        // 4
                            // die;
                        $select->order($values.' ASC');
                }
            }
            
        }

        if(!empty($value['REPORT_SORT_DESC'])){
            // $sortby = $reportdata['REPORT_SORT_DESC'];
            $sortdesdata = explode(',', $value['REPORT_SORT_DESC']);
            $sortdirdesc = 'DESC';
             if(!empty($sortdesdata)){
                foreach ($sortdesdata as $key => $values) {
                        // 4
                        $select->order($values.' DESC');
                }
            }
        }

    //echo $select;

        $datareport = $db->fetchAll($select);
//var_dump($datareport);die;
$numFields = count($ColomnList);
    $string = '<style>table.blueTable {
  border: 1px solid #59A489;
  background-color: #ECEEED;
  width: 100%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable td, table.blueTable th {
  border: 1px solid #AAAAAA;
  padding: 3px 2px;
}
table.blueTable tbody td {
  font-size: 13px;
}
table.blueTable tr:nth-child(even) {
  background: #9CFF88;
}
table.blueTable thead {
  background: #1A8E1B;
  background: -moz-linear-gradient(top, #53aa54 0%, #319931 66%, #1A8E1B 100%);
  background: -webkit-linear-gradient(top, #53aa54 0%, #319931 66%, #1A8E1B 100%);
  background: linear-gradient(to bottom, #53aa54 0%, #319931 66%, #1A8E1B 100%);
  border-bottom: 2px solid #444444;
}
table.blueTable thead th {
  font-size: 15px;
  font-weight: bold;
  color: #FFFFFF;
  text-align: center;
  border-left: 2px solid #DBE0CC;
}
table.blueTable thead th:first-child {
  border-left: none;
}

table.blueTable tfoot {
  font-size: 14px;
  font-weight: bold;
  color: #FFFFFF;
  background: #683AF5;
  background: -moz-linear-gradient(top, #8e6bf7 0%, #774df6 66%, #683AF5 100%);
  background: -webkit-linear-gradient(top, #8e6bf7 0%, #774df6 66%, #683AF5 100%);
  background: linear-gradient(to bottom, #8e6bf7 0%, #774df6 66%, #683AF5 100%);
  border-top: 2px solid #444444;
}
table.blueTable tfoot td {
  font-size: 14px;
}
table.blueTable tfoot .links {
  text-align: right;
}
table.blueTable tfoot .links a{
  display: inline-block;
  background: #1C6EA4;
  color: #FFFFFF;
  padding: 2px 8px;
  border-radius: 5px;
}</style>';
    $headerarr = array();
    foreach ($ColomnList as $key => $valuec) {
                    $tableHeader  .= '<th data-field="'.$valuec['COLM_FIELD'].'">'.$valuec['COLM_NAME'].'</th>';
          
          $headerarr[] = $valuec['COLM_NAME'];
          
                }
//var_dump($ColomnList);die;
    $dataarrfix = array();
    //$dataarr = array();
        foreach ($datareport as $key => $dataRow) {
          $string .= '<tr>';
      
      $dataarr = array();
      $dataarrnew = array();
          for ($i=0; $i < $numFields; $i++) { 
        
                                if($ColomnList[$i]['COLM_FIELD'] == 'PS_STATUS'){
                                    $ColomnList[$i]['COLM_FIELD']  = 'payStatus';
                  $dataarr[] = 'payStatus';
                                }
                                if($ColomnList[$i]['COLM_TYPE']=='1'){
                                    //type text
                                    $string .= '<td>'.$dataRow[$ColomnList[$i]['COLM_FIELD']].'</td>';
                  $dataarr[] = $dataRow[$ColomnList[$i]['COLM_FIELD']];
                                }elseif($ColomnList[$i]['COLM_TYPE']=='2'){
                                    //type number
                                    // die;
                                    $string .= '<td>'.Application_Helper_General::displayNumber($dataRow[$ColomnList[$i]['COLM_FIELD']]).'</td>';
                  $dataarr[] = Application_Helper_General::displayNumber($dataRow[$ColomnList[$i]['COLM_FIELD']]);
                                }elseif($ColomnList[$i]['COLM_TYPE']=='3'){
                                    //type date
                                    $string .= '<td>'.Application_Helper_General::convertDate($dataRow[$ColomnList[$i]['COLM_FIELD']],'dd MMMM yyyy','dd MMM yyyy').'</td>';
                                    $dataarr[] = Application_Helper_General::convertDate($dataRow[$ColomnList[$i]['COLM_FIELD']],'dd MMMM yyyy','dd MMM yyyy');
                                }elseif($ColomnList[$i]['COLM_TYPE']=='4'){
                                    //type date time
                                    $string .= '<td>'.Application_Helper_General::convertDate($dataRow[$ColomnList[$i]['COLM_FIELD']],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</td>';
                                    $dataarr[] = Application_Helper_General::convertDate($dataRow[$ColomnList[$i]['COLM_FIELD']],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss');
                                }else{
                                    $string .= '<td>'.$dataRow[$ColomnList[$i]['COLM_FIELD']].'</td>';
                  $dataarr[] = $dataRow[$ColomnList[$i]['COLM_FIELD']];
                                }

                   //             var_dump($dataarr);

                                // $i++;
                //$dataarrnew = array_merge($dataarrnew,$dataarr);

                 }
         //echo 'here';
         //var_dump($dataarr);die;
         $dataarrfix[] = $dataarr;
                 $string .= '</tr>';



          # code...
        }
        $string .= '</table>';
        // echo $string;;die;
        // foreach ($ as $key => $value) {
        //  # code...
        // }

//var_dump($value);die;
if($value['REPORT_FILE'] == '1'){

$destination = LIBRARY_PATH.'/data/uploads/document/submit/';
      $htmlTemplate = '<!DOCTYPE html><html>
            <head>
                    <title>Report PDF</title>
              <style type="text/css">
                  body {
                    margin:0px 10px 10px 10px;
                    padding:0;
                    background-color:#ffffff;
                  }

                  th {
                    background-color: #000;
                    font-family: Arial, Helvetica, sans-serif;
                    color:#FFF;
                    font-size:11px;
                    font-weight: bold;
                    padding: 5px; text-align: left;
                    border: solid 1px #A9A9A9;
                  }

                  table, td, select, textarea, input {
                    font-size: 11px;
                    font-family:  Arial, Helvetica, sans-serif;
                  }

            .tbl-headercontent {
              color:#FFF;
              background-color: #000;
              font-weight: bold;
              padding: 4px 4px 4px 4px;
              vertical-align: top;
              font-size: 12px;
            }
          
            .tbl-evencontent {
              padding: 4px 4px 4px 4px;
              vertical-align: top;
              font-size: 10px;
              background-color: #f2f2f2;
              border: 1px solid #A9A9A9
            }
            
            .tbl-oddcontent {
              padding: 4px 4px 4px 4px;
              vertical-align: top;
              font-size: 10px;
              border: 1px solid #A9A9A9
            }
            
            .tableform { 
              border-collapse: collapse;
              border: 2px solid #CCCCCC
            }
                
            thead { display: table-header-group }
            tfoot { display: table-row-group }
            tr { page-break-inside: avoid }
    </style>
  </head>
   
  <body>
      <h5>'.$value['REPORT_NAME'].'</h5>

      {FILTER}
      <br/>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">{TABLE_CONTENT}</table>
  </body>
</html>';

//            <div>
//              <table border="0" cellpadding="0" cellspacing="0" width="100%">
//              <tr>
//                <td><img src="file://'.$destination.'../../../frontend/public/images/logo/logo_pdf'.$appBankCode.'.jpg"></td>
//                <td align="center" style="float:center"><strong>'.$bankName.' - '.$appName.'</strong></td>
//                <td align="right" style="float:right"><strong>'.date("d F Y H:i:s").'</strong></td>
//              </tr>
//              </table>  
//            </div>
            
      // $string .= ;
      $tableContent =  '<table class="blueTable" style="widht:90%"><tr>'.$tableHeader.'</tr>'.$string;
      // echo $tableContent;die;

      // print_r($tableContent);die;
      
    $htmlContentSearch = '<table border="0" cellpadding="0" cellspacing="0" width="100%">';
    if($value['REPORT_SCHEDULE'] != 'one'){
      $htmlContentSearch .= '<tr><td>Report Date</td><td>:</td><td>'.date("d-M-Y", strtotime($value['REPORT_START'])).' - '.date("d-M-Y", strtotime($value['REPORT_END'])).'</td></tr>';
    }
    
    if(!empty($value['REPORT_WHERE'])){
            $whereArr = explode(';', $value['REPORT_WHERE']);
            foreach ($whereArr as $key => $valuew) {
                // print_r($whereArr);die;
                $searchkey = ( explode( ' ', $valuew ));
                if($searchkey['0'] == 'PS_EFDATE' || $searchkey['0'] == 'PS_CREATED' || $searchkey['0'] == 'PS_UPDATED'){
                    $var = $searchkey['2'];
                    $searchkey['2'] = date("d-M-Y", strtotime($var) );
          $htmlContentSearch .= '<tr><td>'.$searchkey['0'].'</td><td>:</td><td>'.$searchkey['2'].'</td></tr>';
                    //$value = 'DATE('.$searchkey['0'].') '.$searchkey['1'].' "'.$searchkey['2'].'"';
                }else{
          $htmlContentSearch .= '<tr><td>'.$searchkey['0'].'</td><td>:</td><td>'.$searchkey['2'].'</td></tr>';
        }
                
            }
            
        }
    
    
    $htmlContentSearch .= '</table>';
    
    
    
    
    $htmlPdf =  str_ireplace('{FILTER}', $htmlContentSearch, $htmlTemplate);
        $htmlPdf =  str_ireplace('{TABLE_CONTENT}', $tableContent, $htmlPdf);
      
      // print_r($htmlPdf);die;
      $generateUUID = sprintf( '%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ));   
        
    
      $generateName = date('Y-m-d'). '-' .$generateUUID.$value['ID'];

          $file = file_put_contents($destination.$generateName.'.html',$htmlPdf);

//          shell_exec('/usr/local/bin/wkhtmltopdf '. 'file://'.$destination.$generateName.'.html '. $destination.$generateName.'.pdf');
//          
//          header('Content-Description: File Transfer');
//          header('Content-Type: application/octet-stream');
//          header('Content-Disposition: attachment; filename="'.$fileName.'.pdf"');
//          header('Content-Transfer-Encoding: binary');
//          header('Expires: 0');
//          header('Cache-Control: must-revalidate');
//          header('Pragma: public');
//          header('Content-Length: ' . filesize($destination.$generateName.'.pdf'));
//
//          readfile($destination.$generateName.'.pdf');
//          
//          @unlink($destination.$generateName.'.html');
//          @unlink($destination.$generateName.'.pdf');
//     }

          
      $fileHeader = '<!DOCTYPE html><html><head><title>Header PDF</title></head><body><div style="margin-top:250px;font-size: 12px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td valign="middle"><strong>'.date("l,d-m-Y").'</strong></td>
      <td align="center" valign="middle"></td>
            <td align="right" valign="middle"><img src="https://360.espay.id/images/bluetheme//logosgo_small_2.png" style="width:100px!important"></td>
          </tr>
      <tr>
      <td colspan="3"><td align="center" valign="middle"><strong>REPORT GENERATOR</strong></td></td>
      </tr>
        </table>
            </div></body></html>';
      $fileHeader = file_put_contents($destination.$generateName.'-header.html',$fileHeader);
          $webkitOpt = '--footer-font-size 10 --footer-right "Page [page]/[topage]   " --margin-top 30mm --header-spacing 5 '.
            '--header-html file://'.$destination.$generateName.'-header.html';
          $shell = '/bin/wkhtmltopdf '.$webkitOpt.' file://'.$destination.$generateName.'.html '. $destination.$generateName.'.pdf';
          #shell_exec('/usr/local/bin/wkhtmltopdf '.$webkitOpt.' file://'.$destination.$generateName.'.html '. $destination.$generateName.'.pdf');
          shell_exec($shell);
// print_r($temporary);die;
          
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$fileName.'.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($destination.$generateName.'.pdf'));
                // $file = $destination.$generateName.'.pdf';
           //     readfile($destination.$generateName.'.pdf');
        @unlink($destination.$generateName.'-header.html');
        @unlink($destination.$generateName.'.html');
    
    
                 $updateArr = array('REPORT_FILENAME' => $generateName.'.pdf',
                  'REPORT_FILESTATUS' => 1);
                 $where['ID = ?'] = $value['ID'];
         //var_dump($where);
         //var_dump($updateArr);
                 $db->update('T_REPORT_GENERATOR',$updateArr,$where);


                  $report =  //$this->_db->fetchAll(
                        $db->select()->distinct()
                             //->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID','A.REPORT_FILE','A.REPORT_EMAIL','A.REPORT_CREATED','A.REPORT_CREATEDBY'))
                             ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
                             ->joinLeft(array('B' => 'T_REPORT_FILE'),'A.ID = B.REPORT_ID',array('B.*'));
                             
                     //);
                  $report->where('ID =?',$value['ID']);
          //echo $report;
                  $data = $db->fetchAll($report);
          if(empty($data['0']['REPORT_CREATED'])){
            $countdata = 0;
          }else{
            $countdata = count($data);
          }
                  $insertArr = array('REPORT_ID' => $value['ID'],
                                    'REPORT_FILENAME' => $generateName.'.pdf',
                                    'REPORT_CREATED' => new Zend_Db_Expr('now()'),
                                    'REPORT_INDEX'  => $countdata+1
                                  );
        //var_dump($insertArr);
                  $inserttemplate = $db->insert('T_REPORT_FILE', $insertArr);
    

}else if($value['REPORT_FILE'] == '2'){
  $list = array (
    array(date("l,d-m-Y"), '', '', ''),
    array('REPORT GENERATOR', '', ''),
    array('', ''),
  array($value['REPORT_NAME'], ''),
  array('', '')
  );
  
  
  if(!empty($value['REPORT_WHERE'])){
            $whereArr = explode(';', $value['REPORT_WHERE']);
            foreach ($whereArr as $key => $valuew) {
                // print_r($whereArr);die;
                $searchkey = ( explode( ' ', $valuew ));
                if($searchkey['0'] == 'PS_EFDATE' || $searchkey['0'] == 'PS_CREATED' || $searchkey['0'] == 'PS_UPDATED'){
                    $var = $searchkey['2'];
                    $searchkey['2'] = date("d-M-Y", strtotime($var) );
          $list[] = array($searchkey['0'],'',$searchkey['2']);
          //$htmlContentSearch .= '<tr><td>'.$searchkey['0'].'</td><td>:</td><td>'.$searchkey['2'].'</td></tr>';
                    //$value = 'DATE('.$searchkey['0'].') '.$searchkey['1'].' "'.$searchkey['2'].'"';
                }else{
          $list[] = array($searchkey['0'],'',$searchkey['2']);
          //$htmlContentSearch .= '<tr><td>'.$searchkey['0'].'</td><td>:</td><td>'.$searchkey['2'].'</td></tr>';
        }
                
            }
            
        }
    
  $list[] = $headerarr;
  $list = array_merge($list,$dataarrfix);
  //$list[] = $dataarrfix;
  //var_dump($dataarrfix);die;  
  $destination = LIBRARY_PATH.'/data/uploads/document/submit/';
  
  $generateUUID = sprintf( '%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ));   
        
    
    $generateName = date('Y-m-d'). '-' .$generateUUID.$value['ID'];
  
  //$today = date("Ymd");
  $nametext = $generateName.'.csv';
  //$path_file = LIBRARY_PATH .'/data/logs/';   

      // $myfile = fopen($path_file.$nametext, "a") or die("Unable to open file!");
  $myfile = fopen($destination.$nametext, "a");
  $list =  SGO_Csv::arr_to_csv($list);
  
  fwrite($myfile, $list);
      // fwrite($myfile, .",\n");
  fclose($myfile);

  $report =  //$this->_db->fetchAll(
                        $db->select()->distinct()
                             //->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID','A.REPORT_FILE','A.REPORT_EMAIL','A.REPORT_CREATED','A.REPORT_CREATEDBY'))
                             ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
                             ->joinLeft(array('B' => 'T_REPORT_FILE'),'A.ID = B.REPORT_ID',array('B.*'));
                             
                     //);
            $report->where('ID =?',$value['ID']);
            $data = $db->fetchAll($report);

          if(empty($data['0']['REPORT_CREATED'])){
            $countdata = 0;
          }else{
            $countdata = count($data);
          }

  $insertArr = array('REPORT_ID' => $value['ID'],
                    'REPORT_FILENAME' => $generateName.'.csv',
                    'REPORT_CREATED' => new Zend_Db_Expr('now()'),
                    'REPORT_INDEX'  => $countdata+1
                  );

  $inserttemplate = $db->insert('T_REPORT_FILE', $insertArr);
  
  $updateArr = array('REPORT_FILENAME' => $generateName.'.csv',
                  'REPORT_FILESTATUS' => 1);
    $where['ID = ?'] = $value['ID'];
  //var_dump($where);
  //var_dump($updateArr);die;
  try{
    $db->update('T_REPORT_GENERATOR',$updateArr,$where);
  }
  catch(Exception $e)
    {
    var_dump($e);die;
  }
  //$fp = fopen('file.csv', 'w');

  //foreach ($list as $fields) {
  //    fputcsv($fp, $fields);
  //  }

  //$file = file_put_contents($destination.$fp.'.html',$htmlPdf);
  //fclose($fp);
  
  
  
}


die;



  # code...
}
// print_r($data);die;



  
//  SEND FILE STATUS = 1

  


$filename = basename(__FILE__);
Application_Helper_General::cronLog($filename,$mailedTo,1);

?>