<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class paper_InventorydetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $paper_id = $this->_getParam("paper-id");

        if ($paper_id == "") $this->_redirect('/paper/inventory/');

        $select = $this->_db->select()
            ->from(array('A' => 'M_PAPER_GROUP'), array(
                "*"
            ))

            ->joinleft(array('B' => 'M_BRANCH'), 'A.PAPER_BRANCH = B.BRANCH_CODE', array("*"))
            ->where("PAPER_GROUP = $paper_id")
            ->query()->fetchAll();

        $this->view->paperdata = $select[0];


        // check paper ---------------------------------------------------
        $temp_sheet = ["available" => 0, "used" => 0, "damage" => 0];

        $startpaper = $select[0]["PAPER_FROM"] + 0;
        for ($i = 0; $i < $select[0]["PAPER_QTY"]; $i++) {
            $paperss = str_pad($startpaper, 6, "0", STR_PAD_LEFT);
            $paperss = $select[0]["PAPER_KEY"] . $paperss;
            $select2 = $this->_db->select()
                ->from(array('A' => 'M_PAPER'), array(
                    "*"
                ))
                ->where("PAPER_ID = ?", $paperss)
                ->query()->fetchAll();

            if ($select2[0]["STATUS"] == 0) $temp_sheet["available"] += 1;
            if ($select2[0]["STATUS"] == 1) $temp_sheet["used"] += 1;
            if ($select2[0]["STATUS"] == 2) $temp_sheet["damage"] += 1;

            $startpaper += 1;
        }

        $this->view->sheet = $temp_sheet;

        // ------------------------------------------------------------------

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


        $csv = $this->_getParam('csv');
        if ($csv) {

            $result = [];
            $itung = 1;

            foreach ($select as $key => $value) {
                $start = $value["PAPER_FROM"] + 0;
                for ($i = 0; $i < $value["PAPER_QTY"]; $i++) {
                    $paper_id = $value["PAPER_KEY"] . str_pad($start, 6, '0', STR_PAD_LEFT);
                    $check_m_paper = $this->_db->select()
                        ->from(array('B' => 'M_PAPER'), array("*"))
                        ->where("PAPER_ID LIKE ?", "%" . $paper_id . "%")
                        ->query()->fetch();
                    if (!$check_m_paper) {
                        $temp = [$itung, $paper_id, "Available", '', $value["BRANCH_NAME"], "-", "-"];
                    } else {
                        if ($check_m_paper["STATUS"] == '1') $status = "Printed";
                        if ($check_m_paper["STATUS"] == '2') $status = "Damaged";
                        $temp = [$itung, $paper_id, $status, $check_m_paper["NOTES"], $value["BRANCH_NAME"], $check_m_paper["PAPER_UPDATED"], $check_m_paper["PAPER_UPDATEDBY"]];
                    }
                    array_push($result, $temp);

                    $itung += 1;
                    $start += 1;
                }
            }

            Application_Helper_General::writeLog('ADBU', 'Download CSV Paper Inventory Report');
            $this->_helper->download->csv(array($this->language->_('No'), $this->language->_('Paper Number'), $this->language->_('Status'), $this->language->_('Description'), $this->language->_('Bank Branch'), $this->language->_('Last Updated'), $this->language->_('Last Updated By')), $result, null, $this->language->_('Paper Inventory ' . $select[0]["PAPER_KEY"] . $select[0]["PAPER_FROM"] . "-" . $select[0]["PAPER_KEY"] . $select[0]["PAPER_TO"]));
        }
    }

    public function deleteAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        if (!$this->_request->isPost()) {
            $this->_redirect('home/dashboard');
        }

        $paper_id = $this->_request->getParam('paper_id');

        if (!$paper_id) {
            $this->_redirect('/home/dashboard');
        }

        $select = $this->_db->select()
            ->from(array('A' => 'M_PAPER_GROUP'), array(
                "*"
            ))
            ->joinleft(array('B' => 'M_BRANCH'), 'A.PAPER_BRANCH = B.BRANCH_CODE', array("*"))
            ->where("PAPER_GROUP = $paper_id")
            ->query()->fetchAll();


        // check paper ---------------------------------------------------
        $temp_sheet = ["available" => 0, "used" => 0, "damage" => 0];

        $startpaper = $select[0]["PAPER_FROM"] + 0;
        for ($i = 0; $i < $select[0]["PAPER_QTY"]; $i++) {
            $paperss = str_pad($startpaper, 6, "0", STR_PAD_LEFT);
            $paperss = $select[0]["PAPER_KEY"] . $paperss;
            $select2 = $this->_db->select()
                ->from(array('A' => 'M_PAPER'), array(
                    "*"
                ))
                ->where("PAPER_ID = ?", $paperss)
                ->query()->fetchAll();

            if ($select2[0]["STATUS"] == 0) $temp_sheet["available"] += 1;
            if ($select2[0]["STATUS"] == 1) $temp_sheet["used"] += 1;
            if ($select2[0]["STATUS"] == 2) $temp_sheet["damage"] += 1;

            $startpaper += 1;
        }

        if ($temp_sheet['used'] !== 0 && $temp_sheet["damage"] !== 0) {
            $this->_redirect('/home/dashboard');
        }

        if ($this->_db->delete('M_PAPER_GROUP', ['PAPER_GROUP = ?' => $paper_id])) {

            Application_Helper_General::writeLog("VPAM", "Delete Paper Inventory, Branch " . $select[0]['BRANCH_NAME']);
            $this->setbackURL('/paper/inventory');
            $this->_redirect('/notification/success');
        }
    }
}
