<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'Service/Account.php';

class marginaldeposit_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new marginaldeposit_Model_Marginaldeposit();

		$setting 	= new Settings();
		$enc_pass 	= $setting->getSetting('enc_pass');
		$enc_salt 	= $setting->getSetting('enc_salt');

		$sessToken 			= new Zend_Session_Namespace('Tokenenc');
		$password_hash 		= md5($enc_salt . $enc_pass);
		$rand 				= $this->_userIdLogin . date('YmdHis') . $password_hash;
		$sessToken->token 	= $rand;
		$this->view->token 	= $sessToken->token;

		$statusArr = [
			1 => $this->language->_('Complete'),
			2 => $this->language->_('Not Complete'),
		];
		$this->view->statusArr = $statusArr;
		
		$refresh = $this->_getParam('refresh');
		if ($refresh) {
			$app      = Zend_Registry::get('config');
			$bankCode = $app['app']['bankcode'];

			$sessToken 	= new Zend_Session_Namespace('Tokenenc');
			$password 	= $sessToken->token;

			$AESMYSQL = new Crypt_AESMYSQL();
			$decryption = urldecode($refresh);
			$custID = $AESMYSQL->decrypt($decryption, $password);

			$currentGuarantee = 0;
			$dataDetail = $model->getDataDetail($custID);
			foreach ($dataDetail as $row) {
				$acctNo  = $row['MD_ACCT'];
				$acctCcy = Application_Helper_General::getCurrNum($row['MD_ACCT_CCY']);

				$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
				$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
				//var_dump($result);die;
				if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
					$currentGuarantee = (float)$result['balance_active'];
					$dataUpdate  = ['GUARANTEE_AMOUNT' => $result['balance_active']];
					$whereUpdate = ['MD_ACCT = ?' => $acctNo];

					//$this->_db->beginTransaction();
					try {
						//var_dump($dataUpdate);
						//var_dump($whereUpdate);
						$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
					} catch (Exception $error) {
						//var_dump($error);die;
						$this->_db->rollBack();
						//echo '<pre>';
						//print_r($error->getMessage());
						//echo '</pre><br>';
						//die;
					}
				} else {

					$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
					$result = $svcAccount->inquiryDeposito('AB', TRUE);
					//var_dump($result);die;
					if ($result['response_code'] == '0000') {
						$dataUpdate  = ['GUARANTEE_AMOUNT' => $result['balance']];
						$whereUpdate = ['MD_ACCT = ?' => $acctNo];
						$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
					}
				}
			}

			$check_cg_completeness = $this->_db->select()
				->from("T_BANK_GUARANTEE_DETAIL")
				->where("PS_FIELDNAME = ?", "Insurance Name")
				->where("PS_FIELDVALUE = ?", $custID)
				->query()->fetchAll();

			$cg_completeness = 0;
			if (count($check_cg_completeness) > 0) {
				$checker = 0;
				foreach ($check_cg_completeness as $key => $value) {
					$check_cgins_status = $this->_db->select()
						->from("T_BANK_GUARANTEE")
						->where("BG_REG_NUMBER = ?", $value["BG_REG_NUMBER"])
						->where("CGINS_STATUS = ?", "1");

					$check_cgins_status = $this->_db->fetchRow($check_cgins_status);
					if ($check_cgins_status) {
						$checker += 1;
					}
				}
				if ($checker == count($check_cg_completeness)) {
					$cg_completeness = 1;
				}
			}

			$this->_db->update('M_MARGINALDEPOSIT', [
				"CG_COMPLETENESS" => $cg_completeness,
			], [
				"CUST_ID = ?" => $custID
			]);

			$dataUpdate2  = ['LAST_CHECK' => new Zend_Db_Expr('now()')];
			$whereUpdate2 = ['CUST_ID = ?' => $custID];

			//$this->_db->beginTransaction();
			try {
				$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
			} catch (Exception $error) {
				$this->_db->rollBack();
				//echo '<pre>';
				//print_r($error->getMessage());
				//echo '</pre><br>';
				//die;
			}

			// start validasi top up kurang dari marginal deposit
			$cek_min_md = $this->_db->select()
				->from("M_CUST_LINEFACILITY")
				->where("CUST_ID = ?", $custID)
				->query()->fetchAll();

			$filter = true;
			$filterParam['fCustID'] 	= $custID;

			$data = $model->getData($filterParam, $filter);

			$dataDetail = $model->getDataDetail($custID);

			foreach ($dataDetail as $row) {
				$GUARANTEE_AMOUNT += $row['GUARANTEE_AMOUNT'];
			}

			foreach ($data as $row) {
				$COUNTING_DEADLINE_TOPUP += $row['COUNTING_DEADLINE_TOPUP'];
			}

			if ($GUARANTEE_AMOUNT < $cek_min_md[0]['MARGINAL_DEPOSIT']) {

				$mdData = $this->_db->select()
					->from("M_MARGINALDEPOSIT")
					->where("CUST_ID = ?", $custID);

				$mdData = $this->_db->fetchRow($mdData);

				if ($mdData['LAST_CHECK_MINUS'] == null) {

					$dataUpdate2  = ['LAST_CHECK_MINUS' => new Zend_Db_Expr('now()'), 'COUNTING_DEADLINE_TOPUP' => $COUNTING_DEADLINE_TOPUP + 1];
					$whereUpdate2 = ['CUST_ID = ?' => $custID];
					//var_dump($dataUpdate2);die;
					//$this->_db->beginTransaction();
					try {
						$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
					} catch (Exception $error) {
						$this->_db->rollBack();
						//echo '<pre>';
						//print_r($error->getMessage());
						//echo '</pre><br>';
						//die;
					}
				}
			} else {
				$sqlCekFreezeManual = $this->_db->select()
					->from("M_CUST_LINEFACILITY")
					->where("CUST_ID = ?", $custID);

				$dataFreezeManual = $sqlCekFreezeManual->query()->fetchAll();

				if ($dataFreezeManual[0]['FREEZE_MANUAL'] != '1') {

					$dataUpdate  = ['STATUS' => '1'];
					$whereUpdate = ['CUST_ID = ?' => $custID];

					$this->_db->update('M_CUST_LINEFACILITY', $dataUpdate, $whereUpdate);
					$x = 1;
				}

				$dataUpdate2  = ['LAST_CHECK_MINUS' => null];
				$dataUpdate2  = ['COUNTING_DEADLINE_TOPUP' => null];
				$whereUpdate2 = ['CUST_ID = ?' => $custID];
				//var_dump($dataUpdate2);die;
				//$this->_db->beginTransaction();
				try {
					$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
				} catch (Exception $error) {
					$this->_db->rollBack();
					//echo '<pre>';
					//print_r($error->getMessage());
					//echo '</pre><br>';
					//die;
				}
			}

			// end validasi top up kurang dari marginal deposit



		}

		$select = $this->_db->select()
			->from(
				array('A' => 'M_MARGINALDEPOSIT'),
				array('*')
			)
			->joinLeft(
				array('C' => 'M_CUSTOMER'),
				'C.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			)
			->joinLeft(
				array('D' => 'M_CUST_LINEFACILITY'),
				'D.CUST_ID = A.CUST_ID',
				array('PLAFOND_LIMIT', 'MARGINAL_DEPOSIT', "PKS_STATUS_LF" => "D.STATUS")
			);
			
		// advance filter
		$filterlist = array("ASURANSI" => "fAsuransi", "Tanggal MD Berkurang" => "fTanggal");

        $this->view->filterlist = $filterlist;
		$filterArr = array(
            'filter'    =>  array('StripTags'),
            'fAsuransi'    =>  array('StringTrim', 'StripTags'),
            'fTanggal' =>  array('StringTrim', 'StripTags'),
            'fTanggalEnd'    =>  array('StringTrim', 'StripTags'),
        );

        $validator = array(
            'filter'                 => array(),
            'fAsuransi' => array(),
            'fTanggal'         => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
            'fTanggalEnd'             => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
        );


        $dataParam = array("fAsuransi", "fTanggal");
        $dataParamValue = array();

        $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
        $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
        // print_r($this->_request->getParam('wherecol'));
        foreach ($dataParam as $no => $dtParam) {

            if (!empty($this->_request->getParam('wherecol'))) {
                $dataval = $this->_request->getParam('whereval');
                // print_r($dataval);
                $order = 0;
                foreach ($this->_request->getParam('wherecol') as $key => $value) {
                    if ($value == "fTanggal") {
                        $order--;
                    }
                    if ($dtParam == $value) {
                        $dataParamValue[$dtParam] = $dataval[$order];
                    }
                    $order++;
                }
            }
        }


        // print_r($dataParamValue);die;

        if (!empty($this->_request->getParam('tanggal'))) {
            $updatearr = $this->_request->getParam('tanggal');
            $dataParamValue['fTanggal'] = $updatearr[0];
            $dataParamValue['fTanggalEnd'] = $updatearr[1];
        }


        $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
        // $filter 	= $zf_filter->getEscaped('filter');
        $filter         = $this->_getParam('filter');
        $asuransi     = html_entity_decode($zf_filter->getEscaped('fAsuransi'));

        $datefrom     = html_entity_decode($zf_filter->getEscaped('fTanggal'));
        $dateto     = html_entity_decode($zf_filter->getEscaped('fTanggalEnd'));


        if ($filter_clear == true) {

            $datefrom     = '';
            $dateto     = '';
        }

        if ($filter == TRUE) {

            $this->view->fDateTo    = $dateto;
            $this->view->fDateFrom  = $datefrom;


            if ($datefrom) {
                $FormatDate     = new Zend_Date($datefrom, $this->_dateDisplayFormat);
                $datefrom      = $FormatDate->toString($this->_dateDBFormat);
                $select->where("DATE(A.LAST_CHECK_MINUS) >= ?", $datefrom);
            }

            if ($dateto) {
                $FormatDate     = new Zend_Date($dateto, $this->_dateDisplayFormat);
                $dateto          = $FormatDate->toString($this->_dateDBFormat);
                $select->where("DATE(A.LAST_CHECK_MINUS) <= ?", $dateto);
            }

            if ($asuransi != null) {
                $this->view->asuransi = $asuransi;
                $select->where('C.CUST_NAME LIKE ' . $this->_db->quote('%' . $asuransi. '%'));
            }
        }

        unset($dataParamValue['fTanggalEnd']);
        if (!empty($dataParamValue)) {
            foreach ($dataParamValue as $key => $value) {
                $wherecol[]    = $key;
                $whereval[] = $value;
            }

            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }


		//
		
		$select_marginal_deposit = $select->query()->fetchAll();


		foreach ($select_marginal_deposit as $key => $value) {
			$temp_query = "SELECT SUM(GUARANTEE_AMOUNT) AS GUARANTEE_AMOUNT FROM M_MARGINALDEPOSIT_DETAIL WHERE CUST_ID = '" . $value["CUST_ID"] . "'";
			$temp_query = $this->_db->fetchRow($temp_query);
			$select_marginal_deposit[$key]["GUARANTEE_AMOUNT"] = $temp_query["GUARANTEE_AMOUNT"];
		}



		//var_dump($data);
		$this->paging($select_marginal_deposit);
		// echo '<pre>';print_r($data);echo '</pre>';die('data');

		if ($this->_request->getParam("csv")) {

			$statusLF = ["Aktif", "Perjanjian Berhenti", "Kadaluarsa", "Freeze Pengajuan"];

			$save_temp = [];
			foreach ($select_marginal_deposit as $key => $value) {
				array_push($save_temp, [
					$key + 1,
					// ucwords(strtolower($value["CUST_NAME"])) . " (" . $value["CUST_ID"] . ")",
					$value["CUST_NAME"] . " (" . $value["CUST_ID"] . ")",
					$value["PLAFOND_LIMIT"],
					$value["MARGINAL_DEPOSIT"],
					$value["GUARANTEE_AMOUNT"],
					// $statusArr[$value["CG_COMPLETENESS"]] ? : '-',
					($value['LAST_CHECK_MINUS']) ? date('d M Y H:i', strtotime($value['LAST_CHECK_MINUS'])) : '-',
					($value['LAST_CHECK']) ? date('d M Y H:i', strtotime($value['LAST_CHECK'])) : '-',
					//$statusLF[$value["PKS_STATUS_LF"]],

				]);
			}

			$head = [
				"No",
				$this->language->_('Insurance'),
				$this->language->_('Plafond Limit'),
				$this->language->_('Marginal Deposit'),
				$this->language->_('Current Guarantee'),
				// $this->language->_('Completeness Guarantee'),
				$this->language->_('Tanggal Awal MD Berkurang'),
				$this->language->_('Last Check')
				//$this->language->_('Status')
			];
			return $this->_helper->download->csv($head, $save_temp, null, $this->language->_('Marginal Deposit List - ' . date("Ymd-His")));
		}

		$this->view->filter 		= $filter;
		$this->view->fCompanyName 	= $fCompanyName;
		$this->view->fStatus 		= $fStatus;
		// $this->view->fDateFrom 	= $fDateFrom;
		// $this->view->fDateTo 	= $fDateTo;

		Application_Helper_General::writeLog('VIMD', 'View Insurance Marginal Deposit');
	}

	public function detailAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new marginaldeposit_Model_Marginaldeposit();

		$id = $this->_getParam('id');
		$download = $this->_getParam('download');
		$submit = $this->_getParam('submit');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$custID = $AESMYSQL->decrypt($decryption, $password);

		$params = $this->getRequest()->getParams();
		$this->view->params = $params;
		$filter = true;
		$filterParam['fCustID'] 	= $custID;

		$data = $model->getData($filterParam, $filter);

		$select_marginal_deposit = $this->_db->select()
			->from(
				array('A' => 'M_MARGINALDEPOSIT'),
				array('*')
			)
			->joinLeft(
				array('C' => 'M_CUSTOMER'),
				'C.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			)
			->joinLeft(
				array('D' => 'M_CUST_LINEFACILITY'),
				'D.CUST_ID = A.CUST_ID',
				array('PLAFOND_LIMIT', 'MARGINAL_DEPOSIT', "PKS_STATUS_LF" => "D.STATUS")
			)
			->where("A.CUST_ID = ?", $custID)
			->query()->fetchAll();

		// $select_marginal_deposit = $this->_db->fetchRow($select_marginal_deposit);
		$this->view->data = $select_marginal_deposit;

		$dataDetail = $model->getDataDetail($custID);
		$this->view->dataDetail = $dataDetail;

		$totalDetained = 0;
		foreach ($data as $row) {
			$totalDetained = $totalDetained + (int)$row['GUARANTEE_AMOUNT'];
		}
		$this->view->totalDetained = $totalDetained;
		// echo '<pre>';print_r($totalDetained);echo '</pre>';die('totalDetained');

		// $data = $model->getDataById($custID);
		// $this->view->data = $data;

		$refresh = $this->_getParam('refresh');
		if ($refresh) {
			$app      = Zend_Registry::get('config');
			$bankCode = $app['app']['bankcode'];

			$sessToken 	= new Zend_Session_Namespace('Tokenenc');
			$password 	= $sessToken->token;

			$AESMYSQL = new Crypt_AESMYSQL();
			$decryption = urldecode($refresh);
			$custID = $AESMYSQL->decrypt($decryption, $password);

			$currentGuarantee = 0;
			$dataDetail = $model->getDataDetail($custID);
			//var_dump($dataDetail);die;
			foreach ($dataDetail as $row) {
				$acctNo  = $row['MD_ACCT'];
				$acctCcy = Application_Helper_General::getCurrNum($row['MD_ACCT_CCY']);
				if ($row['MD_ACCT_TYPE'] == 'Deposito') {
					$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
					$result = $svcAccount->inquiryLockDeposito('EM');
					//var_dump($result);die;
				} else {
					$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
					$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
				}
				//]]var_dump($result);die;

				if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
					$currentGuarantee = $currentGuarantee + (float)$result['balance_active'];
					$dataUpdate  = ['GUARANTEE_AMOUNT' => $result['balance_active']];
					$whereUpdate = ['MD_ACCT = ?' => $acctNo];

					$this->_db->beginTransaction();
					try {
						$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
					} catch (Exception $error) {
						$this->_db->rollBack();
						echo '<pre>';
						print_r($error->getMessage());
						echo '</pre><br>';
						die;
					}
				}
			}


			$dataUpdate2  = ['LAST_CHECK' => new Zend_Db_Expr('now()')];
			$whereUpdate2 = ['CUST_ID = ?' => $custID];


			//$this->_db->beginTransaction();
			try {

				$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
				//$this->_db->commit();
			} catch (Exception $error) {

				var_dump($error);
				die;
				$this->_db->rollBack();
				echo '<pre>';
				print_r($error->getMessage());
				echo '</pre><br>';
				die;
			}


			// start validasi top up kurang dari marginal deposit
			$cek_min_md = $this->_db->select()
				->from("M_CUST_LINEFACILITY")
				->where("CUST_ID = ?", $custID)
				->query()->fetchAll();

			$filter = true;
			$filterParam['fCustID'] 	= $custID;

			$data = $model->getData($filterParam, $filter);

			$dataDetail = $model->getDataDetail($custID);

			foreach ($dataDetail as $row) {
				$GUARANTEE_AMOUNT += $row['GUARANTEE_AMOUNT'];
			}

			foreach ($data as $row) {
				$COUNTING_DEADLINE_TOPUP += $row['COUNTING_DEADLINE_TOPUP'];
			}

			if ($GUARANTEE_AMOUNT < $cek_min_md[0]['MARGINAL_DEPOSIT']) {
				$mdData = $this->_db->select()
					->from("M_MARGINALDEPOSIT")
					->where("CUST_ID = ?", $custID);

				$mdData = $this->_db->fetchRow($mdData);

				if ($mdData['LAST_CHECK_MINUS'] == null) {

					$dataUpdate2  = ['LAST_CHECK_MINUS' => new Zend_Db_Expr('now()'), 'COUNTING_DEADLINE_TOPUP' => $COUNTING_DEADLINE_TOPUP + 1];
					$whereUpdate2 = ['CUST_ID = ?' => $custID];
					//var_dump($dataUpdate2);die;
					//$this->_db->beginTransaction();
					try {
						$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
					} catch (Exception $error) {
						$this->_db->rollBack();
						//echo '<pre>';
						//print_r($error->getMessage());
						//echo '</pre><br>';
						//die;
					}
				}
			} else {

				$sqlCekFreezeManual = $this->_db->select()
					->from("M_CUST_LINEFACILITY")
					->where("CUST_ID = ?", $custID);

				$dataFreezeManual = $sqlCekFreezeManual->query()->fetchAll();

				if ($dataFreezeManual[0]['FREEZE_MANUAL'] != '1') {

					$dataUpdate  = ['STATUS' => '1'];
					$whereUpdate = ['CUST_ID = ?' => $custID];

					$this->_db->update('M_CUST_LINEFACILITY', $dataUpdate, $whereUpdate);
					$x = 1;
				}

				$dataUpdate2  = ['LAST_CHECK_MINUS' => null];
				$dataUpdate2  = ['COUNTING_DEADLINE_TOPUP' => null];
				$whereUpdate2 = ['CUST_ID = ?' => $custID];
				//var_dump($dataUpdate2);die;
				//$this->_db->beginTransaction();
				try {
					$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
				} catch (Exception $error) {
					$this->_db->rollBack();
					//echo '<pre>';
					//print_r($error->getMessage());
					//echo '</pre><br>';
					//die;
				}
			}

			// end validasi top up kurang dari marginal deposit

		}
	}


	public function topupAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new marginaldeposit_Model_Marginaldeposit();

		$id = $this->_getParam('id');
		$download = $this->_getParam('download');
		$submit = $this->_getParam('submit');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$custID = $AESMYSQL->decrypt($decryption, $password);

		$params = $this->getRequest()->getParams();
		$this->view->params = $params;

		// $data = $model->getData();
		$data = $this->_db->select()
			->from(
				array('A' => 'M_MARGINALDEPOSIT'),
				array('*')
			)
			->joinLeft(
				array('C' => 'M_CUSTOMER'),
				'C.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			)
			->joinLeft(
				array('D' => 'M_CUST_LINEFACILITY'),
				'D.CUST_ID = A.CUST_ID',
				array('PLAFOND_LIMIT', 'MARGINAL_DEPOSIT', "PKS_STATUS_LF" => "D.STATUS")
			)
			->where("A.CUST_ID = ?", $custID)
			->query()->fetchAll();

		// $select_marginal_deposit = $this->_db->fetchRow($select_marginal_deposit);
		$this->view->data = $data;
		// $this->view->data = $data;
		// echo '<pre>';print_r($data);echo '</pre>';die('data');

		$custName = $data[0]['CUST_NAME'];

		$dataDetail = $model->getDataDetail($custID);
		$this->view->dataDetail = $dataDetail;

		$getDataBGAcct = $model->getDataBGAcct($custID);

		if ($getDataBGAcct) {
			foreach ($getDataBGAcct as $row) {
				$list[] = $row['ACCT'];
			}
		}




		//var_dump($list);die;

		$getCustAcctById = $model->getCustAcctById($custID, $list);



		$custAcctArr = [];
		if ($getCustAcctById) {
			foreach ($getCustAcctById as $row) {
				$custAcctArr[$row['ACCT_NO']] = $row['ACCT_NO'] . ' [' . $row['CCY_ID'] . '] / ' . $row['ACCT_NAME'] . ' / ' . $row['ACCT_DESC'];
			}
		}
		$this->view->custAcctArr = $custAcctArr;

		// start cek balance
		$currentGuarantee = 0;
		//var_dump($dataDetail);die;
		if ($dataDetail) {

			foreach ($dataDetail as $row) {
				$acctNo  = $row['MD_ACCT'];
				$acctCcy = Application_Helper_General::getCurrNum($row['MD_ACCT_CCY']);
				if ($row['MD_ACCT_TYPE'] == 'Deposito') {
					$svcAccountD = new Service_Account($acctNo, $acctCcy, $bankCode);
					$resultD = $svcAccountD->inquiryDeposito('AB', TRUE);
					if ($resultD['response_code'] == '00' || $resultD['response_code'] == '0000') {
						//$saldoGiro =  (float)$resultG['balance_active'];
						$dataUpdate  = ['GUARANTEE_AMOUNT' => $resultD['balance']];
						$whereUpdate = ['MD_ACCT = ?' => $acctNo];

						$this->_db->beginTransaction();
						try {
							$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
						} catch (Exception $error) {
							$this->_db->rollBack();
							echo '<pre>';
							print_r($error->getMessage());
							echo '</pre><br>';
							die;
						}
					}
				} else {

					$svcAccountG = new Service_Account($acctNo, $acctCcy, $bankCode);
					$resultG = $svcAccountG->inquiryAccountBalance('AB', FALSE);
					if ($resultG['response_code'] == '00' || $resultG['response_code'] == '0000') {
						//$saldoGiro =  (float)$resultG['balance_active'];
						$dataUpdate  = ['GUARANTEE_AMOUNT' => $resultG['balance_active']];
						$whereUpdate = ['MD_ACCT = ?' => $acctNo];

						$this->_db->beginTransaction();
						try {
							$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
						} catch (Exception $error) {
							$this->_db->rollBack();
							echo '<pre>';
							print_r($error->getMessage());
							echo '</pre><br>';
							die;
						}
					}
				}
				//var_dump($result);die;




			}

			$dataUpdate2  = ['LAST_CHECK' => new Zend_Db_Expr('now()')];
			$whereUpdate2 = ['CUST_ID = ?' => $custID];

			//$this->_db->beginTransaction();
			try {

				$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
				//die;
				//$this->_db->commit();
			} catch (Exception $error) {
				$this->_db->rollBack();
				echo '<pre>';
				print_r($error->getMessage());
				echo '</pre><br>';
				die;
			}
		}

		// end cek balance

		$totalDetained = 0;
		foreach ($dataDetail as $row) {
			$totalDetained = $totalDetained + (float)$row['GUARANTEE_AMOUNT'];
		}

		//echo $totalDetained;
		$this->view->totalDetained = $totalDetained;
		// echo '<pre>';print_r($getBuser);echo '</pre>';die;



		$process   = $this->_getParam('process');

		$isConfirm = (empty($this->_request->getParam('isConfirm'))) ? false : true;
		$submitBtn = ($this->_request->isPost() && $process == "submit") ? true : false;

		if ($this->_request->isPost()) {
			$filters    = array('*' => array('StringTrim', 'StripTags'));

			$zf_filter_input = new Zend_Filter_Input($filters, null, $this->_request->getPost());

			$acctNo 		= $zf_filter_input->acctNo;
			$acctName 		= $zf_filter_input->acctName;
			$acctType 		= $zf_filter_input->acctType;
			$acctCcy 		= $zf_filter_input->acctCcy;
			$balance 		= $zf_filter_input->balance;

			if ($submitBtn) {
				//var_dump($acctNo);die;
				//$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
				//$result = $svcAccount->inquiryAccountBalance('AB', FALSE);

				if ($isConfirm == false) {
					//$isConfirm = true;
					$totalTopup = 0;
					for ($i = 0; $i < count($balance); $i++) {
						$balanceAmount = str_replace(',', '', $balance[$i]);
						$totalTopup = $totalTopup + (float)$balanceAmount;
						$this->view->balance[$i] = $balanceAmount;
					}

					//$subtotal = $totalDetained + $totalTopup;
					foreach ($dataDetail as $row) {

						$svcAccount = new Service_Account($row['MD_ACCT'], $row['MD_ACCT_CCY']);
						$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
						//var_dump($result);die;
						if ($result['response_code'] == '0000') {

							$saldoJaminan += $result['balance_active'];
						}
					}



					$subtotal = $saldoJaminan + $totalTopup;
					$totalMD  = (float)$data[0]['MARGINAL_DEPOSIT'];
					//echo $subtotal;die;
					if ($subtotal < $totalMD) {
						$this->view->error = true;
						//$this->view->errorMsg = $this->language->_('Total Guarantee Fund must be equal to or greater than the Marginal Deposit that has been determined');
						$this->view->errorMsg = $this->language->_('Jumlah Dana Jaminan harus sama atau lebih besar dari Marginal Deposit yang telah ditentukan');

						$isConfirm = false;
					} else {
						$validasi = false;
						for ($i = 0; $i < count($balance); $i++) {
							$balanceAmount = str_replace(',', '', $balance[$i]);

							//$totalTopup = $totalTopup + (float)$balanceAmount;
							$totalTopup = (float)$balanceAmount;
							$this->view->balance[$i] = $balanceAmount;

							$svcAccount2 = new Service_Account($acctNo[$i], $acctCcy[$i]);
							$result2 = $svcAccount2->inquiryAccountBalance('AB', FALSE);

							if ($result2['response_code'] == '0000' || $result2['response_code'] == '00') {

								$saldoJaminan += $result2['balance_active'];

								if ($result2['status'] != '1') {

									$errmsg[$i] = 'Status rekening tidak aktif';
									$this->view->validasiRekening = $errmsg;
									$isConfirm = false;
									$validasi = true;
								}

								$check_prod_type = $this->_db->select()
									->from("M_PRODUCT_TYPE")
									->where("PRODUCT_CODE = ?", $result2["account_type"])
									->query()->fetch();

								$result2["check_product_type"] = 0;
								if (empty($check_prod_type)) {
									$result2["check_product_type"] = 1;
									$errmsg[$i] = 'Product type tidak terdaftar di product management';
									$this->view->validasiRekening = $errmsg;
									$isConfirm = false;
									$validasi = true;
								}

								$check_cust_acct = $this->_db->select()
									->from("M_CUSTOMER_ACCT")
									->where("ACCT_STATUS = ?", "1")
									->where("ACCT_NO = ?", $result2["account_number"])
									->query()->fetch();

								$result2["check_cust_acct"] = 0;
								if (empty($check_cust_acct)) {
									$result2["check_cust_acct"] = 1;
									$errmsg[$i] = 'Rekening Nasabah pada sistem bukan status approved';
									$this->view->validasiRekening = $errmsg;
									$isConfirm = false;
									$validasi = true;
								}

								if ($totalTopup > $result2['balance_active']) {
									$errmsg[$i] = 'Saldo tidak mencukupi';
									$this->view->errorLimitSaldo = $errmsg;
									$isConfirm = false;
									$validasi = true;
								}
							}
						}
						if ($validasi == false) {
							$isConfirm = true;
						}
					}
				} else {

					//$this->_db->beginTransaction();
					var_dump("tets");die();

					try {

						$info 		= 'Customer ID = ' . $custID . ', Customer Name = ' . $custName;
						$change_id 	= $this->suggestionWaitingApproval('Marginal Deposit', $info, $this->_changeType['code']['new'], null, 'M_MARGINALDEPOSIT,M_MARGINALDEPOSIT_DETAIL', 'TEMP_MARGINALDEPOSIT,TEMP_MARGINALDEPOSIT_DETAIL', $custID, $custName, $custID, $custName);

						$dataInsert = [
							'CHANGES_ID'		=> $change_id,
							'CUST_ID'			=> $custID,
							'CG_COMPLETENESS'	=> $data[0]['CG_COMPLETENESS'],
							'CURRENT_GUARANTEE'	=> $data[0]['CURRENT_GUARANTEE'],
							'LAST_CHECK'		=> $data[0]['LAST_CHECK'],
							'PKS_STATUS'		=> $data[0]['PKS_STATUS'],
							'FLAG'				=> 1, // Waiting Review
							'PKS_STATUS'		=> $data[0]['PKS_STATUS'],
							'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
							'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
						];
						$this->_db->insert('TEMP_MARGINALDEPOSIT', $dataInsert);

						for ($i = 0; $i < count($acctNo); $i++) {
							$dataInsertDetail = [
								'CHANGES_ID'		=> $change_id,
								'CUST_ID'			=> $custID,
								'MD_ACCT'			=> $acctNo[$i],
								'MD_ACCT_NAME'		=> $acctName[$i],
								'MD_ACCT_TYPE'		=> $acctType[$i],
								'MD_ACCT_CCY'		=> $acctCcy[$i],
								'GUARANTEE_AMOUNT'	=> str_replace(',', '', $balance[$i]),
							];
							$this->_db->insert('TEMP_MARGINALDEPOSIT_DETAIL', $dataInsertDetail);
						}
						//$this->_db->commit();
						Application_Helper_General::writeLog('TUMD', 'Top Up Marginal Deposit for ' . $custName . ' (' . $custID . ')');
						$this->setbackURL('/marginaldeposit');
						$this->_redirect('/notification/submited/index');
					} catch (Exception $error) {
						$this->_db->rollBack();
						echo '<pre>';
						print_r($error->getMessage());
						echo '</pre><br>';
						die;
					}
				}
			} else {
				$isConfirm = false;
			}
		} else {
			Application_Helper_General::writeLog('TUMD', 'View Top Up Marginal Deposit');
		}

		$this->view->isConfirm 		= $isConfirm;
		$this->view->acctNo 		= $acctNo;
		$this->view->acctName 		= $acctName;
		$this->view->acctType 		= $acctType;
		$this->view->acctCcy 		= $acctCcy;
		$this->view->balance 		= $balance;
	}

	public function listacctAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new marginaldeposit_Model_Marginaldeposit();

		$id   = $this->_getParam('id');
		$list = $this->_getParam('list');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$custID = $AESMYSQL->decrypt($decryption, $password);
		$listdepo = $model->getDataDetail($custID);
		$getCustAcctById = $model->getCustAcctById($custID, $list);

		$custAcctArr = [];
		if ($getCustAcctById) {
			foreach ($getCustAcctById as $row) {
				if (!empty($listdepo)) {
					foreach ($listdepo as $vl) {
						if ($vl['MD_ACCT_TYPE'] == 'Deposito' && $vl['MD_ACCT'] == $row['ACCT_NO']) {
						} else {
							$custAcctArr[$row['ACCT_NO']] = $row['ACCT_NO'] . ' [' . $row['CCY_ID'] . '] / ' . $row['ACCT_NAME'] . ' / ' . $row['ACCT_DESC'];
						}
					}
				} else {
					$custAcctArr[$row['ACCT_NO']] = $row['ACCT_NO'] . ' [' . $row['CCY_ID'] . '] / ' . $row['ACCT_NAME'] . ' / ' . $row['ACCT_DESC'];
				}
			}
		}

		echo json_encode($custAcctArr);
	}

	public function selectacctAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new marginaldeposit_Model_Marginaldeposit();

		$request = $this->getRequest();

		$acctNo  = $request->acctNo;

		$custAcctNo = $model->getCustAcctByAcctNo($acctNo);

		echo json_encode($custAcctNo);
	}

	public function inquiryaccountbalanceAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$app      = Zend_Registry::get('config');
		$bankCode = $app['app']['bankcode'];

		$request = $this->getRequest();
		$acctNo  = $request->acctNo;

		$ccy = Application_Helper_General::getCurrNum('IDR');

		$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
		$result = $svcAccount->inquiryAccountBalance('AB', TRUE);

		if ($result['response_code'] != '0000') {

			$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
			$result = $svcAccount->inquiryDeposito('AB', TRUE);
		}

		$check_prod_type = $this->_db->select()
			->from("M_PRODUCT_TYPE")
			->where("PRODUCT_CODE = ?", $result["account_type"])
			->query()->fetch();

		$result["check_product_type"] = 0;
		if (!empty($check_prod_type)) {
			$result["check_product_type"] = 1;
		}

		$check_cust_acct = $this->_db->select()
			->from("M_CUSTOMER_ACCT")
			->where("ACCT_STATUS = ?", "1")
			->where("ACCT_NO = ?", $result["account_number"])
			->query()->fetch();

		$result["check_cust_acct"] = 0;
		if (!empty($check_cust_acct)) {
			$result["check_cust_acct"] = 1;
		}

		//var_dump($result);die;
		echo json_encode($result);
	}

	public function lockdepositoAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$app      = Zend_Registry::get('config');
		$bankCode = $app['app']['bankcode'];

		$request = $this->getRequest();
		$acctNo  = $request->acctNo;

		$ccy = Application_Helper_General::getCurrNum('IDR');

		$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
		$result = $svcAccount->lockDeposito();

		echo json_encode($result);
	}

	public function inquirydepositoAction()
	{

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');

		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryDeposito('AB', TRUE);

		$check_prod_type = $this->_db->select()
			->from("M_PRODUCT_TYPE")
			->where("PRODUCT_CODE = ?", $result["account_type"])
			->query()->fetch();

		$result["check_product_type"] = 0;
		if (!empty($check_prod_type)) {
			$result["check_product_type"] = 1;
		}

		$check_cust_acct = $this->_db->select()
			->from("M_CUSTOMER_ACCT")
			->where("ACCT_STATUS = ?", "1")
			->where("ACCT_NO = ?", $result["account_number"])
			->query()->fetch();

		$result["check_cust_acct"] = 0;
		if (!empty($check_cust_acct)) {
			$result["check_cust_acct"] = 1;
		}

		if ($result["response_code"] == "0000" || $result["response_code"] == "00") {
			echo json_encode($result);
		}
	}
}
