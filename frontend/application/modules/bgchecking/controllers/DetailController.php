<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class bgchecking_DetailController extends Application_Main
{
	public function initController()
	{
		$this->_helper->_layout->setLayout('newlayout');
	}

	public function indexAction()
	{
		$numb = $this->_getParam('bgnumb');

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				->where('A.BG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if ($_SESSION['pass_checking']) {
				$simpanPass = $_SESSION['pass_checking'];

				$AESMYSQL = new Crypt_AESMYSQL();
				$passDb     = urldecode($bgdata[0]['PASS_CHECKING']);
				$resultDecrypt = $AESMYSQL->decrypt($passDb, '');

				if ($simpanPass !== $resultDecrypt) {
					return $this->_redirect('/bgchecking');
				}
				unset($_SESSION['pass_checking']);
			} else {
				return $this->_redirect('/bgchecking');
			}

			// $passChecking = $bgdata;

			//$datas = $this->_request->getParams();
			// var_dump($datas);
			// var_dump($bgdata);
			if (empty($bgdata)) {
				$this->view->nobg = '1';
			}

			$dataselect = $bgdata['0'];
			$bgdatadetail = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $dataselect['BG_REG_NUMBER'])
				->query()->fetchAll();

			$selectcomp = $this->_db->select()
				->from(array('A' => 'M_CUSTOMER'), array('*'))
				->joinLeft(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('CITY_NAME'))
				->where('A.CUST_ID = ?', $dataselect['CUST_ID'])
				->query()->fetchAll();

			$this->view->compinfo = $selectcomp[0];

			$selectbranch = $this->_db->select()
				->from(array('A' => 'M_BRANCH'), array('*'))
				//  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
				->where('A.BRANCH_CODE = ?', $dataselect['BG_BRANCH'])
				->query()->fetchAll();

			$this->view->bankCabang = $selectbranch[0];



			if (!empty($bgdata)) {

				$data = $bgdata['0'];

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);


				$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

				$this->view->arrStatus = $arrStatus;

				$arrBankFormat = array(
					1 => 'Bank Standard',
					2 => 'Special Format (with bank approval)'
				);

				$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

				$arrLang = array(
					1 => 'Indonesian',
					2 => 'English',
					3 => 'Billingual',
				);

				$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

				$arrWaranty = array(
					1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
					2 => 'Bank Guarantee Line Facility',
					3 => 'Insurance'

				);

				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

				if (!empty($data['USAGE_PURPOSE'])) {
					$data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
					foreach ($data['USAGE_PURPOSE'] as $key => $val) {
						$str = 'checkp' . $val;
						//var_dump($str);
						$this->view->$str =  'checked';
					}
				}

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}
				$this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->ACCOUNT_NUMBER = $data['ACCT_NO'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];

				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->comment = $data['GUARANTEE_TRANSACTION'];

				$this->view->fileName = $data['FILE'];
				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];
				$this->view->status = $data['BG_STATUS'];

				$tempdir = APPLICATION_PATH . "/../public/QRImages/";

				$teks_qrcode    = 'http://' . $_SERVER['HTTP_HOST'] . "/bgchecking/detail/index/bgnumb/" . $bgdata['0']['BG_NUMBER'];
				$namafile       = $bgdata['0']['BG_NUMBER'] . ".png";
				$quality        = "H";
				$ukuran         = 5;
				$padding        = 1;
				$qrImage        = $tempdir . $namafile;

				QRCode::png($teks_qrcode, $tempdir . $namafile, $quality, $ukuran, $padding);

				$namafile       = $bgdata['0']['BG_NUMBER'] . ".png";
				$path  = APPLICATION_PATH . '/../public/QRImages/';
				$files = scandir($path);
				$files = array_diff(scandir($path), array('.', '..'));
				$dh = opendir($path);
				while ($file = readdir($dh)) {
					if ($file == $namafile) {
						$imageData = base64_encode(file_get_contents($path . '' . $namafile));
						$image = $path . '' . $data['IMAGE'];
						$src = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;
					}
				}

				$this->view->qrcode = $src;

				$path  = APPLICATION_PATH . '/../public/images/image001.jpg';
				$imageData = base64_encode(file_get_contents($path));
				$image = $path;
				//var_dump($path);
				$srcbank = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;

				$this->view->bankimage = $srcbank;

				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Date') {
								$this->view->paDate =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}

				$conf = Zend_Registry::get('config');
				$this->view->bankname = $conf['app']['bankname'];

				$this->view->amount = number_format($data['BG_AMOUNT'], 2, ',', '.');
				$terbilangs = $this->Terbilang($data['BG_AMOUNT']);
				$this->view->terbilang = $terbilangs;
				$this->view->datestart = $this->indodate($data['TIME_PERIOD_START']);
				$this->view->dateend = $this->indodate($data['TIME_PERIOD_END']);
				//echo '<pre>';
				//var_dump($data);

				$download = $this->_getParam('download');
				$download2 = $this->_getParam('pdf');
				$BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
				if ($BG_APPROVE_DOC) {
					$attahmentDestination = UPLOAD_PATH . '/document/bg/';
					//var_dump($attahmentDestination.$data['BG_APPROVE_DOC']);die;
					$this->_helper->download->file($data['BG_APPROVE_DOC'], $attahmentDestination . $data['BG_APPROVE_DOC']);
				}
				//var_dump($this->_request->getParams());die;
				if ($download2) {
					//echo '<pre>';
					//var_dump($bgdata);die;
					$tempdir = APPLICATION_PATH . "/../public/QRImages/";

					$teks_qrcode    = 'http://' . $_SERVER['HTTP_HOST'] . "/bgchecking/detail/index/bgnumb/" . $bgdata['0']['BG_NUMBER'];
					$namafile       = $bgdata['0']['BG_NUMBER'] . ".png";
					$quality        = "H";
					$ukuran         = 5;
					$padding        = 1;
					$qrImage        = $tempdir . $namafile;

					QRCode::png($teks_qrcode, $tempdir . $namafile, $quality, $ukuran, $padding);
					//var_dump($qrImage);die;
					if ($bgdata['0']['BG_LANGUAGE'] == '3') {
						$outputHTML = "<tr><td>" . $this->view->render('/detail/indexpdfbi.phtml') . "</td></tr>";
					} elseif ($bgdata['0']['BG_LANGUAGE'] == '2') {
						$outputHTML = "<tr><td>" . $this->view->render('/detail/indexpdfen.phtml') . "</td></tr>";
					} else {
						$outputHTML = "<tr><td>" . $this->view->render('/detail/indexpdf.phtml') . "</td></tr>";
					}
					//var_dump(APPLICATION_PATH);die;

					$path  = APPLICATION_PATH . '/../public/QRImages/';
					$files = scandir($path);
					$files = array_diff(scandir($path), array('.', '..'));
					$dh = opendir($path);
					while ($file = readdir($dh)) {
						if ($file == $namafile) {
							$imageData = base64_encode(file_get_contents($path . '' . $namafile));
							$image = $path . '' . $data['IMAGE'];
							$src = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;
						}
					}

					$path  = APPLICATION_PATH . '/../public/images/image001.jpg';
					$imageData = base64_encode(file_get_contents($path));
					$image = $path;
					$srcbank = 'data: ' . mime_content_type($image) . ';base64,' . $imageData;
					$terbilangs = $this->Terbilang($data['BG_AMOUNT']);
					$terbilang_en = $this->Terbilangen($data['BG_AMOUNT']);

					$arrPURPOSE = array(
						1 => 'Bidding Guarantee',
						2 => 'Advance Payment Guarantee',
						3 => 'Performance Bond',
						4 => 'Maintenance Guarantee',
						5 => 'Others',
						6 => 'Payment Guarantee',
					);

					$USAGE_PURPOSE = array();
					if (!empty($data['USAGE_PURPOSE'])) {
						foreach ($data['USAGE_PURPOSE'] as $key => $val) {
							$USAGE_PURPOSE[] = $arrPURPOSE[$val];
						}
					}


					//echo '<pre>';
					//var_dump($data);
					//var_dump($terbilangs);
					//die;
					//$path = 'myfolder/myimage.png';
					//$type = pathinfo($path, PATHINFO_EXTENSION);
					//$data = file_get_contents($path);
					//$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
					$translate = array(
						'[[qrcode]]' 			=> $src,
						'[[bankimage]]'			=> $srcbank,
						'[[bg_numb]]'			=> $data['BG_NUMBER'],
						'[[bg_issued]]'         => $this->indodate($data['BG_ISSUED']),
						'[[date_start]]'		=> $data['TIME_PERIOD_START'],
						'[[city]]'		=> $data['RECIPIENT_CITY'],
						'[[benef_name]]'		=> $data['RECIPIENT_NAME'],
						'[[benef_add]]'		=> $data['RECIPIENT_ADDRES'],
						'[[benef_city]]'		=> $data['RECIPIENT_CITY'],
						'[[app_name]]'		=> $selectcomp[0]['CUST_NAME'],
						'[[app_add]]'		=> $selectcomp[0]['CUST_ADDRESS'],
						'[[app_city]]'		=> $selectcomp[0]['CITY_NAME'],
						'[[amount]]'		=> number_format($data['BG_AMOUNT'], 2, ',', '.'),
						'[[terbilang]]'		=> $terbilangs,
						'[[terbilang_en]]'		=> $terbilang_en,
						'[[guarantee_letter]]'		=> $data['GUARANTEE_TRANSACTION'],
						'[[date_start]]'		=> $this->indodate($data['TIME_PERIOD_START']),
						'[[date_end]]'		=> $this->indodate($data['TIME_PERIOD_END']),
						'[[usage_purpose]]'		=> implode(", ", $USAGE_PURPOSE),

					);
					//echo "<pre>";
					//var_dump($translate);

					$outputHTMLnew = strtr($outputHTML, $translate);

					//echo $outputHTMLnew;die;
					$setting = new Settings();
					$master_bank_app_name = $setting->getSetting('master_bank_app_name');
					$master_bank_name = $setting->getSetting('master_bank_name');
					try {

						$this->_helper->download->newPdfblank(null, null, null, $master_bank_app_name . ' - ' . $master_bank_name, $outputHTMLnew);
						die('a');
						echo $outputHTMLnew;
					} catch (Exception $e) {
						var_dump($e);
						die;
					}
					//die;
				}
				//print_r($edit);die;
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}
			} else {
				$this->view->nobg = '1';
			}
		} else {
			$this->view->nobg = '1';
		}
	}

	public function Terbilang($nilai)
	{
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		if ($nilai == 0) {
			return "";
		} elseif ($nilai < 12 & $nilai != 0) {
			return "" . $huruf[$nilai];
		} elseif ($nilai < 20) {
			return $this->Terbilang($nilai - 10) . " Belas ";
		} elseif ($nilai < 100) {
			return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
		} elseif ($nilai < 200) {
			return " Seratus " . $this->Terbilang($nilai - 100);
		} elseif ($nilai < 1000) {
			return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
		} elseif ($nilai < 2000) {
			return " Seribu " . $this->Terbilang($nilai - 1000);
		} elseif ($nilai < 1000000) {
			return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
		} elseif ($nilai < 1000000000) {
			return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
		} elseif ($nilai < 1000000000000) {
			return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
		} elseif ($nilai < 100000000000000) {
			return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
		} elseif ($nilai <= 100000000000000) {
			return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
		}
	}

	public function Terbilangen($nilai)
	{
		$huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
		if ($nilai == 0) {
			return "";
		} elseif ($nilai < 20 & $nilai != 0) {
			return "" . $huruf[$nilai];
		} elseif ($nilai < 100) {
			return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
		} elseif ($nilai < 200) {
			return " Seratus " . $this->Terbilangen($nilai - 100);
		} elseif ($nilai < 1000) {
			return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
		} elseif ($nilai < 2000) {
			return " Seribu " . $this->Terbilangen($nilai - 1000);
		} elseif ($nilai < 1000000) {
			return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
		} elseif ($nilai < 1000000000) {
			return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
		} elseif ($nilai < 1000000000000) {
			return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
		} elseif ($nilai < 100000000000000) {
			return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
		} elseif ($nilai <= 100000000000000) {
			return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
		}
	}

	public function indodate($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}
}
