<?php
class manageproductaccount_Model_Manageproductaccount
{

	protected $_db;

	// constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getData($fParam, $sortBy = null, $sortDir = null, $filter = null)
	{
		$select = $this->_db->select()
			->from(array('A' => 'M_PRODUCT'), array('PRODUCT_ID', 'PRODUCT_CODE', 'PRODUCT_PLAN', 'PRODUCT_NAME', 'ACCT_TYPE', 'LAST_SUGGESTED', 'LAST_SUGGESTEDBY', 'LAST_APPROVED', 'LAST_APPROVEDBY'));

		if (isset($fParam['fproduct_id'])) if (!empty($fParam['fproduct_id'])) $select->where('UPPER(PRODUCT_ID) LIKE ' . $this->_db->quote('%' . strtoupper($fParam['fproduct_id']) . '%'));

		if (isset($fParam['ftipeakun'])) if (!empty($fParam['ftipeakun'])) $select->where('UPPER(PRODUCT_CODE) LIKE ' . $this->_db->quote('%' . strtoupper($fParam['ftipeakun']) . '%'));

		if (isset($fParam['ftipeproduk'])) if (!empty($fParam['ftipeproduk'])) $select->where('UPPER(PRODUCT_PLAN) LIKE ' . $this->_db->quote('%' . strtoupper($fParam['ftipeproduk']) . '%'));

		if (isset($fParam['fdeskripsiproduk'])) if (!empty($fParam['fdeskripsiproduk'])) $select->where('UPPER(PRODUCT_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fParam['fdeskripsiproduk']) . '%'));

		if (isset($fParam['fsuggestedby'])) if (!empty($fParam['fsuggestedby'])) $select->where('UPPER(LAST_SUGGESTEDBY) = ' . $this->_db->quote(strtoupper($fParam['fsuggestedby'])));

		if (isset($fParam['fapprovedby'])) if (!empty($fParam['fapprovedby'])) $select->where('UPPER(LAST_APPROVEDBY) = ' . $this->_db->quote(strtoupper($fParam['fapprovedby'])));

		if ($fParam['fsuggested']) {

			$explodeDate = explode('/', $fParam['fsuggested']);
			$dateSuggest = date('Y-m-d', strtotime($explodeDate[2] . '-' . $explodeDate[1] . '-' . $explodeDate[0]));
			$select->where("LAST_SUGGESTED >= ?", $dateSuggest);
		}

		if ($fParam['fsuggested_end']) {
			$explodeDate = explode('/', $fParam['fsuggested_end']);
			$dateSuggest = date('Y-m-d', strtotime($explodeDate[2] . '-' . $explodeDate[1] . '-' . $explodeDate[0]));
			$select->where("LAST_SUGGESTED <= ?", $dateSuggest);
		}

		if ($fParam['fapproved']) {
			$explodeDate = explode('/', $fParam['fapproved']);
			$dateSuggest = date('Y-m-d', strtotime($explodeDate[2] . '-' . $explodeDate[1] . '-' . $explodeDate[0]));
			$select->where("LAST_APPROVED >= ?", $dateSuggest);
		}

		if ($fParam['fapproved_end']) {
			$explodeDate = explode('/', $fParam['fapproved_end']);
			$dateSuggest = date('Y-m-d', strtotime($explodeDate[2] . '-' . $explodeDate[1] . '-' . $explodeDate[0]));
			$select->where("LAST_APPROVED <= ?", $dateSuggest);
		}



		if (!empty($sortBy) && !empty($sortDir))
			$select->order($sortBy . ' ' . $sortDir);
		//rint_r($fParam);
		//echo $select;//DIE;
		// print_r($fParam);die;
		return $this->_db->fetchall($select);
	}

	public function getDetail($BANK_ID)
	{
		$data = $this->_db->fetchRow(
			$this->_db->select()
				->from(array('M_PRODUCT'))
				->where("PRODUCT_ID=?", $BANK_ID)
		);

		return $data;
	}

	public function insertData($content)
	{
		$this->_db->insert("M_PRODUCT", $content);
	}

	public function insertTempData($content)
	{
		$this->_db->insert("TEMP_PRODUCT", $content);
	}

	public function updateData($PRODUCT_ID, $content)
	{
		$whereArr  = array('PRODUCT_ID = ?' => $PRODUCT_ID);
		$this->_db->update('M_PRODUCT', $content, $whereArr);
	}

	public function getTempProductId($PRODUCT_ID)
	{

		$data = $this->_db->fetchRow(
			$this->_db->select()
				->from(array('TEMP_PRODUCT'))
				->where("PRODUCT_ID=?", $PRODUCT_ID)
		);

		return $data;
	}

	public function deleteData($PRODUCT_ID)
	{
		$this->_db->delete('M_PRODUCT', 'PRODUCT_ID = ' . $this->_db->quote($PRODUCT_ID));
		//		$this->_db->delete('M_PRODUCT','PRODUCT_ID = ?', $PRODUCT_ID);
	}
}
