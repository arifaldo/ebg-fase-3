<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';
	require_once 'CMD/SinglePayment.php';
	require_once 'CMD/Validate/ValidatePaymentSingle.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	
	$truecounter = 0;
	$falsecounter = 0;
	$cronResult = "";

	// INNER JOIN KE T_TRANSACTION
	// PS PERIODIC_STATUS  2
	// PERIODIC_NEXTDATE (today)
	
	$data = $db->select	()
				->FROM	(array('P' => 'T_PERIODIC'),array('*'))
				->JOIN	(array('D' => 'T_PERIODIC_DETAIL'),'P.PS_PERIODIC = D.PS_PERIODIC',array('*'))
				->JOIN	(array('PS' => 'T_PSLIP'),'P.PS_PERIODIC = PS.PS_PERIODIC AND P.PS_PERIODIC_NEXTDATE = PS.PS_EFDATE',array('*')) 
				->JOIN	(array('T' => 'T_TRANSACTION'),'PS.PS_NUMBER = T.PS_NUMBER',array('*'))
				->WHERE('P.PS_PERIODIC_STATUS = 2')		
				->WHERE('PS.PS_STATUS IN (7,5)')		
				->WHERE('PS.PS_TYPE NOT IN (20,19,23)')		
				->WHERE('DATE(P.PS_PERIODIC_NEXTDATE) = '."'".date('Y-m-d')."'".'')
				->WHERE('DATE(P.PS_PERIODIC_ENDDATE) >= '."'".date('Y-m-d')."'".'')
				// ->WHERE('HOUR(PS.PS_EFTIME) = HOUR(NOW()) ')

								
				->query()->fetchAll()
				;
				 //echo $data;die;
 	//print_r($data);die;
// 	echo $data->__toString();die;
	
	//Zend_Debug::dump($data, 'Test'); die;	
		 
	if($data)
	{
		//Zend_Debug::dump($data); die;
		foreach($data as $row)
		{
			$PS_EVERY_PERIODIC_UOM 					= $row['PS_EVERY_PERIODIC_UOM'];
			$PERIODIC_EVERY							= $row['PS_EVERY_PERIODIC'];
			$END_DATE								= $row['PS_PERIODIC_ENDDATE'];
			$PS_TYPE								= $row['PS_TYPE'];
			$PS_NUMBER								= $row['PS_NUMBER'];
			
			$dataday = $db->select	()
				->FROM	(array('P' => 'T_PERIODIC_DAY'),array('P.*'))
				->JOIN	(array('PS' => 'T_PSLIP'),'P.PERIODIC_ID = PS.PS_PERIODIC',array()) 
				->WHERE('PS.PS_NUMBER = ?',$PS_NUMBER)
				->group('P.DAY_ID')				
				->query()->fetchAll()
				;
				// var_dump($PS_EVERY_PERIODIC_UOM);die;
			// NextDate														
			if ($PS_EVERY_PERIODIC_UOM ==1){ //every day

				$date = new DateTime();
				$arrday = array(
							'0' => 'sunday',
							'1' => 'monday',
							'2' => 'tuesday',
							'3' => 'wednesday',
							'4' => 'thursday',
							'5' => 'friday',
							'6' => 'saturday'

						);
				// Modify the date it contains
				$datenumb = date('w');
				if($datenumb==6){
					$datename = 0;
				}else{
					$datename = $datenumb+1;
				}
				// var_dump($datename);
				if(!empty($dataday)){
					$resutnext = 0;
					// echo 'here';
					foreach ($dataday as $key => $value) {
						if($datename == $value['DAY_ID']){
							$string = 'next '.$arrday[$datename];
							// var_dump($string);
							$date->modify($string);

							$NEXT_DATE = $date->format('Y-m-d');
							
							$resutnext = 1;
						}
					}
					// var_dump($NEXT_DATE);echo 'ge';
					if(!$resutnext){
						// ;

						$string = 'next '.$arrday[$dataday['0']['DAY_ID']];
						var_dump($string);
						$date->modify($string);
						$NEXT_DATE = $date->format('Y-m-d');
					}

				}else{
					$NEXT_DATE =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				}				
				
				// $nextDateVal = strtotime($nextDate);
				
				
				// var_dump($NEXT_DATE);
				//echo $NEXT_DATE; die;
				
			}else if($PS_EVERY_PERIODIC_UOM ==2){

				$nextDate =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				$nextDateVal = strtotime($nextDate);
				$NEXT_DATE = date("Y-m-d",strtotime("+7 day", $nextDateVal));

			}elseif ($PS_EVERY_PERIODIC_UOM ==3){ //every date
			
				//$row['PS_PERIODIC_NEXTDATE'] = '2020-10-28';
				$nextDate =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				$nextDateVal = strtotime($nextDate);
				$dateNextMonth = date("Y-m-d",strtotime("+1 month", $nextDateVal));
				$nextMontheVal = strtotime($dateNextMonth);		
				
				$maxDays=date('t', $nextMontheVal); // hari terakhir bulan berikutnya
				//var_dump($dateNextMonth);die;
				//$every = date('j', $nextDateVal);	
				$every = $row['PS_EVERY_PERIODIC'];
					//var_dump($maxDays);
					//var_dump($every);die;
				if($every == 'last'){
					$NEXT_DATE = date("Y-m-t", strtotime($dateNextMonth));
				}else{
					if ($maxDays >=  $every){
						$every = $every;
						$NEXT_DATE = $dateNextMonth;
					}else{
						$every = $maxDays;
						$nextDate = mktime(0,0,0,date("n", strtotime($dateNextMonth)),$every,date("Y", strtotime($dateNextMonth)));
					
						$NEXT_DATE = date("Y-m-d",$nextDate);
					}
				}
				//var_dump($every);die;
				
				//var_dump($NEXT_DATE);die;
				
			}
			
			$param = array();			
			$param['PS_SUBJECT'] 					= $row['PS_SUBJECT'];
			$param['PS_CCY'] 						= $row['PS_CCY'];
			$param['PS_EFDATE'] 					= $NEXT_DATE;//Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['EXPIRY_DATE'] 					= "";//Application_Helper_General::convertDate($EXPIRY_DATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['TRA_AMOUNT'] 					= $row['TRA_AMOUNT'];
			$param['TRA_MESSAGE'] 					= $row['TRA_MESSAGE'];
			$param['TRA_REFNO'] 					= $row['TRA_REFNO'];
			$param['SOURCE_ACCOUNT'] 				= $row['SOURCE_ACCOUNT'];
			$param['BENEFICIARY_ACCOUNT'] 			= $row['BENEFICIARY_ACCOUNT'];
			$param['BENEFICIARY_ACCOUNT_CCY'] 		= $row['BENEFICIARY_ACCOUNT_CCY'];
			$param['BENEFICIARY_ACCOUNT_NAME'] 		= $row['BENEFICIARY_ACCOUNT_NAME'];
			$param['BENEFICIARY_BANK_NAME'] 		= $row['BENEFICIARY_BANK_NAME'];		// for domestic
			$param['BENEFICIARY_ALIAS_NAME'] 		= $row['BENEFICIARY_ALIAS_NAME'];
			$param['BENEFICIARY_EMAIL'] 			= $row['BENEFICIARY_EMAIL'];
			$param["BENEFICIARY_ADDRESS"] 			= $row['BENEFICIARY_ADDRESS'];			// for domestic
			$param["BENEFICIARY_CITIZENSHIP"] 		= $row['BENEFICIARY_CITIZENSHIP'];		// for domestic
			$param["USER_ID"] 						= $row['USER_ID'];						// for domestic
			$param["SOURCE_ACCOUNT_CCY"] 			= $row['SOURCE_ACCOUNT_CCY'];			// for domestic
			$param["SOURCE_ACCOUNT_NAME"] 			= $row['SOURCE_ACCOUNT_NAME'];			// for domestic
			$param["BANK_CODE"] 					= $row['CLR_CODE'];						// for domestic
			$param["CLR_CODE"]						= $row['CLR_CODE'];						// for domestic
			if ($row['TRANSFER_TYPE'] == 1){												// for domestic
				$param["TRANSFER_TYPE"] = "RTGS";
			}elseif ($row['TRANSFER_TYPE'] == 2){
				$param["TRANSFER_TYPE"] = "SKN";
			}elseif ($row['TRANSFER_TYPE'] == 8){
				$param["TRANSFER_TYPE"] = "ONLINE";				
			}			
			$param['PS_TYPE'] 						= $PS_TYPE;
			//$param["TRANSFER_TYPE"] 				= $row['TRANSFER_TYPE'];				// for domestic
			$param["LLD_CATEGORY"] 					= (isset($row['BENEFICIARY_CATEGORY']))		? $row['BENEFICIARY_CATEGORY']		: "";					// for domestic
			$param["LLD_BENEIDENTIF"] 				= (isset($row['BENEFICIARY_ID_TYPE']))		? $row['BENEFICIARY_ID_TYPE']		: "";					// for domestic
			$param["LLD_BENENUMBER"] 				= (isset($row['BENEFICIARY_ID_NUMBER']))		? $row['BENEFICIARY_ID_NUMBER']		: "";					// for domestic
			$param["CITY_CODE"] 					= (isset($row['BENEFICIARY_CITY_CODE']))		? $row['BENEFICIARY_CITY_CODE']		: "";					// for domestic


			$param["LLD_IDENTICAL"] 				= (isset($row['LLD_IDENTICAL']))	? $row['LLD_IDENTICAL']		: "";					// for domestic
			$param["LLD_RELATIONSHIP"] 				= (isset($row['LLD_RELATIONSHIP']))	? $row['LLD_RELATIONSHIP']	: "";					// for domestic
			$param["LLD_PURPOSE"] 					= (isset($row['LLD_PURPOSE']))		? $row['LLD_PURPOSE']		: "";					// for domestic
			$param["LLD_DESCRIPTION"] 				= (isset($row['LLD_DESCRIPTION']))	? $row['LLD_DESCRIPTION']	: "";					// for domestic
			$param["LLD_CODE"] 						= (isset($row['LLD_CODE']))			? $row['LLD_CODE']			: "";					// for domestic
			$param["LLD_DESC"] 						= (isset($row['LLD_DESC']))			? $row['LLD_DESC']			: "";					// for domestic
			
			$param["TRANSFER_FEE"] 					= $row['TRANSFER_FEE'];					// for domestic
			$param['_addBeneficiary'] 				= true; //$this->view->hasPrivilege('BADA');
			$param['_beneLinkage'] 					= true; //$this->view->hasPrivilege('BLBU');
			$param['_priviCreate'] 					= 'CRSP';
			$param['TrfDateType']					= 3;
			$param['PS_PERIODIC']					= $row['PS_PERIODIC'];;
			$PS_NUMBER								= "";
			$param['CROND']							= 1;
			$param['TRANSFER_TYPE']					= $row['TRANSFER_TYPE'];
			$psPeriodicID							= $row['PS_PERIODIC'];		
			$param['PS_STATUS']						= 7;
			//$db->beginTransaction();
// 			echo '<pre>';
// 			print_r($param);die;
			//var_dump($END_DATE);
			//var_dump($NEXT_DATE);die;
			if (strtotime($END_DATE) >= strtotime($NEXT_DATE)){
				// var_dump($PS_TYPE);die;

				$param['PHISTORY_STATUS'] = 22; //p_slip_history status recreate
				$param['TRX_ID'] = $row['TRANSACTION_ID']; //p_slip_history status recreate

				// create T_PSLIP
				$SinglePayment = new SinglePayment($PS_NUMBER, $row['CUST_ID'], $row['USER_ID']);
				if ($PS_TYPE == 1){ // within					
					$result = $SinglePayment->createPaymentWithin($param);
				}elseif ($PS_TYPE == 2){ //domesitic
					$result = $SinglePayment->createPaymentDomestic($param);
				}elseif($PS_TYPE == 21){

									$param['PS_PERIODIC'] = $psPeriodicID == null ? null : $psPeriodicID;
									//var_dump($param['TRANSFER_TYPE']);die;
									if ($param['TRANSFER_TYPE'] == '0') {
										 //echo '<pre>';
										 //print_r($PS_NUMBER);die();
										try {
											$sendProvider	 = new SinglePayment(null, $row['CUST_ID'], 'System');
																
											$resProvider	 = $sendProvider->createPaymentOnline($param, $msg);
											$success = true;
										} catch (Exception $e) {
											$success = false;
											var_dump($e);die;
										}
									}
									else if($param['TRANSFER_TYPE'] == '1' || $param['TRANSFER_TYPE'] == '2'){
										 
										try {
											$sendProvider	 = new SinglePayment(null, $row['CUST_ID'], 'System');
											
																
											$resProvider	 = $sendProvider->createPaymentSknRtgs($param, $msg);
											$success = true;
										} catch (Exception $e) {
											$success = false;
											var_dump($e);die;
										}
									}
				//	$result = $SinglePayment->createPaymentDomestic($param);
				}
				
							
				// update NEXTDATE PERIOIDC				
				$data = array ('PS_PERIODIC_NEXTDATE' => $NEXT_DATE, 'PS_PERIODIC_CURRENTDATE' => date('Y-m-d'));
				$where['PS_PERIODIC = ?'] = $row['PS_PERIODIC'];
				$db->update('T_PERIODIC',$data,$where);
								
			}else{
				
				// update STATUS PERIODIC to be COMPLETED
				$data = array ('PS_PERIODIC_STATUS' => 1, 'PS_PERIODIC_CURRENTDATE' => date('Y-m-d'));
				$where['PS_PERIODIC = ?'] = $row['PS_PERIODIC'];
				$db->update('T_PERIODIC',$data,$where);
			}
			
			//$db->commit();
						
			
			/*$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], $row['USER_ID']);
			$resultRelease = $Payment->releasePayment();
					
			if ($resultRelease['status'] == '00')
			{
				$truecounter++;
			}
			else
			{
				$falsecounter++;
			}*/
		}
		
		/*if($truecounter>0)
		{
			$cronResult .= " ".$truecounter." payment(s) success";
		}
		if($falsecounter>0)
		{
			$cronResult .= " ".$falsecounter." payment(s) failed";
		}*/		
	}
	else
	{
		$cronResult .= "No data released";
	}

	$filename = basename(__FILE__);
	Application_Helper_General::cronLog($filename, $cronResult, 1);
?>