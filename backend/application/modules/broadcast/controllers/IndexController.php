<?php
require_once 'Zend/Controller/Action.php';
class broadcast_IndexController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$arr = null;
		$viewFilter = null;
		$errors = null;
		
		$fields = array	(
							'HelpTopic'  			=> array	(
																	'field' => 'B_SUBJECT',
																	'label' => $this->language->_('Subject'),
																	'sortable' => true
																),
							'TypeTopic'  			=> array	(
																	'field' => 'B_TYPE',
																	'label' => $this->language->_('Type'),
																	'sortable' => true
																),
							'CreatedBy'  			=> array	(
																	'field' => 'B_CREATEDBY',
																	'label' => $this->language->_('Updated Date'),
																	'sortable' => true
																),
							
							// 'FileDescription'  					=> array	(
							// 											'field' => 'IMAGE',
							// 											'label' => $this->language->_('Image'),
							// 											'sortable' => true
							// 										),
							// 'FileName'  			=> array	(
							// 										'field' => 'HELP_FILENAME',
							// 										'label' => $this->language->_('File Name'),
							// 										'sortable' => true
							// 									),
							// 'Uploaded By'  					=> array	(
							// 											'field' => 'CREATEDBY',
							// 											'label' => $this->language->_('Create By'),
							// 											'sortable' => true
							// 										),	
							/*'Uploaded Date'  					=> array	(
																		'field' => 'CREATED',
																		'label' => $this->language->_('Created Date'),
																		'sortable' => true
																	),	*/
							
							
							'sendDate'  					=> array	(
																		'field' => 'B_EFDATE',
																		'label' => $this->language->_('Send Date'),
																		'sortable' => true
																	),	
							'status'  					=> array	(
																		'field' => 'B_STATUS',
																		'label' => $this->language->_('Status'),
																		'sortable' => true
																	),
						);
		$this->view->fields = $fields;


		$filterlist = array('B_SUBJECT','B_CREATED','B_CREATEDBY');
		
		$this->view->filterlist = $filterlist;
	  
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'B_SUBJECT'   => array('StringTrim','StripTags','StringToUpper'),
							'B_CREATED'   	=> array('StringTrim','StripTags','StringToUpper'),
							'B_CREATEDBY' 	=> array('StringTrim','StripTags')
							
		);

		 $dataParam = array('B_SUBJECT','B_CREATEDBY');
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}

		if(!empty($this->_request->getParam('uploaddate'))){
				$createarr = $this->_request->getParam('uploaddate');
					$dataParamValue['B_CREATED'] = $createarr[0];
					$dataParamValue['B_CREATED_END'] = $createarr[1];
			}
		
		// print_r($dataParamValue);die;
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		// $zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		// $filter 	= $zf_filter->getEscaped('filter');
		$delete 	= $this->_getParam('delete');
		
		if($delete)
		{
			//echo("<br>ok<br>");
			$postreq_id	= $this->_request->getParam('req_id');
			if($postreq_id)
			{
				foreach ($postreq_id as $key => $value) 
				{
					if($postreq_id[$key]==0)
					{
						unset($postreq_id[$key]);
					}
					
				}
			}
			
			if($postreq_id == null)
			{
				$params['req_id'] = null;
			}
			else
			{
				$params['req_id'] = 1;
			}
			
			$validators = array	(
										'req_id' => 	array	(	
																	'NotEmpty',
																	'messages' => array	(
																							'Error File ID Submitted',
																						)
																),
									);
			$filtersVal = array	( 	
									'req_id' => array('StringTrim','StripTags')
								);
			$zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
			
			if($zf_filter_input->isValid())
			{			
				try
				{
					foreach ($postreq_id as  $key =>$value) 
					{		
						$HELP_ID_DELETE =  $postreq_id[$key];
				
						$this->_db->beginTransaction();
						$param = array('B_STATUS'=> '3',);	
						
						$where = array('ID = ?' => $HELP_ID_DELETE);
						$query = $this->_db->update ( "M_BROADCAST", $param, $where );
						$this->_db->commit();
					}
					$id = implode(",", $postreq_id);
					Application_Helper_General::writeLog('HLUD','Delete Broadcast. Broadcast ID: ['.$id.']');
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
			else
			{
				$error 			= true;
				$errors 		= $zf_filter_input->getMessages();
				$req_idErr 		= (isset($errors['req_id']))? $errors['req_id'] : null;
				
				$this->_redirect("/broadcast/index?error=true&req_idErr=$req_idErr&filter=Filter");
			}
			
			//$filter = 'Filter';
			
		}
		
		if($this->_getParam('error'))
		{
			$this->view->error 			= $this->_getParam('error');
			
			$this->view->req_idErr 		= $this->_getParam('req_idErr');
			
		}
		
		//if($filter)
		//{
			$select = $this->_db->select()
								->from(	'M_BROADCAST',
										array('*'));
								// ->where("HELP_ISDELETED != 1");
								
		//}
		
		
		$HELP_ID   	= $this->_getParam('ID');
		
		if($HELP_ID)
		{
			$select->where('ID =?',$HELP_ID);
			$data = $this->_db->fetchRow($select);
			$attahmentDestination = UPLOAD_PATH . '/document/help/';
			$this->_helper->download->file($data['HELP_FILENAME'],$attahmentDestination.$data['HELP_SYS_FILENAME']);
			Application_Helper_General::writeLog('HLDL','Download Help.  Help ID: ['.$HELP_ID.'] , Help Topic: ['.$data['HELP_TOPIC'].']');
		}
		else
		{
			Application_Helper_General::writeLog('HLLS','View help list');
		}
		
		if($filter == TRUE)
		{
			// print_r($dataParamValue);die;
			$SEARCH_TEXT   	= $dataParamValue['SEARCH_TEXT'];
			$HELP_TOPIC   	= $dataParamValue['B_SUBJECT'];
			$UPLOADED_BY    = $dataParamValue['B_CREATEDBY'];
			$DATE_START    	= $dataParamValue['B_CREATED'];					
			$DATE_END		= $dataParamValue['B_CREATED_END'];					
			

			// print_r($DATE_START);die;
			$DATESTART   			= (Zend_Date::isDate($DATE_START,$this->_dateDisplayFormat))?
									   new Zend_Date($DATE_START,$this->_dateDisplayFormat):
									   false;
			
			$DATEEND    			= (Zend_Date::isDate($DATE_END,$this->_dateDisplayFormat))?
									   new Zend_Date($DATE_END,$this->_dateDisplayFormat):
									   false;
									   
			$description	= $this->_getParam('description');
			
			if(!empty($DATE_START))
			{
				$select->where("DATE (B_CREATED) >= DATE(".$this->_db->quote($DATESTART->toString($this->_dateDBFormat)).")");
			}
			
			if(!empty($DATE_END))
			{
				$select->where("DATE (B_CREATED) <= DATE(".$this->_db->quote($DATEEND->toString($this->_dateDBFormat)).")");
			}
			
			if($description)
			{
				$select->where("UPPER(B_CONTENT) LIKE ".$this->_db->quote('%'.$description.'%'));
			}
			
			
			
			if($HELP_TOPIC)
			{
				$select->where("UPPER(B_SUBJECT) LIKE ".$this->_db->quote('%'.$HELP_TOPIC.'%'));
			}
			
			if($UPLOADED_BY)
			{
				$select->where("UPPER(B_CREATED) LIKE ".$this->_db->quote('%'.$UPLOADED_BY.'%'));
			}
			// echo $select;die;
			$this->view->DATE_END 		= $DATE_END;
			$this->view->DATE_START 	= $DATE_START;
			$this->view->description 	= $description;
			$this->view->SEARCH_TEXT 	= $SEARCH_TEXT;
			$this->view->HELP_TOPIC 	= $HELP_TOPIC;
			$this->view->UPLOADED_BY 	= $UPLOADED_BY;
		}

		$select->order($sortBy.' '.$sortDir);
		$arr = $this->_db->fetchAll($select);
		
		$csv = $this->_getParam('csv');

		if($this->_request->getParam('print') == 1 || $csv){

			foreach($arr as $key=>$row)
			{
				$arr[$key]['B_CREATEDBY'] = $row['B_CREATEDBY'].' - '.Application_Helper_General::convertDate($row['B_CREATED'],"dd MMM yyyy HH:mm:ss ");
				//$arr[$key]['BENEFICIARY_ACCOUNT'] = $row['BENEFICIARY_ACCOUNT'].' - '.$row['BENEFICIARY_NAME'];
				if($row['B_TYPE'] == '1'){
                    $arr[$key]['B_TYPE']="SMS";    
                }else{
                    $arr[$key]['B_TYPE']="Email";
                }

                if($row['B_STATUS'] == '1'){
                    $arr[$key]['B_STATUS']="Sent";    
                }else if ($row['B_STATUS'] == '2') {
                    $arr[$key]['B_STATUS']="Pending Future Date";
                }else{
                    $arr[$key]['B_STATUS']="Deleted";
                }
				
			}

			// echo "<pre>";
			// var_dump($arr);
			// die();


			
				
				if($csv){
					$newarr = array();
				//echo '<pre>';
				//var_dump($arr);die;
				foreach($arr as $keys => $val){
					$newarr[$keys]['B_SUBJECT'] = $val['B_SUBJECT'];
					
					$newarr[$keys]['B_TYPE'] = $val['B_TYPE'];
					$newarr[$keys]['NEWS'] = $val['B_SUBJECT'];
					$newarr[$keys]['B_CREATEDBY'] = $val['B_CREATEDBY'];
					$newarr[$keys]['B_EFDATE'] = $val['B_EFDATE'];
					$newarr[$keys]['B_STATUS'] = $val['B_STATUS'];
					//$newarr[$keys]['ACTIVE'] = $status;
					
					
					
				}
					Application_Helper_General::writeLog('SYLG','Download CSV Broadcast');				
					$this->_helper->download->csv(array('Subject','Type','Update Date','Send Date','Status'),$newarr,null,'Broadcast');
				}else{
				
						$this->_forward('print', 'index', 'widget', array('data_content' => $newarr, 'data_caption' => 'Broadcast', 'data_header' => $fields));
				}

			
		}

		$this->view->arr = $arr;
		$this->paging($arr);
		unset($dataParamValue['B_CREATED_END']);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			// print_r($wherecol);die;
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}