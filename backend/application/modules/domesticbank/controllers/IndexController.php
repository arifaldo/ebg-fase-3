<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'Service/Account.php';

class Domesticbank_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$flashMessanger = $this->_helper->getHelper('FlashMessenger');

		if(count($flashMessanger->getMessages()) > 0){
			$errorSinkron = array_shift($flashMessanger->getMessages())['error'];
			$this->view->errorSinkron = $errorSinkron;
		}

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$fields = array(
			'bank_name'      => array(
				'field' => 'BANK_NAME',
				'label' => $this->language->_('Nama Bank'),
				'sortable' => true
			),
			'bank_code'      => array(
				'field' => 'BANK_CODE',
				'label' => $this->language->_('Kode Bank'),
				'sortable' => true
			),
			'clearing_code'  => array(
				'field' => 'CLR_CODE',
				'label' => $this->language->_('Kode Kliring'),
				'sortable' => true
			),
			'swift_code'     => array(
				'field' => 'SWIFT_CODE',
				'label' => $this->language->_('Kode RTGS'),
				'sortable' => true
			),
		);


		$filterlist = array("BANK_NAME" => "Nama Bank", "BANK_CODE" => "Kode Bank", "CLR_CODE" => "Kode Kliring", "SWIFT_CODE" => "Kode RTGS");

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'BANK_NAME');
		$sortDir = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(
			'filter' 	  	 => array('StringTrim', 'StripTags'),
			'BANK_NAME'      => array('StringTrim', 'StripTags'),
			'BANK_CODE'      => array('StringTrim', 'StripTags'),
			'CLR_CODE'  => array('StringTrim', 'StripTags'),
			'SWIFT_CODE'      => array('StringTrim', 'StripTags')
			// 'swift_code'     => array('StringTrim','StripTags')
		);

		$dataParam = array("BANK_NAME", "BANK_CODE", "CLR_CODE", "SWIFT_CODE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$getData = Application_Helper_Array::SimpleArray($fields, 'field');

		$select = $this->_db->select()
			->from(array('A' => 'M_DOMESTIC_BANK_TABLE'), array_merge($getData, array('BANK_ID')))
			->where('BANK_ISMASTER <> 1');

		if ($filter == TRUE || $csv || $pdf || $this->_request->getParam('print')) {
			$header = Application_Helper_Array::simpleArray($fields, 'label');

			$fclearing_code = $zf_filter->getEscaped('CLR_CODE');
			$fbank_name     = $zf_filter->getEscaped('BANK_NAME');
			$fbank_code     = $zf_filter->getEscaped('BANK_CODE');
			$fswift_code    = $zf_filter->getEscaped('SWIFT_CODE');
			$fcity_code     = $zf_filter->getEscaped('CITY');

			if ($fclearing_code || $fclearing_code === '0') $select->where('UPPER(CLR_CODE) LIKE ' . $this->_db->quote('%' . strtoupper($fclearing_code) . '%'));
			if ($fbank_name || $fbank_name === '0')     $select->where('BANK_NAME LIKE ' . $this->_db->quote('%' . strtoupper($fbank_name) . '%'));
			if ($fbank_code || $fbank_code === '0')     $select->where('BANK_CODE LIKE ' . $this->_db->quote('%' . strtoupper($fbank_code) . '%'));
			if ($fswift_code || $fswift_code === '0')    $select->where('UPPER(SWIFT_CODE) LIKE ' . $this->_db->quote('%' . strtoupper($fswift_code) . '%'));
			if ($fcity_code || $fcity_code === '0')     $select->where('UPPER(CITY_CODE) LIKE ' . $this->_db->quote('%' . strtoupper($fcity_code) . '%'));

			$this->view->clearing_code = $fclearing_code;
			$this->view->bank_name     = $fbank_name;
			$this->view->bank_code     = $fbank_code;
			$this->view->swift_code    = $fswift_code;
			$this->view->CITY     = $fcity_code;
		}

		//$this->view->success = true;
		$select->order($sortBy . ' ' . $sortDir);

		$select = $this->_db->fetchall($select);

		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		foreach ($selectPdfCsv as $key => $row) {
			$selectPdfCsv[$key]['BANK_CODE'] = "'" . $row['BANK_CODE'];
			$selectPdfCsv[$key]['CLR_CODE'] = "'" . $row['CLR_CODE'];
			unset($selectPdfCsv[$key]['BANK_ID']);
		}


		//--------konfigurasicsv dan pdf---------
		if ($csv) {
			$this->_helper->download->csv($header, $selectPdfCsv, null, 'domestic_bank_list');
			Application_Helper_General::writeLog('DBLS', 'Download CSV Bank Lain');
		} else if ($pdf) {
			$this->_helper->download->pdf($header, $selectPdfCsv, null, 'domestic_bank_list');
			Application_Helper_General::writeLog('DBLS', 'Download PDF Domestic Bank');
		} else if ($this->_request->getParam('print') == 1) {
			$this->_forward('print', 'index', 'widget', array('data_content' => $selectPdfCsv, 'data_caption' => 'Domestic Bank', 'data_header' => $fields));
		} else {
			Application_Helper_General::writeLog('DBLS', 'View Bank Lain');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
	}

	public function syncbanklistAction()
	{
		$this->_helper->layout()->disableLayout();

		$req_id = rand(0, 9999999);
		$app      = Zend_Registry::get('config');
		$bankCode = $app['app']['bankcode'];
		$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
		$result = $svcAccount->inquiryBankList($req_id);

		if ($result['response_code'] == '00' || $result['response_code'] == '0000') {

			$banks = $result['banks'];
			//$this->_db->beginTransaction();

			$where = array('1=1');
			$this->_db->delete('M_DOMESTIC_BANK_TABLE', $where);
			//$where = array('1=1');
			//$resDelete = $db->delete('M_DOMESTIC_BANK_TABLE', $where);
			foreach ($banks as $key => $val) {
				$dataInsert = [
					'CLR_CODE'		=> $val['skn_code'],
					'SWIFT_CODE'			=> $val['bank_code'],
					'BANK_CODE'			=> $val['online_code'],
					'BANK_NAME'		=> $val['bank_name'],
				];

				//var_dump($dataInsert);die;

				$this->_db->insert('M_DOMESTIC_BANK_TABLE', $dataInsert);
				//$this->_db->commit();
			}

			Application_Helper_General::writeLog('DBLS', 'Memperbaharui data Bank Lain tanggal : ' . date('Y-m-d h:s:i'));
			$this->setbackURL('/domesticbank');
			$this->_redirect('/notification/success/index');
		} else {
			$flashMessanger = $this->_helper->getHelper('FlashMessenger');
			$flashMessanger->addMessage(["error" => $result['response_desc'] ?: 'Error from core']);
			$this->_redirect('/domesticbank');
		}
	}
}
