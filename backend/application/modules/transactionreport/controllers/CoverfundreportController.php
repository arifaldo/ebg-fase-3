<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class transactionreport_CoverfundreportController extends Application_Main 
{	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		
//		$data = $this->_db->select()->distinct()
//					->from(array('T_TRANSACTION'),array('SERVICE_NAME'))
////					->order('SERVICE_NAME ASC')
//					->where("SERVICE_NAME != ''")
//					->query()->fetchAll();
//
//    	$this->view->serviceTypeArr = $data;

		$getPayment = new transactionreport_Model_Transactionreport();
		$arrCcyId    = $getPayment->getCCYId();
		$this->view->arrCcyId 		= $arrCcyId;
		
// 		$billerCharges = new setbillercharges_Model_Setbillercharges();
    	$serviceTypeArr 	 = $getPayment->getServiceType();
    	$this->view->serviceTypeArr = $serviceTypeArr;

		$getPayment = new transactionreport_Model_Transactionreport();
		$filterSend = array();
		
		//$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		//Zend_Debug::dump($arrPayType);
		unset($arrPayType['8']);
		
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$arrTraTypeRaw 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		
		$listUserArr = $this->getUserID();
		$listUser = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$listUser = array_merge($listUser,Application_Helper_Array::listArray($listUserArr,'USER_ID','USER_ID'));
// 		var_dump($arrPayStatus);die;
// 		Zend_Debug::dump($arrPayStatus);
		unset($arrPayStatus[10]);
		unset($arrPayStatus[15]);
		unset($arrPayStatus[16]);
		unset($arrPayStatus[1]);
		unset($arrPayStatus[2]);
		unset($arrPayStatus[3]);
		
		
		foreach($arrPayType as $key => $value){
			if($key != 3){
				$optpaytypeRaw[$key] = $this->language->_($value);
			}
		}
		
		$this->view->arrPayType 	= $optpaytypeRaw;
		$this->view->arrPayStatus 	= $arrPayStatus;
		$this->view->arrTraType 	= $arrTraTypeRaw;
		$this->view->listUser 		= $listUser;
		
		$filterSend = array();
		
		$userid = $this->language->_('User ID');
		$username = $this->language->_('User Name');
		$paymentref = $this->language->_('Payment Ref');
		$beneficiaryaccount = $this->language->_('Beneficiary Account');
		$paymentref = $this->language->_('Payment Ref#');
		$trxid = $this->language->_('TRX ID');		
		$traceNo = $this->language->_('Trace No');
		$createddate = $this->language->_('Created Date');
		$beneficiarytype = $this->language->_('Beneficiary Type');
		$updateddate = $this->language->_('Updated Date');
		$paymentdate = $this->language->_('Payment Date');
		$sourceaccount = $this->language->_('Source Account');
		$paymenttype = $this->language->_('Payment Type');
		$paymentstatus = $this->language->_('Payment Status');
		$amount = $this->language->_('Amount');
		$fee = $this->language->_('Fee');
	
		$fields = array	(	
							'nostrobank'  		=> array(
															'field' => 'NOSTRO_NAME',
															'label' => $this->language->_('Nostro Bank'),
															'sortable' => true
														),
							'nostrobankcode'  		=> array(
															'field' => 'NOSTRO_SWIFTCODE',
															'label' => $this->language->_('Swift Code'),
															'sortable' => true
														),
							'currency'  			=> array(
															'field' => 'PS_CCY',
															'label' => $this->language->_('Currency'),
															'sortable' => true
														),							
							'totalvolume'  		=> array(
															'field' => 'COUNTDATA',
															'label' => $this->language->_('Total Volume'),
															'sortable' => true
							),
							
							'amount'  	=> array(
															'field' => 'TOTAL',
															'label' => $this->language->_('Total Amount'),
															'sortable' => true
														),
							
						
																					
						);
		
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$filter_clear 		= $this->_getParam('filter_clear');
		/*	
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		if($sortBy=='U.USER_ID'){
			$sortBy = 'PS_CREATED';
		}
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		*/
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
    //validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	    	$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$data = array();
		
		$filterArr = array('userid'    			=> array('StripTags'),
	                       'username'  			=> array('StripTags','StringTrim','StringToUpper'),
						   
						   'paymentreff' 		=> array('StripTags','StringTrim','StringToUpper'),
						   						   
						   'transferfrom' 		=> array('StripTags','StringTrim'),
						   'transferto' 		=> array('StripTags','StringTrim'),
						   'createdfrom' 		=> array('StripTags','StringTrim'),
						   'createdto' 			=> array('StripTags','StringTrim'),
						   'updatedfrom' 		=> array('StripTags','StringTrim'),
						   'updatedto' 			=> array('StripTags','StringTrim'),
						   
						   'acctsource' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'beneaccount' 		=> array('StripTags','StringTrim','StringToUpper'),
						  						   
						   'paymentstatus' 		=> array('StripTags'),
						   'paymenttype' 		=> array('StripTags'),
						   'transfertype' 		=> array('StripTags'),
						   'providerName' 		=> array('StripTags','StringTrim','StringToUpper'),
				   		   'serviceCategory' 	=> array('StripTags'),
				   		   'traceNo' 	=> array('StripTags','StringTrim','StringToUpper'),
						   'CCYID' 	=> array('StripTags','StringTrim','StringToUpper'),
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array(	"userid","username","paymentreff","transferfrom","transferto","createdfrom","createdto","updatedfrom",
							"updatedto","acctsource","beneaccount","paymentstatus","paymenttype","transfertype","providerName","serviceCategory","traceNo","CCYID");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
		
						'userid' 		=> array(array('InArray', array('haystack' => array_keys($listUserArr)))),
						'username' 		=> array(),	
						
						'paymentreff' 	=> array(),	
												
						'transferfrom' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'transferto'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'createdfrom' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'createdto'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'updatedfrom' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'updatedto'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'acctsource' 	=> array(),	
						'beneaccount' 	=> array(),	
						
						'paymentstatus' => array(array('InArray', array('haystack' => array_keys($arrPayStatus)))),							
						'paymenttype' 	=> array(array('InArray', array('haystack' => array_keys($optpaytypeRaw)))),	
						'transfertype' 	=> array(array('InArray', array('haystack' => array_keys($arrTraTypeRaw)))),	
						'providerName' => array(),	
						'serviceCategory' 	=>array(),	
						'traceNo' 	=>array(),	
						'CCYID' 	=>array(),	
						);

		
		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$fuserid 			= html_entity_decode($zf_filter->getEscaped('userid'));
		$fusername			= html_entity_decode($zf_filter->getEscaped('username'));
		$fpaymentreff 		= html_entity_decode($zf_filter->getEscaped('paymentreff'));
		
		$fcreatedfrom 		= html_entity_decode($zf_filter->getEscaped('createdfrom'));
		$fcreatedto 		= html_entity_decode($zf_filter->getEscaped('createdto'));
		
		$ftransferfrom 		= html_entity_decode($zf_filter->getEscaped('transferfrom'));
		$ftransferto 		= html_entity_decode($zf_filter->getEscaped('transferto'));
		
		$fupdatedfrom		= html_entity_decode($zf_filter->getEscaped('updatedfrom'));
		$fupdatedto			= html_entity_decode($zf_filter->getEscaped('updatedto'));
		
		$facctsource 		= html_entity_decode($zf_filter->getEscaped('acctsource'));
		$fbeneaccount 		= html_entity_decode($zf_filter->getEscaped('beneaccount'));
		$fpaymentstatus 	= html_entity_decode($zf_filter->getEscaped('paymentstatus'));
		$fpaymenttype 		= html_entity_decode($zf_filter->getEscaped('paymenttype'));
		$ftransfertype 		= html_entity_decode($zf_filter->getEscaped('transfertype'));
		$PROVIDER_NAME		= html_entity_decode($zf_filter->getEscaped('providerName'));
		$serviceCategory		= html_entity_decode($zf_filter->getEscaped('serviceCategory'));
		$traceNo			= html_entity_decode($zf_filter->getEscaped('traceNo'));
		$CCYID				= html_entity_decode($zf_filter->getEscaped('CCYID'));
		
		/*if($filter != 'Set Filter')
		{	
			$ftransferfrom = (date("d/m/Y"));
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = $ftransferfrom;
			$this->view->transferto  = $ftransferto;
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
		}*/
		
		if($filter == null)
		{	
			$ftransferfrom = (date("d/m/Y"));
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = $ftransferfrom;
			$this->view->transferto  = $ftransferto;
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
		} 
		
		//if($filter == 'Set Filter')
		if($filter == True)
		{
			if($fuserid!=null)
			{ 
				$filterSend['userid'] = $fuserid;
				$this->view->userid = $fuserid;
			}
			
			if($fusername!=null)
			{ 
				$filterSend['username'] = $fusername;
				$this->view->username = $fusername;
			}
			
			if($fpaymentreff!=null)
			{ 
				$filterSend['paymentreff'] = $fpaymentreff;
				$this->view->paymentreff = $fpaymentreff;
			}
			
			if($ftransferfrom!=null)
			{
				$this->view->transferfrom = $ftransferfrom;
				$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
				$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['transferfrom'] = $transferfrom;
			}
			
			if($ftransferto!=null)
			{
				$this->view->transferto = $ftransferto;
				$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
				$transferto  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['transferto'] = $transferto;
			}
			
			if($fcreatedfrom!=null)
			{
				$this->view->createdfrom = $fcreatedfrom;
				$FormatDate 	= new Zend_Date($fcreatedfrom, $this->_dateDisplayFormat);
				$fcreatedfrom  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['createdfrom'] = $fcreatedfrom;
				
			}
			
			if($fcreatedto!=null)
			{
				$this->view->createdto = $fcreatedto;
				$FormatDate 	= new Zend_Date($fcreatedto, $this->_dateDisplayFormat);
				$fcreatedto  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['createdto'] = $fcreatedto;
				
			}
			
			if($fupdatedfrom!=null)
			{
				$this->view->updatedfrom = $fupdatedfrom;
				$FormatDate 	= new Zend_Date($fupdatedfrom, $this->_dateDisplayFormat);
				$fupdatedfrom  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['updatedfrom'] = $fupdatedfrom;
				
			}
			
			if($fupdatedto!=null)
			{
				$this->view->updatedto = $fupdatedto;
				$FormatDate = new Zend_Date($fupdatedto, $this->_dateDisplayFormat);
				$fupdatedto  = $FormatDate->toString($this->_dateDBFormat);
				$filterSend['updatedto'] = $fupdatedto;
				
			}
			
			if($facctsource!=null)
			{ 
				$filterSend['acctsource'] = $facctsource; 
				$this->view->acctsource = $facctsource;
			}
			
			if($fbeneaccount!=null)	
			{ 
				$filterSend['beneaccount'] = $fbeneaccount;
				$this->view->beneaccount = $fbeneaccount;
			}
			
			if($fpaymentstatus!=null)
			{ 
				$filterSend['paymentstatus'] = $fpaymentstatus;
				$this->view->paymentstatus = $fpaymentstatus;		
			}
			
			if($fpaymenttype!=null)
			{ 
				$filterSend['paymenttype'] = $fpaymenttype;
				$this->view->paymenttype = $fpaymenttype;
			}
			
			if($ftransfertype != null)
			{
				$filterSend['transfertype'] = $ftransfertype;
				$this->view->transfertype = $ftransfertype;
			}
			
			if($PROVIDER_NAME!=null)
			{ 
				$filterSend['providerName'] = $PROVIDER_NAME;
				$this->view->providerName = $PROVIDER_NAME;
			}
			if($serviceCategory!=null)
			{ 
				$filterSend['serviceCategory'] = $serviceCategory;
				$this->view->serviceCategory = $serviceCategory;
			}
			if($traceNo!=null)
			{ 
				$filterSend['traceNo'] = $traceNo;
				$this->view->traceNo = $traceNo;
			}
			if($CCYID!=null)
			{ 
				$filterSend['CCYID'] = $CCYID;
				$this->view->CCYID = $CCYID;
			}
		}
		
		
		else if($filter_clear == TRUE){
			$ftransferfrom = '00/00/0000';
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = '';
			$this->view->transferto  = '';
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
			
	
			
		}
		
		$sorting = $sortBy.' '.$sortDir;
		$result = $getPayment -> getPaymentCoverFund($filterSend,$sorting);


		//summary total
		$totalnumbertx = 0;
		$totalvaluetx = 0;
		foreach($result as $key =>$dt)
		{

			$totalnumbertx = $totalnumbertx + $dt["TRA_AMOUNT"];
			$totalvaluetx  = $totalvaluetx + $dt["TRANSFER_FEE"];

		}

		@$avgnumbertx 	= round($totalnumbertx / count($result),2);
		@$avgvaluetx 	= round($totalvaluetx / count($result),2);
		
		
		//count totalhit and average:
		$totalvaluetx 	= Application_Helper_General::displayMoney($totalvaluetx);
		$totalnumbertx 	= Application_Helper_General::displayMoney($totalnumbertx);
		
		$this->view->totalnumbertx 	= $totalnumbertx;
		$this->view->totalvaluetx 	= $totalvaluetx;
		
		foreach($result as $key=>$value)
		{
// 			if($value['TRANSFER_FEE']==''){
// 				$result[$key]["TRANSFER_FEE"] = '0.00';
// 			}
// 			$result[$key]["LOG_DATE"] = Application_Helper_General::convertDate($value["LOG_DATE"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
// 			$re
			$getPayment = new transactionreport_Model_Transactionreport();
			$count_data = $getPayment->getCountNostro($value["NOSTRO_CODE"]);
			// 					print_r();die;
			
			$transferFee = (!empty($value["TOTAL"])?$value["TOTAL"]:0);
			$result[$key]["COUNT_TOTAL"] = $count_data['0']['TOTAL_COUNT'];
			$result[$key]["TOTAL_DATA"] = $transferFee;
		}

		if ($csv || $pdf) 
		{
				$arr_print = $result;

				$sort_cetak = $this->_getParam('sortby');
				$sortDir_cetak = $this->_getParam('sortdir');
				
				$sortDir_cetak = (Zend_Validate::is($sortDir_cetak,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir_cetak : 'desc';
				$sorting_cetak = $sort_cetak.' '.$sortDir_cetak;
				$arr_cetak = $getPayment ->getPaymentCoverFundCetak($filterSend,$sorting_cetak);
						
				$result = $arr_cetak;
			$i = 0;
			foreach($result as $key => $val)
			{
				$count_data = $getPayment->getCountNostro($value["NOSTRO_CODE"]);
				// 					print_r();die;
					
				$transferFee = (!empty($value["TOTAL"])?$value["TOTAL"]:0);
				$result[$key]["COUNT_TOTAL"] = $count_data['0']['TOTAL_COUNT'];
				$result[$key]["TOTAL_DATA"] = $transferFee;
				
				unset($result[$key]['TOTAL']);
//				unset($arr[$key]['PS_UPDATED']);
			}
			
// 			echo "<pre>";
// 			print_r($result);die;
//				unset($fields['username']);
//				unset($fields['updateddate']);
				
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			
			if($csv)
			{
// 				$arr[$key+1]['number']	= false;
// 				$arr[$key+1][0]	= '';
// 				$arr[$key+1][1]	= '';
// 				$arr[$key+1][2]	= '';
// 				$arr[$key+1][4]	= '';
// 				$arr[$key+1][5]	= '';
// 				$arr[$key+1][6]	= '';
// 				$arr[$key+1][7]	= '';
// 				$arr[$key+1][8]	= '';
// 				$arr[$key+1][9]	= '';
// 				$arr[$key+1][10]	= '';
				
// 				$arr[$key+1][11]	= 'Total';
// 				$arr[$key+1][12]	= $totalnumbertx;
// 				$arr[$key+1][13]	= $totalvaluetx;
// 				$arr[$key+1][14]	= '';
// 				$arr[$key+1][15]	= '';
// 				$arr[$key+1][16]	= '';
// 				$arr[$key+1][17]	= '';
				$this->_helper->download->csv($header,$result,null,$this->language->_('Cover Fund Report'));  
				Application_Helper_General::writeLog('RPPY','Export CSV Cover Fund Report');
			}
			if($pdf)
			{
// 				$arr[$key+1]['number']	= false;
// 				$arr[$key+1][0]	= '';
// 				$arr[$key+1][1]	= '';
// 				$arr[$key+1][2]	= '';
// 				$arr[$key+1][4]	= '';
// 				$arr[$key+1][5]	= '';
// 				$arr[$key+1][6]	= '';
// 				$arr[$key+1][7]	= '';
// 				$arr[$key+1][8]	= '';
// 				$arr[$key+1][9]	= '';
// 				$arr[$key+1][10]	= '';
// 				$arr[$key+1][11]	= '';
// 				$arr[$key+1][12]	= 'Total';
// 				$arr[$key+1][13]	= $totalnumbertx;
// 				$arr[$key+1][14]	= $totalvaluetx;
// 				$arr[$key+1][15]	= '';
// 				$arr[$key+1][16]	= '';
// 				$arr[$key+1][17]	= '';
// 				//$arr[$key+1][18]	= '';
				$this->_helper->download->pdf($header,$result,null,$this->language->_('Cover Fund Report'));  
				Application_Helper_General::writeLog('RPPY','Export PDF Payment Report');
			}
			
			//Zend_Debug::dump($dataSQL);die;
		}else if($this->_request->getParam('print')){
				
				$arr_print = $result;

				$i = 0;
				foreach($result as $key => $val)
				{
					$count_data = $getPayment->getCountNostro($value["NOSTRO_CODE"]);
				// 					print_r();die;
					
				$transferFee = (!empty($value["TOTAL"])?$value["TOTAL"]:0);
				$result[$key]["COUNT_TOTAL"] = $count_data['0']['TOTAL_COUNT'];
				$result[$key]["TOTAL_DATA"] = $transferFee;
				
				unset($result[$key]['TOTAL']);

				}

				if($this->_request->getParam('print') == 1){

				
	            $this->_forward('print', 'index', 'widget', array('data_content' => $arr_print, 'data_caption' => 'Cover Fund Report', 'data_header' => $fields));
	       	}

		}
		else
		{
			Application_Helper_General::writeLog('RPPY','View Cover Fund Report');
		}
		
		$this->paging($result);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}
}
