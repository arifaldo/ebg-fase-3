<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';
class reportcrm_IndexdelController extends Application_Main {


	
	public function indexAction()
    {   

        $this->_helper->_layout->setLayout('newlayout');
        
        $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
        if(count($temp)>1){
            if($temp[0]=='F' || $temp[0]=='S'){
                if($temp[0]=='F')
                    $this->view->error = 1;
                else
                    $this->view->success = 1;
                $msg = ''; unset($temp[0]);
                foreach($temp as $value)
                {
                    if(!is_array($value))
                        $value = array($value);
                    $msg .= $this->view->formErrors($value);
                }
                $this->view->report_msg = $msg;
            }   
        }   

        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        $arrAccount     = $CustomerUser->getAccounts();
        
        if(is_array($arrAccount) && count($arrAccount) > 0){
            foreach($arrAccount as $key => $value){
                
                $val        = $arrAccount[$key]["ACCT_NO"];
                $ccy        = $arrAccount[$key]["CCY_ID"];
                $acctname   = $arrAccount[$key]["ACCT_NAME"];
                //$acctalias    = $arrAccount[$key]["ACCT_ALIAS_NAME"];
                $accttype   = ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';  // 10 : saving, 20 : giro;
                
                $arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';
                
            }
        }
        else { $arrAccountRaw = array();}
// 1679091c5a880faf6fb5e6087eb1b2dc
// echo $this->encryptIt('6');die;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'cmdemo';
        $secret_iv = 'democm';

        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $string = $this->_getParam('report');


        // $string = '10';
        // $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        // $output = base64_encode($output);
         $report = openssl_decrypt(base64_decode($this->_getParam('report')), $encrypt_method, $key, 0, $iv);
    try {
        $where['ID = ?'] = $report;
        // print_r($where);die;
      $result =  $this->_db->delete('T_REPORT_GENERATOR',$where);
                    // $this->_db->insert('T_REPORT_GENERATOR',$insArr);
        $wheredelete['COLM_REPORD_ID = ?'] = $report;
      $result = $this->_db->delete('T_REPORT_COLOMN',$wheredelete);
    } catch (Exception $e) {
        // print_r($e);die;
    }
        

        // $report_id = $this->decryptIt($this->_getParam('report'));
        // print_r($output);die;
      // print_r($result);die;
        if($result){
            $this->_redirect('reportcrm/report');
        }



        
        
       
        
    }

    
}
