<?php
require_once 'Zend/Controller/Action.php';

class insurancebranch_SuggestiondetailController extends Application_Main
{
    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newpopup');

        $model = new insurancebranch_Model_Insurancebranch();

        $changes_id = $this->_getParam('changes_id');
        $changes_id = (Zend_Validate::is($changes_id, 'Digits')) ? $changes_id : 0;
        $this->view->changes_id = $changes_id;

        if ($changes_id) {
            $select = $this->_db->select()
                ->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
                ->where('CHANGES_ID = ' . $this->_db->quote($changes_id))
                ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
            $result = $this->_db->fetchOne($select);

            if (empty($result)) {
                $this->_redirect('/notification/invalid/index');
            } else {
                $select = $this->_db->select()
                    ->from(
                        array('A' => 'TEMP_INS_BRANCH'),
                        array('*')
                    )
                    ->joinLeft(
                        array('B' => 'M_CUSTOMER'),
                        'B.CUST_ID = A.CUST_ID',
                        array('CUST_ID', 'CUST_NAME')
                    )
                    ->joinLeft(
                        array('G' => 'T_GLOBAL_CHANGES'),
                        'G.CHANGES_ID = A.CHANGES_ID',
                        array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
                    )
                    ->where('A.CHANGES_ID = ?', $changes_id);
                $resultData = $this->_db->fetchRow($select);
                // echo '<pre>';print_r($resultData);die('resultData');

                if ($resultData) {
                    $cust_id     = $resultData['CUST_ID'];
                    $cust_name   = $resultData['CUST_NAME'];
                    $branch_code = $resultData['INS_BRANCH_CODE'];

                    $this->view->cust_id            = $resultData['CUST_ID'];
                    $this->view->changes_id         = $resultData['CHANGES_ID'];
                    $this->view->changes_type       = $resultData['CHANGES_TYPE'];
                    $this->view->changes_status     = $resultData['CHANGES_STATUS'];
                    $this->view->read_status        = $resultData['READ_STATUS'];

                    // DATA TEMP INSURANCE BRANCH
                    $this->view->branch_code        = $cust_name . ' (' . $cust_id . ')';
                    $this->view->branch_name        = $resultData['INS_BRANCH_NAME'];
                    $this->view->branch_email       = $resultData['INS_BRANCH_EMAIL'];
                    $this->view->branch_acct        = $resultData['INS_BRANCH_ACCT'] . ' [' . $resultData['INS_BRANCH_CCY'] . ']';
                    $this->view->acct_type          = $resultData['ACCT_TYPE'];

                    // DATA MASTER INSURANCE BRANCH
                    $select = $this->_db->select()
                        ->from(array('A' => 'M_INS_BRANCH'), array('*'))
                        ->joinLeft(
                            array('B' => 'M_CUSTOMER'),
                            'B.CUST_ID = A.CUST_ID',
                            array('CUST_ID', 'CUST_NAME')
                        )
                        ->where('A.INS_BRANCH_CODE = ?', $branch_code);
                    $m_insBranch = $this->_db->fetchRow($select);

                    if ($m_insBranch) {
                        $this->view->m_branch_code        = $m_insBranch['CUST_NAME'] . ' (' . $m_insBranch['CUST_ID'] . ')';
                        $this->view->m_branch_name        = $m_insBranch['INS_BRANCH_NAME'];
                        $this->view->m_branch_email       = $m_insBranch['INS_BRANCH_EMAIL'];
                        $this->view->m_branch_acct        = $m_insBranch['INS_BRANCH_ACCT'] . ' [' . $m_insBranch['INS_BRANCH_CCY'] . ']';
                        $this->view->m_acct_type          = $m_insBranch['ACCT_TYPE'];
                    }

                    $submit_approve         = $this->_getParam('submit_approve');
                    $submit_reject          = $this->_getParam('submit_reject');
                    $submit_request_repair  = $this->_getParam('submit_request_repair');

                    if ($submit_approve) {

                        $svcAccount = new Service_Account($resultData['INS_BRANCH_ACCT'], 'IDR');
                        $result = $svcAccount->inquiryAccountBalance('AB', FALSE);
                        //echo '<pre>';
                        //var_dump($result);die;
                        if ($result['response_code'] == '0000' || $result['response_code'] == '00') {
                            $checkAcct = $this->_db->select()
                                ->from('M_CUSTOMER_ACCT', ['ACCT_STATUS'])
                                ->where('ACCT_NO = ?', $resultData['INS_BRANCH_ACCT'])
                                ->query()->fetch();

                            if (intval($checkAcct['ACCT_STATUS']) === 1) {
                                $valid = true;
                            } else {
                                $valid = false;
                                $err = true;

                                switch (intval($checkAcct['ACCT_STATUS'])) {
                                    case 2:
                                        $errmsg = "Nomor Rekening sedang ditangguhkan";
                                        break;

                                    default:
                                        $errmsg = "Nomor Rekening tidak aktif";
                                        break;
                                }
                            }
                        } else {
                            $valid = false;
                            $err = true;
                            $errmsg = 'Service error';
                        }

                        //var_dump($valid);die;
                        if ($valid) {
                            try {
                                $this->_db->beginTransaction();
                                // INSERT/UPDATE DATA
                                $data1 = [
                                    'CUST_ID'            => $cust_id,
                                    'INS_BRANCH_NAME'    => $resultData['INS_BRANCH_NAME'],
                                    'INS_BRANCH_CODE'    => $resultData['INS_BRANCH_CODE'],
                                    'INS_BRANCH_EMAIL'   => $resultData['INS_BRANCH_EMAIL'],
                                    'INS_BRANCH_ACCT'    => $resultData['INS_BRANCH_ACCT'],
                                    'INS_BRANCH_CCY'     => $resultData['INS_BRANCH_CCY'],
                                    'ACCT_TYPE'          => $resultData['ACCT_TYPE'],
                                    'LAST_SUGGESTED'     => $resultData['LAST_SUGGESTED'],
                                    'LAST_SUGGESTEDBY'   => $resultData['LAST_SUGGESTEDBY'],
                                    'LAST_APPROVED'      => new Zend_Db_Expr('now()'),
                                    'LAST_APPROVEDBY'    => $this->_userIdLogin
                                ];
                                $where1 = ['INS_BRANCH_CODE = ?' => $branch_code];

                                if ($resultData['CHANGES_TYPE'] == 'E') {
                                    $this->_db->update('M_INS_BRANCH', $data1, $where1);
                                } else if ($resultData['CHANGES_TYPE'] == 'L') {
                                    $this->_db->delete('M_INS_BRANCH', $where1);
                                } else {
                                    $this->_db->insert('M_INS_BRANCH', $data1);
                                }

                                $this->_db->delete('TEMP_INS_BRANCH', $where1);

                                $data3  = [
                                    'CHANGES_STATUS'    => 'AP',
                                    'LASTUPDATED'       => new Zend_Db_Expr('now()')
                                ];
                                $where3 = ['CHANGES_ID = ?' => $changes_id];
                                $this->_db->update('T_GLOBAL_CHANGES', $data3, $where3);

                                $this->_db->commit();

                                $this->view->is_approve = true;

                                Application_Helper_General::writeLog('AIBC', 'Approve Insurance Branch Changes for ' . $cust_name . ' (' . $cust_id . ')');
                            } catch (Exception $error) {
                                $this->_db->rollBack();
                                echo '<pre>';
                                print_r($error->getMessage());
                                echo '</pre><br>';
                                die;
                            }
                        }
                    } elseif ($submit_reject) {
                        try {
                            $this->_db->beginTransaction();

                            $where1 = ['INS_BRANCH_CODE = ?' => $branch_code];
                            $this->_db->delete('TEMP_INS_BRANCH', $where1);

                            $data2  = [
                                'CHANGES_STATUS'    => 'RJ',
                                'LASTUPDATED'       => new Zend_Db_Expr('now()')
                            ];
                            $where2 = ['CHANGES_ID = ?' => $changes_id];
                            $this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

                            $this->_db->commit();

                            $this->view->is_reject = true;
                            Application_Helper_General::writeLog('AIBC', 'Reject Insurance Branch Changes for ' . $cust_name . ' (' . $cust_id . ')');
                        } catch (Exception $error) {
                            $this->_db->rollBack();
                            echo '<pre>';
                            print_r($error->getMessage());
                            echo '</pre><br>';
                            die;
                        }
                    }
                    // elseif($submit_request_repair){
                    //     try {
                    //         $this->_db->beginTransaction();

                    //         $data1  = [
                    //             'CHANGES_STATUS'    => 'RR',
                    //             'LASTUPDATED'       => new Zend_Db_Expr('now()')
                    //         ];
                    //         $where1 = ['CHANGES_ID = ?' => $changes_id];
                    //         $this->_db->update('T_GLOBAL_CHANGES', $data1, $where1);

                    //         $this->_db->commit();

                    //         $this->view->is_request_repair = true;
                    //     }
                    //     catch(Exception $error){
                    //         $this->_db->rollBack();
                    //         echo '<pre>';
                    //         print_r($error->getMessage());
                    //         echo '</pre><br>';
                    //         die;
                    //     }
                    // }
                    else {
                        Application_Helper_General::writeLog('VIBC', 'View Insurance Branch Changes for ' . $cust_name . ' (' . $cust_id . ')');
                    }
                    if ($err) {
                        //$err = true;
                        //$errmsg = 'Service not available';

                        $this->view->err = true;
                        $this->view->errmsg = $errmsg;
                    }
                }
            }
        } else {
            $this->_redirect('/popuperror/index/index');
        }
    }

    // public function repairAction()
    // {
    //     $this->_helper->layout()->setLayout('newpopup');

    //     $model = new insurancebranch_Model_Insurancebranch();

    //     $changes_id = $this->_getParam('changes_id');
    //     $changes_id = (Zend_Validate::is($changes_id,'Digits')) ? $changes_id : 0;

    //     if ($changes_id) {
    //         $select = $this->_db->select()
    //                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID'))
    //                             ->where('CHANGES_ID = '.$this->_db->quote($changes_id))
    //                             ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
    //         $result = $this->_db->fetchOne($select);

    //         if (empty($result)) {
    //             $this->_redirect('/notification/invalid/index');
    //         } else {
    //             $select = $this->_db->select()
    //                                 ->from(
    //                                     array('A' => 'TEMP_INS_BRANCH'),
    //                                     array('*')
    //                                 )
    //                                 ->joinLeft(
    //                                     array('B' => 'M_CUSTOMER'),'B.CUST_ID = A.CUST_ID',
    //                                     array('CUST_ID', 'CUST_NAME')
    //                                 )
    //                                 ->joinLeft(
    //                                     array('G' => 'T_GLOBAL_CHANGES'),'G.CHANGES_ID = A.CHANGES_ID',
    //                                     array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS','MODULE')
    //                                 )
    //                                 ->where('A.CHANGES_ID = ?', $changes_id);
    //             $resultData = $this->_db->fetchRow($select);
    //             // echo '<pre>';print_r($resultData);die('resultData');

    //             if ($resultData) {
    //                 $cust_id     = $resultData['CUST_ID'];
    //                 $branch_code = $resultData['INS_BRANCH_CODE'];

    //                 $params = $this->getRequest()->getParams();
    //                 $this->view->params = $params;

    //                 $data = $model->getTempData($branch_code);
    //                 $this->view->data = $data;
    //                 // echo '<pre>';print_r($params);echo '</pre>';die('params');

    //                 $getCustomer = $model->getCustomer();
    //                 $custIdArr   = Application_Helper_Array::listArray($getCustomer, 'CUST_ID', 'CUST_NAME');
    //                 $this->view->custIdArr = $custIdArr;

    //                 $process   = $this->_getParam('process');
    //                 $isConfirm = (empty($this->_request->getParam('isConfirm'))) ? false : true;
    //                 $submitBtn = ($this->_request->isPost() && $process == "submit") ? true : false;

    //                 if ($this->_request->isPost()) {
    //                     $params     = $this->_request->getParams();
    //                     $filters    = array('*' => array('StringTrim', 'StripTags'));
    //                     $validators = array(
    //                         'cust_id'       => array(
    //                                             'NotEmpty',
    //                                             'messages' => array($this->language->_('Insurance Company cannot be empty')),
    //                                         ),
    //                         'cust_name'     => array('allowEmpty' => true),
    //                         'branch_ccy'    => array('allowEmpty' => true),
    //                         'acct_type'     => array('allowEmpty' => true),
    //                         'branch_name'   => array(
    //                                             'NotEmpty',
    //                                             'messages' => array($this->language->_('Branch Name cannot be empty')),
    //                                         ),
    //                         'branch_email'  => array(
    //                                             'NotEmpty',
    //                                             'messages' => array($this->language->_('Branch Email cannot be empty')),
    //                                         ),
    //                         'branch_acct'   => array(
    //                                             'NotEmpty',
    //                                             'messages' => array($this->language->_('Branch Account cannot be empty')),
    //                                         ),
    //                     );

    //                     $zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

    //                     $cust_id        = $zf_filter_input->cust_id;
    //                     $cust_name      = $zf_filter_input->cust_name;
    //                     $branch_ccy     = $zf_filter_input->branch_ccy;
    //                     $acct_type      = $zf_filter_input->acct_type;
    //                     $branch_name    = $zf_filter_input->branch_name;
    //                     $branch_email   = $zf_filter_input->branch_email;
    //                     $branch_acct    = $zf_filter_input->branch_acct;

    //                     if ($submitBtn) {
    //                         if ($zf_filter_input->isValid()) {
    //                             if($isConfirm == false){
    //                                 $isConfirm = true;
    //                             }else{
    //                                 try {
    //                                     $this->_db->beginTransaction();

    //                                     $data1 = [
    //                                         'INS_BRANCH_CODE'   => $branch_code,
    //                                         'CUST_ID'           => $cust_id,
    //                                         'INS_BRANCH_NAME'   => $branch_name,
    //                                         'INS_BRANCH_EMAIL'  => $branch_email,
    //                                         'INS_BRANCH_ACCT'   => $branch_acct,
    //                                         'INS_BRANCH_CCY'    => $branch_ccy,
    //                                         'ACCT_TYPE'         => $acct_type,
    //                                         'FLAG'              => 2, // Waiting Approve
    //                                         'LAST_SUGGESTED'    => new Zend_Db_Expr('now()'),
    //                                         'LAST_SUGGESTEDBY'  => $this->_userIdLogin
    //                                     ];
    //                                     $where1 = ['CHANGES_ID = ?' => $changes_id];
    //                                     $this->_db->update('TEMP_INS_BRANCH', $data1);

    //                                     $data2  = [
    //                                         'CHANGES_STATUS'=> 'WA',
    //                                         'READ_STATUS'   => 2,
    //                                         'LASTUPDATED'   => new Zend_Db_Expr('now()')
    //                                     ];
    //                                     $where2 = ['CHANGES_ID = ?' => $changes_id];
    //                                     $this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

    //                                     $this->_db->commit();

    //                                     Application_Helper_General::writeLog('MIBO', 'Update Insurance Branch for ' . $cust_name . ' (' . $cust_id . ') - '.$branch_name);
    //                                     $this->_redirect('/popup/successpopup/index');
    //                                 } catch (Exception $error) {
    //                                     $this->_db->rollBack();
    //                                     echo '<pre>';
    //                                     print_r($error->getMessage());
    //                                     echo '</pre><br>';
    //                                     die;
    //                                 }
    //                             }
    //                         }else{
    //                             $errors = $zf_filter_input->getMessages();
    //                             $errDesc['cust_id']         = isset($errors['cust_id']) ? $errors['cust_id'] : null;
    //                             $errDesc['branch_name']     = isset($errors['branch_name']) ? $errors['branch_name'] : null;
    //                             $errDesc['branch_email']    = isset($errors['branch_email']) ? $errors['branch_email'] : null;
    //                             $errDesc['branch_acct']     = isset($errors['branch_acct']) ? $errors['branch_acct'] : null;
    //                         }
    //                     } else {
    //                         $isConfirm = false;
    //                     }
    //                 } else {
    //                     Application_Helper_General::writeLog('MIBO', 'View Repair Insurance Branch for '.$cust_name.' ('.$cust_id.')');
    //                 }

    //                 $this->view->isConfirm      = $isConfirm;
    //                 $this->view->errDesc        = $errDesc;
    //                 $this->view->cust_id        = $cust_id;
    //                 $this->view->cust_name      = $cust_name;
    //                 $this->view->branch_ccy     = $branch_ccy;
    //                 $this->view->acct_type      = $acct_type;
    //                 $this->view->branch_name    = $branch_name;
    //                 $this->view->branch_email   = $branch_email;
    //                 $this->view->branch_acct    = $branch_acct;
    //             }
    //         }
    //     } else {
    //         $this->_redirect('/popuperror/index/index');
    //     }
    // }
}
