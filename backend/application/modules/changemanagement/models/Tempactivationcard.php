<?php
/**
 * Tempcustomer model
 *
 * @author
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';

class Changemanagement_Model_Tempactivationcard extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = '';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null)
	{
		//query from TEMP_CUSTOMER
		$debitCard = $this->dbObj->select()
						  ->from('TEMP_DEBITCARD')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetchAll(Zend_Db::FETCH_ASSOC);
			
        if(!count($debitCard))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(debitCard)';
			return false;
        }
		
		foreach($debitCard as $key => $row ){
			
		
				try
					{
						$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
							
						$emailQueueProducer = $queueService->getQueueByProfileName("DEBITCARD_ACTIVATION");
						$insArr['card_no'] = $row['DEBIT_NUMBER'];
						$insArr['acct_no'] = $row['ACCT_NO'];
						$insArr['user_id'] = $row['DEBIT_SUGGESTEDBY'];
						$datarab = json_encode($insArr);
						//var_dump($datarab);die;
						$emailQueueProducer->insertQueueItem($datarab);

						//$customerins = $this->dbObj->update('T_DEBITCARD',$updateArr,$whereArr);
					}
				catch(exception $e)
					{
						var_dump($e);die;
						Zend_Debug::dump($e->getMessage());
					}
					if(!(boolean)$emailQueueProducer)
					{
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query failed(DEBITCARD_ACTIVATION)';
						return false;
					}
				}

		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null)
	{
		$transFailed = false;
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_DEBITCARD')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
		else
		{
			$custId = $customer['CUST_ID'];
		}
//		Zend_Debug::dump($customer);
//		die;
		//update record customer
		$updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$updateArr['DEBIT_APPROVEDBY']   = $actor;

		$whereArr = array(
							'REG_NUMBER = ?'=>(string)$customer['REG_NUMBER'],
							'DEBIT_NUMBER = ?'=>(string)$customer['DEBIT_NUMBER']
						);
						
		$customerupdate = $this->dbObj->update('T_DEBITCARD',$updateArr,$whereArr);

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Edited, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$updateArr['CUST_NAME'].' Change id : '.$this->_changeId);

		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_DEBITCARD')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
        }

        $updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$updateArr['DEBIT_APPROVEDBY']   = $actor;

		$whereArr = array(
							'REG_NUMBER = ?'=>(string)$customer['REG_NUMBER'],
							'DEBIT_NUMBER = ?'=>(string)$customer['DEBIT_NUMBER']
						);
						
		$customerupdate = $this->dbObj->update('T_DEBITCARD',$updateArr,$whereArr);
		
		
		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
		}

		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_DEBITCARD')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
        }

        $updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$updateArr['DEBIT_APPROVEDBY']   = $actor;

		$whereArr = array(
							'REG_NUMBER = ?'=>(string)$customer['REG_NUMBER'],
							'DEBIT_NUMBER = ?'=>(string)$customer['DEBIT_NUMBER']
						);
						
		$customerupdate = $this->dbObj->update('T_DEBITCARD',$updateArr,$whereArr);
		
		
		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
		}
		
		$deleteChanges  = $this->deleteSuspend();
		
		if(!$deleteChanges)
			return false;

		return true;
	}


	/*
	 if delete customer :
	 - delete user
	 - delete bank account
	 - delete user limit
	 - delete daily limit
	*/
	/*public function approveDelete($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER_BIN')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }

        //update status customer
	    $updateArr = array('CUST_BIN_STATUS' => 3);
	    $updateArr['BIN_SUGGESTED']   = $customer['BIN_SUGGESTED'];
		$updateArr['BIN_SUGGESTEDBY'] = $customer['BIN_SUGGESTEDBY'];
		$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BIN_UPDATEDBY']   = $actor;
		
		/////////////////
		$updateArrDataTrans = array('PAID_STATUS' => 3);
		$whereArrDataTrans = array(
			'CUST_BIN = ?'=>(string)$customer['CUST_BIN_TMP']
		);
		$customerupdateDataTrans = $this->dbObj->update('T_VP_TRANSACTION',$updateArrDataTrans,$whereArrDataTrans);
		/////////////////
		
		$whereArr = array(
			'CUST_ID = ?'=>(string)$customer['CUST_ID'],
			'CUST_BIN = ?'=>(string)$customer['CUST_BIN_TMP']
		);
		$customerupdate = $this->dbObj->update('M_CUSTOMER_BIN',$updateArr,$whereArr);

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
        //END update status customer


		
		//delete temp customer
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			      return false;

		return true;
	}*/

	public function approveDelete($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_DEBITCARD')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
        }

        $updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$updateArr['DEBIT_APPROVEDBY']   = $actor;

		$whereArr = array(
							'REG_NUMBER = ?'=>(string)$customer['REG_NUMBER'],
							'DEBIT_NUMBER = ?'=>(string)$customer['DEBIT_NUMBER']
						);
						
		$customerupdate = $this->dbObj->update('T_DEBITCARD',$updateArr,$whereArr);

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
		}
        //END update status customer


		
		//delete temp customer
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			      return false;

		return true;
	}


	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_DEBITCARD',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}


		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_DEBITCARD',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_DEBITCARD',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		return true;
	}

	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_DEBITCARD',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		return true;
	}

	public function deleteDelete()
	{


		$customerdelete = $this->dbObj->delete('TEMP_DEBITCARD',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;



	}


    public function approveDeactivate(){

	}

	public function approveActivate(){

	}

    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}





}