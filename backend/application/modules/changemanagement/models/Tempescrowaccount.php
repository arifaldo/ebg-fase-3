<?php
/**
 * Tempuser model
 * 
 * @author Suhandi
 * @version
 */

class Changemanagement_Model_Tempescrowaccount extends Changemanagement_Model_Tempchanges 
{

	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null){
		//query from TEMP_MASTER
		$tmaster = $this->dbObj->select()
						  			->from('TEMP_MASTER')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
		
		if(!count($tmaster)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Escrow)';
			return false;
        }

		$master = $this->dbObj->select()
						  			->from('M_MASTER')
						  			->where('M_KEY2 = ?',$tmaster['M_KEY2'])
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
		$created 			= array('M_CREATED' => $this->_changesInfo['CREATED'],
												  'M_CREATEDBY' => $this->_changesInfo['CREATED_BY']);
		$updated 			= array('M_UPDATED' => new Zend_Db_Expr('now()'),
												  'M_UPDATEDBY' => $actor);
		$suggested 		= array('M_SUGGESTED' =>  $this->_changesInfo['CREATED'],
												  'M_SUGGESTEDBY' => $this->_changesInfo['CREATED_BY']);
		// SASYA // ACCOUNT BARU UPDATE CURRENCY AND SUGGESTED AND UPDATED [04-12-2013]
		if(count($master))
		{
			$tmaster 				= array_diff_key($tmaster,array('TEMP_ID'=>'','CHANGES_ID'=>''));	

			//UPDATING
			$tmaster					  = array_merge($tmaster,$updated);
			$tmaster					  = array_merge($tmaster,$suggested);

			$update				  = $this->dbObj->update('M_MASTER',$tmaster);
		}
		else
		{
			//--------------------------------INSERT TO MASTER TABLE----------------------------------------------------------

			$tmaster 				= array_diff_key($tmaster,array('TEMP_ID'=>'','CHANGES_ID'=>''));	

			//INSERTING
			$tmaster					  = array_merge($tmaster,$created);
			$tmaster					  = array_merge($tmaster,$updated);
			$tmaster					  = array_merge($tmaster,$suggested);

			$insert				  = $this->dbObj->insert('M_MASTER',$tmaster);

			//-------------------------------END INSERT TO TABLE MASTER-------------------------------------------------------		
		}
		$deleteChanges = $this->deleteNew();
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) 
	{		
		// UPDATE STATUS TO DELETE//
		$statusDelete = null;
		$updateStatusDelete = array(
												'M_STATUS' => 3,
											  );
		$whereArr = array(
											$this->dbObj->quoteInto('M_KEY = ?','ESCROW'),
											$this->dbObj->quoteInto('M_KEY2 = ?',$this->_changesInfo['KEY_FIELD']),
										);
		$statusDelete	= $this->dbObj->update('M_MASTER',$updateStatusDelete,$whereArr);

		$deleteChanges = $this->deleteDelete();
		return true;
	}
	
    /**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

		//delete from TEMP_MASTER		
		$TEMP_MASTER = $this->dbObj->delete('TEMP_MASTER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() 
	{
		//delete from TEMP_MASTER		
		$TEMP_MASTER = $this->dbObj->delete('TEMP_MASTER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
}