<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class loan_DetailController extends Application_Main
{
	public function indexAction(){

		$this->_helper->layout()->setLayout('newpopup');
		$loan_id = $this->_getParam('loan_id');

		$selectLoan = $this->_db->select()
			        ->from(array('L' => 'T_LOAN'))
			        ->where('L.LOAN_ID =?', $loan_id)
			        ->join(array('C' => 'M_CUSTOMER'), 'L.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'));

		$loanData = $this->_db->fetchRow($selectLoan);

		$this->view->loanData = $loanData;
		

		$custInfo = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('C' => 'M_CUSTOMER'))
						->where("CUST_ID = ".$this->_db->quote($loanData['CUST_ID']))
				);
		$this->view->custname	= $custInfo['CUST_NAME'];
		$this->view->custadd	= $custInfo['CUST_ADDRESS'];
		$this->view->custcity	= $custInfo['CUST_CITY'];
		$this->view->custprov	= $custInfo['CUST_PROVINCE'];
		$this->view->custzip	= $custInfo['CUST_ZIP'];
		$this->view->custphone	= $custInfo['CUST_PHONE'];
		$this->view->custfax	= $custInfo['CUST_FAX'];
		$this->view->finance	= $custInfo['CUST_FINANCE'];
		$this->view->custemail	= $custInfo['CUST_EMAIL'];

	}
}