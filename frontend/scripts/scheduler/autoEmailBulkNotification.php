<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

$config    	= Zend_Registry::get('config');

	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWapp = $Settings->getSetting('master_bank_wapp');
	$templateEmailMasterBankApp = $Settings->getSetting('master_bank_app_name');
	
	
$template = file_get_contents(LIBRARY_PATH.'/email template/payment pending inprogress notification.html');
$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');

$transFT   = array( '[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
					'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
					'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl													
				  );

$FOOTER_ID = strtr($FOOTER_ID, $transFT);
$FOOTER_EN = strtr($FOOTER_EN, $transFT);

$db = Zend_Db_Table::getDefaultAdapter();
$subject = 'Payment Pending/In Progress Notification';
$mailedTo = 'No Email Sent';

$paymentStatus 	= $config["payment"]["status"]; 
$paymentType 	= $config["payment"]["type"]; 

$listPaymentStatus = array_combine(array_values($paymentStatus['code']),array_values($paymentStatus['desc']));
$listPaymentType = array_combine(array_values($paymentType['code']),array_values($paymentType['desc']));

$casePaymentStatus = "(CASE PS_STATUS ";
foreach($listPaymentStatus as $key=>$val){
	$casePaymentStatus .= " WHEN '".$key."' THEN '".$val."' ";
}

$casePaymentStatus .= " END)";

$casePaymentType = "(CASE PS_TYPE ";
foreach($listPaymentType as $key=>$val){
	$casePaymentType .= " WHEN '".$key."' THEN '".$val."' ";
}
	
$casePaymentType .= " END)";	$set = new Settings();
$listReceiver = $set->getSetting('email_exception');
$receiver = explode(";", $listReceiver);
	
// 	SEND FILE STATUS = 0
$data = array();
$data_content = '';
$status = 0;
$counter = 0;

$data = $db->SELECT()
	->FROM(array('T' => 'T_PSLIP'),array('PS_NUMBER','PS_EFDATE','PS_TYPE' => $casePaymentType,'PS_STATUS' =>  $casePaymentStatus))
	->JOINLEFT(array('E' => 'T_EMAIL_NOTIFICATION'), 'T.PS_NUMBER = E.PS_NUMBER', array() )
	->WHERE('T.PS_STATUS = 8')
	->WHERE('T.SENDFILE_STATUS = 0')
	->WHERE('E.PS_FAILED_MULTI = 1')
		->QUERY()->FETCHALL()
;

if(count($data) > 0){
	foreach($data as $content) {
		$updateT_PSLIP = array(
			'PS_FAILED_MULTI' => 0,
		);
		
		$whereT_PSLIP  = array(
			'PS_NUMBER = ? ' => $content['PS_NUMBER']
		);
		
		$db->update('T_EMAIL_NOTIFICATION', $updateT_PSLIP, $whereT_PSLIP);

		$data_content .= '
		<tr>
			<td><font>'.++$counter.'</font></td>
			<td><font>'.$content['PS_NUMBER'].'</font></td>
			<td><font>'.Application_Helper_General::convertDate($content['PS_EFDATE'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
			<td><font>'.$content['PS_TYPE'].'</font></td>
			<td><font>'.$content['PS_STATUS'].'</font></td>
			<td><font> Error when upload file to FTP Server </font></td>
		</tr>';
	}
}
	
// 	SEND FILE STATUS = 1
$data = array();
$status = 0;

$data = $db->SELECT()
	->FROM(array('T' => 'T_PSLIP'),array('PS_NUMBER','PS_EFDATE','PS_TYPE' => $casePaymentType,'PS_STATUS' =>  $casePaymentStatus))
	->JOINLEFT(array('E' => 'T_EMAIL_NOTIFICATION'), 'T.PS_NUMBER = E.PS_NUMBER', array() )
	->WHERE('T.PS_STATUS = 8')
	->WHERE('T.SENDFILE_STATUS = 1')
	->WHERE('E.PS_FAILED_MULTI = 1')
	->WHERE(" (TIMESTAMPDIFF(Minute, PS_UPDATED , now())) > 20 ")
	->QUERY()->FETCHALL()
;

if(count($data) > 0){
	foreach($data as $content) {
		$updateT_PSLIP = array(
			'PS_FAILED_MULTI' => 0,
		);
		
		$whereT_PSLIP  = array(
			'PS_NUMBER = ? ' => $content['PS_NUMBER']
		);
		
		$db->update('T_EMAIL_NOTIFICATION', $updateT_PSLIP, $whereT_PSLIP);

		$data_content .= '
		<tr>
			<td><font>'.++$counter.'</font></td>
			<td><font>'.$content['PS_NUMBER'].'</font></td>
			<td><font>'.Application_Helper_General::convertDate($content['PS_EFDATE'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
			<td><font>'.$content['PS_TYPE'].'</font></td>
			<td><font>'.$content['PS_STATUS'].'</font></td>
			<td><font> FIle upload has not received ACK response yet </font></td>
		</tr>';
	}
}
	
if($data_content != ''){	
	$translate = array( 
		'[[DATA_CONTENT]]' => $data_content,
		'[[FOOTER_ID]]' 		=> $FOOTER_ID,
		'[[FOOTER_EN]]' 		=> $FOOTER_EN,
		'[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
		'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
		'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl,
		'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
		'[[master_bank_fax]]' 	=> $templateEmailMasterBankFax,
		'[[master_bank_address]]' 	=> $templateEmailMasterBankAddress,
		'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,
		'[[master_bank_app_name]]' 	=> $templateEmailMasterBankApp,
		'[[master_bank_wapp]]' 	=> $templateEmailMasterBankWapp,	
	);
	
	$mailContent = strtr($template,$translate);
	
	if( count ($receiver)>0 ){
		foreach($receiver as $counter => $email){
			$status = Application_Helper_Email::sendEmail($email,$subject,$mailContent);
			echo "\n\n".$status.' Email sent to ';
			$mailedTo .= 'Email sent to '.$email.' with status : '.$status.';';
		}
	}
}

$filename = basename(__FILE__);
Application_Helper_General::cronLog($filename,$mailedTo,1);
?>