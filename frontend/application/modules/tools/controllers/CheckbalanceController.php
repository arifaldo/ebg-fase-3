<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
class tools_checkbalanceController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
	    $this->_helper->layout()->setLayout('popup');
	
	    $fields = array(
						'acct'  => array('field' => 'ACCT_NO',
											   'label' => $this->language->_('Account Number'),
											   'sortable' => true),
						'acct_name'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Account Name'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						'status'   => array('field'    => '',
											  'label'    => $this->language->_('Account Status'),
											  'sortable' => true),
						'effective'   => array('field'    => '',
											  'label'    => $this->language->_('Effective Balance'),
											  'sortable' => true)
				);
				
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
			
		$custInfo = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('C' => 'M_CUSTOMER'))
						->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				);
		$this->view->custname	= $custInfo['CUST_NAME'];
		$this->view->custadd	= $custInfo['CUST_ADDRESS'];
		$this->view->custcity	= $custInfo['CUST_CITY'];
		$this->view->custprov	= $custInfo['CUST_PROVINCE'];
		$this->view->custzip	= $custInfo['CUST_ZIP'];
		$this->view->custphone	= $custInfo['CUST_PHONE'];
		$this->view->custfax	= $custInfo['CUST_FAX'];
										  
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$data = $this->_db->select()
					 ->from(array('A' => 'M_CUSTOMER_ACCT'))
					 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
					 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					 ->where("A.ACCT_STATUS <> 3")
					 ->where("A.ACCT_STATUS <> 2")
					 ->order('B.GROUP_NAME DESC')
					 ->order('A.ORDER_NO ASC')
					 ->query()->fetchAll();
		
		//Zend_Debug::dump($data);die;
		
		$isGroup = false;
		if(count($data))
		{
			foreach($data as $row)
			{
				if($row['GROUP_ID'])
				{
					$isGroup = true;
				}
			}
		}
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$this->view->isGroup = $isGroup;
		$this->view->custId = $this->_custIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
		
	if($csv)
		{
			$arr = array();
			
			$header = array(
				'Account Number',
				'Account Alias',
				'Account Name',
				'CCY',
				'Account Status',
				'Effective Balance'
			);
			
			if(!$isGroup)
			{
				$arr[0] = $header;
				$i = 1;
				$arr_value_tot_group =  array();
				foreach($data as $row)
				{
					
					$systemBalance = new SystemBalance($this->custId,$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
					$systemBalance->checkBalance();
					
					
					if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
			          $arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
			        }
			        else {
			          $arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
			        }
					$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);
					
					$arr[$i][] = $row['ACCT_NO'];
					$arr[$i][] = $row['ACCT_ALIAS_NAME'];
					$arr[$i][] = $systemBalance->getCoreAccountName();
					$arr[$i][] = $row['CCY_ID'];
					$arr[$i][] = $systemBalance->getCoreAccountStatusDesc();
					$arr[$i][] = $systemBalance->getEffectiveBalance();
					$i++;
				}
				$arr[][] =  implode("; ",$arr_display_tot_group);	
			}
			else
			{
				$i = 0;
				$group = null;
				
				$resultdataGroup =  array();
				foreach ($data as $rowGroup){
					$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup; 
				}
				$i = 0;
				$group = null;
				$arr_value_tot_group = array();
				foreach ( $resultdataGroup as $groupname=>$rowPerGroup ) 
				{
					$i++;
					$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
					$groupname = ($groupname) ? $groupname : 'OTHERS';
					$arr[$i][] = $groupname;
					$i++;
					$arr[$i][] = $header;
					
						if(count($rowPerGroup) > 0){
							
							foreach ($rowPerGroup as $row){
							$i++;	
							$systemBalance = new SystemBalance($this->_custIdLogin,$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
							$systemBalance->checkBalance();
								
								if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
									$arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getEffectiveBalance();
								}else{
									$arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getEffectiveBalance();
								}
								$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);

								$arr[$i][] = $row['ACCT_NO'];
								$arr[$i][] = $row['ACCT_ALIAS_NAME'];
								$arr[$i][] = $systemBalance->getCoreAccountName();
								$arr[$i][] = $row['CCY_ID'];
								$arr[$i][] = $systemBalance->getCoreAccountStatusDesc();
								$arr[$i][] = $systemBalance->getEffectiveBalance();
								
							}
						}
						$i++;	
						$display_tot_group = implode("; ",$arr_display_tot_group);	
						$arr[$i][] = $this->language->_('TOTAL BALANCE'). ' : '.$display_tot_group;
					
				} 
										
			}
			
				
		
		}
		
	}
	
}
