<?php

require_once 'Zend/Controller/Action.php';

class Remittancebank_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$bankcode = $this->language->_('Bank Code');
    	$bankname = $this->language->_('Bank Name');
		$bankaddress1 = $this->language->_('Bank Address')."1";
		$bankaddress2 = $this->language->_('Bank Address')."2";
		$cityname = $this->language->_('City Name');
		$country = $this->language->_('Country');
		$pobnumber = $this->language->_('POB Number');
		$created = $this->language->_('Created Date');
		$createdby = $this->language->_('Created By');
		$updated = $this->language->_('Updated Date');
		$updatedby = $this->language->_('Updated By');

// 		$model = new customer_Model_Customer();
// 		echo 'here';die;
		$selectcountry = $this->_db->select()
		->from('M_COUNTRY')
		->query()->fetchAll();
// 	    $this->view->countryArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME'));
	    $this->view->countryArr = $selectcountry;


	    $fields = array(

						'bank_code'  => array('field' => 'bank_code',
										      'label' => $bankcode,
										      'sortable' => true),
						'bank_name'     => array('field' => 'bank_name',
											      'label' => $bankname,
											      'sortable' => true),
						'bank_address1'      => array('field' => 'bank_address1',
											      'label' => $bankaddress1,
											      'sortable' => true),
						'bank_address2'      => array('field' => 'bank_address2',
											      'label' => $bankaddress2,
											      'sortable' => true),
						'city_name'      => array('field' => 'city_name',
											      'label' => $cityname,
											      'sortable' => true),
						'country_name'      => array('field' => 'country_name',
												      'label' => $country,
												      'sortable' => true),
						'pob_number'      => array('field' => 'pob_number',
												      'label' => $pobnumber,
												      'sortable' => true),
						'created'      => array('field' => 'created',
												      'label' => $created,
												      'sortable' => true),
						'createdby'      => array('field' => 'createdby',
												      'label' => $createdby,
												      'sortable' => true),
						'updated'      => array('field' => 'updated',
												      'label' => $updated,
												      'sortable' => true),
						'updatedby'      => array('field' => 'updatedby',
												      'label' => $updatedby,
												      'sortable' => true),
				      );

	    $filterlist = array('BANK_CODE','BANK_NAME','COUNTRY');

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','bank_code');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'BANK_CODE'  => array('StringTrim','StripTags'),
							'BANK_NAME'      => array('StringTrim','StripTags'),
							'COUNTRY'     => array('StringTrim','StripTags')
		);

		$dataParam = array("BANK_CODE","BANK_NAME","COUNTRY");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		//$filter_lg = $this->_getParam('filter');

		//$tempfields = $fields;
		//unset($tempfields['country_name']);
		//$getData = Application_Helper_Array::SimpleArray($tempfields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_BANK_REMITTANCE'),array('bank_code','bank_name','bank_address1','bank_address2','city_name','country_name'=>'B.COUNTRY_NAME','pob_number','created','createdby','updated','updatedby','country_code'))
					        ->join(array('B' => 'M_COUNTRY'),'A.country_code = B.COUNTRY_CODE', array());


		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf)
		if($filter == true || $csv || $pdf || $this->_request->getParam('print'))
		{
			//$tempfields['country_name'] = $fields['country_name'];
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fbank_code = $zf_filter->getEscaped('BANK_CODE');
			$fbank_name     = $zf_filter->getEscaped('BANK_NAME');
			$fcountry    = $zf_filter->getEscaped('COUNTRY');

	        if($fbank_code) $select->where('UPPER(bank_code) LIKE '.$this->_db->quote('%'.strtoupper($fbank_code).'%'));
	        if($fbank_name)     $select->where('UPPER(bank_name) LIKE '.$this->_db->quote('%'.strtoupper($fbank_name).'%'));
	        if($fcountry)    $select->where('UPPER(A.country_code) LIKE '.$this->_db->quote('%'.strtoupper($fcountry).'%'));

			$this->view->bank_code = $fbank_code;
			$this->view->bank_name  = $fbank_name;
			$this->view->country    = $fcountry;
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);

		$select = $this->_db->fetchall($select);



		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {
	    	foreach($select as $key=>$value)
	    	{
	    		unset($select[$key]['country_code']);
	    	}
				$this->_helper->download->csv($header,$select,null,'REMITTANCE BANK');
				Application_Helper_General::writeLog('RBLS','Download CSV Remittance Bank');
		}
		else if($pdf)
		{
			foreach($select as $key=>$value)
			{
				unset($select[$key]['country_code']);
			}
				$this->_helper->download->pdf($header,$select,null,'REMITTANCE BANK');
				Application_Helper_General::writeLog('RBLS','Download PDF Remittance Bank');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Remittance Bank', 'data_header' => $fields));
       	}
		else
		{
				Application_Helper_General::writeLog('RBLS','View Remittance Bank');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }

	}

}
