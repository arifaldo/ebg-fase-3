<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class rdnaccount_RepairController extends customeraccount_Model_Customeraccount{

  public function indexAction(){
    
  
    $this->_helper->layout()->setLayout('popup');
    
    
    $this->view->custAcct_msg = array();
  	$changes_id = $this->_getParam('changes_id');
  	$changes_id = (Zend_Validate::is($changes_id,'Digits'))? $changes_id : 0;
  	$customer_view = 1; $error_remark = null; $key_value = null;
  	
  	
  	
  	//jika change id ada isinya maka true
  	if($changes_id)
  	{ 
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID','CHANGES_TYPE'))
                             ->where('CHANGES_ID='.$this->_db->quote($changes_id));
                             /* ->where('UPPER(CHANGES_STATUS)='.$this->_db->quote(strtoupper($this->_changeStatus['code']['waitingApproval'])).' OR UPPER(CHANGES_STATUS)='.$this->_db->quote(strtoupper($this->_changeStatus['code']['repairRequested'])))
                             //->where('UPPER(MODULE)='.$this->_db->quote(strtoupper($this->_request->getModuleName())))
                             ->where('UPPER(CHANGES_TYPE)='.$this->_db->quote(strtoupper($this->_changeType['code']['new'])).' OR UPPER(CHANGES_TYPE)='.$this->_db->quote(strtoupper($this->_changeType['code']['edit'])))
                             ->where('UPPER(CREATED_BY)='.$this->_db->quote((string)strtoupper($this->_userIdLogin)))
                             ->where("CHANGES_FLAG='B'");*/
      $resultdata = $this->_db->fetchRow($select);
      
      //jika change id ada di database maka true
      if($resultdata['CHANGES_ID'])
      {
         $select = $this->_db->select()
                               ->from(array('T' =>'TEMP_CUSTOMER_ACCT'))
                               ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS'))
      	                       ->where('T.CHANGES_ID = ?', $changes_id);
      	 $result = $this->_db->fetchRow($select);
      	 
      	 if($result['TEMP_ID'])
      	 {
      	   $change_type = strtoupper($resultdata['CHANGES_TYPE']);
      	   if($change_type==strtoupper($this->_changeType['code']['edit']))$key_value = strtoupper($result['CUST_ID']);
      	   
      	   
      	   
      	   $cust_id    = $result['CUST_ID'];
      	   $acct_email = $result['ACCT_EMAIL'];
      	   $acct_no    = $result['ACCT_NO'];
      	   $ccy_id     = $result['CCY_ID'];
      	   
      	   
      	 }
      	 else{ $changes_id = 0; }
      }
      else{ $changes_id = 0; } 
  	}
  	
  	//jika change id tidak ada isinya maka error
    if(!$changes_id)
    {
      $error_remark = $this->getErrorRemark('22','Suggestion ID');
      //insert log
	  try 
	  {
	  	$this->_db->beginTransaction();
	    if($this->_request->isPost())
	    {
	      if($this->_request->getPost('populate_cif')=='ya'){ $action = strtoupper($this->_actionID['popcif']); }
	  	  else{ $action = strtoupper($this->_actionID['repair']); }
	      $fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($_POST);
	    }
	    else
	    { 
	      $action = strtoupper($this->_actionID['view']); 
	      $fulldesc = null; 
	    }
		$this->backendLog($action,strtoupper($this->_moduleID['cust']),null,$fulldesc,$error_remark);
		$this->_db->commit();
	  }
      catch(Exception $e)
      {
 		$this->_db->rollBack();
		SGO_Helper_GeneralLog::technicalLog($e);
	  }
	  
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL); 	
    }
  	
    
    
    
  	if($this->_request->isPost())
  	{
  	   
  	     $filters = array('acct_no'        => array('StripTags','StringTrim','StringToUpper'),
                          'ccy_id'         => array('StripTags','StringTrim'),
                          'acct_email'     => array('StripTags','StringTrim'),
                          //'cust_id'        => array('StripTags','StringTrim','StringToUpper'),
                          );

         $validators =  array(//'cust_id'       => array(),
      
        				     'acct_no'       => array(),

                             'ccy_id'        => array(),
											                  
						  
                             'acct_email'    => array('NotEmpty',
                                                   // new Application_Validate_EmailAddress(),
											        'messages' => array(
                                                                       $this->language->_('Can not be empty'),
											                          // 'Invalid email format',
												                       )
												    ), 
                            );
  	   
  	
  	   
  	
         $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
      
		//validasi multiple email
      	if($zf_filter_input->acct_email)
	    {
			$validate = new validate;
			$cek_multiple_email = $validate->isValidEmailMultiple($zf_filter_input->acct_email);
	    }
         
      if($zf_filter_input->isValid() && $cek_multiple_email)
	  {
	  
	      $info = 'Cust ID = '.$zf_filter_input->cust_id.', Customer Account = '.$zf_filter_input->acct_no;
        
          $acct_data = $this->_acctData;
        
	      foreach($validators as $key=>$value)
	      {
	  	    if($zf_filter_input->$key)$acct_data[strtoupper($key)] = $zf_filter_input->$key;
	  	  }
	  	
          /*if($zf_filter_input->token_serialno){ $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['yes']); }
	  	  else{ $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['no']); }*/
	  	
	  	try 
	  	{
		  $this->_db->beginTransaction();
		  
		  //$acct_data['ACCT_STATUS'] = 1;
		  $acct_data['ACCT_SUGGESTED']    = new Zend_Db_Expr('now()');
		  $acct_data['ACCT_SUGGESTEDBY']  = $this->_userIdLogin;
		  
		
		  $this->updateGlobalChanges($changes_id,$info,null,null,null,$zf_filter_input->cust_id);
	  	  $this->updateTempCustomerAcct($changes_id,$acct_data);
	  	  
	  	  //log CRUD
		  Application_Helper_General::writeLog('ACAD','Bank Account has been Added, Acct No : '.$zf_filter_input->acct_no. ' Acct Name : '.$acct_data['ACCT_NAME'].' Change id : '.$change_id);
	  	  
          $this->_db->commit();
          
          $this->_redirect('/popup/successpopup');
	  	}
		catch(Exception $e){
		  $this->_db->rollBack();
		  $error_remark = $this->getErrorRemark('82');
		  SGO_Helper_GeneralLog::technicalLog($e);
		}
		  
		
	    //insert log
        try 
        {
	      $this->_db->beginTransaction();
	      $fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($_POST);
	      $this->backendLog(strtoupper($this->_actionID['repair']),strtoupper($this->_moduleID['cust']),$zf_filter_input->cust_id,$fulldesc,$error_remark);
	      $this->_db->commit();
	    }
        catch(Exception $e)
        {
 	      $this->_db->rollBack();
	      SGO_Helper_GeneralLog::technicalLog($e);
	    }
	      
	    if(isset($error_remark)){ $class = 'F'; $msg = $error_remark; }
		else
		{ 
		  $class = 'S';
		  $msg = $this->getErrorRemark('00','Customer ID',$zf_filter_input->cust_id);
		}
		
		$this->_helper->getHelper('FlashMessenger')->addMessage($class);
		$this->_helper->getHelper('FlashMessenger')->addMessage($msg);
        $this->_redirect($this->_backURL); 
        
	  }//END if($zf_filter_input->isValid())
	  else
	  { 
	    $this->view->error = 1;
	  	
	    $this->view->acct_email = ($zf_filter_input->isValid('acct_email'))? $zf_filter_input->acct_email : $this->_getParam('acct_email');
        
        $error = $zf_filter_input->getMessages();
        
		/*if(count($error))$error_remark = $this->displayErrorRemark($error);
	  	$this->view->user_msg = $this->displayError($error);*/
        
        //format error utk ditampilkan di view html 
        $errorArray = null;
        foreach($error as $keyRoot => $rowError)
        {
           foreach($rowError as $errorString)
           {
              $errorArray[$keyRoot] = $errorString;
           }
        }
        
        if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['acct_email'] = $this->language->_('Invalid format');
        
        $this->view->custAcct_msg  = $errorArray;
	  
        
		
		
	  }
    }//END if($this->_request->isPost())
    else 
    {
       $this->view->acct_email   = $acct_email;
       
    }
    
    
     
    $select = $this->_db->select()
  	                       ->from('M_CUSTOMER',array('CUST_NAME'))
  	                       ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
    $this->view->cust_name = $this->_db->fetchOne($select);
  	
    $this->view->acct_no      = $acct_no;
    $this->view->ccy_id       = $ccy_id;
    
    $this->view->changes_id = $changes_id;
    
    $this->view->modulename = $this->_request->getModuleName();
    
    
    
    //insert log
	try {
	  $this->_db->beginTransaction();
	  Application_Helper_General::writeLog('ACAD','Add Source Account');
	  $this->_db->commit();
	}
    catch(Exception $e){
 	  $this->_db->rollBack();
	  SGO_Helper_GeneralLog::technicalLog($e);
	}
  }
}