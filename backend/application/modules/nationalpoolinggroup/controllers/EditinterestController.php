<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class nationalpoolinggroup_EditinterestController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $np = $this->_getParam("ref");

        if($np){
            $npdata = $this->_db->select()
                                    ->from(array('A' => 'T_NOTIONAL_POOLING'),array('*'))
                                    ->join(array('D' => 'T_FILE_SUBMIT'), 'D.FILE_ID = A.N_UD_ID',array('D.DOCUMENT_NUMBER'))
                                    ->where('A.N_NUMBER = ?',$np)
                                    ->query()->fetchAll();

            if($npdata){
                $data = $npdata[0];

                $selectcomp = $this->_db->select()
                                    ->from(array('A' => 'M_CUSTOMER'),array('*'))
                                    ->where('A.CUST_ID = ?',$data['N_CUST_ID'])
                                    ->query()->fetchAll();

                $this->view->compinfo = $selectcomp[0];

                $this->view->N_NUMBER = $data['N_NUMBER'];
                $this->view->N_CUST_ID = $data['N_CUST_ID'];
                $this->view->N_CIF = $data['N_CIF'];
                $this->view->N_STATUS = $data['N_STATUS'];
                $this->view->N_DESC = $data['N_DESC'];
                $this->view->N_CREATEDBY = $data['N_CREATEDBY'];
                $this->view->N_CREATED = $data['N_CREATED'];
                $this->view->N_UD_ID = $data['N_UD_ID'];
                $this->view->doc_id = $data['N_UD_ID'];
                $this->view->exisdoc_underlying = $data['DOCUMENT_NUMBER'];
                 
                $select = $this->_db->select()
                            ->from(array('A' => 'T_NOTIONAL_DETAIL'),array('A.N_IS_SAVING','A.N_IS_CREDIT','A.ACCT_NAME','A.ACCT_DESC','A.N_SOURCE_ACCOUNT','A.ACCT_NAME'))
                            ->join(array('D' => 'T_NOTIONAL_POOLING'), 'D.N_NUMBER = A.N_NUMBER',array('D.N_CUST_ID','D.N_STATUS','N_NUMBER'))
                            ->join(array('B' => 'M_PRODUCT_TYPE'), 'B.PRODUCT_CODE = A.PRODUCT_TYPE AND A.PRODUCT_PLAN = B.PRODUCT_PLAN',array('B.PRODUCT_RATE','B.PRODUCT_TYPE'))
                            ->join(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = D.N_CUST_ID',array('C.CUST_NAME','C.CUST_ID','C.CUST_CIF'))
                            ->where('A.N_NUMBER = ?',$np);
                            
    //echo $select;die;
                $dataacct                   = $this->_db->fetchAll($select);
                $this->view->dataacct = $dataacct;
                
                
                if($this->_request->isPost())
                {
                    
                
                    $adapter = new Zend_File_Transfer_Adapter_Http();
                    $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

                    $adapter->setDestination ( $this->_destinationUploadDir );
                    $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'pdf'));
                    $extensionValidator->setMessage(
                        $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv and *.pdf'
                    );

                    $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                    $sizeValidator->setMessage(
                        'Error: File exceeds maximum size'
                    );

                    $adapter->setValidators ( array (
                        $extensionValidator,
                        $sizeValidator,
                    ));
                    $adapter->setOptions(array('ignoreNoFile'=>true));
            
                    if ($adapter->isValid ())
                    {
                    
                        $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                        if(empty($adapter->getFileName ())){
                            $newFileName = '';
                        }else{
                            $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
                        // var_dump($sourceFileName);
                        }
                        
                        // var_dump($this->_request->getParams());die;
                        $adapter->addFilter ( 'Rename',$newFileName  );

                        if ($adapter->receive ())
                        {
                        //die('here');
                            $id = $this->_getParam("ref");
                            $select = $this->_db->select()
                                    ->from(array('A' => 'T_NOTIONAL_POOLING'),array('*'))                      
                                    ->where('A.N_NUMBER = ?',$id);
                            $datapool                   = $this->_db->fetchAll($select);
                            $insertArr = $datapool['0'];
                            
                            $change_id = $this->suggestionWaitingApproval('Notional Pooling',$info,$this->_changeType['code']['edit'],null,'T_NOTIONAL_POOLING','TEMP_NOTIONAL_POOLING',$selectcomp[0]['CUST_ID'],$selectcomp[0]['CUST_NAME'],$selectcomp[0]['CUST_ID'],$selectcomp[0]['CUST_NAME']);
                            $insertArr['CHANGES_ID'] = $change_id;
                            $insertArr['NOTE'] =  $this->_getParam("deleteNotes");
                            $insertArr['N_STATUS'] =  '0';
                            $insertArr['N_UD_ID'] =  $this->_getParam("doc_id");
                            $insertArr['N_UD_FILE'] =  $newFileName;
                            $insertArr['N_CREATED'] = new Zend_Db_Expr('now()');
                            $insertArr['N_CREATEDBY'] = $this->_userIdLogin;
                            unset($insertArr['N_ID']);
                            unset($insertArr['N_UPDATED']);
                            unset($insertArr['N_UPDATEDBY']);
                            //var_dump($insertArr);
                            //die('here');
                            $insert = $this->_db->insert('TEMP_NOTIONAL_POOLING',$insertArr);
                            
                            $detail = $this->_db->select()
                                                           ->from('T_NOTIONAL_DETAIL')
                                                           ->where($this->_db->quoteInto('N_NUMBER = ?',$id))
                                                           ->query()
                                                           ->fetchAll(Zend_Db::FETCH_ASSOC);
                            foreach($detail as $key => $val){
                                $insertDetail = $detail[$key];
                                $insertDetail['CHANGES_ID'] = $change_id;
                                $tagsaving = $val['N_SOURCE_ACCOUNT'].'saving';
                                $insertDetail['N_IS_SAVING'] = '0';
                                if(!empty($this->_getParam($tagsaving))){
                                    $insertDetail['N_IS_SAVING'] = '1';
                                }
                                $tagcredit = $val['N_SOURCE_ACCOUNT'].'credit';
                                $insertDetail['N_IS_CREDIT'] = '0';
                                if(!empty($this->_getParam($tagcredit))){
                                    $insertDetail['N_IS_CREDIT'] = '1';
                                }
                                //$issaving = 
                                $insert = $this->_db->insert('TEMP_NOTIONAL_DETAIL',$insertDetail);
                            }
                            
                            $rowUpdated = $this->_db->update(
                                'T_GLOBAL_CHANGES',
                                array(
                                    'CHANGES_INFORMATION' => 'Request Repair',
                                ),
                                $this->_db->quoteInto('CHANGES_ID = ?', $change_id)
                            );
                            $this->setbackURL('/'.$this->_request->getModuleName().'/index');
                            $this->_redirect('/notification/submited/index');
                        }
                    
                    }
                }
            }
        }

    }

}