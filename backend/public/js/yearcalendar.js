// <!-- This script and many more are available free online at -->
// <!-- The JavaScript Source!! http://javascript.internet.com -->
// <!-- heavily modified from the free version j.wilkinson 3/17/00 -->
// <!-- Begin
var fs = 1;
var bgborder = "#FFFFCC";
var bghead = "#FF9900";
var bgheadday = "#FFCC00";
var bgday = "#FFFFFF";
var bgholiday = "#FF9999";

// check browser being used
var BrName = navigator.appName;
// var BrVers = navigator.appVersion;
// BrVers = BrVers.substring(0,1); // or 0,4  could return 4.5 instead of just 4

if (BrName == "Microsoft Internet Explorer") 
	UseOuterTable = 0;
else // netscape?
	UseOuterTable = 1;


function day_title(day_name){
	document.write("<TD ALIGN=center WIDTH='14%'"
 		+ " BGCOLOR='" + bgheadday + "'>"
		+ "<font size='" + fs + "'>" + day_name + "</font></TD>")
}

function indexInArray(arr,val){
for(var i=0;i<arr.length;i++) if(arr[i]==val) return i;
return -1;
}

function processdate(datetext,obj)
{
	var val=document.getElementById('dates');
	if(val.value=='')
		arrayval=new Array();
	else
		arrayval=val.value.split('~|~');
		
	var desc=document.getElementById('datesdesc');

	if(desc.value=='')
		arraydesc=new Array();
	else
		arraydesc=desc.value.split('~|~');	
		
	var temp = document.getElementById(obj);
	
	var position=indexInArray(arrayval,datetext)
	if( position >= 0 )
	{
		temp.style.backgroundColor=bgday;
		arrayval.splice(position,1);
		arraydesc.splice(position,1);
	}
	else
	{
		prompttext=prompt('Enter Holiday Description','');

		if(prompttext!==null && prompttext!==""){
		temp.style.backgroundColor=bgholiday;
		arrayval.push(datetext);
		arraydesc.push(prompttext);
		temp.title=prompttext;
		}
	}
	
	var joined=arrayval.join('~|~');
	val.value=joined;	
	
	var descjoined=arraydesc.join('~|~');
	desc.value=descjoined;	
	
	// alert('New val.value : '+ val.value);
	// alert('New desc.value : '+ desc.value);
}

function processdateccy(datetext,obj)
{
	var val=document.getElementById('datesccy');
	if(val.value=='')
		arrayval=new Array();
	else
		arrayval=val.value.split('~|~');
		
	var desc=document.getElementById('datesdescccy');

	if(desc.value=='')
		arraydesc=new Array();
	else
		arraydesc=desc.value.split('~|~');	
	
	var ccyid=document.getElementById('ccyid');

	if(ccyid.value=='')
		arrayccy=new Array();
	else
		arrayccy=ccyid.value.split('~|~');	
		
	var temp = document.getElementById(obj);
	
	var position=indexInArray(arrayval,datetext)
	if( position >= 0 )
	{
		temp.style.backgroundColor=bgday;
		arrayval.splice(position,1);
		arrayccy.splice(position,1);
		arraydesc.splice(position,1);
	}
	else
	{

		$('#holidaycal').modal('show');
		// $('#value1').val('');
		// $('#value2').val('');
		$('#datetext').val(datetext);
		$('#obj').val(obj);
		// $('#holidaycal').on('hidden.bs.modal', function () {
		// 	$('#value1').val( $('#prompttextccy').val() );
		// 	$('#value2').val( $('#prompttext').val() );
		// 	$('#datetext').val(datetext);
			// prompttextccy = $('#value1').val();
			// prompttext=$('#value2').val();
			
			// if(prompttext!==null && prompttext!=="" && prompttextccy!==null && prompttextccy!==""){
			// temp.style.backgroundColor=bgholiday;
			// arrayval.push(datetext);
			// arrayccy.push(prompttextccy);
			// arraydesc.push(prompttext);
			// temp.title=prompttext;
			// }

			// var joined=arrayval.join('~|~');
			// val.value=joined;	
			
			// var descjoined=arraydesc.join('~|~');
			// desc.value=descjoined;	
			
			// var ccyjoined=arrayccy.join('~|~');
			// ccyid.value=ccyjoined;
		// });
		// $('#prompttext').val('');
		// prompttextccy = $('#value1').val();
		// prompttext=$('#value2').val();
		
		// if(prompttext!==null && prompttext!=="" && prompttextccy!==null && prompttextccy!==""){
		// temp.style.backgroundColor=bgholiday;
		// arrayval.push(datetext);
		// arrayccy.push(prompttextccy);
		// arraydesc.push(prompttext);
		// temp.title=prompttext;
		// }
		// $('#value1').val('');
		// $('#value2').val('');
	}
	
	var joined=arrayval.join('~|~');
	val.value=joined;	
	
	var descjoined=arraydesc.join('~|~');
	desc.value=descjoined;	
	
	var ccyjoined=arrayccy.join('~|~');
	ccyid.value=ccyjoined;	
	
	// alert('New val.value : '+ val.value);
	// alert('New desc.value : '+ desc.value);
}

function init(datearray,descarray)
{
	for(i=0;i<datearray.length;i++)
	{
		var temp=document.getElementById(datearray[i]);
		temp.style.backgroundColor=bgholiday;
		temp.title=descarray[i];
	}
}

function fill_table(month,month_length,year,clickfunction) {
	// test for ie vs netscape here, netscape needs nested tables for border colors
	// begin outer table, print month row 
	if( UseOuterTable == 1 )
		document.write("<TABLE BORDER=0 cellspacing=0 cellpadding=0><tr><td"
			+ " BGCOLOR='" + bgborder + "'>");

	var month_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	
	var month_name=month_names[month];
	
	day=1
	document.write("<TABLE BORDER=1 CELLSPACING=2"
		+ " BGCOLOR='" + bgborder + "'><TR>")
	document.write("<TD COLSPAN=7 ALIGN=center"
 		+ " BGCOLOR='" + bghead + "'><b>"
		+ "<font size='" + fs + "'>" 
		+ month_name + "   " + year+"</font></b><TR>")
	day_title("Sun")
	day_title("Mon")
	day_title("Tue")
	day_title("Wed")
	day_title("Thu")
	day_title("Fri")
	day_title("Sat")
	document.write("</TR><TR>")
	
	if(start_day<8)
	for (var i=1;i<start_day;i++) {
		document.write("<TD></TD>")
	}
	
	for (var i=start_day;i<8;i++) {
		var datetext=year+'-'+(month+1)+'-'+day;
		var dateid=String(year)+'x'+String(month+1)+'x'+String(day);
		if(i==1 || i==7)
		{
		document.write("<TD id='"+dateid+"' ALIGN=center"
	 		+ " BGCOLOR='" + bgholiday + "'>"
			+ "<font size='" + fs + "'>" + day + "</font></TD>");
		}
		else
		{
		document.write("<TD id='"+dateid+"' ALIGN=center"
	 		+ " BGCOLOR='" + bgday + "'>"
			+ "<a size='" + fs + "' href='javascript:processdate(\""+datetext+"\",\""+dateid+"\");'>" + day + "</a></TD>");
		}
		day++;
	}
	document.write("</TR>")
	while (day <= month_length) {
		document.write("<TR>")
		for (var i=1;i<=7 && day<=month_length;i++) {
			var datetext=year+'-'+(month+1)+'-'+day;
			var dateid=String(year)+'x'+String(month+1)+'x'+String(day);
			if(i==1 || i==7)
			{
			document.write("<TD id='"+dateid+"' ALIGN=center" 
		 		+ " BGCOLOR='" + bgholiday + "'>"
				+ "<font size='" + fs + "'>" + day + "</font></TD>");
			}
			else
			{
			document.write("<TD id='"+dateid+"' ALIGN=center" 
		 		+ " BGCOLOR='" + bgday+ "'>"
				+ "<a size='" + fs + "' href='javascript:processdate(\""+datetext+"\",\""+dateid+"\");'>" + day + "</a></TD>");
			}
			day++;
		}
		document.write("</TR>")
		start_day=i
	}
	document.write("</TABLE>")

	// test for ie vs netscape here, netscape needs nested tables for border colors
	if( UseOuterTable == 1 )
		document.write("</TD></TR></TABLE>"); // end outer table
}

//without click function
function fill_table_nofunc(month,month_length,year) {
	// test for ie vs netscape here, netscape needs nested tables for border colors
	// begin outer table, print month row 
	if( UseOuterTable == 1 )
		document.write("<TABLE BORDER=0 cellspacing=0 cellpadding=0><tr><td"
			+ " BGCOLOR='" + bgborder + "'>");

	var month_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	
	var month_name=month_names[month];
	
	day=1
	document.write("<TABLE BORDER=1 CELLSPACING=2"
		+ " BGCOLOR='" + bgborder + "'><TR>")
	document.write("<TD COLSPAN=7 ALIGN=center"
 		+ " BGCOLOR='" + bghead + "'><b>"
		+ "<font size='" + fs + "'>" 
		+ month_name + "   " + year+"</font></b><TR>")
	day_title("Sun")
	day_title("Mon")
	day_title("Tue")
	day_title("Wed")
	day_title("Thu")
	day_title("Fri")
	day_title("Sat")
	document.write("</TR><TR>")
	
	if(start_day<8)
	for (var i=1;i<start_day;i++) {
		document.write("<TD></TD>")
	}
	
	for (var i=start_day;i<8;i++) {
		var datetext=year+'-'+(month+1)+'-'+day;
		var dateid=String(year)+'x'+String(month+1)+'x'+String(day);
		if(i==1 || i==7)
		{
		document.write("<TD id='"+dateid+"' ALIGN=center"
	 		+ " BGCOLOR='" + bgholiday + "'>"
			+ "<font size='" + fs + "'>" + day + "</font></TD>");
		}
		else
		{
		document.write("<TD id='"+dateid+"' ALIGN=center"
	 		+ " BGCOLOR='" + bgday + "'>"
			+ "<a size='" + fs + "' href='#'>" + day + "</a></TD>");
		}
		day++;
	}
	document.write("</TR>")
	while (day <= month_length) {
		document.write("<TR>")
		for (var i=1;i<=7 && day<=month_length;i++) {
			var datetext=year+'-'+(month+1)+'-'+day;
			var dateid=String(year)+'x'+String(month+1)+'x'+String(day);
			if(i==1 || i==7)
			{
			document.write("<TD id='"+dateid+"' ALIGN=center" 
		 		+ " BGCOLOR='" + bgholiday + "'>"
				+ "<font size='" + fs + "'>" + day + "</font></TD>");
			}
			else
			{
			document.write("<TD id='"+dateid+"' ALIGN=center" 
		 		+ " BGCOLOR='" + bgday+ "'>"
				+ "<a size='" + fs + "' href='#'>" + day + "</a></TD>");
			}
			day++;
		}
		document.write("</TR>")
		start_day=i
	}
	document.write("</TABLE>")

	// test for ie vs netscape here, netscape needs nested tables for border colors
	if( UseOuterTable == 1 )
		document.write("</TD></TR></TABLE>"); // end outer table
}

function fill_table_nofuncccy(month,month_length,year) {
	// test for ie vs netscape here, netscape needs nested tables for border colors
	// begin outer table, print month row 
	if( UseOuterTable == 1 )
		document.write("<TABLE BORDER=0 cellspacing=0 cellpadding=0><tr><td"
			+ " BGCOLOR='" + bgborder + "'>");

	var month_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	
	var month_name=month_names[month];
	
	day=1
	document.write("<TABLE BORDER=1 CELLSPACING=2"
		+ " BGCOLOR='" + bgborder + "'><TR>")
	document.write("<TD COLSPAN=7 ALIGN=center"
 		+ " BGCOLOR='" + bghead + "'><b>"
		+ "<font size='" + fs + "'>" 
		+ month_name + "   " + year+"</font></b><TR>")
	day_title("Sun")
	day_title("Mon")
	day_title("Tue")
	day_title("Wed")
	day_title("Thu")
	day_title("Fri")
	day_title("Sat")
	document.write("</TR><TR>")
	
	if(start_dayccy<8)
	for (var i=1;i<start_dayccy;i++) {
		document.write("<TD></TD>")
	}
	
	for (var i=start_dayccy;i<8;i++) {
		var datetext=year+'-'+(month+1)+'-'+day;
		var dateid=String(year)+'x'+String(month+1)+'x'+String(day);
		if(i==1 || i==7)
		{
		document.write("<TD id='"+dateid+"' ALIGN=center"
	 		+ " BGCOLOR='" + bgholiday + "'>"
			+ "<font size='" + fs + "'>" + day + "</font></TD>");
		}
		else
		{
		document.write("<TD id='"+dateid+"' ALIGN=center"
	 		+ " BGCOLOR='" + bgday + "'>"
			+ "<a size='" + fs + "' href='#'>" + day + "</a></TD>");
		}
		day++;
	}
	document.write("</TR>")
	while (day <= month_length) {
		document.write("<TR>")
		for (var i=1;i<=7 && day<=month_length;i++) {
			var datetext=year+'-'+(month+1)+'-'+day;
			var dateid=String(year)+'/'+String(month+1)+'/'+String(day);
			if(i==1 || i==7)
			{
			document.write("<TD id='"+dateid+"' ALIGN=center" 
		 		+ " BGCOLOR='" + bgholiday + "'>"
				+ "<font size='" + fs + "'>" + day + "</font></TD>");
			}
			else
			{
			document.write("<TD id='"+dateid+"' ALIGN=center" 
		 		+ " BGCOLOR='" + bgday+ "'>"
				+ "<a size='" + fs + "' href='#'>" + day + "</a></TD>");
			}
			day++;
		}
		document.write("</TR>")
		start_dayccy=i
	}
	document.write("</TABLE>")

	// test for ie vs netscape here, netscape needs nested tables for border colors
	if( UseOuterTable == 1 )
		document.write("</TD></TR></TABLE>"); // end outer table
}

// End -->

function fill_tableccy(month,month_length,year,clickfunction) {
	// test for ie vs netscape here, netscape needs nested tables for border colors
	// begin outer table, print month row 
	if( UseOuterTable == 1 )
		document.write("<TABLE BORDER=0 cellspacing=0 cellpadding=0><tr><td"
			+ " BGCOLOR='" + bgborder + "'>");

	var month_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	
	var month_name=month_names[month];
	
	day=1
	document.write("<TABLE BORDER=1 CELLSPACING=2"
		+ " BGCOLOR='" + bgborder + "'><TR>")
	document.write("<TD COLSPAN=7 ALIGN=center"
 		+ " BGCOLOR='" + bghead + "'><b>"
		+ "<font size='" + fs + "'>" 
		+ month_name + "   " + year+"</font></b><TR>")
	day_title("Sun")
	day_title("Mon")
	day_title("Tue")
	day_title("Wed")
	day_title("Thu")
	day_title("Fri")
	day_title("Sat")
	document.write("</TR><TR>")
	
	if(start_dayccy<8)
	for (var i=1;i<start_dayccy;i++) {
		document.write("<TD></TD>")
	}
	
	for (var i=start_dayccy;i<8;i++) {
		var datetext=year+'-'+(month+1)+'-'+day;
		var dateid=String(year)+'x'+String(month+1)+'x'+String(day);
		if(i==1 || i==7)
		{
		document.write("<TD id='"+dateid+"' ALIGN=center"
	 		+ " BGCOLOR='" + bgholiday + "'>"
			+ "<font size='" + fs + "'>" + day + "</font></TD>");
		}
		else
		{
		document.write("<TD id='"+dateid+"' ALIGN=center"
	 		+ " BGCOLOR='" + bgday + "'>"
			+ "<a size='" + fs + "' href='javascript:processdateccy(\""+datetext+"\",\""+dateid+"\");'>" + day + "</a></TD>");
		}
		day++;
	}
	document.write("</TR>")
	while (day <= month_length) {
		document.write("<TR>")
		for (var i=1;i<=7 && day<=month_length;i++) {
			var datetext=year+'-'+(month+1)+'-'+day;
			var dateid=String(year)+'/'+String(month+1)+'/'+String(day);
			if(i==1 || i==7)
			{
			document.write("<TD id='"+dateid+"' ALIGN=center" 
		 		+ " BGCOLOR='" + bgholiday + "'>"
				+ "<font size='" + fs + "'>" + day + "</font></TD>");
			}
			else
			{
			document.write("<TD id='"+dateid+"' ALIGN=center" 
		 		+ " BGCOLOR='" + bgday+ "'>"
				+ "<a size='" + fs + "' href='javascript:processdateccy(\""+datetext+"\",\""+dateid+"\");'>" + day + "</a></TD>");
			}
			day++;
		}
		document.write("</TR>")
		start_dayccy=i
	}
	document.write("</TABLE>")

	// test for ie vs netscape here, netscape needs nested tables for border colors
	if( UseOuterTable == 1 )
		document.write("</TD></TR></TABLE>"); // end outer table
}
