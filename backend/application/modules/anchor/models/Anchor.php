<?php

Class anchor_Model_Anchor extends Application_Main
{
   
    protected $_moduleDB  = 'ANC';
    
	// constructor
    public function initModel()
    { 
        
    }
    
    public function getMinamtCcy()
    {
         $select =  $this->_db->select()
						      ->from(array('M_MINAMT_CCY'), array('CCY_ID', 'CCY_NUM', 'DESCRIPTION'))
						      ->where("M_MINAMT_CCY.CCY_STATUS=?", $this->_masterStatus['code']['active'])
						      ->order('CCY_ID ASC')
						      ->query()->fetchAll();
	    return $select;
    }
    
    public function getAnchor($anchorId)
    {
        $select = $this->_db->select()
							->from(array('A' => 'M_ANCHOR'))
							->where("A.ANCHOR_ID=?", $anchorId)
							->query()->fetch();
        return $select;
    }
    
    public function getAnchorCust2()
    {
       $select = $this->_db->select()
			               ->from('M_ANCHOR', array('ANCHOR_CUST'))
						   ->query()->fetchAll();
	   return $select;
    }
    
    public function getAnchorLimit($anchorId)
    {
        $select = $this->_db->select()
							->from(array('A' => 'M_ANCHOR_LIMIT'), array('CCY_ID', 'LIMIT'))
							->where("A.ANCHOR_ID=?", $anchorId)
							->order('CCY_ID ASC')
							->query()->fetchAll();
       return $select;
    }
    
    public function getCustomer($anchorInfo)
    {
        $select = $this->_db->select()
						    ->from('M_CUSTOMER',array('CUST_NAME', 'CUST_CIF', 'TITLE_BEFORENAME', 'TITLE_AFTERNAME'))
							->where('CUST_ID = ?',(string)$anchorInfo['ANCHOR_CUST'])
						    ->query()->fetch();
	   return $select;				
    }
    
    public function getCustomer2()
    {
       $select = $this->_db->select()
						->from('M_CUSTOMER', array('CUST_ID', 'CUST_NAME', 'CUST_CIF', 'TITLE_BEFORENAME', 'TITLE_AFTERNAME'))
						->where("M_CUSTOMER.CUST_STATUS=?", $this->_masterStatus['code']['active'])
						->order('CUST_CIF ASC')
						->query()->fetchAll();
	   return $select;	
    }
    
    public function getCustomer3($anchor)
    {
       $select = $this->_db->select()
						  ->from('M_CUSTOMER', array('CUST_ID', 'CUST_NAME', 'CUST_CIF', 'TITLE_BEFORENAME', 'TITLE_AFTERNAME'))
						  ->where("M_CUSTOMER.CUST_STATUS=?", $this->_masterStatus['code']['active'])
						  ->where("M_CUSTOMER.CUST_ID=?", $anchor)
						  ->query()->fetchAll();
	   return $select;	
    }
    
    public function getRootCom($anchorInfo)
    {
       $select = $this->_db->select()
				           ->from('M_ROOT_COMM',array('ROOT_NAME','ROOT_CODE'))
				           ->where('ROOT_ID = ?',$anchorInfo['ROOT_ID'])
						   ->query()->fetch();
       return $select;
    }

    public function getRootCom2()
    {
        $select = $this->_db->select()
						    ->from(array('M_ROOT_COMM'), array('ROOT_NAME', 'ROOT_ID', 'ROOT_CODE'))
						    ->order('ROOT_NAME ASC')
						    ->query()->fetchAll();
		return $select;
    }
    
    public function getTempAnchor($anchorId)
    {
        $select = $this->_db->select()
						    ->from(array('A' => 'TEMP_ANCHOR'))
						    ->where("A.ANCHOR_ID=?", $anchorId)
						    ->query()->fetchAll();
		return $select;
    }
    
    public function getBusinessScheme()
    {
        $select = $this->_db->select()
						    ->from(array('M_BUSINESS_SCHEME'), array('BUSS_SCHEME_CODE', 'BUSS_SCHEME_NAME'))
						    ->order('ORDER_NO ASC')
						    ->query()->fetchAll();
        return $select;
    }
    
    public function getAnchorCust($anchorId)
    {
       $select = $this->_db->select()
							->from(array('A' => 'M_ANCHOR'))
							->join(array('C' => 'M_CUSTOMER'), 'C.CUST_ID=A.ANCHOR_CUST' , array('CUST_ID', 'CUST_NAME', 'CUST_CIF', 'TITLE_BEFORENAME', 'TITLE_AFTERNAME','CUST_STATUS'))
							->where("A.ANCHOR_ID=?", $anchorId)
							->query()->fetch();
       return $select;
    }
    
    public function insertTempAnchor($changesId,$anchorId,$anchorInfo,$status)
    {
    	$this->_db->insert('TEMP_ANCHOR', 
									array(
										'CHANGES_ID'=>$changesId,
										'ANCHOR_ID'=> $anchorId,
										'ANCHOR_CUST' => $anchorInfo['ANCHOR_CUST'],
										'ROOT_ID' => $anchorInfo['ROOT_ID'],
										'ANCHOR_STATUS' => $status,
							));
    }
    
    public function insertTempAnchorLimit($changesId,$anchorId,$limitCorporationCCY,$limitCorporation)
    {
       $this->_db->insert('TEMP_ANCHOR_LIMIT', 
									         array(
										           'CHANGES_ID'=>$changesId,
										           'ANCHOR_ID'=> $anchorId,
										           'CCY_ID' => $limitCorporationCCY,
										           'LIMIT' => $limitCorporation,
								                   ));
    } 
    
    public function getMemberId($anchorId)
    {
       $select = $this->_db->select()
						   ->from(array('S' => 'M_SCHEME'), array())
						   ->join(array('C' => 'M_COMMUNITY'), 'C.SCHEME_ID=S.SCHEME_ID', array())
						   ->join(array('M' => 'M_MEMBER'), 'M.COMM_ID=C.COMM_ID', array('MEMBER_ID'))
						   ->where("S.ANCHOR_ID = ?", $anchorId)
						   ->query()->fetchAll();
		return $select;
    } 
    
}




