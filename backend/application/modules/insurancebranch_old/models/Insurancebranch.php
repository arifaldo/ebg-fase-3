<?php

class insurancebranch_Model_Insurancebranch
{
	protected $_db;

	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getData($filterParam = null, $filter = null)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'M_INS_BRANCH'),
				array('*')
			)
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			);

		if ($filter == TRUE) {
			if ($filterParam['fInsBranchCode']) {
				$select->where('B.CUST_NAME LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchCode'] . '%'));
			}
			if ($filterParam['fInsBranchName']) {
				$select->where('A.INS_BRANCH_NAME LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchName'] . '%'));
			}
			if ($filterParam['fInsBranchAcct']) {
				$select->where('A.INS_BRANCH_ACCT LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchAcct'] . '%'));
			}
			if ($filterParam['fInsBranchSuggest']) {
				$select->where('A.LAST_SUGGESTEDBY LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchSuggest'] . '%'));
			}
			if ($filterParam['fInsBranchApprove']) {
				$select->where('A.LAST_APPROVEDBY LIKE ' . $this->_db->quote('%' . $filterParam['fInsBranchApprove'] . '%'));
			}
		}
		return $this->_db->fetchAll($select);
	}

	public function getDataById($id)
	{
		$select = $this->_db->select()
			->from(array('A' => 'M_INS_BRANCH'), array('*'))
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			)
			->where('A.INS_BRANCH_CODE = ?', $id);
		return $this->_db->fetchRow($select);
	}

	public function getCustomer()
	{
		$select = $this->_db->select()
			->from(array('M_CUSTOMER'), array('CUST_ID', 'CUST_NAME'))
			->where('CUST_MODEL = ?', 2)
			->where('CUST_STATUS = ?', 1)
			->query()->fetchAll();
		return $select;
	}

	public function getCustomerById($cust_id)
	{
		$select = $this->_db->select()
			->from(array('M_CUSTOMER'), array('CUST_ID', 'CUST_NAME'))
			->where('CUST_ID = ?', $cust_id)
			->query()->fetchRow();
		return $select;
	}

	public function getCustAcctById($cust_id)
	{
		$select = $this->_db->select()
			->from(
				array('M_CUSTOMER_ACCT'),
				array('ACCT_NO', 'ACCT_NAME', 'CCY_ID', 'ACCT_DESC')
			)
			->where('CUST_ID = ?', $cust_id);
		return $this->_db->fetchAll($select);
	}

	public function getCustAcctByAcctNo($acct_no)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'M_CUSTOMER_ACCT'),
				array('ACCT_NO', 'ACCT_NAME', 'CCY_ID', 'ACCT_DESC')
			)
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			)
			->where('A.ACCT_NO = ?', $acct_no);
		return $this->_db->fetchRow($select);
	}

	public function getTempData($id)
	{
		$select = $this->_db->select()
			->from(array('A' => 'TEMP_INS_BRANCH'), array('*'))
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			)
			->where('A.INS_BRANCH_CODE = ?', $id);
		return $this->_db->fetchRow($select);
	}

	public function generateCode()
	{
		$lastID = [];
		$select = $this->_db->select()
			->from(
				array('M_INS_BRANCH'),
				array('MAX_ID' => new Zend_Db_Expr('MAX(INS_BRANCH_CODE)'))
			);
		$data = $this->_db->fetchRow($select);
		$lastID[] = $data['MAX_ID'];

		$select = $this->_db->select()
			->from(
				array('TEMP_INS_BRANCH'),
				array('MAX_ID' => new Zend_Db_Expr('MAX(INS_BRANCH_CODE)'))
			);
		$data = $this->_db->fetchRow($select);
		$lastID[] = $data['MAX_ID'];
		$maxID = max($lastID);
		$n = ((int)$maxID) + 1;
		$code = sprintf("%'.03d", $n);
		return $code;
	}
}
