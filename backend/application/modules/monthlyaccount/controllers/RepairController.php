<?php


require_once 'Zend/Controller/Action.php';


class monthlyaccount_RepairController extends Application_Main
{
	public function indexAction() 
	{	
		$changeid = $this->_getParam('changes_id');
		
		$select2 = $this->_db->select()->distinct()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
		$select2 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$result2 = $this->_db->fetchRow($select2);
		$custname = $result2['KEY_VALUE'];
		$custid = $result2['KEY_FIELD'];
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$select -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $select->query()->FetchAll();
		$this->view->result = $result;
		
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		/*$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));*/
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		//$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		//$selectprk = $selectprk->__toString();
		//$selectnoprk = $selectnoprk->__toString();
		//$selectnoprk2 = $selectnoprk2->__toString();
		
		//$unionquery = $this->_db->select()
								//->union(array($selectprk,$selectnoprk));
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		//$selectunion = $this->_db->select()
		//					->from (($unionquery),array('*'));
		//$resultunion = $selectunion->query()->FetchAll();
		//$this->view->resultaccount = $resultunion;
		//Zend_Debug::dump($resultunion);die;
		
		/*$chargeaccount = $this->_db->select()
								->union(array($selectnoprk2,$selectnoprk));*/
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;
		
		$monthlyfeestatus = $result[0]["CUST_MONTHLYFEE_STATUS"];
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = $this->language->_('Disabled');
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = $this->language->_('Enabled');
		}     
		
		foreach($resultunion as $row)
		{	
			$acctno = $row['ACCT_NO'];
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'TEMP_CHARGES_MONTHLY'),array('*'));
			$select4 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($row['ACCT_NO']));
			$select4 -> where("MONTHLYFEE_TYPE = '3'");
			$select4 -> order("TRA_FROM ASC");
			$result4 = $select4->query()->FetchAll();
			//Zend_Debug::dump($result4);die;
			$i = 0;
			$x = 0;
			if($result4)
			{
				foreach($result4 as $row2)
				{	
					$idfrom = $row2['ACCT_NO'].'from'.$i;
   					$idto = $row2['ACCT_NO'].'to'.$i;
   					$idamt = $row2['ACCT_NO'].'amount'.$i;
					$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
					$cekacct = 'cek'.$acctno; 
					
					if($row2['TRA_FROM'] != 0 && $row2['TRA_TO'] != 0)
					{
						$this->view->$idfrom = $row2['TRA_FROM'];
						$this->view->$idto = $row2['TRA_TO'];
					}
					
					$this->view->$idamt = $amt;
					$this->view->$cekacct = $row2['CHARGES_ACCT_NO'];
					$i++;
				}
			}
			$x++;
		}	
		
		Application_Helper_General::writeLog('CHCR','Repair company charges');
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
			if($submit)
			{
				Application_Helper_General::writeLog('CHUD','Repairing Monthly transaction account charges page ('.$changeid.')');
					$error = 0;
					$arraycekisi = array();
					foreach($resultunion as $row)
					{
						$cekisi = 0;
						$cekisi2 = 0;
						$acctno = $row['ACCT_NO'];
						$cekacct = 'cek'.$acctno;
						$rowmax = 0;
						
						for($i=0;$i<5;$i++)
						{
						$z = 0;
						$idamt = $row['ACCT_NO'].'amount'.$i;
						$idfrom = $row['ACCT_NO'].'from'.$i;
   						$idto = $row['ACCT_NO'].'to'.$i;
   						//Zend_Debug::dump($idamt); die;
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						$temp_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
						$this->view->$idamt = $cekamt;
						
						$cekfrom = $this->_getParam($idfrom);
						$cek_angka2 = (Zend_Validate::is($cekfrom,'Digits')) ? true : false;
						$this->view->$idfrom = $cekfrom;
						
						$cekto = $this->_getParam($idto);
						$cek_angka3 = (Zend_Validate::is($cekto,'Digits')) ? true : false;
						$this->view->$idto = $cekto;
						
						$cekcekacct = $this->_getParam($acctno);
   						$cek_angka4 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   						$this->view->$cekacct = $cekcekacct;
   						//Zend_Debug::dump($cekfrom); die;
   						
   						$selectcekcur = $this->_db->select()
					        			->from (($selectprk),array('*'));
						$selectcekcur -> where("ACCT_NO LIKE ".$this->_db->quote($cekcekacct));
						$cekcur = $this->_db->fetchRow($selectcekcur);
   						
						if($cek_angka && $cek_angka2 && $cek_angka3)
						{
							if ($cekfrom >= $cekto && $cekfrom != '0' && $cekto != '0')
							{
								$errid = 'err'.$idfrom;
								$err = $this->language->_('From Range has to be less than To Range');
								$this->view->$errid = $err;
								$error++;
								$z++;
								//die;
							}
							
							if ($cekfrom == '0' && $cekto != '0')
							{
								$errid = 'err'.$idfrom;
								$err = $this->language->_('Range value can not start from 0');
								$this->view->$errid = $err;
								$error++;
								$z++;
								//die;
							}
							
							for($j=$i;$j>=0;$j--)
							{
								if($i != $j)
								{
									$fromerror = false;
									$toerror = false;
									$idfromcek = $row['ACCT_NO'].'from'.$j;
   									$idtocek = $row['ACCT_NO'].'to'.$j;
   									$fromcek = $this->_getParam($idfromcek);
   									$tocek = $this->_getParam($idtocek);
   									
   									if($fromcek && $tocek)
   									{
   										if($fromcek <= $cekfrom && $cekfrom <= $tocek)
   										{
   											$fromerror = true;
   											//echo $cekfrom;die;
   										}
   									
										if($fromcek <= $cekto && $cekto <= $tocek)
   										{
   											$toerror = true;
   										}
   									
   										if($fromerror == true || $toerror == true)
   										{
   											$errid = 'err'.$idto.$j;
											$err = $this->language->_('Value overlap not allowed (overlap with : ').''.$fromcek.'-'.$tocek.')';
											$this->view->$errid = $err;
											$error++;
											$z++;
											
   										}
   									}	
								}
							}
						}
						
						if($amt != null)
						{
							if($cek_angka == false)
							{
								$errid = 'err'.$idamt;
								$err = $this->language->_('Amount value has to be numeric');
								$this->view->$errid = $err;
								$error++;
								$z++;
								//die;
							}
						}
   						
						if($cek_angka2 && $cek_angka3 && $cekfrom != '0' && $cekto != '0')
							{
								if($cekamt == null || $cekamt == '0.00')
								{
									$errid = 'err'.$idamt;
									$err = $this->language->_('Amount value has to be set for this range');
									$this->view->$errid = $err;
									$error++;
									$z++;
									//die;
								}
							}
							
						if($cekcekacct)
						{
								if($cekcur['CCY_ID'] != $row['CCY_ID'])
								{
									$errid = 'err'.$cekacct;
									$err = $this->language->_('Account currency must be same');
									$this->view->$errid = $err;
									$error++;
								}
						}
						
						if($cek_angka && $cekamt != '0.00')
						{
								if($cek_angka2 == false || $cek_angka3 == false)
								{
									$errid = 'err'.$idfrom;
									$err = $this->language->_('From and To value has to be numeric');
									$this->view->$errid = $err;
									$error++;
									$z++;
									//die;
								}
								
								if($cekfrom == null || $cekto == null)
								{
									$errid = 'err'.$idfrom;
									$err = $this->language->_('From and To Range should not be empty while the amount is filled');
									$this->view->$errid = $err;
									$error++;
									$z++;
									//die;
								}
						}
						
							if(!$cekfrom && !$cekto && ($amt == null || $amt == '0.00'))
							{
								$cekisi2++;
							}
						
							if($z > 0)
							{
								$rowmax++;
							}
						
						$cekisi++;
						}
						
						if($cekisi == $cekisi2)
						{
							$temparray = array($acctno);
							$arraycekisi = array_merge($arraycekisi,$temparray);
						}
						
						if($cekisi != $cekisi2)
						{
							if(!$cekcekacct)
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account should be choosen');
								$this->view->$errid = $err;
								$error++;
								//die;
							}
							if($cekcekacct)
							{
								if($cek_angka4 == false)
								{
									$errid = 'err'.$cekacct;
									$err = $this->language->_('Account value has to be numeric');
									$this->view->$errid = $err;
									$error++;
								}
							}
						}
						
						$rowmaxid = 'rowmax'.$row['ACCT_NO'];
						$this->view->$rowmaxid = $rowmax;
					}
					//Zend_Debug::dump($error); die;
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
				    //Zend_Debug::dump($cek_angka2); die;
					$info = $this->language->_('Charges');
					$info2 = $this->language->_('Set Charges Monthly per Account');
					
					$this->updateGlobalChanges($changeid,$info);
				
					$where = array('CHANGES_ID = ?' => $changeid);
					$this->_db->delete('TEMP_CHARGES_MONTHLY',$where);
					
					$x = 0;
					foreach($resultunion as $row)
					{
						$cekacct = $row['ACCT_NO'];
						
						$view = 'list'.$x;
						$this->view->$view = $this->language->_('checked');
						
						for($i=0;$i<6;$i++)
						{
							$idamt = $row['ACCT_NO'].'amount'.$i;
							$idfrom = $row['ACCT_NO'].'from'.$i;
   							$idto = $row['ACCT_NO'].'to'.$i;
   						
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
							$cekfrom = $this->_getParam($idfrom);
						
							$cekto = $this->_getParam($idto);
						
							$cekcekacct = $this->_getParam($cekacct);
						
							if($cekamt && $cekfrom && $cekto)
							{
						
								$data= array(
										'CHANGES_ID' 		=> $changeid,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $cekacct,
										'TRA_FROM' 			=> $cekfrom,
										'TRA_TO' 			=> $cekto,
										'AMOUNT' 			=> $amt,
										'MONTHLYFEE_TYPE' 	=> '3',
										'CHARGES_ACCT_NO'	=> $cekcekacct
										);
								$this->_db->insert('TEMP_CHARGES_MONTHLY',$data);										
								//echo 'aaa';die;
							}
						}
						$x++;
					}
					
					if($arraycekisi)
					{
						foreach($arraycekisi as $array)
						{				
							$cekcekacct = $this->_getParam($array);
								
							$data2= array(
										'CHANGES_ID' 		=> $changeid,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $array,
										'TRA_FROM' 			=> '0',
										'TRA_TO' 			=> '0',
										'AMOUNT' 			=> '0.00',
										'MONTHLYFEE_TYPE' 	=> '3',
										'CHARGES_ACCT_NO'	=> $cekcekacct
										);
							$this->_db->insert('TEMP_CHARGES_MONTHLY',$data2);				
						}
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->_redirect('/popup/successpopup/index');
					}
				}
				
				catch(Exception $e)
				{
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}		
		}
		else
		{
			Application_Helper_General::writeLog('CHUD','View Repair Monthly transaction account charges page ('.$changeid.')');
		}
	}
}
