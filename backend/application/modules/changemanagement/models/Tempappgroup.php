<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempappgroup extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'SBL';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		
		//query from TEMP_APP_GROUP_USER
		$appGroup = $this->dbObj->select()
						  	->from('TEMP_APP_GROUP_USER')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!empty($appGroup)){
        	// $this->_errorCode = '22';
			// $this->_errorMsg = 'Query failed(Approve Group User)';
			// return false;
			$where['CUST_ID = ?'] = $appGroup[0]['CUST_ID'];
			$this->dbObj->delete('M_APP_GROUP_USER',$where);
        }

		if(is_array($appGroup))
		{
			//delete from M_APP_GROUP_USER
			foreach ($appGroup as $row) 
			{
				$insertArr = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$appinsert = $this->dbObj->insert('M_APP_GROUP_USER',$insertArr);
				//update record
				if(!(boolean)$appinsert) {
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Approve Group User)';
					return false;
				}
			} 	
		}				 
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) {
		//will never be called
	}
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_APP_GROUP_USER
		$appdelete = $this->dbObj->delete('TEMP_APP_GROUP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() {
	}
}