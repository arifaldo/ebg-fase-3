<?php	

class agent_DeleteController extends Application_Main
{

  public function indexAction() 
  {
	$user_id = strtoupper($this->_getParam('user_id'));
    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
    $error_remark = null;
	
    if($user_id)
    {
    	$model = new agent_Model_Agent();
    	$user_data = $model->getUserData(array('user_id'=>$user_id));

		if(!empty($user_data))
		{
			$result = $model->getTempUserFromID($user_id);

			if(empty($result))
			{
				$agentData = $model->getAgentData($user_id);
	            $info = 'User ID = '.$user_id.', User Name = '.$user_data['USER_FULLNAME'];
	            
	            $agentData['USER_STATUS'] = 3;
				$agentData['USER_SUGGESTED']    = new Zend_Db_Expr('GETDATE()');
				$agentData['USER_SUGGESTEDBY']  = $this->_userIdLogin;
	            try 
	            {
			      $this->_db->beginTransaction();
			  	  $change_id = $this->suggestionWaitingApproval('Agent',$info,strtoupper($this->_changeType['code']['delete']),null,'M_USER_LKP','TEMP_USER_LKP',$user_id,$user_data['USER_FULLNAME'],$user_id);
				  $model->insertTempAgent($change_id,$agentData);
				  
				  //log CRUD
				  //Application_Helper_General::writeLog('CSUD','User has been Updated (delete), User ID : '.$user_id. ' User Name : '.$user_data['USER_FULLNAME'].' Change id : '.$change_id);
				  $this->_db->commit();
				  $this->setbackURL('/agent/view/index/user_id/'.$user_id);
				  $this->_redirect('/notification/submited/index');
			    }
			    catch(Exception $e) 
			    {
				  $this->_db->rollBack();
				  $errorMsg = 'Exception';
			    }
			}
			else{
				$errorMsg = 'Tidak diperbolehkan ada perubahan sementara menunggu persetujuan perubahan sebelumnya.';
			}
	    }
	    else{
	    	$errorMsg = 'Data Agen tidak ditemukan.';
	    	$user_id = null;
	    }
    }
    else{
    	$errorMsg = 'ID Agen tidak ditemukan.';
    }

	
	//insert log
	//Application_Helper_General::writeLog('CSSP','Unsuspend');

		if($errorMsg){
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

			if($user_id == null)
				$this->_redirect('/agent/agentlist');
			else
				$this->_redirect('/agent/view/index/user_id/'.$user_id);
		}
  }
}
