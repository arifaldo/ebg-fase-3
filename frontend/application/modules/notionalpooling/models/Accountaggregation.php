<?php
class notionalpooling_Model_Accountaggregation
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function convertCurrency($amount, $from = 'EUR', $to = 'USD'){
		if (empty($_COOKIE['exchange_rate'])) {
			$Cookie = new Cookie($_COOKIE);
			$curl = file_get_contents_curl('http://api.fixer.io/latest?symbols='.$from.','.$to.'');
			$rate = $curl['rates'][$to];
			$Cookie->exchange_rate = $rate;
		} else {
			$rate = $_COOKIE['exchange_rate'];
		}
		$output = round($amount * $rate);
	
		return $output;
	}

	public function getUser($userId)
	{
		$result = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('C' => 'M_USER'))
					->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
	}

	public function getUserAccount($param,$sorting)
	{
		$select = $this->_db->select()
			->from(array('A' => 'M_USER_ACCT'))
			->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
			->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
			->where("A.ACCT_STATUS = 1")
			->where("C.MAXLIMIT > 0");
		if(isset($param['userId'])) $select->where("C.USER_LOGIN = ".$this->_db->quote($param['userId']));
		if(isset($param['acctNo'])) $select->where("C.ACCT_NO = ".$this->_db->quote($param['acctNo']));

		if(strpos($sorting,'ACCT_NO') === false){
		}
		else{
			$sorting = str_replace('ACCT_NO','A.ACCT_NO',$sorting);
		}

		$select->order($sorting);

		//echo $select;die;
		$result = $this->_db->fetchAll($select);
		return $result;
	}

	public function getProduct($plan, $code)
	{
		$data = $this->_db->select()
		->from(array('A' => 'M_PRODUCT_TYPE'),array('PRODUCT_CODE','PRODUCT_NAME'))
		->where('PRODUCT_CODE = ?' , $code)
		->where('PRODUCT_PLAN = ?' , $plan)
		->query()
		//print_r($data);die;
		->FetchAll();
	
		return $data;
	}

	public function getCustAccount($param)
	{
		$select = $this->_db->select()
		->from(array('A' => 'M_CUSTOMER_ACCT'))
//		->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
		//->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
		->where("A.ACCT_STATUS = 1");
//		->where("C.MAXLIMIT > 0");
//		if(isset($param['userId'])) $select->where("C.USER_LOGIN = ".$this->_db->quote($param['userId']));
		if(isset($param['custId'])) $select->where("A.CUST_ID = ".$this->_db->quote($param['custId']));
		if(isset($param['acctNo'])) $select->where("C.ACCT_NO = ".$this->_db->quote($param['acctNo']));
		//if(isset($param['acctType'])) $select->where("A.ACCT_TYPE = ".$param['acctType']);
//		$select->order('C.USER_LOGIN ASC');
//		echo $select; die;
		 //		print_r($select->query());die;
		$result = $this->_db->fetchAll($select);
		return $result;
	}
}