<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once "Service/Account.php";
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/Settings.php';

class user_RepairController extends user_Model_User{

  public function indexAction(){


    $this->_helper->layout()->setLayout('popup');

    $this->view->user_msg  = array();

    $setting = new Settings();          
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
    $pw_hash = md5($enc_salt.$enc_pass);
    $rand = $this->_userIdLogin.date('dHis').$pw_hash;
    $sessionNamespace->token  = $rand;
    $this->view->token = $sessionNamespace->token;

    $getprivilege = $this->getprivilege();

    $changes_id = $this->_getParam('changes_id');
    $changes_id = (Zend_Validate::is($changes_id,'Digits'))? $changes_id : 0;
    $customer_view = 1; $error_remark = null; $key_value = null;



    //jika change id ada isinya maka true
    if($changes_id)
    {

              $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        // $cif = $resultdata['CUST_CIF'];

    


      $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID','CHANGES_TYPE'))
                             ->where('CHANGES_ID='.$this->_db->quote($changes_id));
      $resultdata = $this->_db->fetchRow($select);


       

      //jika change id ada di database maka true
      if($resultdata['CHANGES_ID'])
      {
         $result = $this->getTempUser($changes_id);

         // print_r($result);die;

         if($result['TEMP_ID'])
         {
           $change_type = strtoupper($resultdata['CHANGES_TYPE']);
       $this->view->changes_type = $change_type;
           if($change_type==strtoupper($this->_changeType['code']['edit']))$key_value = strtoupper($result['CUST_ID']);

           $cust_id = $result['CUST_ID'];
           $user_id = $result['USER_ID'];

           $selectccy = $this->_db->select()
            ->from('M_MINAMT_CCY',array('CCY_ID','CCY_NUM'))
            ->where('CCY_ID IN ("IDR","USD")');

            $ccylist = $this->_db->fetchAll($selectccy);
            $this->view->ccylist = $ccylist;
            $this->view->ccylist = $ccylist;


            //data makerlimit
            $select = $this->_db->select()
                      ->from(array('M_MAKERLIMIT'),array('*'))
                      ->where("USER_LOGIN = ?", $user_id)
                      ->where('CUST_ID = '.$this->_db->quote($cust_id).' OR CUST_ID = '.$this->_db->quote(BANK).'');
                      
            $makerLimitData = $this->_db->fetchAll($select);

            foreach ($makerLimitData as $key => $value) {
              $newMakerLimitData[$value['ACCT_NO']] = $value['MAXLIMIT'];
            }

            $this->view->makerLimitData = $newMakerLimitData;

            //data temp_makerlimit
            $select = $this->_db->select()
                      ->from(array('TEMP_MAKERLIMIT'),array('*'))
                      ->where("CHANGES_ID = ?", $changes_id);
                      
            $tempMakerLimitData = $this->_db->fetchAll($select);

            foreach ($tempMakerLimitData as $key => $value) {
              $newTempMakerLimitData[$value['ACCT_NO']] = $value['MAXLIMIT'];
            }

            $this->view->tempMakerLimitData = $newTempMakerLimitData;

            //data dailylimit
            $select = $this->_db->select()
                      ->from(array('M_DAILYLIMIT'),array('*'))
                      ->where("USER_LOGIN = ?", $user_id)
                      ->where('CUST_ID = '.$this->_db->quote($cust_id).' OR CUST_ID = '.$this->_db->quote(BANK).'');
                      
            $dailyLimitData = $this->_db->fetchAll($select);

            foreach ($dailyLimitData as $key => $value) {
              $newDailyLimitData[$value['CCY_ID']] = $value['DAILYLIMIT'];
            }

            $this->view->dailyLimitData = $newDailyLimitData;

            //data dailylimit
            $select = $this->_db->select()
                      ->from(array('TEMP_DAILYLIMIT'),array('*'))
                      ->where("CHANGES_ID = ?", $changes_id);
                      
            $tempDailyLimitData = $this->_db->fetchAll($select);

            foreach ($tempDailyLimitData as $key => $value) {
              $newTempDailyLimitData[$value['CCY_ID']] = $value['DAILYLIMIT'];
            }

            $this->view->tempDailyLimitData = $newTempDailyLimitData;



         }
         else{ $changes_id = 0; }
      }
      else{ $changes_id = 0; }

        $result = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_STATUS','CUST_CIF','CUST_LIMIT_USD','CUST_LIMIT_IDR'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             //->where('CUST_STATUS=1');
                             ->query()->fetch();

      if(!$result)$cust_id = null;
      else{
        $companyLim = array();
        $companyLim['IDR'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_IDR']);
        $companyLim['USD'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_USD']);
    $this->view->limitidr = $result['CUST_LIMIT_IDR'];
    $this->view->limitusd = $result['CUST_LIMIT_USD'];
      }


        $core = array();
    $svcAccount = new Service_Account($result['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
    $result   = $svcAccount->cifaccountInquiry($core);

    
    if($core->responseCode == '0000' || $result['ResponseCode']=='0000' ){
          $arrResult = array();
          $err = array();
          
          if(count($core->accountList) > 1){
//            print_r($core->accountList);
            //array data lebih dari 1
            foreach ($core->accountList as $key => $val){
              if($val->status == '0'){  
                $arrResult = array_merge($arrResult, array($val->accountNo => $val));
                array_push($err, $val->accountNo);
              }
//              echo 'here';
            }
          }
          else{
            //array kurang dari 2
            foreach ($core as $data){
              if($data->status == '0'){ 
                $arrResult = array_merge($arrResult, array($data->accountNo => $data));
                array_push($err, $data->accountNo);
              }
//              echo 'here1';
//            Zend_Debug::dump($data);
            }
          }
       
        
        $final = $arrResult;
        if(count($final) < 1){
          $this->view->data1 = '0';
        }
        else{
          $this->view->data = $arrResult;
        }
        
//        $dataPro = array();
        $dataProPlan = array('xxxxx');

          foreach ($final as $dataProductPlan){
            // print_r($dataProductPlan);die;
              if($dataProductPlan->status == '0'){  

                if (isset($dataProductPlan->productType))
                {
                $select_product_type  = $this->_db->select()
                      ->from(array('M_PRODUCT_TYPE'),array('PRODUCT_NAME'))
        //              ->where('PRODUCT_CODE = ?','10');
        //              ->where("PRODUCT_CODE IN(?)", $dataPro)
                      // ->where("PRODUCT_CODE = ?", $dataProductPlan->productPlan)
                      ->where("PRODUCT_CODE = ?", $dataProductPlan->productType);
        //              ->where('PRODUCT_PLAN = ?','62');
                      // echo $select_product_type;
                $userData_product_type = $this->_db->fetchAll($select_product_type);
  //              
                // print_r($userData_product_type);
                $dataProductPlan->planName = $userData_product_type[0]['PRODUCT_NAME'];
                // $dataProductPlanOri = (($dataProductPlan->planName == null)?'':$dataProductPlan);
                }else{
                  $dataProductPlan = $dataProductPlan; 
                }             
              
              // print_r($dataProductPlan);die;
             $dataUser[] = $dataProductPlan;
              // $dataUser[] = $dataProductPlanOri;
//              Zend_Debug::dump($dataProductPlan);
              }
          }

     }
     // print_r($dataUser);
     $this->view->data = $dataUser;
     $this->view->param = $paramSession;

      
    }

    // print_r($cust_id);die;

    //jika change id tidak ada isinya maka error
    if(!$changes_id)
    {
      $error_remark = $this->language->_('Changes ID is not found');
      //insert log
      Application_Helper_General::writeLog('RULC','Repair User List');
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
    }

    if($this->_request->isPost())
    {

         $exclude_fgroup_id = '(UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id);
         $exclude_fgroup_id .= ' OR UPPER(CUST_ID)='.$this->_db->quote('BANK');
         $exclude_fgroup_id .= ') AND UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active']));

      //get privilege id from html
       $priviId    = array();
       $filterPrivilege    = array();
       $validatorPrivilege = array();
    $privcheck = false;
       foreach($getprivilege as $privi)
       {
           $priviParam = $this->_getParam($privi['FPRIVI_ID']);
    //  var_dump($priviParam);
           
           if($priviParam == 'yes')
           {
            $privcheck = true;
             $priviId[] = strip_tags( trim($privi['FPRIVI_ID']) );
             $filterPrivilege[ $privi['FPRIVI_ID'] ]    = array('StripTags','StringTrim');
             $validatorPrivilege[ $privi['FPRIVI_ID'] ] = array('NotEmpty',
                                        array('StringLength',array('min'=>1,'max'=>3)),
                                  'messages' => array($this->language->_('Can not be empty'),
                                              $this->language->_('invalid format'))
                                        );
           }
        }


      $filters = array('user_id'        => array('StripTags','StringTrim','StringToUpper'),
                        'user_email'     => array('StripTags','StringTrim'),
                        'user_fullname'  => array('StripTags','StringTrim'),
                        'user_phone'     => array('StripTags','StringTrim'),
                        'user_ext'             => array('StripTags','StringTrim'),
                        'token_id'             => array('StripTags','StringTrim'),
                        'user_isemail'         => array('StripTags','StringTrim'),

                        'cust_id'        => array('StripTags','StringTrim','StringToUpper'),

                      );


      //merge filter data user with privilege
      $filters = array_merge($filters,$filterPrivilege);
      $expid = "USER_ID != '".$this->_db->quote($user_id);

      $validators =  array('user_id'        => array(),
                           'cust_id'        => array(),

                           'user_email'     => array(//new Application_Validate_EmailAddress(),
                               array('StringLength',array('max'=>128)),
                               'NotEmpty',
                               'messages' => array(
                                                                         $this->language->_('Email lenght cannot be more than').' 128',
                                         $this->language->_('Can not be empty'),
                                                )
                            ),

                           'user_fullname'      => array('NotEmpty',
                               array('StringLength',array('min'=>1,'max'=>50)),
                             'messages' => array($this->language->_('Can not be empty'),
                                                 $this->language->_('invalid format'))
                              ),

               'user_phone'     => array( //array('StringLength',array('min'=>1,'max'=>20)),
                               'Digits',
                               'allowEmpty' => true,
                             'messages' => array(
                                                   $this->language->_('Invalid phone number format'),
                                                   )
                            ),

                         'user_ext'         => array('Digits',
                               array('StringLength',array('min'=>1,'max'=>20)),
                               'allowEmpty' => true,
                             'messages' => array(
                                                  $this->language->_('Must be numeric values'),
                                                  )
                            ),

//               'token_id' => array('Digits',
//                         array('StringLength',array('min'=>1,'max'=>13)),
//                         'allowEmpty' => true,
//                         'messages' => array('invalid format',
//                                             'invalid format'
//                                             )
//                        ),
              'token_id'        => array(
                'allowEmpty' => true,
                                'Alnum',
                                array('StringLength',array('min'=>1,'max'=>13)),
                                array('Db_NoRecordExists',array('table'=>'M_USER','field'=>'TOKEN_ID')),
                            //    array('Db_NoRecordExists',array('table'=>'TEMP_USER','field'=>'TOKEN_ID','exclude'=>$expid)),
                           //     new Zend_Validate_Callback(
      //            array(
      //              'callback'  => array('SGO_Validate_Callback', 'checkTokenSerial'),
                         //               'options' => array($cust_id,$user_id)
      //            )
      //          ),
                'messages' => array(
                                  $this->language->_('Invalid Token Id Format'),
                                    $this->language->_('Invalid Token Id Format'),
                                    $this->language->_('Token Id already in use. Please use another')
                                //    $this->language->_('Token Id already suggested. Please use another')
        //          $this->language->_('Token Id is invalid')
                                )
              ),
              'user_isemail'   => array(
                array(
                  'StringLength',
                  array(
                    'min'=>1,
                    'max'=>25
                  )
                ),
                'allowEmpty' => true,
                'messages' => array($this->language->_('invalid format'))
              ),
            );


    //merge validator data user with privilege
        $validators = array_merge($validators,$validatorPrivilege);

       $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
       //print_r($zf_filter_input->user_email);die;
       $cek_multiple_email = true;
     //validasi multiple email
      if($zf_filter_input->user_email)
      {

        $validate = new validate;
        $cek_email = $validate->isValidEmail($zf_filter_input->user_email);
        
        $select = $this->_db->select()
                           ->from('M_USER',array('USER_EMAIL','USER_ID'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('USER_EMAIL = ?', $this->_getParam('user_email'))
                           ->where('USER_STATUS != 3');

        $data = $this->_db->fetchRow($select);
        
        $checkDuplicateEmail = false;
        if (empty($data)) {
      
          $checkDuplicateEmail = true;
        }else{
          if($data['USER_ID'] == $user_id){
              $checkDuplicateEmail = true;
          }
        }

      }

      $errorLim = false;
      foreach($ccylist as $key => $value){

            $limittag = 'daily'.(string)$value['CCY_ID'];
            $dailylimLeft = $companyLim[$value['CCY_ID']] ;
            if(empty($dailylimLeft)){
              $dailylimLeft = 0;
            }
            $dailylimit = $this->_getParam($limittag);
            $dailylimit = preg_replace("/([^0-9\\.])/i", "", $dailylimit);
          if($dailylimit != '0.00' && $dailylimit != ''){
        //var_dump($dailylimit);
        //var_dump($dailylimLeft);
               if((int)$dailylimit > (int)$dailylimLeft){

        
                  $errorLim = true;
                  $error_remark = "Maximum Amount of Daily Limit in ".$value['CCY_ID']." is ".Application_Helper_General::displayMoney($dailylimLeft);
              }
          }

      }
  
    //var_dump($cek_multiple_email);
    //var_dump($privcheck);
    //var_dump($errorLim);die;
  
      if($zf_filter_input->isValid() && $cek_email  && $privcheck && !$errorLim && $checkDuplicateEmail)
    {
//die('here');
       $info = 'User ID = '.$user_id.', User Name = '.$zf_filter_input->user_fullname;

         $user_data = array('CUST_ID'       => null,
                           'USER_ID'        => null,
                       'USER_FULLNAME'  => null,
                   'USER_PASSWORD'  => null,
                           'USER_EMAIL'     => null,
                           'USER_PHONE'     => null,
                           'USER_STATUS'    => null,
                           'USER_EXT'       => null,
                           'TOKEN_ID'       => null,
                           'USER_ISEMAIL'     => null,
                            );

      foreach($validators as $key=>$value)
      {
        if($zf_filter_input->$key)$user_data[strtoupper($key)] = $zf_filter_input->$key;
      }

      //jika tidak diisi maka bernilai = 0
        if(is_null($user_data['USER_ISEMAIL']) || $user_data['USER_ISEMAIL'] == '' || $user_data['USER_ISEMAIL'] == 0)
      {
         $user_data['USER_ISEMAIL'] = 0;
      }



      //$user_data['USER_STATUS'] = 1;
        $user_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
    $user_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;

    $user_data['USER_ID'] = $user_id;


       try
      {
      $this->_db->beginTransaction();

      //update T_GLOBAL_CHANGES
      $this->updateGlobalChanges($changes_id,$info,null,null,null,null,$cust_id);

      //cek apakah temp fprivi user sebelumnya ada isinya atau tidak, jika ada maka delete temp tersebut
        $tempFpriviUser = $this->getTempFpriviUser($changes_id);
          if(count($tempFpriviUser) > 0)
        {
         $this->deleteTempFpriviUser($changes_id);
        }

        //cek apakah ada privilege yang dicentang, jika ada maka insert privilege tersebut
      $count_privi = count($priviId);
      if($count_privi > 0)  $this->insertTempPrivilege($changes_id,$priviId,$user_data);

      //update TEMP_USER
      $this->updateTempUser($changes_id,$user_data);
  //var_dump($user_data);die;
    $this->deleteTempMaker($changes_id);
      $this->deleteTempDaily($changes_id);
      $this->deleteTempCustacct($changes_id);
      
       $active = json_decode(json_encode($dataUser), true);
     //echo '<pre>';
    //var_dump($active);die;
      $b=0;
      $final1 = array();          
      foreach ($active as $arr => $value){
            $final1[$b++] = 
                  array('accountNo'=>$value['accountNo'],
                    'accountName'=>$this->_request->getParam('accountName'.$value['accountNo']),
                    // 'email'=>$this->_request->getParam('email'.$value),
                    'ccy'=>$this->_request->getParam('ccy'.$value['accountNo']),
                    'productType'=>$this->_request->getParam('productType'.$value['accountNo']),
                    // 'planCode'=>$this->_request->getParam('planCode'.$value),
                    'productName'=>$this->_request->getParam('productName'.$value['accountNo']),
                    'acct_desc'=>$this->_request->getParam('acct_desc'.$value['accountNo'])
                  ); 
      }

          // print_r($final1);die();

          // for($i=1;$i<=$total;$i++){
            foreach ($final1 as $key => $value) {
              # code...
            // }
            $accountNo = $value['accountNo'];
            $accountName = $value['accountName'];
            $email = '';
            $ccy = $value['ccy'];
            $productType = $value['productType'];
            $acct_desc = $value['acct_desc'];
            

            $acct_source = 3;     //1 = prk scm 
          //$acct_desc      = $acct_desc;
            
            $info = 'Account No = '.$accountNo;
              $acct_data['ACCT_STATUS']      = 1;     //1 = unsuspend
          $acct_data['ACCT_SUGGESTED']   = new Zend_Db_Expr('GETDATE()');
          $acct_data['ACCT_SUGGESTEDBY'] = $this->_userIdLogin;
          //data from core
          $acct_data['ACCT_NAME']   = $accountName;
          $acct_data['ACCT_ALIAS_NAME'] = $accountName;
              $acct_data['ACCT_NO'] = $accountNo;
              $acct_data['CUST_ID'] = $cust_id;
              $acct_data['CCY_ID'] = $ccy;
              $acct_data['ACCT_DESC'] = $acct_desc;
              $acct_data['ACCT_TYPE'] = $productType;
              $acct_data['ACCT_EMAIL'] = $email;
              $acct_data['ACCT_SOURCE'] = $acct_source;
                
        try{
              $acct_data['CHANGES_ID'] = $changes_id;
            $insert = $this->_db->insert('TEMP_CUSTOMER_ACCT',$acct_data);
            $limittag = 'limit'.(string)$accountNo;
            $maxlimit = $this->_getParam($limittag);
      $data = array();
      if($maxlimit != '0.00' && $maxlimit != ''){
            $data = array('CHANGES_ID' => $changes_id,
                    'USER_LOGIN' => $user_id,
                    'ACCT_NO' => (string)$accountNo,
                    'CUST_ID' => $this->_custIdLogin,
                    'MAXLIMIT' => Application_Helper_General::convertDisplayMoney($maxlimit),
                    'SUGGESTED' => new Zend_Db_Expr('now()'), 
                    'SUGGESTEDBY' => $this->_userIdLogin, 
                    'MAKERLIMIT_STATUS' => 1
                  );
          //echo '<pre>';
         // var_dump($data);
            $this->_db->insert('TEMP_MAKERLIMIT',$data);
      
      }
      
              }catch(Exception $e)
        {
    //  echo '<pre>';
      //var_dump($e);die;
        }

      }
    
  //  die;
      
       foreach($ccylist as $key => $value){
          $dailydata = array();
          $dailydata['CHANGES_ID'] = $changes_id;
          $dailydata['USER_LOGIN'] = $user_id;
          $dailydata['CCY_ID'] = $value['CCY_ID'];
          $dailydata['CUST_ID'] = $this->_custIdLogin;
          
          $limittag = 'daily'.(string)$value['CCY_ID'];
                $dailylimit = $this->_getParam($limittag);
          if($dailylimit != '0.00' && $dailylimit != ''){
              $dailydata['DAILYLIMIT'] = Application_Helper_General::convertDisplayMoney($dailylimit);
              $dailydata['DAILYLIMIT_STATUS'] = '1';
              
              $dailydata['SUGGESTED'] = new Zend_Db_Expr('now()');
              $dailydata['SUGGESTEDBY'] = $this->_userIdLogin;
              $this->_db->insert('TEMP_DAILYLIMIT',$dailydata);
          }
          
          
        }

      //log CRUD
      Application_Helper_General::writeLog('CSCR','User has been Repaired, User ID : '.$zf_filter_input->user_id. ' User Name : '.$zf_filter_input->user_name.' Change id : '.$changes_id);

          $this->_db->commit();

          $this->_redirect('/popup/successpopup');
      }
    catch(Exception $e)
    {
      $this->_db->rollBack();
      $error_remark = $this->language->_('An Error Occured. Please Try Again');
    }


      if(isset($error_remark)){ $class = 'F'; $msg = $error_remark; }
    // else
    // {
    //   $class = 'S';
    //   $msg = $this->getErrorRemark('00','Customer ID',$zf_filter_input->cust_id);
    // }

    $this->_helper->getHelper('FlashMessenger')->addMessage($class);
    $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
    $this->_redirect($this->_backURL);

    }//END if($zf_filter_input->isValid())
    else
    {

       if(!$privcheck){
        $this->view->errcheckbox = $this->language->_('Please Select at least 1 Privilege');
      }

      $active = json_decode(json_encode($dataUser), true);
      foreach ($active as $arr => $value){
        $limit = 'limit'.$value['accountNo'];
        $this->view->$limit =  Application_Helper_General::displayMoney($this->_request->getParam($limit));
      }
      

      foreach($ccylist as $key => $value){
        $daily = 'daily'.$value['CCY_ID'];
        $this->view->$daily =  Application_Helper_General::displayMoney($this->_request->getParam($daily));
      }

        $this->view->error = 1;
        $this->view->user_fullname  = ($zf_filter_input->isValid('user_fullname'))? $zf_filter_input->user_fullname : $this->_getParam('user_fullname');
      $this->view->user_email = ($zf_filter_input->isValid('user_email'))? $zf_filter_input->user_email : $this->_getParam('user_email');
      $this->view->user_phone = ($zf_filter_input->isValid('user_phone'))? $zf_filter_input->user_phone : $this->_getParam('user_phone');
      $this->view->user_ext   = ($zf_filter_input->isValid('user_ext'))? $zf_filter_input->user_ext : $this->_getParam('user_ext');
      $this->view->token_id        = ($zf_filter_input->isValid('token_id'))? $zf_filter_input->token_id : $this->_getParam('token_id');

        $this->view->user_isemail        = ($zf_filter_input->isValid('user_isemail'))? $zf_filter_input->user_isemail : $this->_getParam('user_isemail');
      $this->view->user_iswebservices  = ($zf_filter_input->isValid('user_iswebservices'))? $zf_filter_input->user_iswebservices : $this->_getParam('user_iswebservices');
        $this->view->user_isemail_early  = ($zf_filter_input->isValid('user_isemail_early '))? $zf_filter_input->user_isemail_early  : $this->_getParam('user_isemail_early');
          $this->view->user_isemail_doc    = ($zf_filter_input->isValid('user_isemail_doc'))? $zf_filter_input->user_isemail_doc : $this->_getParam('user_isemail_doc');

        $this->view->priviView = $priviId;

          // $this->view->user_group = ($zf_filter_input->isValid('fgroup_id'))? $zf_filter_input->fgroup_id : $this->_getParam('fgroup_id');
          //$this->view->token_serialno = ($zf_filter_input->isValid('token_serialno'))? $zf_filter_input->token_serialno : $this->_getParam('token_serialno');
          $error = $zf_filter_input->getMessages();
      /*if(count($error))$error_remark = $this->displayErrorRemark($error);
        $this->view->user_msg = $this->displayError($error);*/

          //format error utk ditampilkan di view html
          $errorArray = null;
          foreach($error as $keyRoot => $rowError)
          {
             foreach($rowError as $errorString)
             {
                $errorArray[$keyRoot] = $errorString;
             }
          }

          if(isSet($cek_email) && $cek_email == false) $errorArray['user_email'] = 'Invalid format';

          $this->view->user_msg = $errorArray;



    }
    }//END if($this->_request->isPost())
    else
    {
        $resultdata = $this->getTempUser($changes_id);

        //get data dari temp user
        $this->view->user_fullname = $resultdata['USER_FULLNAME'];
        $this->view->user_email    = $resultdata['USER_EMAIL'];
        $this->view->user_phone    = $resultdata['USER_PHONE'];
        $this->view->token_id      = $resultdata['TOKEN_ID'];
        $this->view->user_ext      = $resultdata['USER_EXT'];

        $this->view->user_isemail  = $resultdata['USER_ISEMAIL'];
        $this->view->user_iswebservices  = $resultdata['USER_ISWEBSERVICES'];
        $this->view->user_isemail_early  = $resultdata['USER_ISEMAIL_EARLY'];
        // $this->view->user_isemail_doc    = $resultdata['USER_ISEMAIL_DOC'];


        //get data dari temp privilege
        //$fuser_id  = $cust_id . $user_id;
        $privilege = $this->getTempFpriviUser($changes_id);

          $daily = $this->getTempDaily($changes_id);
        $maker = $this->getTempMaker($changes_id);

        if(!empty($daily)){
          foreach ($daily as $arr => $value){
            $daily = 'daily'.$value['CCY_ID'];

            $amount = $value['DAILYLIMIT'];

            $this->view->$daily =  Application_Helper_General::displayMoney($amount);
          }
        }

        if(!empty($maker)){
          foreach ($maker as $arr => $value){
            $limit = 'limit'.$value['ACCT_NO'];

            $amount = $value['MAXLIMIT'];

            $this->view->$limit =  Application_Helper_General::displayMoney($amount);
          }
        }

    $this->view->priviView = array();
      if(count($privilege) > 0)
      {
       $this->view->priviView = Application_Helper_Array::simpleArray($privilege,'FPRIVI_ID');
      }



    }


     //--------query privilege-------------
     //modifikasi privilege agar bisa ditampilkan sesuai golongan
     $this->view->getModuleDescArr = $this->getModuleDescArr();

     $privilege_final = array();
     $privilege_final_modif = array();
     foreach($getprivilege as $row)
     {
         $fprivi_moduleid = trim($row['FPRIVI_MODULEID']);
         $privilege_final[$fprivi_moduleid][] = $row;
         $privilege_final_modif = $this->modifPrivi($privilege_final);
     }

     $this->view->fprivilege = $privilege_final_modif;

     $setting = new Settings();
    $system_type = $setting->getSetting('system_type');
     $this->view->template   = Application_Helper_Array::listArray($this->getTemplate($system_type),'FTEMPLATE_ID','FTEMPLATE_DESC');

     $priviTemplate = array();
     foreach($this->getPriviTemplate() as $row)
     {
       $priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
     }

     $this->view->priviTemplate = $priviTemplate;
    //END query privilege


    //get customer name
    $select = $this->_db->select()
                           ->from('M_CUSTOMER',array('CUST_NAME'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
    $this->view->cust_name = $this->_db->fetchOne($select);

    $this->view->cust_id = $cust_id;
    $this->view->user_id = $user_id;
    $this->view->changes_id = $changes_id;
    $this->view->modulename = $this->_request->getModuleName();



    //insert log
   Application_Helper_General::writeLog('RULC','Repair User List');


  } 


}



