<?php

require_once 'Zend/Controller/Action.php';

class Generalsetting_TransactionlimitController extends Application_Main
{

    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()
					->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					->where('MODULE_ID = ?' , 'GNS')
					->query()->FetchAll();
		$setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		foreach($setting as $key=>$value)
		{
			if(!empty($key))
				$this->view->$key = $value;
		}

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');
		
		$this->view->system_type = $system_type;

		// convert to validate
//			$threshold_rtgs = $this->_getParam('threshold_rtgs');		
//			$threshold_rtgs_val =	Application_Helper_General::convertDisplayMoney($threshold_rtgs);
//			$this->_setParam('threshold_rtgs',$threshold_rtgs_val);

			// $threshold_lld = $this->_getParam('threshold_lld');		
			// $threshold_lld_val =	Application_Helper_General::convertDisplayMoney($threshold_lld);
			// $this->_setParam('threshold_lld',$threshold_lld_val);

			// $admin_fee_company = $this->_getParam('admin_fee_company');		
			// $admin_fee_company_val =	Application_Helper_General::convertDisplayMoney($admin_fee_company);
			// $this->_setParam('admin_fee_company',$admin_fee_company_val);
			
			// $admin_fee_account = $this->_getParam('admin_fee_account');		
			// $admin_fee_account_val =	Application_Helper_General::convertDisplayMoney($admin_fee_account);
			// $this->_setParam('admin_fee_account',$admin_fee_account_val);
			
			// $global_charges_skn = $this->_getParam('global_charges_skn');		
			// $global_charges_skn_val =	Application_Helper_General::convertDisplayMoney($global_charges_skn);
			// $this->_setParam('global_charges_skn',$global_charges_skn_val);
			
			// $global_charges_rtgs = $this->_getParam('global_charges_rtgs');		
			// $global_charges_rtgs_val =	Application_Helper_General::convertDisplayMoney($global_charges_rtgs);
			// $this->_setParam('global_charges_rtgs',$global_charges_rtgs_val);
			
			// $threshold_lld_remittance = $this->_getParam('threshold_lld_remittance');
			// $threshold_lld_remittance_val =	Application_Helper_General::convertDisplayMoney($threshold_lld_remittance);
			// $this->_setParam('threshold_lld_remittance',$threshold_lld_remittance_val);

			// $remittance_limit_permonthly = $this->_getParam('remittance_limit_permonthly');		
			// $remittance_limit_permonthly_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_permonthly);
			// $this->_setParam('remittance_limit_permonthly',$remittance_limit_permonthly_val);
				
			
			$min_amount_skn = $this->_getParam('min_amount_skn');		
			$min_amount_skn_val =	Application_Helper_General::convertDisplayMoney($min_amount_skn);
			$this->_setParam('min_amount_skn',$min_amount_skn_val);
			
			$max_amount_skn = $this->_getParam('max_amount_skn');		
			$max_amount_skn_val =	Application_Helper_General::convertDisplayMoney($max_amount_skn);
			$this->_setParam('max_amount_skn',$max_amount_skn_val);
			
			$min_amount_rtgs = $this->_getParam('min_amount_rtgs');		
			$min_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($min_amount_rtgs);
			$this->_setParam('min_amount_rtgs',$min_amount_rtgs_val);
			
			$max_amount_rtgs = $this->_getParam('max_amount_rtgs');		
			$max_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($max_amount_rtgs);
			$this->_setParam('max_amount_rtgs',$max_amount_rtgs_val);
			
			// $min_amount_pb_idr = $this->_getParam('min_amount_pb_idr');		
			// $min_amount_pb_idr_val =	Application_Helper_General::convertDisplayMoney($min_amount_pb_idr);
			// $this->_setParam('min_amount_pb_idr',$min_amount_pb_idr_val);
			
			// $max_amount_pb_idr = $this->_getParam('max_amount_pb_idr');		
			// $max_amount_pb_idr_val =	Application_Helper_General::convertDisplayMoney($max_amount_pb_idr);
			// $this->_setParam('max_amount_pb_idr',$max_amount_pb_idr_val);
			
			// $min_amount_pb_usd = $this->_getParam('min_amount_pb_usd');		
			// $min_amount_pb_usd_val =	Application_Helper_General::convertDisplayMoney($min_amount_pb_usd);
			// $this->_setParam('min_amount_pb_usd',$min_amount_pb_usd_val);
			
			// $max_amount_pb_usd = $this->_getParam('max_amount_pb_usd');		
			// $max_amount_pb_usd_val =	Application_Helper_General::convertDisplayMoney($max_amount_pb_usd);
			// $this->_setParam('max_amount_pb_usd',$max_amount_pb_usd_val);
			
			// $min_amount_ol = $this->_getParam('min_amount_ol');		
			// $min_amount_ol_val =	Application_Helper_General::convertDisplayMoney($min_amount_ol);
			// $this->_setParam('min_amount_ol',$min_amount_ol_val);
			
			// $max_amount_ol = $this->_getParam('max_amount_ol');		
			// $max_amount_ol_val =	Application_Helper_General::convertDisplayMoney($max_amount_ol);
			// $this->_setParam('max_amount_ol',$max_amount_ol_val);
			
			// $min_amount_batch = $this->_getParam('min_amount_batch');		
			// $min_amount_batch_val =	Application_Helper_General::convertDisplayMoney($min_amount_batch);
			// $this->_setParam('min_amount_batch',$min_amount_ol_val);
			
			// $max_amount_batch = $this->_getParam('max_amount_batch');		
			// $max_amount_batch_val =	Application_Helper_General::convertDisplayMoney($max_amount_batch);
			// $this->_setParam('max_amount_batch',$max_amount_batch_val);
			
			$escrow_acct_idr = $this->_getParam('escrow_acct_idr');
			// $escrow_acct_idr_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_idr);
			$this->_setParam('escrow_acct_idr',$escrow_acct_idr);
			
			$escrow_acct_usd = $this->_getParam('escrow_acct_usd');
			// $escrow_acct_usd_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_usd);
			$this->_setParam('escrow_acct_usd',$escrow_acct_usd);
				
			// $remittance_limit_per_month = $this->_getParam('remittance_limit_per_month');
			// $remittance_limit_per_month_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_per_month);
			// $this->_setParam('remittance_limit_per_month',$remittance_limit_per_month_val);

			// $range_futuredate = $this->_getParam('range_futuredate');
			// $escrow_acct_usd_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_usd);
			// $this->_setParam('range_futuredate',$range_futuredate);
				
			/* $SKN_cost = $this->_getParam('SKN_cost');		
			$SKN_cost_val =	Application_Helper_General::convertDisplayMoney($SKN_cost);
			$this->_setParam('SKN_cost',$SKN_cost_val);

			$RTGS_cost = $this->_getParam('RTGS_cost');		
			$RTGS_cost_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_cost',$RTGS_cost_val); */

			/* $SKN_charge = $this->_getParam('SKN_charge');		
			$SKN_charge_val =	Application_Helper_General::convertDisplayMoney($SKN_charge);
			$this->_setParam('SKN_charge',$SKN_charge_val);

			$RTGS_charge = $this->_getParam('RTGS_charge');		
			$RTGS_charge_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_charge',$RTGS_charge_val); */

			//FILTER
			{
			$filters = array(
							
							// 'auto_monthlyfee' => array('StringTrim','StripTags'),
							// 'remittance_limit_per_month' => array('StringTrim','StripTags'),
							// 'range_futuredate' => array('StringTrim','StripTags'),
							// 'threshold_lld' => array('StringTrim','StripTags'),
							// 'threshold_lld_remittance' => array('StringTrim','StripTags'),
							// 'remittance_limit_permonthly' => array('StringTrim','StripTags'),
							'min_amount_skn' => array('StringTrim','StripTags'),
							'max_amount_skn' => array('StringTrim','StripTags'),
							'min_amount_rtgs' => array('StringTrim','StripTags'),
							'max_amount_rtgs' => array('StringTrim','StripTags'),
							// 'min_amount_pb_idr' => array('StringTrim','StripTags'),
							// 'max_amount_pb_idr' => array('StringTrim','StripTags'),
							// 'min_amount_pb_usd' => array('StringTrim','StripTags'),
							// 'max_amount_pb_usd' => array('StringTrim','StripTags'),
            			    // 'min_amount_ol' => array('StringTrim','StripTags'),
            			    // 'max_amount_ol' => array('StringTrim','StripTags'),
            			    // 'min_amount_batch' => array('StringTrim','StripTags'),
            			    // 'max_amount_batch' => array('StringTrim','StripTags'),
            			    // 'cancel_local_remittance' => array('StringTrim','StripTags'),
			    
							/*'admin_fee_company' => array('StringTrim','StripTags'),
							'admin_fee_account' => array('StringTrim','StripTags'),
							'global_charges_skn' => array('StringTrim','StripTags'),
							'global_charges_rtgs' => array('StringTrim','StripTags'),*/
			
							// 'SKN_cost' => array('StringTrim','StripTags'),
							// 'SKN_charge' => array('StringTrim','StripTags'),
							// 'RTGS_cost' => array('StringTrim','StripTags'),
							// 'RTGS_charge' => array('StringTrim','StripTags'),
							
							);
			}

			//VALIDATE
			{
			$validators = array(
									
									// 'remittance_limit_per_month' => array('NotEmpty',
									// 						array('Between', array('min'=>1,'max'=>9999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 								//$this->language->_('Amount must be greater than 0.00'),
									// 								$this->language->_('Value between 1 - 9999999999.99'),
									// 								$this->language->_('Value between 1 - 9999999999.99'),)),
                    			   
									// 'threshold_lld' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						('Can not be empty',
									// 						'Value between 1 - 9999999999999.99',
									// 						'Must be numeric values',	)),
									
									// 'threshold_lld_remittance' => array('NotEmpty',
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						('Can not be empty',
									// 						'Value between 1 - 9999999999999.99',
									// 						'Must be numeric values',	)),
// 									'remittance_limit_per_month' => array('NotEmpty',
// 															array('Between', array('min'=>1,'max'=>9999999999999.99)),
// 															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
// 															'messages' => array
// 															('Can not be empty',
// 															'Value between 1 - 9999999999999.99',
// 															'Must be numeric values',	)),
														
									'min_amount_skn' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
															
									'max_amount_skn' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
															
									'min_amount_rtgs' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'max_amount_rtgs' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),

									// 'min_amount_pb_idr' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 						$this->language->_('Amount must be greater than 0.00'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),	)),
									// 'max_amount_pb_idr' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 						$this->language->_('Amount must be greater than 0.00'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),	)),

									// 'min_amount_pb_usd' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 						$this->language->_('Amount must be greater than 0.00'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),	)),
									// 'max_amount_pb_usd' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 						$this->language->_('Amount must be greater than 0.00'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),	)),

									// 'min_amount_batch' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 						$this->language->_('Amount must be greater than 0.00'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),	)),
									// 'max_amount_batch' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 						$this->language->_('Amount must be greater than 0.00'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),	)),
									// // 'range_futuredate' => array(	'NotEmpty',
                                    // //     			        'messages' => array
									// // 						('Can not be empty')),
															
									// 'min_amount_ol' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 						$this->language->_('Amount must be greater than 0.00'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),	)),
									// 'max_amount_ol' => array('NotEmpty',														
									// 						array('Between', array('min'=>1,'max'=>9999999999999.99)),
									// 						new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
									// 						'messages' => array
									// 						($this->language->_('Can not be empty'),
									// 						$this->language->_('Amount must be greater than 0.00'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),
									// 						$this->language->_('Value between 1 - 9999999999999.99'),	)),		 
															);
			}

				$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				$arrfck = array('min_amount_skn','max_amount_skn','min_amount_rtgs','max_amount_rtgs');

				$cek = $this->_db->select()
									->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID'))
									->WHERE ('MODULE_ID = ?','GNS')
									->query()->FetchAll();
				if($cek == null)
				{
					if($this->_request->isPost() && $this->view->hasPrivilege('STUD'))
					{
						//validasi global admin fee
						
						
						if($zf_filter->isValid() )
						{
							$this->_db->beginTransaction();
							try{

								$info = "GENERAL SETTING";
								$change_id = $this->suggestionWaitingApproval('General Setting',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','','Global Transaction Limit Setting');
								//$change_id = 1;
								
								foreach($setting as $key=>$value){
										if (in_array($key, $arrfck)) {
											$valuee = $this->_getParam2($key);
											$data = array(	'CHANGES_ID' => $change_id,
															'SETTING_ID' => $key,
															'SETTING_VALUE' => htmlspecialchars_decode($valuee),
															'MODULE_ID' => 'GNS',
															);
											$this->_db->insert('TEMP_SETTING',$data);	
										}
											
								}

										Application_Helper_General::writeLog('STUD','Update General Setting');
										$this->_db->commit();
										$this->view->success = true;
										$msg = "Settings Change Request Saved";
										$this->view->report_msg = $msg;
								}
								catch(Exception $e)
								{
										$this->_db->rollBack();
								}
						}
						else{
							$docErr = 'Error in processing form values. Please correct values and re-submit.';
							$this->view->report_msg = $docErr;

							foreach($zf_filter->getMessages() as $key=>$err){
							$xxx = 'x'.$key;
							$this->view->$xxx = $this->displayError($err);
							}
						}
					}
				}else{
					$this->view->error = true;
					//$this->fillParam($zf_filter_input);
					$this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
				}
				

		if($this->_request->isPost()){
		    
			foreach($setting as $key=>$value)
			{
				if (in_array($key, $arrfck)){
					$this->view->$key =   $this->_getParam($key);
				}
				else{
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
				}
			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		}
		else{
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			Application_Helper_General::writeLog('STLS','View General Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 200 - $disable_note_len;
		$note_len 	 = 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;


	}
}