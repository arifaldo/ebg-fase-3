<?php

Class systembalance_Model_Systembalance extends Application_Main
{
    protected $_acctData;

	// constructor
    public function initModel()
    { 
      	$this->_acctData = array('ACCT_NO'              => null,
					         'CUST_ID'         			=> null,
					         'CCY_ID'          			=> null,
					         'ACCT_NAME'                => null,
					         'GROUP_ID'                 => null,
					         'ACCT_TYPE'                => null,
					         'ACCT_DESC'                => null,
					         'ACCT_ALIAS_NAME' 	        => null,
					         'ACCT_EMAIL'   	        => null,
					         'ORDER_NO'		            => null,
					        );
           
    }
	
    public function getAcctno($acct_no,$cust_id)
    { 
       $select = $this->_db->select()
                           ->from('M_CUSTOMER_ACCT')
                           ->where('ACCT_NO='.$this->_db->quote((string)$acct_no))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->query()->fetch();
	   return $select;
    }
	
 	private function insertTemp($data,$id)
 	{
  		$insertArr = array(
  			'CHANGES_ID'     => $id,
			'ROOT_CODE'      => $data['ROOT_CODE'],
			'ROOT_NAME'      => $data['ROOT_NAME'],
			'ROOT_STATUS'    => $data['ROOT_STATUS'],
			'PRINCIPLE_CUST' => $data['PRINCIPLE_CUST'],
		);

    	$this->_db->insert('TEMP_SET_SYSTEMBALANCE', $insertArr);
  	}

  	public function deleteTemp($id)
  	{
  		$where = array('CHANGES_ID = ?' => $id);
  		$this->_db->delete('TEMP_SET_SYSTEMBALANCE', $where);
  	}
	
   
}




