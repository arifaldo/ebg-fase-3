<?php
require_once 'Zend/Controller/Action.php';
class Accountgroup_acctgroupchangeController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$fields = array(
						'number'  => array('field' => 'ACCT_NO',
											   'label' => $this->language->_('Account Number'),
											   'sortable' => false),
						'name'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Account Name'),
											   'sortable' => false),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => false),
						'alias'  => array('field' => 'ACCT_ALIAS_NAME',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => false),
						'group'  => array('field' => 'GROUP_NAME',
											   'label' => $this->language->_('Account Group'),
											   'sortable' => false)
				);
				
		//get sortby, sortdir
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	=> array('StringTrim','StripTags'),
							'number'    => array('StringTrim','StripTags'),
							'name'    	=> array('StringTrim','StripTags','StringToUpper'),
							'alias' 	=> array('StringTrim','StripTags','StringToUpper'),
							'group'     => array('StringTrim','StripTags')
		);
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$info = $this->_db->fetchRow(
				  $this->_db->select()
					   ->from("T_CUST_ACCT_GROUP_CHANGES") 
					   ->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   );
		
		$suggest = $this->_db->select()
					   ->from(array('A' => 'TEMP_CUSTOMER_ACCT_GROUP')) 
					   ->joinLeft(array('G' => 'M_GROUPING'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'))
					   ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   ->where("A.GROUP_ID>0 AND A.GROUP_ID IS NOT NULL")
					   ->order($sortBy.' '.$sortDir)
					   ->query()->fetchAll();
		
		$current = $this->_db->select()
					   ->from(array('A' => 'M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_NAME','CCY_ID','ACCT_ALIAS_NAME')) 
					   ->joinLeft(array('G' => 'M_GROUPING'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'))
					   ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
					   ->where("A.GROUP_ID>0 AND A.GROUP_ID IS NOT NULL")
					   ->order($sortBy.' '.$sortDir)
					   ->query()->fetchAll();
		
		$this->view->suggestInfo = $info;
		$this->view->suggestData = $suggest;
		$this->view->currentData = $current;
		$this->view->fields = $fields;
		
		Application_Helper_General::writeLog('GRAM','Viewing Account Group Changes');
	}
	
	public function deleteAction()
	{
		$suggestId = $this->_getParam('suggest_id');
		$suggestId = (Zend_Validate::is($suggestId,'Digits'))? $suggestId : null;
		if($suggestId)
		{
			try 
			{
				$this->_db->beginTransaction();
				
				$where['SUGGEST_ID = ?'] = $suggestId;
				$this->_db->delete('T_CUST_ACCT_GROUP_CHANGES',$where);
				$this->_db->delete('TEMP_CUSTOMER_ACCT_GROUP',$where);
				
				$this->_db->commit();
				
				Application_Helper_General::writeLog('GRAM','Deleting Account Group'.$suggestId);
				
				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
				$this->_redirect('/notification/success');
			}
			catch(Exception $e) 
			{
				//rollback changes
				$this->_db->rollBack();
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				
				Application_Helper_General::writeLog('GRAM','Roll Back Deleting Account Group'.$suggestId);
			}				
		}
		else
		{
		   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
		   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
		   Application_Helper_General::writeLog('GRAM','Invalid Data Deleting Account Group'.$suggestId);
		}
			
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}	
		}
		
		$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());	
	}
}
