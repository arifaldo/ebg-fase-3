<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'General/Account.php';
require_once 'Crypt/AESMYSQL.php';

class registerechannel_IndexController extends Application_Main
{
	public function initController()
	{
		$this->_helper->_layout->setLayout('newlayout');
	}

	public function indexAction()
	{
		$setting = new Settings();
		$this->view->show_register_account = $setting->getSetting('system_type');

		$system_type = $setting->getSetting('system_type');
		$this->view->system_type = $system_type;

		$max_length_userid = $this->_db->select()
			->from('M_SETTING')
			->where('SETTING_ID = ?', "max_length_userid");
		$max_length_userid = $this->_db->fetchRow($max_length_userid);
		$this->view->max_length_userid = $max_length_userid['SETTING_VALUE'];


		// $getCompanyType = $this->getCompanyType();
		// $companyTypeArr = Application_Helper_Array::listArray($getCompanyType,'COMPANY_TYPE_CODE','COMPANY_TYPE_DESC');

		// $getBusinessEntity = $this->getBusinessEntity();
		// $businessEntityArr = Application_Helper_Array::listArray($getBusinessEntity,'BUSINESS_ENTITY_CODE','BUSINESS_ENTITY_DESC');

		// $getDebitur = $this->getDebitur();
		// $debiturArr = Application_Helper_Array::listArray($getDebitur,'DEBITUR_CODE','DEBITUR_DESC');

		// $getCreditQuality = $this->getCreditQuality();
		// $creditQualityArr = Application_Helper_Array::listArray($getCreditQuality,'CODE','DESCRIPTION');

		// $getCity = $this->getCity();
		// $cityArr = Application_Helper_Array::listArray($getCity,'CITY_CODE','CITY_NAME');

		// $getCountry = $this->getCountry();
		// $countryArr = Application_Helper_Array::listArray($getCountry,'COUNTRY_CODE','COUNTRY_NAME');

		//tes

		$cust = $this->getRequest()->getParam('cust_id');

		$encrypt = new Crypt_AESMYSQL();

		$decrypcust_id =  ($this->sslDec($cust));


		if (!empty($decrypcust_id)) {

			$selectcust = $this->_db->select()
				->from('TEMP_ONLINECUST_BIS')
				->where('CUST_ID = ?', $decrypcust_id);

			$CustId = $this->_db->fetchRow($selectcust);

			$this->view->cust_id = $decrypcust_id;
			$this->view->cust_reg_number = $CustId['CUST_REG_NUMBER'];
			$this->view->cust_name = $CustId['CUST_NAME'];
			$this->view->cust_npwp = $CustId['CUST_NPWP'];
			$this->view->cust_type = $CustId['CUST_TYPE'];
			$this->view->grup_bumn = $CustId['GRUP_BUMN'];
			$this->view->cust_workfield = $CustId['BUSINESS_TYPE'];
			$this->view->go_public = $CustId['GO_PUBLIC'];
			$this->view->debitur_code = $CustId['DEBITUR_CODE'];
			$this->view->cust_address = $CustId['CUST_ADDRESS'];
			$this->view->cust_village = $CustId['CUST_VILLAGE'];
			$this->view->cust_district = $CustId['CUST_DISTRICT'];
			$this->view->city_list = $CustId['CUST_CITY'];
			$this->view->cust_zip = $CustId['CUST_ZIP'];
			$this->view->country_code = $CustId['COUNTRY_CODE'];
			$this->view->cust_contact = $CustId['CUST_CONTACT'];
			$this->view->cust_contact_phone = $CustId['CUST_CONTACT_PHONE'];
			// $this->view->cust_contact = $CustId['CUST_CONTACT'];
			$this->view->cust_email = $CustId['CUST_EMAIL'];
			$this->view->cust_phone = $CustId['CUST_PHONE'];
			$this->view->cust_contact = $CustId['CUST_CONTACT'];
			$this->view->cust_website = $CustId['CUST_WEBSITE'];
			$this->view->cust_approver = $CustId['CUST_APPROVER'];
			$this->view->cust_same_user = $CustId['CUST_SAME_USER'];
			$this->view->bank_branch = $CustId['BANK_BRANCH'];


			$selectuser = $this->_db->select()
				->from('T_ONLINECUST_BIS_USER')
				->where('CUST_ID = ?', $decrypcust_id);

			$Adminuser = $this->_db->fetchRow($selectuser);

			//Zend_Debug::dump($Adminuser);

			$this->view->user_id = $Adminuser['USER_ID'];
			$this->view->user_email = $Adminuser['USER_EMAIL'];
			$this->view->user_fullname = $Adminuser['USER_FULLNAME'];
			$this->view->user_phone = $Adminuser['USER_PHONE'];
			$this->view->user_nik = $Adminuser['USER_NIK'];
			$this->view->user_priv = $Adminuser['USER_PRIVI'];



			$selectacct = $this->_db->select()
				->from('T_ONLINECUST_BIS_ACC')
				->where('CUST_ID = ?', $decrypcust_id);

			$Accomp = $this->_db->fetchAll($selectacct);

			$this->view->Accomp = $Accomp;


			//Zend_Debug::dump($Accomp);


			//untuk update			

		}

		$model = new registerechannel_Model_Registerechannel();
		$country = Application_Helper_Array::listArray($model->getCountry(), 'COUNTRY_CODE', 'COUNTRY_NAME');

		$this->view->countryArr = array_merge(array('' => '--- ' . $this->language->_('Pilih') . ' ---'), $country);

		//add
		$getCompanyType = $this->_db->select()
			->from('M_COMPANY_TYPE')
			->query()->fetchAll();

		//$getCompanyTypeArr = $this->_db->fetchAll($getCompanyType);

		//Zend_Debug::dump($getCompanyType);

		$this->view->getCompanyType = $getCompanyType;

		$company_type = Application_Helper_Array::listArray($model->getCompanyType(), 'COMPANY_TYPE_CODE', 'COMPANY_TYPE_DESC');

		//$this->view->companytypeArr = array_merge(array(''=>'--- '.$this->language->_('Any Value').' ---'),$getCompanyType);

		$business_type = Application_Helper_Array::listArray($model->getBusinessType(), 'BUSINESS_ENTITY_CODE', 'BUSINESS_ENTITY_DESC');
		// $this->view->businesstypeArr = array_merge(array(''=>'--- '.$this->language->_('Any Value').' ---'),$business_type);

		// $debitur_code = Application_Helper_Array::listArray($model->getDebiturCode(),'DEBITUR_CODE','DEBITUR_DESC');

		$businesstypeArr = $model->getBusinessType();
		$this->view->businesstypeArr = $businesstypeArr;

		$city_list = $model->getCity();
		$this->view->citylistArr = $city_list;
		$cityArr = Application_Helper_Array::listArray($model->getCity(), 'CITY_CODE', 'CITY_NAME');
		$countryArr = Application_Helper_Array::listArray($model->getCountry(), 'COUNTRY_CODE', 'COUNTRY_NAME');



		$debitur_code = $model->getDebiturCode();
		$this->view->debiturcodeArr = $debitur_code;
		$debiturArrc = Application_Helper_Array::listArray($model->getDebiturCode(), 'DEBITUR_CODE', 'DEBITUR_DESC');



		// echo "<pre>";
		// print_r($debitur_code);
		// print_r(array_merge(array(''=>'--- '.$this->language->_('Any Value').' ---'),$debitur_code));die;



		//icon
		$destination = LIBRARY_PATH . '/data/temp/';
		$path1 =  $destination . '../../../frontend/public/assets/themes/assets/newlayout/img/maker.PNG';
		$type1 = pathinfo($path1, PATHINFO_EXTENSION);
		$data1 = file_get_contents($path1);

		$base641 = 'data:image/' . $type1 . ';base64,' . base64_encode($data1);
		$MakerNameCircle = '<a style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="' . $base641 . '">
		<span class="hovertextcontent" style="left: -100px;">' . $MakerList . '</span></a>';

		$this->view->makerNameCircle = $MakerNameCircle;


		$path2 =  $destination . '../../../frontend/public/assets/themes/assets/newlayout/img/reviewer.PNG';
		$type2 = pathinfo($path2, PATHINFO_EXTENSION);
		$data2 = file_get_contents($path2);

		$base642 = 'data:image/' . $type2 . ';base64,' . base64_encode($data2);

		$ReviewerNameCircle = '<a style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="' . $base642 . '">
		<span class="hovertextcontent" style="left: -100px;">' . $ReviewerList . '</span></a>';

		$this->view->reviewerNameCircle = $ReviewerNameCircle;

		$path3 =  $destination . '../../../frontend/public/assets/themes/assets/newlayout/img/approver.png';
		$type3 = pathinfo($path3, PATHINFO_EXTENSION);
		$data3 = file_get_contents($path3);

		$base643 = 'data:image/' . $type3 . ';base64,' . base64_encode($data3);

		$ApproverNameCircle = '<a style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="' . $base643 . '">
		<span class="hovertextcontent" style="left: -100px;">' . $ApproverList . '</span></a>';

		$this->view->approverNameCircle = $ApproverNameCircle;


		$path4 =  $destination . '../../../frontend/public/assets/themes/assets/newlayout/img/releaser.png';
		$type4 = pathinfo($path4, PATHINFO_EXTENSION);
		$data4 = file_get_contents($path4);

		$base644 = 'data:image/' . $type4 . ';base64,' . base64_encode($data4);

		$releaserNameCircle = '<a style="text-decoration: none;" disabled><img class="' . $imgClass . '" src="' . $base644 . '">
		<span class="hovertextcontent" style="left: -100px;">' . $releaserList . '</span></a>';

		$this->view->releaserNameCircle = $releaserNameCircle;


		//end

		$ccyArr = Application_Helper_Array::listArray($model->getCCY(), 'CCY_ID', 'CCY_ID');
		$this->view->ccyArr = array_merge(array('' => '--- ' . $this->language->_('Any Value') . ' ---'), $ccyArr);

		$limitidr = $this->_getParam('cust_limit_idr');
		$limitidr = Application_Helper_General::convertDisplayMoney($limitidr);
		$this->_setParam('cust_limit_idr', $limitidr);

		$limitusd = $this->_getParam('cust_limit_usd');
		$limitusd = Application_Helper_General::convertDisplayMoney($limitusd);
		$this->_setParam('cust_limit_usd', $limitusd);


		$selectAggrement = $this->_db->select()
			->from('M_SETTING')
			->where('SETTING_ID = ?', 'ftemplate_agreementonline');
		$terms = $this->_db->fetchRow($selectAggrement);

		$templateEmailMasterBankName = $this->_getSettingBis('master_bank_email');
		$terms['SETTING_VALUE'] = str_ireplace('[[master_bank_email]]', $templateEmailMasterBankName, $terms['SETTING_VALUE']);

		$this->view->terms = $terms['SETTING_VALUE'];

		//$params = $this->_request->getParams();
		$IDCUST	= $decrypcust_id;

		if ($this->_request->isPost()) {

			// echo("<pre>");

			//printr_r($request->_getParam());die;
			//customer data
			$filters = array(
				'cust_name'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'bank_branch'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_type'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_npwp'      => array('StripTags', 'StringTrim', 'HtmlEntities'),

				'cust_workfield'    => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'go_public'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'debitur_code'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'group_bumn'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_address'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_village'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_district'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_city'         => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_zip'          => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'country_code'      => array('StripTags', 'StringTrim', 'HtmlEntities'),

				'cust_contact'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_contact_phone'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_phone'        => array('StripTags', 'StringTrim', 'HtmlEntities'),

				'cust_email'        => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_website'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_approver'      => array('StripTags', 'StringTrim', 'HtmlEntities'),
				'cust_same_user'      => array('StripTags', 'StringTrim', 'HtmlEntities'),

			);



			//$filters = array('*' => array('StringTrim', 'StripTags'));


			// $validators =  array(


			// 	'cust_name'                => array('NotEmpty',
			// 										   'messages' => array($this->language->_('Can not be empty'),
			// 																 )
			// 									),					              

			// 	'cust_type'                 => array('NotEmpty',
			// 										'messages' => array($this->language->_('Can not be empty'),
			// 															)
			// 								),	

			// 	'cust_npwp'         	 => array(array('StringLength',array('min'=>1,									'max'=>128)),
			// 									'allowEmpty' => true,
			// 									 'Digits',
			// 									'messages' => array(
			// 													   $this->language->_('The maximum characters allowed is').' 128'
			// 														)
			// 								   ),


			// 	'cust_workfield'            => array('allowEmpty' => true,
			// 										 array('StringLength',array('max'=>80)),
			// 										 'messages' => array(
			// 															   $this->language->_('The maximum characters allowed is').' 80',
			// 														   )
			// 									),

			// 	'go_public'            => array('allowEmpty' => true,
			// 									array('StringLength',array('max'=>80)),
			// 									'messages' => array(
			// 														  $this->language->_('The maximum characters allowed is').' 80',
			// 													  )
			// 							   ),

			// 	'debitur_code'          => array('allowEmpty' => true,
			// 							   array('StringLength',array('max'=>80)),
			// 							   'messages' => array(
			// 													 $this->language->_('The maximum characters allowed is').' 80',
			// 												 )
			// 						  ),


			// 	'cust_address'            => array('allowEmpty'=>true,
			// 										array('StringLength',array('max'=>200)),
			// 									   'messages' => array(
			// 															//$this->language->_('Can not be empty'),
			// 															$this->language->_('The maximum characters allowed is').' 200'
			// 														   )
			// 									),

			// 	'cust_village'               => array(array('StringLength',array('min'=>1,'max'=>20)),
			// 									'allowEmpty' => true,
			// 									'messages' => array(
			// 														$this->language->_('The maximum characters allowed is').' 20'
			// 													   )
			// 								   ),

			// 	'cust_district'               => array(array('StringLength',array('min'=>1,'max'=>20)),
			// 								   'allowEmpty' => true,
			// 								   'messages' => array(
			// 													   $this->language->_('The maximum characters allowed is').' 20'
			// 													  )
			// 								  ),



			// 	'cust_city'               => array(array('StringLength',array('min'=>1,'max'=>20)),
			// 									   'allowEmpty' => true,
			// 									   'messages' => array(
			// 														   $this->language->_('The maximum characters allowed is').' 20'
			// 														  )
			// 									  ),

			// 	'cust_zip'           => array(
			// 									   'allowEmpty' => true,
			// 										 'Digits',
			// 									   'messages' => array(
			// 															$this->language->_('Invalid Zip Code format'),
			// 														  )
			// 									),

			// 	'country_code'           => array(
			// 									   'NotEmpty',
			// 									   'messages' => array(
			// 															  $this->language->_('Can not be empty'),
			// 														  )
			// 									  ),				              


			// 	   'cust_contact'       => array(//'allowEmpty' => true,
			// 								   'NotEmpty',
			// 								 array('StringLength',array('min'=>1,'max'=>128)),
			// 								 'messages' => array(
			// 													 $this->language->_('Can not be empty'),
			// 													$this->language->_('The maximum characters allowed is').' 128')
			// 								 ),

			// 	'cust_contact_phone'        => array(//'Digits',
			// 									array('StringLength',array('min'=>1,'max'=>128)),
			// 									//'allowEmpty'=>true,
			// 									'NotEmpty',
			// 									'messages' => array(//$this->language->_('Invalid phone number format'),
			// 														$this->language->_('The maximum characters allowed is').' 128',
			// 														$this->language->_('Can not be empty')
			// 													   )
			// 										 ),	

			// 	'cust_phone'        => array(//'Digits',
			// 								 array('StringLength',array('min'=>1,'max'=>128)),
			// 								 //'allowEmpty'=>true,
			// 								 'NotEmpty',
			// 								 'messages' => array(//$this->language->_('Invalid phone number format'),
			// 													 $this->language->_('The maximum characters allowed is').' 128',
			// 													 $this->language->_('Can not be empty')
			// 													)
			// 									  ),					         



			// 	'cust_email'        => array( 
			// 								  array('StringLength',array('min'=>1,'max'=>128)),
			// 								  //'allowEmpty' => true,
			// 								  'NotEmpty',
			// 								  'messages' => array(
			// 													 $this->language->_('Email length cannot be more than 128'),
			// 													 $this->language->_('Can not be empty'),
			// 													 )
			// 								),

			// 	'cust_website'      => array(new Application_Validate_Hostname(),
			// 									   array('StringLength',array('min'=>1,'max'=>100)),
			// 									   'allowEmpty' => true,
			// 									   'messages' => array($this->language->_('Invalid URL format'),
			// 															  $this->language->_('The maximum characters allowed is').' 100'
			// 														   )
			// 								),
			// 	'cust_approver'  => array(
			// 									'allowEmpty' => true
			// 								),
			// 	'cust_same_user'  => array(
			// 									'allowEmpty' => true
			// 								),

			// 	);

			$validators = array(
				'cust_name'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Company Name cannot be empty'))
				),
				'cust_npwp'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Company NPWP cannot be empty'))
				),
				'cust_type'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Silahkan pilih'))
				),
				'cust_workfield'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Silahkan pilih'))
				),
				'debitur_code'  => array(
					'allowEmpty' => true
				),
				'group_bumn'  => array(
					'allowEmpty' => true
				),
				'go_public'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Silah pilih'))
				),
				'cust_address'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Address cannot be empty'))
				),
				'cust_village'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kelurahan cannot be empty'))
				),
				'cust_district'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kecamatan cannot be empty'))
				),
				'cust_city'  => array(
					'NotEmpty',
					'messages' => array($this->language->_('Kabupaten/City cannot be empty'))
				),
				'cust_zip'   => array(
					'NotEmpty',
					'messages' => array($this->language->_('Zip cannot be empty'))
				),
				'country_code'    => array(
					'NotEmpty',
					'messages' => array($this->language->_('Silahkan pilih'))
				),
				'cust_contact'	 => array('allowEmpty' => true),
				'cust_contact_phone'     => array(
					'allowEmpty' => true,
					array('StringLength', array('min' => 1, 'max' => 128)),
					'messages' => array(
						$this->language->_('Contact Number maximum characters allowed is') . ' 128'
					)
				),
				'cust_phone'       => array(
					'allowEmpty' => true,
					array('StringLength', array('min' => 1, 'max' => 128)),
					'messages' => array(
						$this->language->_('Contact Number maximum characters allowed is') . ' 128'
					)
				),
				'cust_email' => array(
					'NotEmpty',
					'messages' => array($this->language->_('Contact Email cannot be empty'))
				),
				'cust_website'  => array(
					'allowEmpty' => true
				),

				'cust_same_user'  => array(
					'allowEmpty' => true
				),
				'cust_approver'  => array(
					'allowEmpty' => true
				),
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);


			// echo "<pre>";
			// var_dump($zf_filter_input);
			// echo"papapapa";
			// var_dump($zf_filter_input->isValid());






			$flag_user_id = 'T';

			//validasi multiple email
			// if($this->_getParam('cust_email'))
			// {
			// 	$validate = new validate;
			//   	$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('cust_email'));
			// }
			// else
			// {
			//   	$cek_multiple_email = true;
			// }

			//$cek_multiple_email = true;

			$toUpload = false;

			// if($zf_filter_input->isValid() && $flag_user_id == 'T' && $cek_multiple_email == true)
			if ($zf_filter_input->isValid() && $flag_user_id == 'T') {
				$custData = array();

				foreach ($validators as $key => $value) {
					if ($zf_filter_input->$key) {
						$custData[strtoupper($key)] = $zf_filter_input->$key;
					}
					//$custData[strtoupper($key)] = $zf_filter_input->getEscaped($key);	
				}

				// substr npwp
				$custData['CUST_NPWP'] = str_replace('.', '', $custData['CUST_NPWP']);
				$custData['CUST_NPWP'] = str_replace('-', '', $custData['CUST_NPWP']);

				//      echo"lala4x";
				//Zend_Debug::dump($custData);die;


				$maxlen = 12;
				$namelen = $maxlen - 2;
				$namePart = trim(substr(preg_replace('/[^a-zA-Z0-9]/', '', $zf_filter_input->cust_name), 0, $namelen));
				$numberPart = mt_rand(11, 99);
				$tempID = $namePart . $numberPart;

				$notunique = false;



				$cekCustId = $this->_db->select()
					->from('TEMP_MYONLINE_BIS')
					->where('CUST_ID = ?', $tempID)
					->query()->fetchAll();
				if (!empty($cekCustId)) {
					$notunique = true;
				}

				while ($notunique) {
					$newNumber = mt_rand(11, 99);
					$tempID = $namePart . $newNumber;

					$cekId = $this->_db->select()
						->from('TEMP_MYONLINE_BIS')
						->where('CUST_ID = ?', $tempID)
						->query()->fetchAll();

					if (empty($cekId))
						$notunique = false;
					else
						$notunique = true;
				}

				$custData['CUST_ID'] = $tempID;

				$cust_id = $tempID;
				$toUpload = true;
			} else {

				$toUpload = false;
				$this->view->error = 1;

				$this->view->errMsg = 'Error in processing form values. Please correct values and re-submit';

				foreach ($validators as $key => $value) {
					$this->view->$key = ($zf_filter_input->isValid($key)) ? $zf_filter_input->getEscaped($key) : $this->_getParam($key);
				}


				$error = $zf_filter_input->getMessages();

				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$keyname = "error_" . $keyRoot;
						$this->view->$keyname = $this->language->_($errorString);
					}
				}




				$tempKeyName = "error_";

				if ($this->_request->getParams('cust_type') == "") {
					$tempKeyName = $tempKeyName . "cust_type";





					$this->view->$tempKeyName = "Harap pilih.";
				}

				// if($this->_request->getParams('cust_workfield') == ""){
				// 	$this->view->$tempKeyName."cust_workfield" = "Harap pilih.";
				// }

				// if($this->_request->getParams('go_public') == ""){
				// 	$this->view->$tempKeyName."go_public" = "Harap pilih.";
				// }

				// if($this->_request->getParams('group_bumn') == ""){
				// 	$this->view->$tempKeyName."group_bumn" = "Harap pilih.";
				// }

				// if($this->_request->getParams('country_code') == ""){
				// 	$this->view->$tempKeyName."country_code" = "Harap pilih.";
				// }



				// if(isSet($cek_multiple_email) && $cek_multiple_email == false) $this->view->error_cust_email = $this->language->_('Invalid email format');		
			}

			//customer data -- ends


			$bankacc = $this->getCustomerAcct($this->_getParam('cust_id'));

			$this->view->bankacc = $bankacc;
			//user data
			$cekUser = $this->_getParam('user_id');

			if (!empty($cekUser)) {
				// $filters2 = array('userid' => 'StringTrim','StripTags','StringToUpper',
				// 				'fullname' => 'StringTrim','StripTags','StringToUpper',
				// 				'phone' => 'StringTrim','StripTags',
				// 				'email' => 'StringTrim','StripTags',
				// 				'ext' => 'StringTrim','StripTags',
				// 				'privi' => 'StringTrim','StripTags','StringToUpper',
				// 			);

				$filters2 = array(
					'user_id' => 'StringTrim', 'StripTags', 'StringToUpper',
					'user_fullname' => 'StringTrim', 'StripTags',
					'user_phone' => 'StringTrim', 'StripTags',
					'user_email' => 'StringTrim', 'StripTags',
					'user_privi' => 'StringTrim', 'StripTags', 'StringToUpper',
					'user_nik' => 'StringTrim', 'StripTags',
					'admuser' => 'StringTrim', 'StripTags',
				);



				$zf_filter = new Zend_Filter_Input($filters2, array(), $this->_request->getParams());


				//    echo("<pre>");
				//    Zend_Debug::dump($zf_filter);

				// $tempUserId = explode(",", $zf_filter->userid);
				// $tempFullname =explode(",", $zf_filter->fullname);
				// $tempPhone = explode(",", $zf_filter->phone);
				// $tempEmail = explode(",", $zf_filter->email);
				// $tempExt = explode(",", $zf_filter->ext);
				// $tempPrivi = explode(",", $zf_filter->privi);


				$tempCustId = explode(",", $zf_filter->cust_id);
				$tempUserId = explode(",", $zf_filter->user_id);
				$tempFullname = explode(",", $zf_filter->user_fullname);
				$tempPhone = explode(",", $zf_filter->user_phone);
				$tempEmail = explode(",", $zf_filter->user_email);
				//$tempExt = explode(",", $zf_filter->ext);
				$tempPrivi = explode(",", $zf_filter->user_privi);
				$tempUserNik = explode(",", $zf_filter->user_nik);
				$admuser = explode(",", $zf_filter->admuser);

				// echo "mencoba";
				//    Zend_Debug::dump($tempUserId);




				$userList = array();

				// foreach ($tempUserId as $key => $value) {
				// 	$userList[$key]['USER_ID'] = $value;
				// 	$userList[$key]['USER_FULLNAME'] = $tempFullname[$key];
				// 	$userList[$key]['USER_PHONE'] = $tempPhone[$key];
				// 	$userList[$key]['USER_EXT'] = $tempExt[$key];
				// 	$userList[$key]['USER_EMAIL'] = $tempEmail[$key];
				// 	$userList[$key]['USER_PRIVI'] = $tempPrivi[$key];
				// }

				foreach ($tempUserId as $key => $value) {
					$userList[$key]['CUST_ID'] = $cust_id;
					$userList[$key]['USER_ID'] = $value;
					$userList[$key]['USER_FULLNAME'] = $tempFullname[$key];
					$userList[$key]['USER_NIK'] = $tempUserNik[$key];
					$userList[$key]['USER_PHONE'] = $tempPhone[$key];
					//$userList[$key]['USER_EXT'] = $tempExt[$key];
					$userList[$key]['USER_EMAIL'] = $tempEmail[$key];
					$userList[$key]['USER_PRIVI'] = $admuser[0] == 'Y' ? 'ADMIN' : 'NON ADMIN';
				}

				$this->view->userDisplay = '-';
				$this->view->userList = $userList;
			}

			//user data -- ends

			//acc data
			$cekAcc = $this->_getParam('acct_no');

			if (!empty($cekAcc)) {
				// $filters3 = 	array( 
				// 					'acctno'	=> 	array('StringTrim','StripTags'),
				// 					'acctname'	=> 	array('StringTrim','StripTags','StringToUpper'),
				// 					'acctccy'	=> 	array('StringTrim','StripTags','StringToUpper'),
				// 					'acctown'	=> 	array('StringTrim','StripTags','StringToUpper'),
				// 				);


				$filters3 = array(
					'acct_no' => 'StringTrim', 'StripTags',
					'acct_name' => 'StringTrim', 'StripTags', 'StringToUpper',
				);


				$zf_filter2 = new Zend_Filter_Input($filters3, array(), $this->_request->getParams());



				$tempAccount = explode(",", $zf_filter2->acct_no);
				$tempAccName = explode(",", $zf_filter2->acct_name);
				$tempAcccust_id = explode(",", $zf_filter2->cust_id);
				//	$tempAccOwn = explode(",", $zf_filter2->acctown);

				//    echo("percobaan");
				// 	Zend_Debug::dump($zf_filter2);


				$acctList = array();

				foreach ($zf_filter2->acct_no as $key => $value) {
					$acctList[$key]['CUST_ID'] = $cust_id;
					$acctList[$key]['ACCT_NO'] = $value;
					$acctList[$key]['ACCT_NAME'] = $zf_filter2->acct_name[$key];
					$acctList[$key]['ACCT_CCY'] = $zf_filter2->acct_ccy[$key];
					//$acctList[$key]['ACCT_CCY'] = $tempAccCcy[$key];
					//$acctList[$key]['ACCT_OWNERSHIP'] = $tempAccOwn[$key];
				}

				$this->view->accDisplay = '-';
				$this->view->acctList = $acctList;

				//   echo("percobaan");
				// Zend_Debug::dump($acctList);die;
			}

			$docUploaded = false;

			if ($toUpload == true) {

				//document upload



				$attachmentDestination 	= UPLOAD_PATH . '/document/temp/';
				$adapter 				= new Zend_File_Transfer_Adapter_Http(array('ignoreNoFile' => true));

				$adapter->setDestination($attachmentDestination);

				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'pdf,jpg,jpeg'));
				$extensionValidator->setMessage(
					'Error: File extension has to be pdf/jpg/jpeg'
				);

				$maxsize = 4096000;
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxsize));
				$sizeValidator->setMessage(
					'Error: File size cannot exceed ' . ($maxsize / 1000000) . ' Mb'
				);

				$adapter->setValidators(array($extensionValidator, $sizeValidator));

				$files = $adapter->getFileInfo();

				$errorArray = array();

				$fileArr = array();

				foreach ($files as $fieldname => $fileinfo) {

					if (!empty($fileinfo["name"])) {
						if ($adapter->isValid($fileinfo["name"])) {
							$fileName[] = $fileinfo['name'];

							$newFileName = $cust_id . "_" . $fileinfo["name"];
							$adapter->addFilter('Rename', array('target' => $attachmentDestination . $newFileName, 'overwrite' => true));

							if ($adapter->receive($fileinfo["name"])) {
								$tempName = $cust_id . "_";
								if ($newFileName != $tempName)
									$fileArr[$fieldname] = $newFileName;
							} else {
								$errmsgs = $adapter->getMessages();
								foreach ($errmsgs as $key => $val) {
									$errfield = "error_" . $fieldname;
									$errorArray[$errfield] = $val;
								}
							}
						} else {
							$errmsgs = $adapter->getMessages();
							foreach ($errmsgs as $key => $val) {
								$errfield = "error_" . $fieldname;
								$errorArray[$errfield] = $val;
							}
						}
					}
				}
				//document -- ends

				if (!empty($errorArray)) {
					$docUploaded = false;
					foreach ($errorArray as $key => $val) {
						$this->view->$key = $val;
					}

					foreach ($validators as $key => $value) {
						$this->view->$key = ($zf_filter_input->isValid($key)) ? $zf_filter_input->getEscaped($key) : $this->_getParam($key);
					}
				} else {
					$docUploaded = true;
				}
			}

			$docUploaded = true;

			// echo "<pre>";
			// 		Zend_Debug::dump($this->_request->getParams());

			// 		echo "tes1";
			// 		Zend_Debug::dump($userList);
			// 		echo "tesw";
			// 		Zend_Debug::dump($acctList);

			$params = $this->_request->getParams();
			$repair = $this->_getParam('update');
			$cusdt = $this->_getParam('cust_id');

			$selectcusdt = $this->_db->select()
				->from('TEMP_ONLINECUST_BIS')
				->where('CUST_ID = ?', $cusdt);

			$selectchanges = $this->_db->fetchRow($selectcusdt);
			$changesid =  $selectchanges['CHANGES_ID'];

			// echo $repair;
			// echo $cusdt;

			// Zend_Debug::dump($params);die;


			if ($docUploaded == true) {

				if ($repair) {
					try {

						$this->_db->beginTransaction();

						$insertArr = $custData;

						$data2  = [
							'CHANGES_STATUS'	=> 'WA',
						];
						$where2 = ['CHANGES_ID = ?' => $changesid];
						$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);





						$model->updateMyOnlineRegs($insertArr, $cusdt);

						//Zend_Debug::dump($userList);die;
						if (!empty($userList)) {
							foreach ($userList as $row) {
								$insertUser = array();

								// $content1 = [

								// 'CUST_ID' => $row['CUST_ID'],
								// 'USER_ID' => $row['USER_ID'],
								// 'USER_FULLNAME' => $row['USER_FULLNAME'],
								// 'USER_PHONE' => $row['USER_PHONE'],
								// 'USER_EMAIL' => $row['USER_EMAIL'],
								// 'USER_PRIVI' => 'ADMIN'

								// ];

								//$insertUser['CUST_ID'] = $row['CUST_ID'];
								$insertUser['USER_ID'] = $row['USER_ID'];
								$insertUser['USER_FULLNAME'] = $row['USER_FULLNAME'];
								$insertUser['USER_PHONE'] = $row['USER_PHONE'];
								//$insertUser['USER_EXT'] = $row['USER_EXT'];
								$insertUser['USER_EMAIL'] = $row['USER_EMAIL'];
								//$insertUser['USER_PRIVI'] = $row['USER_PRIVI'];
								$insertUser['USER_NIK'] = $row['USER_NIK'];
								$insertUser['USER_PRIVI'] = $row['USER_PRIVI'];

								//$insert1 = $this->_db->insert('T_ONLINECUST_BIS_USER',$content1);

								// 	echo("<pre>");

								// echo "tussss";
								//Zend_Debug::dump($insertUser);die;


								$model->updateMyOnlineBisUser($insertUser, $cusdt);
								//$insert = $this->_db->insert('T_ONLINECUST_BIS_USER',$insertUser);
							}
						}

						if (!empty($acctList)) {
							$wheredel = array('CUST_ID = ?' => $cusdt);
							$this->_db->delete('T_ONLINECUST_BIS_ACC', $wheredel);

							foreach ($acctList as $row) {
								$insertAcc = array();

								$insertAcc['CUST_ID'] = $cusdt;
								$insertAcc['ACCT_NO'] = $row['ACCT_NO'];
								$insertAcc['ACCT_NAME'] = $row['ACCT_NAME'];
								$insertAcc['ACCT_CCY'] = $row['ACCT_CCY'];
								// $insertAcc['ACCT_OWNERSHIP'] = $row['ACCT_OWNERSHIP'];

								// echo "tesu";
								// Zend_Debug::dump($insertAcc);
								// die("disini die");

								//$insert1 = $this->_db->insert('T_ONLINECUST_BIS_ACC',$insertAcc);


								$model->insertMyOnlineBisAcc($insertAcc);
								//$inserts =$this->_db->insert('T_ONLINECUST_BIS_ACC',$insertAcc);
							}
						}



						$this->_db->commit();

						$template = $this->_getSettingBis('bemailtemplate_greeting_eregis');
						$templateEmailMasterBankAppName = $this->_getSettingBis('master_bank_app_name');
						$templateEmailMasterBankName = $this->_getSettingBis('master_bank_name');
						$masterBankEmail = $this->_getSettingBis('master_bank_email');
						$masterBankTlp = $this->_getSettingBis('master_bank_telp');
						$masterBankWapp = $this->_getSettingBis('master_bank_wapp');

						$cont_name = $this->_getParam('cust_contact');
						$comp_name = $this->_getParam('cust_name');
						$email_user = $this->_getParam('cust_email');

						$template = str_ireplace('[[contact_name]]', $cont_name, $template);
						$template = str_ireplace('[[company_name]]', $comp_name, $template);
						$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
						$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
						$template = str_ireplace('[[master_bank_email]]', $masterBankEmail, $template);
						$template = str_ireplace('[[master_bank_telp]]', $masterBankTlp, $template);
						$template = str_ireplace('[[master_bank_wapp]]', $masterBankWapp, $template);
						$template = str_ireplace('[[online_registration_number]]', empty($insertArr['CUST_REG_NUMBER']) ? $selectchanges['CUST_REG_NUMBER'] : $insertArr['CUST_REG_NUMBER'], $template);

						$subject = "Request Repair";
						//Application_Helper_Email::queueEmail($email,$subject,$template,__FILE__.__LINE__);
						// Application_Helper_Email::sendEmail($email_user,$subject,$template);


						// $this->view->business_type      = $businessEntityArr[$resultData['BUSINESS_TYPE']];



						//data utk dikirim ke view indexpdf

						$this->view->custRegNumber = $selectchanges['CUST_REG_NUMBER'] == '' ? '-' : $selectchanges['CUST_REG_NUMBER'];
						$this->view->custName = $custData['CUST_NAME'] == '' ? '-' : $custData['CUST_NAME'];
						$this->view->gopublic = $custData['GO_PUBLIC'] == 'Y' ? 'Y' : 'T';
						$this->view->grupbumn = $custData['GROUP_BUMN'] == 'Y' ? 'Y' : 'T';
						$this->view->compName = $custData['CUST_ID'] == '' ? '-' : $custData['CUST_ID'];
						$this->view->compNpwp = $custData['CUST_NPWP'] == '' ? '-' : $custData['CUST_NPWP'];
						$this->view->compType = $company_type[$custData['CUST_TYPE']];
						$this->view->cust_workfield = $custData['CUST_WORKFIELD'] == '' ? '-' : $custData['CUST_WORKFIELD'];
						$this->view->businessType = $business_type[$custData['CUST_WORKFIELD']];
						$this->view->address = $custData['CUST_ADDRESS'] == '' ? '-' : $custData['CUST_ADDRESS'];
						$this->view->city = $cityArr[$custData['CUST_CITY']];

						$this->view->debiturcode = $custData['DEBITUR_CODE'];
						$this->view->debiturname = $debiturArrc[$custData['DEBITUR_CODE']];
						$this->view->cust_village = $custData['CUST_VILLAGE'] == '' ? '-' : $custData['CUST_VILLAGE'];
						$this->view->cust_district = $custData['CUST_DISTRICT'] == '' ? '-' : $custData['CUST_DISTRICT'];
						$this->view->cust_district = $custData['CUST_DISTRICT'] == '' ? '-' : $custData['CUST_DISTRICT'];
						// $this->view->state = $country[$custData['COUNTRY_CODE']] == '' ? '-' : $custData['COUNTRY_CODE'];
						$this->view->country = $country[$custData['COUNTRY_CODE']];
						$this->view->contactPerson = $custData['CUST_CONTACT'] == '' ? '-' : $custData['CUST_CONTACT'];
						$this->view->custnumber = $custData['CUST_CONTACT_PHONE'] == '' ? '-' : $custData['CUST_CONTACT_PHONE'];
						$this->view->contactNumber = $custData['CUST_PHONE'] == '' ? '-' : $custData['CUST_PHONE'];
						$this->view->extension = $custData['CUST_EXT'] == '' ? '-' : $custData['CUST_EXT'];
						$this->view->faxNumber = $custData['CUST_FAX'] == '' ? '-' : $custData['CUST_FAX'];
						$this->view->contactEmail = $custData['CUST_EMAIL'] == '' ? '-' : $custData['CUST_EMAIL'];
						$this->view->compWebsite = $custData['CUST_WEBSITE'] == '' ? '-' : $custData['CUST_WEBSITE'];
						$this->view->zip = $custData['CUST_ZIP'] == '' ? '-' : $custData['CUST_ZIP'];
						$this->view->custApprove = $custData['CUST_APPROVER'];


						$this->view->serviceType = $custData['CUST_FINANCE'] == 1 ? 'Financial' : 'Non Financial';
						$this->view->limitIdr = $custData['CUST_LIMIT_IDR'] == '' ? '-' : $custData['CUST_LIMIT_IDR'];
						$this->view->limitUsd = $custData['CUST_LIMIT_USD'] == '' ? '-' : $custData['CUST_LIMIT_USD'];

						$this->view->userList = $userList;
						$this->view->fileName = $fileName;

						//generate pdf utk di attach ke email
						$outputHTML = "<tr><td>" . $this->view->render('/index/indexpdfnew.phtml') . "</td></tr>";

						//echo $outputHTML;die();

						$namefile = $this->language->_('E-Registration') . date('Y-m-d_H:i:s');

						$this->_helper->download->generatePdf(null, null, null, $namefile, $outputHTML, false);

						Application_Helper_Email::sendEmailWithFile($email_user, $subject, $template, 'E_Registration_' . date('Y_m_d_H:i:s') . '.pdf', LIBRARY_PATH . '/data/temp/' . $namefile . '.pdf');


						$this->_redirect('/registerechannel/index/success');

						// Application_Helper_Email::sendEmailWithFile($email_user,$subject,$template,'statement', LIBRARY_PATH.'/data/temp/eStatement_xxxxxxx_2020_02.pdf');
						// die('finish');
						// echo "<pre>";
						// print_r($pdf);die();

					} catch (Exception $e) {
						//echo $e->getMessage();
						//die("haha");
						//die("haha");
						$this->_db->rollBack();
						$this->view->error = true;
						$this->view->errMsg = "Failed to repair, please try again";

						foreach ($validators as $key => $value) {
							$this->view->$key = ($zf_filter_input->isValid($key)) ? $zf_filter_input->getEscaped($key) : $this->_getParam($key);
						}

						//unlink files uploaded
						foreach ($fileArr as $key => $val) {
							$folder = UPLOAD_PATH . '/document/temp/';
							$filename = $folder . $val;
							unlink($filename);
						}
					}
				} else {

					try {



						$this->_db->beginTransaction();

						$insertArr = $custData;






						$typeTitle = "Online Registration - Bank Guarantee Aplicant";
						$changeType = "N";

						// $change_id = $this->suggestionWaitingApproval($typeTitle,null,$changeType,null,null,'TEMP_MYONLINE_BIS','BIS',$insertArr['CUST_NAME']);

						$change_id = $this->suggestionWaitingApproval($typeTitle, null, $changeType, null, 'M_CUSTOMER', 'TEMP_ONLINECUST_BIS', 'BIS', $insertArr['CUST_NAME']);


						$insertArr['CUST_UPDATED']		= null;
						$insertArr['CUST_UPDATEDBY']	= null;
						$insertArr['CUST_CREATED'] = date('Y-m-d H:i:s');
						$insertArr['CUST_CREATEDBY'] = 'SYSTEM';
						$insertArr['CUST_SUGGESTED']	= null;
						$insertArr['CUST_SUGGESTEDBY']	= null;
						$insertArr['CUST_MODEL'] = '1';

						$insertArr['CHANGES_ID'] = $change_id;

						//$insertArr['CUST_ID'] = $this->_getParam('cust_id');






						// foreach ($fileArr as $key => $value) {
						// 	$insertArr[strtoupper($key)] = $value;
						// }

						// if(!isset($insertArr['OTHER_DOC']))
						// 	$insertArr['OTHER_DOC'] = "";

						$insertArr['CUST_LIMIT_IDR'] = (empty($insertArr['CUST_LIMIT_IDR'])) ? 0 : $insertArr['CUST_LIMIT_IDR'];
						$insertArr['CUST_LIMIT_USD'] = (empty($insertArr['CUST_LIMIT_USD'])) ? 0 : $insertArr['CUST_LIMIT_USD'];

						if ($insertArr['CUST_FINANCE'] == '3') {
							$insertArr['CUST_LIMIT_IDR'] = 1;
							$insertArr['CUST_LIMIT_USD'] = 1;
						}

						$date = date("dmy");
						$time = date("his");

						$generateregnum = "ONLREG" . $date . $time;


						$insertArr['CUST_REG_NUMBER'] = $generateregnum;
						$insertArr['BANK_BRANCH'] = $this->_getParam('bank_branch');
						$insertArr['GRUP_BUMN'] = $this->_getParam('group_bumn');





						// Insert temp
						// $this->print($cust_data);

						// 	echo "tussss";
						// 		Zend_Debug::dump($insertArr);
						// 		echo "aaaaa";
						// 		Zend_Debug::dump($userList);
						// 		 echo "lala";
						//  Zend_Debug::dump($acctList);



						//$this->_db->insert('T_ONLINECUST_BUS',$insertArr);

						//$id = $model->insertMyOnlineReg('bis',$insertArr);
						// echo "tussss";
						// Zend_Debug::dump($insertArr);
						//Zend_Debug::dump($insertArr);die;
						// Zend_Debug::dump($acctList);die;
						$model->insertMyOnlineRegs($change_id, $insertArr);
						// var_dump($change_id);die;

						if (!empty($userList)) {
							foreach ($userList as $row) {
								$insertUser = array();

								// $content1 = [

								// 'CUST_ID' => $row['CUST_ID'],
								// 'USER_ID' => $row['USER_ID'],
								// 'USER_FULLNAME' => $row['USER_FULLNAME'],
								// 'USER_PHONE' => $row['USER_PHONE'],
								// 'USER_EMAIL' => $row['USER_EMAIL'],
								// 'USER_PRIVI' => 'ADMIN'

								// ];

								$insertUser['CUST_ID'] = $row['CUST_ID'];
								$insertUser['USER_ID'] = $row['USER_ID'];
								$insertUser['USER_FULLNAME'] = $row['USER_FULLNAME'];
								$insertUser['USER_PHONE'] = $row['USER_PHONE'];
								//$insertUser['USER_EXT'] = $row['USER_EXT'];
								$insertUser['USER_EMAIL'] = $row['USER_EMAIL'];
								$insertUser['USER_NIK'] = $row['USER_NIK'];
								$insertUser['USER_PRIVI'] = $row['USER_PRIVI'];
								// $insertUser['BANK_BRANCH'] = '14';
								// $insertUser['BANK_BRANCH'] = $row['BANK_BRANCH'];


								//$insert1 = $this->_db->insert('T_ONLINECUST_BIS_USER',$content1);

								// 	echo("<pre>");

								// echo "tussss";
								// Zend_Debug::dump($insertUser);


								$model->insertMyOnlineBisUser($insertUser);
								//$insert = $this->_db->insert('T_ONLINECUST_BIS_USER',$insertUser);
							}
						}

						if (!empty($acctList)) {
							foreach ($acctList as $row) {
								$insertAcc = array();

								$insertAcc['CUST_ID'] = $row['CUST_ID'];
								$insertAcc['ACCT_NO'] = $row['ACCT_NO'];
								$insertAcc['ACCT_NAME'] = $row['ACCT_NAME'];
								$insertAcc['ACCT_CCY'] = $row['ACCT_CCY'];
								// $insertAcc['ACCT_OWNERSHIP'] = $row['ACCT_OWNERSHIP'];

								// echo "tesu";
								// Zend_Debug::dump($insertAcc);
								// die("disini die");

								//$insert1 = $this->_db->insert('T_ONLINECUST_BIS_ACC',$insertAcc);

								$model->insertMyOnlineBisAcc($insertAcc);
								//$inserts =$this->_db->insert('T_ONLINECUST_BIS_ACC',$insertAcc);
							}
						}







						$bankacc = $this->getCustomerAcct($custData['CUST_ID']);

						$this->view->bankacc = $bankacc;


						// $this->_db->insert('TEMP_ONLINECUST_BUS',$content);   




						//$id = $model->insertMyOnlineReg('bis',$insertArr);






						// if(!empty($userList)){
						// 	foreach($userList as $row){
						// 		$insertUser = array();

						// 		// $content1 = [

						// 		// 'CUST_ID' => $row['CUST_ID'],
						// 		// 'USER_ID' => $row['USER_ID'],
						// 		// 'USER_FULLNAME' => $row['USER_FULLNAME'],
						// 		// 'USER_PHONE' => $row['USER_PHONE'],
						// 		// 'USER_EMAIL' => $row['USER_EMAIL'],
						// 		// 'USER_PRIVI' => 'ADMIN'

						// 		// ];

						// 		$insertUser['CUST_ID'] = $row['CUST_ID'];
						// 		$insertUser['USER_ID'] = $row['USER_ID'];
						// 		$insertUser['USER_FULLNAME'] = $row['USER_FULLNAME'];
						// 		$insertUser['USER_PHONE'] = $row['USER_PHONE'];
						// 		//$insertUser['USER_EXT'] = $row['USER_EXT'];
						// 		$insertUser['USER_EMAIL'] = $row['USER_EMAIL'];
						// 		//$insertUser['USER_PRIVI'] = $row['USER_PRIVI'];
						// 		$insertUser['USER_PRIVI'] = 'ADMIN';

						// 		//$insert1 = $this->_db->insert('T_ONLINECUST_BIS_USER',$content1);

						// 	// 	echo("<pre>");

						// 	// Zend_Debug::dump($insertUser);die;
						// 		//$model->insertMyOnlineBisUser($insertUser);
						// 		$this->_db->insert('T_ONLINECUST_BIS_USER',$insertUser);
						// 	}
						// }

						// if(!empty($acctList)){
						// 	foreach($acctList as $row){
						// 		$insertAcc = array();

						// 		$insertAcc['CUST_ID'] = $row['CUST_ID'];
						// 		$insertAcc['ACCT_NO'] = $row['ACCT_NO'];
						// 		$insertAcc['ACCT_NAME'] = $row['ACCT_NAME'];
						// 		// $insertAcc['ACCT_CCY'] = $row['ACCT_CCY'];
						// 		// $insertAcc['ACCT_OWNERSHIP'] = $row['ACCT_OWNERSHIP'];

						// 		// echo "tesu";
						// 		// Zend_Debug::dump($insertAcc);
						// 		//die("asd");

						// 		//$insert1 = $this->_db->insert('T_ONLINECUST_BIS_ACC',$insertAcc);

						// 		//$model->insertMyOnlineBisAcc($insertAcc);
						// 		$this->_db->insert('T_ONLINECUST_BIS_ACC',$insertAcc);
						// 	}
						// }

						$this->_db->commit();

						$template = $this->_getSettingBis('bemailtemplate_greeting_eregis');
						$templateEmailMasterBankAppName = $this->_getSettingBis('master_bank_app_name');
						$templateEmailMasterBankName = $this->_getSettingBis('master_bank_name');
						$masterBankEmail = $this->_getSettingBis('master_bank_email');
						$masterBankTlp = $this->_getSettingBis('master_bank_telp');
						$masterBankWapp = $this->_getSettingBis('master_bank_wapp');

						$cont_name = $this->_getParam('cust_contact');
						$comp_name = $this->_getParam('cust_name');
						$email_user = $this->_getParam('cust_email');

						$template = str_ireplace('[[contact_name]]', $cont_name, $template);
						$template = str_ireplace('[[company_name]]', $comp_name, $template);
						$template = str_ireplace('[[master_bank_app_name]]', $templateEmailMasterBankAppName, $template);
						$template = str_ireplace('[[master_bank_name]]', $templateEmailMasterBankName, $template);
						$template = str_ireplace('[[master_bank_email]]', $masterBankEmail, $template);
						$template = str_ireplace('[[master_bank_telp]]', $masterBankTlp, $template);
						$template = str_ireplace('[[master_bank_wapp]]', $masterBankWapp, $template);
						$template = str_ireplace('[[online_registration_number]]', $insertArr['CUST_REG_NUMBER'], $template);

						$subject = "Greeting Registration";
						//Application_Helper_Email::queueEmail($email,$subject,$template,__FILE__.__LINE__);
						// Application_Helper_Email::sendEmail($email_user,$subject,$template);




						//Zend_Debug::dump($custData);die;
						//data utk dikirim ke view indexpdf
						$model = new registerechannel_Model_Registerechannel();

						$debiturArrc = Application_Helper_Array::listArray($model->getDebiturCode(), 'DEBITUR_CODE', 'DEBITUR_DESC');

						$this->view->custRegNumber = $insertArr['CUST_REG_NUMBER'] == '' ? '-' : $insertArr['CUST_REG_NUMBER'];
						$this->view->custName = $custData['CUST_NAME'] == '' ? '-' : $custData['CUST_NAME'];
						$this->view->gopublic = $custData['GO_PUBLIC'] == 'Y' ? 'Y' : 'T';
						$this->view->grupbumn = $custData['GROUP_BUMN'] == 'Y' ? 'Y' : 'T';
						$this->view->compName = $custData['CUST_ID'] == '' ? '-' : $custData['CUST_ID'];
						$this->view->compNpwp = $custData['CUST_NPWP'] == '' ? '-' : $custData['CUST_NPWP'];
						$this->view->compType = $company_type[$custData['CUST_TYPE']];
						$this->view->cust_workfield = $custData['CUST_WORKFIELD'] == '' ? '-' : $custData['CUST_WORKFIELD'];
						$this->view->businessType = $business_type[$custData['CUST_WORKFIELD']];
						$this->view->address = $custData['CUST_ADDRESS'] == '' ? '-' : $custData['CUST_ADDRESS'];
						$this->view->city = $cityArr[$custData['CUST_CITY']];

						$this->view->debiturcode = $custData['DEBITUR_CODE'];
						$this->view->debiturname = $debiturArrc[$custData['DEBITUR_CODE']];
						$this->view->cust_village = $custData['CUST_VILLAGE'] == '' ? '-' : $custData['CUST_VILLAGE'];
						$this->view->cust_district = $custData['CUST_DISTRICT'] == '' ? '-' : $custData['CUST_DISTRICT'];
						$this->view->cust_district = $custData['CUST_DISTRICT'] == '' ? '-' : $custData['CUST_DISTRICT'];
						// $this->view->state = $country[$custData['COUNTRY_CODE']] == '' ? '-' : $custData['COUNTRY_CODE'];
						$this->view->country = $country[$custData['COUNTRY_CODE']];
						$this->view->contactPerson = $custData['CUST_CONTACT'] == '' ? '-' : $custData['CUST_CONTACT'];
						$this->view->custnumber = $custData['CUST_CONTACT_PHONE'] == '' ? '-' : $custData['CUST_CONTACT_PHONE'];
						$this->view->contactNumber = $custData['CUST_PHONE'] == '' ? '-' : $custData['CUST_PHONE'];
						$this->view->extension = $custData['CUST_EXT'] == '' ? '-' : $custData['CUST_EXT'];
						$this->view->faxNumber = $custData['CUST_FAX'] == '' ? '-' : $custData['CUST_FAX'];
						$this->view->contactEmail = $custData['CUST_EMAIL'] == '' ? '-' : $custData['CUST_EMAIL'];
						$this->view->compWebsite = $custData['CUST_WEBSITE'] == '' ? '-' : $custData['CUST_WEBSITE'];
						$this->view->zip = $custData['CUST_ZIP'] == '' ? '-' : $custData['CUST_ZIP'];
						$this->view->custApprove = $custData['CUST_APPROVER'];


						$this->view->serviceType = $custData['CUST_FINANCE'] == 1 ? 'Financial' : 'Non Financial';
						$this->view->limitIdr = $custData['CUST_LIMIT_IDR'] == '' ? '-' : $custData['CUST_LIMIT_IDR'];
						$this->view->limitUsd = $custData['CUST_LIMIT_USD'] == '' ? '-' : $custData['CUST_LIMIT_USD'];

						$this->view->userList = $userList;
						$this->view->fileName = $fileName;

						//generate pdf utk di attach ke email
						$outputHTML = "<tr><td>" . $this->view->render('/index/indexpdfnew.phtml') . "</td></tr>";
						//echo $email_user;
						//echo $outputHTML;die();

						$namefile = $this->language->_('E-Registration') . date('Y-m-d_H:i:s');

						$this->_helper->download->generatePdf(null, null, null, $namefile, $outputHTML, false);
						//die('a');

						Application_Helper_Email::sendEmailWithFile($email_user, $subject, $template, 'E_Registration_' . date('Y_m_d_H:i:s') . '.pdf', LIBRARY_PATH . '/data/temp/' . $namefile . '.pdf');

						// Application_Helper_Email::sendEmailWithFile($email_user,$subject,$template,'statement', LIBRARY_PATH.'/data/temp/eStatement_xxxxxxx_2020_02.pdf');
						//die($sendEmail);
						// echo "<pre>";
						// print_r($pdf);die();
						//die;
						$this->_redirect('/registerechannel/index/success');
					} catch (Exception $e) {
						//echo $e->getMessage();
						//die("haha");
						//die("haha");
						$this->_db->rollBack();
						$this->view->error = true;
						$this->view->errMsg = "Failed to add data, please try again";

						foreach ($validators as $key => $value) {
							$this->view->$key = ($zf_filter_input->isValid($key)) ? $zf_filter_input->getEscaped($key) : $this->_getParam($key);
						}

						//unlink files uploaded
						foreach ($fileArr as $key => $val) {
							$folder = UPLOAD_PATH . '/document/temp/';
							$filename = $folder . $val;
							unlink($filename);
						}
					}
				}
			}
		} else {
			//die("hehe");
			$this->view->userDisplay = 'none';
			$this->view->accDisplay = 'none';
		}
	}

	public function successAction()
	{

		$setting = new Settings();
		$this->view->master_bank_name = $setting->getSetting('master_bank_name');
		$this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');
	}

	public function checkbalanceAction()
	{


		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		$ccy_id = strtoupper($this->_getParam('ccy_id'));
		//die($acct_no);

		$ccy_id_num = Application_Helper_General::getCurrNum($ccy_id);
		//             die ($cust_cif);
		$account    = new Account($acct_no, $ccy_id_num, NULL, NULL);


		$account->setFlag(false);
		$account->setFlagAddSourceAcct(false);
		$result    = $account->checkBalance();
		//echo json_encode($result);
		echo json_encode($result);
	}

	private function _getSettingBis($setting_id)
	{
		$select = $this->_db->select()
			->from(array('M_SETTING'), array('SETTING_VALUE'))
			->where('SETTING_ID = ?', $setting_id);

		return $this->_db->fetchOne($select);
	}

	public function sslDec($msg)
	{
		list($pass, $iv, $method) = $this->sslPrm();
		return trim(urldecode(openssl_decrypt(urldecode($msg), $method, $pass, false, $iv)));
	}

	public function sslPrm()
	{
		return array("6a1f325be4c0492063e83a8cb2cb9ae7", "IV (optional)", "aes-128-cbc");
	}

	public function getCustomerAcct($decrypcust_id)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'T_ONLINECUST_BIS_ACC'),
				array(
					'A.CUST_ID',
					'A.ACCT_NO',
					'A.ACCT_NAME',
					'CCY_ID' => 'A.ACCT_CCY',
					'A.ACCT_OWNERSHIP'
				)
			)
			->where('A.CUST_ID = ?', $decrypcust_id);
		return $this->_db->fetchAll($select);
	}
}
