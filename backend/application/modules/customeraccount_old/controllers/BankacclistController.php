<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class customeraccount_BankacclistController extends customeraccount_Model_Customeraccount
{
	protected $_idModule = 'RPAC';

	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
		
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
		$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());

		$listId = $this->_db->select()->distinct()
		->from(array('M_CUSTOMER_ACCT'),
			array('CUST_ID','CCY_ID'))
		->order('CUST_ID ASC')
		->query()->fetchAll();
		$this->view->listCustId = Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');
		$this->view->listCcyId = Application_Helper_Array::listArray($listId,'CCY_ID','CCY_ID');

		$status = $this->_masterglobalstatus;
		$options = array_combine(array_values($status['code']),array_values($status['desc']));
		$this->view->status = array(''=>'--- '.$this->language->_('Any Value').' ---');
		foreach($options as $key => $value){ if($key != 4) $optpayStatusRaw[$key] = $this->language->_($value); }
		//$this->view->status += $options;
		$this->view->status += $optpayStatusRaw;

		$fields = array	(
			'companyID' => array	(
				'field' => 'C.CUST_ID',
				'label' => $this->language->_('Company Code'),
				'sortable' => true
			),
			'companyname' => array	(
				'field' => 'CUST_NAME',
				'label' => $this->language->_('Company Name'),
				'sortable' => true
			),
			'acct'  					=> array	(
				'field' => 'ACCT_NO',
				'label' => $this->language->_('Account Number'),
				'sortable' => true
			),
			'accountname'  					=> array	(
				'field' => 'ACCT_NAME',
				'label' => $this->language->_('Account Name'),
				'sortable' => true
			),
			'currency'  					=> array	(
				'field' => 'CCY_ID',
				'label' => $this->language->_('CCY'),
				'sortable' => true
			),

			'acctdesc' => array	(
				'field' => 'ACCT_DESC',
				'label' => $this->language->_('Type'),
				'sortable' => true
			),

			'latestsuggestapp'   => array('field'    => 'ACCT_UPDATED',
				'label'    => $this->language->_('Approved Date'),
				'sortable' => true),

			'Status'  => array	(
				'field' => 'ACCT_STATUS',
				'label' => $this->language->_('Status'),
				'sortable' => true
			),
		);


		$filterlist = array('CUSTOMER_CODE','COMPANY_NAME','ACCOUNT_NUMBER','ACCOUNT_NAME','CCY','TYPE','APPROVE_DATE','STATUS');

		$this->view->filterlist = $filterlist;

		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy = $this->_getParam('sortby');
		$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$filterArr = array('filter' => array('StripTags','StringTrim'),
			'CUSTOMER_CODE' => array('StripTags','StringTrim','StringToUpper'),
			'COMPANY_NAME'  => array('StripTags','StringTrim','StringToUpper'),
			'CCY'  => array('StripTags','StringTrim','StringToUpper'),
			'ACCOUNT_NUMBER'    => array('StripTags','StringTrim','StringToUpper'),
			'ACCOUNT_NAME'  => array('StripTags','StringTrim','StringToUpper'),
			'TYPE' => array('StripTags','StringToUpper'),
			'STATUS' => array('StripTags','StringToUpper'),
			'APPROVE_START' => array('StripTags','StringTrim','StringToUpper'),
			'APPROVE_END' => array('StripTags','StringTrim','StringToUpper'),
		);

		$validators = array(
			'filter' => array(),
			'CUSTOMER_CODE'    => array(),
			'COMPANY_NAME'  => array(),
			'CCY'  => array(),
			'ACCOUNT_NUMBER'    => array(),
			'ACCOUNT_NAME'  => array(),
			'TYPE' => array(),
			'STATUS' => array(),
			'APPROVE_START' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'APPROVE_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		$dataParam = array('CUSTOMER_CODE','COMPANY_NAME','CCY','ACCOUNT_NUMBER','ACCOUNT_NAME','TYPE','STATUS','APPROVE_START','APPROVE_END');

		$dataParamValue = array();

		foreach ($dataParam as $no => $dtParam)
		{

			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if($value == "PS_LATEST_APPROVAL" || $value == "PS_LATEST_SUGGESTION"){
						$order--;
					}
					if($dtParam==$value){
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}

			}
		}

		if($this->_request->getParam('createdate')){
			$dataParamValue['APPROVE_START'] = $this->_request->getParam('createdate')[0];
			$dataParamValue['APPROVE_END'] = $this->_request->getParam('createdate')[1];
		}

		if(!empty($dataParamValue)){
			$filterlistdata = [];
			foreach($dataParamValue as $fil => $val){
				$paramTrx = $fil . "-" . $val;
				array_push($filterlistdata, $paramTrx);
			}
		}else{
			$filterlistdata = array("Filter - None");
		}

		$this->view->data_filter = $filterlistdata;

		$zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);

		$filter = $this->_getParam('filter');


		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$caseStatus = "(CASE CA.ACCT_STATUS ";
		foreach($options as $key=>$val)
		{
			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseStatus .= " END)";

		$select = $this->_db->select()
		->FROM (array('C' => 'M_CUSTOMER'),array('C.CUST_ID','C.CUST_NAME'))
		->JOINLEFT (array('CA' => 'M_CUSTOMER_ACCT'),'C.CUST_ID = CA.CUST_ID',array('CA.ACCT_NO','CA.ACCT_NAME','CA.ACCT_EMAIL','CA.CCY_ID','CA.ACCT_DESC','CA.ORDER_NO'))
		->JOINLEFT (array('G' => 'M_GROUPING'),'G.GROUP_ID = CA.GROUP_ID',array('G.GROUP_NAME','ACCT_STATUS' => $caseStatus,'CA.ACCT_SUGGESTED','CA.ACCT_SUGGESTEDBY','CA.ACCT_UPDATED','CA.ACCT_UPDATEDBY'));
		$select->where('CA.ACCT_NO IS NOT NULL');

		if($filter==$this->language->_('Set Filter'))
		{

			$companyid = html_entity_decode($zf_filter->getEscaped('CUSTOMER_CODE'));
			$companyname = html_entity_decode($zf_filter->getEscaped('COMPANY_NAME'));
			$accnumber = html_entity_decode($zf_filter->getEscaped('ACCOUNT_NUMBER'));
			$ccy_id = html_entity_decode($zf_filter->getEscaped('CCY'));
			$accname = html_entity_decode($zf_filter->getEscaped('ACCOUNT_NAME'));
			$stat = html_entity_decode($zf_filter->getEscaped('STATUS'));
			$type = html_entity_decode($zf_filter->getEscaped('TYPE'));
			$approve_start = html_entity_decode($zf_filter->getEscaped('APPROVE_START'));
			$approve_end = html_entity_decode($zf_filter->getEscaped('APPROVE_END'));

			if($companyid)
			{
				// $this->view->companyid = $companyid;
				$select->where("UPPER(C.CUST_ID) LIKE ".$this->_db->quote('%'.$companyid.'%'));
			}

			if($companyname)
			{
				// $this->view->companyname = $companyname;
				$select->where("UPPER(C.CUST_NAME) LIKE ".$this->_db->quote('%'.$companyname.'%'));
			}

			if($accnumber)
			{
				// $this->view->accnumber = $accnumber;
				$select->where("ACCT_NO LIKE ".$this->_db->quote('%'.$accnumber.'%'));
			}

			if($accname)
			{
				// $this->view->accname = $accname;
				$select->where("UPPER(CA.ACCT_NAME) LIKE ".$this->_db->quote('%'.$accname.'%'));
			}

			if($stat || $stat == "0")
			{
				// $this->view->stat = $stat;
				$select->where("CA.ACCT_STATUS LIKE ".$this->_db->quote($stat));
			}

			if($ccy_id)
			{
				// $this->view->ccy_id = $ccy_id;
				$select->where("CA.CCY_ID LIKE ".$this->_db->quote($ccy_id));
			}

			if($type)
			{
				// $this->view->groupname = $groupname;
				$select->where("ACCT_DESC LIKE ".$this->_db->quote('%'.$type.'%'));
			}

			if (!empty($approve_start) && !empty($approve_end)) {

				$select->where("DATE(ACCT_SUGGESTED) >= ?", date('Y-m-d', strtotime($approve_start)));
				$select->where("DATE(ACCT_SUGGESTED) <= ?", date('Y-m-d', strtotime($approve_end)));
			}

		}

		$header = Application_Helper_Array::simpleArray($fields, 'label');
		unset($header[14]);



		if($csv)
		{

			// $select = $this->_db->fetchall($select);

			$data = [];

			foreach ($this->_db->fetchall($select) as $row) {
				$subData = [];

				if($row['CCY_ID'] != 'IDR' && $row['CCY_ID'] != 'USD' && $row['CCY_ID'] != ''){
					$ccy = Application_Helper_General::getCurrCode($row['CCY_ID']);
				}else{
					$ccy = $row['CCY_ID'];
				} 

				$subData['CUST_ID'] = $row['CUST_ID'];
				$subData['CUST_NAME'] = $row['CUST_NAME'];
				$subData['ACCT_NO'] = $row['ACCT_NO'];
				$subData['ACCT_NAME'] = $row['ACCT_NAME'];
				$subData['CCY_ID'] = $ccy;
				$subData['ACCT_DESC'] = $row['ACCT_DESC'];
				$subData['ACCT_UPDATED'] = Application_Helper_General::convertDate($row['ACCT_UPDATED'],$this->displayDateTimeFormat,$this->defaultDateFormat);
				$subData['ACCT_STATUS'] = $row['ACCT_STATUS'];

				$data[] = $subData;
			}

			// $data = $this->_db->fetchall($select);



			$this->_db->insert('T_BACTIVITY',array(
				'LOG_DATE'         => new Zend_Db_Expr('now()'),
				'USER_ID'          => $this->_userIdLogin,
				'USER_NAME'        => $this->_userNameLogin,
				'ACTION_DESC'      => $this->_idModule,
				'ACTION_FULLDESC'  => 'Download CSV Customer User List',
			));

			$this->_helper->download->csv($header,$data,null,$this->language->_('Bank Account List Report'));

			// Application_Helper_General::writeLog($this->_idModule,'Importing CSV Customer > Bank Account List ');
		}
		else if($pdf)
		{
			$data = $this->_db->fetchall($select);
			$this->_helper->download->pdf($header,$data,null,$this->language->_('Bank Account List Report'));
			Application_Helper_General::writeLog($this->_idModule,'Importing PDF Customer > Bank Account List');
		}
		elseif($this->_request->getParam('print') == 1){

			if(!empty($this->_getParam('data_filter'))){
				$arrFilter = [];
				foreach($this->_getParam('data_filter') as $key=>$val){
					$arr = explode("-",$val) ;
					if($arr[0] == "APPROVE_START" || $arr[0] == "APPROVE_END"){
						$colmn = ltrim(rtrim($arr[0]));
						$value = $arr[1]."-".$arr[2]."-".$arr[3];
					}else{
						$colmn = ltrim(rtrim($arr[0]));
						$value = ltrim(rtrim($arr[1]));
					}
					$arrFilter[$colmn] = $value;
				}

				if(empty($arrFilter)){
					$val = $this->_getParam('data_filter');
					$arr = explode("-",$val) ;

					$colmn = ltrim(rtrim($arr[0]));
					$value = ltrim(rtrim($arr[1]));
					$arrFilter[$colmn] = $value;
				}

				$companyid       = html_entity_decode($arrFilter['CUSTOMER_CODE']);
				$companyname     = html_entity_decode($arrFilter['COMPANY_NAME']);
				$accnumber      = html_entity_decode($arrFilter['ACCOUNT_NUMBER']);
				$ccy_id   = html_entity_decode($arrFilter['CCY']);
				$accname    = html_entity_decode($arrFilter['ACCOUNT_NAME']);
				$stat    = html_entity_decode($arrFilter['STATUS']);
				$type    = html_entity_decode($arrFilter['TYPE']);
				$approve_start    = html_entity_decode($arrFilter['APPROVE_START']);
				$approve_end    = html_entity_decode($arrFilter['APPROVE_END']);

				if($companyid){
					$select->where("UPPER(C.CUST_ID) LIKE ".$this->_db->quote('%'.$companyid.'%'));
				}if($companyname){
					$select->where("UPPER(C.CUST_NAME) LIKE ".$this->_db->quote('%'.$companyname.'%'));
				}if($accnumber){
					$select->where("ACCT_NO LIKE ".$this->_db->quote('%'.$accnumber.'%'));
				}if($accname){
					$select->where("UPPER(CA.ACCT_NAME) LIKE ".$this->_db->quote('%'.$accname.'%'));
				}if($stat || $stat == "0"){
					$select->where("CA.ACCT_STATUS LIKE ".$this->_db->quote($stat));
				}if($ccy_id){
					$select->where("CA.CCY_ID LIKE ".$this->_db->quote($ccy_id));
				}if($type){
					$select->where("ACCT_DESC LIKE ".$this->_db->quote('%'.$type.'%'));
				}
				if (!empty($approve_start) && !empty($approve_end)) {
					$select->where("DATE(ACCT_SUGGESTED) >= ?", date('Y-m-d', strtotime($approve_start)));
					$select->where("DATE(ACCT_SUGGESTED) <= ?", date('Y-m-d', strtotime($approve_end)));
				}
			}

			$data = $this->_db->fetchall($select);
			unset($fields['action']);
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Bank Account List Report', 'data_header' => $fields));
		}
		else
		{
			Application_Helper_General::writeLog('ACLS','Viewing Customer > Bank Account List');
		}


		$select->order($sortBy.' '.$sortDir);
		$this->paging($select,30);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->modulename = $this->_request->getModuleName();

		if(!empty($dataParamValue)){
			$this->view->createdStart = $dataParamValue['APPROVE_START'];
			$this->view->createdEnd = $dataParamValue['APPROVE_END'];

			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}


	}

}
