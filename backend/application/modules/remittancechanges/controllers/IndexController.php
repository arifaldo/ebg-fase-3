<?php


require_once 'Zend/Controller/Action.php';


class remittancechanges_IndexController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()->distinct()
					->from(array('M_CUSTOMER'),array('CUST_ID'))
					->where("CUST_STATUS != '3'")
					->order('CUST_ID ASC')
				 	-> query() ->fetchAll();
		//Zend_Debug::dump($status); die;
		$this->view->var=$select;
		
		/*$select3 = $this->_db->select()
				->from(array('B' => 'M_CHARGES_WITHIN'),array('*'));
		$coba = $this->_db->fetchAll($select3);
		Zend_Debug::dump($select3); die;*/		
		
		$list = array(	''				=>	'--- '.$this->language->_('Any Value').' ---',
						'Enabled'		=>	$this->language->_('Enabled'),
						'Disabled' 		=> 	$this->language->_('Disabled'));
		$this->view->optionlist = $list;
		
		$ceklist = array(	'0'		=>	$this->language->_('Disabled'),
							'1' 	=> 	$this->language->_('Enabled'));
		
		$companyCode = $this->language->_('Company Code');
		$companyName = $this->language->_('Company Name');
		$monthlyFee = $this->language->_('Monthly Fee');
		$suggestedDate = $this->language->_('Suggested Date');
		$suggester = $this->language->_('Suggester');
		
		$fields = array	(
							'Company Code'  			=> array	(
																	'field' => 'B.CUST_ID',
																	'label' => $companyCode,
																	'sortable' => true
																),
							'Company Name'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => $companyName,
																	'sortable' => true
																),
							// 'Charges'  					=> array	(
							// 										'field' => 'CUST_CHARGES_STATUS',
							// 										'label' => $this->language->_('Charges'),
							// 										'sortable' => true
							// 									),
							// 'Monthly Fee'  		=> array	(
							// 										'field' => 'CUST_MONTHLYFEE_STATUS',
							// 										'label' => $monthlyFee,
							// 										'sortable' => true
							// 									),
							'Suggest Date'  		=> array	(
																	'field' => 'CUST_SUGGESTED',
																	'label' => $suggestedDate,
																	'sortable' => true
																),
							'Suggestor'  		=> array	(
																	'field' => 'CUST_SUGGESTEDBY',
																	'label' => $suggester,
																	'sortable' => true
																),
						);

		$filterlist = array('COMP_CODE','COMP_NAME','SUGEST_DATE','SUGESTBY');
		
		$this->view->filterlist = $filterlist;
	  
						
		$filterArr = array('filter' 			=> array('StripTags','StringTrim'),
	                       'COMP_CODE'    			=> array('StripTags','StringTrim'),
	                       'COMP_NAME'  			=> array('StripTags','StringTrim','StringToUpper'),
						   'SUGESTBY'  		=> array('StripTags','StringTrim','StringToUpper'),
						   // 'chargeStatus'  		=> array('StripTags','StringTrim'),
						   // 'monthlyfeeStatus'  	=> array('StripTags','StringTrim'),
						   'SUGEST_DATE'  		=> array('StripTags','StringTrim'),
						   'SUGEST_DATE_END'  			=> array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array('filter' 			=> array(),
	                       'COMP_CODE'    			=> array(),
	                       'COMP_NAME'  			=> array(),
						   'SUGESTBY'  		=> array(),
						   // 'chargeStatus'  		=> array(),
						   // 'monthlyfeeStatus'  	=> array(),
						   'SUGEST_DATE'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'SUGEST_DATE_END'  			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );
	     
		 $dataParam = array("COMP_CODE","COMP_NAME","SUGESTBY");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}

		if(!empty($this->_request->getParam('sugestdate'))){
				$createarr = $this->_request->getParam('sugestdate');
					$dataParamValue['SUGEST_DATE'] = $createarr[0];
					$dataParamValue['SUGEST_DATE_END'] = $createarr[1];
			}                 
	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    // $filter = $zf_filter->getEscaped('filter');
	    $filter 		= $this->_getParam('filter');
	    $custid = html_entity_decode($zf_filter->getEscaped('COMP_CODE'));
		$custname = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
		$suggestor = html_entity_decode($zf_filter->getEscaped('SUGESTBY'));
		// $chargeStatus = html_entity_decode($zf_filter->getEscaped('chargeStatus'));
		// $monthlyfeeStatus = html_entity_decode($zf_filter->getEscaped('monthlyfeeStatus'));
		$datefrom = html_entity_decode($zf_filter->getEscaped('SUGEST_DATE'));
		$dateto = html_entity_decode($zf_filter->getEscaped('SUGEST_DATE_END'));
		//Zend_Debug::dump($this->_masterglobalstatus); die;
		
		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');
		
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		/*if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}*/
		
		// $chargeStatusarr = "(CASE B.CUST_CHARGES_STATUS ";
  // 		foreach($ceklist as $key=>$val)
  // 		{
  //  			$chargeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  // 		}
  // 			$chargeStatusarr .= " END)";
		
  // 		$adminfeeStatusarr = "(CASE B.CUST_MONTHLYFEE_STATUS ";
  // 		foreach($ceklist as $key=>$val)
  // 		{
  //  			$adminfeeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  // 		}
  // 			$adminfeeStatusarr .= " END)";
  			
		$select2 = $this->_db->select()
						->from(array('B' => 'M_CUSTOMER'),array('B.CUST_ID',
																'B.CUST_NAME',
																// 'CUST_CHARGES_STATUS' => $chargeStatusarr, 
																// 'CUST_MONTHLYFEE_STATUS' => $adminfeeStatusarr,
																'B.CUST_SUGGESTED',
																'B.CUST_SUGGESTEDBY'))
						->where("CUST_STATUS != '3'");
		$arr = $this->_db->fetchAll($select2);
		
		//if($filter == null || $filter == 'Set Filter')
		if($filter == true)
		{
		 $this->view->fDateTo    = $dateto;
		 $this->view->fDateFrom  = $datefrom;
		//Zend_Debug::dump($custid); die;
		 
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
					//Zend_Debug::dump($dateto); die;	
	            }
		
		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}
		
		if($filter == true)
		{
			// if($chargeStatus)
			// {
			// 	$this->view->chargeStatus = $chargeStatus;
			// 	if($chargeStatus == "Enabled")
			// 	{
			// 		$cekchargeStatus = "1";
			// 	}
			// 	if($chargeStatus == "Disabled")
			// 	{
			// 		$cekchargeStatus = "0";
			// 	}
			// 	$select2->where("B.CUST_CHARGES_STATUS LIKE ".$this->_db->quote($cekchargeStatus));
			// }
			
			// if($monthlyfeeStatus)
			// {
			// 	$this->view->adminfeeStatus = $monthlyfeeStatus;
			// 	if($monthlyfeeStatus == "Enabled")
			// 	{
			// 		$cekadminfeeStatus = "1";
			// 	}
			// 	if($monthlyfeeStatus == "Disabled")
			// 	{
			// 		$cekadminfeeStatus = "0";
			// 	}
			// 	$select2->where("CUST_MONTHLYFEE_STATUS LIKE ".$this->_db->quote($cekadminfeeStatus));
			// }

		    if($custid)
		    {
	       		$this->view->cekcustid = $custid;
	       		$select2->where("B.CUST_ID LIKE ".$this->_db->quote($custid));
		    }
		    
			if($custname)
			{
	       		$this->view->custname = $custname;
	       		$select2->where("B.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));
			}
			
			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
	       		$select2->where("B.CUST_SUGGESTEDBY LIKE ".$this->_db->quote('%'.$suggestor.'%'));
		    }
		}
		
			// echo $select2;die;
		
		//$select2->order($sortBy.' '.$sortDir);
		$select2->order($sortBy.' '.$sortDir);                               
    	$this->paging($select2,30);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	if($csv || $pdf || $this->_request->getParam('print'))
    	{
    		$header = Application_Helper_Array::simpleArray($fields, "label");
    		$arr = $this->_db->fetchAll($select2);
    		
    		foreach($arr as $key=>$value)
			{
				$arr[$key]["CUST_SUGGESTED"] = Application_Helper_General::convertDate($value["CUST_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
    		
	    	if($csv)
			{
				Application_Helper_General::writeLog('CHLS','Download CSV Company Charges List');
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv($header,$arr,null,$this->language->_('Company Charges'));
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('CHLS','Download PDF Company Charges List');
				$this->_helper->download->pdf($header,$arr,null,$this->language->_('Company Charges'));
			}
		if($this->_request->getParam('print') == 1){
				//echo $select2;die;
				$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Company Charges', 'data_header' => $fields));
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('CHLS','View Company Charges List');
    	}


    	unset($dataParamValue['SUGEST_DATE_END']);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}