<?php

require_once 'Zend/Controller/Action.php';

class Changemanagement_ManageController extends Application_Main
{

	protected $_moduleDB  = 'MGS';

	public function indexAction()
	{
		$select = array();
		$displayDateFormat  = $this->_dateDisplayFormat;
		$databaseSaveFormat = $this->_dateDBFormat;
		$changesTypeCodeArr = $this->_suggestType['code'];

		$this->view->dateDisplayFormat   = $this->_dateDisplayFormat;
		$this->view->dateJqueryFormat    = $this->_dateJqueryFormat;
		$this->view->dateDBDefaultFormat = $this->_dateDBDefaultFormat;
		$this->view->suggestType   = $this->_suggestType;
		$this->view->suggestStatus = $this->_suggestStatus;

		$privi = $this->_priviId;	// get privi ID


		//get filtering param
		$filterArr = array(
			'filter'			=> array('StringTrim', 'StripTags'),
			'keyValue' 	  		=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'tableName' 		=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'suggestionType' 	=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'changesCreatedFrom' => array('StringTrim', 'StripTags'),
			'changesCreatedTo'	=> array('StringTrim', 'StripTags'),
		);
		$zf_filter = new Zend_Filter_Input($filterArr, array(), $this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->setbackURL();
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		/*	if($filter=='Set Filter' || $filter=='Clear Filter') {
			$this->view->showTable = true;
    */
		//define table header information 
		//format : 
		//array('label to use in param' => array('field' => 'field name of this field in db table',
		//									     'label' => 'label to use in view',
		//									     'sortable' => 'True/False, make field is sortable/not'
		//))
		$fields = array(
			'suggestionId' => array(
				'field' => 'CHANGES_ID',
				'label' => 'Suggestion Id',
				'sortable' => true
			),
			'suggestionDate' => array(
				'field' => 'CREATED',
				'label' => 'Suggest Date',
				'sortable' => true
			),
			'suggestionType'  => array(
				'field' => 'CHANGES_TYPE',
				'label' => 'Type',
				'sortable' => true
			),
			'tableName'    => array(
				'field' => 'DISPLAY_TABLENAME',
				'label' => 'Table Name',
				'sortable' => true
			),
			'keyValue'     => array(
				'field' => 'KEY_VALUE',
				'label' => 'Key Table',
				'sortable' => true
			),
			'suggestionInfo' => array(
				'field' => 'CHANGES_INFORMATION',
				'label' => 'Suggestion Information',
				'sortable' => true
			),
			'suggestionBy' => array(
				'field' => 'CREATED_BY',
				'label' => 'Suggest By',
				'sortable' => true
			),
			/*'suggestionId' => array('field' => 'CHANGES_ID', 
						  						    'label' => 'Suggestion Id',
						  						    'sortable' => true),
							*/
			'suggestionStatus' => array(
				'field' => 'CHANGES_STATUS',
				'label' => 'Suggestion Status',
				'sortable' => true
			),
			'note'			  => array(
				'field' => 'CHANGES_REASON',
				'label' => 'Note',
				'sortable' => false
			)
		);

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby');
		$sortDir = $this->_getParam('sortdir');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		//Zend_Debug::dump($sortBy);
		//Zend_Debug::dump($sortDir);						 	  
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		if ($filter == 'Set Filter') {
			//Zend_Debug::dump($zf_filter->getEscaped('departmentId'),'Department Id : ');

			$keyValue = html_entity_decode($zf_filter->getEscaped('keyValue'));
			$tableName = html_entity_decode($zf_filter->getEscaped('tableName'));

			$changesCreatedFrom = (Zend_Date::isDate($zf_filter->getEscaped('changesCreatedFrom'), $displayDateFormat)) ?
				new Zend_Date($zf_filter->getEscaped('changesCreatedFrom'), $displayDateFormat) :
				false;
			$changesCreatedTo = (Zend_Date::isDate($zf_filter->getEscaped('changesCreatedTo'), $displayDateFormat)) ?
				new Zend_Date($zf_filter->getEscaped('changesCreatedTo'), $displayDateFormat) :
				false;
			$suggestionType = (Zend_Validate::is($zf_filter->getEscaped('suggestionType'), 'InArray', array('haystack' => $changesTypeCodeArr))) ?
				$zf_filter->getEscaped('suggestionType') :
				false;

			$this->view->keyValue = $keyValue;
			$this->view->tableName = $tableName;

			if ($changesCreatedFrom)
				$this->view->changesCreatedFrom = $changesCreatedFrom->toString($displayDateFormat);
			if ($changesCreatedTo)
				$this->view->changesCreatedTo = $changesCreatedTo->toString($displayDateFormat);
			if ($suggestionType)
				$this->view->suggestionType = $suggestionType;
		}

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$whPriviID = "'" . implode("','", $privi) . "'";

			/*r  $sqlprv = $this->_db->select()
							->distinct()
							->from (array('M' => 'M_MODULE'), 
									array('M.SYSTEM_MODULE'))
							->join (array('A' => 'M_MODULE_ACTION'), 'M.MODULE_ID = A.MODULE_ID', array())
							->join (array('P' => 'M_BPRIVI_ACTION'), 'A.MODULE_ACTION_ID = P.MODULE_ACTION_ID', array())
							->where("M.SYSTEM_MODULE <> ''")
							->where("M.SYSTEM_MODULE IS NOT NULL")
							->where("P.BPRIVI_ID IN (".$whPriviID.")");  r*/




			/*$sqlprv = $this->_dbObj->select()
							->distinct()
							->from (array('M' => 'M_MODULE'), 
									array('M.MODULE_DESC'))
							->join (array('P' => 'M_BPRIVILEGE'), 'M.MODULE_ID = P.MODULE_ID', array())
							->where("P.BPRIVI_ID IN (".$whPriviID.")");*/

			//r $priviMod = $this->_db->fetchAll($sqlprv);




			//r if (!empty($priviMod))		// kalo priviModule is empty, means gak punya akses ke semua module, jadi gak bisa generate all report
			//r {
			//r $priviMod = Application_Helper_Array::simpleArray($priviMod,'SYSTEM_MODULE');
			//r  $whModuleDesc = "'".implode("','", $priviMod)."'";


			//compose query (use filtering and sorting)
			$select = $this->_db->select()
				->from(array('G' => 'T_GLOBAL_CHANGES'))
				->where('CHANGES_STATUS = ' . $this->_db->quote('WA') . ' OR CHANGES_STATUS = ' . $this->_db->quote('RR'))
				// ->where('CHANGES_FLAG = '.$this->_db->quote('B'))
				->where("G.MODULE IN (" . $whModuleDesc . ")")
				->where('UPPER(CREATED_BY) = ' . $this->_db->quote(strtoupper($this->_userIdLogin)))
				->order('CREATED DESC');



			//r }	// if (!empty($priviMod))
		}

		if ($filter == 'Set Filter') {
			//where clauses
			if ($keyValue)
				$select->where("UPPER(KEY_VALUE) LIKE " . $this->_db->quote('%' . $keyValue . '%'));
			if ($tableName)
				$select->where("UPPER(DISPLAY_TABLENAME) LIKE " . $this->_db->quote('%' . $tableName . '%'));
			if ($changesCreatedFrom)
				$select->where("CONVERT(date,CREATED) >= CONVERT(DATE," . $this->_db->quote($changesCreatedFrom->toString($databaseSaveFormat)) . ")");
			if ($changesCreatedTo)
				$select->where("CONVERT(date,CREATED) <= CONVERT(DATE," . $this->_db->quote($changesCreatedTo->toString($databaseSaveFormat)) . ")");
			if ($suggestionType)
				$select->where("CHANGES_TYPE = " . $this->_db->quote($suggestionType));
		}


		//order clause
		$select->order(array($sortBy . ' ' . $sortDir));

		/*r  $dataSelect = $this->_db->select()
                                                     ->from(array('BPG' => 'M_BPRIVI_GROUP'), array('BPG.BPRIVI_ID'))
                                                         ->join(array('BG' => 'M_BGROUP'), 'BPG.BGROUP_ID = BG.BGROUP_ID', array())
                                                         ->joinLeft(array('BP' => 'M_BPRIVILEGE'), 'BPG.BPRIVI_ID = BP.BPRIVI_ID',array())
                                                         ->where("BPG.BGROUP_ID IN (?)",'FVC002')
                                                         ->where("BP.BPRIVI_STATUS = 'A'")
                                                         ->where("BG.BGROUP_STATUS = 'A'"); r*/




		$this->view->fields = $fields;
		//$this->view->paginatorParam = $select;


		//process posted data
		if ($this->_request->isPost()) {

			$filterArr = array('change_del'	=> 'Digits');
			$zf_filter = new Zend_Filter_Input($filterArr, array(), $this->_request->getPost());

			$zf_filter = new Zend_Filter_Input($filterArr, array(), $this->_request->getPost());

			$deleted_id = array();
			$failed_id = array();
			$failed_message = array();
			$success_message = array();
			$clauseChanges = "CHANGES_STATUS = 'WA'";

			if (is_array($zf_filter->change_del)) {
				for ($i = 0; $i < count($zf_filter->change_del); $i++) {
					if (Zend_Validate::is($zf_filter->change_del[$i], 'Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $clauseChanges))) {
						$gcmodel = new Changemanagement_Model_Globalchanges($zf_filter->change_del[$i]); //,Zend_Registry::get('custLogin')
						$deleted = $gcmodel->delete();

						if ($deleted) {
							$deleted_id[$i]      = $zf_filter->change_del[$i];
							$success_message[$i] = $this->getErrorRemark('00', 'Suggestion ID', $gcmodel->getModuleId(), $zf_filter->change_del[$i]);
							$this->backendLog('DL', strtoupper($gcmodel->getModuleId()), $zf_filter->change_del[$i], $this->_getAllParams(), null);
						} else {
							$failed_id[$i]      = $zf_filter->change_del[$i];
							$failed_message[$i] = $this->getErrorRemark($gcmodel->getErrorCode(), null, $gcmodel->getModuleId());
							$this->backendLog('DL', strtoupper($gcmodel->getModuleId()), $zf_filter->change_del[$i], null, $this->getErrorRemark($gcmodel->getErrorCode(), null, $gcmodel->getModuleId()));
						}
					} else {
						$failed_id[$i]      = $zf_filter->change_del[$i];
						$failed_message[$i] = $this->getErrorRemark('22', 'Suggestion ID', $this->_moduleDB);
						$this->backendLog('DL', $this->_moduleDB, $zf_filter->change_del[$i], null, $this->getErrorRemark('22', 'Suggestion ID', $this->_moduleDB));
					}
				}
			}

			$this->view->deletedId = $deleted_id;
			$this->view->failedId  = $failed_id;
			$this->view->failedMessage  = $failed_message;
			$this->view->successMessage = $success_message;
			$this->view->deleted = true;
		} else {
			$this->backendLog('V', $this->_moduleDB, null, null, null);		// log for view
		}
		/* 	}
    	else
    	{
    		$this->backendLog('V',$this->_moduleDB,null,null,null);		// log for view
    		$this->view->showTable = false;
    	}
    */
		$this->paging($select);
	}

	//===============================================================================================================	
	/*public function detailAction() {
		
		$this->_db=Zend_Db_Table::getDefaultAdapter();
		
    	$this->_helper->layout()->setLayout('popup');
    	$id = Zend_Filter::filterStatic($this->_request->getParam('id'),'Digits');
    	$gcmodel = new Changemanagement_Model_Globalchanges($id);
    	$detailLink = $gcmodel->getDetailLink();
    	
    	$this->_forward($detailLink['action'],$detailLink['controller'],$detailLink['module'],$detailLink['params']);
	}
	*/
}
