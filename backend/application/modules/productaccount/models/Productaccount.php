<?php
Class productaccount_Model_Productaccount {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getData($fParam,$sortBy = null,$sortDir = null,$filter = null)
    {
		$select = $this->_db->select()
					        ->from(array('A' => 'M_PRODUCT_TYPE'),array('PRODUCT_ID','PRODUCT_CODE','PRODUCT_PLAN','PRODUCT_NAME','ACCT_TYPE')); 
		
		// if($filter == 'Set Filter')
		{  			
	        if(isset($fParam['fproduct_code'])) if(!empty($fParam['fproduct_code'])) $select->where('UPPER(PRODUCT_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fParam['fproduct_code']).'%'));
	        if(isset($fParam['fproduct_id'])) if(!empty($fParam['fproduct_id'])) $select->where('UPPER(PRODUCT_ID) = '.$this->_db->quote(strtoupper($fParam['fproduct_id'])));
	        if(isset($fParam['fplan_code'])) if(!empty($fParam['fplan_code']))    $select->where('PRODUCT_PLAN LIKE '.$this->_db->quote('%'.strtoupper($fParam['fplan_code']).'%'));
	        if(isset($fParam['fproduct_name'])) if(!empty($fParam['fproduct_name']))    $select->where('PRODUCT_NAME LIKE '.$this->_db->quote('%'.strtoupper($fParam['fproduct_name']).'%'));
		}
		if( !empty($sortBy) && !empty($sortDir) )
			$select->order($sortBy.' '.$sortDir);
		
		// echo $select;p
		// print_r($fParam);die;
       return $this->_db->fetchall($select);
    }
  
    public function getDetail($BANK_ID)
    {
		$data = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_PRODUCT_TYPE')) 
									 ->where("PRODUCT_ID=?", $BANK_ID)
							);
		
       return $data;
    }
  
    public function insertData($content)
    {
		$this->_db->insert("M_PRODUCT_TYPE",$content);
    }
  
    public function updateData($PRODUCT_ID,$content)
    {
		$whereArr  = array('PRODUCT_ID = ?'=>$PRODUCT_ID);
		$this->_db->update('M_PRODUCT_TYPE',$content,$whereArr);
    }
  
    public function deleteData($PRODUCT_ID)
    {
    	$this->_db->delete('M_PRODUCT_TYPE','PRODUCT_ID = '.$this->_db->quote($PRODUCT_ID));
//		$this->_db->delete('M_PRODUCT_TYPE','PRODUCT_ID = ?', $PRODUCT_ID);
    }
}