<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';

class debitbalance_DetailController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	protected $_bankName;

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
		$this->_transferStatus 	= $conf["transfer"]["status"];
	}
	
	

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$filter 			= new Application_Filtering();

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;

		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		$AESMYSQL = new Crypt_AESMYSQL();
		$DEBIT_NUMBER 			= urldecode($filter->filter($this->_getParam('debitno'), "debitno"));
		$DEBIT_NUMBER = $AESMYSQL->decrypt($DEBIT_NUMBER, $password);
		
		
				

		$pdf 				= $filter->filter($this->_getParam('pdf'), "BUTTON");
		$cancelfuturedatebtn = $filter->filter($this->_getParam('cancelfuturedate'), "BUTTON");
		$backBtn			= $filter->filter($this->_getParam('back'), "BUTTON");
		
		$data  = $this->_db->fetchRow(
                     $this->_db->select()
                               ->from(array('T' => 'T_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('*',
							   'd_sugest'=>'D.DEBIT_SUGGESTED',
							   'd_sugestby'=>'D.DEBIT_SUGGESTEDBY',
							   'd_approve'=>'D.DEBIT_APPROVED',
							   'd_approveby'=>'D.DEBIT_APPROVEDBY',
							   'd_status'=>'T.DEBIT_STATUS',
							   ))
							   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
							   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
							   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=T.DEBIT_NUMBER',array('USER_ID'))
							   ->joinleft(array('G' => 'M_USER'),'F.USER_ID=G.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
                               ->where('T.DEBIT_NUMBER = ?', $DEBIT_NUMBER)
                               );
		
		$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							->where('A.REG_NUMBER = ?',$REG_NUMBER);
							//echo '<pre>';
			//var_dump($data);die;				
		//$data = $this->_db->fetchRow($select);	
		
		
							if(!empty($data['USER_FULLNAME'])){
								$data['DEBIT_NAME'] = $data['USER_FULLNAME'];
							}else{
								$data['DEBIT_NAME'] = $data['CUST_NAME'];
							}	
							
							if(!empty($data['USER_EMAIL'])){
								$data['DEBIT_EMAIL'] = $data['USER_EMAIL'];
							}else{
								$data['DEBIT_EMAIL'] = $data['CUST_EMAIL'];
							}
						
		if($data['GROUP_ID'] == NULL){
			$data['GROUP_NAME'] = 'Others';
		}				
		
		if($data['d_status'] == '1'){
			$data['d_status'] = 'Active';
		}elseif($data['d_status'] == '4'){
			$data['d_status'] = 'Suspend';
		}elseif($data['d_status'] == '3'){
			$data['d_status'] = 'Delete';
		}else{
			$data['d_status'] = 'Unsuspend';
		}
		
		
		if($data['DEBIT_TYPE'] == '1'){
			$data['DEBIT_TYPE'] = 'Corporate';
		}else{
			$data['DEBIT_TYPE'] = 'Card Holder';
		}
		
		if($data['DEBIT_ATM'] == '1'){
			$data['DEBIT_ATM'] = 'On';
		}else{
			$data['DEBIT_ATM'] = 'Off';
		}
		
		if($data['DEBIT_EDC'] == '1'){
			$data['DEBIT_EDC'] = 'On';
		}else{
			$data['DEBIT_EDC'] = 'Off';
		}
		
		
		$datainq =  new Service_Account($data['ACCT_NO'],'000');
		$dataInquiry = $datainq->miniaccountInquiry('AB',FALSE); 
		
		//$systemBalance = new SystemBalance($this->_custIdLogin,$id,Application_Helper_General::getCurrNum($bank));
		//$systemBalance->setFlag(false);
		//$systemBalance->checkBalance();
		
		$temmpbalance =  $this->_db->select()
					              ->from(array('A' => 'T_DEBIT_BALANCE'))
					              ->where("A.ACCT_NO = ".$this->_db->quote($DEBIT_NUMBER))
					              //->where("A.CCY = ".$this->_db->quote($bank))
								  ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));
								 
								//echo $temmpbalance;
		$balance = $this->_db->fetchAll($temmpbalance);
								//  die('here');
								
								 
					
					             
		

		if($dataInquiry['ResponseCode'] == '00'){
					if(empty($balance)){
					                  $param = array(
					                     'ACCT_NO' => $id,
					                     
					                     'CUST_ID' => $this->_custIdLogin,
					                     
					                     'BALANCE' => $dataInquiry['Balance'],
					                     'DATE' => new Zend_Db_Expr("now()")
					                 );
					                 $this->_db->insert('T_DEBIT_BALANCE',$param);
					             }else{
					                $updateArr['BALANCE'] = $dataInquiry['Balance'];
									
									
					                $updateArr['DATE'] = new Zend_Db_Expr('now()');
					                $whereArr = array( #'CUST_ID = ?' => (string)$custId,
					                        'ACCT_NO = ?' => (string)$id,
					                       // 'BANK_CODE = ? ' => (string)$request['BANK_CODE'],
					                        'CUST_ID = ? ' => (string)$this->_custIdLogin
					                    );

					                try{
					                	$balanceupdate = $this->_db->update('T_DEBIT_BALANCE',$updateArr,$whereArr);
					                }catch (Exception $e) {
					                    print_r($e);die;
									}
					            }
								
			$datatemp = $this->_db->fetchAll(
			$this->_db->select()
                               ->from(array('T' => 'T_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('REG_NUMBER','VA_NUMBER'))
							   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
							   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
							   
							   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=T.DEBIT_NUMBER',array('USER_ID'))
							   ->joinleft(array('G' => 'M_USER'),'F.USER_ID=G.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
							   ->joinLeft(array('B' => 'T_DEBIT_BALANCE'),'B.CUST_ID = T.CUST_ID AND B.ACCT_NO = T.DEBIT_NUMBER',array('BALANCE',
					'rs_datetime' => 'DATE'))
                               //->where('D.VA_NUMBER = ?', $param['va'])
							   ->group('T.DEBIT_NUMBER')				 
				 //->order('B.GROUP_NAME DESC')
				 //->order('A.ORDER_NO ASC')
			);
			
			
			$resultdatagroup  = $this->_db->fetchAll(
                     $this->_db->select()
                               ->from(array('T' => 'T_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('REG_NUMBER','VA_NUMBER'))
							   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
							   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
							   
                               //->where('D.VA_NUMBER = ?', $param['va'])
							   ->group('GROUP_ID')
                               );
			//echo $data;die;
			//echo '<pre>';
			//var_dump($data);die;
			
					// echo $data;die;
			

			
			
			$save['lastupdate']	= date('Y-m-d H:i:s');
			$save['data'] = $datatemp;
			$save['resultdatagroup'] = $resultdatagroup;
			$frontendOptions = array ('lifetime' => 2419200,
									  'automatic_serialization' => true );
			$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
			$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
			$cacheID = 'DBCB'.$this->_custIdLogin;
	        $cache->save($save,$cacheID);
								
					$data['BALANCE'] = 'IDR '.Application_Helper_General::displayMoneyplain($dataInquiry['Balance']);
					//break;
		}else{
					$data['BALANCE'] = 'IDR N/A';
					//break;
		}
		
		$this->view->debitno = $DEBIT_NUMBER;
		$this->view->data = $data;


		
	}
}
