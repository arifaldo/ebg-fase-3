<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'CMD/Validate/Validate.php';
require_once("Service/Account.php");

class customeraccount_ConfirmController extends customeraccount_Model_Customeraccount
{

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $cust_id = strtoupper($this->_getParam('cust_id'));
    $cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;
    $this->view->CCYData = $this->getCcy();

    $acct_email    = strip_tags(trim($this->_getParam('acct_email')));
    $ccy_id        = strtoupper(strip_tags(trim($this->_getParam('ccy_id'))));
    $acct_no       = strip_tags(trim($this->_getParam('acct_no')));
    $acct_alias    = strip_tags(trim($this->_getParam('acct_alias')));
    $acct_balance  = strip_tags(trim($this->_getParam('acct_balance')));
    $acct_owner    = strip_tags(trim($this->_getParam('acct_owner')));

    $this->view->specialRate = $_SESSION['special_rate'];

    if ($cust_id) {
      $select = $this->_db->select()
        ->from('M_CUSTOMER', array('CUST_ID', 'CUST_CIF', '*'))
        ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
        ->where('CUST_STATUS!=3');
      $result = $this->_db->fetchRow($select);
      if (!$result) {
        $cust_id = null;
      } else {
        $cust_cif = $result['CUST_CIF'];
      }

      $this->view->data = $result;
    }

    //print_r($cust_cif);die;

    $error_remark = null;

    if (!$cust_id) {
      $error_remark = 'invalid cust id';
      //insert log
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
    }

    //jika acct status terdahulu adalah deleted, maka tampilkan notice :
    //pengecekan status account, jika status deleted maka tipe suggestion jadi edit, selain itu tipenya add
    $acctInfo = $this->getAccStatus($acct_no);

    // $acct_is_deleted = false;

    // if (!empty($acctInfo)) {
    //   if ($acctInfo['ACCT_STATUS'] == 3) {
    //     $acct_is_deleted = true;
    //     $this->view->cust_id_deleted   = $acctInfo['CUST_ID'];

    //     $custInfo = $this->getCustData($acctInfo['CUST_ID']);
    //     $this->view->cust_name_deleted = $custInfo['CUST_NAME'];
    //   }
    // }
    // $this->view->acct_is_deleted = $acct_is_deleted;

    if ($this->_request->isPost()) {
      $exclude_fgroup_id = '(UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id);
      $exclude_fgroup_id .= ' OR UPPER(CUST_ID)=' . $this->_db->quote('BANK');
      $exclude_fgroup_id .= ') AND UPPER(FGROUP_STATUS)=' . $this->_db->quote(strtoupper($this->_masterStatus['code']['active']));

      $filters = array(
        'acct_no'     => array('StripTags', 'StringTrim', 'StringToUpper'),
        'ccy_id'      => array('StripTags', 'StringTrim'),
        'acct_email'  => array('StripTags', 'StringTrim'),
        'cust_id'     => array('StripTags', 'StringTrim', 'StringToUpper'),
        'acct_alias'  => array('StripTags', 'StringTrim'),
      );

      $TokenError = array();

      $validators =  array(
        'cust_id'        => array(),
        'cust_alias'     => array(
          'NotEmpty',
          'messages' => array('Can not be empty')
        ),
        'acct_no'        => array(
          'NotEmpty',
          'Digits',
          array('StringLength', array('min' => 10, 'max' => 20)),
          // array('Db_NoRecordExists', array('table' => 'M_CUSTOMER_ACCT', 'field' => 'ACCT_NO', 'exclude' => 'ACCT_STATUS!=3')),
          // array('Db_NoRecordExists', array('table' => 'TEMP_CUSTOMER_ACCT', 'field' => 'ACCT_NO')),
          'messages' => array(
            'Can not be empty',
            'Account Number must be numeric',
            'Account Number length must be between 10 and 16 digits',
            // 'Account already assigned. Please use another',
            // 'Account Number ' . $acct_no . ' already suggested! Please Approve or Reject previous suggestion first!',
          )
        ),
        // 'ccy_id'         => array(
        //   'NotEmpty',
        //   'messages' => array('Can not be empty')
        // ),
        'acct_email'     => array( //new Application_Validate_EmailAddress(),
          array('StringLength', array('max' => 128)),
          'allowEmpty' => true,
          'messages'   => array('Email lenght cannot be more than 128')
        ),
        /* 'acct_balance'  => array('NotEmpty',
											         'messages' => array(
											                             'Can not be empty',
												                        )
											        ),	*/
      );

      $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

      //--------------validasi tambahan-------------------
      $cust_id_acc_flag = true;

      if ($zf_filter_input->acct_no) {
        //integrate ke core

        $data = $this->_db->fetchRow(
          $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER_ACCT'))
            ->join(array('C' => 'M_MAKERLIMIT'), 'A.ACCT_NO = C.ACCT_NO', array())
            ->where("A.ACCT_STATUS = 1")
            ->where("A.CUST_ID = " . $this->_db->quote($this->_custIdLogin))
            ->where("C.USER_LOGIN = " . $this->_db->quote($this->_userIdLogin))
            ->where("C.MAXLIMIT > 0")
            ->group('A.ACCT_NO')
            ->order('A.ORDER_NO ASC')
        );



        $accountInfo = $this->getAccountInfo($zf_filter_input->acct_no, null, null);

        $callService = new Service_Account('', '', '', '', '', $accountInfo['cif']);
        $inquiryCif = $callService->inquiryCIFAccount();

        $inquiryCif = array_shift(array_filter($inquiryCif['accounts'], function ($account) use ($acct_no) {
          if (strpos($acct_no, $account['account_number']) !== false) return true;
        }));

        if ($accountInfo['response_desc'] != 'Success') //00 = success
        {
          $cust_id_acc_flag = false;
          $cust_id_acc_type = 1;
          $error_msg = $accountInfo['response_desc'];
        }
      }
      //----------END validasi tambahan-------------------

      if ($zf_filter_input->acct_email) {
        $validate = new validate;
        $cek_multiple_email = $validate->isValidEmailMultiple($zf_filter_input->acct_email);
      } else {
        $cek_multiple_email = true;
      }

      // Zend_Debug::dump($cust_id_acc_flag);
      // Zend_Debug::dump($cek_multiple_email);
      // Zend_Debug::dump($zf_filter_input->getMessages());
      // die('here'); 
      if ($zf_filter_input->isValid() && $cust_id_acc_flag == true && $cek_multiple_email == true) {
        $info = 'Account No = ' . $zf_filter_input->acct_no;
        $acct_data = $this->_acctData;
        //print_r($acct_data);die;                  
        foreach ($validators as $key => $value) {
          if ($zf_filter_input->$key) $acct_data[strtoupper($key)] = $zf_filter_input->$key;
        }

        try {
          $this->_db->beginTransaction();
          //print_r($accountInfo);die;
          $acct_data['ACCT_STATUS']      = 1;     //1 = unsuspend
          $acct_data['ACCT_SUGGESTED']   = new Zend_Db_Expr('now()');
          $acct_data['ACCT_SUGGESTEDBY'] = $this->_userIdLogin;
          $acct_data['ACCT_NO'] = $acct_no;
          // print_r($accountInfo);die;
          // data from core
          // $acct_alias       = strip_tags( trim($this->_getParam('acct_alias')) );
          // print_r($zf_filter_input->acct_alias);
          $acct_data['CCY_ID']     = $accountInfo['currency'];
          $acct_data['ACCT_NAME']     = $accountInfo['account_name'];
          $acct_data['ACCT_ALIAS_NAME'] = $acct_alias;
          // $acct_data['ACCT_DESC']     = $accountInfo['type_desc'];
          $acct_data['ACCT_DESC']     = $inquiryCif['type_desc'];
          $acct_data['ACCT_TYPE']     = $accountInfo['account_type'];
          $acct_data['ACCT_SOURCE']     = $accountInfo['type'];

          /* $acct_data['ACCT_RESIDENT']   	= $accountInfo['Citizenship'];
          $acct_data['ACCT_CITIZENSHIP']   	= $accountInfo['National'];
          $acct_data['ACCT_CATEGORY']   	= $accountInfo['Category'];
          $acct_data['ACCT_ID_TYPE']   	= $accountInfo['IdentificationType'];
          $acct_data['ACCT_ID_NUMBER']   	= $accountInfo['IdentificationNumber'];
          */
          // echo '<pre>';
          // print_r($acct_data);die;
          // if (!empty($acctInfo)) { // commented by 23 mei 2023
            // if ($acctInfo['ACCT_STATUS'] == 3 || $acctInfo['ACCT_STATUS'] == 1 || $acctInfo['ACCT_STATUS'] == 2) {
              // $change_id = $this->suggestionWaitingApproval('Bank Account', $info, $this->_changeType['code']['edit'], null, 'M_CUSTOMER_ACCT', 'TEMP_CUSTOMER_ACCT', $zf_filter_input->acct_no, $acct_data['ACCT_NAME'], $zf_filter_input->cust_id);
            // }
          // } else {
          // }
          $change_id = $this->suggestionWaitingApproval('Bank Account', $info, $this->_changeType['code']['new'], null, 'M_CUSTOMER_ACCT', 'TEMP_CUSTOMER_ACCT', $zf_filter_input->acct_no, $acct_data['ACCT_NAME'], $zf_filter_input->cust_id);
          $this->insertTempCustomerAcct($change_id, $acct_data);

          //log CRUD
          Application_Helper_General::writeLog('ACAD', 'Bank Account has been Added, Acct No : ' . $zf_filter_input->acct_no . ' Acct Name : ' . $acct_data['ACCT_NAME'] . ' Change id : ' . $change_id);

          $this->_db->commit();

          $this->_redirect('/notification/submited/index');
          //$this->_redirect('/notification/success/index/cust_id/'.$cust_id);
        } catch (Exception $e) {
          $this->_db->rollBack();

          echo $e->getMessage();
          die;
          SGO_Helper_GeneralLog::technicalLog($e);
        }

        if (isset($error_remark)) {
          //insert log
          try {
            $this->_db->beginTransaction();
            $fulldesc = Application_Helper_General::displayFullDesc($_POST);
            $this->backendLog(strtoupper($this->_changeType['code']['new']), strtoupper($this->_moduleID['user']), null, $fulldesc, $error_remark);
            $this->_db->commit();
          } catch (Exception $e) {
            $this->_db->rollBack();
            SGO_Helper_GeneralLog::technicalLog($e);
          }

          $this->_helper->getHelper('FlashMessenger')->addMessage('F');
          $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
          $this->_redirect($this->_backURL);
        } else {
          $this->view->success = 1;
          $msg = '';
          $this->view->user_msg = $msg;
        }
      } else {
        $this->view->error = 1;
        $this->view->acct_no = ($zf_filter_input->isValid('acct_no')) ? $zf_filter_input->acct_no : $this->_getParam('acct_no');
        $this->view->acct_alias = ($zf_filter_input->isValid('acct_alias')) ? $zf_filter_input->acct_alias : $this->_getParam('acct_alias');
        //$this->view->user_email = ($zf_filter_input->isValid('user_email'))? $zf_filter_input->user_email : $this->_getParam('user_email');
        $this->view->user_group = ($zf_filter_input->isValid('fgroup_id')) ? $zf_filter_input->fgroup_id : $this->_getParam('fgroup_id');

        $error = $zf_filter_input->getMessages();
        /*if(count($error))$error_remark = $this->displayErrorRemark($error);
        $this->view->user_msg = $this->displayError($error);*/

        $errorArray = null;
        //konfigurasi error cust id jika didelete
        if ($cust_id_acc_flag == false) {
          if ($cust_id_acc_type == 1) {
            $errorArray['acct_no'] = $result['errorMessage']; // error dari host / TO
          } else if ($cust_id_acc_type == 2) {
            $errorArray['acct_no'] = 'NOTICE : This account is assigned (already deleted) to Company : [' . $custDataBasedAcc['CUST_NAME'] . ' �(' . $custDataBasedAcc['CUST_ID'] . ')]';
          }
        }

        //format error utk ditampilkan di view html 

        foreach ($error as $keyRoot => $rowError) {
          foreach ($rowError as $errorString) {
            $errorArray[$keyRoot] = $errorString;
          }
        }
        // print_r($errorArray);die;
        $this->view->custAcct_msg  = $errorArray;
      }
    } else {
      $this->view->cust_id      = $cust_id;
      $this->view->acct_no      = $acct_no;
      $this->view->acct_email   = $acct_email;
      $this->view->ccy_id       = $ccy_id;

      //get data from core
      $acct_info = $this->getAccountInfo($acct_no, $ccy_id, $cust_cif);

      $this->view->acct_balance = $acct_info['acct_balance'];
      $this->view->acct_owner   = $acct_info['acct_owner'];
      $this->view->acct_alias   = $acct_alias;
      $this->view->acct_desc    = $acct_info['product_type'];
      $this->view->acct_type    = $acct_info['account_type'];
      $this->view->acct_status  = $acct_info['acct_status'];
      $this->view->currency = ($acct_info["currency"]) ?: "-";

      $select = $this->_db->select()
        ->from('M_CUSTOMER', array('CUST_NAME'))
        ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id));
      $this->view->cust_name = $this->_db->fetchOne($select);

      $callService = new Service_Account('', '', '', '', '', $acct_info['cif']);
      $inquiryCif = $callService->inquiryCIFAccount();

      $inquiryCif = array_shift(array_filter($inquiryCif['accounts'], function ($account) use ($acct_no) {
        if (strpos($acct_no, $account['account_number']) !== false) return true;
      }));
    }

    $select = $this->_db->select()
      ->from('M_FGROUP', array('FGROUP_ID', 'FGROUP_NAME'))
      ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id) . ' OR UPPER(CUST_ID)=' . $this->_db->quote('BANK'))
      ->where('UPPER(FGROUP_STATUS)=' . $this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
    $this->view->fgroup = $this->_db->fetchAll($select);

    $acct_info = $this->getAccountInfo($acct_no, $ccy_id, $cust_cif);

    $this->view->acct_balance = $acct_info['acct_balance'];
    $this->view->acct_owner   = $acct_info['account_name'];
    // $this->view->acct_desc    = $acct_info['type_desc'];
    $this->view->acct_desc    = ucfirst(strtolower($inquiryCif['type_desc']));
    $this->view->acct_status  = $acct_info['status'];
    $this->view->ccy_id       = $ccy_id;

    $select = $this->_db->select()
      ->from('M_CUSTOMER', array('CUST_NAME'))
      ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id));
    $this->view->cust_name = $this->_db->fetchOne($select);
    // print_r($acct_info);die;
    $this->view->cust_id = $cust_id;

    $this->view->modulename = $this->_request->getModuleName();

    //insert log
    /* try
    {
      $this->_db->beginTransaction();
      if($this->_request->isPost())
      {
        $action = strtoupper($this->_changeType['code']['new']);
        $fulldesc = Application_Helper_General::displayFullDesc($_POST);
      }
      else
      {
      	$action = strtoupper($this->_actionID['view']);
      	$fulldesc = 'CUST_ID:'.$cust_id;
      }
      $this->backendLog($action,strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
      $this->_db->commit();
    }
      catch(Exception $e)
      {
    	  $this->_db->rollBack();
      SGO_Helper_GeneralLog::technicalLog($e);
    }*/
    Application_Helper_General::writeLog('ACAD', '');
  }


  //information from core
  private function getAccountInfo($acct_no, $ccy_id, $cif)
  {
    //integrate ke core
    $svcAccount = new Service_Account($acct_no, null);
    $result = $svcAccount->inquiryAccontInfo('AI', TRUE);

    if ($result['response_desc'] == 'Success' || $result['response_code'] == '0000' || $result['response_code'] == '00') //00 = success
    {
      $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
      $result2 = $svcAccountCIF->inquiryCIFAccount();

      $filterBy = $result['account_number']; // or Finance etc.
      $new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
        return ($var['account_number'] == $filterBy);
      });

      $singleArr = array();
      foreach ($new as $key => $val) {
        $singleArr = $val;
      }

      $svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);

      // if ($singleArr['type_desc'] == 'Deposito') {
      if ($singleArr['type_desc'] == 'Deposito' || $svcAccountBalance->inquiryDeposito()['response_code'] == '0000') {
        $result3 = $svcAccountBalance->inquiryDeposito();
      } else {
        $svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
        $result3 = $svcAccountBalance->inquiryAccountBalance();
      }
    }

    $result = array_merge($result, $singleArr, $result3);

    return $result;
  }

  /* private function getAcctDesc($acct_no,$ccy_id)
  {
		$result = $this->getAccountInfo($acct_no,$ccy_id); 
  
		return $result['product_type']; 
  } */
}
