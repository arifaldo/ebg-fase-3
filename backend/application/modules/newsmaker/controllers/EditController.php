<?php

require_once 'Zend/Controller/Action.php';


class newsmaker_EditController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$HELP_ID   	= $this->_getParam('ID');
		$this->view->HELP_ID = $HELP_ID;
		
		$filterArr = array(	'filter'=> array('StringTrim','StripTags'),
							'submit'=> array('StringTrim','StripTags'));
		
		$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$filter 	= $this->_getParam('filter');
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}	
		}

		$attahmentDestination 	= UPLOAD_PATH . '/document/help/';		
		$errorRemark 			= null;
		$adapter 				= new Zend_File_Transfer_Adapter_Http();

		$select = $this->_db->select()
										->from(	'M_CONTENT',
												array(
														'ID',
												'IMAGE', 
												'TITLE',
												'NEWS', 
												'CREATED', 
												'CREATEDBY', 
												'NEWS',
												'READMORE',
												'TARGET',
												'EFDATE',
												'EXPDATE',
												'DATE_TYPE',
												'STATUS',
												'TYPE'))
										->where('ID =?',$HELP_ID);

		$data = $this->_db->fetchRow($select);

			if($this->_request->isPost())
			{
				$params = $this->_request->getParams();
				$fileExt 				= "png";


				$sourceFileName = $adapter->getFileName();

					if($sourceFileName == null)
					{
						$sourceFileName = null;
						$fileType = null;
					}
					else
					{
						$sourceFileName = substr(basename($adapter->getFileName()), 0);
						if($_FILES["document"]["type"])
						{
							$adapter->setDestination($attahmentDestination);
							$maxFileSize 			= $this->getSetting('Fe_attachment_maxbyte');
							$fileType = $adapter->getMimeType();
							$size = $_FILES["document"]["size"];
						}
						else
						{
							$fileType = null;
							$size = null;
						}
					}
				$paramsName['sourceFileName'] 	= $sourceFileName;




				$errorRemark = null;
				
				$filters = array(
									'helpTopic'	=> array('StripTags', 'StringTrim'),
									'description'	=> array('StripTags', 'StringTrim'),
									'type'	=> array('StripTags', 'StringTrim'),
									'CROSS_CURR'	=> array('StripTags', 'StringTrim'),
									'postdatetype'	=> array('StripTags', 'StringTrim'),
									'news'	=> array('StripTags', 'StringTrim'),
								);
				$exclude = "HELP_ID != $HELP_ID and HELP_ISDELETED != 1";
				$validators = array(
										'sourceFileName' => array(
															'NotEmpty',
															array('Db_NoRecordExists',array(
																								'table'=>'T_HELP',
																								'field'=>'HELP_FILENAME',
																								'exclude'   => array(
																														'field' => 'HELP_ISDELETED',
																														'value' => '1'
																													)
																							)
																),
															'messages' => array(
																					$this->language->_("Error : File Upload cannot be left blank or File Size too large"),
																					"Error : File ".$sourceFileName.$this->language->_(" Already Exist")
																				)
														),
										'helpTopic' => array(
																	'NotEmpty',
																	array('StringLength', array('max' => 50)),
																	new Zend_Validate_Regex('/^[a-zA-Z 0-9]*$/'),
																	array('Db_NoRecordExists',array(
																									'table'=>'T_HELP',
																									'field'=>'HELP_TOPIC',
																									'exclude'=>$exclude
																								)
																			),
																	'messages' => array(
																							$this->language->_("Error : Help Topic cannot be left blank"),
																							$this->language->_("Error : Help Topic size more than 50 char"),
																							$this->language->_("Error : Help Topic Format Must Alpha Numeric"),
																							$this->language->_("Error : Help Topic Already Exist")
																						)
															),
										'description' => array(),
										'type' => array('sourceFileName' => array(
															'NotEmpty',

														),
													'messages' => array(
																						$this->language->_("Error : Type cannot be left blank"),
																	)
													),
										'CROSS_CURR'	=> array(),
										'postdatetype'	=> array(),
										'news'	=> array(),	
										'target' => array(),	
									);
									
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
				$description 	= ($zf_filter_input->isValid('description'))? $zf_filter_input->description : $this->_request->getParam('description'); 
				$helpTopic 		= ($zf_filter_input->isValid('helpTopic'))? $zf_filter_input->helpTopic : $this->_request->getParam('helpTopic'); 
			
				// $helpTopic 		= $this->getRequest()->getParam('helpTopic');
				// $description 	= $this->getRequest()->getParam('description');

				$expdate = Application_Helper_General::convertDate($params['to_date'],$this->_dateDBFormat,$this->defaultDateFormat).' '.$params['to_time'];

				$curDate = date("Y-m-d H:i:s");

				if($params['postdatetype'] == 1){
					$postdate = $data['EFDATE'];
					if ($postdate > $expdate) {
						$status = 2;
					}
					else if ($curDate < $postdate) {
						$status = 3;
					}
					else{
						$status = 1;
					}

				}else{
					$postdate = Application_Helper_General::convertDate($params['from_date'],$this->_dateDBFormat,$this->defaultDateFormat).' '.$params['from_time'];

					if ($postdate > $expdate) {
						$status = 2;
					}
					else if ($curDate < $postdate) {
						$status = 3;
					}
					else{
						$status = 1;
					}
				}


				if($zf_filter_input->isValid())
				{

					if (!empty($_FILES['document']['name'])) {
						$fileTypeMessage = explode('/',$fileType);
						$fileType =  $fileTypeMessage[1];
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
						$extensionValidator->setMessage("Extension file must be *.png");

						$maxFileSize = "1024000";
						$size = number_format($size);

						$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
						$sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");
						
						$adapter->setValidators(array($extensionValidator, $sizeValidator));

						if($adapter->isValid())
						{
							$newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
							$adapter->addFilter('Rename', $newFileName);
							/*$fileType = $adapter->getMimeType();*/

							if($adapter->receive())
							{
								$imageName = $newFileName;
							}
						}
						else{
							$this->view->error = true;
							$errors = array($adapter->getMessages());
							$this->view->errorMsg = $errors;
						}
					}else{
						$imageName = $data['IMAGE'];
					}

					
					try
					{
						$this->_db->beginTransaction();
						// 'ID',
						// 						'IMAGE', 
						// 						'TITLE',
						// 						'NEWS', 
						// 						'CREATED', 
						// 						'CREATEDBY', 
						// 						'TYPE'
						$param = array(
							'IMAGE'			=> $imageName,
								'TITLE' 			=> $params['helpTopic'],
								'NEWS'		=> $params['news'],
								'CREATEDBY'			=> $this->_userIdLogin, 
								'CREATED' 		=> new Zend_Db_Expr('now()'), 
								'TYPE'		=> $params['type'],
								'STATUS'	=> $status,
								'READMORE'  => $params['CROSS_CURR'],
								'EFDATE'	=> $postdate,
								'EXPDATE'	=> $expdate,
								'TARGET'	=> $params['target'],
								'DATE_TYPE' => $params['postdatetype'],
						);	
						
						$where = array('ID = ?' => $HELP_ID);
						$query = $this->_db->update ( "M_CONTENT", $param, $where );
						$this->_db->commit();
						Application_Helper_General::writeLog('HLUD','Edit News. News ID: ['.$HELP_ID.'], File name : ['.$data['HELP_FILENAME'].']');
						$this->setbackURL('/newsmaker');
						$this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						$this->_db->rollBack();
						/*die($e);
						$errorRemark = $this->getErrorRemark('82');
						$this->view->errorMsg = array(array($errorRemark));*/
					//	SGO_Helper_GeneralLog::technicalLog($e);
					}
				}
				else
				{
					$this->view->error 				= true;
					$errors 						= $zf_filter_input->getMessages();
					
					$this->view->helpTopicErr			= (isset($errors['helpTopic']))? $errors['helpTopic'] : null;
					$this->view->descriptionErr 		= (isset($errors['description']))? $errors['description'] : null;
				}	
			}
		
		$this->view->file = $data['IMAGE'];
		if($this->_request->isPost())
		{
			$this->view->helpTopic = (isset($zf_filter_input->helpTopic)) ? $zf_filter_input->helpTopic : $this->getRequest()->getParam('helpTopic');
			$this->view->description = (isset($zf_filter_input->description)) ? $zf_filter_input->description : $this->getRequest()->getParam('description');
		}
		else
		{
			$this->view->helpTopic = $data['TITLE'];
			$this->view->type = $data['TYPE'];
			$this->view->readmore = $data['READMORE'];
			$this->view->target = $data['TARGET'];
			$this->view->datetype = $data['DATE_TYPE'];
			$this->view->efdate = $data['EFDATE'];
			$this->view->expdate = $data['EXPDATE'];

			//image path
			$path  = UPLOAD_PATH . '/document/help/';
            $files = scandir($path);
            $files = array_diff(scandir($path), array('.', '..'));
            $dh = opendir($path);
            while($file = readdir($dh)) {
	            if($file == $data['IMAGE']){
	                $imageData = base64_encode(file_get_contents($path.''. $data['IMAGE']));
	                $image = $path.''. $data['IMAGE'];
	                $src = 'data: '.mime_content_type($image).';base64,'.$imageData;
	            }
            }

			$this->view->imagepath = $src;
			
			
			$description = $data['NEWS'];
			$this->view->news = $description;
		}
		$disable_note_len 	 = (isset($description))  ? strlen($description)  : 0;	
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
		
		$disable_note_len 	 = 100 - $disable_note_len;		
		$note_len 	 = 100 - $note_len;	

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('HLUD','Viewing Edit Help');
		}
						
	}
}
