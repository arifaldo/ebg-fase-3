<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SweepPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'CMD/Validate/ValidatePaymentOpen.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';

class opensweep_SamebankController extends Application_Main
{

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
		$this->view->maxdate = $setting->getSetting('range_futuredate');
		
	}


	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$custID = $this->_custIdLogin;
		$userID = $this->_userIdLogin;
		$this->view->ccyArr = $this->getCcy();
		$CustomerUser = new CustomerUser($custID, $userID);




		
		$cust_id = $this->_custIdLogin;
		
		$selecthistory = $this->_db->select()
			->from(array('T_FORECAST'), array('*'))
			->where('F_CUST_ID = ?', $cust_id)
			//->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();
		$datahistory = array();
		foreach ($selecthistory as $key => $value) {
				$datahistory[$key]['title'] = 'IDR '.Application_Helper_General::displayMoney($value['F_AMOUNT']);
				$datahistory[$key]['start'] = $value['F_DATE'];
				$datahistory[$key]['color'] = '#007bff';
		}
		
		foreach($selecthistory as $row) {
			$dataDate[] = $row['F_DATE'];
			$dataAmount[] = $row['F_AMOUNT'];
		}

		$history = array_combine($dataDate, $dataAmount);

		$this->view->historyCal = $history;
		$this->view->dHistory = $selecthistory;
		$this->view->hdata = json_encode($datahistory);

		$selectholiday = $this->_db->select()
			->from(array('M_HOLIDAY'), array('*'))
			->query()->fetchall();
		$dataholiday = array();
		foreach ($selectholiday as $key => $value) {
			$dataholiday[$key]['start'] = $value['HOLIDAY_DATE'];
			$dataholiday[$key]['display'] = 'background';
		}

		$this->view->holidayData = json_encode($dataholiday);

		

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;	

		$select = $this->_db->select()
                          ->from(array('MAB'=>'M_APP_BOUNDARY'))
                          ->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
                          ->where('MAB.CUST_ID = ?', (string)$cust_id)
                          ->group('BOUNDARY_ID');
                    // echo $select;
                  $result = $this->_db->fetchAll($select);

                  $dataArr = array();
                  // $alf = 'A';
                  // $alf++;
                  // ++$alf;
                  // print_r((int)$alf);
                  // $nogroup = sprintf("%02d", 1);
                  // print_r($result);die;
          
          
                  foreach($result as $row){
                    list($grouptype,$groupname,$groupnum) = explode("_",$row['GROUP_USER_ID']);
                    if($grouptype == 'N')
                      $name = 'Group '.trim($groupnum,'0');
                    else
                      $name = 'Special Group';

                    $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
                                // echo $selectUser;die;
                                ->query()->fetchall();

                    $userlist = '';

          $policythen = explode(' THEN ', $row['POLICY']);

          foreach ($policythen as $keythen => $valuethen) {
          

                    $policy = explode(' AND ', $valuethen);

                    foreach ($policy as $key => $value) {
                      $replaceVal = str_replace('(', '', $value);
                      $replaceVal = str_replace(')', '', $replaceVal);
                      $policy[$key] = $replaceVal;
                    }

                    foreach ($policy as $key => $value) {
                      if($value == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist))
                            $userlist .= $val['USER_ID'];
                          else
                            $userlist .= ', '.$val['USER_ID'];
                        }                         
                      }else{



                        $alphabet = array("A"=>1,"B"=>2,"C"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>26,);

                        $policyor = explode(' OR ', $value);

                        // print_r($policyor);die;
                        foreach ($policyor as $numb => $valpol) {
                          if($valpol == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist)){
                            $userlist .= $val['USER_ID'];
                          }
                          else{
                            $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }else{
                          $nogroup = sprintf("%02d", $alphabet[$valpol]);
                          // print_r($valpol);
                          $group = 'N_'.$cust_id.'_'.$nogroup;
                          $selectUser = $this->_db->select()
                                  ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                  ->where('GROUP_USER_ID = ?', $group)
                                  // echo $selectUser;die;
                                  ->query()->fetchall();  
                                // print_r($selectUser);  
                          foreach($selectUser as $val){
                            if(empty($userlist))
                              $userlist .= $val['USER_ID'];
                            else
                              $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }
                        

                      }
                    }
          
          }
                    // print_r($userlist);die;
                    // $nogroup = sprintf("%02d", $key+1);

                    // foreach($selectUser as $val){
                    //  if(empty($userlist))
                    //    $userlist .= $val['USER_ID'];
                    //  else
                    //    $userlist .= ', '.$val['USER_ID'];
                    // }

                    $arrTraType     = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
                    $arrTraType['19'] = 'Cash Pooling Same Bank';
                    $arrTraType['20'] = 'Cash Pooling Same Bank';
                    $arrTraType['23'] = 'Cash Pooling Other Bank';


          $userl = explode(', ',$userlist);
          $userlist = array_unique($userl);
          $userlistnew = '';
          
          foreach($userlist as $ky  => $val){
            //var_dump($ky);
            if($ky == 0){
            $userlistnew .= $val;
            //var_dump($userlistnew);
            }else{
            $userlistnew .= ', '.$val;
            }
          }

              $changepolicy = explode(" ", $row['POLICY']);
              $data = array();
              foreach ($changepolicy as $keypolicy => $valpolicy) {

                if ($valpolicy == 'AND') {
                  $valpolicy = '<span style="color: blue;">and</span>';
                }elseif ($valpolicy == 'OR') {
                  $valpolicy = '<span style="color: blue;">or</span>';
                }elseif ($valpolicy == 'THEN') {
                  $valpolicy = '<span style="color: blue;">then</span>';
                }

                $replacepolicy1 = str_replace('(', '<span style="color: blue;">(</span>', $valpolicy);
                $replacepolicy2 = str_replace(')', '<span style="color: blue;">)</span>', $replacepolicy1);

                $data[$keypolicy] = $this->language->_($replacepolicy2);
              }

              $valpolicy2 = implode(" ", $data);

                    $dataArr[] = array( 
                              'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
                              'CURRENCY' => $row['CCY_BOUNDARY'],
                              'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN'])." - ".Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
                              'GROUP_NAME' => $valpolicy2,
                              'USERS' => $userlistnew);
                  }

          //var_dump($dataArr);
                  // print_r($dataArr);die;
                 /* print_r($row);die();*/

                  $this->view->dataBoundary = $dataArr;




		/*
		 * generate payment ref
		 */

		$banklist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'), array('A.BANK_CODE'))
				// ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
				//  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
				//  // ->join(array('D' => 'M_DOMESTIC_BANK_TABLE'),'D.BANK_NAME = B.BANK_NAME',array('D.SWIFT_CODE'))
				->where('A.CUST_ID = ?', $this->_custIdLogin)
				->group('A.BANK_CODE')
			// ->where("A.BANK_CODE IN ('008','014','013','009','032','031','153','002') ")
			// ->order('A.APIKEY_ID ASC')
		);
		// var_dump($banklist);
		$par = array();
		if (!empty($banklist)) {
			foreach ($banklist as $key => $value) {
				$par[] = $value['BANK_CODE'];
			}
		}
		// var_dump($par);
		// var_dump($banklist);die;
		if (empty($par)) {
			$par = array('000');
		}

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'))
			->where('C.BANK_CODE IN (?)', $par);
		// echo $selectbank;die;
		$databank 					= $this->_db->fetchAll($selectbank);

		$this->view->BankArr = $databank;


		$this->view->paymentreff = $this->generatePaymentReff(1); //send payment ref to view

		$this->view->sourceType = 2; //source type, 1 = single select, 2 = upload

		$complist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_USER'), array('CUST_ID'))

				->where("A.USER_ID = ? ", $this->_userIdLogin)
		);
		// echo $complist;;die;
		// var_dump($complist);die;
		$comp = "'";
		// print_r($complist);die;
		foreach ($complist as $key => $value) {
			$comp .= "','" . $value['CUST_ID'] . "','";
		}
		$comp .= "'";


		$acctlist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'))
				->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
				->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
				// ->where('A.ACCT_STATUS = ?','5')
				->where("A.CUST_ID IN (" . $comp . ")")
				->order('A.APIKEY_ID ASC')
			// echo $acctlist;
		);
		// echo $acctlist;die;
		// echo '<pre>';
		// echo $acctlist;
		// print_r($acctlist);die;
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}

		$acct = array();
		foreach ($account as $key => $value) {
			$acct[$key - 1]['ACCT_NO'] = $value['account_number'];
			$acct[$key - 1]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$key - 1]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$key - 1]['ACCT_NAME'] = $value['account_name'];
		}
		// var_dump($acct);
		$AccArr = $acct;


		$paramscr = array('CCY_IN' => 'IDR');
		$this->view->sourceAcc = $AccArr;
		
		
		$TRA_MIN_AMOUNT = $this->_getParam('TRA_MIN_AMOUNT');
					
		//if (empty($TRA_MIN_AMOUNT)) {
			$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				foreach($ccyList as $k => $valccy){
					if($valccy['CCY_ID'] == 'IDR'){
						$minamt = str_replace('.00', '', Application_Helper_General::displayMoney($valccy['MIN_AMT']));
						$this->view->TRA_MIN_AMOUNT = $minamt;
					}
				}
			//var_dump($ccyList);die;
				
				//}
		//		$this->view->sourceAcc = $CustomerUser->getAccounts();
		// 		print_r($this->view->sourceAcc);die;
		//$sourceAcc =  $CustomerUser->getAccounts();

		$select   = $CustomerUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		$select->where("B.BENEFICIARY_ISAPPROVE = ?", 1);

		// $resultBeneficiary = $this->_db->fetchAll($select);
		// $this->view->listBeneficiary = $resultBeneficiary;
		// $this->view->listBeneficiary = $AccArr;
		$this->view->listBeneficiary = array('' => ' -- Please Bank -- ');

			

		/*
		 * Verify form
		*/

		$sessionNameRand = new Zend_Session_Namespace('openSweep');
		if ($this->_request->isPost()) {

			$isConfirmPage = $this->_getParam('confirmPage');
			$confirmPage = true;
			$TRA_MIN_AMOUNT = $this->_getParam('TRA_MIN_AMOUNT');
			if (empty($TRA_MIN_AMOUNT)) {
										$error = true;
										$confirmPage = false;
										$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Min amount cannot be left blank') . '.';
										$this->view->error      = true;
			}else{
				$minamount = str_replace('.00', '', Application_Helper_General::displayMoney($minamt));
				$tra_min_amount = str_replace('.00', '', Application_Helper_General::displayMoney($TRA_MIN_AMOUNT));
				$this->view->TRA_MIN_AMOUNT = $tra_min_amount;
				//var_dump($minamount);
				//var_dump($tra_min_amount);
				if((int)$minamount>(int)$tra_min_amount){
										$error = true; 
										$confirmPage = false;
										$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Min amount cannot lower than IDR ') .$minamt. '.';
										$this->view->error      = true;
				}
			}
			
			//var_dump($error_msg);
		//var_dump($error);die;
			//if hal pertama di hit, akan msk ke confirm  page
			if (!$isConfirmPage) {

				$sourceType = $this->_getParam('sourceType');
				$acctBank = $this->_getParam('ACCT_BANK');
				$beneBank = $this->_getParam('BENEF_BANK');

				if (empty($acctBank)) {
					$this->view->error = true;
					$confirmPage = false;
					$error_msg[] = 'Bank cannot be left blank. Please correct it';
					$this->view->error_msg = $error_msg;
				}

				//var_dump($sourceType);die;
				if ($sourceType == 1) { //single source
					$acSource = $this->_getParam('ACCTSRC');
					
				} else { //upload file
					// echo "<pre>";
					// var_dump($this->_request->getParams());
					$adapter = new Zend_File_Transfer_Adapter_Http();

					$adapter->setDestination($this->_destinationUploadDir);
					$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
					$extensionValidator->setMessage(
						$this->language->_('Error') . ': ' . $this->language->_('Extension file must be') . ' *.txt'
					);

					$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
					$sizeValidator->setMessage(
						'Error: File exceeds maximum size'
					);

					$adapter->setValidators(array(
						$extensionValidator,
						$sizeValidator,
					));

					$sourceFileName = substr_replace(basename($adapter->getFileName()), '', 100);

					$extension = explode('.', $sourceFileName);

					$extensionName = $extension[1];

					$newFileName = $adapter->getFileName() . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter('Rename', $newFileName);
					$error = false;
					// var_dump($newFileName);
					if ($adapter->isValid())
					// if (true)
					{

						if ($adapter->receive()) {

							//               	$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
							// $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

							$adapter->addFilter('Rename', $newFileName);
							// $adapter->receive();
							$myFile = file_get_contents($newFileName);
							$arry_myFile = explode("\n", $myFile);
							@unlink($newFileName);
							// echo "<pre>";

							$datasource = array();
							$sourceno = 0;
							if (!empty($arry_myFile)) {
								// var_dump($arry_myFile);die;
								foreach ($arry_myFile as $key => $value) {
									if ($key == 0) {
										$datafile = explode('|', $value);
										$file_id = $datafile['1'];
										$file_date = $datafile['4'] . '-' . sprintf("%02d", $datafile['3']) . '-' . sprintf("%02d", $datafile['2']);
										$total_trx = $datafile['5'];
										$ccy_trx = $datafile['6'];
									}
									$dataex = explode('|', $value);
									if(empty($dataex['2']) || $dataex['2'] == ''){
										//var_dump($dataex);
										//die('here1');
										$error = true;
										$confirmPage = false;
										$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Remain Balance cannot be left blank') . '.';
										$this->view->error      = true;
										break;
									}
									if ($dataex['0'] == '1') {
										$datasource[$sourceno]['ACCT_NO'] = $dataex['1'];
										$datasource[$sourceno]['REMAIN_BALANCE'] = $dataex['2'];
										$datasource[$sourceno]['DESC'] = $dataex['3'];
										$datasource[$sourceno]['CUST_REF'] = $dataex['4'];
										$datasource[$sourceno]['NOTIF_SMS'] = '';
										$datasource[$sourceno]['NOTIF_EMAIL'] = '';
										$sourceno++;
									}
								}

								foreach ($datasource as $key => $value) {
									$checkdata = $this->_db->fetchAll(
										$this->_db->select()
											->from(array('A' => 'M_APIKEY'), array('*'))
											->where("A.VALUE = ? ", $value['ACCT_NO'])
											->where("A.CUST_ID = ? ", $this->_custIdLogin)
										);
									if(empty($checkdata)){
										$error = true;
										$confirmPage = false;
										$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Account is not registered') . '.';
										$this->view->error      = true;	
									}
								}

								if (count($datasource) >= 50) {
									$error = true;
									$confirmPage = false;
									$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Max 50 data') . '.';
									$this->view->error      = true;
								}

								if (count($datasource) != $total_trx) {
									$error = true;
									$confirmPage = false;
									$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('total data file invalid') . '.';
									$this->view->error      = true;
								}
							}

							if ($this->_getParam('accteopen') == 1) {
								$input_file_id = $this->_getParam('FILE_IDBENE');
							} else {
								$input_file_id = $this->_getParam('FILE_ID');
							}

							if (strtoupper($input_file_id) != 'BYPASSIDFU') {
								if (strtoupper($input_file_id) != strtoupper($file_id)) {
									$error = true;
									$confirmPage = false;
									$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File ID or Uploaded Date not matched. Please check your uploaded file') . '.';
									$this->view->error      = true;
								}
							}


							if ($file_date != date('Y-m-d')) {
								$error = true;
								$confirmPage = false;
								$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File ID or Uploaded Date not matched. Please check your uploaded file') . '.';
								$this->view->error      = true;
							}

							// var_dump($arry_myFile);die;
							// $getTypeArr = explode(".", $sourceFileName);
							// $getType = $getTypeArr[1];

							// var_dump($newFileName);

							// $data = $this->_helper->parser->parseCSV($newFileName);

							// @unlink($newFileName);

							$acSource = $datasource;
							$acBenef = $this->_getParam('ACBENEF');
							if (!empty($datasource)) {
								foreach ($datasource as $key => $value) {
									if ($value['ACCT_NO'] == $acBenef) {
										$error = true;
										$this->view->error = true;
										$confirmPage = false;
										$error_msg[] = 'Source Account cannot be same with Beneficiary Account';
									}
								}
							} else //kalo total record = 0
							{
								$error = true;
								$confirmPage = false;
								$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Wrong File Formats or Delimited String') . '.';
								$this->view->error      = true;
							}
						}
					} else {

						$error = true;
						$this->view->error = true;
						$confirmPage = false;
						foreach ($adapter->getMessages() as $key => $val) {
							// var_dump($val);
							if ($key == 'fileUploadErrorNoFile')
								$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File cannot be left blank. Please correct it') . '.';
							else
								$error_msg[] = $val;
							break;
						}
						// die('here');
					}
				}

				$acBenef = $this->_getParam('ACBENEF_PLAIN');
				// die('here1');
				//validasi source account tidak boleh sama dengan beneficiary account
				//if single source
				if ($sourceType == 1) {
					if ($acSource == $acBenef) {
						$this->view->error = true;
						$confirmPage = true;
						$error_msg[] = 'Source Account cannot be same with Beneficiary Account';
					}
				}
				//if upload
				else {

					//validasi source account
					$source_error = array();
					// if (!$error) {
					// 	foreach ($acSource as $key => $value) {
					// 		$error_array = $this->validateOpenSourceAccount($value, $acBenef);
					// 		if (!empty($error_array)) {
					// 			array_push($source_error, $error_array);		
					// 		}
					// 	}
					// }
				}

				if (!empty($source_error)) {
					$this->view->error = true;
					$confirmPage = true;
					$error_msg[] = 'Source Account Error';
					$this->view->source_error = $source_error;
				}
				
				$ACBENEF_EMAIL = $this->_getParam('ACBENEF_EMAIL');
				$ACBENEF_ALIAS = $this->_getParam('ACBENEF_ALIAS');
				
				$paymentReff = $this->_getParam('teamplate_reff');
				$paymentSubject = $this->_getParam('paymentsubject');
				$sourceName = $this->_getParam('sourceName');
				$benefName = $this->_getParam('benefName');
				$message = $this->_getParam('TRA_MESSAGE');
				$sweepOption = $this->_getParam('ta_sel');
				$trfDateType = $this->_getParam('tranferdatetype');
				if ($trfDateType == '2') {
					$trfTime = $this->_getParam('PS_EFTIMEF');
				} else {
					$trfTime = $this->_getParam('PS_EFTIMEP');
				}
				
				$jam = preg_match("/^([01]?[0-9]|2[0-3])\:+[0-5][0-9]$/", $trfTime);
				if(!$jam){
						// $this->view->error = true;
						// $confirmPage = true;
						// $error_msg[] = 'Invalid Time Format';
				}

			//	var_dump($sweepOption);die; 
				
				//remains balance 
				if ($sweepOption == 1) {
					// die('vas');
					$remainBalanceType = $this->_getParam('rem_per');
					$remainsBalance = $this->_getParam('remainbalance');
					//Zend_Debug::dump($this->_getAllParams());
					//var_dump($remainsBalance);
					//die('fea');
					//$remainsBalance = $this->_getParam('remainbalance');
					if(empty($remainsBalance) && $sourceType == '1'){
						//var_dump($sourceType);
					//	die('here');
						$error = true;
								$confirmPage = false;
								$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Remain Balance cannot be left blank') . '.';
								$this->view->error      = true;
					}
				}
				//maintain balance
				else {
					//Zend_Debug::dump($this->_getAllParams());
					 
					
					$maintainbalance =  $this->_getParam('maintainbalance');
					if ($trfDateType == 2) {
						if(empty($maintainbalance)){
							$error = true;
									$confirmPage = false;
									$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Maintain Balance cannot be left blank') . '.';
									$this->view->error      = true;
						}
					}else{
						
					$repetition = $this->_getParam('repetition');	
						
					//var_dump($error_msg);
					//die('here');
					if($repetition == '1'){
					$report_day = $this->_getParam('report_day');
					
					$arrday = array(
						'0' => 'sun',
						'1' => 'mon',
						'2' => 'tue',
						'3' => 'wed',
						'4' => 'thu',
						'5' => 'fry',
						'6' => 'sat'

					);
					//var_dump($report_day);
					foreach ($report_day as $key => $value) {
						 
						$tag = 'pooling_' . $arrday[$value];
						$valueday = $this->_getParam($tag);
						//var_dump($tag);
						//var_dump($valueday);
						if($valueday == ''){
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Maintain balance amount cannot be blank';
							break;
						}
					}
					 
					
					$pooling_sun = $this->_getParam('pooling_sun');
					$pooling_mon = $this->_getParam('pooling_mon');
					$pooling_tue = $this->_getParam('pooling_tue');
					$pooling_wed = $this->_getParam('pooling_wed');
					$pooling_thu = $this->_getParam('pooling_thu');
					$pooling_fry = $this->_getParam('pooling_fry');
					$pooling_sat = $this->_getParam('pooling_sat');

					$fore_sun = $this->_getParam('forecast_sun');
					$fore_mon = $this->_getParam('forecast_mon');
					$fore_tue = $this->_getParam('forecast_tue');
					$fore_wed = $this->_getParam('forecast_wed');
					$fore_thu = $this->_getParam('forecast_thu');
					$fore_fry = $this->_getParam('forecast_fry');
					$fore_sat = $this->_getParam('forecast_sat');
					
					}
					
					}
					
				}  

				//if immediate just pass it, bcs immediate will get current date and time when insert
				//futuredate
				if ($trfDateType == 2) {
					$efDate = $this->_getParam('PS_FUTUREDATE');
					$session = $this->_getParam('PS_EFTIMEF');

					if (empty($efDate)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Future Date Must Be Selected';
					}
					if (empty($session)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Session Must Be Selected';
					}
				} else if ($trfDateType == 3) {
					$repetition = $this->_getParam('repetition');
					$efDate = $this->_getParam('PS_EFDATE');
					//daily
					if ($repetition == 1) {
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
						
						$repeatOn = $this->_getParam('report_day');
											//$endDate = $sessionNameConfrim->endDate;

											//get from start date
											$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
											$nextDate = $nextDate->format('Y-m-d');




											
														$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
//var_dump($datenumb);die;												
																		foreach ($dataday as $key => $value) {
																			if($value== '0' || $value == '6'){
																					unset($dataday[$key]);
																			}
																		}
																			//var_dump($efDate);
																			//var_dump($dataday);
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value];
																				//var_dump($string);die;
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																				
																				$nextDatenow = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDatenow = $nextDatenow->format('Y-m-d');
																				$datenumb = date("w", strtotime($nextDatenow));
																				//var_dump($nextDatenow);
																				//var_dump($datenumb);
																				//var_dump($value);
																				//echo '-';
																				if ($datenumb == $value) {
																					$nextDateArr[] = $nextDatenow;
																				//	var_dump($nextDate);die('1');
																					break;

																				}
																				$nextDateArr[] = $nextDate;
																				//var_dump($nextDate);die('2');
																			//	break;
																		//	}
																		//	}
																			
																			
																			
																		}
																		
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																			//var_dump($nextDateArr);
																			//if(!empty($nextDateArr['1'])){
																			//	$nextDate = $nextDateArr['1'];
																			//}else{
																				$nextDate = $nextDateArr['0'];
																		//	}
																			
																			//$param['PS_EFDATE'] = $nextDateArr['0'];
																	}
						
						//var_dump($nextDate);
							$efDate = Application_Helper_General::convertDate($nextDate,$this->_dateDisplayFormat , $this->_dateDBFormat);
							//$efDate = gmdate("d/m/Y", strtotime($nextDate));
						//var_dump($efDate);die;
					}
					//weekly
					else if ($repetition == 2) {
						$repeatEvery = $this->_getParam('selectrepeat');
						$repeatOn = $this->_getParam('report_day');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

						if (empty($repeatEvery)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat Every Must Be Selected';
						}
						if (empty($repeatOn)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat On Be Selected';
						}
					}
					//monthly
					else if ($repetition == 3) {
						$repeatEvery = $this->_getParam('selectrepeat');
						// $repeatOn = $this->_getParam('PS_REPEATON');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

						if (empty($repeatEvery)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat Every Must Be Selected';
						}
						// if (empty($repeatOn)) {
						// 	$this->view->error = true;
						// 	$confirmPage = false;
						// 	$error_msg[] = 'Repeat On Be Selected';
						// }
					}
					$trfDateType = $this->_getParam('tranferdatetype');
					if ($trfDateType == '3') {
						$session = $this->_getParam('PS_EFTIMEP');
					} else {
						$session = $this->_getParam('PS_EFTIMEF');
					}
					// var_dump($session);

					if (empty($endDate)) { 
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'End Date Must Be Selected';
					}
					if (empty($session)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Session Time Must Be Selected';
					}
				}

				$notif = $this->_getParam('notif');

				if ($notif == 2) {
					$email_notif = $this->_getParam('email_notif');
					$sms_notif = $this->_getParam('sms_notif');
				}

				if (empty($error_msg)) {
					// var_dump($fore_sun);
					// var_dump($pooling_sun);
					// echo "<pre>";
					// var_dump($this->_request->getParams());die;
					$poolling = array(
						'0' => (isset($pooling_sun))			? $pooling_sun			: $fore_sun,
						'1' => (isset($pooling_mon))			? $pooling_mon			: $fore_mon,
						'2' => (isset($pooling_tue))			? $pooling_tue			: $fore_tue,
						'3' => (isset($pooling_wed))			? $pooling_wed			: $fore_wed,
						'4' => (isset($pooling_thu))			? $pooling_thu			: $fore_thu,
						'5' => (isset($pooling_fry))			? $pooling_fry			: $fore_fry,
						'6' => (isset($pooling_sat))			? $pooling_sat			: $fore_sat
					);
					// var_dump($poolling);die;
					$accteopen = $this->_getParam('accteopen');

					$databank = $this->_db->fetchAll(
						$this->_db->select()
							->from(array('A' => 'M_BANK_TABLE'), array('*'))
							->where("A.BANK_CODE = ? ", $acctBank)
					);
					// ->where("A.CUST_ID = ? ", $this->_custIdLogin));

					// acctBank$se 



					$report_day = $this->_getParam('report_day');
					$toConfirm = false;
					// $repetition = $this->_getParam('repetition');
					if ($accteopen != '1') {
						// die('here');
						$paramdata = array(
							'SOURCE_TYPE' => $sourceType,
							'ACCT_NO'	=> $acSource,
							'ACBENEF_EMAIL' => $ACBENEF_EMAIL,
							'ACBENEF_ALIAS' => $ACBENEF_ALIAS,
							'ACCT_NAME'	=> $sourceName,
							'BENEF_NO' 	=> $acBenef,
							'BENEF_NAME' => $benefName,
							'DESC' 		=> $message,
							'DATEE_TYPE' 	=> $trfDateType,
							'SUBJECT' 		=> $paymentSubject,
							'BALANCE_TYPE' 		=> $remainBalanceType,
							'SWEEP_TYPE'	=> $sweepOption,
							'REMAIN_BALANCE' 		=> $remainsBalance,
							'EFDATE' 		=> $efDate,
							'ENDDATE' 		=> $endDate,
							'REPEAT_DAY'	=> $repeatOn,
							'REPEAT_EVERY'	=> $repeatEvery,
							'REPEAT_EVERY'	=> $repeatEvery,
							'NOTIF' 		=> $notif,
							'EMAIL_NOTIF' 		=> $email_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'POOLING'		=> $poolling,
							'REPETITION'	=> $repetition,
							'EFTIME'		=> $trfTime,
							'SWEEPIN'		=> $accteopen,
							'BANK_CODE'		=> $acctBank,
							'SWIFT_CODE'	=> $databank['0']['SWIFT_CODE'],
							'CLR_CODE'		=> $databank['0']['CLR_CODE'],
							'REPORT_DAY'	=> $report_day,
							'REPETITION'	=> $repetition,
							'MAINTAIN_BALANCE' => $maintainbalance,
							'TRA_MIN_AMOUNT' => $TRA_MIN_AMOUNT
						);
					} else {
						$acSources = $this->_getParam('ACCTSRC');
						$paramdata = array(
							'SOURCE_TYPE' => $sourceType,
							'ACCT_NO'	=> $acSources,
							'ACBENEF_EMAIL' => $ACBENEF_EMAIL,
							'ACCT_NAME'	=> $sourceName,
							'BENEF_NO' 	=> $acSource,
							'BENEF_NAME' => $benefName,
							'ACBENEF_ALIAS' => $ACBENEF_ALIAS,
							'DESC' 		=> $message,
							'DATEE_TYPE' 	=> $trfDateType,
							'SUBJECT' 		=> $paymentSubject,
							'BALANCE_TYPE' 		=> $remainBalanceType,
							'SWEEP_TYPE'	=> $sweepOption,
							'REMAIN_BALANCE' 		=> $remainsBalance,
							'EFDATE' 		=> $efDate,
							'ENDDATE' 		=> $endDate,
							'REPEAT_DAY'	=> $repeatOn,
							'REPEAT_EVERY'	=> $repeatEvery,
							'REPEAT_EVERY'	=> $repeatEvery,
							'NOTIF' 		=> $notif,
							'EMAIL_NOTIF' 		=> $email_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'POOLING'		=> $poolling,
							'REPETITION'	=> $repetition,
							'EFTIME'		=> $trfTime,
							'SWEEPIN'		=> $accteopen,
							'BANK_CODE'		=> $acctBank,
							'SWIFT_CODE'	=> $databank['0']['SWIFT_CODE'],
							'CLR_CODE'		=> $databank['0']['CLR_CODE'],
							'REPORT_DAY'	=> $report_day,
							'TRA_MIN_AMOUNT' => $TRA_MIN_AMOUNT
						);
						// echo "<pre>";
						// var_dump($paramdata);die;
					}

					$paramdata['databank'] = $databank;
					$paramdata['dataacct'] = $acct;
					$paramdata['selectUsergroup'] = $selectUsergroup;
					$paramdata['dataArr'] = $dataArr;
					// echo "<pre>";
					// var_dump($error_msg);
					 //var_dump($this->_request->getParams());
					// echo "<pre>";
					// var_dump($paramdata);die;

					$accteopen1 = $this->_getParam('accteopen');

					$trfDateType = $this->_getParam('tranferdatetype');
					if ($trfDateType == 2) {

						if ($accteopen1 != '1') {
							$acct_no = $acSource;
							$benef_no = $acBenef;
							$sweepOption = $this->_getParam('ta_sel');
							if ($sweepOption == 1) {
							
								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($remainsBalance);
							}else{
									$maintainbalance =  $this->_getParam('maintainbalance');	
									$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($maintainbalance);
							}
							
							
							$TRA_MIN_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT);
						} else {
							$acct_no = $this->_getParam('ACCTSRC');
							$benef_no = $acSource;
							$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($remainsBalance);
							$TRA_MIN_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT);
						}
						//var_dump($TRA_AMOUNT_num);
					} else if ($trfDateType == 3) {

						$sweepOption = $this->_getParam('ta_sel');
						if ($sweepOption == 1) {
							$acct_no = $this->_getParam('ACCTSRC');
							$benef_no = $acSource;
							$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($remainsBalance);
							$TRA_MIN_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT);
						}else{

							$repetition = $this->_getParam('repetition');
							//var_dump($repetition);die();
							//daily
							if ($repetition == 1) {
								$datetest = Application_Helper_General::convertDate($efDate,'yyyy-MM-dd','yyyy-MM-dd');
								$day = strtolower(date('D', strtotime($datetest)));
								$tagday = 'pooling_'.$day;
								$amountbalance = $this->_getParam($tagday);
								$acct_no = $acSource;
								$benef_no = $acBenef;
								//var_dump($tagday);
								//var_dump($amountbalance);
								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($amountbalance);
								$TRA_MIN_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT);
								//die;
							}
							//weekly or monthly
							else if ($repetition == 2 || $repetition == 3) {
								$acct_no = $acSource;
								$benef_no = $acBenef;
								$maintainbalance =  $this->_getParam('maintainbalance');
								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($maintainbalance);
								$TRA_MIN_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT);
							}
						}

					}

					if ($sweepOption == 2) {
						$maintainbalance =  $this->_getParam('maintainbalance');
						//if ($trfDateType == 2) {
							if(empty($maintainbalance)){
								$datetest = Application_Helper_General::convertDate($efDate,'yyyy-MM-dd','yyyy-MM-dd');
								$day = strtolower(date('D', strtotime($date)));
								$tagday = 'pooling_'.$day;
								$amountbalance = $this->_getParam($tagday);
								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($amountbalance);
							}else{
								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($maintainbalance);
							//}
						}
					} 

					$paramPayment = array(	
											"PS_SUBJECT"				=> $paymentSubject,
											"PS_EFDATE"					=> $efDate,
											"CATEGORY"					=> 'OPEN SWEEP',
											"_dateFormat"				=> $this->_dateDisplayFormat,
											"_dateDBFormat"				=> $this->_dateDBFormat,
											
										 );
										
					$paramTrxArr[0] = array(
											"TRANSFER_TYPE" 			=> 'ONLINE',
											"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
											"TRA_MIN_AMOUNT" 			=> $TRA_MIN_AMOUNT_num,
											"TRA_MESSAGE" 				=> $message,
											// "TRA_REFNO" 				=> $TRA_REFNO,
											"ACCTSRC" 					=> $acct_no,
											"ACBENEF" 					=> $benef_no,
											// "ACBENEF_CCY" 				=> $ACBENEF_CCY,
											// "ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
											// "ACCTSRC_BANKCODE"			=> $ACCTSRC_BANKCODE,
										// for Beneficiary data, except (bene CCY and email), must be passed by reference
											"ACBENEF_BANKNAME" 			=> &$benefName,
											// "ReffId" 					=> &$ReffId,
											// "ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
												// "ACBENEF_CITIZENSHIP" 		=> &$ACBENEF_CITIZENSHIP,		// W:WNI, N: WNA
											// "ACBENEF_RESIDENT" 			=> &$ACBENEF_RESIDENT,			// 1/R: RESIDENT, 2/NR: NON-RESIDENT
											// "ACBENEF_ADDRESS1" 			=> &$ACBENEF_ADDRESS,
											// "ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
										//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
										//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

										//	"ORG_DIR" 					=> $ORG_DIR,
											"BANK_CODE" 				=> $acctBank,
										//	"BANK_NAME" 				=> $BANK_NAME,
										//	"BANK_BRANCH" 				=> $BANK_BRANCH,
										//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
										//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
										//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,

											// "LLD_IDENTICAL" 			=> $LLD_IDENTICAL,
										//	"LLD_CITIZENSHIP"			=> $LLD_CITIZENSHIP,
											// "LLD_CATEGORY" 				=> $LLD_CATEGORY,
											// "LLD_RELATIONSHIP" 			=> $LLD_RELATIONSHIP,
											// "LLD_PURPOSE" 				=> $LLD_PURPOSE,
											// "LLD_DESCRIPTION" 			=> $LLD_DESCRIPTION,
											// "LLD_BENEIDENTIF" 			=> $LLD_BENEIDENTIF,
											// "LLD_BENENUMBER" 			=> $LLD_BENENUMBER,
											// "LLD_SENDERIDENTIF" 		=> $LLD_SENDERIDENTIF,
											// "LLD_SENDERNUMBER" 			=> $LLD_SENDERNUMBER,
											// "CITY"						=> $CITY,
											// "CITY_CODE" 				=> $CITY_CODE, //yang akan dikirimkan ke vlink field 3
											"SWEEP_TYPE"				=> $sweepOption,
											'SWIFT_CODE'				=> $databank['0']['SWIFT_CODE'],
											'CLR_CODE'					=> $databank['0']['CLR_CODE'],
											"PS_EMAIL"					=> $email_notif,
											"PS_NOTIF"					=> $notif,
											"PS_SMS"					=> $sms_notif,
										 );

					
					$validate   = new ValidatePaymentOpen($this->_custIdLogin, $this->_userIdLogin);

					$resWs = array();
					// $paramTrxArr = array();
					 
					//var_dump($datetest);
					 //echo '<pre>';
					 //var_dump($paramPayment);
					 //var_dump($paramTrxArr);die;
					$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);

					if ($resultVal == false) {
 
						$errorMsg1 		= $validate->getErrorMsg();	
						//var_dump($errorMsg1);die;
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = $errorMsg1;

					}else{
						$sessionNamespace = new Zend_Session_Namespace('confirmSweep');
						$sessionNamespace->content = $paramdata;

						$toConfirm = true;
					}

					
				}else{
					$confirmPage = false;
				}
	

					$paymentReff = $this->_getParam('teamplate_reff');
					$paymentSubject = $this->_getParam('paymentsubject');
					$sourceName = $this->_getParam('sourceName');
					$benefName = $this->_getParam('benefName');
					$message = $this->_getParam('TRA_MESSAGE');
					$sweepOption = $this->_getParam('ta_sel');
					$trfDateType = $this->_getParam('tranferdatetype');
					if ($trfDateType == '2') {
						$trfTime = $this->_getParam('PS_EFTIMEF');
						$this->view->PS_EFTIMEF = $trfTime;
					} else {
						$trfTime = $this->_getParam('PS_EFTIMEP');
						$this->view->PS_EFTIMEP = $trfTime;
					}

					$remainBalanceType = $this->_getParam('rem_per');

					$ACCT_BANK = $this->_getParam('ACCT_BANK');
					$ACCTSRC = $this->_getParam('ACCTSRC');
					$ACCTSRC_PLAIN = $this->_getParam('ACCTSRC_PLAIN');
					$ACBENEF = $this->_getParam('ACBENEF'); 
					$FILE_ID = $this->_getParam('FILE_ID');
					$TRA_MESSAGE = $this->_getParam('TRA_MESSAGE');
					$PS_EFDATEFUTURE = $this->_getParam('PS_FUTUREDATE');


					$PS_EFDATE = $this->_getParam('PS_EFDATE');

					$repetition = $this->_getParam('repetition');
					if (!empty($repetition)) {
						$repeatstr = 'repetition' . $repetition;
						// var_dump($repeatstr);
						$this->view->repetition = $repetition;
						$this->view->$repeatstr = 'selected';
					}

					$PS_ENDDATEPERIODIC = $this->_getParam('PS_ENDDATEPERIODIC');

					$selectrepeat = $this->_getParam('selectrepeat');
					if (!empty($selectrepeat)) {
						$repeat = 'repeat' . $selectrepeat;
						$this->view->$repeat = 'selected';
					}



					$this->view->PS_EFDATE = $PS_EFDATE;
					$this->view->endDatePeriodic = $PS_ENDDATEPERIODIC;

					$report_day = $this->_getParam('report_day');

					if (!empty($report_day)) {
						foreach ($report_day as $key => $value) {
							$this->view->{'check' . $value} = 'checked';
						}
					}


					// var_dump($PS_EFDATEFUTURE);die;
					// var_dump($this->_getParam('paymentsubject'));
					// var_dump($paymentsubject);die;
					$this->view->FILE_ID = $FILE_ID;
					$this->view->ACCTSRC = $ACCTSRC;
					$this->view->ACBENEF = $ACBENEF;
					$this->view->ACBENEF_PLAIN = $this->_getParam('ACBENEF_PLAIN');
					
					$this->view->TRA_MESSAGE = $TRA_MESSAGE;
					$this->view->PS_EFDATEFUTURE = $PS_EFDATEFUTURE;

					$aliassource = '';
					$aliasbenef = '';
					foreach ($acct as $key => $value) {
						if ($value['ACCT_NO'] == $sourceName) {
							$aliassource = $value['ACCT_ALIAS'];
						}

						if ($value['ACCT_NO'] == $benefName) {
							$aliasbenef = $value['ACCT_ALIAS'];
						}
					}
					$bank_name = '';
					foreach ($databank as $key => $value) {
						if ($value['BANK_CODE'] == $ACCT_BANK) {
							$bank_name = $value['BANK_NAME'];
						}
					}
					// var_dump($trfDateType);
					// $this->view->remainbalance = 
					$this->view->paymentReff = $paymentReff;
					$this->view->paymentsubject = $this->_getParam('paymentsubject');
					$this->view->acSource = $acSource;
					$this->view->sourceName = $sourceName;
					$this->view->acBenef = $acBenef;
					$this->view->benefName = $benefName;
					$this->view->message = $message;
					$this->view->sweepOption = $sweepOption;
					$this->view->TrfDateType = $trfDateType;
					$this->view->sourceType = $sourceType;
					$this->view->ACCT_BANK = $ACCT_BANK;

					$remainsBalance = $this->_getParam('remainbalance');
					$maintainbalance = $this->_getParam('maintainbalance');
					//remains balance
					$this->view->remainBalanceType = $remainBalanceType;
					// var_dump($remainsBalance);die;
					$this->view->remainsBalance = $remainsBalance;
					$this->view->maintainbalance = $maintainbalance;

					//maintain balance
					$this->view->pooling_sun = $pooling_sun;
					$this->view->pooling_mon = $pooling_mon;
					$this->view->pooling_tue = $pooling_tue;
					$this->view->pooling_wed = $pooling_wed;
					$this->view->pooling_thu = $pooling_thu;
					$this->view->pooling_fry = $pooling_fry;
					$this->view->pooling_sat = $pooling_sat;

					$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
					$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
					$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
					$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
					$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
					$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
					$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);

					//trf date type
					//future date
					$this->view->efDate = $efDate;
					//periodic
					$this->view->repetition = $repetition;
					$this->view->endDate = $endDate;
					$this->view->repeatEvery = $repeatEvery;
					$this->view->repeatOn = $repeatOn;
					$this->view->endDate = $endDate;
					
					//Zend_Debug::dump($this->_getAllParams());
					
					//notif
					$this->view->notif = $notif;
					$this->view->email_notif = $email_notif;
					$this->view->sms_notif = $sms_notif;

					$this->view->session = $session;

					$this->view->TrfDateType 		= (isset($trfDateType))			? $trfDateType			: '2';

					$paramBack = array(
						'paymentReff' => $paymentReff,
						'ACCTSRCTEXT' => $this->_getParam('ACCTSRCTEXT'),
						'ACBENEF_EMAIL' => $ACBENEF_EMAIL,
						'ACBENEF_PLAIN'	=> $acBenef,
						'paymentSubject' => $paymentSubject,
						'sourceName'	=> $sourceName,
						'benefName'	=> $benefName,
						'message'	=> $message,
						'sweepOption'	=> $sweepOption,
						'sourceType'	=> $sourceType,
						'trfDateType'	=> $trfDateType,
						'trfTime'	=> $trfTime,
						'remainBalanceType'	=> $remainBalanceType,
						'ACCT_BANK'	=> $ACCT_BANK,
						'ACCTSRC'	=> $ACCTSRC,
						'ACBENEF'	=> $ACBENEF,
						'FILE_ID'	=> $FILE_ID,
						'TRA_MESSAGE'	=> $TRA_MESSAGE,
						'PS_EFDATE'	=> $PS_EFDATE,
						'PS_EFDATEFUTURE'	=> $PS_EFDATEFUTURE,
						'repetition'	=> $repetition,
						'PS_ENDDATEPERIODIC'	=> $PS_ENDDATEPERIODIC,
						'selectrepeat'	=> $selectrepeat,
						'report_day'	=> $report_day,
						'FILE_ID'	=> $FILE_ID,
						'remainsBalance'	=> $remainsBalance,
						'maintainbalance'	=> $maintainbalance,
						'pooling_sun'	=> $pooling_sun,
						'pooling_mon'	=> $pooling_mon,
						'pooling_tue'	=> $pooling_tue,
						'pooling_wed'	=> $pooling_wed,
						'pooling_thu'	=> $pooling_thu,
						'pooling_fry'	=> $pooling_fry,
						'pooling_sat'	=> $pooling_sat,
						'efDate'	=> $efDate,
						'endDate'	=> $endDate,
						'repeatEvery'	=> $repeatEvery,
						'repeatOn'	=> $repeatOn,
						'notif'			=> $notif,
						'email_notif'	=> $email_notif,
						'sms_notif'		=> $sms_notif,
						'TRA_MIN_AMOUNT' => $TRA_MIN_AMOUNT,
						'session'		=> $session
					);

					$sessionNamespace->paramBack = $paramBack;
				}



					$this->view->ACBENEF_BANKNAME = $this->_getParam('ACBENEF_BANKNAME');
					$this->view->ACCTSRC = $this->_getParam('ACCTSRC');
					$this->view->ACCTSRCTEXT = $this->_getParam('ACCTSRCTEXT');
					$this->view->ACCTSRC_BANKCODE = $this->_getParam('sourceBankCode');
					$this->view->ACCT_BANK = $this->_getParam('ACCT_BANK');
					$this->view->ACBENEF_ALIAS = $this->_getParam('ACBENEF_ALIAS');
					$this->view->ACBENEF_EMAIL = $this->_getParam('ACBENEF_EMAIL');

				$this->view->error_msg = $error_msg;

				//page indicator
				$this->view->confirmPage = $confirmPage;

				if ($toConfirm) {
					$this->_redirect('/opensweep/samebank/confirm');
				}
			//if submit di confirmpage
		}

		$sessionNamespace = new Zend_Session_Namespace('confirmSweep');
		$paramBack = $sessionNamespace->paramBack;
		$back = $sessionNamespace->back;

		if ($back) {
			
			if ($paramBack['trfDateType'] == '2') {
				$this->view->PS_EFTIMEF = $paramBack['trfTime'];
			} else {
				$this->view->PS_EFTIMEP = $paramBack['trfTime'];
			}

			if (!empty($paramBack['repetition'])) {
				$repeatstr = 'repetition' . $paramBack['repetition'];
				// var_dump($repeatstr);
				$this->view->repetition = $paramBack['repetition'];
				$this->view->$repeatstr = 'selected';
			}

			if (!empty($paramBack['selectrepeat'])) {
				$repeat = 'repeat' . $paramBack['selectrepeat'];
				$this->view->$repeat = 'selected';
			}

			$this->view->PS_EFDATE = $paramBack['PS_EFDATE'];
			$this->view->endDatePeriodic =  $paramBack['PS_ENDDATEPERIODIC'];

			if (!empty($paramBack['report_day'])) {
				foreach ($paramBack['report_day'] as $key => $value) {
					$this->view->{'check' . $value} = 'checked';
				}
			}

			$this->view->FILE_ID = $paramBack['FILE_ID'];
			$this->view->ACCTSRC = $paramBack['ACCTSRC'];
			$this->view->ACBENEF = $paramBack['ACBENEF'];
			$this->view->TRA_MESSAGE = $paramBack['TRA_MESSAGE'];
			$this->view->PS_EFDATEFUTURE = $paramBack['PS_EFDATEFUTURE'];

			$aliassource = '';
			$aliasbenef = '';
			foreach ($acct as $key => $value) {
				if ($value['ACCT_NO'] == $sourceName) {
					$aliassource = $value['ACCT_ALIAS'];
				}

				if ($value['ACCT_NO'] == $benefName) {
					$aliasbenef = $value['ACCT_ALIAS'];
				}
			}
			$bank_name = '';
			foreach ($databank as $key => $value) {
				if ($value['BANK_CODE'] == $ACCT_BANK) {
					$bank_name = $value['BANK_NAME'];
				}
			}
			$this->view->ACBENEF_BANKNAME = $this->_getParam('ACBENEF_BANKNAME');
			$this->view->ACCTSRC = $paramBack['ACCTSRC'];
			$this->view->ACCTSRCTEXT = $paramBack['ACCTSRCTEXT'];
			$this->view->ACCTSRC_BANKCODE = $paramBack['sourceBankCode'];
			// var_dump($trfDateType);
			// $this->view->remainbalance = 
			$this->view->paymentReff = $paramBack['paymentReff'];
			$this->view->paymentsubject = $paramBack['paymentSubject'];
			$this->view->paymentsubject = $paramBack['paymentSubject'];
			$this->view->acSource = $paramBack['ACCTSRC'];
			$this->view->sourceName = $paramBack['sourceName'];
			$this->view->acBenef = $paramBack['ACBENEF'];
			$this->view->benefName = $paramBack['benefName'];
			$this->view->message = $paramBack['message'];
			$this->view->sweepOption = $paramBack['sweepOption'];
			$this->view->TrfDateType = $paramBack['trfDateType'];
			$this->view->sourceType = $paramBack['sourceType'];
			$this->view->ACCT_BANK = $paramBack['ACCT_BANK'];
			$this->view->TRA_MIN_AMOUNT = $paramBack['TRA_MIN_AMOUNT'];
			$this->view->ACBENEF_EMAIL = $paramBack['ACBENEF_EMAIL'];
			$this->view->ACBENEF_PLAIN = $paramBack['ACBENEF_PLAIN'];
			
			


			//remains balance
			$this->view->remainBalanceType = $paramBack['remainBalanceType'];
			// var_dump($remainsBalance);die;
			$this->view->remainsBalance = $paramBack['remainsBalance'];
			$this->view->maintainbalance = $paramBack['maintainbalance'];

			//maintain balance
			$this->view->pooling_sun = $paramBack['pooling_sun'];
			$this->view->pooling_mon = $paramBack['pooling_mon'];
			$this->view->pooling_tue = $paramBack['pooling_tue'];
			$this->view->pooling_wed = $paramBack['pooling_wed'];
			$this->view->pooling_thu = $paramBack['pooling_thu'];
			$this->view->pooling_fry = $paramBack['pooling_fry'];
			$this->view->pooling_sat = $paramBack['pooling_sat'];

			$this->view->pooling_sun_view = $this->moneyAliasFormatter($paramBack['pooling_sun']);
			$this->view->pooling_mon_view = $this->moneyAliasFormatter($paramBack['pooling_mon']);
			$this->view->pooling_tue_view = $this->moneyAliasFormatter($paramBack['pooling_tue']);
			$this->view->pooling_wed_view = $this->moneyAliasFormatter($paramBack['pooling_wed']);
			$this->view->pooling_thu_view = $this->moneyAliasFormatter($paramBack['pooling_thu']);
			$this->view->pooling_fry_view = $this->moneyAliasFormatter($paramBack['pooling_fry']);
			$this->view->pooling_sat_view = $this->moneyAliasFormatter($paramBack['pooling_sat']);

			//trf date type
			//future date
			$this->view->efDate = $paramBack['efDate'];
			//periodic
			$this->view->repetition = $paramBack['repetition'];
			$this->view->endDate = $paramBack['endDate'];
			$this->view->repeatEvery = $paramBack['repeatEvery'];
			$this->view->repeatOn = $paramBack['repeatOn'];

			//notif
			$this->view->notif = $paramBack['notif'];
			$this->view->email_notif = $paramBack['email_notif'];
			$this->view->sms_notif = $paramBack['sms_notif'];
			$this->view->session = $paramBack['session'];

			$sessionNamespace->back = false;
		}
		else{
			unset($_SESSION['confirmSweep']);
		}
				 
				 
				 
		//Recrate Ongoing
		if ($this->_request->getParam('recreate')) {
			$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
			$filter       = new Application_Filtering();
			$AESMYSQL = new Crypt_AESMYSQL();
			$password = $sessionNamespace->token;
			$PS_NUMBER = urldecode($filter->filter($this->_getParam('recreate'), "recrate"));
			$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

			$recreate  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'))
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER')
				->where('P.PS_NUMBER = ?', $PS_NUMBER);
			$recreate = $this->_db->fetchRow($recreate);

			$this->view->paymentsubject = $recreate['PS_SUBJECT'];
			$this->view->ACCT_BANK = $recreate['BENEF_ACCT_BANK_CODE'];
			$this->view->ACBENEF = $recreate['BENEFICIARY_ACCOUNT'];
			$this->view->ACCTSRC = $recreate['SOURCE_ACCOUNT'];
			$this->view->TRA_MESSAGE = $recreate['TRA_MESSAGE'];
			
		}
	}


	public function acctsrcsuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

     //    $complist = $this->_db->fetchAll(
     //                $this->_db->select()
     //                     ->from(array('A' => 'M_USER'),array('CUST_ID'))
					// 	->where("A.CUST_ID = ? ", $this->_custIdLogin)
     //                     ->where("A.USER_ID = ? ", $this->_userIdLogin)
     //           );	
    	// $comp = "'";
    	// foreach ($complist as $key => $value) {
    	// 	$comp .= "','".$value['CUST_ID']."','";
    	// }
    	// $comp .= "'";

    	$Settings = new Settings();
    	$dombank = $Settings->getSetting('bank_support_domestic');

        $acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 //->where("A.CUST_ID IN (".$comp.")")
						  ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 //->where("A.BANK_CODE IN (".$dombank.")")
						 ->order('A.APIKEY_ID ASC')
				);
		
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}
		
		$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$i]['ACCT_CCY'] = $value['account_currency'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];

			$acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
			$acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
			$acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
			$acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			$i++;
		}

		$newacct = array();
		foreach ($acct as $key => $value) {
			$newacct[$key]['value'] = $value['ACCT_NO'];
			$newacct[$key]['bankcode'] = $value['BANK_CODE'];
			$newacct[$key]['bankname'] = $value['BANK_NAME'];
			$newacct[$key]['ACCT_ALIAS'] = $value['ACCT_ALIAS'];
			if(empty($value['ACCT_ALIAS'])){
				$newacct[$key]['label'] = $value['ACCT_NO']." (IDR) / ".$value['ACCT_BANK'];
			}else{
				$newacct[$key]['label'] = $value['ACCT_NO']." (IDR) / ".$value['ACCT_BANK']." / ".$value['ACCT_ALIAS'];
			}
			
		}
        

        $searchTerm = $_GET['term'];

        $accData = array();
        foreach ($newacct as $key => $value) {

        	if (strpos(strtolower($value['label']), strtolower($searchTerm)) !== false) {
		        $data['value'] = $value['label'];
        		$data['bankcode']    = $value['bankcode'];
				$data['bankname']    = $value['bankname'];
        		$data['labeltext']    = $value['value'];
        		$data['aliasname']    = $value['ACCT_ALIAS'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <span>'.$value['label'].'</span>
		        </a>';
		        array_push($accData, $data);
        	}
        } 
        echo json_encode($accData);

	}
	
	
	public function acctbenefsuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

     //    $complist = $this->_db->fetchAll(
     //                $this->_db->select()
     //                     ->from(array('A' => 'M_USER'),array('CUST_ID'))
					// 	 ->where("A.CUST_ID = ? ", $this->_custIdLogin)
     //                     ->where("A.USER_ID = ? ", $this->_userIdLogin)
     //           );	
    	// $comp = "'";
    	// foreach ($complist as $key => $value) {
    	// 	$comp .= "','".$value['CUST_ID']."','";
    	// }
    	// $comp .= "'";

    	$bankcode = $_GET['bankcode'];
    	$sourceacct = $_GET['sourceacct'];
		$sourceacct = explode(' ',$sourceacct);
		$sourceacct = $sourceacct[0];
		
    		if (!empty($sourceacct)) {
    		 	$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->where("A.VALUE != ?", (string)$sourceacct)
						 ->order('A.APIKEY_ID ASC')
				); 
				
				//$array = array($acctlist);
    	
    		}
    		else{
    			$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID = ?",$this->_custIdLogin)
						 ->where("A.BANK_CODE = ?", (string)$bankcode)
						 ->order('A.APIKEY_ID ASC')
				); 
				
				//$acctlist = array();
    		}
    	
		
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}
		
		$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$i]['ACCT_CCY'] = $value['account_currency'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];

			$acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
			$acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
			$acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
			$acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			$i++;
		}

	//echo json_encode($acct);
		$newacct = array();
		foreach ($acct as $key => $value) {
			$newacct[$key]['value'] = $value['ACCT_NO'];
			$newacct[$key]['bankcode'] = $value['BANK_CODE'];
			$newacct[$key]['bankname'] = $value['BANK_NAME'];
			$newacct[$key]['aliasname'] = $value['ACCT_ALIAS'];
			
			if(empty($value['ACCT_ALIAS'])){
				$newacct[$key]['label'] = $value['ACCT_NO']." (".$value['ACCT_CCY'].")";
			}else{
				$newacct[$key]['label'] = $value['ACCT_NO']." (".$value['ACCT_CCY'].")";
			}
			
		}
        

        $searchTerm = $_GET['term'];

        $accData = array();
        foreach ($newacct as $key => $value) {

        	if (strpos(strtolower($value['label']), strtolower($searchTerm)) !== false) {
		        $data['value'] = $value['label'];
        		$data['bankcode']    = $value['bankcode'];
				$data['bankname']    = $value['bankname'];
				$data['aliasname']    = $value['aliasname'];
				
        		$data['labeltext']    = $value['value'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <span>'.$value['label'].'</span>
		        </a>';
		        array_push($accData, $data);
        	}
        }
        echo json_encode($accData);
		

	}


	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('confirmSweep');
		$data = $sessionNamespace->content;
		
		$custID = $this->_custIdLogin;
		$userID = $this->_userIdLogin;
		
		$CustomerUser = new CustomerUser($custID, $userID);
		//echo "<pre>";
		// var_dump($this->_request->getParams());
		// var_dump($data);
		// die();
		$this->view->TRA_MIN_AMOUNT = $data['TRA_MIN_AMOUNT'];
		// every atau every date
		if($this->_custSameUser){
			$this->view->token = true;

			$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
			// echo $selectQuery;
			$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

			// var_dump($usergoogleAuth);die; 
			if (!empty($usergoogleAuth)) {
				$this->view->googleauth = true;
				//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
				$settingObj = new Settings();
				$maxtoken = $settingObj->getSetting("max_failed_token");
				$this->view->tokenfail = (int)$maxtoken-1;
				if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
					//die;  
					$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
					
					
					
					$tokenfail = (int)$maxtoken - ((int)$usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
					$this->view->tokenfail = $tokenfail;
				}
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}
			else{
				$this->view->nogoauth = '1';
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}

			

			$this->view->googleauth = true;
		}
//echo '<pre>';
			//var_dump($data);die;
			
		$this->view->selectUsergroup = $data['selectUsergroup'];
		$this->view->dataBoundary = $data['dataArr'];

		if ($this->_request->isPost()) {

			if ($this->_getParam('back') == true)
			{
				$sessionNamespace->back 	= true;
				$this->_redirect('/opensweep/samebank');
			}


			$filter = new Application_Filtering();
			$RECURING 				= $filter->filter($this->_request->getParam('tranferdatetype'), "PERIODIC_TYPE"); //trf date type

			/* filter */
			$filterArr = array(
				// 'paymentsubject'    => array('StripTags','StringTrim'),
				'ACCTSRC'    => array('StripTags', 'StringTrim'),
				'ACBENEF'  => array('StripTags', 'StringTrim'),
				'ta_sel'  => array('StripTags', 'StringTrim') // sweep option
			);

			$validators = array(
				// 'paymentsubject' => array('NotEmpty',
				// 				'messages' => array(
				//                                               				$this->language->_('Payment Subject cannot be left blank.')
				// 									)
				// 			),
				'ACCTSRC' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Source Account cannot be left blank.')
					)
				),
				'ACBENEF' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Beneficiary Account cannot be left blank.')
					)
				),
				'ta_sel' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Sweep Option cannot be left blank.')
					)
				),
			);



			$zf_filter = new Zend_Filter_Input($filterArr, $validators, $this->_request->getParams());

			// $param = $this->_getAllParams();
			if ($zf_filter->isValid()) {
				// die('here');
				$paymentReff = $this->_getParam('teamplate_reff');
				$paymentSubject = $this->_getParam('paymentsubject');
				$sourceType = $this->_getParam('sourceType');
				$acSource = $this->_getParam2('ACCTSRC');
				$acBenef = $this->_getParam2('ACBENEF');
				$message = $this->_getParam('TRA_MESSAGE');
				$sweepOption = $this->_getParam('ta_sel');
				$trfDateType = $this->_getParam('tranferdatetype');

				$accteopen = $this->_getParam('accteopen');


				//remains balance
				if ($sweepOption == 1) {
					$remainBalanceType = $this->_getParam('rem_per');
					$remainsBalance = $this->_getParam('remainbalance');

					$remainsBalanceArr = explode('.', $remainsBalance);
					$remainsBalance = str_replace(',', '', $remainsBalanceArr[0]);
				}
				//maintain balance
				else {
					$pooling_sun = $this->_getParam('pooling_sun');
					$pooling_mon = $this->_getParam('pooling_mon');
					$pooling_tue = $this->_getParam('pooling_tue');
					$pooling_wed = $this->_getParam('pooling_wed');
					$pooling_thu = $this->_getParam('pooling_thu');
					$pooling_fry = $this->_getParam('pooling_fry');
					$pooling_sat = $this->_getParam('pooling_sat');
				}


				//futuredate
				if ($trfDateType == 2) {
					$efDate = $this->_getParam('PS_FUTUREDATE');
					$session = $this->_getParam('session_inp');

					$nextDate = $efDate;
					$repetition = 0;
				}

				//periodic
				else if ($trfDateType == 3) {
					$repetition = $this->_getParam('repetition');

					//daily
					if ($repetition == 1) {


						$repeatOn = $this->_getParam('report_day');
											$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
		//var_dump($data);die;
											//get from start date
											$nextDate = DateTime::createFromFormat('d/m/Y', $data['EFDATE']);
											$nextDate = $nextDate->format('Y-m-d');
											$efDate = $data['EFDATE'];



											
														$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
//var_dump($datenumb);die;												
																		foreach ($dataday as $key => $value) {
																			if($value== '0' || $value == '6'){
																					unset($dataday[$key]);
																			}
																		}
																			//var_dump($efDate);
																			//var_dump($dataday);
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value];
																				//var_dump($string);die;
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																				
																				$nextDatenow = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDatenow = $nextDatenow->format('Y-m-d');
																				$datenumb = date("w", strtotime($nextDatenow));
																				//var_dump($nextDatenow);
																				//var_dump($datenumb);
																				//var_dump($value);
																				//echo '-';
																				if ($datenumb == $value) {
																					$nextDateArr[] = $nextDatenow;
																				//	var_dump($nextDate);die('1');
																					break;

																				}
																				$nextDateArr[] = $nextDate;
																				//var_dump($nextDate);die('2');
																			//	break;
																		//	}
																		//	}
																			
																			
																			
																		}
																		
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																			//var_dump($nextDateArr);
																			//if(!empty($nextDateArr['1'])){
																			//	$nextDate = $nextDateArr['1'];
																			//}else{
																				$nextDate = $nextDateArr['0'];
																		//	}
																			
																			//$param['PS_EFDATE'] = $nextDateArr['0'];
																	}

						//var_dump($nextDate);die;
 


						// var_dump($NEXT_DATE);die;
					}
					//weekly
					else if ($repetition == 2) {
						
						
						$repeatOn = $this->_getParam('report_day');
											$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
											
											$efDate = $data['EFDATE'];
											//get from start date
											$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
											$nextDate = $nextDate->format('Y-m-d');

											$dataday = $repeatOn;
											$arrday = array(
												'0' => 'sunday',
												'1' => 'monday',
												'2' => 'tuesday',
												'3' => 'wednesday',
												'4' => 'thursday',
												'5' => 'friday',
												'6' => 'saturday'
											);

											// get number of day in a week of startdate
											$datenumb = date("w", strtotime($nextDate));

											if (!empty($dataday)) {
												foreach ($dataday as $key => $value) {

													if ($datenumb == $value) {
														$nextDate = $nextDate;
														break;
													}
													else {
														$string = 'next ' . $arrday[$dataday[0]];
														// var_dump($string);die();
														$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
														$nextDate->modify($string);
														$nextDate = $nextDate->format('Y-m-d');

														break;
													}
												}
											}
						
						$daysArr  = $this->_getParam('report_day');
						$repeatEvery = $daysArr['0'];
						//$repeatOn = $this->_getParam('report_day');
						//$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

						//$nextDate = date("Y-m-d", strtotime("+1 week"));
					}
					//monthly
					else if ($repetition == 3) {
						
						
						$repeatEvery = $this->_getParam('repeat_every');
											$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
											$efDate = $data['EFDATE'];
											//get from start date
											$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
											$nextDate = $nextDate->format('Y-m-d');

											// get date of startdate
											$datenumb = date("d", strtotime($nextDate));
											//var_dump($nextDate);
											//var_dump($repeatEvery);
											//var_dump($datenumb);
											if($repeatEvery == 'last'){
												
												$nextDate = date("Y-m-t", strtotime($nextDate));
												
												//die;
											}else{
											 
												if ($datenumb == $repeatEvery) {
													$nextDate = $nextDate;
												}
												else if($datenumb < $repeatEvery){
													$nextDateExplode = explode('-', $nextDate);

													$nextDateExplode[2] = $repeatEvery;

													$nextDate = implode('-', $nextDateExplode);
												}
												else if($datenumb > $repeatEvery) {

													$nextDateExplode = explode('-', $nextDate);

													$nextDateExplode[2] = $repeatEvery;

													$nextDate = implode('-', $nextDateExplode);

													$nextDate = date('Y-m-d', strtotime("+1 month", strtotime($nextDate)));
												}
											}
						
						//die;
					//	$repeatEvery = $this->_getParam('repeat_every');
				//		$repeatOn = $this->_getParam('PS_REPEATON');
					//	$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
					//	$strdate = 'Y-m-'.$repeatEvery;
					//	$datenext = date($strdate);
					//	$da = strtotime($datenext);
					//	$dt = strtotime(date('Y-m-d'));
						//var_dump($datenext);
					//	if($da>=$dt){
					//		$nextDate = $datenext;
					//	}else{
					//		$nextDate = date($strdate, strtotime("+1 month"));
					//	}
						
						
					}
					$session = $this->_getParam('session_inp');
				}
				//var_dump($nextDate);
				$notif = $this->_getParam('notif');

				if ($notif == 2) {
					$email_notif = $this->_getParam('email_notif');
					$sms_notif = $this->_getParam('sms_notif');
				}


				// cek value of remain balance percentage
				$RBVal = 0;
				if ($remainBalanceType == 2) { // jika percentage

					$valx = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($remainsBalance));

					if ($valx > 100) {	// jika lebih dari 100%
						$RBVal = $RBVal + 1;
					} else {
						$RBVal = $RBVal + 0;
					}
				} else {
					$RBVal = 0;
				}

				if ($RBVal > 0) { //jika remain balance percentage lebih dari 100%					
					$errorMsg		 		= $this->language->_('Remain Balance must not grather than 100');
					$this->view->error 		= true;
					$this->view->error_msg	= $errorMsg;
				}

				$this->_db->beginTransaction();



				try {
					//echo '<pre>';
					//var_dump($data);die;
					//periodic
					//T_PERIODIC
					$START_DATE = join('-', array_reverse(explode('/', date('d/m/Y'))));

					if ($data['DATEE_TYPE'] == 2) {
						$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $efDate);
						$expDate = $EXPIRY_DATE->format('Y-m-d');

						$NEXT_DATE = DateTime::createFromFormat('d/m/Y', $nextDate);
						$nextDate = $NEXT_DATE->format('Y-m-d');
						// var_dump($nextDate);die;
					} else if ($data['DATEE_TYPE'] == 3) {
						// echo 'here';
						$start = $this->_getParam('PS_STARTDATEPERIODIC');
						$STR_DATE = DateTime::createFromFormat('d/m/Y', $start);
						$START_DATE = $STR_DATE->format('Y-m-d');


						// $START_DATE = join('-',array_reverse(explode('/',date('d/m/Y'))));

						$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
						$expDate = $EXPIRY_DATE->format('Y-m-d');
					}
					// var_dump($TrfDateType);
					// 	var_dump($START_DATE);die;
					// var_dump($TrfDateType);

					if (!empty($data['DATEE_TYPE'] == 3 && !empty($endDate))) {
						$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
						$endDate = $EF_DATE->format('Y-m-d');
						// var_dump($endDate);die;
					}

					if (empty($endDate)) {
						if ($data['DATEE_TYPE'] == 2) {

							// $endDate = join('-',array_reverse(explode('/',date('d/m/Y'))));
							$EF_DATE = DateTime::createFromFormat('d/m/Y', $efDate);
							$endDate = $EF_DATE->format('Y-m-d');
						} else {
							$endDate = join('-', array_reverse(explode('/', date('d/m/Y'))));
							$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
							$endDate = $EF_DATE->format('Y-m-d');
						}
					}

					// var_dump($START_DATE);
					// var_dump($endDate);die;
					if ($data['DATEE_TYPE'] == 3) {
					$insertPeriodic = array(
						'PS_EVERY_PERIODIC' 	=> $repeatEvery,
						'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
						'PS_EVERY_PERIODIC_UOM' => $repetition, 	// 5: every day of, 6: every date of
						'PS_PERIODIC_STARTDATE' => $START_DATE,
						'PS_PERIODIC_ENDDATE'	=> $endDate,
						'PS_PERIODIC_NEXTDATE'	=> $nextDate,
						'PS_PERIODIC_STATUS' 	=> 2,					// 2: INPROGRESS KALO BELUM BERAKHIR, 1: COMPLETE KALO SUDAH HABIS END DATE, 0: CANCEL
						'USER_ID' 				=> $this->_userIdLogin,
						'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
						'SESSION_TYPE'			=> 0,
					);
					
					
					 //echo '<pre>';
					 //var_dump($this->_request->getParams());
					 //var_dump($data); 
					 //print_r($insertPeriodic);die();

					$this->_db->insert('T_PERIODIC', $insertPeriodic);
					$psPeriodicID =  $this->_db->lastInsertId();
					}

					//select data beneficiary dari m_apikey
					$beneList = $this->_db->fetchAll(
						$this->_db->select()
							->from(array('A' => 'M_APIKEY'), array('*'))
							->where("A.VALUE = ? ", $acBenef)
							->where("A.CUST_ID = ? ", $this->_custIdLogin)
							->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
							->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
					);

					foreach ($beneList as $key => $value) {
						$newBeneList[$value['FIELD']] = $value['VALUE'];
						$newBeneList['bank_code'] = $value['BANK_CODE'];
						$newBeneList['alias_name'] = $value['account_alias'];
						$newBeneList['bank_name'] = $value['BANK_NAME'];
						$newBeneList['id'] = $value['APIKEY_ID'];
					}


					//if upload then decode json

					if ($sourceType == 1) {
						$acSource = $acSource;
					} else {
						$acSource = json_decode($acSource, 1);
					}

					if ($accteopen == 1) {
						$acctBenef = json_decode($acBenef, 1);
					}
					// var_dump($sourceType);die;
					//if single select
					if ($sourceType == 1) {

						//select data source account dari m_apikey
						$sourceList = $this->_db->fetchAll(
							$this->_db->select()
								->from(array('A' => 'M_APIKEY'), array('*'))
								->where("A.VALUE = ? ", $acSource)
								->where("A.CUST_ID = ? ", $this->_custIdLogin)
								->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
								->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
						);

						foreach ($sourceList as $key => $value) {
							$newSourceList[$value['FIELD']] = $value['VALUE'];
							$newSourceList['bank_code'] = $value['BANK_CODE'];
							$newSourceList['bank_name'] = $value['BANK_NAME'];
						}
						
						if ($data['DATEE_TYPE'] == 3) {
						
						//T_PERIODIC_DETAIL
						$insertPeriodicDetail = array(
							'PS_PERIODIC' 				=> $psPeriodicID,
							'SOURCE_ACCOUNT' 			=> $acSource,
							'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
							'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
							'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
							'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
							'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
							'BENEFICIARY_ACCOUNT' 		=> $acBenef,
							'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
							'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
							// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
							'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
							'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
							// 'TRA_AMOUNT' 				=> $remainsBalance,
							'TRA_MESSAGE' 				=> $message,
							'TRA_MESSAGE_ADD'			=> '',
							'PAYMENT_TYPE'				=> 19,
							'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
							'BALANCE_TYPE'				=> 1,  // 1=Fixed Amount;2=Percentage
							'TRA_REMAIN'				=> $remainsBalance
						);
						$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

						$report_day = $this->_getParam('report_day');
						//var_dump($data);die;
						if (!empty($report_day)) {

							$filter_day = array_unique($report_day);
							foreach ($filter_day as $keyday => $valday) {
								if($data['POOLING'][$valday] == '' ){
									$data['POOLING'][$valday] = 0;
								}
								$insertPeriodicday = array(
									'PERIODIC_ID' => $psPeriodicID,
									'DAY_ID'		=> $valday,
									'LIMIT_AMOUNT'		=> $data['POOLING'][$valday]
								);

								$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
							}
						}
						
						}
					}

					//if data upload
					else {

						//loop data upload source dan select dri db m_apikey
						// echo "<pre>";
						// var_dump($acSource);die;
						//        	echo "<pre>";
						// var_dump($acSource);
						// var_dump($this->_request->getParams());die;

						// var_dump($accteopen);die;
						if ($accteopen == 1) {

							foreach ($acctBenef as $row => $source) {

								$sourceList = $this->_db->fetchAll(
									$this->_db->select()
										->from(array('A' => 'M_APIKEY'), array('*'))
										->where("A.VALUE = ? ", $source['ACCT_NO'])
										->where("A.CUST_ID = ? ", $this->_custIdLogin)
										->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
										->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
								);

								foreach ($sourceList as $key => $value) {
									$newSourceList[$value['FIELD']] = $value['VALUE'];
									$newSourceList['bank_code'] = $value['BANK_CODE'];
									$newSourceList['bank_name'] = $value['BANK_NAME'];
								}
								if ($data['DATEE_TYPE'] == 3) {
								//T_PERIODIC_DETAIL
								$insertPeriodicDetail = array(
									'PS_PERIODIC' 				=> $psPeriodicID,
									'SOURCE_ACCOUNT' 			=> $acSource,
									'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
									'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
									'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
									'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
									'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
									'BENEFICIARY_ACCOUNT' 		=> $source['ACCT_NO'],
									'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
									'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
									// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
									'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
									'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
									// 'TRA_AMOUNT' 				=> $remainsBalance,
									'TRA_MESSAGE' 				=> $message,
									'TRA_MESSAGE_ADD'			=> '',
									'PAYMENT_TYPE'				=> 19,
									'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
									'BALANCE_TYPE'				=> 1,  // 1=Fixed Amount;2=Percentage
									'TRA_REMAIN'				=> $source['REMAIN_BALANCE']
								);
								$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

								$report_day = $this->_getParam('report_day');

								if (!empty($report_day)) {
									$filter_day = array_unique($report_day);
									foreach ($report_day as $keyday => $valday) {

										$insertPeriodicday = array(
											'PERIODIC_ID' => $psPeriodicID,
											'DAY_ID'		=> $valday,
											'LIMIT_AMOUNT'		=> $data['POOLING'][$valday]
										);

										$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
									}
								}
								
								}
								//reset array
								unset($newSourceList);
							}
						} else {

							foreach ($acSource as $row => $source) {

								$sourceList = $this->_db->fetchAll(
									$this->_db->select()
										->from(array('A' => 'M_APIKEY'), array('*'))
										->where("A.VALUE = ? ", $source['ACCT_NO'])
										->where("A.CUST_ID = ? ", $this->_custIdLogin)
										->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
										->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
								);

								foreach ($sourceList as $key => $value) {
									$newSourceList[$value['FIELD']] = $value['VALUE'];
									$newSourceList['bank_code'] = $value['BANK_CODE'];
									$newSourceList['bank_name'] = $value['BANK_NAME'];
								}
			
								if ($data['DATEE_TYPE'] == 3) {
								//T_PERIODIC_DETAIL
								$insertPeriodicDetail = array(
									'PS_PERIODIC' 				=> $psPeriodicID,
									'SOURCE_ACCOUNT' 			=> $source['ACCT_NO'],
									'SOURCE_ACCOUNT_CCY' 		=> $newSourceList['account_currency'],
									'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
									'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
									'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
									'SOURCE_ACCOUNT_BANK_NAME' 	=> $newSourceList['bank_name'],
									'BENEFICIARY_ACCOUNT' 		=> $acBenef,
									'BENEFICIARY_ACCOUNT_CCY' 	=> $newBeneList['account_currency'],
									'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
									// 'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
									'BENEFICIARY_BANK_CODE' 	=> $newBeneList['bank_code'],
									'BENEFICIARY_BANK_NAME' 	=> $newBeneList['bank_name'],
									// 'TRA_AMOUNT' 				=> $remainsBalance,
									'TRA_MESSAGE' 				=> $message,
									'TRA_MESSAGE_ADD'			=> '',
									'PAYMENT_TYPE'				=> 19,
									'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
									'BALANCE_TYPE'				=> 1,  // 1=Fixed Amount;2=Percentage
									'TRA_REMAIN'				=> $source['REMAIN_BALANCE']
								);
								$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);

								$report_day = $this->_getParam('report_day');

								if (!empty($report_day)) {

									$filter_day = array_unique($report_day);
									foreach ($filter_day as $keyday => $valday) {

										$insertPeriodicday = array(
											'PERIODIC_ID' => $psPeriodicID,
											'DAY_ID'		=> $valday,
											'LIMIT_AMOUNT'		=> $data['POOLING'][$valday]
										);

										$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
									}
								}
								
								
								// echo '<pre>';
								// var_dump($data);

								$pooling_data = $data['POOLING'];
								$poolingamount = Application_Helper_General::convertDisplayMoney($data['MAINTAIN_BALANCE']);
								if (empty($poolingamount)) {
									$poolingamount = 0;
								}
								if (!empty($pooling_data)) {

									foreach ($pooling_data as $keyday => $valday) {
										if ($valday != NULL) {
											$insertPoolingcday = array(
												'PERIODIC_ID' => $psPeriodicID,
												'DAY_ID'		=> $keyday,
												'AMOUNT'		=> $valday
											);
											// var_dump($insertPoolingcday);die;
											$this->_db->insert('T_PERIODIC_POOLING', $insertPoolingcday);
										}
										// else{
										// 	$insertPoolingcday = array(
										// 		'PERIODIC_ID' => $psPeriodicID,
										// 		'DAY_ID'		=> $keyday,
										// 		'AMOUNT'		=> $poolingamount
										// 	);

										// 	$this->_db->insert('T_PERIODIC_POOLING', $insertPoolingcday);

										// }

									}
								} else {

									foreach ($report_day as $keyday => $valday) {
										$insertPoolingcday = array(
											'PERIODIC_ID' => $psPeriodicID,
											'DAY_ID'		=> $keyday,
											'AMOUNT'		=> $poolingamount
										);
										// var_dump($insertPoolingcday);die;
										$this->_db->insert('T_PERIODIC_POOLING', $insertPoolingcday);
									}
								}

								}
								//reset array
								unset($newSourceList);
							}
						}
					}

					$this->_db->commit();



					$date = new DateTime();
					$arrday = array(
						'0' => 'sunday',
						'1' => 'monday',
						'2' => 'tuesday',
						'3' => 'wednesday',
						'4' => 'thursday',
						'5' => 'friday',
						'6' => 'saturday'

					);
					// Modify the date it contains
					$datenumb = date('w');
					$datenumb = $datenumb + 1;
					if ($datenumb == 6) {
						$datename = 0;
					} else {
						$datename = $datenumb;
					}
					// var_dump($this->_request->getParams());die;
					// var_dump($efDate);
					// var_dump($START_DATE);
					// var_dump($endDate);die;
					$daterange = $this->GetDays($START_DATE, $endDate);
					// var_dump($daterange);die;

					// if(!empty($repeatOn)){
					// $resutnext = 0;
					// // echo 'here';
					foreach ($daterange as $no => $values) {

						$day = date('w', strtotime($values));
						foreach ($repeatOn as $key => $value) {
							if ($day == $value) {
								$arrdate[] = $values;
							}
						}
					}
					// var_dump(min($arrdate));
					// die;



					// echo "string";
					if (!empty($arrdate)) {
						// ;

						$efDate = min($arrdate);
					}

					// }

					// echo "<pre>";
					// 		var_dump($repetition);
					// 		var_dump($this->_request->getParams());die;

					//param utk create payment sweep
					if ($data['DATEE_TYPE'] == 3) {
					$param['PS_PERIODIC'] 			= $psPeriodicID;
					}
					$param['PS_SUBJECT'] 			= $paymentSubject;
					if ($trfDateType == 2) {
						$data['PS_EFDATE']  			= $expDate;
						// new Zend_Db_Expr("GETDATE()");	
					} else if ($trfDateType == 3) {
						if ($repetition == 1) {
							$param['PS_EFDATE']  			= $nextDate;
						}
						$data['PS_EFDATE']  			= $nextDate;
					}
					
					$param['PS_EFDATE']  			= $nextDate;
					$param['PS_EFTIME']  			= $data['EFTIME'];
					if ($sweepOption == 1) {
						$param['PS_TYPE'] 				= 19;
					} else {
						$param['PS_TYPE'] 				= 20;
					}
					$param['PS_CCY']  				= 'IDR';
					$param['REF_ID']  				= $paymentReff;
					$param['REMAIN_BALANCE_TYPE']	= $remainBalanceType;
					$param['SWEEP'] 				= "SI";
					$param['SWEEP_TYPE'] 				= $sweepOption;
					$param['BENE_DATA']				= $newBeneList;
					$param['TRA_MIN_AMOUNT']		= Application_Helper_General::convertDisplayMoney($data['TRA_MIN_AMOUNT']);

					 //var_dump($this->_request->getParams());die;
					//if single select
					if ($sourceType == 1) {
						// var_dump($sweepOption);die;
						if ($sweepOption == 1) {
							$traamount = $remainsBalance;
						} else {
							$traamount = Application_Helper_General::convertDisplayMoney($data['MAINTAIN_BALANCE']);
						}

						//data transaction single
						$param['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 			=> $acSource,
							'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
							'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
							'SOURCE_ACCOUNT_BANK_CODE' 	=> $newSourceList['bank_code'],
							'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
							'BENEFICIARY_ID' 			=> $newBeneList['id'],
							'BENEFICIARY_ACCOUNT' 		=> $acBenef,
							'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
							'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
							'BENEFICIARY_ALIAS_NAME' 	=> $newBeneList['alias_name'],
							'TRANSFER_TYPE' 			=> 'PB',
							'TRA_AMOUNT' 				=> $traamount,
							'TRA_MESSAGE' 				=> $message,
							'TRA_REFNO'					=> '',
							'TRA_ADDITIONAL_MESSAGE' 	=> '',
							'TRA_REMAIN'				=> $traamount
						);
					} else {

						//data transaction upload file


						// echo "<pre>";
						// var_dump($acSource);
						// var_dump($this->_request->getParams());die;


						if ($accteopen == 1) {

							foreach ($acctBenef as $row => $source) {

								// var_dump($source);
								$sourceList = $this->_db->fetchAll(
									$this->_db->select()
										->from(array('A' => 'M_APIKEY'), array('*'))
										->where("A.VALUE = ? ", $source['ACCT_NO'])
										->where("A.CUST_ID = ? ", $this->_custIdLogin)
										->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
										->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
								);
								// var_dump($source['NOTIF_EMAIL']);
								foreach ($sourceList as $key => $value) {
									$newSourceList[$value['FIELD']] = $value['VALUE'];
									$newSourceList['bank_code'] = $value['BANK_CODE'];
									$newSourceList['alias_name'] = $value['account_alias'];
									$newSourceList['bank_name'] = $value['BANK_NAME'];
								}

								if (!empty($source['NOTIF_EMAIL']) || !empty($source['NOTIF_SMS'])) {
									$TRA_NOTIF = '1';
								} else {
									$TRA_NOTIF = '0';
								}
								$param['TRANSACTION_DATA'][] = array(
									'SOURCE_ACCOUNT' 			=> $acSource,
									'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
									'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
									'SOURCE_ACCOUNT_BANK_CODE' 	=> $data['BANK_CODE'],
									'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
									'BENEFICIARY_ID' 			=> $newBeneList['id'],
									'BENEFICIARY_ACCOUNT' 		=> $source['ACCT_NO'],
									'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
									'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
									'BENEFICIARY_ALIAS_NAME' 	=> $newBeneList['alias_name'],
									'BENEFICIARY_EMAIL'			=> $data['ACBENEF_EMAIL'],
									'SWIFT_CODE'				=> $data['SWIFT_CODE'],
									'CLR_CODE'					=> $data['CLR_CODE'],
									'BANK_CODE'					=> $data['BANK_CODE'],
									'TRANSFER_TYPE' 			=> 'PB',
									'TRA_AMOUNT' 				=> $source['REMAIN_BALANCE'],
									'TRA_MESSAGE' 				=> $message,
									'TRA_REFNO'					=> $source['CUST_REF'],
									'TRA_ADDITIONAL_MESSAGE' 	=> '',
									'TRA_REMAIN'				=> $source['REMAIN_BALANCE'],
									'TRA_NOTIF'				=> $TRA_NOTIF,
									'TRA_EMAIL'				=> $source['NOTIF_EMAIL'],
									'TRA_SMS'				=> $source['NOTIF_SMS']
								);

								//reset array
								unset($newSourceList);
							}
						} else {

							foreach ($acSource as $row => $source) {

								// var_dump($source);die;
								$sourceList = $this->_db->fetchAll(
									$this->_db->select()
										->from(array('A' => 'M_APIKEY'), array('*'))
										->where("A.VALUE = ? ", $source['ACCT_NO'])
										->where("A.CUST_ID = ? ", $this->_custIdLogin)
										->join(array('B' => 'M_APIKEY'), 'A.APIKEY_ID = B.APIKEY_ID', array('*'))
										->join(array('C' => 'M_BANKTABLE'), 'A.BANK_CODE = C.BANK_CODE', array('BANK_NAME'))
								);
								// var_dump($source['NOTIF_EMAIL']);
								foreach ($sourceList as $key => $value) {
									$newSourceList[$value['FIELD']] = $value['VALUE'];
									$newSourceList['bank_code'] = $value['BANK_CODE'];
									$newSourceList['alias_name'] = $value['account_alias'];
									$newSourceList['bank_name'] = $value['BANK_NAME'];
								}

								if (!empty($source['NOTIF_EMAIL']) || !empty($source['NOTIF_SMS'])) {
									$TRA_NOTIF = '1';
								} else {
									$TRA_NOTIF = '0';
								}
								$param['TRANSACTION_DATA'][] = array(
									'SOURCE_ACCOUNT' 			=> $source['ACCT_NO'],
									'SOURCE_ACCOUNT_NAME' 		=> $newSourceList['account_name'],
									'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
									'SOURCE_ACCOUNT_BANK_CODE' 	=> $data['BANK_CODE'],
									'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
									'BENEFICIARY_ID' 			=> $newBeneList['id'],
									'BENEFICIARY_ACCOUNT' 		=> $acBenef, 
									'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
									'BENEFICIARY_ACCOUNT_NAME' 	=> $newBeneList['account_name'],
									
									'SWIFT_CODE'				=> $data['SWIFT_CODE'],
									'CLR_CODE'					=> $data['CLR_CODE'],
									'BANK_CODE'					=> $data['BANK_CODE'],
									'BENEFICIARY_ALIAS_NAME' 	=> $newBeneList['alias_name'],
									'TRANSFER_TYPE' 			=> 'PB',
									'TRA_AMOUNT' 				=> $source['REMAIN_BALANCE'],
									'TRA_MESSAGE' 				=> $message,
									'TRA_REFNO'					=> $source['CUST_REF'],
									'TRA_ADDITIONAL_MESSAGE' 	=> '',
									'TRA_REMAIN'				=> $source['REMAIN_BALANCE'],
									'TRA_NOTIF'				=> $TRA_NOTIF,
									'TRA_EMAIL'				=> $source['NOTIF_EMAIL'],
									'TRA_SMS'				=> $source['NOTIF_SMS']
								);

								//reset array
								unset($newSourceList);
							}
						}
					}

					$param['_addBeneficiary'] = $this->view->hasPrivilege('BADA');
					$param['_beneLinkage'] = $this->view->hasPrivilege('BLBU');
					$param['_priviCreate'] = 'IPMO';

					$SweepPayment = new SweepPayment(null, $this->_custIdLogin, $this->_userIdLogin);

					 //echo '<pre>';
					 //print_r($param);die();

					if($this->_custSameUser){
										if(!$this->view->hasPrivilege('PRLP')){
											// die('here');
											
											$errMessage = $this->language->_("Error: You don't have privilege to release payment");
											$this->view->error = true;
											$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
										}else{
											
											///google auth
											$challengeCode		= $this->_getParam('challengeCode');

											$inputtoken1 		= $this->_getParam('inputtoken1');
											$inputtoken2 		= $this->_getParam('inputtoken2');
											$inputtoken3 		= $this->_getParam('inputtoken3');
											$inputtoken4 		= $this->_getParam('inputtoken4');
											$inputtoken5 		= $this->_getParam('inputtoken5');
											$inputtoken6 		= $this->_getParam('inputtoken6');

											$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;


											$select3 = $this->_db->select()
												 ->from(array('C' => 'M_USER'));
											$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
											// $select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
											$data2 = $this->_db->fetchRow($select3);

											// $code = $param['googleauth'];
		

											$pga = new PHPGangsta_GoogleAuthenticator();
									    	 //var_dump($data2['GOOGLE_CODE']);
									    	 //var_dump($code);
									    	 //print_r($responseCode);die();
											$setting 		= new Settings();
											$google_duration 	= $setting->getSetting('google_duration');
									        if($pga->verifyCode($data2['GOOGLE_CODE'], $responseCode, $google_duration))
									        {
									        	$datatoken = array(
															'USER_FAILEDTOKEN' => 0
														);

														$wheretoken =  array();
														$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
														$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
														$data = $this->_db->update('M_USER',$datatoken,$wheretoken);
									        	$resultToken = $resHard['ResponseCode'] == '0000';
									        	$tokenAuth = true;
									        }else{
									        	$tokenFailed = $CustomerUser->setLogToken();
									        	$tokenAuth = false;	
									        	$this->view->popauth = true;
									        	if ($tokenFailed === true) {
										 		$this->_redirect('/default/index/logout');
										 	}
									        }
										}
										//var_dump($tokenAuth);
					if($tokenAuth){
						$param['sameuser'] = 1;
						$result = $SweepPayment->createOpenSweep($param);
					}else{
						$this->view->error = true;
						// $docErr = $this->displayError($zf_filter->getMessages());
						// print_r($docErr);die;
						$this->view->tokenError = true;
						$docErr = 'Invalid Token';
						$this->view->report_msg = $docErr;
					}					
										
				}else{
					//echo '<pre>';
					//var_dump($param);die; 
					$result = $SweepPayment->createOpenSweep($param);
				}
					
					if($this->_custSameUser){
						
						if($result){
							$this->view->paymentReff = $result;
							$this->view->result = $result;
						} 
						
					}else{
							$this->setbackURL('/opensweep/samebank'); 
							$this->_redirect('/notification/success/index');
					}

					
				} catch (Exception $e) {
					// $this->_db->rollBack();
					 //print_r($e);
					 //die();
				}
			} else {
				$this->view->error = true;
				$docErr = $this->displayError($zf_filter->getMessages());
				// print_r($docErr);die;
				
				$this->view->report_msg = $docErr;
			}
		}
		// echo "<pre>";
		// var_dump($data);die;


		//maintain balance
		if (!empty($data['POOLING'])) {
			$arrday = array(
				'0' => 'sun',
				'1' => 'mon',
				'2' => 'tue',
				'3' => 'wed',
				'4' => 'thu',
				'5' => 'fry',
				'6' => 'sat'

			);
			foreach ($data['POOLING'] as $key => $value) {
				// var_dump($arrday[$key]);
				${'pooling_' . $arrday[$key]} = $value;
				// var_dump($pooling_sun);
			}
			// var_dump($pooling_sun);die;
			// var_dump( $data['POOLING']);die;


			$this->view->pooling_sun = $pooling_sun;
			$this->view->pooling_mon = $pooling_mon;
			$this->view->pooling_tue = $pooling_tue;
			$this->view->pooling_wed = $pooling_wed;
			$this->view->pooling_thu = $pooling_thu;
			$this->view->pooling_fry = $pooling_fry;
			$this->view->pooling_sat = $pooling_sat;

			$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
			$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
			$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
			$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
			$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
			$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
			$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);
			// var_dump($data);die;
			if (!empty($data['MAINTAIN_BALANCE'])) {

				$this->view->mainbalance = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($data['MAINTAIN_BALANCE']));
			}
		}
			
		$this->view->paymentSubject = $data['SUBJECT'];
		// if($data['BALANCE_TYPE'])
		$this->view->sweepbene = $data['SWEEPIN'];
		$this->view->sourceType = $data['SOURCE_TYPE'];
		$this->view->acSource = $data['ACCT_NO'];
		
		$this->view->acSourceBank = $data['databank']['0']['BANK_NAME'];
		
		foreach($data['dataacct'] as $keyacct => $valacct){
			if($valacct['ACCT_NO'] == $data['ACCT_NO']){
				$this->view->acSourceAlias = $valacct['ACCT_ALIAS'];
			}
			
			if($valacct['ACCT_NO'] == $data['BENEF_NO']){
				$this->view->acBenefAlias = $valacct['ACCT_ALIAS'];
			}
		}
		
		
		$this->view->acBenefEmail = $data['ACBENEF_EMAIL'];
		$this->view->acBenef = $data['BENEF_NO'];

		// $paramdata['databank'] = $databank;
		// $paramdata['dataacct'] = $acct;

		$aliasbenef = '';
		foreach ($data['dataacct'] as $key => $value) {
			if ($value['ACCT_NO'] == $data['ACCT_NO']) {
				// var_dump($value);die;
				$aliassource = $value['ACCT_ALIAS'];
			}

			if ($value['ACCT_NO'] == $data['BENEF_NO']) {
				$aliasbenef = $value['ACCT_ALIAS'];
			}
		}

		// var_dump(expression)
		$bank_name = '';
		foreach ($data['databank'] as $key => $value) {
			if ($value['BANK_CODE'] == $data['BANK_CODE']) {
				$bank_name = $value['BANK_NAME'];
			}
		}

		$this->view->benefName = $data['BENEF_NO'].' (IDR)';
		$this->view->sourceName = $data['ACCT_NO'].' (IDR)';
		// if($data['SWEEPIN'] == '1'){
		$this->view->beneSource = $data['BENEF_NO'];
		// 	$this->view->sourceType = '1';
		// 	$this->view->acSource = $data['BENEF_NO'];
		// 	$this->view->sourceName = $data['BENEF_NAME'];
		// }


		$this->view->sweepOption = $data['SWEEP_TYPE'];


		$this->view->remainsBalance = $data['REMAIN_BALANCE'];
		$this->view->remainBalanceType = $data['BALANCE_TYPE'];

		$this->view->trfDateType = $data['DATEE_TYPE'];
		$this->view->efDate = $data['EFDATE'];
		$this->view->endDate = $data['ENDDATE'];
		$this->view->repetition = $data['REPETITION'];
		$this->view->repeatEvery = $data['REPEAT_EVERY'];
		$this->view->repeatOn = $data['REPEAT_DAY'];

		$this->view->notif = $data['NOTIF'];
		$this->view->email_notif = $data['EMAIL_NOTIF'];
		$this->view->sms_notif = $data['SMS_NOTIF'];
		$this->view->message = $data['DESC'];
		$this->view->PS_EFTIMEP = $data['EFTIME'];
		$this->view->PS_EFTIMEF = $data['EFTIME'];

		$this->view->repeat_day = $data['REPORT_DAY'];
	}

	function GetDays($sStartDate, $sEndDate)
	{
		// Firstly, format the provided dates.  
		// This function works best with YYYY-MM-DD  
		// but other date formats will work thanks  
		// to strtotime().  
		$sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
		$sEndDate = gmdate("Y-m-d", strtotime($sEndDate));

		// Start the variable off with the start date  
		$aDays[] = $sStartDate;

		// Set a 'temp' variable, sCurrentDate, with  
		// the start date - before beginning the loop  
		$sCurrentDate = $sStartDate;

		// While the current date is less than the end date  
		// var_dump($sCurrentDate);
		// var_dump($sEndDate);
		// $sCurrentDate = gmdate("Y-m-d", strtotime("+2 day", strtotime($sCurrentDate)));  
		// var_dump($sCurrentDate);die;
		while ($sCurrentDate < $sEndDate) {
			// Add a day to the current date
			$sCurrentDate = date('Y-m-d', strtotime($sCurrentDate . "+1 days"));
			// strtotime($sCurrentDate . "+1 days");  
			// $sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));  
			// if($sCurrentDate == '2020-01-30'){
			// var_dump($sCurrentDate);die;
			// }

			// Add this new day to the aDays array  
			$aDays[] = $sCurrentDate;
		}

		// Once the loop has finished, return the  
		// array of days.  
		return $aDays;
	}

	function moneyAliasFormatter($n)
	{
		// first strip any formatting;
		return str_replace('.00', '', Application_Helper_General::displayMoney($n));
		// $n = (0+str_replace(",", "", $n));

		// // is this a number?
		// if (!is_numeric($n)) return false;

		// // now filter it;
		// if ($n > 1000000000000) return round(($n/1000000000000), 2).' T';
		// elseif ($n > 1000000000) return round(($n/1000000000), 2).' B';
		// elseif ($n > 1000000) return round(($n/1000000), 2).' M';
		// elseif ($n > 1000) return $n;

		// return number_format($n);
	}

	function validateOpenSourceAccount($accsrc, $benef)
	{

		$error_array = array();
		$error_msg = array();
		$error = false;
		if ($accsrc == $benef) {
			$error = true;
			array_push($error_msg, 'Source Account Cannot Be Same As Beneficiary Account');
		}

		$sourceList = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'), array('*'))
				->where("A.VALUE = ? ", $accsrc)
				->where("A.CUST_ID = ? ", $this->_custIdLogin)
		);

		if (empty($sourceList)) {
			$error = true;
			array_push($error_msg, 'Source Account is Not Registered');
		}

		if ($error) {
			$error_array[$accsrc] = $error_msg;
		}

		return $error_array;
	}

	public function generatePaymentReff($forTransaction)
	{

		/*$paymentreff = "SI".date("Y").date("m").date("d").strtoupper(uniqid());
		
		return $paymentreff;*/

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(8));
		$checkDigit  = '';

		$paymentreff   = "SI" . $currentDate . $seqNumber . $checkDigit;

		return $paymentreff;
	}

	private function fillParams($param)
	{
		$this->view->paymentsubject			= $param["paymentsubject"];
		$this->view->ACBENEF				= $param["ACBENEF"];
		$this->view->ACBENEF_BANKNAME		= $param["ACBENEF_BANKNAME"];
		$this->view->ACBENEF_ALIAS			= $param["ACBENEF_ALIAS"];
		$this->view->CURR_CODE				= $param["CURR_CODE"];
		$this->view->datetime				= $param["end_date"];
		$this->view->end_date				= $param["end_date"];
		$this->view->REMAIN_BALANCE_TYPE	= $param["REMAIN_BALANCE_TYPE"];
		$this->view->TrfPeriodicType		= $param["tranferdateperiodictype"];
		$this->view->dayname 				= $param["dayname"];
		$this->view->day					= $param["day"];
		$this->view->sesion_sweep 			= $param["sesion_sweep"];
		$this->view->ACCTSRC				= $param["sesion_sweep"];
	}


	public function accountlistAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$bankCode = $this->_getParam('bankCode');

		// $complist = $this->_db->fetchAll(
		// 	$this->_db->select()
		// 		->from(array('A' => 'M_USER'), array('CUST_ID'))

		// 		->where("A.USER_ID = ? ", $this->_userIdLogin)
		// );

		// $comp = "'";
		// foreach ($complist as $key => $value) {
		// 	$comp .= "','" . $value['CUST_ID'] . "','";
		// }
		// $comp .= "'";


		$acctlist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'))
				->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
				->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
				// ->where('A.ACCT_STATUS = ?','5')
				// ->where("A.CUST_ID IN (" . $comp . ")")
				->where("A.CUST_ID = ?",$this->_custIdLogin)
				->where("A.BANK_CODE = ?", $bankCode)
				->order('A.APIKEY_ID ASC')
			// echo $acctlist;
		);

		$account = array();
		//echo '<pre>';
		//var_dump($acctlist);die;
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			if(empty($value['account_alias'])){
				
					$alias = $account[$value['ID']]['account_name'];
			}else{
				$alias = $account[$value['ID']]['account_alias'];
			}
			
			$account[$value['ID']]['account_alias'] = $alias;
		}
		
	//	echo '<pre>';
	//	var_dump($account);die;
		$acct = array();
		foreach ($account as $key => $value) {
			$acct[$key - 1]['ACCT_NO'] = $value['account_number'];
			$acct[$key - 1]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$key - 1]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$key - 1]['ACCT_NAME'] = $value['account_name'];
		}

			// $html = "<option value=''>-- Select Source Account --</option>";
			// foreach ($acct as $key => $value) {
			// 	$html .= "<option value = '" . $value['ACCT_NO'] . "'>";

			// 	if (!empty($value['ACCT_ALIAS'])) {
			// 		// $html .= $value['ACCT_NO']." [".$value['ACCT_BANK']."] - ".$value['ACCT_ALIAS']."</option>";
			// 		$html .= $value['ACCT_NO'] . " (IDR) - " . $value['ACCT_ALIAS'] . "</option>";
			// 	} else {
			// 		// $html .= $value['ACCT_NO']." [".$value['ACCT_BANK']."] </option>";
			// 		$html .= $value['ACCT_NO'] . " (IDR) </option>";
			// 	}
			// }

		echo json_encode($acct);
	}

	public function generateTransactionID(){ 
 
		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(5));
		$trxId = 'FR'.$currentDate.$this->_custIdLogin.$seqNumber;

		return $trxId;
	}
	
	
	public function bulkAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$custID = $this->_custIdLogin;
		$userID = $this->_userIdLogin;
		$this->view->ccyArr = $this->getCcy();
		$CustomerUser = new CustomerUser($custID, $userID);

		$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;

		$select = $this->_db->select()
                          ->from(array('MAB'=>'M_APP_BOUNDARY'))
                          ->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
                          ->where('MAB.CUST_ID = ?', (string)$cust_id)
                          ->group('BOUNDARY_ID');
                    // echo $select;
                  $result = $this->_db->fetchAll($select);

                  $dataArr = array();
                  // $alf = 'A';
                  // $alf++;
                  // ++$alf;
                  // print_r((int)$alf);
                  // $nogroup = sprintf("%02d", 1);
                  // print_r($result);die;
          
          
                  foreach($result as $row){
                    list($grouptype,$groupname,$groupnum) = explode("_",$row['GROUP_USER_ID']);
                    if($grouptype == 'N')
                      $name = 'Group '.trim($groupnum,'0');
                    else
                      $name = 'Special Group';

                    $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
                                // echo $selectUser;die;
                                ->query()->fetchall();

                    $userlist = '';

          $policythen = explode(' THEN ', $row['POLICY']);

          foreach ($policythen as $keythen => $valuethen) {
          

                    $policy = explode(' AND ', $valuethen);

                    foreach ($policy as $key => $value) {
                      $replaceVal = str_replace('(', '', $value);
                      $replaceVal = str_replace(')', '', $replaceVal);
                      $policy[$key] = $replaceVal;
                    }

                    foreach ($policy as $key => $value) {
                      if($value == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist))
                            $userlist .= $val['USER_ID'];
                          else
                            $userlist .= ', '.$val['USER_ID'];
                        }                         
                      }else{



                        $alphabet = array("A"=>1,"B"=>2,"C"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>26,);

                        $policyor = explode(' OR ', $value);

                        // print_r($policyor);die;
                        foreach ($policyor as $numb => $valpol) {
                          if($valpol == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist)){
                            $userlist .= $val['USER_ID'];
                          }
                          else{
                            $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }else{
                          $nogroup = sprintf("%02d", $alphabet[$valpol]);
                          // print_r($valpol);
                          $group = 'N_'.$cust_id.'_'.$nogroup;
                          $selectUser = $this->_db->select()
                                  ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                  ->where('GROUP_USER_ID = ?', $group)
                                  // echo $selectUser;die;
                                  ->query()->fetchall();  
                                // print_r($selectUser);  
                          foreach($selectUser as $val){
                            if(empty($userlist))
                              $userlist .= $val['USER_ID'];
                            else
                              $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }
                        

                      }
                    }
          
          }
                    // print_r($userlist);die;
                    // $nogroup = sprintf("%02d", $key+1);

                    // foreach($selectUser as $val){
                    //  if(empty($userlist))
                    //    $userlist .= $val['USER_ID'];
                    //  else
                    //    $userlist .= ', '.$val['USER_ID'];
                    // }

                    $arrTraType     = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
                    $arrTraType['19'] = 'Cash Pooling Same Bank';
                    $arrTraType['20'] = 'Cash Pooling Same Bank';
                    $arrTraType['23'] = 'Cash Pooling Other Bank';


          $userl = explode(', ',$userlist);
          $userlist = array_unique($userl);
          $userlistnew = '';
          
          foreach($userlist as $ky  => $val){
            //var_dump($ky);
            if($ky == 0){
            $userlistnew .= $val;
            //var_dump($userlistnew);
            }else{
            $userlistnew .= ', '.$val;
            }
          }

              $changepolicy = explode(" ", $row['POLICY']);
              $data = array();
              foreach ($changepolicy as $keypolicy => $valpolicy) {

                if ($valpolicy == 'AND') {
                  $valpolicy = '<span style="color: blue;">and</span>';
                }elseif ($valpolicy == 'OR') {
                  $valpolicy = '<span style="color: blue;">or</span>';
                }elseif ($valpolicy == 'THEN') {
                  $valpolicy = '<span style="color: blue;">then</span>';
                }

                $replacepolicy1 = str_replace('(', '<span style="color: blue;">(</span>', $valpolicy);
                $replacepolicy2 = str_replace(')', '<span style="color: blue;">)</span>', $replacepolicy1);

                $data[$keypolicy] = $this->language->_($replacepolicy2);
              }

              $valpolicy2 = implode(" ", $data);

                    $dataArr[] = array( 
                              'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
                              'CURRENCY' => $row['CCY_BOUNDARY'],
                              'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN'])." - ".Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
                              'GROUP_NAME' => $valpolicy2,
                              'USERS' => $userlistnew);
                  }

          //var_dump($dataArr);
                  // print_r($dataArr);die;
                 /* print_r($row);die();*/

                  $this->view->dataBoundary = $dataArr;


		/*
		 * generate payment ref
		 */

		$banklist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'), array('A.BANK_CODE'))
				// ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
				//  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
				//  // ->join(array('D' => 'M_DOMESTIC_BANK_TABLE'),'D.BANK_NAME = B.BANK_NAME',array('D.SWIFT_CODE'))
				->where('A.CUST_ID = ?', $this->_custIdLogin)
				->group('A.BANK_CODE')
			// ->where("A.BANK_CODE IN ('008','014','013','009','032','031','153','002') ")
			// ->order('A.APIKEY_ID ASC')
		);
		// var_dump($banklist);
		$par = array();
		if (!empty($banklist)) {
			foreach ($banklist as $key => $value) {
				$par[] = $value['BANK_CODE'];
			}
		}
		// var_dump($par);
		// var_dump($banklist);die;
		if (empty($par)) {
			$par = array('000');
		}

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'))
			->where('C.BANK_CODE IN (?)', $par);
		// echo $selectbank;die;
		$databank 					= $this->_db->fetchAll($selectbank);

		$this->view->BankArr = $databank;


		$this->view->paymentreff = $this->generatePaymentReff(1); //send payment ref to view

		$this->view->sourceType = 2; //source type, 1 = single select, 2 = upload

		// $complist = $this->_db->fetchAll(
		// 	$this->_db->select()
		// 		->from(array('A' => 'M_USER'), array('CUST_ID'))

		// 		->where("A.USER_ID = ? ", $this->_userIdLogin)
		// );
		// // echo $complist;;die;
		// // var_dump($complist);die;
		// $comp = "'";
		// // print_r($complist);die;
		// foreach ($complist as $key => $value) {
		// 	$comp .= "','" . $value['CUST_ID'] . "','";
		// }
		// $comp .= "'";


		$acctlist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'))
				->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
				->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
				// ->where('A.ACCT_STATUS = ?','5')
				//->where("A.CUST_ID IN (" . $comp . ")")
				->where("A.CUST_ID = ?",$this->_custIdLogin)
				->order('A.APIKEY_ID ASC')
			// echo $acctlist;
		);
		// echo $acctlist;die;
		// echo '<pre>';
		// echo $acctlist;
		// print_r($acctlist);die;
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}

		$acct = array();
		foreach ($account as $key => $value) {
			$acct[$key - 1]['ACCT_NO'] = $value['account_number'];
			$acct[$key - 1]['ACCT_ALIAS'] = $value['account_alias'];
			$acct[$key - 1]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$key - 1]['ACCT_NAME'] = $value['account_name'];
		}
		// var_dump($acct);
		$AccArr = $acct;


		$paramscr = array('CCY_IN' => 'IDR');
		$this->view->sourceAcc = $AccArr;
		
		
		$TRA_MIN_AMOUNT = $this->_getParam('TRA_MIN_AMOUNT');
					
		//if (empty($TRA_MIN_AMOUNT)) {
			$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				foreach($ccyList as $k => $valccy){
					if($valccy['CCY_ID'] == 'IDR'){
						$minamt = str_replace('.00', '', Application_Helper_General::displayMoney($valccy['MIN_AMT']));
						$this->view->TRA_MIN_AMOUNT = $minamt;
					}
				}
			//var_dump($ccyList);die;
				
				//}
		//		$this->view->sourceAcc = $CustomerUser->getAccounts();
		// 		print_r($this->view->sourceAcc);die;
		//$sourceAcc =  $CustomerUser->getAccounts();

		$select   = $CustomerUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		$select->where("B.BENEFICIARY_ISAPPROVE = ?", 1);

		// $resultBeneficiary = $this->_db->fetchAll($select);
		// $this->view->listBeneficiary = $resultBeneficiary;
		// $this->view->listBeneficiary = $AccArr;
		$this->view->listBeneficiary = array('' => ' -- Please Bank -- ');

			

		/*
		 * Verify form
		*/

		$sessionNameRand = new Zend_Session_Namespace('openSweep');
		if ($this->_request->isPost()) {

			$isConfirmPage = $this->_getParam('confirmPage');
			$confirmPage = true;
			$TRA_MIN_AMOUNT = $this->_getParam('TRA_MIN_AMOUNT');
			if (empty($TRA_MIN_AMOUNT)) {
										$error = true;
										$confirmPage = false;
										$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Min amount cannot be left blank') . '.';
										$this->view->error      = true;
			}else{
				$minamount = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($minamt));
				$tra_min_amount = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT));
				$this->view->TRA_MIN_AMOUNT = $tra_min_amount;
				//var_dump($minamount);
				//var_dump($tra_min_amount);
				if((int)$minamount>(int)$tra_min_amount){
										$error = true; 
										$confirmPage = false;
										$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Min amount cannot lower than IDR ') .$minamt. '.';
										$this->view->error      = true;
				}
			}
			
			//var_dump($error_msg);
		//var_dump($error);die('here');
			//if hal pertama di hit, akan msk ke confirm  page
			if (!$isConfirmPage) {

				$sourceType = $this->_getParam('sourceType');
				$acctBank = $this->_getParam('ACCT_BANK');
				$beneBank = $this->_getParam('BENEF_BANK');

				if (empty($acctBank)) {
					$this->view->error = true;
					$confirmPage = false;
					$error_msg[] = 'Bank cannot be left blank. Please correct it';
					$this->view->error_msg = $error_msg;
				}

				//var_dump($sourceType);die;
				if ($sourceType == 1) { //single source
					$acSource = $this->_getParam('ACCTSRC');
					
				} else { //upload file
					// echo "<pre>";
					// var_dump($this->_request->getParams());
					$adapter = new Zend_File_Transfer_Adapter_Http();

					$adapter->setDestination($this->_destinationUploadDir);
					$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
					$extensionValidator->setMessage(
						$this->language->_('Error') . ': ' . $this->language->_('Extension file must be') . ' *.txt'
					);

					$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
					$sizeValidator->setMessage(
						'Error: File exceeds maximum size'
					);

					$adapter->setValidators(array(
						$extensionValidator,
						$sizeValidator,
					));

					$sourceFileName = substr_replace(basename($adapter->getFileName()), '', 100);

					$extension = explode('.', $sourceFileName);

					$extensionName = $extension[1];

					$newFileName = $adapter->getFileName() . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter('Rename', $newFileName);
					$error = false;
					// var_dump($newFileName);
					if ($adapter->isValid())
					// if (true)
					{

						if ($adapter->receive()) {

							//               	$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
							// $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

							$adapter->addFilter('Rename', $newFileName);
							// $adapter->receive();
							$myFile = file_get_contents($newFileName);
							$arry_myFile = explode("\n", $myFile);
							@unlink($newFileName);
							// echo "<pre>";

							$datasource = array();
							$sourceno = 0;
							if (!empty($arry_myFile)) {
								// var_dump($arry_myFile);die;
								foreach ($arry_myFile as $key => $value) {
									if ($key == 0) {
										$datafile = explode('|', $value);
										$file_id = $datafile['1'];
										$file_date = $datafile['4'] . '-' . sprintf("%02d", $datafile['3']) . '-' . sprintf("%02d", $datafile['2']);
										$total_trx = $datafile['5'];
										$ccy_trx = $datafile['6'];
									}
									$dataex = explode('|', $value);
									if(empty($dataex['2']) || $dataex['2'] == ''){
										//var_dump($dataex);
										//die('here1');
										$error = true;
										$confirmPage = false;
										$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Remain Balance cannot be left blank') . '.';
										$this->view->error      = true;
										break;
									}
									if ($dataex['0'] == '1') {
										$datasource[$sourceno]['ACCT_NO'] = $dataex['1'];
										$datasource[$sourceno]['REMAIN_BALANCE'] = $dataex['2'];
										$datasource[$sourceno]['DESC'] = $dataex['3'];
										$datasource[$sourceno]['CUST_REF'] = $dataex['4'];
										$datasource[$sourceno]['NOTIF_SMS'] = '';
										$datasource[$sourceno]['NOTIF_EMAIL'] = '';
										$sourceno++;
									}
								}

								foreach ($datasource as $key => $value) {
									$checkdata = $this->_db->fetchAll(
										$this->_db->select()
											->from(array('A' => 'M_APIKEY'), array('*'))
											->where("A.VALUE = ? ", $value['ACCT_NO'])
											->where("A.CUST_ID = ? ", $this->_custIdLogin)
										);
									if(empty($checkdata)){
										$error = true;
										$confirmPage = false;
										$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Account is not registered') . '.';
										$this->view->error      = true;	
									}
								}

								if (count($datasource) >= 50) {
									$error = true;
									$confirmPage = false;
									$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Max 50 data') . '.';
									$this->view->error      = true;
								}

								if (count($datasource) != $total_trx) {
									$error = true;
									$confirmPage = false;
									$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('total data file invalid') . '.';
									$this->view->error      = true;
								}
							}

							if ($this->_getParam('accteopen') == 1) {
								$input_file_id = $this->_getParam('FILE_IDBENE');
							} else {
								$input_file_id = $this->_getParam('FILE_ID');
							}

							if (strtoupper($input_file_id) != 'BYPASSIDFU') {
								if (strtoupper($input_file_id) != strtoupper($file_id)) {
									$error = true;
									$confirmPage = false;
									$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File ID or Uploaded Date not matched. Please check your uploaded file') . '.';
									$this->view->error      = true;
								}
							}


							if ($file_date != date('Y-m-d')) {
								$error = true;
								$confirmPage = false;
								$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File ID or Uploaded Date not matched. Please check your uploaded file') . '.';
								$this->view->error      = true;
							}

							// var_dump($arry_myFile);die;
							// $getTypeArr = explode(".", $sourceFileName);
							// $getType = $getTypeArr[1];

							// var_dump($newFileName);

							// $data = $this->_helper->parser->parseCSV($newFileName);

							// @unlink($newFileName);

							$acSource = $datasource;
							$acBenef = $this->_getParam('ACBENEF');
							if (!empty($datasource)) {
								foreach ($datasource as $key => $value) {
									if ($value['ACCT_NO'] == $acBenef) {
										$error = true;
										$this->view->error = true;
										$confirmPage = false;
										$error_msg[] = 'Source Account cannot be same with Beneficiary Account';
									}
								}
							} else //kalo total record = 0
							{
								$error = true;
								$confirmPage = false;
								$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Wrong File Formats or Delimited String') . '.';
								$this->view->error      = true;
							}
						}
					} else {

						$error = true;
						$this->view->error = true;
						$confirmPage = false;
						foreach ($adapter->getMessages() as $key => $val) {
							// var_dump($val);
							if ($key == 'fileUploadErrorNoFile')
								$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('File cannot be left blank. Please correct it') . '.';
							else
								$error_msg[] = $val;
							break;
						}
						// die('here');
					}
				}

				$acBenef = $this->_getParam('ACBENEF_PLAIN');
				// die('here1');
				//validasi source account tidak boleh sama dengan beneficiary account
				//if single source
				if ($sourceType == 1) {
					if ($acSource == $acBenef) {
						$this->view->error = true;
						$confirmPage = true;
						$error_msg[] = 'Source Account cannot be same with Beneficiary Account';
					}
				}
				//if upload
				else {

					//validasi source account
					$source_error = array();
					// if (!$error) {
					// 	foreach ($acSource as $key => $value) {
					// 		$error_array = $this->validateOpenSourceAccount($value, $acBenef);
					// 		if (!empty($error_array)) {
					// 			array_push($source_error, $error_array);		
					// 		}
					// 	}
					// }
				}

				if (!empty($source_error)) {
					$this->view->error = true;
					$confirmPage = true;
					$error_msg[] = 'Source Account Error';
					$this->view->source_error = $source_error;
				}
				
				$ACBENEF_EMAIL = $this->_getParam('ACBENEF_EMAIL');
				$ACBENEF_ALIAS = $this->_getParam('ACBENEF_ALIAS');
				
				$paymentReff = $this->_getParam('teamplate_reff');
				$paymentSubject = $this->_getParam('paymentsubject');
				$sourceName = $this->_getParam('sourceName');
				$benefName = $this->_getParam('benefName');
				$message = $this->_getParam('TRA_MESSAGE');
				$sweepOption = $this->_getParam('ta_sel');
				$trfDateType = $this->_getParam('tranferdatetype');
				if ($trfDateType == '2') {
					$trfTime = $this->_getParam('PS_EFTIMEF');
				} else {
					$trfTime = $this->_getParam('PS_EFTIMEP');
				}
				//var_dump($trfTime);
				$jam = preg_match("/^([01]?[0-9]|2[0-3])\:+[0-5][0-9]$/", $trfTime);
				//var_dump($jam);
				if(!$jam){
						// $this->view->error = true;
						// //$confirmPage = true;
						// $error_msg[] = 'Invalid Time Format';
				}

				//var_dump($sweepOption);die; 
				
				//remains balance
				if ($sweepOption == 1) {
					// die('vas');
					$remainBalanceType = $this->_getParam('rem_per');
					$remainsBalance = $this->_getParam('remainbalance');
					//Zend_Debug::dump($this->_getAllParams());
					//var_dump($remainsBalance);
					//die('fea');
					//$remainsBalance = $this->_getParam('remainbalance');
					if(empty($remainsBalance) && $sourceType == '1'){
						//var_dump($sourceType);
					//	die('here');
						$error = true;
								$confirmPage = false;
								$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Remain Balance cannot be left blank') . '.';
								$this->view->error      = true;
					}
				}
				//maintain balance
				else {
					// die('here');
					$maintainbalance =  $this->_getParam('maintainbalance');
					if ($trfDateType == 2) {
						if(empty($maintainbalance)){
							$error = true;
									$confirmPage = false;
									$error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Maintain Balance cannot be left blank') . '.';
									$this->view->error      = true;
						}
					}
					$report_day = $this->_getParam('report_day');
					
					$arrday = array(
						'0' => 'sun',
						'1' => 'mon',
						'2' => 'tue',
						'3' => 'wed',
						'4' => 'thu',
						'5' => 'fry',
						'6' => 'sat'

					);
					//var_dump($report_day);
					foreach ($report_day as $key => $value) {
						 
						$tag = 'pooling_' . $arrday[$value];
						$valueday = $this->_getParam($tag);
						//var_dump($tag);
						//var_dump($valueday);
						if($valueday == ''){
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Maintain balance amount cannot be blank';
							break;
						}
					}
					 
					
					$pooling_sun = $this->_getParam('pooling_sun');
					$pooling_mon = $this->_getParam('pooling_mon');
					$pooling_tue = $this->_getParam('pooling_tue');
					$pooling_wed = $this->_getParam('pooling_wed');
					$pooling_thu = $this->_getParam('pooling_thu');
					$pooling_fry = $this->_getParam('pooling_fry');
					$pooling_sat = $this->_getParam('pooling_sat');

					$fore_sun = $this->_getParam('forecast_sun');
					$fore_mon = $this->_getParam('forecast_mon');
					$fore_tue = $this->_getParam('forecast_tue');
					$fore_wed = $this->_getParam('forecast_wed');
					$fore_thu = $this->_getParam('forecast_thu');
					$fore_fry = $this->_getParam('forecast_fry');
					$fore_sat = $this->_getParam('forecast_sat');
				}

				//if immediate just pass it, bcs immediate will get current date and time when insert
				//futuredate
				if ($trfDateType == 2) {
					$efDate = $this->_getParam('PS_FUTUREDATE');
					$session = $this->_getParam('PS_EFTIMEF');

					if (empty($efDate)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Future Date Must Be Selected';
					}
					if (empty($session)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Session Must Be Selected';
					}
				} else if ($trfDateType == 3) {
					$repetition = $this->_getParam('repetition');
					$efDate = $this->_getParam('PS_EFDATE');
					//daily
					if ($repetition == 1) {
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
					}
					//weekly
					else if ($repetition == 2) {
						$repeatEvery = $this->_getParam('selectrepeat');
						$repeatOn = $this->_getParam('report_day');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

						if (empty($repeatEvery)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat Every Must Be Selected';
						}
						if (empty($repeatOn)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat On Be Selected';
						}
					}
					//monthly
					else if ($repetition == 3) {
						$repeatEvery = $this->_getParam('selectrepeat');
						// $repeatOn = $this->_getParam('PS_REPEATON');
						$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

						if (empty($repeatEvery)) {
							$this->view->error = true;
							$confirmPage = false;
							$error_msg[] = 'Repeat Every Must Be Selected';
						}
						// if (empty($repeatOn)) {
						// 	$this->view->error = true;
						// 	$confirmPage = false;
						// 	$error_msg[] = 'Repeat On Be Selected';
						// }
					}
					$trfDateType = $this->_getParam('tranferdatetype');
					if ($trfDateType == '3') {
						$session = $this->_getParam('PS_EFTIMEP');
					} else {
						$session = $this->_getParam('PS_EFTIMEF');
					}
					// var_dump($session);

					if (empty($endDate)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'End Date Must Be Selected';
					}
					if (empty($session)) {
						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = 'Session Time Must Be Selected';
					}
				}

				$notif = $this->_getParam('notif');

				if ($notif == 2) {
					$email_notif = $this->_getParam('email_notif');
					$sms_notif = $this->_getParam('sms_notif');
				}

				if (empty($error_msg)) {
					// var_dump($fore_sun);
					// var_dump($pooling_sun);
					// echo "<pre>";
					// var_dump($this->_request->getParams());die;
					$poolling = array(
						'0' => (isset($pooling_sun))			? $pooling_sun			: $fore_sun,
						'1' => (isset($pooling_mon))			? $pooling_mon			: $fore_mon,
						'2' => (isset($pooling_tue))			? $pooling_tue			: $fore_tue,
						'3' => (isset($pooling_wed))			? $pooling_wed			: $fore_wed,
						'4' => (isset($pooling_thu))			? $pooling_thu			: $fore_thu,
						'5' => (isset($pooling_fry))			? $pooling_fry			: $fore_fry,
						'6' => (isset($pooling_sat))			? $pooling_sat			: $fore_sat
					);
					// var_dump($poolling);die;
					$accteopen = $this->_getParam('accteopen');

					$databank = $this->_db->fetchAll(
						$this->_db->select()
							->from(array('A' => 'M_BANK_TABLE'), array('*'))
							->where("A.BANK_CODE = ? ", $acctBank)
					);
					// ->where("A.CUST_ID = ? ", $this->_custIdLogin));

					// acctBank$se
					//echo '<pre>';
					//var_dump($datasource);
					//var_dump($accteopen);

					$report_day = $this->_getParam('report_day');
					$toConfirm = false;
					// $repetition = $this->_getParam('repetition');
					if ($accteopen != '1') {
						// die('here');
						$paramdata = array(
							'SOURCE_TYPE' => $sourceType,
							'ACCT_NO'	=> $acSource,
							'ACBENEF_EMAIL' => $ACBENEF_EMAIL,
							'ACBENEF_ALIAS' => $ACBENEF_ALIAS,
							'ACCT_NAME'	=> $sourceName,
							'BENEF_NO' 	=> $acBenef,
							'BENEF_NAME' => $benefName,
							'DESC' 		=> $message,
							'DATEE_TYPE' 	=> $trfDateType,
							'SUBJECT' 		=> $paymentSubject,
							'BALANCE_TYPE' 		=> $remainBalanceType,
							'SWEEP_TYPE'	=> $sweepOption,
							'REMAIN_BALANCE' 		=> $remainsBalance,
							'EFDATE' 		=> $efDate,
							'ENDDATE' 		=> $endDate,
							'REPEAT_DAY'	=> $repeatOn,
							'REPEAT_EVERY'	=> $repeatEvery,
							'REPEAT_EVERY'	=> $repeatEvery,
							'NOTIF' 		=> $notif,
							'EMAIL_NOTIF' 		=> $email_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'POOLING'		=> $poolling,
							'REPETITION'	=> $repetition,
							'EFTIME'		=> $trfTime,
							'SWEEPIN'		=> $accteopen,
							'BANK_CODE'		=> $acctBank,
							'SWIFT_CODE'	=> $databank['0']['SWIFT_CODE'],
							'CLR_CODE'		=> $databank['0']['CLR_CODE'],
							'REPORT_DAY'	=> $report_day,
							'REPETITION'	=> $repetition,
							'MAINTAIN_BALANCE' => $maintainbalance,
							'TRA_MIN_AMOUNT' => $TRA_MIN_AMOUNT
						);
					} else {
						$acSources = $this->_getParam('ACCTSRC');
						$paramdata = array(
							'SOURCE_TYPE' => $sourceType,
							'ACCT_NO'	=> $acSources,
							'ACBENEF_EMAIL' => $ACBENEF_EMAIL,
							'ACCT_NAME'	=> $sourceName,
							'BENEF_NO' 	=> $acSource,
							'BENEF_NAME' => $benefName,
							'ACBENEF_ALIAS' => $ACBENEF_ALIAS,
							'DESC' 		=> $message,
							'DATEE_TYPE' 	=> $trfDateType,
							'SUBJECT' 		=> $paymentSubject,
							'BALANCE_TYPE' 		=> $remainBalanceType,
							'SWEEP_TYPE'	=> $sweepOption,
							'REMAIN_BALANCE' 		=> $remainsBalance,
							'EFDATE' 		=> $efDate,
							'ENDDATE' 		=> $endDate,
							'REPEAT_DAY'	=> $repeatOn,
							'REPEAT_EVERY'	=> $repeatEvery,
							'REPEAT_EVERY'	=> $repeatEvery,
							'NOTIF' 		=> $notif,
							'EMAIL_NOTIF' 		=> $email_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'SMS_NOTIF' 		=> $sms_notif,
							'POOLING'		=> $poolling,
							'REPETITION'	=> $repetition,
							'EFTIME'		=> $trfTime,
							'SWEEPIN'		=> $accteopen,
							'BANK_CODE'		=> $acctBank,
							'SWIFT_CODE'	=> $databank['0']['SWIFT_CODE'],
							'CLR_CODE'		=> $databank['0']['CLR_CODE'],
							'REPORT_DAY'	=> $report_day,
							'TRA_MIN_AMOUNT' => $TRA_MIN_AMOUNT
						);
						// echo "<pre>";
						// var_dump($paramdata);die;
					}

					$paramdata['databank'] = $databank;
					$paramdata['dataacct'] = $acct;
					$paramdata['selectUsergroup'] = $selectUsergroup;
					$paramdata['dataArr'] = $dataArr;
					// echo "<pre>";
					// var_dump($error_msg);
					// var_dump($this->_request->getParams());
					// echo "<pre>";
					// var_dump($paramdata);die;

					$accteopen1 = $this->_getParam('accteopen');


					//var_dump($accteopen1);
					if ($accteopen1 != '1') {
						$acct_no = $acSource;
						$benef_no = $acBenef;
						$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($remainsBalance);
						$TRA_MIN_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT);

					} else {
						$acct_no = $this->_getParam('ACCTSRC');
						$benef_no = $acSource;

						$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($remainsBalance);
						$TRA_MIN_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_MIN_AMOUNT);

					}
					// var_dump($sweepOption); die;
					if ($sweepOption == 2) {
						$maintainbalance =  $this->_getParam('maintainbalance');
						//if ($trfDateType == 2) {
							if(empty($maintainbalance)){
								$datetest = Application_Helper_General::convertDate($efDate,'yyyy-MM-dd','yyyy-MM-dd');
								$day = strtolower(date('D', strtotime($date)));
								$tagday = 'pooling_'.$day;
								$amountbalance = $this->_getParam($tagday);
								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($amountbalance);
							}else{
								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($maintainbalance);
							//}
						}
					}

					$paramPayment = array(	
											"PS_SUBJECT"				=> $paymentSubject,
											"CATEGORY"					=> 'OPEN SWEEP',
											"PS_EFDATE"					=> $efDate,
											"_dateFormat"				=> $this->_dateDisplayFormat,
											"_dateDBFormat"				=> $this->_dateDBFormat,
											
										 );
					
					if ($accteopen != '1') {
						$paramTrxArr = array();
						//var_dump($acct_no);
						foreach($acct_no  as  $keys => $valacct){
							
							if(empty($TRA_AMOUNT_num)){
								$TRA_AMOUNT_num = $valacct['REMAIN_BALANCE'];
							}
							$paramTrxArr[$keys] = array(
											"TRANSFER_TYPE" 			=> 'ONLINE',
											"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
											"TRA_MIN_AMOUNT" 			=> $TRA_MIN_AMOUNT_num,
											"TRA_MESSAGE" 				=> $message,
											// "TRA_REFNO" 				=> $TRA_REFNO,
											"ACCTSRC" 					=> $valacct['ACCT_NO'],
											"ACBENEF" 					=> $benef_no,
											// "ACBENEF_CCY" 				=> $ACBENEF_CCY,
											// "ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
											// "ACCTSRC_BANKCODE"			=> $ACCTSRC_BANKCODE,
										// for Beneficiary data, except (bene CCY and email), must be passed by reference
											"ACBENEF_BANKNAME" 			=> &$benefName,
											// "ReffId" 					=> &$ReffId,
											// "ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
												// "ACBENEF_CITIZENSHIP" 		=> &$ACBENEF_CITIZENSHIP,		// W:WNI, N: WNA
											// "ACBENEF_RESIDENT" 			=> &$ACBENEF_RESIDENT,			// 1/R: RESIDENT, 2/NR: NON-RESIDENT
											// "ACBENEF_ADDRESS1" 			=> &$ACBENEF_ADDRESS,
											// "ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
										//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
										//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

										//	"ORG_DIR" 					=> $ORG_DIR,
											"BANK_CODE" 				=> $acctBank,
										//	"BANK_NAME" 				=> $BANK_NAME,
										//	"BANK_BRANCH" 				=> $BANK_BRANCH,
										//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
										//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
										//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,

											// "LLD_IDENTICAL" 			=> $LLD_IDENTICAL,
										//	"LLD_CITIZENSHIP"			=> $LLD_CITIZENSHIP,
											// "LLD_CATEGORY" 				=> $LLD_CATEGORY,
											// "LLD_RELATIONSHIP" 			=> $LLD_RELATIONSHIP,
											// "LLD_PURPOSE" 				=> $LLD_PURPOSE,
											// "LLD_DESCRIPTION" 			=> $LLD_DESCRIPTION,
											// "LLD_BENEIDENTIF" 			=> $LLD_BENEIDENTIF,
											// "LLD_BENENUMBER" 			=> $LLD_BENENUMBER,
											// "LLD_SENDERIDENTIF" 		=> $LLD_SENDERIDENTIF,
											// "LLD_SENDERNUMBER" 			=> $LLD_SENDERNUMBER,
											// "CITY"						=> $CITY,
											// "CITY_CODE" 				=> $CITY_CODE, //yang akan dikirimkan ke vlink field 3
											"SWEEP_TYPE"				=> $sweepOption,
											'SWIFT_CODE'				=> $databank['0']['SWIFT_CODE'],
											'CLR_CODE'					=> $databank['0']['CLR_CODE'],
											"PS_EMAIL"					=> $email_notif,
											"PS_NOTIF"					=> $notif,
											"PS_SMS"					=> $sms_notif,
										 );
						}
						
					}else{
						 
						$paramTrxArr[0] = array(
											"TRANSFER_TYPE" 			=> 'ONLINE',
											"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
											"TRA_MIN_AMOUNT" 			=> $TRA_MIN_AMOUNT_num,
											"TRA_MESSAGE" 				=> $message,
											// "TRA_REFNO" 				=> $TRA_REFNO,
											"ACCTSRC" 					=> $acct_no,
											"ACBENEF" 					=> $benef_no,
											// "ACBENEF_CCY" 				=> $ACBENEF_CCY,
											// "ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
											// "ACCTSRC_BANKCODE"			=> $ACCTSRC_BANKCODE,
										// for Beneficiary data, except (bene CCY and email), must be passed by reference
											"ACBENEF_BANKNAME" 			=> &$benefName,
											// "ReffId" 					=> &$ReffId,
											// "ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
												// "ACBENEF_CITIZENSHIP" 		=> &$ACBENEF_CITIZENSHIP,		// W:WNI, N: WNA
											// "ACBENEF_RESIDENT" 			=> &$ACBENEF_RESIDENT,			// 1/R: RESIDENT, 2/NR: NON-RESIDENT
											// "ACBENEF_ADDRESS1" 			=> &$ACBENEF_ADDRESS,
											// "ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
										//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
										//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

										//	"ORG_DIR" 					=> $ORG_DIR,
											"BANK_CODE" 				=> $acctBank,
										//	"BANK_NAME" 				=> $BANK_NAME,
										//	"BANK_BRANCH" 				=> $BANK_BRANCH,
										//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
										//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
										//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,

											// "LLD_IDENTICAL" 			=> $LLD_IDENTICAL,
										//	"LLD_CITIZENSHIP"			=> $LLD_CITIZENSHIP,
											// "LLD_CATEGORY" 				=> $LLD_CATEGORY,
											// "LLD_RELATIONSHIP" 			=> $LLD_RELATIONSHIP,
											// "LLD_PURPOSE" 				=> $LLD_PURPOSE,
											// "LLD_DESCRIPTION" 			=> $LLD_DESCRIPTION,
											// "LLD_BENEIDENTIF" 			=> $LLD_BENEIDENTIF,
											// "LLD_BENENUMBER" 			=> $LLD_BENENUMBER,
											// "LLD_SENDERIDENTIF" 		=> $LLD_SENDERIDENTIF,
											// "LLD_SENDERNUMBER" 			=> $LLD_SENDERNUMBER,
											// "CITY"						=> $CITY,
											// "CITY_CODE" 				=> $CITY_CODE, //yang akan dikirimkan ke vlink field 3
											"SWEEP_TYPE"				=> $sweepOption,
											'SWIFT_CODE'				=> $databank['0']['SWIFT_CODE'],
											'CLR_CODE'					=> $databank['0']['CLR_CODE'],
											"PS_EMAIL"					=> $email_notif,
											"PS_NOTIF"					=> $notif,
											"PS_SMS"					=> $sms_notif,
										 );
						
					}
					
					

					
					$validate   = new ValidatePaymentOpen($this->_custIdLogin, $this->_userIdLogin);

					$resWs = array();
					// $paramTrxArr = array();
					
					//var_dump($datetest);
					 //echo '<pre>';
					 //var_dump($paramPayment);
					 //var_dump($paramTrxArr);die(); 
					$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);

					if ($resultVal == false) {

						$errorMsg1 		= $validate->getErrorMsg();	
						//var_dump($errorMsg1);die;

						$this->view->error = true;
						$confirmPage = false;
						$error_msg[] = $errorMsg1;

					}else{
						$sessionNamespace = new Zend_Session_Namespace('confirmSweep');
						$sessionNamespace->content = $paramdata;

						$toConfirm = true;
					}

					
				}
	

					$paymentReff = $this->_getParam('teamplate_reff');
					$paymentSubject = $this->_getParam('paymentsubject');
					$sourceName = $this->_getParam('sourceName');
					$benefName = $this->_getParam('benefName');
					$message = $this->_getParam('TRA_MESSAGE');
					$sweepOption = $this->_getParam('ta_sel');
					$trfDateType = $this->_getParam('tranferdatetype');
					if ($trfDateType == '2') {
						$trfTime = $this->_getParam('PS_EFTIMEF');
						$this->view->PS_EFTIMEF = $trfTime;
					} else {
						$trfTime = $this->_getParam('PS_EFTIMEP');
						$this->view->PS_EFTIMEP = $trfTime;
					}

					$remainBalanceType = $this->_getParam('rem_per');

					$ACCT_BANK = $this->_getParam('ACCT_BANK');
					$ACCTSRC = $this->_getParam('ACCTSRC');
					$ACBENEF = $this->_getParam('ACBENEF');
					$FILE_ID = $this->_getParam('FILE_ID');
					$TRA_MESSAGE = $this->_getParam('TRA_MESSAGE');
					$PS_EFDATEFUTURE = $this->_getParam('PS_FUTUREDATE');


					$PS_EFDATE = $this->_getParam('PS_EFDATE');

					$repetition = $this->_getParam('repetition');
					if (!empty($repetition)) {
						$repeatstr = 'repetition' . $repetition;
						// var_dump($repeatstr);
						$this->view->repetition = $repetition;
						$this->view->$repeatstr = 'selected';
					}

					$PS_ENDDATEPERIODIC = $this->_getParam('PS_ENDDATEPERIODIC');

					$selectrepeat = $this->_getParam('selectrepeat');
					if (!empty($selectrepeat)) {
						$repeat = 'repeat' . $selectrepeat;
						$this->view->$repeat = 'selected';
					}



					$this->view->PS_EFDATE = $PS_EFDATE;
					$this->view->endDatePeriodic = $PS_ENDDATEPERIODIC;

					$report_day = $this->_getParam('report_day');

					if (!empty($report_day)) {
						foreach ($report_day as $key => $value) {
							$this->view->{'check' . $value} = 'checked';
						}
					}


					// var_dump($PS_EFDATEFUTURE);die;
					// var_dump($this->_getParam('paymentsubject'));
					// var_dump($paymentsubject);die;
					$this->view->FILE_ID = $FILE_ID;
					$this->view->ACCTSRC = $ACCTSRC;
					$this->view->ACBENEF = $ACBENEF;
					$this->view->TRA_MESSAGE = $TRA_MESSAGE;
					$this->view->PS_EFDATEFUTURE = $PS_EFDATEFUTURE;

					$aliassource = '';
					$aliasbenef = '';
					foreach ($acct as $key => $value) {
						if ($value['ACCT_NO'] == $sourceName) {
							$aliassource = $value['ACCT_ALIAS'];
						}

						if ($value['ACCT_NO'] == $benefName) {
							$aliasbenef = $value['ACCT_ALIAS'];
						}
					}
					$bank_name = '';
					foreach ($databank as $key => $value) {
						if ($value['BANK_CODE'] == $ACCT_BANK) {
							$bank_name = $value['BANK_NAME'];
						}
					}
					// var_dump($trfDateType);
					// $this->view->remainbalance = 
					$this->view->paymentReff = $paymentReff;
					$this->view->paymentsubject = $this->_getParam('paymentsubject');
					$this->view->acSource = $acSource;
					$this->view->sourceName = $sourceName;
					$this->view->acBenef = $acBenef;
					$this->view->benefName = $benefName;
					$this->view->message = $message;
					$this->view->sweepOption = $sweepOption;
					$this->view->TrfDateType = $trfDateType;
					$this->view->sourceType = $sourceType;
					$this->view->ACCT_BANK = $ACCT_BANK;

					$remainsBalance = $this->_getParam('remainbalance');
					$maintainbalance = $this->_getParam('maintainbalance');
					//remains balance
					$this->view->remainBalanceType = $remainBalanceType;
					// var_dump($remainsBalance);die;
					$this->view->remainsBalance = $remainsBalance;
					$this->view->maintainbalance = $maintainbalance;

					//maintain balance
					$this->view->pooling_sun = $pooling_sun;
					$this->view->pooling_mon = $pooling_mon;
					$this->view->pooling_tue = $pooling_tue;
					$this->view->pooling_wed = $pooling_wed;
					$this->view->pooling_thu = $pooling_thu;
					$this->view->pooling_fry = $pooling_fry;
					$this->view->pooling_sat = $pooling_sat;

					$this->view->pooling_sun_view = $this->moneyAliasFormatter($pooling_sun);
					$this->view->pooling_mon_view = $this->moneyAliasFormatter($pooling_mon);
					$this->view->pooling_tue_view = $this->moneyAliasFormatter($pooling_tue);
					$this->view->pooling_wed_view = $this->moneyAliasFormatter($pooling_wed);
					$this->view->pooling_thu_view = $this->moneyAliasFormatter($pooling_thu);
					$this->view->pooling_fry_view = $this->moneyAliasFormatter($pooling_fry);
					$this->view->pooling_sat_view = $this->moneyAliasFormatter($pooling_sat);

					//trf date type
					//future date
					$this->view->efDate = $efDate;
					//periodic
					$this->view->repetition = $repetition;
					$this->view->endDate = $endDate;
					$this->view->repeatEvery = $repeatEvery;
					$this->view->repeatOn = $repeatOn;
					$this->view->endDate = $endDate;

					//notif
					$this->view->notif = $notif;
					$this->view->email_notif = $email_notif;
					$this->view->sms_notif = $sms_notif;

					$this->view->session = $session;
					
					
					$this->view->ACCTSRC = $this->_getParam('ACCTSRC');
					$this->view->ACCTSRCTEXT = $this->_getParam('ACCTSRCTEXT');
					$this->view->ACCTSRC_BANKCODE = $this->_getParam('sourceBankCode');
					$this->view->ACCT_BANK = $this->_getParam('ACCT_BANK');
					$this->view->ACBENEF_ALIAS = $this->_getParam('ACBENEF_ALIAS');
					$this->view->ACBENEF_EMAIL = $this->_getParam('ACBENEF_EMAIL');
					
					
					
					
					
					

					$this->view->TrfDateType 		= (isset($trfDateType))			? $trfDateType			: '2';

					$paramBack = array(
						'ACCTSRCTEXT' => $this->_getParam('ACCTSRCTEXT'),
						'sourceBankCode' => $this->_getParam('sourceBankCode'),
						'CLR_CODE' => $this->_getParam('CLR_CODE'),
						'SWIFT_CODE' => $this->_getParam('SWIFT_CODE'),
						'ACBENEF_BANKNAME' => $this->_getParam('ACBENEF_BANKNAME'),
						'ACBENEF_ALIAS' => $this->_getParam('ACBENEF_ALIAS'),
						'ACBENEF_EMAIL' => $this->_getParam('ACBENEF_EMAIL'),
					
						'paymentReff' => $paymentReff,
						'ACBENEF_EMAIL' => $ACBENEF_EMAIL,
						'ACBENEF_PLAIN'	=> $acBenef,
						'paymentSubject' => $paymentSubject, 
						'sourceName'	=> $sourceName,
						'benefName'	=> $benefName,
						'message'	=> $message,
						'sweepOption'	=> $sweepOption,
						'sourceType'	=> $sourceType,
						'trfDateType'	=> $trfDateType,
						'trfTime'	=> $trfTime,
						'remainBalanceType'	=> $remainBalanceType,
						'ACCT_BANK'	=> $ACCT_BANK,
						'ACCTSRC'	=> $ACCTSRC,
						'ACBENEF'	=> $ACBENEF,
						'FILE_ID'	=> $FILE_ID,
						'TRA_MESSAGE'	=> $TRA_MESSAGE,
						'PS_EFDATE'	=> $PS_EFDATE,
						'PS_EFDATEFUTURE'	=> $PS_EFDATEFUTURE,
						'repetition'	=> $repetition,
						'PS_ENDDATEPERIODIC'	=> $PS_ENDDATEPERIODIC,
						'selectrepeat'	=> $selectrepeat,
						'report_day'	=> $report_day,
						'FILE_ID'	=> $FILE_ID,
						'remainsBalance'	=> $remainsBalance,
						'maintainbalance'	=> $maintainbalance,
						'pooling_sun'	=> $pooling_sun,
						'pooling_mon'	=> $pooling_mon,
						'pooling_tue'	=> $pooling_tue,
						'pooling_wed'	=> $pooling_wed,
						'pooling_thu'	=> $pooling_thu,
						'pooling_fry'	=> $pooling_fry,
						'pooling_sat'	=> $pooling_sat,
						'efDate'	=> $efDate,
						'endDate'	=> $endDate,
						'repeatEvery'	=> $repeatEvery,
						'repeatOn'	=> $repeatOn,
						'notif'			=> $notif,
						'email_notif'	=> $email_notif,
						'sms_notif'		=> $sms_notif,
						'TRA_MIN_AMOUNT' => $TRA_MIN_AMOUNT,
						'session'		=> $session
					);
					
					
					
					$this->view->BANK_NAME = $paramBack['BANK_NAME'];
					$this->view->BANK_CODE = $paramBack['BANK_CODE'];
					$this->view->CLR_CODE = $paramBack['CLR_CODE'];
					$this->view->SWIFT_CODE = $paramBack['SWIFT_CODE'];
					$this->view->ACBENEF_BANKNAME = $paramBack['ACBENEF_BANKNAME'];
					$this->view->ACBENEF_ALIAS = $paramBack['ACBENEF_ALIAS'];
					$this->view->ACBENEF_EMAIL = $paramBack['ACBENEF_EMAIL'];
					
					$sessionNamespace->paramBack = $paramBack;
				}
				
				// echo '<pre>';
				// var_dump($paramBack);
			
				$this->view->error_msg = $error_msg;

				//page indicator
				$this->view->confirmPage = $confirmPage;

				if ($toConfirm) {
					$this->_redirect('/opensweep/samebank/confirm');
				}
			//if submit di confirmpage
		}

		$sessionNamespace = new Zend_Session_Namespace('confirmSweep');
		$paramBack = $sessionNamespace->paramBack;
		
		$back = $sessionNamespace->back;

		if ($back) {
			
			if ($paramBack['trfDateType'] == '2') {
				$this->view->PS_EFTIMEF = $paramBack['trfTime'];
			} else {
				$this->view->PS_EFTIMEP = $paramBack['trfTime'];
			}

			if (!empty($paramBack['repetition'])) {
				$repeatstr = 'repetition' . $paramBack['repetition'];
				// var_dump($repeatstr);
				$this->view->repetition = $paramBack['repetition'];
				$this->view->$repeatstr = 'selected';
			}

			if (!empty($paramBack['selectrepeat'])) {
				$repeat = 'repeat' . $paramBack['selectrepeat'];
				$this->view->$repeat = 'selected';
			}

			$this->view->PS_EFDATE = $paramBack['PS_EFDATE'];
			$this->view->endDatePeriodic =  $paramBack['PS_ENDDATEPERIODIC'];

			if (!empty($paramBack['report_day'])) {
				foreach ($paramBack['report_day'] as $key => $value) {
					$this->view->{'check' . $value} = 'checked';
				}
			}

			$this->view->FILE_ID = $paramBack['FILE_ID'];
			$this->view->ACCTSRC = $paramBack['ACCTSRC'];
			$this->view->ACBENEF = $paramBack['ACBENEF'];
			$this->view->TRA_MESSAGE = $paramBack['TRA_MESSAGE'];
			$this->view->PS_EFDATEFUTURE = $paramBack['PS_EFDATEFUTURE'];

			$aliassource = '';
			$aliasbenef = '';
			foreach ($acct as $key => $value) {
				if ($value['ACCT_NO'] == $sourceName) {
					$aliassource = $value['ACCT_ALIAS'];
				}

				if ($value['ACCT_NO'] == $benefName) {
					$aliasbenef = $value['ACCT_ALIAS'];
				}
			}
			$bank_name = '';
			foreach ($databank as $key => $value) {
				if ($value['BANK_CODE'] == $ACCT_BANK) {
					$bank_name = $value['BANK_NAME'];
				}
			}
			
			
			
			
					
			$this->view->BANK_NAME = $paramBack['BANK_NAME'];
			$this->view->BANK_CODE = $paramBack['BANK_CODE'];
			$this->view->CLR_CODE = $paramBack['CLR_CODE'];
			$this->view->SWIFT_CODE = $paramBack['SWIFT_CODE'];
			$this->view->ACBENEF_BANKNAME = $paramBack['ACBENEF_BANKNAME'];
			$this->view->ACBENEF_ALIAS = $paramBack['ACBENEF_ALIAS'];
			$this->view->ACBENEF_EMAIL = $paramBack['ACBENEF_EMAIL'];
			
			// var_dump($trfDateType);
			// $this->view->remainbalance = 
			$this->view->paymentReff = $paramBack['paymentReff'];
			$this->view->paymentsubject = $paramBack['paymentSubject'];
			$this->view->paymentsubject = $paramBack['paymentSubject'];
			$this->view->acSource = $paramBack['ACCTSRC'];
			$this->view->sourceName = $paramBack['sourceName'];
			$this->view->acBenef = $paramBack['ACBENEF'];
			$this->view->benefName = $paramBack['benefName'];
			$this->view->message = $paramBack['message'];
			$this->view->sweepOption = $paramBack['sweepOption'];
			$this->view->TrfDateType = $paramBack['trfDateType'];
			$this->view->sourceType = $paramBack['sourceType'];
			$this->view->ACCT_BANK = $paramBack['ACCT_BANK'];
			$this->view->TRA_MIN_AMOUNT = $paramBack['TRA_MIN_AMOUNT'];
			$this->view->ACBENEF_EMAIL = $paramBack['ACBENEF_EMAIL'];
			$this->view->ACBENEF_PLAIN = $paramBack['ACBENEF_PLAIN'];
			
			


			//remains balance
			$this->view->remainBalanceType = $paramBack['remainBalanceType'];
			// var_dump($remainsBalance);die;
			$this->view->remainsBalance = $paramBack['remainsBalance'];
			$this->view->maintainbalance = $paramBack['maintainbalance'];

			//maintain balance
			$this->view->pooling_sun = $paramBack['pooling_sun'];
			$this->view->pooling_mon = $paramBack['pooling_mon'];
			$this->view->pooling_tue = $paramBack['pooling_tue'];
			$this->view->pooling_wed = $paramBack['pooling_wed'];
			$this->view->pooling_thu = $paramBack['pooling_thu'];
			$this->view->pooling_fry = $paramBack['pooling_fry'];
			$this->view->pooling_sat = $paramBack['pooling_sat'];

			$this->view->pooling_sun_view = $this->moneyAliasFormatter($paramBack['pooling_sun']);
			$this->view->pooling_mon_view = $this->moneyAliasFormatter($paramBack['pooling_mon']);
			$this->view->pooling_tue_view = $this->moneyAliasFormatter($paramBack['pooling_tue']);
			$this->view->pooling_wed_view = $this->moneyAliasFormatter($paramBack['pooling_wed']);
			$this->view->pooling_thu_view = $this->moneyAliasFormatter($paramBack['pooling_thu']);
			$this->view->pooling_fry_view = $this->moneyAliasFormatter($paramBack['pooling_fry']);
			$this->view->pooling_sat_view = $this->moneyAliasFormatter($paramBack['pooling_sat']);

			//trf date type
			//future date
			$this->view->efDate = $paramBack['efDate'];
			//periodic
			$this->view->repetition = $paramBack['repetition'];
			$this->view->endDate = $paramBack['endDate'];
			$this->view->repeatEvery = $paramBack['repeatEvery'];
			$this->view->repeatOn = $paramBack['repeatOn'];

			//notif
			$this->view->notif = $paramBack['notif'];
			$this->view->email_notif = $paramBack['email_notif'];
			$this->view->sms_notif = $paramBack['sms_notif'];
			$this->view->session = $paramBack['session'];

			$sessionNamespace->back = false;
		}
		else{
			unset($_SESSION['confirmSweep']);
		}
				 
		//Recrate Ongoing
		if ($this->_request->getParam('recreate')) {
			$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
			$filter       = new Application_Filtering();
			$AESMYSQL = new Crypt_AESMYSQL();
			$password = $sessionNamespace->token;
			$PS_NUMBER = urldecode($filter->filter($this->_getParam('recreate'), "recrate"));
			$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

			$recreate  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'))
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER')
				->where('P.PS_NUMBER = ?', $PS_NUMBER);
			$recreate = $this->_db->fetchRow($recreate);

			$this->view->paymentsubject = $recreate['PS_SUBJECT'];
			$this->view->ACCT_BANK = $recreate['BENEF_ACCT_BANK_CODE'];
			$this->view->ACBENEF = $recreate['BENEFICIARY_ACCOUNT'];
			$this->view->ACCTSRC = $recreate['SOURCE_ACCOUNT'];
			$this->view->TRA_MESSAGE = $recreate['TRA_MESSAGE'];
			
		}
	}
	
	
	public function banksuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

		$banklist = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_APIKEY'), array('A.BANK_CODE'))
				// ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
				//  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
				//  // ->join(array('D' => 'M_DOMESTIC_BANK_TABLE'),'D.BANK_NAME = B.BANK_NAME',array('D.SWIFT_CODE'))
				->where('A.CUST_ID = ?', $this->_custIdLogin)
				->group('A.BANK_CODE')
			// ->where("A.BANK_CODE IN ('008','014','013','009','032','031','153','002') ")
			// ->order('A.APIKEY_ID ASC')
		);
		// var_dump($banklist);
		$par = array();
		if (!empty($banklist)) {
			foreach ($banklist as $key => $value) {
				$par[] = $value['BANK_CODE'];
			}
		}
		// var_dump($par);
		// var_dump($banklist);die;
		if (empty($par)) {
			$par = array('000');
		}

        $banklist = $this->_db->fetchAll(
			$this->_db->select()
				 ->from(array('A' => 'M_BANK_TABLE'), array('*'))
				 ->where('A.BANK_CODE IN (?)', $par)
		);

	    $bankdata = array();
	    if (!empty($banklist)) {
	    	foreach ($banklist as $key => $value) {
				$bankdata[$key]['swiftcode'] = $value['SWIFT_CODE'];
				$bankdata[$key]['clrcode'] = $value['CLR_CODE'];
				$bankdata[$key]['value'] = $value['BANK_NAME'];
				$bankdata[$key]['bankcode'] = $value['BANK_CODE'];
	    	}
	    }

        $searchTerm = $_GET['term'];

        $bankDataList = array();
        foreach ($bankdata as $key => $value) {

        	if (strpos(strtolower($value['value']), strtolower($searchTerm)) !== false) {
		        $data['swiftcode'] = $value['swiftcode'];
        		$data['clrcode']    = $value['clrcode'];
        		$data['value']    = $value['value'];
        		$data['bankcode']    = $value['bankcode'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <span>'.$value['value'].'</span>
		        </a>';
		        array_push($bankDataList, $data);
        	}
        }

        echo json_encode($bankDataList);

	}

	public function viewhistoryAction(){

		$this->_helper->layout()->setLayout('newpopup');
		// $filter 		= new Application_Filtering();
		// $cust_id 		= $filter->filter($this->_getParam('custid'), "F_CUST_ID");
		$cust_id = $this->_custIdLogin;
		
		$selecthistory = $this->_db->select()
			->from(array('T_FORECAST'), array('*'))
			->where('F_CUST_ID = ?', $cust_id)
			->query()->fetchall();
		$datahistory = array();
		foreach ($selecthistory as $key => $value) {
				$datahistory[$key]['title'] = 'IDR '.Application_Helper_General::displayMoney($value['F_AMOUNT']);
				$datahistory[$key]['start'] = $value['F_DATE'];
				$datahistory[$key]['color'] = '#007bff';
		}

		$this->view->hdata = json_encode($datahistory);

		foreach($selecthistory as $row) {
			$dataDate[] = $row['F_DATE'];
			$dataAmount[] = $row['F_AMOUNT'];
		}

		$history = array_combine($dataDate, $dataAmount);

		$this->view->historyCal = $history;
		$this->view->dHistory = $selecthistory;

		$selectholiday = $this->_db->select()
			->from(array('M_HOLIDAY'), array('*')) 
			->query()->fetchall();
		$dataholiday = array();
		foreach ($selectholiday as $key => $value) {
			$dataholiday[$key]['start'] = $value['HOLIDAY_DATE'];
			$dataholiday[$key]['display'] = 'background';
		}

		$this->view->holidayData = json_encode($dataholiday);
		
		$linear = $this->_getParam('linear');

		$this->view->linear = $linear;

		$startd = $this->_getParam('drange');
		$this->view->startd = $startd;

		
	} 

	public function rangeforecastAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		

	}
}
