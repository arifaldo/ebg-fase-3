<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
class useropenlimit_SuggestiondetailController extends Application_Main
{

  	public function indexAction()
  	{
  		$this->_helper->layout()->setLayout('popup');
  		$params = $this->_request->getParams();
		
		$fields = array(
					'bank'   => array('field'    => 'BANK_NAME',
                                        'label'    => $this->language->_('Bank'),
                                        'sortable' => false),
					'acct'   => array('field'    => 'A.ACCT_NO',
                                        'label'    => $this->language->_('Account No'),
                                        'sortable' => false),
    
                    'acct_name'      => array('field'    => 'ACCT_NAME',
                                        'label'    => $this->language->_('Account Name'),
                                        'sortable' => false),
    
                    'bodyno' => array('field'    => 'CCY_ID',
                                        'label'    => $this->language->_('Currency'),
                                        'sortable' => false),
    
                    'sys_balance'   => array('field'    => 'MAXLIMIT',
                                        'label'    => $this->language->_('Limit'),
                                        'sortable' => false),
                   );
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby','A.ACCT_NO');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir','ASC');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
  		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;


		
  		if(array_key_exists('changes_id', $params))
  		{
  			// $filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			// $validators = array(
  			// 	'changes_id' => array(
  			// 		'NotEmpty',
  			// 		'Digits',
					// 'messages' => array(
					// 	'No Suggestion ID',
					// 	'Wrong ID Format',
					// ),
  			// 	),
  			// );
		
  			// $zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		//if($zf_filter_input->isValid())
	  		//{
				//die;
				 $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
				   $password = $sessionNamespace->token; 
				   $this->view->token = $sessionNamespace->token;  


				$AESMYSQL = new Crypt_AESMYSQL();
				$PS_NUMBER      = urldecode($this->_getParam('changes_id'));
				$changeId = $AESMYSQL->decrypt($PS_NUMBER, $password);  

				// $zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
				// if($zf_filter_input->isValid())
				// {
					$cust_id = $user_id = $tempData = $masterData = null;
				
				$changeInfo = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('A' => 'T_GLOBAL_CHANGES'))
						->where('CHANGES_ID = ?', $changeId)
						->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'")
					);
					
				if(!$changeInfo)
				{
					$this->_redirect('/notification/invalid/index');
				}
				
				$cust_id = $changeInfo['COMPANY_CODE'];
				$cust_name = $changeInfo['COMPANY_NAME'];
				$user_id = $changeInfo['KEY_FIELD'];
				
	  			$tempData = $this->_db->fetchAll(
	  				$this->_db->select()
						->from(array('A' => 'TEMP_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
						->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						//->joinLeft(array('B' => 'M_CUSTOMER_ACCT'),'A.ACCT_NO = B.ACCT_NO',array('CCY_ID','ACCT_NAME'))
						->joinLeft(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('CUST_NAME'))
						->where('CHANGES_ID = ?', $changeId)
						->where('A.MAXLIMIT_OPEN = 1')
						->where('A.MAXLIMIT > 0')
						->order($sortBy.' '.$sortDir)
	  			);
				//echo $tempData;die;
				//var_dump($tempData);die;
				// if(isset($tempData[0]['CUST_ID']) && isset($tempData[0]['USER_LOGIN']))
				// {
					// $cust_id = $tempData[0]['CUST_ID'];
					// $user_id = $tempData[0]['USER_LOGIN'];
				// }
				
	  			if($cust_id && $user_id)
				{
	  				$keyValue = $user_id;

					$masterData = $this->_db->fetchAll(
						$this->_db->select()
							->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
							->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
							//->joinLeft(array('B' => 'M_CUSTOMER_ACCT'),'A.ACCT_NO = B.ACCT_NO',array('CCY_ID','ACCT_NAME'))
							->where('A.CUST_ID = ?', $cust_id)
							->where('A.USER_LOGIN = ?', $user_id)
							->where('A.MAXLIMIT > 0')
							->where('A.MAXLIMIT_OPEN = 1')
							->where('A.MAKERLIMIT_STATUS <> 3')
						//	->where('B.ACCT_STATUS <> 3')
							->order($sortBy.' '.$sortDir)
					);
				}

	  			$this->view->suggested_by = $changeInfo['CREATED_BY'];
	  			$this->view->suggestion_date = $changeInfo['CREATED'];
	  			$this->view->temp_data = $tempData;
	  			$this->view->master_data = $masterData;
	  			$this->view->changes_id = $changeId;
	  			$this->view->cust_id = $cust_id;
	  			$this->view->cust_name = $cust_name;
	  			$this->view->user_id = $user_id;
				$this->view->fields = $fields;
	  	//	}
	  	//	else
	  	//	{
	  			// $errors = $zf_filter_input->getMessages();
	  			// $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  			// $this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
      	//		$this->_redirect('/popuperror');
	  	//	}
  		}
  		else
  		{
  			// $errorRemark = 'No Suggestion ID';

	  		// $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  		// $this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
      	//	$this->_redirect('/popuperror');
    	}
    	if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('MLCL','Viewing Suggestion Detail User Limit Cust ID : ( '.$cust_id.' ) User ID ( '.$user_id.' )');
    	}
	}
}