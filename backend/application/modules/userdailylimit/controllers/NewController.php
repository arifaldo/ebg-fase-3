<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class userdailylimit_NewController extends userdailylimit_Model_Userdailylimit
{

  public function indexAction()
  { 
    $this->_helper->layout()->setLayout('newlayout');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_login'), $password));
    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $user_id : null;

    $ccy_id = strtoupper($AESMYSQL->decrypt($this->_getParam('ccy_id'), $password));
    $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
    $error_remark = null;


    //convert dailylimit agar bisa masuk ke database
    $dailyLimit = $this->_getParam('dailylimit');
    $dailyLimit = Application_Helper_General::convertDisplayMoney($dailyLimit);
    $this->_setParam('dailylimit',$dailyLimit);
    //END convert dailylimit agar bisa masuk ke database
 

    $this->view->userDailyLimit_msg = array();


    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_LIMIT_USD','CUST_LIMIT_IDR'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('CUST_STATUS!=3');
      $result = $this->_db->fetchRow($select);
      if(!$result)$cust_id = null;
      else{
        $companyLim = array();
        $companyLim['IDR'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_IDR']);
        $companyLim['USD'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_USD']);
      }
    }

    if(!$cust_id)
    {
      $error_remark = $this->language->_('customer id is not found');
      //insert log
      try
      {
        $this->_db->beginTransaction();

        if($this->_request->isPost())
        {
          $action   = strtoupper($this->_changeType['code']['new']);
           $fulldesc = Application_Helper_General::displayFullDesc($_POST);
        }
        else
        {
          $action = strtoupper($this->_actionID['view']); $fulldesc = null;
        }

        $this->backendLog($action,strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
        $this->_db->commit();
      }
      catch(Exception $e)
      {
        $this->_db->rollBack();
        SGO_Helper_GeneralLog::technicalLog($e);
      }

      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }


     $userData = $this->_db->select()
                              ->from(array('A' => 'M_USER'),array('USER_ID'))
 							  //->join(array('C' => 'M_DAILYLIMIT'), 'A.USER_ID = C.USER_LOGIN AND A.CUST_ID = C.CUST_ID', array())			   							  
                              ->where('UPPER(A.CUST_ID)='.$this->_db->quote((string)$cust_id))
                              ->where('USER_STATUS!=3')
							  ->order('A.USER_ID ASC')
                              ->query()->fetchAll();
     

     $tempuserData = $this->_db->select()
                              ->from('TEMP_DAILYLIMIT',array('USER_LOGIN'))
                              ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             // ->where('USER_STATUS!=3');
                              ->query()->fetchAll();
							  
	$makeruserData = $this->_db->select()
                              ->from('M_DAILYLIMIT',array('USER_LOGIN'))
                              ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                              ->where('DAILYLIMIT_STATUS=1')
                              ->query()->fetchAll();
	
	
        //echo $tempuserData;die;      
                
      foreach ($userData as $key => $value) {

          foreach ($tempuserData as $keytemp => $valuetemp) {
             if($value['USER_ID'] == $valuetemp['USER_LOGIN']){
      //   die('gsa');
                unset($userData[$key]);
             }
          }
		  foreach ($makeruserData as $keytemp => $valuetemp) {
             if($value['USER_ID'] == $valuetemp['USER_LOGIN']){
      //   die('gsa');
                unset($userData[$key]);
             }
          }
        # code...
      }
  
  $this->view->userData = $userData;

     $getCcy = $this->getCcyList();
     $this->view->CCYData  = $getCcy;


    if($this->_request->isPost())
    {
      $sumArr = array();

      foreach($getCcy as $val){
        $tempCCY = $val['CCY_ID'];
        $selectSum = $this->_db->select()
                              ->from('M_DAILYLIMIT', new Zend_Db_Expr('SUM(DAILYLIMIT)'))
                              ->where('UPPER(CUST_ID)= ?', $cust_id)
                              ->where('DAILYLIMIT_STATUS != ?', '3')
                              ->where('CCY_ID = ?', $tempCCY);
        $sumArr[$val['CCY_ID']] = $this->_db->fetchOne($selectSum);
      }

      $ccy_id = $this->_getParam('ccy_id');

       $exclude_fgroup_id = '(UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id);
       $exclude_fgroup_id .= ' OR UPPER(CUST_ID)='.$this->_db->quote('BANK');
       $exclude_fgroup_id .= ') AND UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active']));

       $filters = array('user_login'  => array('StripTags','StringTrim','StringToUpper'),
                        'ccy_id'      => array('StripTags','StringTrim','StringToUpper'),
                        'dailylimit'  => array('StripTags','StringTrim'),
                        );

     $validators =  array('cust_id'     => array('NotEmpty',
                            'messages' => array($this->language->_('Can not be empty'))
                            ),

                         'user_login'  => array('NotEmpty',
                                                  //array('Db_NoRecordExists',array('table'=>'M_DAILYLIMIT','field'=>'USER_LOGIN','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' and UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id.' and DAILYLIMIT_STATUS = "1"'))),
                                                  //array('Db_NoRecordExists',array('table'=>'TEMP_DAILYLIMIT','field'=>'USER_LOGIN','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' and UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id))),
                                                  'messages' => array($this->language->_('Can not be empty'),
                                                                      //$this->language->_('This User id and Currency combination already exists'),
                                                                      //$this->language->_('This User id and Currency combination already suggested')
                                                                      )
                                                    ),

                           'ccy_id'        => array('NotEmpty',
                              'messages' => array($this->language->_('Can not be empty'))
                              ),

               'dailylimit'    => array('NotEmpty',
                              'Float',
                              array('GreaterThan',0),
                              array('StringLength',array('min'=>1,'max'=>16)), //batas = 13 digit,16
                              'messages' => array($this->language->_('Can not be empty'),
                                                $this->language->_('Invalid Maximum Amount Format'),
                                                  $this->language->_('Maximum Amount must be greater than zero'),
                                        $this->language->_('Too many significant digits.Maximum digit allowed : 13 digit(s)'),
                                                 )
                              ),
                         );


    $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

      if($zf_filter_input->isValid())
      {
        $checkMaster = $this->_db->select()
                              ->from(array('M_DAILYLIMIT'), array('USER_LOGIN'))
                              ->where('USER_LOGIN = ?', $zf_filter_input->user_login)
                              ->where('CUST_ID = ?', $cust_id)
                              ->where('CCY_ID = ?', $zf_filter_input->ccy_id)
                              ->where('DAILYLIMIT_STATUS = ?', '1');
                              //echo $checkMaster;
        $uniqueMaster = $this->_db->fetchOne($checkMaster);


        $checkTemp = $this->_db->select()
                              ->from(array('TEMP_DAILYLIMIT'), array('USER_LOGIN'))
                              ->where('USER_LOGIN = ?', $zf_filter_input->user_login)
                              ->where('CUST_ID = ?', $cust_id)
                              ->where('CCY_ID = ?', $zf_filter_input->ccy_id);
                              // echo $checkTemp;
        $uniqueTemp = $this->_db->fetchOne($checkTemp);

        // var_dump($uniqueMaster);
        // var_dump($uniqueTemp);
        if(empty($uniqueMaster) && empty($uniqueTemp)){
          // die('her');
          $ccy = $zf_filter_input->ccy_id;
          $dailylim = $zf_filter_input->dailylimit;
          $dailylimLeft = $companyLim[$ccy] ;
          // - Application_Helper_General::convertDisplayMoney($sumArr[$ccy]);

          $errorLim = false;

          if($dailylim > $dailylimLeft){


            $errorLim = true;
            $error_remark = "Maximum Amount of Daily Limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);
          }
          else{

            $user_info = $this->_db->select()
                                       ->from('M_USER',array('USER_FULLNAME'))
                                       ->where('UPPER(USER_ID)='.$this->_db->quote((string)$zf_filter_input->user_login))
                                       ->query()->fetch();

            $user_fullname = $user_info['USER_FULLNAME'];

            $info = 'Daily Limit = '.$zf_filter_input->dailylimit;
            $dailyLimitData = $this->_dailyLimitData;

            foreach($validators as $key=>$value)
            {
              if($zf_filter_input->$key) $dailyLimitData[strtoupper($key)] = $zf_filter_input->$key;
            }

            /*if($zf_filter_input->token_serialno)
            {
               $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['yes']);
            }
            else
            {
               $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['no']);
            }*/


            $dailyLimitData['CUST_ID'] = $cust_id;
            $dailyLimitData['DAILYLIMIT_STATUS'] = 1;

            $dailyLimitData['UPDATED']     = null;
            $dailyLimitData['UPDATEDBY']   = null;
            $dailyLimitData['CREATED']     = new Zend_Db_Expr('now()');
            $dailyLimitData['CREATEDBY']   = $this->_userIdLogin;

            $dailyLimitData['SUGGESTED']    = new Zend_Db_Expr('now()');
            $dailyLimitData['SUGGESTEDBY']  = $this->_userIdLogin;

            try
            {
              $this->_db->beginTransaction();
              $change_id = $this->suggestionWaitingApproval('User Daily Limit',$info,$this->_changeType['code']['new'],null,'M_DAILYLIMIT','TEMP_DAILYLIMIT',$zf_filter_input->user_login,$user_fullname,$cust_id);

              $this->insertTempDailyLimit($change_id,$dailyLimitData);

              //log CRUD
              Application_Helper_General::writeLog('DLAD','User Daily Limit has been Added, User Id : '.$zf_filter_input->user_login. ' CCY : '.$zf_filter_input->ccy_id.' Change id : '.$change_id);

              $this->_db->commit();

              $this->_redirect('/notification/submited/index');
              //$this->_redirect('/notification/success/index');
            }
            catch(Exception $e)
            {
              $this->_db->rollBack();
              $error_remark = $this->getErrorRemark('82');
              SGO_Helper_GeneralLog::technicalLog($e);
            }
          }
        }
        else{
          if(!empty($uniqueMaster))
            $error_remark = $this->language->_('This User id and Currency combination already exists');
          else
            $error_remark = $this->language->_('This User id and Currency combination already suggested');
        }


        if(isset($error_remark))
        {
          //insert log
          try
          {
            $this->_db->beginTransaction();
            $fulldesc = Application_Helper_General::displayFullDesc($_POST);
            $this->backendLog(strtoupper($this->_changeType['code']['new']),strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
            $this->_db->commit();
          }
          catch(Exception $e)
          {
            $this->_db->rollBack();
            SGO_Helper_GeneralLog::technicalLog($e);
          }

          //$this->_helper->getHelper('FlashMessenger')->addMessage('F');
          //$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
          //$this->_redirect($this->view->backURL);

          $errorArray = array();
          if($errorLim == true){
            $errorArray['dailylimit'] = $error_remark;
          }
          else{
            if(!empty($uniqueMaster) || !empty($uniqueTemp))
              $errorArray['user_login'] = $error_remark;
            else
              $this->view->error_msg = $error_remark;
          }

          if(!empty($errorArray))
            $this->view->userDailyLimit_msg = $errorArray;

          $this->view->ccy_id  = ($zf_filter_input->isValid('ccy_id'))?  $zf_filter_input->ccy_id  : $this->_getParam('ccy_id');
          $this->view->user_id = ($zf_filter_input->isValid('user_login'))? $zf_filter_input->user_login : $this->_getParam('user_login');
          $this->view->max_amt = ($zf_filter_input->isValid('dailylimit'))? $zf_filter_input->dailylimit : $this->_getParam('dailylimit');
        }
    }
    else
    {
      $this->view->error = 1;

      $this->view->ccy_id  = ($zf_filter_input->isValid('ccy_id'))?  $zf_filter_input->ccy_id  : $this->_getParam('ccy_id');
      $this->view->user_id = ($zf_filter_input->isValid('user_login'))? $zf_filter_input->user_id : $this->_getParam('user_login');
      $this->view->max_amt = ($zf_filter_input->isValid('dailylimit'))? $zf_filter_input->dailylimit : $this->_getParam('dailylimit');



     $error = $zf_filter_input->getMessages();

     //format error utk ditampilkan di view html
     $errorArray = null;
     foreach($error as $keyRoot => $rowError)
     {
        foreach($rowError as $errorString)
        {
          $errorArray[$keyRoot] = $errorString;
        }
     }



     $this->view->userDailyLimit_msg = $errorArray;
    }
  }
  else
  {
     //$this->view->max_amt = Application_Helper_General::displayMoney('9999999999999.99');
     $this->view->ccy_id  = $getCcy[0]['CCY_ID'];
     $this->view->user_id = (isset($userData[0]['USER_ID']) ? $userData[0]['USER_ID'] : NULL );
  }


  $select = $this->_db->select()
                         ->from('M_FGROUP',array('FGROUP_ID','FGROUP_NAME'))
                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' OR UPPER(CUST_ID)='.$this->_db->quote('BANK'))
                         ->where('UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
  $this->view->fgroup = $this->_db->fetchAll($select);

  $select = $this->_db->select()
                         ->from('M_CUSTOMER',array('CUST_NAME'))
                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
  $this->view->cust_name = $this->_db->fetchOne($select);

  $this->view->cust_id = $cust_id;
  $this->view->modulename = $this->_request->getModuleName();


    //insert log
    try
    {
      $this->_db->beginTransaction();

      Application_Helper_General::writeLog('DLAD','Add User Daily Limit');

      $this->_db->commit();
    }
    catch(Exception $e)
    {
      $this->_db->rollBack();
      SGO_Helper_GeneralLog::technicalLog($e);
    }

  }


}
