<?php


require_once 'Zend/Controller/Action.php';


class realtimecharges_RepairController extends Application_Main
{
	public function indexAction() 
	{
		$change_id = $this->_getParam('changes_id');
		
		$select = $this->_db->select()
							->from('T_GLOBAL_CHANGES');
		$select -> where("CHANGES_ID LIKE ".$this->_db->quote($change_id));
		$result = $this->_db->FetchRow($select);
		$this->view->result=$result;
		
		$chargestatus = $result["CUST_CHARGES_STATUS"];
		
		if($chargestatus == 0)
		{
			$this->view->chargestatus = 'Disabled';
		}
		if($chargestatus == 1)
		{
			$this->view->chargestatus = 'Enabled';
		}
		
		$custid = $result['KEY_FIELD'];
		
		$selectacc = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectacc -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectacc -> where("A.ACCT_SOURCE NOT LIKE '1'");
		$selectacc -> where("A.ACCT_SOURCE IS NOT NULL");
		
		$resultacc = $selectacc->query()->FetchAll();
		$this->view->resultaccount = $resultacc;
		
		//Zend_Debug::dump($custid); die;
		$select3 = $this->_db->select()
					->from('TEMP_CHARGES_WITHIN')
					->where("CHANGES_ID LIKE ".$this->_db->quote($change_id))
					->query()->FetchAll();
					
		if($select3)
		{			
			foreach($select3 as $row)
				{	
					$id = $row['ACCT_NO'].'amount';							
					$amt = Application_Helper_General::convertDisplayMoney($row['AMOUNT']);		
					$this->view->$id = $amt;
					//Zend_Debug::dump($id); die;
				}
		}		

		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
			if($submit)
			{
				Application_Helper_General::writeLog('CHCR','Repairing Realtime charges ('.$change_id.')');
				$this->_db->beginTransaction();
				try
				{
					$info = 'Charges';
					$error = 0;
					$this->updateGlobalChanges($change_id,$info);
					
					if($select3)
					{
						$where = array('CHANGES_ID = ?' => $change_id);
						$this->_db->delete('TEMP_CHARGES_WITHIN',$where);
					}
					
					foreach($resultacc as $row)
					{
						$id = $row['ACCT_NO'].'amount';
						$cekamt = $this->_getParam($id);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						if($cekamt == null)
						{
							$amt = '0.00';
						}
						$temp_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
						//Zend_Debug::dump($select3); die;
						
						if($cek_angka)
						{				
							$data= array(
											'CHANGES_ID'		=> $change_id,
											'CUST_ID' 			=> $custid,
											'ACCT_NO' 			=> $row['ACCT_NO'],
											'AMOUNT' 			=> $amt,
											'CHARGES_ACCT_NO' 	=> $row['ACCT_NO'],
											'BUSINESS_TYPE'		=> '1',
										);
							$this->_db->insert('TEMP_CHARGES_WITHIN',$data);				
							$this->view->$id = $amt;										
						}
							
						else
						{
		
						$this->view->$id = $amt;
						$errid = 'err'.$row['ACCT_NO'];	
						$err = 'Amount value has to be numeric';
						$this->view->$errid = $err;
						$error++;
						}
					}
					
					if($error == 0)
					{	
						//die;
						$this->_db->commit();
						$this->_redirect('/popup/successpopup/index');
					}
				}
				catch(Exception $e)
				{
					//die;
					$this->_db->rollBack();
				}
			}
			else
			{
				Application_Helper_General::writeLog('CHCR','View Repair Realtime charges ('.$change_id.')');
			}
	}
}
