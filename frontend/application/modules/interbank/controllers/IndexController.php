<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
class interbank_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	// public function __construct()
	// {
	// 	$this->_db = Zend_Db_Table::getDefaultAdapter();
	// }


	public function indexAction()
	{
		// $custId = $this->_custIdLogin;
		$this->_helper->layout()->setLayout('newlayout');

		// if (!$this->view->hasPrivilege('ITCE')) { 
		// 	$this->_redirect('/authorizationacl/index/index');
		// }

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'))
			->order('C.BANK_NAME ASC');


		$databank 					= $this->_db->fetchAll($selectbank);

		$m_fee = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('A' => 'M_FEE'), array('SKN_FEE', 'BANK_CODE', 'RTGS_FEE'))
					->where("A.CUST_ID = ? ", $this->_custIdLogin)
			);

		foreach ($m_fee as $key => $value) {
			$bankcode[$value['BANK_CODE']] = $value['SKN_FEE'];
			$bankcode1[$value['BANK_CODE']] = $value['RTGS_FEE'];

		}

		$app = Zend_Registry::get('config');
		// charges.type.code.api/
		$status = $app['charges']['type']['code']['api'];
		$desc = $app['charges']['type']['desc']['api'];
		// var_dump($status);die;
		$paramservice = array();
		foreach ($status as $key => $value) {
			$paramservice[$value] = $desc[$key];
		}

		$this->view->paramservice = $paramservice;

		// $bankArr = array(''=>'-- '.$this->language->_('Please Select').' --');
		foreach ($databank as $key => $value) {
			// print_r($value);die;
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		$this->view->DEBIT_BANKarr  = $bankArr;

		$optMonth = '';
		$min1month = date("F Y", strtotime("first day of -1 month"));
		$min1monthval = date("Y-m", strtotime("first day of -1 month"));
		$min2month = date("F Y", strtotime("first day of -2 month"));
		$min2monthval = date("Y-m", strtotime("first day of -2 month"));
		$min3month = date("F Y", strtotime("first day of -3 month"));
		$min3monthval = date("Y-m", strtotime("first day of -3 month"));

		$currentMonthVal = date("Y-m");
		$currentMonth = date("F Y");

		$optMonth .= '<option value="' . $min3monthval . '">' . $min3month . '</option>';
		$optMonth .= '<option value="' . $min2monthval . '">' . $min2month . '</option>';
		$optMonth .= '<option value="' . $min1monthval . '">' . $min1month . '</option>';
		$optMonth .= '<option value="' . $currentMonthVal . '" selected>' . $currentMonth . ' (Ongoing)</option>';

		$this->view->optMonth = $optMonth;

		$test1 = date('Y-m-01');
		$test2 = date('Y-m-d');
		$this->view->from = $test1;
		$this->view->to = $test2;


		// if ($this->_request->isPost()) {
		// 	$month = $this->_getParam('month');

		// 	$hitlistbank = $this->_db->fetchAll(
		// 		$this->_db->select()
		// 			->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
		// 			->JOIN(array('B' => 'M_BANK_TABLE'), 'A.DIGI_BANK = B.BANK_CODE', array('B.BANK_NAME'))
		// 			->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
		// 			->where("A.DIGI_SERVICE != 0")
		// 			// ->where("A.DIGI_BANK != NULL")
		// 			// ->where("A.DIGI_SERVICE != NULL")
		// 			->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $month . "'")
		// 			->group('A.DIGI_BANK')
		// 	);

		// 	$hitlist = $this->_db->fetchAll(
		// 		$this->_db->select()
		// 			->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
		// 			->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
		// 			->where("A.DIGI_SERVICE != 0")
		// 			->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $month . "'")
		// 			->group('A.DIGI_BANK')
		// 			->group('A.DIGI_SERVICE')
		// 	);
		// } else {
			$whereCurDate = date('Y-m');

			$hitlistbank = $this->_db->select()
					->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
					->JOIN(
						array('B' => 'M_BANK_TABLE'),
						'A.DIGI_BANK = B.BANK_CODE',
						array('B.BANK_NAME')
					)
					->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
					->where("A.DIGI_SERVICE IN (4,5)")
					// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $whereCurDate . "'")
					->group('A.DIGI_BANK');

			$hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($test1)." and ".$this->_db->quote($test2));

			$hitlistbank = $this->_db->fetchAll($hitlistbank);

			$hitlist = $this->_db->select()
					->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
					->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
					->where("A.DIGI_SERVICE IN (4,5)")
					// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $whereCurDate . "'")
					->group('A.DIGI_BANK')
					->group('A.DIGI_SERVICE');

			$hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($test1)." and ".$this->_db->quote($test2));

			$hitlist = $this->_db->fetchAll($hitlist);
		// }

		//get data from globalvar.ini
		$serviceType = array_flip($this->_chargestype['code']);
		$serviceDesc =  $this->_chargestype['desc']['api'];
		$serviceColumnName =  $this->_chargestype['dbcolumn']['api'];

		$this->view->serviceType = $serviceType;
		$this->view->serviceColumnName = $serviceColumnName;
		$this->view->serviceDesc = $serviceDesc;

		//get charges data from M_CHARGES_API
		$custIdList = "'" . $this->_custIdLogin . "','GLOBAL'";
		$chargesFee = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_CHARGES_API'), array('*'))
				->where("A.CUST_ID IN (" . $custIdList . ")")
		);

		$chargesFee = $chargesFee[0];

		unset($chargesFee['BALANCE_INQUIRY']);
		unset($chargesFee['ACCOUNT_STATEMENT']);
		unset($chargesFee['INHOUSE_TRANSFER']);
		unset($chargesFee['ONLINE_TRANSFER']);
		unset($chargesFee['KURS_RATE_INQUIRY']);
		unset($chargesFee['INHOUSE_BENEF_INQUIRY']);
		unset($chargesFee['INTERBANK_BENEF_INQUIRY']);

		$this->view->chargesFee = $chargesFee;

		if (!empty($chargesFee)) {
			foreach ($chargesFee as $key => $value) {
				$priceTable[$key] = $value;
			}
		}
		$this->view->priceTable = $priceTable;
	

		$total = 0;
		if (!empty($hitlist)) {
			foreach ($hitlist as $key => $value) {
				$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
				$totalsrv = (int) $value['TOTALHIT'] * $price;
				$hitlist[$key]['TOTALPRICE'] = $totalsrv;

				$total = $total + $totalsrv;
			}
			$this->view->hitlistbank = $hitlistbank;
			// print_r($hitlist);die();
			$this->view->servicelist = $hitlist;
			// echo "<pre>";
			// var_dump($hitlistbank);
			// var_dump($hitlist);
			$this->view->totalprice = $total;

			foreach ($bankArr as $key => $value) {
				foreach ($hitlist as $ht) {
					if ($key == $ht['DIGI_BANK']) {

						// var_dump($ht['DIGI_SERVICE']);
						$sknfee  = $bankcode[$ht['DIGI_BANK']];
						$rtgsfee = $bankcode1[$ht['DIGI_BANK']];

						$price = $chargesFee[$serviceColumnName[$serviceType[$ht['DIGI_SERVICE']]]];
						$totalsrv = $price;

						// if ($ht['TOTALHIT'] != 0) {
						// 	$chargesrtgs = $rtgsfee * 
						// }
						// $hitlist[$key]['TOTALPRICE'] = $totalsrv;

						$dataTable[$ht['DIGI_BANK']][$ht['DIGI_SERVICE']] = array($ht['TOTALHIT'], $totalsrv,$sknfee,$rtgsfee);

					}
				}
			}

			// die();

			$this->view->dataTable = $dataTable;
			
		}

		Application_Helper_General::writeLog('VBOB', 'View Billing Console');

		$pdf = $this->_getParam('pdf');

		if ($pdf) {
			$base64 = $this->_getParam('assetsChartBase64');

			$datefrom = $this->_getParam('startdate');
			$dateto = $this->_getParam('enddate');
			$totalcharges = $this->_getParam('totalcharges');


			$serviceType1 = array_flip($this->_chargestype['code']);
			$this->view->serviceType1 = $serviceType1;

			$hitlistbank = $this->_db->select()
					->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
					->JOIN(array('B' => 'M_BANK_TABLE'), 'A.DIGI_BANK = B.BANK_CODE', array('B.BANK_NAME'))
					->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
					->where("A.DIGI_SERVICE = 1 && 2")
					// ->where("A.DIGI_BANK != NULL")
					// ->where("A.DIGI_SERVICE != NULL")
					// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $month . "'")
					->group('A.DIGI_BANK');
			// );

			if(!empty($datefrom) && empty($dateto))
	            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') >= ".$this->_db->quote($datefrom));
	            
		   	if(empty($datefrom) && !empty($dateto))
		            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') <= ".$this->_db->quote($dateto));
		            
		    if(!empty($datefrom) && !empty($dateto))
		            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

		    $hitlistbank = $this->_db->fetchAll($hitlistbank);

			$hitlist = $this->_db->select()
					->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
					->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
					->where("A.DIGI_SERVICE = 1 && 2")
					// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $month . "'")
					->group('A.DIGI_BANK')
					->group('A.DIGI_SERVICE');
			// );

			if(!empty($datefrom) && empty($dateto))
	            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') >= ".$this->_db->quote($datefrom));
	            
		   	if(empty($datefrom) && !empty($dateto))
		            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') <= ".$this->_db->quote($dateto));
		            
		    if(!empty($datefrom) && !empty($dateto))
		            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

			$hitlist = $this->_db->fetchAll($hitlist);

			$total = 0;
			if (!empty($hitlist)) {
				foreach ($hitlist as $key => $value) {
					$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
					$totalsrv = (int) $value['TOTALHIT'] * $price;
					$hitlist[$key]['TOTALPRICE'] = $totalsrv;

					$total = $total + $totalsrv;
				}
				$this->view->pdfHitlistbank = $hitlistbank;
				$this->view->pdfServicelist = $hitlist;
				$this->view->pdfTotalprice = $totalcharges;


				foreach ($bankArr as $key => $value) {
					foreach ($hitlist as $ht) {
						if ($key == $ht['DIGI_BANK']) {

							$sknfee  = $bankcode[$ht['DIGI_BANK']];
							$rtgsfee = $bankcode1[$ht['DIGI_BANK']];

							$price = $chargesFee[$serviceColumnName[$serviceType[$ht['DIGI_SERVICE']]]];
							$totalsrv = $price;

							$dataTable1[$ht['DIGI_BANK']][$ht['DIGI_SERVICE']] = array($ht['TOTALHIT'], $totalsrv,$sknfee,$rtgsfee);

						}
					}
				}

				$this->view->dataTable1 = $dataTable1;
			}

			// $formatedMonth = DateTime::createFromFormat('Y-m', $month);
			// $formMonth = $formatedMonth->format('F Y');

			if(!empty($datefrom) && empty($dateto)){
	            $formatedMonth = DateTime::createFromFormat('Y-m-d', $datefrom);
				$formMonth = $formatedMonth->format('d F Y');
			}
	            
		   	if(empty($datefrom) && !empty($dateto)){
		        $formatedMonth = DateTime::createFromFormat('Y-m-d', $dateto);
				$formMonth = $formatedMonth->format('d F Y');
		    }       
		    if(!empty($datefrom) && !empty($dateto)){
		        $formatedMonth1 = DateTime::createFromFormat('Y-m-d', $datefrom);
				$formMonth1 = $formatedMonth1->format('d F Y');

				$formatedMonth2 = DateTime::createFromFormat('Y-m-d', $dateto);
				$formMonth2 = $formatedMonth2->format('d F Y');

				$formMonth = $formMonth1.' - '.$formMonth2;
		    }
			// echo $formMonth;
			// die();

			// $curMonth = strtotime(date("Y-m"));

			// $datediff = strtotime($month) - $curMonth;
			// $difference = floor($datediff / (60 * 60 * 24));

			// $title = '';
			// if ($difference == 0) {
			// 	$title = 'Billing Console_' . $formMonth . ' (Ongoing)';
			// 	$this->view->formatedMonth = $formMonth . ' (Ongoing)';
			// } else {
				$title = 'Interbank Cost Estimation_' . $formMonth;
				$this->view->formatedMonth = $formMonth;
			// }

			Application_Helper_General::writeLog('BAIQ', 'Print PDF');
			// $HTMLchart = $this->view->render($this->view->controllername.'/chart.phtml');
			$HTMLtable = $this->view->render($this->view->controllername . '/pdf.phtml');
			$this->_helper->download->pdfWithChart(null, null, null, $title, $this->_custNameLogin, 'INTERBANK COST ESTIMATION', $base64, '280px', $HTMLtable);
		}
	}


	public function updatedataAction()
	{
		

		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

		$amountrtgs 	= $this->_getParam('amountrtgs');
		$bankcode 		= $this->_getParam('bankcode');
		$amountskn 	= $this->_getParam('amountskn');

		if (!empty($amountrtgs)) {

			$select	= $this->_db->select()
			->from(array('M_FEE'), array('*'))
			->where('CUST_ID = ?', $this->_custIdLogin)
			->where('BANK_CODE = ?', $bankcode);

			$pslipTrxData1 = $this->_db->fetchAll($select);


			try {

				$this->_db->beginTransaction();

				if (empty($pslipTrxData1)) {
					$data = array(
						'CUST_ID' 		=> $this->_custIdLogin,
						'RTGS_FEE' 		=> $amountrtgs,
						// 'SKN_FEE'		=> 0,
						'BANK_CODE' 	=> $bankcode,
						'UPDATED' 		=> new Zend_Db_Expr("now()"),
						'UPDATEDBY' 	=> $this->_userIdLogin
					);

					$this->_db->insert('M_FEE', $data);
				} else {

					$data = array(
						'RTGS_FEE' 		=> $amountrtgs,
						'UPDATED' 		=> new Zend_Db_Expr("now()"),
						'UPDATEDBY' 	=> $this->_userIdLogin,
					);

					$where = array(
						'CUST_ID = ?' 	=> $this->_custIdLogin,
						'BANK_CODE = ?' => $bankcode,
					);
					$this->_db->update('M_FEE', $data, $where);
				}

				$this->_db->commit();
				echo true;
			}
			catch(Exception $e)
			{
				
				//rollback changes
				$this->_db->rollBack();
				echo false;

			}

		}

		if (!empty($amountskn)) {

			$select	= $this->_db->select()
			->from(array('M_FEE'), array('*'))
			->where('CUST_ID = ?', $this->_custIdLogin)
			->where('BANK_CODE = ?', $bankcode);

			$pslipTrxData1 = $this->_db->fetchAll($select);


			try {

				$this->_db->beginTransaction();

				if (empty($pslipTrxData1)) {
					$data = array(
						'CUST_ID' 		=> $this->_custIdLogin,
						// 'RTGS_FEE' 		=> 0,
						'SKN_FEE'		=> $amountskn,
						'BANK_CODE' 	=> $bankcode,
						'UPDATED' 		=> new Zend_Db_Expr("now()"),
						'UPDATEDBY' 	=> $this->_userIdLogin
					);

					$this->_db->insert('M_FEE', $data);
				} else {

					$data = array(
						'SKN_FEE' 		=> $amountskn,
						'UPDATED' 		=> new Zend_Db_Expr("now()"),
						'UPDATEDBY' 	=> $this->_userIdLogin,
					);

					$where = array(
						'CUST_ID = ?' 	=> $this->_custIdLogin,
						'BANK_CODE = ?' => $bankcode,
					);
					$this->_db->update('M_FEE', $data, $where);
				}

				$this->_db->commit();
				echo true;
			}
			catch(Exception $e)
			{
				
				//rollback changes
				$this->_db->rollBack();
				echo false;

			}

		}

	}

	public function changemonthAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// $value = $this->_getParam('value');

		$datefrom = $this->_getParam('startdate');
		$dateto = $this->_getParam('enddate');

		$hitlistbank = $this->_db->select()
				->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
				->JOIN(array('B' => 'M_BANK_TABLE'), 'A.DIGI_BANK = B.BANK_CODE', array('B.BANK_NAME'))
				->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
				->where("A.DIGI_SERVICE IN (4,5)")
				// ->where("A.DIGI_BANK != NULL")
				// ->where("A.DIGI_SERVICE != NULL")
				// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d')= '" . $value . "'")
				->group('A.DIGI_BANK');

		if(!empty($datefrom) && empty($dateto))
	            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $hitlistbank->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

		$hitlistbank = $this->_db->fetchAll($hitlistbank);



		$hitlist = $this->_db->select()
				->from(array('A' => 'T_DIGI_LOG'), array('TOTALHIT' => 'COUNT(DIGI_ID)', 'DIGI_BANK', 'DIGI_SERVICE'))
				->where("A.DIGI_CUST = ? ", $this->_custIdLogin)
				->where("A.DIGI_SERVICE IN (4,5)")
				// ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '" . $value . "'")
				->group('A.DIGI_BANK')
				->group('A.DIGI_SERVICE');
		// );

		if(!empty($datefrom) && empty($dateto))
	            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $hitlist->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m-%d') between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		
		
		$hitlist = $this->_db->fetchAll($hitlist);

		$m_fee = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('A' => 'M_FEE'), array('SKN_FEE', 'BANK_CODE', 'RTGS_FEE'))
					->where("A.CUST_ID = ? ", $this->_custIdLogin)
			);

		foreach ($m_fee as $key => $value) {
			$bankcode[$value['BANK_CODE']] = $value['SKN_FEE'];
			$bankcode1[$value['BANK_CODE']] = $value['RTGS_FEE'];

		}

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'))
			->order('C.BANK_NAME ASC');


		$databank 					= $this->_db->fetchAll($selectbank);

		foreach ($databank as $key => $value) {
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		//get data from globalvar.ini
		
		$config = Zend_Registry::get('config');
		
		$serviceType = array_flip($config['charges']['type']['code']['api']);
		$serviceDesc =  $config['charges']['type']['desc']['api'];
		$serviceColumnName =  $this->_chargestype['dbcolumn']['api'];

		$total = 0;
		$custIdList = "'" . $this->_custIdLogin . "','GLOBAL'";
		$chargesFee = $this->_db->fetchAll(
			$this->_db->select()
				->from(array('A' => 'M_CHARGES_API'), array('*'))
				->where("A.CUST_ID IN (" . $custIdList . ")")
		);

		$chargesFee = $chargesFee[0];

		unset($chargesFee['BALANCE_INQUIRY']);
		unset($chargesFee['ACCOUNT_STATEMENT']);
		unset($chargesFee['INHOUSE_TRANSFER']);
		unset($chargesFee['ONLINE_TRANSFER']);
		unset($chargesFee['KURS_RATE_INQUIRY']);
		unset($chargesFee['INHOUSE_BENEF_INQUIRY']);
		unset($chargesFee['INTERBANK_BENEF_INQUIRY']);


		if (!empty($chargesFee)) {
			foreach ($chargesFee as $key => $value) {
				$priceTable[$key] = $value;
			}
		}
		//echo '<pre>';
		//var_dump($hitlist);die;
		if (!empty($hitlist)) {
			foreach ($hitlist as $key => $value) {
				$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
				$totalsrv = (int) $value['TOTALHIT'] * $price;
				$hitlist[$key]['TOTALPRICE'] = $totalsrv;

				$total = $total + $totalsrv;
			}

			foreach ($bankArr as $key => $value) {
				foreach ($hitlist as $ht) {
					if ($key == $ht['DIGI_BANK']) {

						// var_dump($ht['DIGI_SERVICE']);
						$sknfee  = $bankcode[$ht['DIGI_BANK']];
						$rtgsfee = $bankcode1[$ht['DIGI_BANK']];

						$price = $chargesFee[$serviceColumnName[$serviceType[$ht['DIGI_SERVICE']]]];
						$totalsrv = $price;

						// if ($ht['TOTALHIT'] != 0) {
						// 	$chargesrtgs = $rtgsfee * 
						// }
						// $hitlist[$key]['TOTALPRICE'] = $totalsrv;

						$dataTable[$ht['DIGI_BANK']][$ht['DIGI_SERVICE']] = array($ht['TOTALHIT'], $totalsrv,$sknfee,$rtgsfee);

					}
				}
			}

		}
		//echo '<pre>';
		//var_dump($dataTable);die;

		$result['hitlistbank'] = $hitlistbank;
		$result['servicelist'] = $hitlist;
		$result['totalprice'] = 'Total: IDR ' . str_replace('.00', '', Application_Helper_General::displayMoney($total));

		if (!empty($hitlistbank)) {
			$datagraph = array();
			$totalhit = 0;

			$setting = new Settings();
			$global_charges_rtgs = $setting->getSetting('global_charges_rtgs');
			$global_charges_skn = $setting->getSetting('global_charges_skn');


			foreach ($hitlistbank as $key => $value) {
				$totalhit += $value['TOTALHIT'];
			}

			foreach ($dataTable as $test => $d_value) {

			  $skn = 0;
			  $rtgs = 0;
			 
			  foreach ($d_value as $ht => $ht_value) {
				//var_dump($serviceType);die;
			    $serviceType[$ht] == 'skn' ? $skn = $ht_value[0] : '';
			    $serviceType[$ht] == 'rtgs' ? $rtgs = $ht_value[0] : '';

			    $skn_fee = $ht_value[2];
			    $rtgs_fee = $ht_value[3];

			    if ($rtgs_fee != NULL || !empty($rtgs_fee)) {

			      $rtgscharges = $rtgs_fee * $rtgs;
			        
			    }else{

			      $rtgscharges = $global_charges_rtgs * $rtgs;
			      
			    }

			    if ($skn_fee != NULL || !empty($skn_fee)) {

			      $skncharges = $skn_fee * $skn;
			        
			    }else{
			      
			      $skncharges = $global_charges_skn * $skn;
			      
			    }

			    $totalchargesrtgs = $rtgscharges + $skncharges;
			    
			  }

			  $alltotalchargesrtgs = $alltotalchargesrtgs + $totalchargesrtgs;

			}

			foreach ($hitlistbank as $key => $value) {

				foreach ($bankArr as $test => $val) {
					if ($test == $value['DIGI_BANK']) {

					  $price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
					  $totalsrv = $value['TOTALHIT'] * $price;
					 
					}
				}

			      $totalchargesrtgs = 0;

			      foreach ($dataTable as $test => $d_value) {

			        if($test == $value['DIGI_BANK']){
			       
			          $skn = 0;
			          $rtgs = 0;
			         
			          foreach ($d_value as $ht => $ht_value) {

			            $serviceType[$ht] == 'skn' ? $skn = $ht_value[0] : '';
			            $serviceType[$ht] == 'rtgs' ? $rtgs = $ht_value[0] : '';

			            $skn_fee = $ht_value[2];
			            $rtgs_fee = $ht_value[3];

			            if ($rtgs_fee != NULL || !empty($rtgs_fee)) {

			              $rtgscharges = $rtgs_fee * $rtgs;
			                
			            }else{

			              $rtgscharges = $global_charges_rtgs * $rtgs;
			              
			            }

			            if ($skn_fee != NULL || !empty($skn_fee)) {

			              $skncharges = $skn_fee * $skn;
			                
			            }else{
			              
			              $skncharges = $global_charges_skn * $skn;
			              
			            }

			            $totalchargesrtgs = $rtgscharges + $skncharges;
			           
			          }

			        }

			      }

			    $datagraph[$key]['y'] = number_format(((int) $totalchargesrtgs / (int) $alltotalchargesrtgs) * 100);
      			$datagraph[$key]['toolTipContent'] = 'IDR '.str_replace('.00', '', Application_Helper_General::displayMoney($totalchargesrtgs));
				$datagraph[$key]['legendText'] = $value['BANK_NAME'];
				$datagraph[$key]['name'] = $value['BANK_NAME'];
				$datagraph[$key]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			}
		}

		// $result['datapointstr'] = $str;

		$visitorsData['Service'][] = array(
			// 'click' => 'visitorsChartDrilldownHandler',
			'cursor' => 'pointer',
			'explodeOnClick' => false,
			'name' => "New vs Returning Visitors",
			'showInLegend' => true,
			'indexLabel' => "{y}%",
			'indexLabelPlacement' => "inside",
			'indexLabelFontColor' => 'white',
			'indexLabelFontSize' => 11,
			// 'indexLabelPlacement' => "outside",
			'type' => "doughnut",
			'dataPoints' => $datagraph
		);

		if (!empty($hitlistbank)) {


			$str = '';
			foreach ($hitlistbank as $key => $value) {

				$datagraphdetail = array();

				$strdata = array();
				foreach ($hitlist as $k => $val) {
					if ($value['DIGI_BANK'] == $val['DIGI_BANK']) {
						$datagraphdetail['y'] = (int) $val['TOTALHIT'];
						$service = array();
						$service[1] = 'Balance Inquiry';
						$service[2] = 'Register ';
						$service[NULL] = 'Transfer Fee';
						$datagraphdetail['name'] = $service[$val['DIGI_SERVICE']];
						$datagraphdetail['indexLabel'] = $service[$val['DIGI_SERVICE']] . ' : ' . $val['TOTALHIT'];
						$datagraphdetail['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

						array_push($strdata, $datagraphdetail);
						unset($datagraphdetail);
					}
				}

				if (!empty($hitlistbank[$key + 1])) {

					$visitorsData[$value['BANK_NAME']][] = array(
						// 'click' => 'visitorsChartDrilldownHandler',
						'cursor' => 'pointer',
						'explodeOnClick' => false,
						'color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
						'name' => $value['BANK_NAME'],
						'type' => 'doughnut',
						'showInLegend' => 'true',
						'indexLabel' => "{y}%",
						'indexLabelPlacement' => "inside",
						'indexLabelFontColor' => 'white',
						'indexLabelFontSize' => 11,
						'dataPoints' => $strdata,
					);
				} else {

					$visitorsData[$value['BANK_NAME']][] = array(
						// 'click' => 'visitorsChartDrilldownHandler',
						'cursor' => 'pointer',
						'explodeOnClick' => false,
						'color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
						'name' => $value['BANK_NAME'],
						'type' => 'doughnut',
						// 'indexLabelPlacement' => 'outside',
						'showInLegend' => 'true',
						'indexLabel' => "{y}%",
						'indexLabelPlacement' => "inside",
						'indexLabelFontColor' => 'white',
						'indexLabelFontSize' => 11,
						'dataPoints' => $strdata,
					);
				}
			}
		}

		$result['visitorsdata'] = json_encode($visitorsData);

		$i = 0;
		$tableData = '';

		if (!empty($dataTable)) {
				//price
				// $p_balance = $priceTable['BALANCE_INQUIRY'];
				// $p_pb = $priceTable['INHOUSE_TRANSFER'];
				// $p_online = $priceTable['ONLINE_TRANSFER'];
				$p_skn = $priceTable['SKN_TRANSFER'];
				$p_rtgs = $priceTable['RTGS_TRANSFER'];
				$p_interbenefinquiry = $priceTable['INTERBANK_BENEF_INQUIRY'];
				// $p_statement = $priceTable['ACCOUNT_STATEMENT'];
				// $p_inhousebenefinquiry = $priceTable['INHOUSE_BENEF_INQUIRY'];

				$setting = new Settings();
                $global_charges_rtgs = $setting->getSetting('global_charges_rtgs');
                $global_charges_skn = $setting->getSetting('global_charges_skn');

                $alltotalchargesrtgs = 0;
			
			foreach ($dataTable as $key => $d_value) {
				$balance = 0;
				$pb = 0;
				$online = 0;
				$skn = 0;
				$rtgs = 0;
				$interbenefinquiry = 0;
				$inhousebenefinquiry = 0;
				
				foreach ($d_value as $ht => $ht_value) {
					//echo '<pre>';
				//var_dump($ht_value);	
					// $serviceType[$ht] == 'balance' ? $balance = $ht_value[0] : '';
					// $serviceType[$ht] == 'pb' ? $pb = $ht_value[0] : '';
					// $serviceType[$ht] == 'online' ? $online = $ht_value[0] : '';
					$serviceType[$ht] == 'skn' ? $skn = $ht_value[0] : '';
					$serviceType[$ht] == 'rtgs' ? $rtgs = $ht_value[0] : '';
					// $serviceType[$ht] == 'interbenefinquiry' ? $interbenefinquiry = $ht_value[0] : '';
					// $serviceType[$ht] == 'statement' ? $statement = $ht_value[0] : '';
					// $serviceType[$ht] == 'inhousebenefinquiry' ? $inhousebenefinquiry = $ht_value[0] : '';

					// $serviceType[$ht] == 'balance' ? $p_balance = $ht_value[1] : '';
					// $serviceType[$ht] == 'pb' ? $p_pb = $ht_value[1] : '';
					// $serviceType[$ht] == 'online' ? $p_online = $ht_value[1] : '';
					// $serviceType[$ht] == 'skn' ? $p_skn = $ht_value[1] : '';
					// $serviceType[$ht] == 'rtgs' ? $p_rtgs = $ht_value[1] : '';
					// $serviceType[$ht] == 'interbenefinquiry' ? $p_interbenefinquiry = $ht_value[1] : '';
					// $serviceType[$ht] == 'statement' ? $p_statement = $ht_value[1] : '';
					// $serviceType[$ht] == 'inhousebenefinquiry' ? $p_inhousebenefinquiry = $ht_value[1] : '';

					$skn_fee = $ht_value[2];
                    $rtgs_fee = $ht_value[3];

                    if ($rtgs_fee != NULL || !empty($rtgs_fee)) {

                        $rtgscharges = $rtgs_fee * $rtgs;
                       	$amountrtgs = '<a href="#" onClick="inputAmount2(\''.$bankArr[$key].'\',\''.$key.'\',\''.$rtgs_fee.'\')">'.str_replace('.00', '', Application_Helper_General::displayMoney($rtgs_fee)).'</a><input type="hidden" name="pooling2_'.$key.'" id="pooling2_'.$key.'" value="'.$rtgs_fee.'">';

                    }else{

                      $rtgscharges = $global_charges_rtgs * $rtgs;  
                      $amountrtgs = '<a href="#" onClick="inputAmount2(\''.$bankArr[$key].'\',\''.$key.'\',\''.$global_charges_rtgs.'\')">'.str_replace('.00', '', Application_Helper_General::displayMoney($global_charges_rtgs)).'</a><input type="hidden" name="pooling2_'.$key.'" id="pooling2_'.$key.'">';
                      
                    }

                    if ($skn_fee != NULL || !empty($skn_fee)) {

                        $skncharges = $skn_fee * $skn;
                      	$amountskn = '<a href="#" onClick="inputAmount(\''.$bankArr[$key].'\',\''.$key.'\',\''.$skn_fee.'\')">'.str_replace('.00', '', Application_Helper_General::displayMoney($skn_fee)).'</a><input type="hidden" name="pooling_'.$key.'" id="pooling_'.$key.'" value="'.$skn_fee.'">';

                    }else{
                      
	                    $skncharges = $global_charges_skn * $skn;
                      	$amountskn = '<a href="#" onClick="inputAmount(\''.$bankArr[$key].'\',\''.$key.'\',\''.$global_charges_skn.'\')">'.str_replace('.00', '', Application_Helper_General::displayMoney($global_charges_skn)).'</a><input type="hidden" name="pooling_'.$key.'" id="pooling_'.$key.'">';

                    }

                    $totalchargesrtgs = $rtgscharges + $skncharges;
                    
				}

				$alltotalchargesrtgs = $alltotalchargesrtgs + $totalchargesrtgs;

				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
				$tableData .= '<tr>  
										<td class="' . $td_css . '">' . $bankArr[$key] . '</td>
										<td class="' . $td_css . ' centeralign">'.$amountskn.'</td>
										<td class="' . $td_css . ' centeralign"><p>' . number_format($skn) . '</p></td>
										<td class="' . $td_css . ' centeralign">'.str_replace('.00', '', Application_Helper_General::displayMoney($skncharges)).'</td>
										<td class="' . $td_css . ' centeralign">'.$amountrtgs.'</td>
										<td class="' . $td_css . ' centeralign"><p>' . number_format($rtgs) . '</p></td>
										<td class="' . $td_css . ' centeralign">'.str_replace('.00', '', Application_Helper_General::displayMoney($rtgscharges)).'</td>
										<td class="' . $td_css . ' centeralign">'.str_replace('.00', '', Application_Helper_General::displayMoney($totalchargesrtgs)).'</td>
							</tr>';
				$i++;
				
			}
			//var_dump($tableData);die;
			
		}
		
		
		// if (!empty($hitlist)) {
		// 	foreach ($hitlist as $key => $value) {
		// 		$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
		// 		$tableData .= '<tr>  
		//                 <td class="' . $td_css . '">' . $bankArr[$value['DIGI_BANK']] . '</td>
		//                 <td class="' . $td_css . '">' . $serviceDesc[$serviceType[$value['DIGI_SERVICE']]] . '</td>
		//                 <td class="' . $td_css . '" align="right">' . number_format($value['TOTALHIT']) . '</td>
		//                 <td class="' . $td_css . '" style="max-width: 160px;" ><label class="col-md-3" style="text-align:left">IDR</label><label class="col-md-9" style="text-align:right">' . Application_Helper_General::displayMoney($value['TOTALPRICE']) . '</label></td>
		//               </tr>';
		// 		$i++;
		// 	}
		// } 
		else {
			$tableData .= '<tr><td colspan="8" align="center">---- No Data ----</td></tr>';
		}

		$result['tableData'] = $tableData;
		$result['alltotalpricesknrtgs'] = $alltotalchargesrtgs;

		echo json_encode($result);
	}

	
}
