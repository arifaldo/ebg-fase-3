<?php

class specialobligee_Model_Specialobligee
{
	protected $_db;

	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getData($filterParam = null, $filter = null)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'M_CUST_SPOBLIGEE'),
				array('*')
			)
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME', 'CUST_CITY', 'CUST_STATUS')
			)->joinLeft(
				array('C' => 'M_COUNTRY'),
				'B.COUNTRY_CODE = C.COUNTRY_CODE',
				array('COUNTRY_NAME')
			);

		if ($filter == TRUE) {
			// if ($filterParam['fCompanyCode']) {
			// 	$select->where('A.CUST_ID LIKE ' . $this->_db->quote('%' . $filterParam['fCompanyCode'] . '%'));
			// }

			if ($filterParam['fCompanyName']) {
				$select->where('B.CUST_NAME LIKE ' . $this->_db->quote('%' . $filterParam['fCompanyName'] . '%'));
			}
			if ($filterParam['pksstatus']) {
				$select->where('A.STATUS = ?', $filterParam['pksstatus']);
			}

			if ($filterParam["datefrom"]) {
				if ($filterParam["dateto"]) {
					$datefrom = (strtotime($filterParam['datefrom']) < strtotime($filterParam['dateto'])) ? $filterParam['datefrom'] : $filterParam['dateto'];
					$dateto = (strtotime($filterParam['datefrom']) < strtotime($filterParam['dateto'])) ? $filterParam['dateto'] : $filterParam['datefrom'];

					$select->where('A.PKS_EXP_DATE BETWEEN "' . $datefrom . '" AND "' . $dateto . '"');
				} else {
					$select->where('A.PKS_EXP_DATE >= "' . $filterParam['datefrom'] . '"');
				}
			} elseif ($filterParam["dateto"]) {
				if ($filterParam["datefrom"]) {
					$datefrom = (strtotime($filterParam['datefrom']) < strtotime($filterParam['dateto'])) ? $filterParam['datefrom'] : $filterParam['dateto'];
					$dateto = (strtotime($filterParam['datefrom']) < strtotime($filterParam['dateto'])) ? $filterParam['dateto'] : $filterParam['datefrom'];
					$select->where('A.PKS_EXP_DATE BETWEEN "' . $datefrom . '" AND "' . $dateto . '"');
				} else {
					$select->where('A.PKS_EXP_DATE <= "' . $filterParam['dateto'] . '"');
				}
			}
		}

		return $this->_db->fetchAll($select);
	}

	public function getDataById($id)
	{
		$data = $this->_db->select()
			->from(array('A' => 'M_CUST_SPOBLIGEE'), array('*'))
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME')
			)
			->joinLeft(
				array('C' => 'M_CUSTOMER_ACCT'),
				'A.CUST_ID = C.CUST_ID',
				array('ACCT_NO', 'ACCT_NAME', 'CCY_ID', 'ACCT_DESC')
			)
			->where('A.ID = ?', $id);
		return $this->_db->fetchRow($data);
	}

	public function getDataByCustId($cust_id)
	{
		$data = $this->_db->select()
			->from(array('A' => 'M_CUST_SPOBLIGEE'), array('*'))
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME', 'COLLECTIBILITY_CODE')
			)
			->where('A.CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($data);
	}

	public function getCustomer()
	{
		$cust_id = $this->_db->select()
			->from(array('M_CUST_SPOBLIGEE'), array('CUST_ID'))
			->query()->fetchAll();

		$select = $this->_db->select()
			->from('M_CUSTOMER')
			->where('CUST_MODEL = ?', 3)
			->where('CUST_STATUS = ?', 1);

		if ($cust_id) $select->where('CUST_ID NOT IN (?)', $cust_id);

		$select = $select->query()->fetchAll();

		return $select;
	}

	public function getCustomerById($cust_id)
	{
		$select = $this->_db->select()
			->from('M_CUSTOMER')
			->where('CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($select);
	}

	public function getCustAcctById($cust_id)
	{
		$data = $this->_db->select()
			->from(
				array('A' => 'M_CUSTOMER_ACCT'),
				array(
					'ACCT_NO'	=> 'A.ACCT_NO',
					'ACCT_NAME'	=> 'A.ACCT_NAME',
					'CCY_ID'	=> 'A.CCY_ID',
					'ACCT_DESC'	=> 'A.ACCT_DESC'
				)
			)
			->where('A.CUST_ID = ?', $cust_id);
		return $this->_db->fetchAll($data);
	}

	public function getCustAcctByAcct($acct_no)
	{
		$data = $this->_db->select()
			->from(
				array('A' => 'M_CUSTOMER_ACCT'),
				array(
					'ACCT_NO'	=> 'A.ACCT_NO',
					'ACCT_NAME'	=> 'A.ACCT_NAME',
					'CCY_ID'	=> 'A.CCY_ID',
					'ACCT_DESC'	=> 'A.ACCT_DESC'
				)
			)
			->where('A.ACCT_NO = ?', $acct_no);
		return $this->_db->fetchRow($data);
	}

	public function getTempSpecialObligee($cust_id)
	{
		$select = $this->_db->select()
			->from(array('A' => 'TEMP_CUST_SPOBLIGEE'), array('*'))
			->where('A.CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($select);
	}

	public function getCity()
	{
		$select = $this->_db->select()
			->from('M_CITYLIST')
			->where('CITY_CODE IS NOT NULL')
			->query()->fetchAll();
		return $select;
	}
}
