<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class transactiondailylimit_SuggestionDetailController extends Application_Main
{
	 public function initController(){
			  $this->_helper->layout()->setLayout('popup');
	}
	public function indexAction() 
	{

		$status = $this->_changeStatus;
		$options = array_combine(array_values($status['code']),array_values($status['desc']));
		
		$changesid = $this->_getparam('changes_id');
		$model = new transactiondailylimit_Model_Transactiondailylimitsetting();
		$cek = $model->cekChanges($changesid);
		
		$selectnew = $model->getTempSetting($changesid);
		
		if(!$cek){
			$this->view->cek = 0;	
			$this->view->changeId = $changesid;		
			$this->_redirect('/notification/invalid/index');
		}
		else{
			$select	= $model->getChangesData($changesid);
			$this->view->created = $select['CREATED'];
			$this->view->createdby = $select['CREATED_BY'];
			foreach($options as $codestat=>$descstat)
			{
				if($codestat == $select['CHANGES_STATUS'])
				{
					$this->view->status = $descstat;
				}
			}
		
			$this->view->cek = 1;
			
//			DATA LAMA			
			$selectold = $model->getSetting();

				foreach($selectold as $row)
				{$this->view->$row['SETTING_ID'] = $row['SETTING_VALUE'];}
				
//			DATA BARU				
				foreach($selectnew as $row)
				{	
					$new = "new".$row['SETTING_ID'];
					$this->view->$new = $row['SETTING_VALUE'];
				}				
			$action  = $this->_request->isPost();

			$this->view->changes_id = $this->_getParam('changes_id');
			//Application_Helper_General::writeLog('STCL','View Suggestion Detail General Setting');
		}
	}
}
