<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';

class predefinedbeneficiary_LocalremittanceacctController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	protected $_payType;
	public function initController()
	{
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
		//$this->_payType = "'".$this->_beneftype["code"]["domestic"]."' , '".$this->_beneftype["code"]["online"]."'";
		//$this->_payType = "'".$this->_beneftype["code"]["remittance"]."'";


	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();

		$aliasname = $this->language->_('Alias Name');
    	$beneficiaryaccount = $this->language->_('Destination Name');

    	$nrc = $this->language->_('NRC');
  		$phone = $this->language->_('Phone');
  		$ccy = $this->language->_('CCY');
  		$favorite = $this->language->_('Favorite');
  		$bankcode = $this->language->_('Bank Code');
  		$status = $this->language->_('Status');
  		$bankname = $this->language->_('Bank Name');
  		$delete = $this->language->_('Action');
  		$address = $this->language->_('Address');





		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $aliasname,
											   'sortable' => true),*/
						// 'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
						// 					   'label' => $beneficiaryaccount,
						// 					   'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => $beneficiaryaccount,
											   'sortable' => true),
						'nrc'  => array('field' => 'BENEFICIARY_ID_NUMBER',
											   'label' => $nrc,
											   'sortable' => true),

						'phone'  => array('field' => 'BENEFICIARY_PHONE',
											   'label' => $phone,
											   'sortable' => true),
						'status'  => array('field' => 'BENEFICIARY_ISAPPROVE_disp',
											   'label' => $status,
											   'sortable' => true),

						'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
											  'label'    => $delete,
											  'sortable' => false)
				);

		$filterlist = array('NRC','BENEFICIARY_NAME');
		
		$this->view->filterlist = $filterlist;


		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'alias' 	  	=> array('StringTrim','StripTags'),
							'NRC'    => array('StringTrim','StripTags','StringToUpper'),
							'BENEFICIARY_NAME'    => array('StringTrim','StripTags')
		);

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		// $filter = $zf_filter->getEscaped('filter');
		$filter = $this->_request->getParam('filter');

		$alpha = $this->_getParam('alpha');

		$this->view->currentPage = $page;
		$this->view->sortBy = $param['sortBy'] = $sortBy;
		$this->view->sortDir = $param['sortDir'] = $sortDir;
		$this->view->alpha = $alpha;

		if($filter == TRUE)
		{
			$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('NRC');
			$fName = $zf_filter->getEscaped('BENEFICIARY_NAME');
			$payType = $zf_filter->getEscaped('benef_type');

			if($fAlias) $param['fAlias'] = $fAlias;
	        if($fAcct) $param['fAcct'] = $fAcct;
	        if($fName) $param['fName'] = $fName;
	        if($payType) $param['payType'] = $payType;

			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->benef_type = $payType;
		}

		$param['user_id'] = $this->_userIdLogin;
		$param['cust_id'] = $this->_custIdLogin;
		$param['payType'] = '4';
		$param['beneLinkage'] = $this->view->hasPrivilege('BLBU');
		/*if(empty($payType))
			$param['payType'] = $this->_payType;*/
		if($alpha) $param['alpha'] = $alpha;

		$select   = $model->getBeneficiaries($param);
// 		print_r($select);die;
		$this->paging($select);
		$this->view->payType = array(''=> ' -- Please Select -- ',$this->_beneftype["code"]["domestic"]=>$this->_beneftype["desc"]["domestic"],$this->_beneftype["code"]["online"]=>$this->_beneftype["desc"]["online"]);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($this->_request->getParam('wherecol'))){
				$this->view->wherecol			= $this->_request->getParam('wherecol');
			}

			if(!empty($this->_request->getParam('whereopt'))){
				$this->view->whereopt			= $this->_request->getParam('whereopt');
			}

			if(!empty($this->_request->getParam('whereval'))){
				$this->view->whereval			= $this->_request->getParam('whereval');
			}
	    Application_Helper_General::writeLog('BLBA','View Destination Account');
	}

	public function addbeneficiaryAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->bankpop = 'disabled';

//		$payment					= new payment_Model_Payment();
		$modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();


		$settings 			= new Application_Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBA');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();

		//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1

		//validasi token -- begin
		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$data2 					= $this->_db->fetchRow($select3);


		if($this->_request->isPost() )
		{

		    $filters = array(
		        'NRC' => array('StringTrim','StripTags'),
		        'PHONE' => array('StringTrim','StripTags'),
		        // 'auto_monthlyfee' => array('StringTrim','StripTags'),
		        'ACBENEF' => array('StringTrim','StripTags')
		        );

		    $validators = array(

		        'NRC'      => array('NotEmpty',
		            'messages' => array(
		                $this->language->_('Can not be empty')
		            )
		        ),
		        'PHONE'      => array('NotEmpty',
		            'messages' => array(
		                $this->language->_('Can not be empty')
		            )
		        ),
		        'ACBENEF'      => array('NotEmpty',
		            'messages' => array(
		                $this->language->_('Can not be empty')
		            )
		        ),

		    );




			//$benefType = $this->_getParam('benefType');
			//setelah disamakan dgn FSD
			$benefType = '4';//$this->_getParam('benefType'); //kalo dua ONLINE harus panggil fundtransferinquiry, kalo 1 skn rtgs langsung lewat

				$NRC   = $this->_request->getParam('NRC');
				$PHONE   = $this->_request->getParam('PHONE');
				$ACBENEF = $this->_request->getParam('ACBENEF');

// 			$this->view->CURR_CODE = $CURR_CODE;


	//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//

				$fPHONE   = $filter->filter($PHONE  , "PHONE");
				$fNRC = $filter->filter($NRC , "NRC");
				$fACBENEF = $filter->filter($ACBENEF, "ACBENEF");


				$selectcheck = $this->_db->select()
				->from(array('C' => 'M_BENEFICIARY'));
				$selectcheck->where("CUST_ID = ?",$this->_custIdLogin);
				$selectcheck->where("BENEFICIARY_ID_NUMBER = ?",$fNRC);
				$selectcheck->where("BENEFICIARY_PHONE = ?",$fPHONE);
// 				 echo $selectcheck;die;
				$checkbene 					= $this->_db->fetchRow($selectcheck);
			//validasi token -- end
     			// $zf_filter_input =/ array();
			// if(empty($checkbene)){
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			// }


			if($zf_filter_input->isValid())
			{
// 			    die;
// 				$content = array(
// 								'BRANCH_NAME' 	 => $zf_filter_input->branch_name,
// 								'BANK_ADDRESS' 	 => $zf_filter_input->branch_address,
//             				    'CITY_NAME' 	 => $zf_filter_input->city_name,
//             				    'REGION_NAME' 	 => $zf_filter_input->region_name,
//             				    'CONTACT' 	 => $zf_filter_input->contact
// 						       );
// 				echo 'here';die;
				if(empty($checkbene)){
				$privibenelinkage = $this->view->hasPrivilege('BLBA');
					$content = array(
											'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
											'USER_ID_LOGIN' 			=> $this->_userIdLogin,
											'BENEFICIARY_ID_NUMBER' 		=> $zf_filter_input->NRC,
											'BENEFICIARY_PHONE' 		=> $zf_filter_input->PHONE,
											'BENEFICIARY_NAME' 			=> $zf_filter_input->ACBENEF,
											'_beneLinkage'				=> $privibenelinkage
									);




							$content['BENEFICIARY_TYPE'] = 4; // 2 = domestic ( SKN/RTGS )
							//$content['CLR_CODE'] = $CLR_CODE;
							$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
							$sessionNamespace->mode = 'Add';
							$sessionNamespace->content = $content;
							$this->_redirect('/predefinedbeneficiary/localremittanceacct/confirm');
				}else{
					$this->view->succes = false;
					$this->view->error = true;
               		$this->view->report_msg = $this->language->_('Beneficiary NRC/Phone already exist');
				}

			}
			else
			{

				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        // print_r($errorArray);die;
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}

		$this->view->NRC = $NRC;
		$this->view->PHONE = $PHONE;
		$this->view->ACBENEF = $ACBENEF;

		Application_Helper_General::writeLog('BADA','Add Local Remittance Destination Account');

	}

	public function confirmAction()
	{

		$data = $this->_db->fetchRow(
			$this->_db->select()
			->from(array('C' => 'M_USER'))
			->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
			->limit(1)
		);

		$this->view->userId				= $data['USER_ID'];

		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		$this->view->PHONE = $content['BENEFICIARY_PHONE'];
		$this->view->ACBENEF = $content['BENEFICIARY_NAME'];
		$this->view->NRC = $content['BENEFICIARY_ID_NUMBER'];
// 		print_r($content);die;
		$mode = $sessionNamespace->mode;
		$this->view->mode = $mode;


// 		die;
//  		$this->fillParams(null,$content['PHONE'],$content['ACBENEF'],$content['NRC']);
		if($this->_request->isPost() )
		{
			if($this->_getParam('submit1')==$this->language->_('Back'))
			{
				if($mode=='Add'){
					$this->_redirect('/predefinedbeneficiary/localremittanceacct/addbeneficiary/isback/1');
				}elseif ($mode=='Edit'){
					$this->_redirect('/predefinedbeneficiary/localremittanceacct/editbeneficiary/benef_id/'.$content['BENEFICIARY_ID'].'/isback/1');
				}
			}
			else
			{
				try
				{
					//-----insert benef--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					$add = $Beneficiary->add($content);
					Application_Helper_General::writeLog('BADA','Add Destination Account '.$content['BENEFICIARY_ACCOUNT']);
					$this->_db->commit();
					unset($_SESSION['beneficiaryAccountAddEdit']);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
				}
			}
		}
		Application_Helper_General::writeLog('BADA','Confirm Destination Account');
	}

	public function editemailAction()
	{
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;
		$benef_id = $this->_getParam('benef_id');
		$param['cust_id'] = $this->_custIdLogin;
		//echo $benef_id."asfd"; die;

		if(!$this->_request->isPost())
		{
			//$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
// 					$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					// 					print_r($temp);;die;
// 					$this->view->CATEGORY_NAME_LLD		  		= $category_name['0']['sm_text1'];


					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];

					$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
					$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
					$this->view->BANK_CITY = $resultdata['BANK_CITY'];
					$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];
					$this->view->SWIFT_CODE = $resultdata['SWIFT_CODE'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];
					$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					$arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					$this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];

					$CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
					$arr 				= $modelCity->getCityCode($CITY_CODEGet);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
			}
			else
			{
			    $error_remark = 'Beneficiary ID does not exist.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			}
		}
		else
		{
			$filters = array( 	'BENEFICIARY_ID' => array('StringTrim','StripTags'),
								'ACBENEF_EMAIL' 	=> array('StringTrim','StripTags'));

			$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																$this->language->_('Error').': '.$this->language->_('Beneficiary ID cannot be left blank').'.',
																$this->language->_('Error').': '.$this->language->_('Wrong Format Beneficiary ID').'.')
														),
								'ACBENEF_EMAIL' => array(	//new SGO_Validate_EmailAddress(),
															'allowEmpty' => true,
															//'messages' => array(
															//	'Error: Wrong Format Email Address of {$email}. Please correct it.')
														));

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				$error = false;

				if (trim($zf_filter_input->ACBENEF_EMAIL) != "")
				{
					$arr_email = explode(";",$zf_filter_input->ACBENEF_EMAIL);
					foreach ($arr_email as $value)
					{
						if (Zend_Validate::is(trim($value), 'EmailAddress') == false)
						{
							$error = true;
							break;
						}
					}
				}

				if (!$error)
				{
					try
					{
						//-----edit email--------------
						$this->_db->beginTransaction();

						$Beneficiary = new Beneficiary();
						//echo $benef_id; die;
						$Beneficiary->editEmail($zf_filter_input->BENEFICIARY_ID,$zf_filter_input->ACBENEF_EMAIL);

						Application_Helper_General::writeLog('BEDA','Edit Email Destination Account '.$this->_getParam('ACBENEF'));

						$this->_db->commit();
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
					}
				}
				else
				{
					$this->view->error = true;
					$param['benef_id'] = $zf_filter_input->BENEFICIARY_ID;
					$temp = $model->getBeneficiaries($param);
					$resultdata = $temp[0];

					  if($resultdata)
					  {
							$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
							$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
							$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
							$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
							$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
							$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
							$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
							$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
							$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
							$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
							$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

							$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
							$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
							$this->view->BANK_CITY = $resultdata['BANK_CITY'];
							$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
							$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
							$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];

							$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
							$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

							$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
							$CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
							$arr 				= $modelCity->getCityCode($CITY_CODEGet);

							// 9. Create LLD string
							$settings 			= new Application_Settings();
							$LLD_array 			= array();
							$LLD_DESC_arrayCat 	= array();
							$lldTypeArr  		= $settings->getLLDDOMType();

							if (!empty($LLD_CATEGORY))
							{
								$lldCategoryArr  	= $settings->getLLDDOMCategory();
								$LLD_array["CT"] 	= $LLD_CATEGORY;
								$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
							}

							$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
							$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
							$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
							$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
					   }
					$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
					$docErr = $this->language->_('Error').': '.$this->language->_('Invalid Format - Email Address').'.';
					$this->view->report_msg = $docErr;
				}
			}
			else
			{
				$this->view->error = true;
				$param['benef_id'] = $zf_filter_input->BENEFICIARY_ID;
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				  if($resultdata)
				  {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

						$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
						$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

						$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
						$CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
						$arr 				= $modelCity->getCityCode($CITY_CODEGet);

						// 9. Create LLD string
						$settings 			= new Application_Settings();
						$LLD_array 			= array();
						$LLD_DESC_arrayCat 	= array();
						$lldTypeArr  		= $settings->getLLDDOMType();

						if (!empty($LLD_CATEGORY))
						{
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_array["CT"] 	= $LLD_CATEGORY;
							$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
						}

						$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
						$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
						$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				   }
				$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}

			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}
			}
		}
		Application_Helper_General::writeLog('BEDA','Edit Email Destination Account ');

	}



	public function detailAction()
	{
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;
		$benef_id = $this->_getParam('benef_id');
		$param['cust_id'] = $this->_custIdLogin;
		//echo $benef_id."asfd"; die;

		if(!$this->_request->isPost())
		{
			//$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
				//print_r($param);die;
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->SWIFT_CODE  		= $resultdata['SWIFT_CODE'];


					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					// 					$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					// 					print_r($temp);;die;
					// 					$this->view->CATEGORY_NAME_LLD		  		= $category_name['0']['sm_text1'];


					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];

					$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
					$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
					$this->view->BANK_CITY = $resultdata['BANK_CITY'];
					$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					$arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					$this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];
					$CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
					$arr 				= $modelCity->getCityCode($CITY_CODEGet);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
			}
			else
			{
				$error_remark = 'Beneficiary ID does not exist.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			}
		}

		Application_Helper_General::writeLog('BEDA','Edit Email Destination Account ');

	}

	public function deleteAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;
		$param['cust_id'] = $this->_custIdLogin;
		if(!$this->_request->isPost())
		{
			$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;

			if($benef_id)
			{
		//print_r($param);die;
				$temp = $model->getBeneficiaries($param);
				$resultdata =  $temp[0];
// 				echo "<pre>";
// 				print_r($resultdata);die;
			  if($resultdata)
			  {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->ACBENEF_NAME  		= $resultdata['BENEFICIARY_NAME'];
					$this->view->ACBENEF_PHONE  		= $resultdata['BENEFICIARY_PHONE'];

					 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					 					$this->view->NATIONALITY		  		= $resultdata['BENEFICIARY_CITIZENSHIP'];

					 					$CATEGORY = $resultdata['BENEFICIARY_CATEGORY'];
										if($CATEGORY == '5'){
											$CATEGORY_CODE = 'Company';
										}
										elseif($CATEGORY == '1'){
											$CATEGORY_CODE = 'Individual';
										}
										elseif($CATEGORY == '2'){
											$CATEGORY_CODE = 'Government';
										}
										elseif($CATEGORY == '3'){
											$CATEGORY_CODE = 'Bank';
										}
										elseif($CATEGORY == '4'){
											$CATEGORY_CODE = 'Non Bank Financial Institution';
										}
										elseif($CATEGORY == '6'){
											$CATEGORY_CODE = 'Other';
										}
					//$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					 					//print_r($resultdata['BENEFICIARY_CATEGORY']);;die;
					 					$this->view->CATEGORY_NAME_LLD		  		= $CATEGORY_CODE;

					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];

					$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
					$this->view->BENEFICIARY_BANK_CODE = $resultdata['SWIFT_CODE'];

					$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					$CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
					$arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					$this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];
					$this->view->SWIFT_CODE =   $resultdata['SWIFT_CODE'];

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
			   }
			}
			else
			{
			   $error_remark = $this->language->_('Error').': '.$this->language->_('Beneficiary ID cannot be left blank').'.';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			}
		}
		else
		{
			$filters = array(  	'BENEFICIARY_ID' => array('StringTrim','StripTags'),
								'STATUS' => array('StringTrim','StripTags'));

			$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																$this->language->_('Error').': '.$this->language->_('Beneficiary ID cannot be left blank').'.',
																$this->language->_('Error').': '.$this->language->_('Wrong Format Beneficiary ID').'.')
														),
								'STATUS' 		=> array(	'NotEmpty',
															'Digits',
															'messages' => array(
																$this->language->_('Error').': '.$this->language->_('Status cannot be left blank').'.',
																$this->language->_('Error').': '.$this->language->_('Wrong Format Status').'.')
														));

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				try
				{
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					//$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
					//print_r($zf_filter_input->STATUS);die;
					if($zf_filter_input->STATUS)
						$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
					else
						$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
					$this->_db->commit();
					Application_Helper_General::writeLog('BEDA','Delete Destination Account '.$this->_getParam('ACBENEF'));
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
				}
			}
			else
			{
				$this->view->error = true;
				$param['benef_id'] = $zf_filter_input->BENEFICIARY_ID;
				$temp = $model->getBeneficiaries($param);
				$resultdata =  $temp[0];
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];
				}
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}
		}

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}
    	Application_Helper_General::writeLog('BEDA','Delete Destination Account ');
	}

	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID');
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS');
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->ACBENEF_BANKNAME = ($zf_filter_input->isValid('ACBENEF_BANKNAME')) ? $zf_filter_input->ACBENEF_BANKNAME : $this->_getParam('ACBENEF_BANKNAME');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ADDRESS = ($zf_filter_input->isValid('ADDRESS')) ? $zf_filter_input->ADDRESS : $this->_getParam('ADDRESS');
		$this->view->CITIZENSHIP = ($zf_filter_input->isValid('CITIZENSHIP')) ? $zf_filter_input->CITIZENSHIP : $this->_getParam('CITIZENSHIP');
		$this->view->NATIONALITY = ($zf_filter_input->isValid('NATIONALITY')) ? $zf_filter_input->NATIONALITY : $this->_getParam('NATIONALITY');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
		$this->view->BANK_NAME = ($zf_filter_input->isValid('BANK_NAME')) ? $zf_filter_input->BANK_NAME : $this->_getParam('BANK_NAME');
		$this->view->CLR_CODE = ($zf_filter_input->isValid('CLR_CODE')) ? $zf_filter_input->CLR_CODE : $this->_getParam('CLR_CODE');
		$this->view->BANK_CITY = ($zf_filter_input->isValid('BANK_CITY')) ? $zf_filter_input->BANK_CITY : $this->_getParam('BANK_CITY');
	}

	private function fillParams($benefType,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_ID=null,$BENEFICIARY_TYPE,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE)
	{
		$modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
		$arr 					= $modelCity->getCityCode($CITY_CODEGet);

		// 9. Create LLD string
		$settings 			= new Application_Settings();
		$LLD_array 			= array();
		$LLD_DESC_arrayCat 	= array();
		$lldTypeArr  		= $settings->getLLDDOMType();

		if (!empty($LLD_CATEGORY))
		{
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$LLD_array["CT"] 	= $LLD_CATEGORY;
			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
		}

		if (!empty($LLD_BENEIDENTIF))
		{
			$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
			$LLD_array["CT"] 	= $LLD_BENEIDENTIF;
			$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$LLD_BENEIDENTIF];
		}

		if($BENEFICIARY_ID)
		$this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		//$this->view->benefType = $benefType;
		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ADDRESS = $ACBENEF_ADDRESS;
		$this->view->CITIZENSHIP = $ACBENEF_RESIDENT;
		$this->view->NATIONALITY = $ACBENEF_CITIZENSHIP;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
		$this->view->BANK_NAME = $BANK_NAME;
		$this->view->CLR_CODE = $CLR_CODE;
		$this->view->BANK_CITY = $BANK_CITY;
		$this->view->BENEFICIARY_TYPE = $BENEFICIARY_TYPE;

		$this->view->LLD_CATEGORY = $LLD_CATEGORY;;
		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
		$this->view->CITY_CODE = $CITY_CODE;

		$this->view->CITY_NAME = $arr[0]['CITY_NAME'];
		$this->view->CATEGORY_NAME_LLD = $LLD_CATEGORY_POST;
		$this->view->BENEIDENTIF_NAME_LLD = $LLD_BENEIDENTIF_POST;


	}

	public function suggestdeleteAction()
	{
		$filters = array( 'BENEFICIARY_ID' => array('StringTrim','StripTags'));
		$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
														'Digits',
														'messages' => array(
																$this->language->_('Error').': '.$this->language->_('Beneficiary ID cannot be left blank').'.',
																$this->language->_('Error').': '.$this->language->_('Wrong Format Beneficiary ID').'.')
													));

		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
		if($zf_filter_input->isValid())
		{
			$Beneficiary = new Beneficiary();
			$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
		}
		$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
	}
}
