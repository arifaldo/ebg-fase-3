<?php
require_once 'Zend/Controller/Action.php';

class domesticcharges_IndexController extends Application_Main
{
	public function indexAction()
	{
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
	
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()->distinct()
					->from(array('M_CUSTOMER'),array('CUST_ID'))
					->where("CUST_STATUS != '3'")
					->order('CUST_ID ASC')
				 	-> query() ->fetchAll();
		//Zend_Debug::dump($status); die;
		$this->view->var=$select;

		/*$select3 = $this->_db->select()
				->from(array('B' => 'M_CHARGES_WITHIN'),array('*'));
		$coba = $this->_db->fetchAll($select3);
		Zend_Debug::dump($select3); die;*/

		$list = array(	''				=>	'--- '.$this->language->_('Any Value').' ---',
						'Enabled'		=>	$this->language->_('Enabled'),
						'Disabled' 		=> 	$this->language->_('Disabled'));
		$this->view->optionlist = $list;

		$ceklist = array(	'0'		=>	$this->language->_('Disabled'),
							'1' 	=> 	$this->language->_('Enabled'));

		$companyCode = $this->language->_('Company Code');
		$companyName = $this->language->_('Company');
		$company_name = $this->language->_('CCY');
		$monthlyFee = $this->language->_('RTGS');
		$suggestedDate = $this->language->_('Domestic Online');
		$suggester = $this->language->_('Charge Account');

		$fields = array	(
							// 'Company Code'  			=> array	(
							// 										'field' => 'B.CUST_ID',
							// 										'label' => $companyCode,
							// 										'sortable' => true
							// 									),
							'Company'  			=> array	(
																	'field' => 'COMPANY',
																	'label' => $companyName,
																	'sortable' => true
																),
							'CompanyName'  			=> array	(
																	'field' => 'CHARGES_CCY',
																	'label' => $company_name,
																	'sortable' => true
																),
							'Charges'  					=> array	(
																	'field' => 'SKN',
																	'label' => $this->language->_('SKN'),
																	'sortable' => true
																),
							'Monthly Fee'  		=> array	(
																	'field' => 'RTGS',
																	'label' => $monthlyFee,
																	'sortable' => true
																),
							'Suggest Date'  		=> array	(
																	'field' => 'DOM',
																	'label' => $suggestedDate,
																	'sortable' => true
																),
							'Suggestor'  		=> array	(
																	'field' => 'CHARGES_NO',
																	'label' => $suggester,
																	'sortable' => true
																),
							'suggested'  		=> array	(
																	'field' => 'CHARGES_SUGGESTED',
																	'label' => $this->language->_('Last Suggested Date'),
																	'sortable' => true
																),
							'suggestedby'  		=> array	(
																	'field' => 'CHARGES_SUGGESTEDBY',
																	'label' => $this->language->_('Last Suggested By'),
																	'sortable' => true
																),
							'approved'  		=> array	(
																	'field' => 'CHARGES_APPROVED',
																	'label' => $this->language->_('Last Approved Date'),
																	'sortable' => true
																),
							'approvedby'  		=> array	(
																	'field' => 'CHARGES_APPROVEDBY',
																	'label' => $this->language->_('Last Approved By'),
																	'sortable' => true
																),
						);

		$filterlist = array("CUST_ID","MONTHLY_FEE_STATUS","CUST_NAME","SUGGESTOR");
		$this->view->filterlist = $filterlist;

		$filterArr = array('filter' 			=> array('StripTags','StringTrim'),
	                       'custid'    			=> array('StripTags','StringTrim'),
	                       'custname'  			=> array('StripTags','StringTrim','StringToUpper'),
						   'suggestor'  		=> array('StripTags','StringTrim','StringToUpper'),
						   'chargeStatus'  		=> array('StripTags','StringTrim'),
						   'monthlyfeeStatus'  	=> array('StripTags','StringTrim'),
						   'fDateFrom'  		=> array('StripTags','StringTrim'),
						   'fDateTo'  			=> array('StripTags','StringTrim'),
	                      );

	    $validator = array('filter' 			=> array(),
	                       'custid'    			=> array(),
	                       'custname'  			=> array(),
						   'suggestor'  		=> array(),
						   'chargeStatus'  		=> array(),
						   'monthlyfeeStatus'  	=> array(),
						   'fDateFrom'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo'  			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );


	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $custid = html_entity_decode($zf_filter->getEscaped('custid'));
		$custname = html_entity_decode($zf_filter->getEscaped('custname'));
		$suggestor = html_entity_decode($zf_filter->getEscaped('suggestor'));
		$chargeStatus = html_entity_decode($zf_filter->getEscaped('chargeStatus'));
		$monthlyfeeStatus = html_entity_decode($zf_filter->getEscaped('monthlyfeeStatus'));
		$datefrom = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$dateto = html_entity_decode($zf_filter->getEscaped('fDateTo'));
		//Zend_Debug::dump($this->_masterglobalstatus); die;

		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');

		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		/*if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}*/

		$chargeStatusarr = "(CASE B.CUST_CHARGES_STATUS ";
  		foreach($ceklist as $key=>$val)
  		{
   			$chargeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$chargeStatusarr .= " END)";

  		$adminfeeStatusarr = "(CASE B.CUST_MONTHLYFEE_STATUS ";
  		foreach($ceklist as $key=>$val)
  		{
   			$adminfeeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$adminfeeStatusarr .= " END)";

		$select2 = $this->_db->select()->distinct()
						->from(array('B' => 'M_CUSTOMER'),array('B.CUST_ID',
																'B.CUST_NAME',
																'CUST_CHARGES_STATUS' => $chargeStatusarr,
																'CUST_MONTHLYFEE_STATUS' => $adminfeeStatusarr,
																'B.CUST_SUGGESTED',
																'COMPANY'	=> new Zend_Db_Expr("CONCAT(B.CUST_NAME , ' (' , B.CUST_ID , ')  ' )"),
																'B.CUST_SUGGESTEDBY'))
						->joinleft(array('A' => 'M_CHARGES_OTHER'), 'A.CUST_ID = B.CUST_ID ',array('A.CHARGES_TYPE','A.CHARGES_NO','A.CHARGES_AMT','A.CHARGES_CCY'))
						->where("CUST_STATUS != '3'");
		$arr = $this->_db->fetchAll($select2);

		//if($filter == null || $filter == 'Set Filter')
		if($filter == true)
		{
		 $this->view->fDateTo    = $dateto;
		 $this->view->fDateFrom  = $datefrom;
		//Zend_Debug::dump($custid); die;

		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);
	            }

	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
					//Zend_Debug::dump($dateto); die;
	            }

		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) >= ".$this->_db->quote($datefrom));

	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) <= ".$this->_db->quote($dateto));

	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.CUST_SUGGESTED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}

		if($filter == true)
		{
			if($chargeStatus)
			{
				$this->view->chargeStatus = $chargeStatus;
				if($chargeStatus == "Enabled")
				{
					$cekchargeStatus = "1";
				}
				if($chargeStatus == "Disabled")
				{
					$cekchargeStatus = "0";
				}
				$select2->where("B.CUST_CHARGES_STATUS LIKE ".$this->_db->quote($cekchargeStatus));
			}

			if($monthlyfeeStatus)
			{
				$this->view->adminfeeStatus = $monthlyfeeStatus;
				if($monthlyfeeStatus == "Enabled")
				{
					$cekadminfeeStatus = "1";
				}
				if($monthlyfeeStatus == "Disabled")
				{
					$cekadminfeeStatus = "0";
				}
				$select2->where("CUST_MONTHLYFEE_STATUS LIKE ".$this->_db->quote($cekadminfeeStatus));
			}

		    if($custid)
		    {
	       		$this->view->cekcustid = $custid;
	       		$select2->where("B.CUST_ID LIKE ".$this->_db->quote($custid));
		    }

			if($custname)
			{
	       		$this->view->custname = $custname;
	       		$select2->where("B.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));
			}

			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
	       		$select2->where("B.CUST_SUGGESTEDBY LIKE ".$this->_db->quote('%'.$suggestor.'%'));
		    }
		}



		//$select2->order($sortBy.' '.$sortDir);
		$select2->order($sortBy.' '.$sortDir);


		$select3 = $select2;
		
		// echo $select2;die;
		$result2 = $select3->query()->FetchAll();
		$this->view->result2 = $result2;
		$select3->group('B.CUST_ID');
    	$this->paging($select3,30);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	$header = Application_Helper_Array::simpleArray($fields, "label");
    	$arr = $this->_db->fetchAll($select2);

    	if($this->_request->getParam('print') == 1){

    		foreach($arr as $key=>$row)
			{
				$arr[$key]['CUST_NAME'] = $row['CUST_NAME'].' ('.$row['CUST_ID'].')';

                if($row['CHARGES_TYPE'] == '1'){

                  $arr[$key]['SKN'] = Application_Helper_General::displayMoney($row['CHARGES_AMT']);  
                  $arr[$key]['CHARGES_ACCT_NO'] = $row['CHARGES_ACCT_NO']; 
                  $arr[$key]['CHARGES_CCY'] = $row['CHARGES_CCY'];  
                }else{
                  $arr[$key]['SKN'] = '0.00';
                }              
              
                if($row['CHARGES_TYPE'] == '2'){
                  $arr[$key]['RTGS'] = Application_Helper_General::displayMoney($row['CHARGES_AMT']);  
                  $arr[$key]['CHARGES_ACCT_NO'] = $row['CHARGES_ACCT_NO']; 
                  $arr[$key]['CHARGES_CCY'] = $row['CHARGES_CCY'];  
                }else{
                  $arr[$key]['RTGS'] = '0.00';
                }
             
                if($row['CHARGES_TYPE'] == '8'){
                  $arr[$key]['DOM'] = Application_Helper_General::displayMoney($row['CHARGES_AMT']);  
                  $arr[$key]['CHARGES_ACCT_NO'] = $row['CHARGES_ACCT_NO']; 
                  $arr[$key]['CHARGES_CCY'] = $row['CHARGES_CCY'];  
                }else{
                  $arr[$key]['DOM'] = '0.00';
                }
              
				
			}

			// echo "<pre>";
			// var_dump($arr);
			// die();

			$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Payment Report', 'data_header' => $fields));
		}

    	if($csv || $pdf)
    	{
    		
    		foreach($arr as $key=>$value)
			{
				$arr[$key]["CUST_SUGGESTED"] = Application_Helper_General::convertDate($value["CUST_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}

	    	if($csv)
			{
				Application_Helper_General::writeLog('CHLS','Download CSV Company Charges List');
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}

			if($pdf)
			{
				Application_Helper_General::writeLog('CHLS','Download PDF Company Charges List');
				$this->_helper->download->pdf($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('CHLS','View Company Charges List');
    	}
	}
}
