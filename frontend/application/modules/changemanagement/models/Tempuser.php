<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';

class Changemanagement_Model_Tempuser extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'CUS';
	// const actual_link_fo = 'http://espay.id:2019';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null){
	
	 
		//user password = autogenerate
		//perlu insert ke T_REQ_PIN_MAILER
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
						  // print_r($user);die;
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }
        
		//Validation, user can only be activated if this user's customer is active
		$customer = $this->dbObj->select()
							  	 ->from('M_CUSTOMER')
							  	 ->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	 ->query()
							  	 ->fetch(Zend_Db::FETCH_ASSOC);

		if($customer['CUST_STATUS'] == 'I') {
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		
		
		//--------------------------------INSERT TO TABLE M_USER----------------------------------------------------------
		$insertArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['USER_CREATED']     = new Zend_Db_Expr('now()');
		$insertArr['USER_CREATEDBY']   = $actor;
		$insertArr['USER_UPDATED']     = new Zend_Db_Expr('now()');
		$insertArr['USER_UPDATEDBY']   = $actor;
		$insertArr['USER_ISNEW']       = 1;
		$insertArr['USER_DATEPASS']     = new Zend_Db_Expr('now() + INTERVAL 1 DAY');

		$str=rand(); 
		$rand = md5($str); 
		$insertArr['USER_CODE']       	= $rand;
		// print_r($insertArr);die;
		try{
			$userins = $this->dbObj->insert('M_USER',$insertArr);	
		}catch(Exception $e) 
  		{
  			// print_r($e);die;
		}
		// print_r($userins);die;
		if(!(boolean)$userins) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}	
		//-------------------------------END INSERT TO TABLE M_USER-------------------------------------------------------
		
		
		//----------------jika new user pertama x, maka ada tambahan yaitu : request change password dan update requeset unlock user----------------------
		$customerUser = new CustomerUser($user['CUST_ID'],$user['USER_ID']);
	    // $customerUser->requestUnlockResetPassword(2);    //2 = print
		// $customerUser->approveRequestResetPassword();
		//-----------END jika new user pertama x, maka ada tambahan yaitu : request change password dan update requeset unlock user-----------------------
		
		
		//--------------------------------INSERT TO TABLE M_PRIVILEGE-----------------------------------------------------
	    $privilege = $this->dbObj->select()
						          ->from('TEMP_FPRIVI_USER')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		if(count($privilege) > 0)
		{
		   foreach($privilege as $privi)
		   {
		      $insertPrivi = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
              $this->dbObj->insert('M_FPRIVI_USER',$insertPrivi);


		   }
        }


        $custacct = $this->dbObj->select()
						          ->from('TEMP_CUSTOMER_ACCT')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		if(count($custacct) > 0)
		{
		   foreach($custacct as $privi)
		   {
		   	// var_dump($privi);
		   		$customeracct = $this->dbObj->select()
							  	 ->from('M_CUSTOMER_ACCT')
							  	 ->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	 ->where('ACCT_NO = ?',(string)$privi['ACCT_NO']);
							  	 // ->query()
							  	 // ->fetch(Zend_Db::FETCH_ASSOC);
							  	 // echo $customeracct;echo "<br>";
				if(empty($customeracct)){
				      $insertPrivi = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		              $this->dbObj->insert('M_CUSTOMER_ACCT',$insertPrivi);
              	}
		   }
        }


        $makerlimit = $this->dbObj->select()
						          ->from('TEMP_MAKERLIMIT')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
								  
		if(count($makerlimit) > 0)
		{
		   foreach($makerlimit as $privi)
		   {
		      $insertMaker = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$insertMaker['CUST_ID']     = $user['CUST_ID'];
			  	$insertMaker['CREATED']     = new Zend_Db_Expr('now()');
				$insertMaker['UPDATEDBY']   = $actor;
				$insertMaker['UPDATED']     = new Zend_Db_Expr('now()');
				$insertMaker['CREATEDY']   = $actor;
				
              $this->dbObj->insert('M_MAKERLIMIT',$insertMaker);
			  //var_dump($insertMaker);
		   }
        }
		//die;


        $dailylimit = $this->dbObj->select()
						          ->from('TEMP_DAILYLIMIT')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		if(count($dailylimit) > 0)
		{
		   foreach($dailylimit as $privi)
		   {
		      $insertDaily = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
			 
			  $insertDaily['CUST_ID']     = $user['CUST_ID'];
			  $insertDaily['CREATED']     = new Zend_Db_Expr('now()');
			  $insertDaily['CREATEDBY']   = $actor;
			  $insertDaily['UPDATEDBY']   = $actor;
			  $insertDaily['UPDATED']     = new Zend_Db_Expr('now()');
              $this->dbObj->insert('M_DAILYLIMIT',$insertDaily);
		   }
        }
		//-------------------------------END INSERT TO TABLE M_PRIVILEGE-------------------------------------------------
		
		
		
		$debitcard = $this->dbObj->select()
						          ->from('TEMP_USER_DEBIT')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		if(count($debitcard) > 0)
		{
		   foreach($debitcard as $privi)
		   {
		      $insertDebit = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
			 
			  
              $this->dbObj->insert('M_USER_DEBIT',$insertDebit);
		   }
        }
		
        //--setiap kita buat user baru, maka akan dibuat jg user dailylimit baru, sesuai tabel M_MINAMT_CCY yang aktif---
        //--------------------------------INSERT TO TABLE M_DAILYLIMIT---------------------------------------------------
// 	    $minamtCcy = $this->dbObj->select()
// 						          ->from('M_MINAMT_CCY',array('CCY_ID'))
// 						          //->where('CCY_STATUS ='.$this->dbObj->quote('A'))
// 						          ->query()->fetchAll();
        
// 		if(!empty($minamtCcy))
//         {
//              $insertArrDailylimit['SUGGESTED']   = $user['USER_SUGGESTED'];
// 	         $insertArrDailylimit['SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
// 		   	 $insertArrDailylimit['CREATED']     = new Zend_Db_Expr('now()');
// 		   	 $insertArrDailylimit['CREATEDBY']   = $actor;
// 		   	 $insertArrDailylimit['UPDATED']     = new Zend_Db_Expr('now()');
// 		   	 $insertArrDailylimit['UPDATEDBY']   = $actor;
		
// 		     $insertArrDailylimit['DAILYLIMIT_STATUS']  = 1;
// 		   	 $insertArrDailylimit['USER_LOGIN']         = $user['USER_ID'];
// 		   	 $insertArrDailylimit['CUST_ID']            = $user['CUST_ID'];
// // 		   	 $insertArrDailylimit['DAILYLIMIT']         = '0';
//              $insertArrDailylimit['DAILYLIMIT']         = '9999999999999.99';
		   	  
		   	 
//              foreach($minamtCcy as $ccyId)
//              {
// 	       		$insertArrDailylimit['CCY_ID'] = $ccyId['CCY_ID'];
		        
// 	       		$dailyLimitInsert = $this->dbObj->insert('M_DAILYLIMIT',$insertArrDailylimit);
		        
// 	       		/*Zend_Debug::dump($insertArrDailylimit); 
// 						          die;*/
	       		
// 	       	    if(!(boolean)$dailyLimitInsert) 
// 		  		{
// 			   		$this->_errorCode = '82';
// 			   		$this->_errorMsg = 'Query failed(User Daily Limit)';
// 			   		return false;
// 		   		}
//              }
             
            
//          }
         //------------------------------END INSERT TO TABLE M_DAILYLIMIT-------------------------------------------------
        
        
        
        $subject = "NEW USER ACCOUNT INFORMATION";
        $setting = new Settings();
        $templateEmailMasterBankName = $setting->getSetting('master_bank_name');
        $templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
        $templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
        $templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
        $templateEmailMasterBankWapp = $setting->getSetting('master_bank_wapp');
        $template = $setting->getSetting('femailtemplate_newuser');
        $url_fo = $setting->getSetting('url_fo');
        // 		$subject = "CMD - New User";

        //   $data = $this->dbObj->select()
        //  ->from('TEMP_USER')
        //  ->where('CHANGES_ID = ?',$this->_changeId)
        //  ->query()
        //	->fetch(Zend_Db::FETCH_ASSOC);
        //       		print_r($user);die;
        $datacreated = $this->dbObj->select()
        ->from('M_USER')
        ->where('USER_ID = ?',$user['USER_SUGGESTEDBY'])
        ->where('CUST_ID = ?',$user['CUST_ID'])
        ->query()
        ->fetch(Zend_Db::FETCH_ASSOC);


        $date = date('Y-m-d h:i:s', strtotime("+1 days"));
        $template = str_ireplace('[[user_fullname]]',$user['USER_FULLNAME'],$template);
        $template = str_ireplace('[[comp_accid]]',$user['CUST_ID'],$template);
        
        // $template = str_ireplace('[[buser_name]]',$user['USER_FULLNAME'],$template);
        $template = str_ireplace('[[user_login]]',$user['USER_ID'],$template);
        // $template = str_ireplace('[[user_email]]',$user['USER_EMAIL'],$template);
        $template = str_ireplace('[[user_status]]','Approved',$template);
        date_default_timezone_set("Asia/Jakarta");
		$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
        $template = str_ireplace('[[exp_date]]',$datenow,$template);


        $template = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$template);
        $template = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$template);
        $template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);
        $template = str_ireplace('[[master_bank_app_name]]',$templateEmailMasterBankAppName,$template);
        $template = str_ireplace('[[master_bank_wapp]]',$templateEmailMasterBankWapp,$template);
		// define("ENCRYPTION_KEY", "!@#$%^&*");
		// $string = "This is the original data string!";

		$cust = ( $this->sslEnc(  $user['CUST_ID'] ) );
		$userid = ( $this->sslEnc(  $user['USER_ID'] ) );
		$newPassword = $url_fo.'/pass/index?safetycheck=&code='.urldecode($rand).'&cust_id='.urlencode($cust).'&user_id='.urlencode($userid).'&code='.urlencode($rand);
		// echo "<br />";
		// echo $decrypted = decrypt($encrypted, "permataNet92");

        // $linkpass = '<a href="/pass/index/index/cust/'.$crypcust_id.'/user/'.$crypuser_id.'">Verification Password</a>';
        // echo $linkpass;die;
        $template = str_ireplace('[[confirm_link]]',$newPassword,$template);
        
        $email = $user['USER_EMAIL'];
                		// print_r($template);die;
        // echo $template;die;
        // var_dump($user);
        // var_dump($email);
        // var_dump($subject);
        // echo $template;die;
		if($email!=''){
		        $result = Application_Helper_Email::sendEmail($email,$subject,$template);
		}



        $deleteChanges = $this->deleteNew();
		if(!$deleteChanges) return false;
		
			
		//integrate ke core (TOKEN)			
        //ADD TOKEN
		if($user['TOKEN_ID'])
        {
			$param = array();
			$param['TokenSerial'] 	= $user['TOKEN_ID'];
			$param['UserName'] 		= $user['USER_FULLNAME'];
			
			$param['CustAddress'] 	= $customer['CUST_ADDRESS'];
			$param['CustCity'] 		= $customer['CUST_CITY'];
			$param['CustName'] 		= $customer['CUST_NAME'];
			$param['CustZipCode'] 	= $customer['CUST_ZIP'];
			$data =  new Service_Token($user['CUST_ID'],$user['USER_ID']);
	
	        $result = $data->addPairToken($param);	
	        //$result['ResponseCode'] = '00';
	        
	        if($result['ResponseCode'] != '00')  //00 = success
	        {
	            $this->_errorMsg = $result['ResponseDesc'];
	            $this->_errorCode = '1000';
	             
	            return false;
	        }
	        //END integrate ke core (TOKEN)		
        }

      	// var_dump('here');die;
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  	->from('TEMP_USER')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }

		//Validation, user can only be activated if this user's customer is active
		$customer = $this->dbObj->select()
							  	 ->from('M_CUSTOMER')
							  	 ->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	 ->query()
							  	 ->fetch(Zend_Db::FETCH_ASSOC);

		if($customer['CUST_STATUS'] == 'I') {
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		
        //query from M_USER
		$m_user = $this->dbObj->select()
						  	  ->from('M_USER')
						  	  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  	  ->where('CUST_ID = ?',(string)$user['CUST_ID'])
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($m_user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }		
        	
						  	
		//update record
		$updateArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','USER_ID'=>'','USER_STATUS'=>'','USER_PASSWORD'=>'',
												'USER_LASTLOGIN'=>'',
												'USER_LASTACTIVITY'=>'',
												'USER_LOCKREASON'=>'',
												'USER_RRESET'=>'',
												'USER_FAILEDTOKEN'=>'',
												'USER_ISNEW'=>'',
												'USER_CLEARTEXT_PWD'=>'',
												'USER_ISEMAILPWD'=>'',
												'USER_LASTCHANGEPWD'=>'',
												'USER_RPWD_ISBYBO'=>'',
												'USER_RPWD_ISBYUSER'=>'',
												'USER_RPWD_ISEMAILED'=>'',
												'USER_RPWD_ISPRINTED'=>'',
												'FGROUP_ID'=>'',
												'USER_ISEMAIL_TRAN'=>'',
												'USER_UPDATED'=>'',
												'USER_UPDATEDBY'=>'',
												'USER_HASH'=>'',
												'SESSION_HASH'=>'',
		                                        'USER_RCHANGE'=>'','USER_ISREQUIRE_CHANGEPWD'=>'','USER_ISLOGIN'=>'','USER_ISLOCKED'=>'','USER_FAILEDATTEMPT'=>'','USER_CREATED'=>'','USER_CREATEDBY'=>''));
		$updateArr['USER_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['USER_UPDATEDBY']   = $actor;
		
		$whereArr = array('CUST_ID = ?'=>$user['CUST_ID'],
						  'USER_ID = ?'=>$user['USER_ID']);
		
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
		
		//delete privilege master jika ada
		$fuser_id = $user['CUST_ID'] . $user['USER_ID'];
	    $privilegeMaster = $this->dbObj->select()
						               ->from('M_FPRIVI_USER')
						               ->where('FUSER_ID ='.$this->dbObj->quote($fuser_id))
						               ->query()->fetchAll();
						               
		if(count($privilegeMaster) > 0)
		{
		   $this->dbObj->delete('M_FPRIVI_USER',array('FUSER_ID = ?'=>$fuser_id));
		}
		
		//insert privilege baru
	    $privilege = $this->dbObj->select()
						          ->from('TEMP_FPRIVI_USER')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		$linkage = true;
		if(count($privilege) > 0)
		{
		   foreach($privilege as $privi)
		   {
		      $insertPrivi = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
              $this->dbObj->insert('M_FPRIVI_USER',$insertPrivi);

              if($privi['FPRIVI_ID'] == 'BLBU'){
              	$linkage = false;
              } 

		   }
        }

        if($linkage){
        	$whereArr = array('CUST_ID = ?'=>$user['CUST_ID'],
						  'USER_ID = ?'=>$user['USER_ID']);

        	$this->dbObj->delete('M_BENEFICIARY_USER',$whereArr);
        }
		
		$userdelete = $this->dbObj->delete('M_USER_DEBIT',$this->dbObj->quoteInto('USER_ID = ?',$user['USER_ID']));
		
		$debitcard = $this->dbObj->select()
						          ->from('TEMP_USER_DEBIT')
						          ->where('CHANGES_ID = ?',$this->_changeId)
						          ->query()->fetchAll();
		if(count($debitcard) > 0)
		{
		   foreach($debitcard as $privi)
		   {
		      $insertDebit = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
			 
			  
              $this->dbObj->insert('M_USER_DEBIT',$insertDebit);
		   }
        }


  //       $custacct = $this->dbObj->select()
		// 				          ->from('TEMP_CUSTOMER_ACCT')
		// 				          ->where('CHANGES_ID = ?',$this->_changeId)
		// 				          ->query()->fetchAll();
		// if(count($custacct) > 0)
		// {
		//    foreach($custacct as $privi)
		//    {
		//       $insertPrivi = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
  //             $this->dbObj->insert('M_CUSTOMER_ACCT',$insertPrivi);
		//    }
  //       }

        //query from TEMP_MAKERLIMIT
		// $userLimit = $this->dbObj->select()
		// 				   ->from('TEMP_MAKERLIMIT')
		// 				   ->where('CHANGES_ID = ?',$this->_changeId)
		// 				   ->query()
		// 				   ->fetchAll(Zend_Db::FETCH_ASSOC);
		// if(!count($userLimit))
		// {
  //       	$this->_errorCode = '22';
		// 	$this->_errorMsg = 'Query failed(User Limit)';
		// 	return false;
  //       }
        
		// if(count($userLimit) > 0)
		// {
		//    foreach($userLimit as $privi)
		//    {
		//    	  //delete dari master table M_MAKERLIMIT yg USER_LOGIN nya sama
		// 	  $where = array(
		// 		'USER_LOGIN = ?' =>(string)$privi['USER_LOGIN'],
		// 		'ACCT_NO = ?' =>(string)$privi['ACCT_NO']
		// 	  );
		// 	  $del = $this->dbObj->delete('M_MAKERLIMIT',$where);

		//       $insertPrivi = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//       $insertPrivi['UPDATEDBY']   = $actor;
		// 	  $insertPrivi['UPDATED']     = new Zend_Db_Expr('now()');
  //             $this->dbObj->insert('M_MAKERLIMIT',$insertPrivi);
		//    }
  //       }

  //       //query from TEMP_DAILYLIMIT
		// $userDailyLimit = $this->dbObj->select()
		// 				   ->from('TEMP_DAILYLIMIT')
		// 				   ->where('CHANGES_ID = ?',$this->_changeId)
		// 				   ->query()
		// 				   ->fetchAll(Zend_Db::FETCH_ASSOC);
		// if(!count($userDailyLimit))
		// {
  //       	$this->_errorCode = '22';
		// 	$this->_errorMsg = 'Query failed(User Limit)';
		// 	return false;
  //       }
        
		// if(count($userDailyLimit) > 0)
		// {
		//    foreach($userDailyLimit as $privi)
		//    {
		//    	  //delete dari master table M_DAILYLIMIT yg USER_LOGIN nya sama
		// 	  $where = array(
		// 		'USER_LOGIN = ?' =>(string)$privi['USER_LOGIN'],
		// 		'CCY_ID = ?' =>(string)$privi['CCY_ID']
		// 	  );
		// 	  $del = $this->dbObj->delete('M_DAILYLIMIT',$where);

		//       $insertDaily = array_diff_key($privi,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		// 	  $insertDaily['UPDATEDBY']   = $actor;
		// 	  $insertDaily['UPDATED']     = new Zend_Db_Expr('now()');
  //             $this->dbObj->insert('M_DAILYLIMIT',$insertDaily);
		//    }
  //       }
        
        $deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges) return false;
		
		
		//integrate ke core (TOKEN), delete token lama dan add token baru	
        //proses dilakukan jika token baru berbeda dengan token lama, jika sama maka tidak diproses
        if($user['TOKEN_ID'])
        {
	        if($m_user['TOKEN_ID'] != $user['TOKEN_ID'])
			{  
			    $resultRemove = array();
			
			    //jika token id di master user ada isinya maka diremove, jika tidak ada tidak usah di remove
			    if(!empty($m_user['TOKEN_ID']))
			    {
				    //REMOVE TOKEN
				    $dataRemove   = new Service_Token($m_user['CUST_ID'],$m_user['USER_ID'],$m_user['TOKEN_ID']);
					$param['condition'] = 1;
					$resultRemove = $dataRemove->removePairToken($param);
			        //$resultRemove['ResponseCode'] = '00';
			        
			        if($resultRemove['ResponseCode'] != '00')  //00 = success
			        {
			            $this->_errorMsg = $resultRemove['ResponseDesc'];
			            return false;
			        }
			    }
			    else $resultRemove['ResponseCode'] = '00';
				
				if($resultRemove['ResponseCode'] == '00')
				{    
			        //ADD TOKEN		
					$param = array();
					$param['TokenSerial'] 	= $user['TOKEN_ID'];
					$param['UserName'] 		= $user['USER_FULLNAME'];
					
					$param['CustAddress'] 	= $customer['CUST_ADDRESS'];
					$param['CustCity'] 		= $customer['CUST_CITY'];
					$param['CustName'] 		= $customer['CUST_NAME'];
					$param['CustZipCode'] 	= $customer['CUST_ZIP'];
					$dataAdd =  new Service_Token($user['CUST_ID'],$user['USER_ID']);
			
					$resultAdd = $dataAdd->addPairToken($param);	
			        //$resultAdd['ResponseCode'] = '00';
			        
					/*echo $user['CUST_ID'] . '  '.$user['USER_ID'] . '  '.$user['TOKEN_ID']; 
					Zend_Debug::dump($resultAdd); die;*/
					
			        if($resultAdd['ResponseCode'] != '00')  //00 = success
			        {
			            $this->_errorMsg = $resultAdd['ResponseDesc'];
			            return false;
			        }
			        //END integrate ke core (TOKEN)	
				}
				else
				{
				    $this->_errorMsg = $resultRemove['ResponseDesc'];
		            return false;
				}
			}//END if($m_user['TOKEN_ID'] != $user['TOKEN_ID'])
        }
        else if($m_user['TOKEN_ID'])
		{ 
		     //REMOVE TOKEN
				$dataRemove   = new Service_Token($user['CUST_ID'], $user['USER_ID'], $m_user['TOKEN_ID']);
				$param['condition'] = 1;
				$resultRemove = $dataRemove->removePairToken($param);
		     //$resultRemove['ResponseCode'] = '00';
		     
		     //Zend_Debug::dump($resultRemove); die;
				
			 if($resultRemove['ResponseCode'] != '00')
			 {
		        $this->_errorMsg = $resultRemove['ResponseDesc'];
	            return false;
			 }
		}
        
		
		
		
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp User)';
			return false;
        }
        
        
	    //query from M_USER
		$m_user = $this->dbObj->select()
						  ->from('M_USER')
						  ->where('CUST_ID = ?',(string)$user['CUST_ID'])
						  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($m_user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Master User)';
			return false;
        }
        

		//Validation, user can only be activated if this user's customer is active
		$customer = $this->dbObj->select()
							    ->from('M_CUSTOMER')
							  	->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	->query()
							  	->fetch(Zend_Db::FETCH_ASSOC);
		if($customer['CUST_STATUS'] == 'I') {
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		
		
		$updateArr = array('USER_STATUS' => 1);
		$updateArr['USER_SUGGESTED']   = $user['USER_SUGGESTED'];
		$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
		$updateArr['USER_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['USER_UPDATEDBY']   = $actor;
		
		
		/*//jika new user pertama x, maka ada tambahan yaitu : request change password dan update requeset unlock user
		if($m_user['USER_ISNEW'] == 1)
		{
		    $customerUser = new CustomerUser($user['CUST_ID'],$user['USER_ID']);
		    $customerUser->requestUnlockResetPassword(2);    //2 = print
		    $customerUser->approveRequestResetPassword();
		}*/
		
		
	
		$whereArr = array('CUST_ID = ?'=>(string)$user['CUST_ID'],
						  'USER_ID = ?'=>(string)$user['USER_ID']);
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$subject = "STATUS USER ACCOUNT INFORMATION";
	    $setting = new Settings();
	    $templateEmailMasterBankName = $setting->getSetting('master_bank_name');
	    $templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
	    $templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
	    $template = $setting->getSetting('ftemplate_addusernotification');
	    
	    // 		$subject = "CMD - New User";

	    $data = $this->dbObj->select()
	    ->from('TEMP_USER')
	    ->where('CHANGES_ID = ?',$this->_changeId)
	    ->query()
	    ->fetch(Zend_Db::FETCH_ASSOC);
	    // 		print_r($data);die;
	    $datacreated = $this->dbObj->select()
	    ->from('M_USER')
	    ->where('USER_ID = ?',$user['USER_SUGGESTEDBY'])
	    ->where('CUST_ID = ?',$user['CUST_ID'])
	    ->query()
	    ->fetch(Zend_Db::FETCH_ASSOC);
	    // 		print_r($datacreated);die;


	    $template = str_ireplace('[[user_fullname]]',$user['USER_FULLNAME'],$template);
	    $template = str_ireplace('[[comp_accid]]',$user['CUST_ID'],$template);

	    $template = str_ireplace('[[buser_name]]',$user['USER_FULLNAME'],$template);
	    $template = str_ireplace('[[user_login]]',$user['USER_ID'],$template);
	    $template = str_ireplace('[[user_email]]',$user['USER_EMAIL'],$template);
	    $template = str_ireplace('[[user_status]]','Unsuspend',$template);

	    $template = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$template);
	    $template = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$template);
	    $template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);

	    $email = $datacreated['USER_EMAIL'];
	    if($email!=''){
	    	$result = Application_Helper_Email::sendEmail($email,$subject,$template);
            }

		
		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;
		
			
			
			
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }
        
		//Validation, user can only be activated if this user's customer is active
		/*$customer = $this->dbObj->select()
							  	 ->from('M_CUSTOMER')
							  	 ->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	 ->query()
							  	 ->fetch(Zend_Db::FETCH_ASSOC);

		if($customer['CUST_STATUS'] == 'I') {
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		*/
        
		//update user status			  
		//$updateArr = array('USER_STATUS' => 'I', 'USER_HASTOKEN' => 'N');
		
		$updateArr = array('USER_STATUS' => '2');
		$updateArr['USER_SUGGESTED']   = $user['USER_SUGGESTED'];
		$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
		$updateArr['USER_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['USER_UPDATEDBY']   = $actor;
		
		
		
		$whereArr = array('CUST_ID = ?'=>(string)$user['CUST_ID'],
						  'USER_ID = ?'=>(string)$user['USER_ID']);
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$subject = "STATUS USER ACCOUNT INFORMATION";
	    $setting = new Settings();
	    $templateEmailMasterBankName = $setting->getSetting('master_bank_name');
	    $templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
	    $templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
	    $template = $setting->getSetting('ftemplate_addusernotification');
	    // 		$subject = "CMD - New User";

	    $data = $this->dbObj->select()
	    ->from('TEMP_USER')
	    ->where('CHANGES_ID = ?',$this->_changeId)
	    ->query()
	    ->fetch(Zend_Db::FETCH_ASSOC);
	    // 		print_r($data);die;
	    $datacreated = $this->dbObj->select()
	    ->from('M_USER')
	    ->where('USER_ID = ?',$user['USER_SUGGESTEDBY'])
	    ->where('CUST_ID = ?',$user['CUST_ID'])
	    ->query()
	    ->fetch(Zend_Db::FETCH_ASSOC);
	    // 		print_r($datacreated);die;


	    $template = str_ireplace('[[user_fullname]]',$user['USER_FULLNAME'],$template);
	    $template = str_ireplace('[[comp_accid]]',$user['CUST_ID'],$template);

	    $template = str_ireplace('[[buser_name]]',$user['USER_FULLNAME'],$template);
	    $template = str_ireplace('[[user_login]]',$user['USER_ID'],$template);
	    $template = str_ireplace('[[user_email]]',$user['USER_EMAIL'],$template);
	    $template = str_ireplace('[[user_status]]','Suspend',$template);

	    $template = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$template);
	    $template = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$template);
	    $template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);

	    $email = $datacreated['USER_EMAIL'];
	    if($email!=''){
	    	$result = Application_Helper_Email::sendEmail($email,$subject,$template);
            }
		
		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}


/**
 * Returns an encrypted & utf8-encoded
 */
 
	public function sslPrm()
	{
	return array("6a1f325be4c0492063e83a8cb2cb9ae7","IV (optional)","aes-128-cbc");
	}

	public function sslEnc($msg)
	{
	  list ($pass, $iv, $method)=$this->sslPrm();
		 return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}
 
	function encrypt($pure_string, $encryption_key) {
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
	    return $encrypted_string;
	}

	/**
	 * Returns decrypted original string
	 */
	function decrypt($encrypted_string, $encryption_key) {
	    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
	    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
	    return $decrypted_string;
	}
	
	public function approveDelete($actor = null) 
	{
		//query from TEMP_USER
		$user = $this->dbObj->select()
						  ->from('TEMP_USER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
						  
		if(!count($user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }
        
        //query from M_USER
		$m_user = $this->dbObj->select()
						  	  ->from('M_USER')
						  	  ->where('USER_ID = ?',(string)$user['USER_ID'])
						  	  ->where('CUST_ID = ?',(string)$user['CUST_ID'])
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($m_user))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }								 

        
		//Validation, user can only be activated if this user's customer is active
		$customer = $this->dbObj->select()
							    ->from('M_CUSTOMER')
							  	->where('CUST_ID = ?',(string)$user['CUST_ID'])
							  	->query()
							  	->fetch(Zend_Db::FETCH_ASSOC);
							  	
		if($customer['CUST_STATUS'] == 'I') 
		{
			$this->_errorCode = '73';
			$this->_errorMsg = 'Parent Customer Inactive';
			return false;
		}
		
		
		$updateArr['USER_STATUS']      = 3;
		$updateArr['USER_ISLOGIN']     = 0;
		$updateArr['USER_ISLOCKED']    = 0;
		$updateArr['USER_FAILEDATTEMPT'] = 0;
		$updateArr['USER_LOCKREASON']    = '';
		$updateArr['USER_SUGGESTED']   = $user['USER_SUGGESTED'];
		$updateArr['USER_SUGGESTEDBY'] = $user['USER_SUGGESTEDBY'];
		$updateArr['USER_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['USER_UPDATEDBY']   = $actor;
		
		
		$whereArr = array('CUST_ID = ?'=>(string)$user['CUST_ID'],
						  'USER_ID = ?'=>(string)$user['USER_ID']);
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);

			$updateMaker['UPDATED']     = new Zend_Db_Expr('now()');
		$updateMaker['UPDATEDBY']   = $actor;
		$updateMaker['MAKERLIMIT_STATUS']   = '3';
		$whereMaker = array('CUST_ID = ?'=>(string)$user['CUST_ID'],
						  'USER_LOGIN = ?'=>(string)$user['USER_ID']);
		$usermaker = $this->dbObj->update('M_MAKERLIMIT',$updateMaker,$whereMaker);


		$updateDaily['UPDATED']     = new Zend_Db_Expr('now()');
		$updateDaily['UPDATEDBY']   = $actor;
		$updateDaily['DAILYLIMIT_STATUS']   = '3';
		$whereDaily = array('CUST_ID = ?'=>(string)$user['CUST_ID'],
						  'USER_LOGIN = ?'=>(string)$user['USER_ID']);
		$usermaker = $this->dbObj->update('M_DAILYLIMIT',$updateDaily,$whereDaily);
 
		
		if(!(boolean)$userupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
	    //integrate ke core (TOKEN), delete token lama dan add token baru	
	    //diremove jika hanya token di M_USER ada isinya, jika tidak ada maka tidak usah dihapus
	    if($m_user['TOKEN_ID'])
	    {
	        //REMOVE TOKEN
			$data   = new Service_Token($user['CUST_ID'], $user['USER_ID'], $m_user['TOKEN_ID']);
			$param['condition'] = 1;
			$result = $data->removePairToken($param);
	        //$result['ResponseCode'] = '00';
			
			if($result['ResponseCode'] != '00')    //00 = 'success'
			{
		        $this->_errorMsg = $result['ResponseDesc'];
	            return false;
			}
	    }
	    
	   $subject = "STATUS USER ACCOUNT INFORMATION";
	    $setting = new Settings();
	    $templateEmailMasterBankName = $setting->getSetting('master_bank_name');
	    $templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
	    $templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
	    $templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
	    $templateEmailMasterBankWapp = $setting->getSetting('master_bank_wapp');
	    $template = $setting->getSetting('femailtemplate_userstatus');
	    // 		$subject = "CMD - New User";

	    $data = $this->dbObj->select()
	    ->from('TEMP_USER')
	    ->where('CHANGES_ID = ?',$this->_changeId)
	    ->query()
	    ->fetch(Zend_Db::FETCH_ASSOC);
	    // 		print_r($data);die;
	    $datacreated = $this->dbObj->select()
	    ->from('M_USER')
	    ->where('USER_ID = ?',$user['USER_SUGGESTEDBY'])
	    ->where('CUST_ID = ?',$user['CUST_ID'])
	    ->query()
	    ->fetch(Zend_Db::FETCH_ASSOC);
	    // 		print_r($datacreated);die;


	    $template = str_ireplace('[[user_fullname]]',$user['USER_FULLNAME'],$template);
	    $template = str_ireplace('[[comp_accid]]',$user['CUST_ID'],$template);

	    $template = str_ireplace('[[buser_name]]',$user['USER_FULLNAME'],$template);
	    $template = str_ireplace('[[user_id]]',$user['USER_ID'],$template);
	    $template = str_ireplace('[[user_email]]',$user['USER_EMAIL'],$template);
	    $template = str_ireplace('[[user_status]]','Deleted',$template);

	    $template = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$template);
	    $template = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$template);
	    $template = str_ireplace('[[master_bank_wapp]]',$templateEmailMasterBankWapp,$template);
	    $template = str_ireplace('[[master_bank_app_name]]',$templateEmailMasterBankAppName,$template);
	    $template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);

	    // $email = $datacreated['USER_EMAIL'];
	    $email = $user['USER_EMAIL'];
	    if($email!=''){
	    	$result = Application_Helper_Email::sendEmail($email,$subject,$template);
            }

        $deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges) return false;
		
		return true;
	}
	
    /**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
		
		
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		
	    
		$this->dbObj->delete('TEMP_FPRIVI_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		$this->dbObj->delete('TEMP_CUSTOMER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		$this->dbObj->delete('TEMP_MAKERLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$this->dbObj->delete('TEMP_DAILYLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$this->dbObj->delete('TEMP_USER_DEBIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		      
             
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		$this->dbObj->delete('TEMP_CUSTOMER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		$this->dbObj->delete('TEMP_MAKERLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$this->dbObj->delete('TEMP_DAILYLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		$this->dbObj->delete('TEMP_FPRIVI_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$this->dbObj->delete('TEMP_USER_DEBIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteDelete() 
	{
		$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
		
	
	
	}
	
	
	
    public function approveDeactivate(){

	}

	public function approveActivate(){

	}
	
    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}
	
	
	
	
}