<?php
/**
 * Tempcustomer model
 *
 * @author
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';

class Changemanagement_Model_Tempcorporatedcard extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = '';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUST_DEBIT')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
  
		//insert to master table M_CUSTOMER
		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['DEBIT_STATUS']   = '1';
		$insertArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$insertArr['DEBIT_APPROVEDBY']   = $actor;
		//$insertArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$insertArr['APPROVED_DATE']     = new Zend_Db_Expr('now()');
		//$insertArr['BIN_UPDATEDBY']   = $actor;
		
		//$insertArr['CUST_ID']   = $customer['CUST_ID'];
		//$insertArr['CUST_BIN']   = $customer['CUST_BIN'];
		
					$request = array();
					
					if($insertArr['DEBIT_TYPE'] == '1'){
						$cust_type = 'CORPORATE';	
					}else{
						$cust_type = 'CARDHOLDER';	
					}
					
					
					$selectcomp = $this->dbObj->select()
                            ->from('M_CUSTOMER', array('*'))
                             ->where('CUST_ID = ? ',$insertArr['CUST_ID'])
                            ->where('CUST_STATUS = 1 ');
					//echo $selectcomp;die;
					$datacomp = $this->dbObj->fetchAll($selectcomp);
					
					//echo '<pre>';
					//var_dump($datacomp);die;
					
					
					$request['header']['sender_id'] = 'D360';
					$request['header']['signature'] = '';
					$request['data']['customer_type'] = $cust_type;
					$request['data']['bank_code'] = '999';
					$request['data']['cif'] = $datacomp['0']['CUST_CIF'];
					$request['data']['account_type'] = '2';
					$request['data']['account_number'] = $insertArr['VA_NUMBER'];
					$request['data']['account_currency'] = '000';
					$request['data']['account_name'] = $datacomp['0']['CUST_NAME'];
					$request['data']['email'] = $datacomp['0']['CUST_EMAIL'];
					$request['data']['phone_number'] = $datacomp['0']['CUST_PHONE'];
					$request['data']['birthdate'] = $datacomp['0']['CUST_CREATED'];
					$request['data']['address'] = $datacomp['0']['CUST_ADDRESS'];
					$request['data']['city'] = $datacomp['0']['CUST_CITY'];
					$request['data']['state'] = $datacomp['0']['CUST_PROVINCE'];
					$clientUser  =  new SGO_Soap_ClientUser();
					
					$success = $clientUser->callapi('registeraccount',$request);
					$result  = $clientUser->getResult();
					
					
					$requestva = array(
						'sender_id' => 'D360',
						'order_id' => $insertArr['VA_NUMBER'],
						'remark1' => $datacomp['0']['CUST_PHONE'],
						'remark2' => $datacomp['0']['CUST_NAME'],
						'remark3' => $datacomp['0']['CUST_EMAIL']
						
					);
					$success = $clientUser->callapi('generateva',$requestva);
					$result  = $clientUser->getResult();
					$resultArr = json_decode(json_encode($result), true);
					if(!empty($resultArr['va_list'])){
						foreach($resultArr['va_list'] as $ky => $vl){
							$inpArr = array(
								'REG_NUMBER' => $insertArr['REG_NUMBER'],
								'VA_NUMBER' => $vl['va_number'],
								'BANK_CODE' => $vl['bank_code'],
								'FEE' => $vl['fee']
							);
							$customerins = $this->dbObj->insert('T_VA_DEBIT',$inpArr);
						}
						
					}
					//echo '<pre>';
					//var_dump($resultArr);die;
					
					
					
					
		
	try
		{
			$customerins = $this->dbObj->insert('T_CUST_DEBIT',$insertArr);
		}
	catch(exception $e)
		{
			Zend_Debug::dump($e->getMessage());
		}
		if(!(boolean)$customerins)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null)
	{
		$transFailed = false;
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUST_DEBIT')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
		else
		{
			$custId = $customer['CUST_ID'];
		}
//		Zend_Debug::dump($customer);
//		die;
		//update record customer
		$updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$insertArr['DEBIT_STATUS']   = '1';
		$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$updateArr['DEBIT_APPROVEDBY']   = $actor;

		$whereArr = array(
							'REG_NUMBER = ?'=>(string)$customer['REG_NUMBER'],
							'VA_NUMBER = ?'=>(string)$customer['VA_NUMBER']
						);
						
		$customerupdate = $this->dbObj->update('T_CUST_DEBIT',$updateArr,$whereArr);

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Edited, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$updateArr['CUST_NAME'].' Change id : '.$this->_changeId);

		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUST_DEBIT')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
        }

        $updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$updateArr['DEBIT_APPROVEDBY']   = $actor;

		$whereArr = array(
							'REG_NUMBER = ?'=>(string)$customer['REG_NUMBER'],
							'VA_NUMBER = ?'=>(string)$customer['VA_NUMBER']
						);
						
		$customerupdate = $this->dbObj->update('T_CUST_DEBIT',$updateArr,$whereArr);
		
		
		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUST_DEBIT')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
        }

        $updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$updateArr['DEBIT_APPROVEDBY']   = $actor;

		$whereArr = array(
							'REG_NUMBER = ?'=>(string)$customer['REG_NUMBER'],
							'VA_NUMBER = ?'=>(string)$customer['VA_NUMBER']
						);
						
		$customerupdate = $this->dbObj->update('T_CUST_DEBIT',$updateArr,$whereArr);
		
		
		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		$deleteChanges  = $this->deleteSuspend();
		
		if(!$deleteChanges)
			return false;

		return true;
	}


	/*
	 if delete customer :
	 - delete user
	 - delete bank account
	 - delete user limit
	 - delete daily limit
	*/
	public function approveDelete($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUST_DEBIT')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Debit Card)';
			return false;
        }

        $updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		//$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		//$updateArr['BIN_UPDATEDBY']   = $actor;
		$updateArr['DEBIT_APPROVED']     = new Zend_Db_Expr('now()');
		$updateArr['DEBIT_APPROVEDBY']   = $actor;

		$whereArr = array(
							'REG_NUMBER = ?'=>(string)$customer['REG_NUMBER'],
							'VA_NUMBER = ?'=>(string)$customer['VA_NUMBER']
						);
						
		$customerupdate = $this->dbObj->update('T_CUST_DEBIT',$updateArr,$whereArr);

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
        //END update status customer


		
		//delete temp customer
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			      return false;

		return true;
	}

	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUST_DEBIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}


		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUST_DEBIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUST_DEBIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		return true;
	}

	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUST_DEBIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		return true;
	}

	public function deleteDelete()
	{


		$customerdelete = $this->dbObj->delete('TEMP_CUST_DEBIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;



	}


    public function approveDeactivate(){

	}

	public function approveActivate(){

	}

    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}





}