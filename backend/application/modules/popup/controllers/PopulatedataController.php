<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Service/Account.php';
require_once 'General/Settings.php';
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';

class popup_PopulatedataController extends customer_Model_Customer
{
	public function initController()
	{
		$this->_helper->layout()->setLayout('newpopup');
	}

	public function indexAction()
	{
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$filterArr = array(
			'next' => array('StringTrim', 'StripTags'),
			'acct_no' => array('StringTrim', 'StripTags')
		);

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $this->_request->getParams());
		$next = $zf_filter->getEscaped('next');
		$acct_no = $zf_filter->getEscaped('acct_no');
		$this->view->acct_no = $acct_no;

		if ($next == TRUE) {
			if (empty($acct_no)) {
				$this->view->message = 'Error : Account number cannot be left blank';
			} else {


				//$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
				//				//$result		= $svcAccount->inquiryAccontInfo();
				//$result		= $svcAccount->inquiryAccontInfo('AI',TRUE);
				// $account = new Account($acct_no, null, null, null);
				// $account = new Service_Account($acct_no, null, null, null);
				$account = new Service_Account($acct_no, null, null, null);
				//var_dump($val['TRANSFER_TYPE']);
				//var_dump($val['TRANSACTION_ID']);

				// $result = $account->checkAccountInfo(false);
				$result = $account->inquiryAccontInfo(false);

				//var_dump($result);die;
				//echo $result['balance_active'];
				//echo $result['data'];
				// if($result['balance_active'] == "0" || $result['balance'] == "0"){
				// 	//die('e');
				// 	$this->view->message = 'Error : Account number tidak aktif';
				// }else
				// if ($result['data'] == false) {

				if ($result['response_code'] != '0000' && $result['response_code'] != '00') {
					//die('e');
					$this->view->message = 'Error : Account number tidak ada';
				} else {
					//die;
					$responseCode = $result['response_code'];
					$responseDesc = $result['response_desc'];

					$AccountName 	= $result['account_name'] ?: '-';
					$Cif 			= $result['cif'] ?: '-';
					$Birthday 		= $result['birthday'] ?: '-';
					$Phone 			= $result['phone_number'] ?: '-';
					$Email 			= $result['email'] ?: '-';
					$Address 		= $result['address'] ?: '-';
					$City 			= $result['city'] ?: '-';
					$State 			= $result['state'] ?: '-';

					$getNpwp = preg_replace('/[^0-9]/', '', $result['npwp']);
					$checkNpwp = (strlen($getNpwp) == 16) ? $getNpwp : str_pad($getNpwp, 16, '0', STR_PAD_LEFT);
					$nonpwp 		= $result['npwp'] == "" ? '0000000000000000' : $checkNpwp;
					$noakte			= $result['no_akte'] ?: '-';

					$sessionNamespace 		= new Zend_Session_Namespace('populatedata');
					$paramSession 			= $sessionNamespace->paramSession;

					$paramSession['acct_no']		= $acct_no;
					$paramSession['AccountName']	= $AccountName;
					$paramSession['Cif']			= $Cif;
					$paramSession['Birthday']		= $Birthday;
					$paramSession['Phone']			= $Phone;
					$paramSession['Email']			= $Email;
					$paramSession['Address']		= $Address;
					$paramSession['City']			= $City;
					$paramSession['State']			= $State;
					$paramSession['nonpwp']			= $nonpwp;
					$paramSession['noakte']			= $noakte;

					$sessionNamespace->paramSession = $paramSession;

					$this->_redirect('/popup/populatedata/confirm/index/');
				}


				//die;


			}
		}

		Application_Helper_General::writeLog('Populate Data', 'Populate Data');
	}


	public function confirmAction()
	{
		//$this->_getParam('back');
		//		$customerUser = new customer_Model_Customer();
		$set = new Settings();
		$maxLengthUserid = 12; //$set->getSetting('max_length_userid');
		$minLengthUserid = 5; //$set->getSetting('min_length_userid');


		$sessionNamespace 	= new Zend_Session_Namespace('populatedata');
		$paramSession 		= $sessionNamespace->paramSession;
		$this->view->paramSession = $paramSession;
		//		Zend_Debug::dump($paramSession);

		$acct_no		= $paramSession['acct_no'];
		$AccountNameOri 	= $paramSession['AccountName'];
		//		$AccountName 	= str_replace(",", '', $AccountNameOri);
		$AccountName 	= preg_replace("/[^0-9a-zA-Z]/", "", $AccountNameOri);
		$this->accountName = $AccountName;
		$Cif 			= $paramSession['Cif'];
		$BirthdayOri	= $paramSession['Birthday'];
		$Phone 			= $paramSession['Phone'];
		$Email 			= $paramSession['Email'];
		$Address 		= $paramSession['Address'];
		$City 			= $paramSession['City'];
		$State 			= $paramSession['State'];

		$tblName = $this->_getParam('id');
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];
		// echo 'here';die;
		//var_dump($paramSession);die;
		$core = array();
		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $Cif);
		$result = $svcAccountCIF->inquiryCIFAccount();
		$core =  $result['accounts'];


		//	echo '<pre>';
		//	var_dump($result);
		//var_dump($core);die;
		if ($result['response_code'] == '0000') {
			$arrResult = array();
			$err = array();

			if (count($core) > 1) {
				//            print_r($core->accountList);
				//array data lebih dari 1
				foreach ($core as $key => $val) {
					//if($val->status == '0'){
					$arrResult = array_merge($arrResult, array($val['account_number'] => $val));
					array_push($err, $val['account_number']);
					//}
					//              echo 'here';
				}
			} else {
				//array kurang dari 2
				foreach ($core as $data) {
					//if($data->status == '0'){
					$arrResult = array_merge($arrResult, array($data['account_number'] => $data));
					array_push($err, $data['account_number']);
					//}
					//              echo 'here1';
					//            Zend_Debug::dump($data);
				}
			}
			// print_r($core->accountList);
			$final = $arrResult;
			if (count($final) < 1) {
				$this->view->data1 = '0';
			} else {
				$this->view->data = $arrResult;
			}


			//        $dataPro = array();
			$dataProPlan = array('xxxxx');
			foreach ($final as $dataProductPlan) {

				//if($dataProductPlan->status == '0'){
				if (isset($dataProductPlan['type'])) {
					$select_product_type  = $this->_db->select()
						->from(array('M_PRODUCT_TYPE'), array('PRODUCT_NAME'))
						->where("PRODUCT_CODE = ?", $dataProductPlan['type']);
					//              ->where('PRODUCT_PLAN = ?','62');
					// echo $select_product_type;
					$userData_product_type = $this->_db->fetchAll($select_product_type);
					//
					// print_r($userData_product_type);
					$dataProductPlan['planName'] = $userData_product_type[0]['PRODUCT_NAME'];
					// $dataProductPlanOri = (($dataProductPlan->planName == null)?'':$dataProductPlan);
				} else {
					$dataProductPlan = $dataProductPlan;
				}

				// print_r($dataProductPlan);die;
				$dataUser[] = $dataProductPlan;
				//}
			}
		}
		// print_r($dataUser);
		$optHtml = "<table cellpadding='3' cellspacing='0' border='0' class='table table-sm table-striped'><tr class='headercolor'><th width='3%' valign='top' style='background-color: #0a3918; color:white'></th><th  valign='top'  style='background-color: #0a3918; color:white'>Account Number</th><th  valign='top'  style='background-color: #0a3918; color:white'>Account Name</th><th  valign='top'  style='background-color: #0a3918; color:white'>Currency</th><th  valign='top'  style='background-color: #0a3918; color:white'>Type</th></tr>";

		//var_dump($dataUser);die;
		if (!empty($dataUser)) {
			foreach ($dataUser as $key => $row) {
				$i = $i + 1;
				$acct_source = 3;
				$acct_desc   = $row['type_desc'];

				$button_checkbox = "<input type='checkbox' name='req_id[]' id='" . $row['account_number'] . "' value='" . $row['account_number'] . "'>";
				// $button_checkbox = $this->formCheckbox("req_id[{$row->accountNo}]",null);
				$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
				// print_r($row);echo 'here';
				// if ($this->data[$key] != ""){

				if ($row['account_number'] == '') {
				} else {
					$optHtml .= '<tr><td class="' . $td_css . '"><input type="checkbox" name="req_id[]" id="' . $row['account_number'] . '" onclick="check()" value="' . $row['account_number'] . '"></td><td class="' . $td_css . '">' . $row['account_number'] . '</td><td class="' . $td_css . '"><input type="hidden" name="accountName' . $row['account_number'] . '" value="' . $row['planName'] . '">' . $row['planName'] . '</td>';
					$optHtml .= '<td class="' . $td_css . '"><input type="hidden" name="ccy' . $row['account_number'] . '" value="' . Application_Helper_General::getCurrCode($row['currency']) . '">' . Application_Helper_General::getCurrCode($row['currency']) . '</td><td class="' . $td_css . '"><input type="hidden" name="acct_desc' . $row['account_number'] . '" value="' . $acct_desc . '"><input type="hidden" name="productType' . $row['account_number'] . '" value="' . $row['type'] . '">' . $acct_desc . '<input type="hidden" name="planCode' . $row['account_number'] . '" value="' . $row['type'] . '"><input type="hidden" name="productName' . $row['account_number'] . '" value="' . $acct_desc . '"></td></tr>';
				}
				// }

			}
		} else {
			// if($this->data == ''){
			$optHtml .= '<tr><td colspan="5" class="tbl-evencontent" align="center">' . $this->language->_('No Data') . '</td></tr>';
		}
		$optHtml .= '</table>';
		//echo $optHtml;die;
		$this->view->ciftable = $optHtml;

		$birthdayRep = str_replace('-', '', $BirthdayOri);
		$birthday = substr($birthdayRep, 4, 4);
		$birthdayY = substr($birthdayRep, 2, 4);

		$shuffledBirthday = str_shuffle($birthday); //acak birthday dm
		$shuffledBirthdayOri = str_shuffle($birthdayRep); //acak birthday Ymd
		$shuffledBirthdayY = str_shuffle($birthdayY); //acak birthday Ymd

		$this->view->maxLengthUserid = $maxLengthUserid;

		//$subUserid = substr($AccountName, '0','9');
		//$random = str_pad(mt_rand(0, 9999), 4, '0', STR_PAD_LEFT);

		$random = rand($minLengthUserid, $maxLengthUserid);

		$subUserid1 = substr($AccountName, 0, $maxLengthUserid);
		$replaceUserid1 = str_replace(' ', '', $subUserid1);

		$useridNew1 = strtoupper($replaceUserid1 . $shuffledBirthday);

		if (strlen($useridNew1) > $maxLengthUserid) {
			$useridNeww = strtoupper(substr($AccountName, '0', $maxLengthUserid - 4));
			$useridNew2 = str_replace(' ', '', $useridNeww);
		} else {
			$useridNew2 = strtoupper($replaceUserid1);
		}

		$getUseridTmp1 = $this->getUserIDTmp($useridNew2);
		$getUserid1 = $this->getUserID($useridNew2);

		if (count($getUseridTmp1) < 1) {
			if (count($getUserid1) < 1) {
				//tidak ada data - boleh insert
				if (strlen($useridNew2) < $minLengthUserid) {
					$useridNeww = strtoupper(substr($AccountName, '0', $maxLengthUserid - 3));
					$useridNew2 = str_replace(' ', '', $useridNeww);
					if (strlen($useridNew2) < $minLengthUserid) {
						$useridNeww = strtoupper(substr($AccountName, '0', $maxLengthUserid - 2));
						$useridNew = str_replace(' ', '', $useridNeww);
						$this->view->useridNew = $useridNew;
					} else {
						$useridNew = str_replace(' ', '', $useridNeww);
						$this->view->useridNew = $useridNew;
					}
				} else {
					$useridNew = $useridNew2;
					$this->view->useridNew = $useridNew;
				}
			} else {
				//echo'ganti data';
				$subUserid1 = substr($AccountName, 0, $maxLengthUserid - 4);
				$replaceUserid1 = str_replace(' ', '', $subUserid1);
				$useridNew = strtoupper($replaceUserid1);
				$this->view->useridNew = $useridNew;
			}
		} else {
			//ganti data temp
			$subUserid1 = substr($AccountName, 0, $maxLengthUserid - 4);
			$replaceUserid1 = str_replace(' ', '', $subUserid1);
			$useridNew = strtoupper($replaceUserid1);
			$this->view->useridNew = $useridNew;
		}

		//die;
		Application_Helper_General::writeLog('Populate Data', 'Populate Data Confirm');
	}
}
