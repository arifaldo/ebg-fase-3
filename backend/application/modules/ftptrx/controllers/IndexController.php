<?php

require_once 'Zend/Controller/Action.php';

class Ftptrx_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$countrycode = $this->language->_('ID Transaction');
		$countryname = $this->language->_('Type');



	    $fields = array(
						'country_code'  => array('field' => 'COUNTRY_CODE',
											      'label' => $countrycode,
											      'sortable' => true),
						'country_name'     => array('field' => 'COUNTRY_NAME',
											      'label' => $countryname,
											      'sortable' => true),
						'date'     => array('field' => 'COUNTRY_NAME',
											      'label' => $this->language->_('Date'),
											      'sortable' => true),
						'time'     => array('field' => 'COUNTRY_NAME',
											      'label' => $this->language->_('Time'),
											      'sortable' => true),
						'amount'     => array('field' => 'COUNTRY_NAME',
											      'label' => $this->language->_('Amount'),
											      'sortable' => true),
												  
						'desc'     => array('field' => 'COUNTRY_NAME',
											      'label' => $this->language->_('Description'),
											      'sortable' => true),
						'stats'     => array('field' => 'COUNTRY_NAME',
											      'label' => $this->language->_('Status'),
											      'sortable' => true),
				      );

	    $filterlist = array("COUNTRY_NAME");

		$this->view->filterlist = $filterlist;


		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','COUNTRY_NAME');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'COUNTRY_NAME'  => array('StringTrim','StripTags'),
		);

		$dataParam = array("COUNTRY_NAME");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');



		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		//$filter_lg = $this->_getParam('filter');

		



		
		






	      
	    $filterdate 		= $this->_getParam('filterdate');
		$this->view->filterdate = $filterdate; 
	    // echo "<pre>";
	    // print_r($filter);exit();
	    // $ObjLaporanKonsol->SetDebugOn(); 
	    // $today = date("Ymd"); 
	    if(!empty($filterdate)){
//var_dump($filterdate);die;
//	    define('UPLOAD_PATH',LIBRARY_PATH . '/data/uploads');
		$arr        =   explode('/',$filterdate);
		$newDate    =   $arr[2].$arr[1].$arr[0];
		
		
		
		$ftp_server="ftp.saranaonline.com";
		$ftp_user_name="espay";
		$ftp_user_pass="06es04pay17";
		
		 // print_r($tgl);die;
		 $thn =  $arr[2];
		 $bln =  $arr[1];
		 $day =  $arr[0];
		// set up basic connection
		$conn_id = ftp_connect($ftp_server); 
		// print_r($conn_id);die;
		// login with username and password
		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 


			$postpaidcode = '836301211-'.$thn.''.$bln.''.$day.'01.RPT';

			$prepaidcode = '836301422-'.$thn.''.$bln.''.$day.'01.RPT';

			$nontagcode = '836305111-'.$thn.''.$bln.''.$day.'01.RPT';
	
		$destinationUploadDir = UPLOAD_PATH.'/document/temp/';
		$postpaidcode = '836301211-'.$newDate.'01.RPT';
		$postpaiddata = file(UPLOAD_PATH.'/document/temp/'.$postpaidcode);
		
		if(!empty($postpaiddata)){

		$ftpfile   = file_get_contents('ftp://espay:06es04pay17@ftp.saranaonline.com/'.$thn.'/'.$bln.'/'.$postpaidcode);


		$result = file_put_contents($destinationUploadDir.$postpaidcode,$ftpfile);

		//if($result){echo 'success';}else{echo 'fail';}

		$ftpfilepre   = file_get_contents('ftp://espay:06es04pay17@ftp.saranaonline.com/'.$thn.'/'.$bln.'/'.$prepaidcode);

		$result = file_put_contents($destinationUploadDir.$prepaidcode,$ftpfilepre);


		$ftpfilenon   = file_get_contents('ftp://espay:06es04pay17@ftp.saranaonline.com/'.$thn.'/'.$bln.'/'.$nontagcode);

		$result = file_put_contents($destinationUploadDir.$nontagcode,$ftpfilenon);
		}
		
		
		
//$destinationUploadDir = '/app/360espay/digibank/library/data/uploads/document/temp/';
	
	    

	    $prepaidcode = '836301422-'.$newDate.'01.RPT';

	    $nontagcode = '836305111-'.$newDate.'01.RPT';

	    $postpaiddata = file(UPLOAD_PATH.'/document/temp/'.$postpaidcode);

	    $prepaiddata = file(UPLOAD_PATH.'/document/temp/'.$prepaidcode);

	    $nontagdata = file(UPLOAD_PATH.'/document/temp/'.$nontagcode);
	     
	     

	    $data = array_merge($postpaiddata,$prepaiddata);
	    $resultdata = array_merge($data,$nontagdata);
		 $arrBulan = array("Januari",
                        "Februari",
                        "Maret",
                        "April",
                        "Mei",
                        "Juni", 
                        "Juli",
                        "Agustus",
                        "September",
                        "Oktober",
                        "November",
                        "Desember");
	//	echo '<pre>';
	//	var_dump($resultdata);die;
		if(!empty($resultdata)){
			
			$i = 0;
			$data = array();
			 foreach ($resultdata as $val) {       
                
                $postdata = explode(',', $val);
                //var_dump($postdata);die;
                foreach ($postdata as $key => $postval) {
                  
                     if($postdata['3']!='00000000000')
                    {

                     if($key==0){
                        if($postval=='0'){
                          $restext = 'Success';
                        }else if($postval=='2'){
                          $restext = 'Cancel';
                        }else if($postval == '3'){
                          $restext = 'Must be paid';
                        }

                        // $time_text = $time[0].':'.$time['1'].':'.$time['2'];
                        // print_r($time_text);die;
						$data[$i]['status'] = $restext;
                        //$this->mrTemplate->addVar('item', 'DATA'.$key, $restext);
                        // print_r($time);die;
                    }
                    else if($key==1){
                        $time = str_split($postval, 2);

                        $time_text = $time[0].':'.$time['1'].':'.$time['2'];
                        // print_r($time_text);die;
						$data[$i]['time'] = $time_text;
                        //$this->mrTemplate->addVar('item', 'DATA'.$key, $time_text);
                        // print_r($time);die;
                    }else if($key == 2){
                        $tanggal = str_split($postval, 2);
                        $errdate = (int)$tanggal[0]-1;
                        // print_r($errdate);die;
                        if($errdate>=12){
                          $bulantxt = 'N/A';
                        }else{
                          $bulan   = $arrBulan[(int)$tanggal[0]-1];
                          $bulantxt = $tanggal[1].' '.$bulan;
                        }
						
						$data[$i]['date'] = $bulantxt;
                        //$this->mrTemplate->addVar('item', 'DATA'.$key, $bulantxt);

                        // print_r($bulan);die;
                    }else if($key==6){
                      if($postval == '301211'){
                        $type = 'PLN POSTPAID';
                      }else if($postval == '301422'){
                        $type = 'PLN PREPAID';
                      }else if($postval == '305111'){
                        $type = 'NON TAGLIST';
                      }
                      // print_r($postval);die;
					  $data[$i]['type'] = $type;
                      //$this->mrTemplate->addVar('item', 'DATA'.$key, $type);

                    }else if($key == 8){
                        // $amountset = round($beginsaldo/100);
                      $temp_amount = (int)$postval+$temp_amount;
                      $temp_data = (int)1+$temp_data;
                      $amount = number_format( (int)$postval , 0 ,'' , ',' );
					  $data[$i]['amount'] = $amount;
                      //$this->mrTemplate->addVar('item', 'DATA'.$key, $amount);
                    }
					else if($key == 9){
                        // $amountset = round($beginsaldo/100);
                      
					  $data[$i]['id'] = $postval;
                      //$this->mrTemplate->addVar('item', 'DATA'.$key, $amount);
                    }
					else if($key == 11){
                        // $amountset = round($beginsaldo/100);
                      
					  $data[$i]['desc'] = $postval;
                      //$this->mrTemplate->addVar('item', 'DATA'.$key, $amount);
                    }

                    else{
                    //$this->mrTemplate->addVar('item', 'DATA'.$key, $postval);
                    }


                  }else{

                  }
				  
					
					
				}
				$i++;
			 }
				
			
			
		}
		
		//echo '<pre>';
		//var_dump($data);die;
		  $this->view->data = $data;


		}
		//--------konfigurasicsv dan pdf---------
	    
				Application_Helper_General::writeLog('COLS','View FTP Transaction List');
		
		//-------END konfigurasicsv dan pdf------------

		
		$this->view->fields = $fields;
		$this->view->filter = $filter;

	}

}
