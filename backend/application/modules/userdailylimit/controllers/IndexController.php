<?php

require_once 'Zend/Controller/Action.php';

class userdailylimit_IndexController extends userdailylimit_Model_Userdailylimit 
{

  public function indexAction() 
  {

    $setting = new Settings();          
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
    $pw_hash = md5($enc_salt.$enc_pass);
    $rand = $this->_userIdLogin.date('dHis').$pw_hash;
    $sessionNamespace->token  = $rand;
    $this->view->token = $sessionNamespace->token; 
  
    $this->_helper->layout()->setLayout('newlayout');
  
    //pengaturan url untuk button back
	$this->setbackURL('/'.$this->_request->getModuleName().'/index/'); 
  
  	$this->view->CCYData = $this->getCcy();
  	
  	$custArr  = Application_Helper_Array::listArray($this->getAllCustomer(),'CUST_ID','CUST_ID');
    $custArr  = array_merge(array(''=>'-- '.$this->language->_('Any Value').' --'),$custArr);
    $this->view->custArr = $custArr;
  	
    //$statusArr = Application_Helper_Array::globalvarArray($this->_masteruserStatus);
	$statusArr = array(''=>'-- '.$this->language->_('Any Value').' --','1'=>$this->language->_('Approved'),'3'=>$this->language->_('Deleted'));
    $this->view->statusArr = $statusArr;
    
    
    $this->view->signArr = array('EQ'=>'=', 'NE'=>'!=', 'LT'=>'<', 'GT'=>'>', 'LE'=>'<=', 'GE'=>'>=');
		
	//format display date
    $this->view->dateDisplayFormat = $this->_dateDisplayFormat;
  	
    
  	$fields = array(
  	                'custid'   => array('field'    => 'd.CUST_ID',
                                        'label'    => $this->language->_('Company'),
                                        'sortable' => true),
    
                    // 'custname' => array('field'    => 'c.CUST_NAME',
                    //                     'label'    => $this->language->_('Company Name'),
                    //                     'sortable' => true),
    
  	                'userId'        => array('field'     => 'd.USER_LOGIN',
                                             'label'     => $this->language->_('User'),
                                             'sortable'  => true),
  	
  	                'ccy'  		    => array('field'     => 'd.CCY_ID',
                                             'label'     => $this->language->_('CCY'),
                                             'sortable'  => true),
  	
  	                'dailyLimit'   	=> array('field'     => 'd.DAILYLIMIT',
                                             'label'     => $this->language->_('Max Limit'),
                                             'sortable'  => true),
  	
                    'status'        => array('field'     => 'd.DAILYLIMIT_STATUS',
                                             'label'     => $this->language->_('Status'),
                                             'sortable'  => true),
  	
  	                'latestSuggestion'     => array('field'  => 'd.SUGGESTED',
                                               'label'       => $this->language->_('Last Suggested'),
                                               'sortable'    => true),
    
                    // 'latestSuggestor'   => array('field'  => 'd.SUGGESTEDBY',
                    //                            'label'    => $this->language->_('Last Suggester'),
                    //                            'sortable' => true),
    
                    'latestApproval'    => array('field'  => 'd.UPDATED',
                                               'label'    => $this->language->_('Last Approved'),
                                               'sortable' => true),
    
                    // 'latestApprover'  => array('field'    => 'd.UPDATEDBY',
                    //                            'label'    => $this->language->_('Latest Approver'),
                    //                            'sortable' => true)
  	                );

    $filterlist = array('Last Suggestion Date','Last Approval Date','LATEST_SUGGESTER','LATEST_APPROVER','COMP_ID','COMP_NAME','CCY','USER_ID','STATUS');
    
    $this->view->filterlist = $filterlist;

    $page = $this->_getParam('page');
    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    $sortBy = $this->_getParam('sortby');
    $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
	$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
	/*echo $sortBy . '  '. $sortDir;
	die;*/

     $filterArr = array('filter' => array('StripTags','StringTrim'),
                     'COMP_ID'    => array('StripTags','StringTrim','StringToUpper'),
                     'COMP_NAME'  => array('StripTags','StringTrim','StringToUpper'),
                     'CCY'  => array('StripTags','StringTrim','StringToUpper'),
                     'USER_ID'    => array('StripTags','StringTrim','StringToUpper'),
                     'STATUS' => array('StripTags','StringToUpper'),
                     'Last Suggestion Date' => array('StripTags','StringTrim','StringToUpper'),
                     'Last Suggestion Date_END'     => array('StripTags','StringTrim'),
                     'LATEST_SUGGESTER'      => array('StripTags','StringTrim','StringToUpper'),
                     'Last Approval Date'   => array('StripTags','StringTrim','StringToUpper'),
                     'Last Approval Date_END'       => array('StripTags','StringTrim'),
                     'LATEST_APPROVER'       => array('StripTags','StringTrim','StringToUpper'),
                    );

    $validators = array(
                    'filter' => array(),
                      'COMP_ID'    => array(),
                       'COMP_NAME'  => array(),
                       'CCY'  => array(),
                       'USER_ID'    => array(),
                       'STATUS' => array(),     
                       'Last Suggestion Date' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                       'Last Suggestion Date_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                       'LATEST_SUGGESTER'      => array(),
                       'Last Approval Date'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                       'Last Approval Date_END'       => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                       'LATEST_APPROVER'       => array(),
                    );

                      
     $dataParam = array('LATEST_SUGGESTER','LATEST_APPROVER','COMP_ID','COMP_NAME','CCY','USER_ID','STATUS');
      $dataParamValue = array();
        
      $clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
      $dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
    // print_r($dataParam);die;

      // print_r($output);die;
      // print_r($this->_request->getParam('wherecol'));
      foreach ($dataParam as $no => $dtParam)
      {
      
        if(!empty($this->_request->getParam('wherecol'))){
          $dataval = $this->_request->getParam('whereval');
          // print_r($dataval);
          $order = 0;
            foreach ($this->_request->getParam('wherecol') as $key => $value) {
              if($value == "Last Approval Date" || $value == "Last Suggestion Date"){
                  $order--;
                }
              if($dtParam==$value){
                $dataParamValue[$dtParam] = $dataval[$order];
              }
              $order++;
            }
          
        }
      }
      // print_r($dataParamValue);
      // die; 
        if(!empty($this->_request->getParam('lsdate'))){
          $lsarr = $this->_request->getParam('lsdate');
            $dataParamValue['Last Suggestion Date'] = $lsarr[0];
            $dataParamValue['Last Suggestion Date_END'] = $lsarr[1];
        }
        if(!empty($this->_request->getParam('ladate'))){
          $laarr = $this->_request->getParam('ladate');
            $dataParamValue['Last Approval Date'] = $laarr[0];
            $dataParamValue['Last Approval Date_END'] = $laarr[1];
        }


    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
    $filter     = $this->_getParam('filter');
    
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;
      
  	$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
	if(count($temp)>1){
      	if($temp[0]=='F' || $temp[0]=='S'){
      		if($temp[0]=='F')
        		$this->view->error = 1;
        	else
        		$this->view->success = 1;
      		$msg = ''; unset($temp[0]);
      		foreach($temp as $value)
      		{
      			if(!is_array($value))
      				$value = array($value);
      			$msg .= $this->view->formErrors($value);
      		}
        	$this->view->user_msg = $msg;
     	}	
    }

	$select = $this->_db->select()
					->from(array('d' => 'M_DAILYLIMIT'))
          ->JOINLEFT (array('MU' => 'M_USER'),'MU.USER_ID = d.USER_LOGIN',array('MU.USER_FULLNAME','MU.USER_ID'))
					->join(array('c' => 'M_CUSTOMER'),'d.CUST_ID = c.CUST_ID',array('CUST_NAME','company'  => new Zend_Db_Expr("CONCAT(c.CUST_NAME , ' (' , c.CUST_ID , ')  ' )"),
                    'user'  => new Zend_Db_Expr("CONCAT(MU.USER_FULLNAME , ' (' , MU.USER_ID , ')  ' )")));

    if($filter == $this->language->_('Set Filter'))
    {
      $fcust_id    = html_entity_decode($zf_filter->getEscaped('COMP_ID'));
      $fcust_name  = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
      $fuser_id    = html_entity_decode($zf_filter->getEscaped('USER_ID'));
      $fccy_id     = html_entity_decode($zf_filter->getEscaped('CCY'));
      $status      = html_entity_decode($zf_filter->getEscaped('STATUS'));
      
	 $latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('Last Suggestion Date'));
    $latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('Last Suggestion Date_END'));
    $latestSuggestor        = html_entity_decode($zf_filter->getEscaped('LATEST_SUGGESTER'));
    $latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('Last Approval Date'));
    $latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('Last Approval Date_END'));
    $latestApprover         = html_entity_decode($zf_filter->getEscaped('LATEST_APPROVER'));
		
		//konversi date agar dapat dibandingkan
		$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom,$this->_dateDisplayFormat))?
								   new Zend_Date($latestSuggestionFrom,$this->_dateDisplayFormat):
								   false;
		
		$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo,$this->_dateDisplayFormat))?
								   new Zend_Date($latestSuggestionTo,$this->_dateDisplayFormat):
								   false;
														   
		$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom,$this->_dateDisplayFormat))?
								   new Zend_Date($latestApprovalFrom,$this->_dateDisplayFormat):
								   false;

		$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo,$this->_dateDisplayFormat))?
								   new Zend_Date($latestApprovalTo,$this->_dateDisplayFormat):
								   false;		
	  
								 	  
      $this->view->fcust_id   = $fcust_id;
      $this->view->fcust_name = $fcust_name;
      $this->view->fuser_id   = $fuser_id;
      $this->view->fccy_id    = $fccy_id;
      $this->view->status     = $status;

      if($fcust_id)    $select->where('UPPER(d.CUST_ID) LIKE '.$this->_db->quote('%'.$fcust_id.'%'));
      if($fcust_name)  $select->where('UPPER(c.CUST_NAME) LIKE '.$this->_db->quote('%'.$fcust_name.'%'));
      if($fuser_id)    $select->where('UPPER(d.USER_LOGIN) LIKE '.$this->_db->quote('%'.$fuser_id.'%'));
      if($fccy_id)     $select->where('UPPER(d.CCY_ID) LIKE '.$this->_db->quote('%'.$fccy_id.'%'));
      if($status)      $select->where('d.DAILYLIMIT_STATUS='.$status);
      if($latestSuggestor)  $select->where('UPPER(d.SUGGESTEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestSuggestor).'%'));
	  if($latestApprover)   $select->where('UPPER(d.UPDATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestApprover).'%'));
    
      //filter date
		if($latestSuggestionFrom)  $select->where("DATE(d.SUGGESTED) >= DATE(".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
		if($latestSuggestionTo)    $select->where("DATE(d.SUGGESTED) <= DATE(".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");
		if($latestApprovalFrom)    $select->where("DATE(d.UPDATED) >= DATE(".$this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)).")");
		if($latestApprovalTo)      $select->where("DATE(d.UPDATED) <= DATE(".$this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)).")");

		$this->view->latestSuggestor  = $latestSuggestor;
		$this->view->latestApprover   = $latestApprover;
		
		if($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
		if($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
		if($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
		if($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);

	
    }
    
    
    //utk sorting
    $select->order($sortBy.' '.$sortDir);
          

    $select = $select->query()->fetchAll();		

    
    $this->paging($select);
    $this->view->fields = $fields;
    $this->view->filter = $filter;
  	
  	//$this->view->status_type = $this->_masteruserStatus;
    $this->view->modulename = $this->_request->getModuleName();
  	$this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
    $this->view->statusDesc = $this->_masterglobalstatus['desc'];
  	
  	
  	
    //insert log
    try 
    {
	  $this->_db->beginTransaction();
	  if(!$this->_request->isPost()){
	  Application_Helper_General::writeLog('DLLS','Viewing Customer > User Daily Limit List');
	  }
      $this->_db->commit();
	}
	catch(Exception $e) 
	{
	  $this->_db->rollBack();
  	  SGO_Helper_GeneralLog::technicalLog($e);
	}

  if(!empty($dataParamValue)){
          $this->view->lsdateStart = $dataParamValue['Last Suggestion Date'];
          $this->view->lsdateEnd = $dataParamValue['Last Suggestion Date_END'];
          $this->view->ladateStart = $dataParamValue['Last Approval Date'];
          $this->view->ladateEnd = $dataParamValue['Last Approval Date_END'];

            unset($dataParamValue['Last Suggestion Date_END']);
          unset($dataParamValue['Last Approval Date_END']);
      foreach ($dataParamValue as $key => $value) {
        $wherecol[] = $key;
        $whereval[] = $value;
      }
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }

  
  }
}