<?php
require_once 'Zend/Controller/Action.php';


class Chargestemplatelist_AddController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$this->view->ccyArr = Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('CTAD','View Add Charge Template page');
		}
		if(is_array($ccyArr))
		{
	        	$ccyArrValidate = Application_Helper_Array::simpleArray($ccyArr,'CCY_ID');
		}
		
			$this->view->template_name	 	= $this->_getParam('template_name');
			$this->view->charge_skn_curr 	= $this->_getParam('charge_skn_curr');
			$this->view->charge_rtgs_curr 	= $this->_getParam('charge_rtgs_curr');
			$this->view->charge_rtgs 		= $this->_getParam('charge_rtgs');
			$this->view->charge_skn 		= $this->_getParam('charge_skn');
		
			$chargertgs = $this->_getParam('charge_rtgs');
			$chargeskn 	= $this->_getParam('charge_skn');
			
			$tempchargertgs = Application_Helper_General::convertDisplayMoney($chargertgs);
			$tempchargeskn = Application_Helper_General::convertDisplayMoney($chargeskn);
			
			$chargertgs = str_replace('.','',$tempchargertgs);
			$chargeskn = str_replace('.','',$tempchargeskn);
			
			$this->_setParam('charge_rtgs',$chargertgs);
			$this->_setParam('charge_skn',$chargeskn);
			
			$filters = array(
			                 'template_name'    => array('StripTags'),
							 'charge_skn_curr' 	=> array('StringTrim','StripTags'),
							 'charge_rtgs_curr' => array('StringTrim','StripTags'),
							 'charge_rtgs'    	=> array('StringTrim','StripTags'),
							 'charge_skn'    	=> array('StringTrim','StripTags')
							);
		 		
			$validators = array(
			                    'template_name' 	=>	array	(
			                    								'NotEmpty',
			                                         			new Zend_Validate_StringLength(array('max'=>128)),
			                                         			array('Db_NoRecordExists',array('table'=>'M_TEMPLATE_CHARGES_OTHER','field'=>'TEMPLATE_NAME')),
																array('Db_NoRecordExists',array('table'=>'TEMP_TEMPLATE_CHARGES_OTHER','field'=>'TEMPLATE_NAME')),
																'messages'	=> array(
																         			$this->language->_('Can not be empty'),
																         			$this->language->_('Data too long (max 35 chars)'),
																					$this->language->_('Charge Template Name already in use. Please use another'),
																					$this->language->_('Charge Template Name already suggested. Please use another')
			                                            							)
																),
																
								'charge_skn_curr' 	=> 	array	(
																'NotEmpty',
			                                         			new Zend_Validate_StringLength(array('max'=>3)),
													 			'messages'	=> array(
																         			$this->language->_('Can not be empty'),
																         			$this->language->_('Data too long (max 3 chars)'),
			                                                             			)
																),
																					
								'charge_rtgs_curr'  => 	array	(
																'NotEmpty',
													 			new Zend_Validate_StringLength(array('max'=>3)),
													 			'messages' => array(
													                     			$this->language->_('Can not be empty'),
																         			$this->language->_('Data too long (max 3 chars)'),
																         			)
																),
																
								'charge_rtgs'      => 	array	(
																'NotEmpty',	 
													 			new Zend_Validate_StringLength(array('max'=>15)),
													 			'Digits',
													 			'messages' => array(
													                     			$this->language->_('Can not be empty'),
																         			$this->language->_('Data too long (max 15 chars)'),
																		 			$this->language->_('Entry Must be Numbers')
																         			)
																),
																
								'charge_skn'      => 	array	(
																'NotEmpty',
													 			new Zend_Validate_StringLength(array('max'=>15)),
													 			'Digits',
													 			'messages' => array(
													                     			$this->language->_('Can not be empty'),
																         			$this->language->_('Data too long (max 15 chars)'),
																		 			$this->language->_('Entry Must be Numbers')
																         			)
																),
							   );
		
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			$re = '%^[a-zA-Z0-9+\-]*$%';		
			$cekspecialchar = preg_match($re, $zf_filter_input->template_name)? 1 : 0;
			
			$submit = $this->_getParam('submit');
			
			if($submit && $zf_filter_input->isValid() && $this->view->hasPrivilege('CTAD'))
			{
				Application_Helper_General::writeLog('CTAD','Add Charge Template');
				$this->_db->beginTransaction();
				try 
				{
					$changeInfo= "New Template Charge Other";
					$changeType = $this->_changeType['code']['new'];
					$keyField = '';
					$keyValue = $zf_filter_input->template_name;

					$masterTable = 'M_TEMPLATE_CHARGES_OTHER,M_TEMPLATE_CHARGES_OTHER_DETAIL';
					$tempTable =  'TEMP_TEMPLATE_CHARGES_OTHER,TEMP_TEMPLATE_CHARGES_OTHER_DETAIL';
				
					$displayTableName = 'Charges Template';
				//var_dump($dataforinsert);exit;					
					$changesId = $this->suggestionWaitingApproval($displayTableName, $changeInfo, $changeType,'',$masterTable,$tempTable,$keyField,$keyValue);
				
			    	$content = array(
							'TEMPLATE_NAME' => $zf_filter_input->template_name,
							'CREATED' 		=> date("Y-m-d H:i:s"),
							'CREATEDBY' 	=> $this->_userIdLogin,
							'SUGGESTED' 	=> date("Y-m-d H:i:s"),
							'SUGGESTEDBY' 	=> $this->_userIdLogin,
							'CHANGES_ID' 	=> $changesId,
			    			'STATUS'		=> '1'
						    );
				//insert into temp
					$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER',$content);
					
					$lastIdTemp = $this->_db->fetchOne('select @@identity');
				
					$content2 = array(
							'TEMP_ID' 		=> 	$lastIdTemp,
							'TEMPLATE_NAME' => 	$zf_filter_input->template_name,
							'CHARGES_TYPE' 	=> 	1,
							'CHARGES_CCY' 	=>	$zf_filter_input->charge_rtgs_curr,
							'CHARGES_AMT' 	=> 	$tempchargertgs,
							'CHANGES_ID' 	=>	$changesId
						    );
					$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content2);	

                	$content3 = array(
                			'TEMP_ID' 		=> 	$lastIdTemp,
							'TEMPLATE_NAME' => 	$zf_filter_input->template_name,
							'CHARGES_TYPE' 	=> 	2,
							'CHARGES_CCY' 	=>	$zf_filter_input->charge_skn_curr,
							'CHARGES_AMT' 	=> 	$tempchargeskn,
							'CHANGES_ID' 	=> 	$changesId
						    );
					$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content3);
					
					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
					$this->_redirect('/notification/submited');
				} 
				catch (Exception $e) 
				{
					$this->_db->rollBack();	
				}
			}
			if($submit && !$zf_filter_input->isValid())
			{
				foreach($zf_filter_input->getMessages() as $key=>$err)
				{
					$xxx = 'x'.$key;
					$this->view->$xxx = $this->displayError($err);
				}
				
				if (!$cekspecialchar)
				{		
					$this->view->xtemplate_name = $this->language->_('Must not contain special characters');
					$this->view->template_name	= '';
				}
				$templatename = $this->_getParam('template_name');
			}
	}
}