<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'Crypt/AESMYSQL.php';

class airlines_IndexController extends airlines_Model_Airlines
{

	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$password = $sessionNamespace->token; 

		$this->_helper->layout()->setLayout('newlayout');   

		//get airlines data
		$airlinesData = $this->getAirlines();

		foreach ($airlinesData as $key => $value) {
			$airlinesArr[$value['AIRLINE_CODE']] = $value['AIRLINE_NAME'];
		}

		$this->view->airlinesArr = $airlinesArr;

		if ($this->_request->getParam('print')) {
			$data_status = $this->_getParam('data_status');
			// var_dump($data_status);die();
			
			$airlinesData = $this->getAllAirlinesSettings($data_status);
			
		}else{
			$airlinesData = $this->getAllAirlinesSettings(1);
		}
		
		$clientUser  =  new SGO_Soap_ClientUser();
		
		foreach ($airlinesData as $key => $value) {

			$transaction_info = json_decode($value['TRANSACTION_INFO'], 1);
			$currentBalance = 0;
			$successCheckBalance = 0;
			$travelagentArr = array();

			foreach ($transaction_info as $row => $rowVal) {
				if (!in_array($rowVal['travelagentid'], $travelagentArr)) {
					$paramObj = array(
						'airline_code' => $value['AIRLINE_CODE'],
						'AUTH_USER' => 'travel',
						'AUTH_PASS' => 'test',
						'TRAVELAGENT_ID' => $rowVal['travelagentid']
					);

					//check balance
					$success = $clientUser->callapi('airlinesDepositInquiry',$paramObj,'airlines/agent/info');
			
					if($clientUser->isTimedOut()){
						$returnStruct = array(
								'ResponseCode'	=>'XT',
								'ResponseDesc'	=>'Service Timeout',					
								'Cif'			=>'',
								'AccountList'	=> '',
						);
					}else{
						
						$result  = $clientUser->getResult();

						if($result->error_code == '0000'){
							$currentBalance += $result->account_info->available_credit;
							$successCheckBalance++;
						}
					}
					array_push($travelagentArr, $rowVal['travelagentid']);
				}
			}

			if ($successCheckBalance == count($travelagentArr)) {
				
				$airlinesData[$key]['CURRENT_BALANCE'] = $currentBalance;
				$airlinesData[$key]['LAST_UPDATED'] = date('d-M-Y H:i:s');

				if ($currentBalance < $value['WARN_LEVEL2']) {
					$airlinesData[$key]['BALANCE_STATUS'] = 'danger';
				}
				else if($currentBalance < $value['WARN_LEVEL1']){
					$airlinesData[$key]['BALANCE_STATUS'] = 'warning';
				}

			}
			//harusnya error failed inquiry
			else{
				$currentBalance = rand(50000000, 100000000);

				$airlinesData[$key]['CURRENT_BALANCE'] = $currentBalance;
				$airlinesData[$key]['LAST_UPDATED'] = date('d-M-Y H:i:s');

				if ($currentBalance < $value['WARN_LEVEL2']) {
					$airlinesData[$key]['BALANCE_STATUS'] = 'danger';
				}
				else if($currentBalance < $value['WARN_LEVEL1']){
					$airlinesData[$key]['BALANCE_STATUS'] = 'warning';
				}
			}
			if($value['AIRLINE_CODE'] == 'QG'){
				$airlinesData[$key]['CURRENT_BALANCE'] = 7317800 ;
			}

		}

		$totalna = 0;
		foreach ($airlinesData as $key => $value) {

			if ($value['CURRENT_BALANCE'] == NULL || empty($value['CURRENT_BALANCE'])) {
				$totalna++;
			}

			if($totalna>=1){
				$this->view->totalna = $totalna;
			}
		}

		$this->view->airlinesData = $airlinesData;

		$this->view->compName = $this->_custNameLogin;


		$param = array(
			'CUST_ID' => $this->_custIdLogin
		);
		//get airlines data
		$airlinesLogData = $this->getAirlinesLog('*', $param);

		foreach ($airlinesLogData as $key => $value) {
			if ($value['STATUS'] == 3 || $value['STATUS'] == 4) {
				$airlinesLogData[$key]['DESCRIPTION'] = '<span class="text-danger">'.$value['DESCRIPTION'].'</span>';
			}
			else if($value['STATUS'] == 2){
				$airlinesLogData[$key]['DESCRIPTION'] = '<span class="text-success">'.$value['DESCRIPTION'].'</span>';
			}
		}

		$this->view->airlinesLogData = $airlinesLogData;

		$path = '/assets/themes/assets/newlayout/img/';
		$airlinesImg = array(
			'QZ' => $path.'airasia-indonesia.png',
			'QG' => $path.'citilink.png',
			'JT' => $path.'lionair.png',
			'FY' => $path.'firefly.png',
			'8b' => $path.'transnusa.png',
			'LA' => $path.'linkaja.png'
		);

		$this->view->airlinesImg = $airlinesImg; 

		$airlines = $this->_getParam('airlines');
		$csv = $this->_getParam('csv');

		if ($csv) {

			if($airlines){

				$airlineCode = $airlines;
			    $rowVal = null;
			    $startDate = null;
			    $endDate = null;
			    $modal = 1;

		    	$account = $this->getAccountList();
		        
		        foreach ($account as $key => $value) {
		        	$newAccount[$value['account_number']] = $value;
		        }

			    $select = $this->_db->select()
		                ->from( array( 'TT' => 'T_AIRLINES_LOG', 
		                		array('*'))
		            		  )
		                ->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.BANK_CODE = D.BANK_CODE',array('D.BANK_NAME'))
		                ->where('TT.CUST_ID = ?', (string)$this->_custIdLogin);

		        if ($airlineCode != 'all') {
		         	$select->where('AIRLINE_CODE = ?', (string)$airlineCode);
		        }

		        if ($modal == 1) {
		        	$select->where('STATUS = 2 OR STATUS = 4');
		        	$select->limit(10);
		        }

		        if ($rowVal == 'date') {
		        	$select->WHERE('DATE_TIME >= ?', $startDate.' 00:00:00')
						   ->WHERE('DATE_TIME <= ?', $endDate.' 23:59:59');
		        }
		        else if($rowVal == '5'){
		        	$select->limit(5);
		        } 
		        else if($rowVal == '10'){
		        	$select->limit(10);
		        }

		        $select->order('DATE_TIME DESC');

		        $airlinesLogData = $this->_db->fetchAll($select);

		        //get airlines data
				$airlinesData = $this->getAirlines();

				foreach ($airlinesData as $key => $value) {
					$airlinesArr[$value['AIRLINE_CODE']] = $value['AIRLINE_NAME'];
				}

				$headerData[] = array($this->language->_('Date Time'),$this->language->_('Bank Name'),$this->language->_('From Source Account'),$this->language->_('Top Up Amount'));

				foreach ($airlinesLogData as $key => $value) { 

		    		// $dateTime = date_create($value['DATE_TIME']); 
				    $newDateTime = date("d M Y H:i:s", strtotime($value['DATE_TIME']));

				    if (empty($newAccount[$value['SOURCE_ACCOUNT']]['account_alias'])) {
				    	$alias = '';
				    }
				    else{
				    	$alias = ' - '.$newAccount[$value['SOURCE_ACCOUNT']]['account_alias'];
				    }

				    $paramTrx = array(	
										"DATE_TIME"  			=> $newDateTime,
										"BANK_NAME"				=> $value['BANK_NAME'],
										"SOURCE_ACCOUNT"  		=> $value['SOURCE_ACCOUNT'].' - '.$alias,
										"TOPUP_AMOUNT" 			=> 'IDR '.str_replace('.00', '', Application_Helper_General::displayMoney($value['TOPUP_AMOUNT'])),
								);
					$newData[] = $paramTrx;

				}

				$datenow = date("Ymd");
				$this->_helper->download->csv($headerData,$newData,null,'Automatic_Topup'.$this->_userIdLogin.'_'.$datenow); 

			}
			
		}

		if ($this->_request->getParam('print')) {

			$setting = new Settings();
			$master_bank_app_name = $setting->getSetting('master_bank_app_name');
			$master_bank_name = $setting->getSetting('master_bank_name');
			$masterbank = $master_bank_app_name . ' - ' . $master_bank_name; 

			if ($data_status == 1) {
				$temp_status = 'Active';
			}else{
				$temp_status = 'Non Active';
			}

			$paramsdata= array(
	          'data_caption'    => $masterbank, 
	          'airlinesData'    => $airlinesData, 
	          'airlinesImg'     => $airlinesImg,
	          'data_compname'	=> $this->_custNameLogin,
	          'data_status'		=> $temp_status,
	        );

			$this->_forward('printautotopup', 'index', 'widget', $paramsdata);
		}
		
	}

	public function deleteAction(){

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


		$this->_helper->layout()->setLayout('newpopup');
		$AESMYSQL = new Crypt_AESMYSQL();
		$airlines = $AESMYSQL->decrypt($this->_getParam('airlines'), $password);

		$data = array(
			'CUST_ID'				=> $this->_custIdLogin,
			'AIRLINE_CODE'			=> $airlines
		);

		//get airlines data
		$airlinesData = $this->getAirlines();
		foreach ($airlinesData as $key => $value) {
			$airlinesArr[$value['AIRLINE_CODE']] = $value['AIRLINE_NAME'];
		}

		$this->_db->beginTransaction();

		try {

			$change_id = $this->suggestionWaitingApproval('Airline Setting','Delete airline setting',strtoupper($this->_changeType['code']['delete']),null,'M_AIRLINES_SETTINGS','TEMP_AIRLINES_SETTINGS',$airlines,$airlinesArr[$airlines],$this->_custIdLogin);

			$data['CHANGES_ID'] = $change_id;

			$this->_db->insert('TEMP_AIRLINES_SETTINGS',$data);
			$successInsert = true;

			// $where['CUST_ID = ?'] = $this->_custIdLogin;
			// $where['AIRLINE_CODE = ?'] = $airlines;
			// $delete = $this->_db->delete('M_AIRLINES_SETTINGS', $where);
			$delete = true;

		} catch (Exception $e) {
			$delete = false;
			$this->_db->rollBack();
			$this->view->error = true;
			$error_msg[] = 'Failed to delete data';
			$this->view->error_msg = $error_msg;
		}

		if ($delete) {
			$this->_db->commit();	

			$this->setbackURL('/'.$this->_request->getModuleName());
			$this->_redirect('/notification/submited');
		}
	}

	public function changeairlineslogAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $airlineCode = $this->_getParam('airlinecode');
	    $rowVal = $this->_getParam('rowval');
	    $startDate = $this->_getParam('startdate');
	    $endDate = $this->_getParam('enddate');
	    $modal = $this->_getParam('modal');

	    //get account list data
    	$account = $this->getAccountList();
        
        foreach ($account as $key => $value) {
        	$newAccount[$value['account_number']] = $value;
        }

	    $select = $this->_db->select()
                ->from('T_AIRLINES_LOG', '*')
                ->where('CUST_ID = ?', (string)$this->_custIdLogin);

        if ($airlineCode != 'all') {
         	$select->where('AIRLINE_CODE = ?', (string)$airlineCode);
        }

        if ($modal == 1) {
        	$select->where('STATUS = 2 OR STATUS = 4');
        	$select->limit(10);
        }

        if ($rowVal == 'date') {
        	$select->WHERE('DATE_TIME >= ?', $startDate.' 00:00:00')
				   ->WHERE('DATE_TIME <= ?', $endDate.' 23:59:59');
        }
        else if($rowVal == '5'){
        	$select->limit(5);
        } 
        else if($rowVal == '10'){
        	$select->limit(10);
        }

        $select->order('DATE_TIME DESC');

        $airlinesLogData = $this->_db->fetchAll($select);

        //get airlines data
		$airlinesData = $this->getAirlines();

		foreach ($airlinesData as $key => $value) {
			$airlinesArr[$value['AIRLINE_CODE']] = $value['AIRLINE_NAME'];
		}

		$tableData = '';

        if ($modal == 1) {
        	if (!empty($airlinesLogData)) { 

        		foreach ($airlinesLogData as $key => $value) { 

        		$dateTime = date_create($value['DATE_TIME']); 
			    $newDateTime = date_format($dateTime, "d M Y H:i:s");

			    if (empty($newAccount[$value['SOURCE_ACCOUNT']]['account_alias'])) {
			    	$alias = '';
			    }
			    else{
			    	$alias = ' - '.$newAccount[$value['SOURCE_ACCOUNT']]['account_alias'];
			    }

        		$tableData .= ' <tr>
									<td>'.$newDateTime.'</td>
									<td>
										<div class="col-md-12 row">
											<div class="col-md-3">
												<img src="'.$newAccount[$value['SOURCE_ACCOUNT']]['BANK_IMG'].'" style="background-position: left 100px;" width="80px">
											</div>
											<div class="col-md-9">
												'.$value['SOURCE_ACCOUNT'].$alias.'
											</div>
										</div>
									</td>
									<td><span class="float-left">IDR</span><span class="float-right">'.Application_Helper_General::displayMoney($value['TOPUP_AMOUNT']).'</span></td>
								</tr>';
				}

        	}
        	else{
        		$tableData .= '<tr>
									<td colspan="5" align="center" class="textBlack">------------- No Data -------------</td>
							  </tr>';
        	}
        }
        else{

			if (!empty($airlinesLogData)) { 

				foreach ($airlinesLogData as $key => $value) {
					if ($value['STATUS'] == 3 || $value['STATUS'] == 4) {
						$airlinesLogData[$key]['DESCRIPTION'] = '<span class="text-danger">'.$value['DESCRIPTION'].'</span>';
					}
					else if($value['STATUS'] == 2){
						$airlinesLogData[$key]['DESCRIPTION'] = '<span class="text-success">'.$value['DESCRIPTION'].'</span>';
					}
				}

				foreach ($airlinesLogData as $key => $value) { 

					if (empty($value['TOPUP_AMOUNT'])) {
						$topupAmount = '-';
					} 
					else{
						$topupAmount = Application_Helper_General::displayMoney($value['TOPUP_AMOUNT']);
					}

					$dateTime = date_create($value['DATE_TIME']); 
			    	$newDateTime = date_format($dateTime, "d M Y H:i:s");

					$tableData .= ' <tr>
										<td>'.$airlinesArr[$value['AIRLINE_CODE']].'</td>
										<td>'.$value['DESCRIPTION'].'</td>
										<td>'.$topupAmount.'</td>
										<td>'.$newDateTime.'</td>
									</tr>';
				}

			}else { 
				$tableData .= '<tr>
									<td colspan="5" align="center" class="textBlack">------------- No Data -------------</td>
							  </tr>';
			} 
        }

		

		echo $tableData; 
	}

	function balanceupdatecacheAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'MAGR'.$this->_custIdLogin;

                       
			                $updateArr['CUST_BALANCE'] = '0';
			                $whereArr = array( 'CUST_ID = ?' => $this->_custIdLogin);
			                $balanceupdate = $this->_db->update('M_CUSTOMER',$updateArr,$whereArr);
        

        $cache->remove($cacheID);
        return true;
	}

	function balancenaAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'MAGR'.$this->_custIdLogin;
        $data = $cache->load($cacheID);



        $complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );	
    	$comp = "'";
    	foreach ($complist as $key => $value) {
    		$comp .= "','".$value['CUST_ID']."','";
    	}
    	$comp .= "'";

        $acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANK_TABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->order('A.APIKEY_ID ASC')
				);
						 // echo $acctlist;die;
		
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['cust'] = $value['CUST_ID'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}


		 $clientUser  =  new SGO_Soap_ClientUser();
    	
	//	echo '<pre>';
		//var_dump($data['datapers']);die;
	    foreach ($data['datapers'] as $k => $val) {
			//var_dump($val);
			//if($val['account_number'] == '46613930121')
				//$val['account_balance'] ='N/A';
			//}	
	      	if($val['account_balance'] == 'N/A' || $val['account_balance'] == NULL){
	        		foreach ($account as $key => $request) {

	        				if($request['BANK_CODE'] == $val['bank_code'] && $request['account_number'] == $val['account_number']){
	        					$success = $clientUser->callapi('balance',$request,'b2b/inquiry/balance');
    							$result  = $clientUser->getResult();
								//var_dump($result);
								$data['datapers'][$k]['account_balance'] = $result->account_balance;
								//var_dump($request);die;
								$temmpbalance =  $this->_db->select()
					              ->from(array('A' => 'M_BALANCE'))
					              ->where("A.ACCT_NO = ".$this->_db->quote($request['account_number']))
					              ->where("A.BANK_CODE = ".$this->_db->quote($request['BANK_CODE']))
								  ->where("A.CUST_ID = ".$this->_db->quote($request['cust']));
								 
								//echo $temmpbalance;
								  $balance = $this->_db->fetchAll($temmpbalance);
								//  die('here');
								
								 
					
					             if(empty($balance)){
					                  $param = array(
					                     'ACCT_NO' => $request['account_number'],
					                     'BANK_CODE' => $request['BANK_CODE'],
										 'ACCT_NAME' => $result->account_name,
					                     'CUST_ID' => $request['cust'],
					                     'CCY' => $result->account_currency,
					                     'BALANCE' => $result->account_balance,
					                     'UPDATED' => new Zend_Db_Expr("now()"),
					                     'RABBIT_ID' => (string)$request['id']
					                 );
					                 $this->_db->insert('M_BALANCE',$param);
					             }else{
					                $updateArr['BALANCE'] = $result->account_balance;
									$updateArr['ACCT_NAME']	= $result->account_name;
									$updateArr['RABBIT_ID']	= (string)$request['id'];
					                $updateArr['UPDATED'] = new Zend_Db_Expr('now()');
					                $whereArr = array( #'CUST_ID = ?' => (string)$custId,
					                        'ACCT_NO = ?' => (string)$request['account_number'],
					                        'BANK_CODE = ? ' => (string)$request['BANK_CODE'],
					                        'CUST_ID = ? ' => (string)$request['cust']
					                    );

					                try{
					                	$balanceupdate = $this->_db->update('M_BALANCE',$updateArr,$whereArr);
					                }catch (Exception $e) {
					                    print_r($e);die;
									}
					            } 
								
								//insert T_DIGI_LOG
								$paramlog = array(
						            'DIGI_USER' => $this->_userIdLogin,
						            'DIGI_CUST' => $this->_custIdLogin,
						            'DIGI_BANK' => $request['BANK_CODE'],
						            'DIGI_ACCOUNT' => $request['account_number'],
						            'DIGI_ERROR_CODE' => $result->error_code,
						            'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
						            'DIGI_SERVICE'  => 1
						        );

						        $this->_helper->ServiceLog->serviceLog($paramlog);
	        				}

					}
	        }
	       	
	    }
	    $cache->save($data,$cacheID);


	}

	public function lastupdatedAction(){
// 		ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
	
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // $rsdate = $this->_getParam('rsdate');

         $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'MAGR'.$this->_custIdLogin;
        $data = $cache->load($cacheID);

		
		//var_dump($data);die;
		if(!empty($data)){
			$save['lastupdate']	= date('Y-m-d H:i:s');
			$save['datapers'] = $data['datapers'];
			$save['totalmanual'] = $data['totalmanual']-$balance['0']['PLAFOND'];
			$save['totalonline'] = $data['totalonline'];	
			$cache->save($save,$cacheID);
			$dateupdate = date("d-M-Y H:i:s");
			//echo $save['lastupdate'];
			echo $dateupdate.',-';
			
		}else{
		
	
		// foreach ($account as $key => $request) {
			# code...
				
			
				$balance = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'T_RABBIT_FLAG'),array('DONE','USER_ID'))
						 
						 
						 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
						 ->order('A.ID DESC')
						 // ->where("A.ACCT_NO = ? ",$request['account_number'])
						 // ->where("A.BANK_CODE = ? ",$request['BANK_CODE'])
						 
				);

						 // var_dump($balance);
				if(!empty($balance)){
				// print_r($balance);die;
				$date=date_create($balance['0']['DONE']);
				$dateupdate = date_format($date,"d-M-Y H:i:s");
				
					echo $dateupdate.','.$balance['0']['USER_ID'];
				}
			// }
		}
        	
	}

	public function changestatusAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 

        $status = $this->_getParam('status');
        $priv = $this->_getParam('priv');

        $airlinesData = $this->getAllAirlinesSettings($status);

		$clientUser  =  new SGO_Soap_ClientUser();
		foreach ($airlinesData as $key => $value) {

			$transaction_info = json_decode($value['TRANSACTION_INFO'], 1);
			$currentBalance = 0;
			$successCheckBalance = 0;
			$travelagentArr = array();

			foreach ($transaction_info as $row => $rowVal) {
				if (!in_array($rowVal['travelagentid'], $travelagentArr)) {
					$paramObj = array(
						'airline_code' => $value['AIRLINE_CODE'],
						'AUTH_USER' => 'travel',
						'AUTH_PASS' => 'test',
						'TRAVELAGENT_ID' => $rowVal['travelagentid']
					);

					//check balance
					$success = $clientUser->callapi('airlinesDepositInquiry',$paramObj,'airlines/agent/info');
			
					if($clientUser->isTimedOut()){
						$returnStruct = array(
								'ResponseCode'	=>'XT',
								'ResponseDesc'	=>'Service Timeout',					
								'Cif'			=>'',
								'AccountList'	=> '',
						);
					}else{
						
						$result  = $clientUser->getResult();

						if($result->error_code == '0000'){
							$currentBalance += $result->account_info->available_credit;
							$successCheckBalance++;
						}
					}
					array_push($travelagentArr, $rowVal['travelagentid']);
				}
			}

			if ($successCheckBalance == count($travelagentArr)) {

				$airlinesData[$key]['CURRENT_BALANCE'] = $currentBalance;
				$airlinesData[$key]['LAST_UPDATED'] = date('d-M-Y H:i:s');

				if ($currentBalance < $value['WARN_LEVEL2']) {
					$airlinesData[$key]['BALANCE_STATUS'] = 'danger';
				}
				else if($currentBalance < $value['WARN_LEVEL1']){
					$airlinesData[$key]['BALANCE_STATUS'] = 'warning';
				}
			}
			//harusnya error failed inquiry
			else{
				$currentBalance = rand(50000000, 100000000);

				$airlinesData[$key]['CURRENT_BALANCE'] = $currentBalance;
				$airlinesData[$key]['LAST_UPDATED'] = date('d-M-Y H:i:s');

				if ($currentBalance < $value['WARN_LEVEL2']) {
					$airlinesData[$key]['BALANCE_STATUS'] = 'danger';
				}
				else if($currentBalance < $value['WARN_LEVEL1']){
					$airlinesData[$key]['BALANCE_STATUS'] = 'warning';
				}
			}
		}

		$path = '/assets/themes/assets/newlayout/img/';
		$airlinesImg = array(
			'QZ' => $path.'airasia-indonesia.png',
			'QG' => $path.'citilink.png',
			'JT' => $path.'lionair.png',
			'FY' => $path.'firefly.png',
			'8b' => $path.'transnusa.png'
		);

		$AESMYSQL = new Crypt_AESMYSQL();
		$tableData = '';
		foreach ($airlinesData as $key => $value) {

			$encrypted_airline = $AESMYSQL->encrypt($value['AIRLINE_CODE'], $password);                
			$encairline = urlencode($encrypted_airline);

			$tableData .= '<div class="col-md-4 pr-0">
							<div class="airlinesdiv '.$value['BALANCE_STATUS'].' d-flex justify-content-center" data-toggle="modal" data-target="#detailModal" data-airline="'.$value['AIRLINE_CODE'].'" data-encairline="'.$encairline.'" data-balance="'.$value['CURRENT_BALANCE'].'" data-status="'.$value['BALANCE_STATUS'].'">
								<div class="row col-md-12 pr-0">';

									if(!empty($value['BALANCE_STATUS'])){ 
										$tableData .= '<span class="indicator '.$value['BALANCE_STATUS'].'">!</span>';
									} 

					$tableData .= '<div class="col-md-4 my-auto pl-0" align="left">

										<img class="img-responsive" src="'.$airlinesImg[$value['AIRLINE_CODE']].'" width="75px">
										<br>';

					$textStyle = '';
					if (!empty($value['BALANCE_STATUS'])) {
						$textStyle = 'text'.ucfirst($value['BALANCE_STATUS']);
					}
										
					$tableData .= '</div>
									<div class="col-md-8 my-auto pr-1">
										<p class="balance '.$textStyle.'"><b>IDR '.Application_Helper_General::displayMoney($value['CURRENT_BALANCE']).'</b></p> 
									</div>

									<p class="lastUpdated homeCardText">'.$this->language->_('Last Update: '.$value['LAST_UPDATED']).'</p>
								</div>
							</div>
						</div>';
		}

		if ($priv) {

			$tableData .= '<div class="col-md-4 pr-0">
								<div class="airlinesdiv d-flex justify-content-center shadow" style="cursor: pointer;" onclick="addDeposit()">
									<i class="fas fa-plane my-auto homeCardText text-black-50" style="font-size: 30px;"></i>
									<h6 class="m-0 my-auto display-5 homeCardText text-black-50">&nbsp;&nbsp;&nbsp;<b>Add New Airlines</b></h6>
								</div>
							</div>';
		} 
		

		echo $tableData; 
	}

}
 