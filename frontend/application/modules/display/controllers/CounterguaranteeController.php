<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class display_CounterguaranteeController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $auth = Zend_Auth::getInstance()->getIdentity();

    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    $csv = $this->_getParam('csv');
    $pdf = $this->_getParam('pdf');
    $fields = array(
      'alias'      => array(
        'field' => 'PRINCIPAL_NUMBER',
        'label' => $this->language->_('Principle Agreement'),
        'sortable' => true
      ),
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('BG Number'),
      ),
      'startdate' => array(
        'field' => 'BG_AMOUNT',
        'label' => $this->language->_('BG Amount'),
      ),
      'BG_BRANCH'   => array(
        'field'    => 'BG_BRANCH',
        'label'    => $this->language->_('Bank Branch'),
      ),
      'branch'   => array(
        'field'    => 'PRINCIPAL_INSURANCE',
        'label'    => $this->language->_('Insurance Branch'),
      ),
      'enddate'   => array(
        'field'    => 'BG_ISSUED',
        'label'    => $this->language->_('Issue Date'),
      ),
      'recipient' => array(
        'field' => 'BG_CG_DEADLINE',
        'label' => $this->language->_('Deadline'),
      ),
    );
    $filterlist = array('PRINCIPAL_NUMBER', 'BG_NUMBER', 'BG_BRANCH', 'PRINCIPAL_INSURANCE', 'BG_ISSUED', 'BG_CG_DEADLINE');

    $this->view->filterlist = $filterlist;

    //get filtering param
    $filterArr = array(
      'filter'                  => array('StringTrim', 'StripTags'),
      'PRINCIPAL_NUMBER'         => array('StringTrim', 'StripTags'),
      'BG_NUMBER'               => array('StripTags'),
      'BG_BRANCH'               => array('StringTrim', 'StripTags'),
      'PRINCIPAL_INSURANCE'     => array('StripTags'),
      'BG_ISSUED'               => array('StringTrim', 'StripTags'),
      'BG_CG_DEADLINE'            => array('StringTrim', 'StripTags'),
      // 'BG_CG_DEADLINE_END'	          => array('StringTrim', 'StripTags'),
    );


    $dataParam = array('PRINCIPAL_NUMBER', 'BG_NUMBER', 'BG_BRANCH', 'PRINCIPAL_INSURANCE', 'BG_ISSUED', 'BG_CG_DEADLINE');
    $dataParamValue = array();
    foreach ($dataParam as $dtParam) {

      if (!empty($this->_request->getParam('whereco'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('whereco') as $key => $value) {
          if ($dtParam == $value) {
            if (!empty($dataParamValue[$dtParam])) {
              $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
            }

            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }

      // $dataPost = $this->_request->getPost($dtParam);
      // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
    }

    if (!empty($this->_request->getParam('createdate'))) {
      // echo "<code>tes = $BG_CG_DEADLINE_END</code><BR>"; die;
      $createarr = $this->_request->getParam('createdate');
      $dataParamValue['BG_CG_DEADLINE_START'] = $createarr[0];
      $dataParamValue['BG_CG_DEADLINE_END'] = $createarr[1];
    }

    if (!empty($this->_request->getParam('bgissuedate'))) {
      // echo "<code>tes = $BG_CG_DEADLINE_END</code><BR>"; die;
      $bgissuearr = $this->_request->getParam('bgissuedate');
      $dataParamValue['BG_ISSUED_START'] = $bgissuearr[0];
      $dataParamValue['BG_ISSUED_END'] = $bgissuearr[1];
    }


    $validator = array(
      'filter'                 => array(),
      'PRINCIPAL_NUMBER'      => array(),
      'BG_NUMBER'              => array(),
      'BG_BRANCH'              => array(),
      'PRINCIPAL_INSURANCE'    => array(),
      'BG_ISSUED'             => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BG_CG_DEADLINE'         => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      // 'BG_CG_DEADLINE_END' 	  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    );

    $zf_filter   = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
    // echo '<pre>';
    // print_r($zf_filter); 
    $filter     = $this->_getParam('filter');

    $PRINCIPAL_NUMBER       = $zf_filter->getEscaped('PRINCIPAL_NUMBER');
    $BG_NUMBER              = $zf_filter->getEscaped('BG_NUMBER');
    $BG_BRANCH              = $zf_filter->getEscaped('BG_BRANCH');
    $PRINCIPAL_INSURANCE    = $zf_filter->getEscaped('PRINCIPAL_INSURANCE');
    $BG_ISSUED              = $zf_filter->getEscaped('BG_ISSUED');
    $BG_CG_DEADLINE_START    = $zf_filter->getEscaped('BG_CG_DEADLINE_START');
    $BG_CG_DEADLINE_END      = $zf_filter->getEscaped('BG_CG_DEADLINE_END');
    $BG_ISSUED_START        = $zf_filter->getEscaped('BG_ISSUED_START');
    $BG_ISSUED_END          = $zf_filter->getEscaped('BG_ISSUED_END');

    // echo "<code>PRINCIPAL_NUMBER = $PRINCIPAL_NUMBER</code><BR>";  
    // echo "<code>BG_CG_DEADLINE_END = $BG_CG_DEADLINE_END</code><BR>"; die;
    $principal_number = $this->_db->select()
      ->from(
        array('B' => 'T_BANK_GUARANTEE_DETAIL'),
        array(
          'PS_FIELDVALUE'      => 'B.PS_FIELDVALUE'
        )
      )

      //->where('B.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
      ->where("B.PS_FIELDNAME = 'Principle Agreement Number'")
      ->where("B.BG_REG_NUMBER = A.BG_REG_NUMBER")
      ->limit(1);

    // echo "<code>principal_number = $principal_number</code><BR>";  
    $principal_insurance = $this->_db->select()
      ->from(
        array('C' => 'T_BANK_GUARANTEE_DETAIL'),
        array(
          'PS_FIELDVALUE'      => 'C.PS_FIELDVALUE'
        )
      )

      //->where('C.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
      ->where("C.PS_FIELDNAME = 'Insurance Name'")
      //->where("C.USER_ID = 'FIKRI01'")
      ->where("C.BG_REG_NUMBER = A.BG_REG_NUMBER")
      ->limit(1);

    // if ($filter == TRUE) 
    // {
    //   if ($PRINCIPAL_NUMBER) 
    //   {
    //     $principal_number->where("B.PS_FIELDVALUE = " . $this->_db->quote($PRINCIPAL_NUMBER)); 
    //     $this->view->PRINCIPAL_NUMBER = $PRINCIPAL_NUMBER;
    //   } 

    //   if ($PRINCIPAL_INSURANCE) 
    //   {
    //     $principal_insurance->where("B.PS_FIELDVALUE = " . $this->_db->quote($PRINCIPAL_INSURANCE)); 
    //     $this->view->PRINCIPAL_INSURANCE = $PRINCIPAL_INSURANCE;
    //   } 

    // }



    if ($auth->custModel == "1") {
      $select = $this->_db->select()
        ->from(
          array('A' => 'T_BANK_GUARANTEE'),
          array(
            'REG_NUMBER'      => 'A.BG_REG_NUMBER',
            'BG_NUMBER'        => 'A.BG_NUMBER',
            'SUBJECT'        => 'A.BG_SUBJECT',
            'BG_AMOUNT'        => 'A.BG_AMOUNT',
            'BG_BRANCH'        => 'A.BG_BRANCH',
            'BG_ISSUED'        => 'A.BG_ISSUED',
            'BG_CG_DEADLINE'        => 'A.BG_CG_DEADLINE',
            'PRINCIPAL_NUMBER'     => new Zend_Db_Expr('(' . $principal_number . ')'),
            'PRINCIPAL_INSURANCE'   => new Zend_Db_Expr('(' . $principal_insurance . ')'),

          )
        )
        ->joinleft(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE', array('B.BRANCH_NAME'))
        ->where('A.BG_STATUS = ?', '15')
        ->where('A.CGINS_STATUS != ?', '1')
        ->order('A.BG_CREATED DESC');
      $select->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
    } else if ($auth->custModel == "2") {
      $select = $this->_db->select()
        ->from(
          array('A' => 'T_BANK_GUARANTEE'),
          array(
            'REG_NUMBER'      => 'A.BG_REG_NUMBER',
            'BG_NUMBER'        => 'A.BG_NUMBER',
            'SUBJECT'        => 'A.BG_SUBJECT',
            'BG_AMOUNT'        => 'A.BG_AMOUNT',
            'BG_BRANCH'        => 'A.BG_BRANCH',
            'BG_ISSUED'        => 'A.BG_ISSUED',
            'BG_CG_DEADLINE'        => 'A.BG_CG_DEADLINE',
            'PRINCIPAL_NUMBER'     => new Zend_Db_Expr('(' . $principal_number . ')'),
            'PRINCIPAL_INSURANCE'   => new Zend_Db_Expr('(' . $principal_insurance . ')'),

          )
        )
        ->joinleft(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE', array('B.BRANCH_NAME'))
        ->where('A.BG_STATUS = ?', '15')
        ->where('A.CGINS_STATUS != ?', '1')
        ->order('A.BG_CREATED DESC');
      $select->where('A.BG_INSURANCE_CODE =' . $this->_db->quote((string)$this->_custIdLogin));
    } else {
      $select = $this->_db->select()
        ->from(
          array('A' => 'T_BANK_GUARANTEE'),
          array(
            'REG_NUMBER'      => 'A.BG_REG_NUMBER',
            'BG_NUMBER'        => 'A.BG_NUMBER',
            'SUBJECT'        => 'A.BG_SUBJECT',
            'BG_AMOUNT'        => 'A.BG_AMOUNT',
            'BG_BRANCH'        => 'A.BG_BRANCH',
            'BG_ISSUED'        => 'A.BG_ISSUED',
            'BG_CG_DEADLINE'        => 'A.BG_CG_DEADLINE',
            'PRINCIPAL_NUMBER'     => new Zend_Db_Expr('(' . $principal_number . ')'),
            'PRINCIPAL_INSURANCE'   => new Zend_Db_Expr('(' . $principal_insurance . ')'),

          )
        )
        ->joinleft(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE', array('B.BRANCH_NAME'))
        ->where('A.BG_STATUS = ?', '15')
        ->where('A.CGINS_STATUS != ?', '1')
        ->order('A.BG_CREATED DESC');
    }

    // var_dump($select->query());
    // die();

    // echo '<pre>';
    // print_r($selectArr); 

    if ($filter == TRUE) {
      if ($PRINCIPAL_NUMBER) {
        $select->having("PRINCIPAL_NUMBER LIKE " . $this->_db->quote('%' . $PRINCIPAL_NUMBER . '%'));
        $this->view->PRINCIPAL_NUMBER = $PRINCIPAL_NUMBER;
      }
      if ($BG_NUMBER) {
        $select->where("A.BG_NUMBER LIKE " . $this->_db->quote('%' . $BG_NUMBER . '%'));
        $this->view->BG_NUMBER = $BG_NUMBER;
      }

      if ($BG_BRANCH) {
        $select->where("A.BG_BRANCH LIKE " . $this->_db->quote('%' . $BG_BRANCH . '%'));
      }

      if ($BG_CG_DEADLINE_START) {
        $FormatDate   = new Zend_Date($BG_CG_DEADLINE_START, $this->_dateDisplayFormat);
        $from     = $FormatDate->toString($this->_dateDBFormat);
        if ($from) {
          $select->where('DATE(BG_CG_DEADLINE) >= ' . $this->_db->quote($from));
        }
        $this->view->BG_CG_DEADLINE_START = $BG_CG_DEADLINE_START;
      }

      if ($BG_CG_DEADLINE_END) {
        $FormatDate   = new Zend_Date($BG_CG_DEADLINE_END, $this->_dateDisplayFormat);
        $to     = $FormatDate->toString($this->_dateDBFormat);
        if ($to) {
          $select->where('DATE(BG_CG_DEADLINE) <= ' . $this->_db->quote($to));
        }
        $this->view->BG_CG_DEADLINE_END = $BG_CG_DEADLINE_END;
      }

      if ($BG_ISSUED_START) {
        $FormatDate   = new Zend_Date($BG_ISSUED_START, $this->_dateDisplayFormat);
        $from     = $FormatDate->toString($this->_dateDBFormat);
        if ($from) {
          $select->where('DATE(BG_ISSUED) >= ' . $this->_db->quote($from));
        }
        $this->view->BG_ISSUED_START = $BG_ISSUED_START;
      }

      if ($BG_ISSUED_END) {
        $FormatDate   = new Zend_Date($BG_ISSUED_END, $this->_dateDisplayFormat);
        $to     = $FormatDate->toString($this->_dateDBFormat);
        if ($to) {
          $select->where('DATE(BG_ISSUED) <= ' . $this->_db->quote($to));
        }
        $this->view->BG_ISSUED_END = $BG_ISSUED_END;
      }
    }

    if ($csv || $pdf || $this->_request->getParam('print')) {
      $result = $this->_db->fetchAll($select);

      $data_val = array();
      $i = 0;
      foreach ($result as $data) {
        // $data['FPRIVI_DESC'] = $this->language->_($data['FPRIVI_DESC']);
        array_push($data_val, $data);
        //$data_val[$i++] = $data;
      }

      if ($csv) {
        Application_Helper_General::writeLog('VCGL', 'Download CSV Counter Guarantee Certificate Required List');

        $selectcomp = $this->_db->select()
          ->from('M_CUSTOMER', array('value' => 'CUST_ID', 'CUST_NAME'))
          ->where('CUST_ID = ?', $this->_custIdLogin);

        $selectcomp = $this->_db->fetchAll($selectcomp);
        $headerData[] = $selectcomp[0]['CUST_NAME'];
        $newData[] = array('Counter Guarantee Certificate Required List');

        // if($filter == TRUE)
        // {
        //   if(!empty($datefrom) && empty($dateto))
        //   {
        //     $newData[] = array('From : '.Application_Helper_General::convertDate($datefrom,'dd/MM/yyyy'));
        //   }

        //   if(empty($datefrom) && !empty($dateto))
        //   {
        //       $newData[] = array(' to : '.Application_Helper_General::convertDate($dateto,'dd/MM/yyyy'));
        //   }

        //   if(!empty($datefrom) && !empty($dateto))
        //   {
        //       $newData[] = array('From : '.Application_Helper_General::convertDate($datefrom,'dd/MM/yyyy').' to : '.Application_Helper_General::convertDate($dateto,'dd/MM/yyyy'));
        //   }

        //   if(!empty($user))
        //   {
        //       $newData[] = array('User ID : '.$user);
        //   }

        //   if(!empty($active))
        //   {
        //       $newData[] = array('Activity Type : '.$arr[0]['FPRIVI_DESC']);
        //   }

        // }

        // if($filter == null)
        // {
        //   $newData[] = array('From : '.Application_Helper_General::convertDate($datefrom,'dd/MM/yyyy').' to : '.Application_Helper_General::convertDate($dateto,'dd/MM/yyyy'));
        // }

        $newData[] = array(' ');
        // $newData[] = array($this->language->_('Date/Time'),$this->language->_('User ID'),$this->language->_('User Name'),$this->language->_('Activity'),$this->language->_('Description'));
        $newData[]  = Application_Helper_Array::simpleArray($fields, "label");

        // echo '<pre>';
        // print_r($header);die;

        foreach ($data_val as $p => $pTrx) {
          $paramTrx = array(
            "PRINCIPAL_NUMBER"        => $pTrx['PRINCIPAL_NUMBER'],
            "REG_NUMBER"              => $pTrx['REG_NUMBER'],
            "BG_BRANCH"                => $pTrx['BG_BRANCH'],
            "PRINCIPAL_INSURANCE"      => $pTrx['PRINCIPAL_INSURANCE'],
            "BG_ISSUED"                => Application_Helper_General::convertDate($pTrx['BG_ISSUED'], $this->displayDateTimeFormat, $this->defaultDateFormat),
            "BG_CG_DEADLINE"            => Application_Helper_General::convertDate($pTrx['BG_CG_DEADLINE'], $this->displayDateTimeFormat, $this->defaultDateFormat),
          );
          $newData[] = $paramTrx;
        }
        $date = date("Y-m-d");
        $this->_helper->download->csv($headerData, $newData, null, "Counter Guarantee Certificate Required List");
      }


      if ($this->_request->getParam('print')) {
        // unset($fields['action']);
        $filterlistdatax = $this->_request->getParam('data_filter');
        $this->_forward('print', 'index', 'widget', array('data_content' => $result, 'data_caption' => 'Counter Guarantee Certificate Required List', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
      }
    }

    //echo "<code>select = $select</code><BR>";   die;
    $selectArr = $this->_db->fetchAll($select);
    // var_dump($selectArr);
    // die();

    foreach ($selectArr as $key => $value) {
      $temp_save = $this->_db->select()
        ->from(["TBGD" => "T_BANK_GUARANTEE_DETAIL"])
        ->where("TBGD.BG_REG_NUMBER = ?", $value["REG_NUMBER"])
        ->where("TBGD.PS_FIELDNAME = ?", "Insurance Branch");

      $temp_save = $this->_db->fetchRow($temp_save);

      if (!empty($temp_save)) {
        $temp_ins_branch = $this->_db->select()
          ->from("M_INS_BRANCH")
          ->where("INS_BRANCH_CODE = ?", $temp_save["PS_FIELDVALUE"]);

        $temp_ins_branch = $this->_db->fetchRow($temp_ins_branch);

        $selectArr[$key]["INS_BRANCH_NAME"] = $temp_ins_branch["INS_BRANCH_NAME"];
      }
    }

    $this->paging($selectArr);

    if (!empty($dataParamValue)) {
      // $this->view->BG_CG_DEADLINE_START = $dataParamValue['BG_CG_DEADLINE_START'];
      // $this->view->BG_CG_DEADLINE_END = $dataParamValue['BG_CG_DEADLINE_END'];

      unset($dataParamValue['BG_CG_DEADLINE_START']);
      unset($dataParamValue['BG_CG_DEADLINE_END']);
      unset($dataParamValue['BG_ISSUED_START']);
      unset($dataParamValue['BG_ISSUED_END']);
      foreach ($dataParamValue as $key => $value) {
        $wherecol[]  = $key;
        $whereval[] = $value;
      }
      //var_dump($wherecol);die('here');

      // print_r($whereval);die;
    } else {
      $wherecol = array();
      $whereval = array();
    }
    // echo '<pre>'; 
    // print_r($wherecol); 
    // print_r($whereval);
    $this->view->wherecol     = $wherecol;
    $this->view->whereval     = $whereval;
    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));


    $this->view->arrStatus = $arrStatus;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;

    Application_Helper_General::writeLog('VCGL', $this->language->_('Lihat Daftar Sertifikat Kontra Garansi Asuransi Tertunda'));
  }

  public function sugestdataAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');
    $colomnname = $this->_getParam('colomnname');

    if ($colomnname == 'BG_BRANCH') {
      $branchArr = array();
      $select = $this->_db->select()
        ->from(
          array('B'            => 'M_BRANCH'),
          array('*')
        );
      // ->where("B.CUST_ID  = ?" , (string)$this->_custIdLogin)
      // ->where("B.USER_ID  = ?" , (string)$this->_userIdLogin);

      // echo "<code>select = $select</code><BR>"; die;
      $branchArr = $this->_db->fetchAll($select);
      // bila empty, user bene linkage cannot see all payment
      if (empty($branchArr)) {
        $branchArr[] = "0";
      }

      $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
      foreach ($branchArr as $key => $value) {
        if ($tblName == $value['BRANCH_CODE']) {
          $select = 'selected';
        } else {
          $select = '';
        }
        // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        $optHtml .= "<option value='" . $value['BRANCH_CODE'] . "' " . $select . ">" . $value['BRANCH_CODE'] . " - " . $value['BRANCH_NAME'] . "</option>";
      }
    } else if ($colomnname == 'PRINCIPAL_INSURANCE') {

      $branchinsuranceArr = array();
      $select = $this->_db->select()
        ->from(
          array('B'            => 'M_BRANCH'),
          array('*')
        );

      // echo "<code>select = $select</code><BR>"; die;
      $branchinsuranceArr = $this->_db->fetchAll($select);
      // bila empty, user bene linkage cannot see all payment
      if (empty($branchinsuranceArr)) {
        $branchinsuranceArr[] = "0";
      }

      $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
      foreach ($branchinsuranceArr as $key => $value) {
        if ($tblName == $value['BRANCH_CODE']) {
          $select = 'selected';
        } else {
          $select = '';
        }
        $optHtml .= "<option value='" . $value['BRANCH_CODE'] . "' " . $select . ">" . $value['BRANCH_CODE'] . " - " . $value['BRANCH_NAME'] . "</option>";
      }
    }
    echo $optHtml;
  }

  public function detailAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $bgRegNumber = $this->_getParam('bgnumb');

    $sessToken  = new Zend_Session_Namespace('Tokenenc');
    $password   = $sessToken->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    // $decryption = urldecode($bgRegNumber);
    $decryption = $bgRegNumber;
    $bgRegNumberDecrypt = $AESMYSQL->decrypt($decryption, $password);

    $config = Zend_Registry::get('config');

    $getBgDataDetail = $this->_db->select()
      ->from("T_BANK_GUARANTEE_DETAIL")
      ->where('BG_REG_NUMBER = ?', $bgRegNumberDecrypt)
      ->query()->fetchAll();

    $getInsBranchCode = $getBgDataDetail[array_search('Insurance Branch', array_column($getBgDataDetail, 'PS_FIELDNAME'))]['PS_FIELDVALUE'];

    $getDataBg = $this->_db->select()
      ->from(
        array('TBG' => 'T_BANK_GUARANTEE'),
        array('*')
      )
      ->joinLeft(
        array('MB' => 'M_BRANCH'),
        'MB.BRANCH_CODE = TBG.BG_BRANCH',
        array('BRANCH_NAME')
      )
      ->joinLeft(
        array('MC' => 'M_CUSTOMER'),
        'MC.CUST_ID = TBG.CUST_ID',
        array(
          'CUST_ID',
          'CUST_NAME',
          'CUST_NPWP',
          'CUST_ADDRESS',
          'CUST_CITY',
          'CUST_FAX',
          'CUST_CONTACT',
          'CUST_PHONE',
        )
      )
      ->joinLeft(
        array('MCST' => 'M_CUSTOMER'),
        'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
        array(
          "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
        )
      )
      ->joinLeft(
        array('INSURANCE' => 'M_CUSTOMER'),
        'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
        array(
          "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
        )
      )
      ->joinLeft(
        array('MCL' => 'M_CITYLIST'),
        'MCL.CITY_CODE = MC.CUST_CITY',
        array('CITY_NAME')
      )
      ->joinLeft(
        array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
        'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
        array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
      )
      ->joinLeft(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        'TBGS.BG_NUMBER = TBG.BG_NUMBER',
        [
          "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
        ]
      )
      ->where('TBG.BG_REG_NUMBER = ?', $bgRegNumberDecrypt)
      ->query()->fetch();

    $getInsBranchCode = $this->_db->select()
      ->from("M_INS_BRANCH")
      ->where("INS_BRANCH_CODE = ?", $getInsBranchCode)
      ->query()->fetch();

    $getDataBg['INSURANCE_BRANCH'] = $getInsBranchCode['INS_BRANCH_NAME'];

    if (!$getDataBg) {
      $getBgDataDetail = $this->_db->select()
        ->from("T_BANK_GUARANTEE_DETAIL")
        ->where('BG_REG_NUMBER = ?', $bgRegNumberDecrypt)
        ->query()->fetchAll();

      $getInsBranchCode = $getBgDataDetail[array_search('Insurance Branch', array_column($getBgDataDetail, 'PS_FIELDNAME'))]['PS_FIELDVALUE'];

      $getDataBg = $this->_db->select()
        ->from(
          array('TBG' => 'T_BANK_GUARANTEE'),
          array('*')
        )
        ->joinLeft(
          array('MB' => 'M_BRANCH'),
          'MB.BRANCH_CODE = TBG.BG_BRANCH',
          array('BRANCH_NAME')
        )
        ->joinLeft(
          array('MC' => 'M_CUSTOMER'),
          'MC.CUST_ID = TBG.CUST_ID',
          array(
            'CUST_ID',
            'CUST_NAME',
            'CUST_NPWP',
            'CUST_ADDRESS',
            'CUST_CITY',
            'CUST_FAX',
            'CUST_CONTACT',
            'CUST_PHONE',
          )
        )
        ->joinLeft(
          array('MCST' => 'M_CUSTOMER'),
          'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
          array(
            "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
          )
        )
        ->joinLeft(
          array('INSURANCE' => 'M_CUSTOMER'),
          'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
          array(
            "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
            "INSURANCE_BRANCH" => 'INSURANCE.CUST_PROVINCE',
          )
        )
        ->joinLeft(
          array('MCL' => 'M_CITYLIST'),
          'MCL.CITY_CODE = MC.CUST_CITY',
          array('CITY_NAME')
        )
        ->joinLeft(
          array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
          'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
          array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
        )
        ->joinLeft(
          array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
          'TBGS.BG_NUMBER = TBG.BG_NUMBER',
          [
            "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
          ]
        )
        ->where('TBG.BG_REG_NUMBER = ?', $bgRegNumberDecrypt)
        ->query()->fetch();

      $getInsBranchCode = $this->_db->select()
        ->from("M_INS_BRANCH")
        ->where("INS_BRANCH_CODE = ?", $getInsBranchCode)
        ->query()->fetch();

      $getDataBg['INSURANCE_BRANCH'] = $getInsBranchCode['INS_BRANCH_NAME'];
    }

    if ($getDataBg['BG_OLD']) {
      $bgOld = $this->_db->select()
        ->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
        ->where('BG_NUMBER = ?', $getDataBg['BG_OLD'])
        ->query()->fetch();

      $this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
      $this->view->prevProv = $bgOld['PROVISION_FEE'];
    }

    // bg status
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];
    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));
    $this->view->arrStatus = $arrStatus;

    // bg type
    $bgType         = $config["bg"]["type"]["desc"];
    $bgCode         = $config["bg"]["type"]["code"];
    $arrbgType = array_combine(array_values($bgCode), array_values($bgType));
    $this->view->arrbgType = $arrbgType;

    // tipe dokumen
    $docTypeCode = $config["bgdoc"]["type"]["code"];
    $docTypeDesc = $config["bgdoc"]["type"]["desc"];
    $docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));
    $this->view->docTypeArr = $docTypeArr;

    //BG Counter Guarantee Type
    $bgcgType         = $config["bgcg"]["type"]["desc"];
    $bgcgCode         = $config["bgcg"]["type"]["code"];
    $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
    $this->view->warranty_type_text_new = $arrbgcg[$getDataBg['COUNTER_WARRANTY_TYPE']];

    // publish form
    $bgpublishType     = $config["bgpublish"]["type"]["desc"];
    $bgpublishCode     = $config["bgpublish"]["type"]["code"];
    $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
    $this->view->publishForm = $arrbgpublish[$getDataBg['BG_PUBLISH']];
    if ($getDataBg['BG_PUBLISH'] == 1) {
      $getPaperPrint = $this->_db->select()
        ->from('M_PAPER')
        ->where('notes = ?', $getDataBg['BG_NUMBER'])
        ->where('STATUS = 1')
        ->query()->fetchAll();

      $totalKertas = count($getPaperPrint);

      $this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
    }

    // bank format
    $arrBankFormat = array(
      1 => 'Bank Standard',
      2 => 'Special Format (with bank approval)'
    );
    $this->view->bankFormat = $arrBankFormat[$getDataBg['BG_FORMAT']];
    $this->view->bankFormatNumber = $getDataBg['BG_FORMAT'];

    // language format
    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Billingual',
    );
    $this->view->languagetext = $arrLang[$getDataBg['BG_LANGUAGE']];

    // cg document file
    $get_cg_doc = $this->_db->select()
      ->from("T_BANK_GUARANTEE_DETAIL")
      ->where("BG_REG_NUMBER = ?", $getDataBg["BG_REG_NUMBER"])
      ->where("PS_FIELDNAME = ?", "Counter Guarantee Document")
      ->query()->fetchAll();
    $this->view->get_cg_doc = $get_cg_doc[0];
    $this->view->need_cg_doc = (empty($get_cg_doc)) ? true : false;

    if (!$get_cg_doc) {
      $get_cg_doc = $this->_db->select()
        ->from("TEMP_BANK_GUARANTEE_DETAIL")
        ->where("BG_REG_NUMBER = ?", $getDataBg["BG_REG_NUMBER"])
        ->where("PS_FIELDNAME = ?", "Counter Guarantee Document")
        ->query()->fetchAll();
      $this->view->get_cg_doc = $get_cg_doc[0];
      $this->view->need_cg_doc = (empty($get_cg_doc)) ? true : false;
    }

    // cg document number
    $get_cg_doc_number = $this->_db->select()
      ->from("T_BANK_GUARANTEE_DETAIL")
      ->where("BG_REG_NUMBER = ?", $getDataBg["BG_REG_NUMBER"])
      ->where("PS_FIELDNAME = ?", "Counter Guarantee Number")
      ->query()->fetchAll();

    $this->view->get_cg_doc_number = $get_cg_doc_number[0]['PS_FIELDVALUE'];

    if (!$get_cg_doc_number[0]['PS_FIELDVALUE']) {
      $get_cg_doc_number = $this->_db->select()
        ->from("TEMP_BANK_GUARANTEE_DETAIL")
        ->where("BG_REG_NUMBER = ?", $getDataBg["BG_REG_NUMBER"])
        ->where("PS_FIELDNAME = ?", "Counter Guarantee Number")
        ->query()->fetchAll();

      $this->view->get_cg_doc_number = $get_cg_doc_number[0]['PS_FIELDVALUE'];
    }

    // data detail
    if (!empty($getBgDataDetail)) {
      foreach ($getBgDataDetail as $key => $value) {

        if ($value['PS_FIELDNAME'] == 'Principle Agreement Number') {
          $this->view->principleAgreementNumber =   $value['PS_FIELDVALUE'];
        }
        if ($value['PS_FIELDNAME'] == 'Principle Agreement Granted Date') {
          $this->view->principleAgreementDate =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Premium') {
          $this->view->principleInsurancePremium =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Administration') {
          $this->view->principleInsuranceAdm =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Stamp') {
          $this->view->principleInsuranceStamp =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Counter Guarantee Number') {
          $this->view->counterGuaranteeNumber =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Counter Guarantee Document') {
          $this->view->counterGuaranteeDocument =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
          $ins_branch_code =   $value['PS_FIELDVALUE'];

          $sqlinsurancebranch = $this->_db->select()
            ->from("M_INS_BRANCH", ["INS_BRANCH_ACCT"])
            ->where("INS_BRANCH_CODE = ?", $ins_branch_code);
          $get_norekinsurance = ($sqlinsurancebranch)
            ->query()->fetchAll();
          $this->view->norekCabang =   $get_norekinsurance[0]['INS_BRANCH_ACCT'];
        }
      }
    }

    // ispost
    if ($this->_request->isPost()) {
      $cgNumber = $this->_request->getPost('cg_number');
      $cgFile = $_FILES['chooseFile']; //name, type, tmp_name, error, size
      $fileName = uniqid() . '_' . basename($cgFile['name']);

      // upload file to server
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';
      $target_file = $attahmentDestination . $fileName;
      if (move_uploaded_file($cgFile["tmp_name"], $target_file)) {

        // update atau insert document number
        if ($get_cg_doc_number[0]) {
          // apabila sebelumnya sudah ada nomor kontra
          try {

            if ($cgNumber === $get_cg_doc_number[0]['PS_FIELDVALUE']) {
              $this->_db->update('T_BANK_GUARANTEE_DETAIL', [
                'PS_FIELDVALUE' => $cgNumber,
              ], [
                'BG_REG_NUMBER = ?' => $bgRegNumberDecrypt,
                'PS_FIELDNAME = ?' => 'Counter Guarantee Number'
              ]);
            } else {
              $this->_db->delete('T_BANK_GUARANTEE_DETAIL', [
                'BG_REG_NUMBER = ?' => $bgRegNumberDecrypt,
                'PS_FIELDNAME = ?' => 'Counter Guarantee Number'
              ]);

              $insertCgNumber = array(
                'BG_REG_NUMBER'         => $bgRegNumberDecrypt,
                'CUST_ID'           => $this->_custIdLogin,
                'USER_ID'           => $this->_userIdLogin,
                'PS_FIELDNAME'        => 'Counter Guarantee Number',
                'PS_FIELDTYPE'    => 1,
                'PS_FIELDVALUE'    => $cgNumber,
              );

              $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $insertCgNumber);
            }
          } catch (\Throwable $th) {
            //throw $th;
          }
        } else {
          $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', [
            'PS_FIELDNAME' => 'Counter Guarantee Number',
            'PS_FIELDVALUE' => $cgNumber,
            'CUST_ID' => $this->_custIdLogin,
            'USER_ID' => $this->_userIdLogin,
            'PS_FIELDTYPE' => 1,
            'BG_REG_NUMBER' => $bgRegNumberDecrypt
          ]);
        }

        // insert document file
        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', [
          'PS_FIELDNAME' => 'Counter Guarantee Document',
          'PS_FIELDVALUE' => $fileName,
          'CUST_ID' => $this->_custIdLogin,
          'USER_ID' => $this->_userIdLogin,
          'PS_FIELDTYPE' => 1,
          'BG_REG_NUMBER' => $bgRegNumberDecrypt
        ]);

        // insert granted date
        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', [
          'PS_FIELDNAME' => 'Counter Guarantee Granted Date',
          'PS_FIELDVALUE' => new Zend_Db_Expr('now()'),
          'CUST_ID' => $this->_custIdLogin,
          'USER_ID' => $this->_userIdLogin,
          'PS_FIELDTYPE' => 1,
          'BG_REG_NUMBER' => $bgRegNumberDecrypt
        ]);

        // redirect to success page
        // Application_Helper_General::writeLog('PSBG', "Printing Stage"); //log activity harus diganti

        Application_Helper_General::writeLog('VCGL', $this->language->_('Menyerahkan Sertifikat Kontra Garansi Asuransi BG No') . ' : ' . $getDataBg['BG_NUMBER']);

        $this->setbackURL('/' . $this->_request->getModuleName() . '/counterguarantee/');
        return $this->_redirect('/notification/success/index');
      } else {
        die('failed');
      }
    }

    Application_Helper_General::writeLog('VCGL', $this->language->_('Lihat Detail Sertifikat Kontra Garansi Asuransi Terutunda BG No') . ' : ' . $getDataBg['BG_NUMBER']);

    $this->view->data = $getDataBg;
  }

  public function downloadAction()
  {
    $bgRegNumber = $this->_getParam('bgnumb');

    $sessToken  = new Zend_Session_Namespace('Tokenenc');
    $password   = $sessToken->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $decryption = urldecode($bgRegNumber);
    // $bgRegNumberDecrypt = $AESMYSQL->decrypt($decryption, $password);
    $bgRegNumberDecrypt = $AESMYSQL->decrypt($bgRegNumber, $password);

    $get_cg_doc = $this->_db->select()
      ->from("TEMP_BANK_GUARANTEE_DETAIL")
      ->where("BG_REG_NUMBER = ?", $bgRegNumberDecrypt)
      ->where("PS_FIELDNAME = ?", "Counter Guarantee Document")
      ->query()->fetch();

    $get_cg_doc = $get_cg_doc['PS_FIELDVALUE'];

    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
    return $this->_helper->download->file($get_cg_doc, $attahmentDestination . $get_cg_doc);
  }
}
