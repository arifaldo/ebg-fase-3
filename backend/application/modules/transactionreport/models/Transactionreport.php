<?php
class transactionreport_Model_Transactionreport
{
	protected $_db;
	
    // constructor
	public function __construct()
	{	
		$config = Zend_Registry::get('config');
		$this->_masterglobalstatus 	= $config['master']['globalstatus'];
		$this->_beneftype 			= $config['account']['beneftype'];
		$this->_paymentStatus 		= $config["payment"]["status"]; 
		$this->_paymentType 		= $config["payment"]["type"]; 
		$this->_transferType 		= $config["transfer"]["type"]; 
		$this->_transferStatus 		= $config["transfer"]["status"];
		$this->_historystatus		= $config["history"]["status"];  
		$this->_db 					= Zend_Db_Table::getDefaultAdapter();
	}
	
	public function getProvider($provId)
    {
		$select2 = $this->_db->select()
						->FROM(array('A' => 'M_SERVICE_PROVIDER'),
						array('A.PROVIDER_ID','A.PROVIDER_NAME','A.SERVICE_TYPE'))
						->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array('B.SERVICE_NAME'));
//						->WHERE('A.PROVIDER_ID = ? ', $provId);
		$select2->where('A.PROVIDER_ID = ? ', $provId);			
// 		print_r($select2->query());die;
//		$select2->order($sortBy.' '.$sortDir);       
		
       return $this->_db->fetchAll($select2);
    }
    
    public function getCountNostro($nCode)
    {
    	$select	= $this->_db->select()
    	->from(	array('P' => 'T_TRANSACTION'),array('TOTAL_COUNT' =>'COUNT(P.TRA_AMOUNT)'))
    	
    	->join(	array('L' => 'T_PSLIP'), 'L.PS_NUMBER = P.PS_NUMBER', array())
    	->join(	array('U' => 'M_CUSTOMER'), 'L.CUST_ID = U.CUST_ID', array())
    	->join(	array('T' => 'M_NOSTRO_BANK'), 'T.NOSTRO_ID = P.NOSTRO_CODE', array())
    	->where("P.NOSTRO_CODE = ? ", $nCode)
    	->group('P.NOSTRO_CODE');
//     	    	 echo "<pre>";
//     	    	 print_r($select->query());die;
    	return $this->_db->fetchAll($select);
    }
    
	public function getName($provId,$field)
    {
		$select2 = $this->_db->select()
						->FROM(array('A' => 'M_SERVICE_PROVIDER'),
						array('A.PROVIDER_ID','A.PROVIDER_NAME','A.SERVICE_TYPE'))
						->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array('B.SERVICE_NAME'));
		if($field == 'PROVIDER_NAME'){
			if(isSet($provId))
			{ 
				$select2->where("UPPER(A.PROVIDER_NAME) LIKE ".$this->_db->quote('%'.$provId.'%')); 
			}
    	}
    	elseif($field == 'SERVICE_ID'){
			if(isSet($provId))
			{ 
				$select2->where('B.SERVICE_ID = ? ', $provId);
			}
    	}
    	
		
       return $data = $this->_db->fetchAll($select2);
    }
    
    function getCCYId()
    {
    	$select = $this->_db->select()
    	->from(	array(	'M'			=>'M_MINAMT_CCY'),
    			array(	'ccyid'		=>'CCY_ID') )
    			->query()->fetchAll();
    
    	foreach($select as $key => $val)
    	{
    		foreach($val as $key1 => $val1)
    		{
    			$result[$val1] = $val1;
    		}
    	}
    	return $result;
    }
    
    public function getServiceType()
    {
    	$data = $this->_db->select()->distinct()
    	->from(array('M_SERVICES_TYPE'),array('SERVICE_ID','SERVICE_NAME'))
    	->order('SERVICE_NAME ASC')
    	-> query() ->fetchAll();
    
    	return $data;
    }
	
 	public function getPaymentRecon($fParam,$sorting)
    {
		$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
// 		var_dump($this->_paymentType);
		
// 		var_dump($paytypesarr);
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
		
		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($paytypesarr as $key=>$val)
  		{
//    			$casePayType .= " WHEN ".$key." THEN '".$val."'";
  		}
//   			$casePayType .= " WHEN '3'  THEN P.PS_CATEGORY";
  			$casePayType .= "  WHEN 16 THEN 'Purchase'";
  			$casePayType .= " WHEN 17 THEN 'Payment'";
  			$casePayType .= "  END)";
  			
  		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE 'N/A' END)";
		
		$select	= $this->_db->select()
						->distinct()
						->from(	array('P' => 'T_PSLIP'),array())
						->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array())
//						->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array('*'))
						->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'U.USER_ID',
																									'U.USER_FULLNAME',
																									'P.PS_NUMBER',	
																									'T.TRX_ID',
																									'P.PS_CREATED',
																									'P.PS_UPDATED',
																									'P.PS_EFDATE',
																									'PS_TYPE'=> $casePayType,
																									'P.PS_BILLER_ID',
																									'P.UUID',
																									'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," (",T.SOURCE_ACCOUNT_CCY,") ",T.SOURCE_ACCOUNT_NAME)'),
																									'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," (",T.BENEFICIARY_ACCOUNT_CCY,") ",T.BENEFICIARY_ACCOUNT_NAME)'),									
																									'PS_STATUS'	=> $casePayStatus,
																									
																									'T.TRA_STATUS','T.TRACE_NO','T.BANK_RESPONSE','T.TRANSFER_FEE','T.BILLER_ORDER_ID','T.TRA_AMOUNT'))
						
						//->where("T.TRANSFER_TYPE <= 2");
						->where("T.TRANSFER_TYPE <= 8");
						
    	if(isSet($fParam['userid']))
    	{ 
    		$select->where("UPPER(U.USER_ID) = ".$this->_db->quote($fParam['userid'])); 
    	}
    	if(isSet($fParam['providerName']))
		{ 
			$PS_BILLER_ID_GET = $this->getName($fParam['providerName'],'PROVIDER_NAME');
			$datArr = array();
			foreach ($PS_BILLER_ID_GET as $value) {
				array_push($datArr, $value['PROVIDER_ID']);
			}
			if(count($PS_BILLER_ID_GET) < 1){
				$select->where("P.PS_BILLER_ID in (?) ", $fParam['providerName']);	
			}
			else{
				$select->where("P.PS_BILLER_ID in (?) ", $datArr);	
			}
		}
    	
    	if(isSet($fParam['serviceCategory']))
		{ 
			$PS_BILLER_ID_GET = $this->getName($fParam['serviceCategory'],'SERVICE_ID');
			$datArr = array();
			foreach ($PS_BILLER_ID_GET as $value) {
				array_push($datArr, $value['PROVIDER_ID']);
			}
			if(count($PS_BILLER_ID_GET) < 1){
				$select->where("P.PS_BILLER_ID in (?) ", $fParam['serviceCategory']);	
			}
			else{
				$select->where("P.PS_BILLER_ID in (?) ", $datArr);
			}
		}
// 		var_dump($fParam);die;
		if(isSet($fParam['username']))
		{ 
			$select->where("UPPER(U.USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['username'].'%')); 
		}
		
		if(isSet($fParam['paymentreff']))
		{ 
			$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%')); 
		}
		
    	if(isSet($fParam['referenceno']))
		{ 
			$select->where("UPPER(T.TRX_ID) LIKE ".$this->_db->quote('%'.$fParam['referenceno'].'%')); 
		}
		
		if(isSet($fParam['transferfrom']))
		{
			$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
		}
		
		if(isSet($fParam['transferto']))
		{
			$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
		}
		
		/*if(isSet($fParam['createdfrom']))
		{
			$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
		}
		
		if(isSet($fParam['createdto']))
		{
			$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
		}
		
		if(isSet($fParam['updatedfrom']))
		{
			$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
		}
		
		if(isSet($fParam['updatedto']))
		{
			$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
		}
		
		if(isSet($fParam['acctsource']))
		{ 
			$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
		
		if(isSet($fParam['beneaccount']))	
		{ 
			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }*/
		
		if(isSet($fParam['paymentstatus']))	
		{ 
			$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']); 
		}
		$select->where("P.PS_STATUS = 5 ");
		
		if(isSet($fParam['paymenttype']))
		{ 
			$fPayType 	 	= explode(",", $fParam['paymenttype']);
			$select->where("P.PS_TYPE in (?) ", $fPayType);		
		}
		
		$select->where("P.PS_TYPE in (16,17)");
		$select->where("T.TRA_STATUS = 3 ");
		
		if(isSet($fParam['transfertype']))
		{
			$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
		}
		
		$select->order($sorting);
// 		echo "<pre>";
// 		var_dump($select);die;
		return $this->_db->fetchAll($select);
    }

	public function getPaymentLLD($fParam,$sorting)
    {
    	$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
    	$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
    
    	$casePayType = "(CASE P.PS_TYPE ";
    	foreach($paytypesarr as $key=>$val)
    	{
    		$casePayType .= " WHEN ".$key." THEN '".$val."'";
    	}
    	$casePayType .= " WHEN '11' THEN P.PS_CATEGORY";
    	$casePayType .= " WHEN '12' THEN P.PS_CATEGORY";
    	$casePayType .= " ELSE 'N/A' END)";
    		
    	$casePayStatus = "(CASE P.PS_STATUS ";
    	foreach($paystatusarr as $key=>$val)
    	{
    		$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
    	}
    	$casePayStatus .= " ELSE 'N/A' END)";
    
    	$select	= $this->_db->select()
										->FROM(		array(	'T'=>'T_TRANSACTION'),array())
										->JOINLEFT(	array(	'P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',
													array(	'companycode'	=>'C.CUST_ID',
															'companyname'	=>'C.CUST_NAME',
															'payref'		=>'P.PS_NUMBER',	
															'transactionid'	=>'T.TRANSACTION_ID',	
															'benebankname'	=>'T.BENEFICIARY_BANK_NAME',
															'transferdate'	=>'P.PS_EFDATE',
																											
															'acctsrc'		=> new Zend_Db_Expr("
																				T.SOURCE_ACCOUNT + ' (' + T.SOURCE_ACCOUNT_CCY + ')  ' + T.SOURCE_ACCOUNT_NAME + ' / ' + T.SOURCE_ACCOUNT_ALIAS_NAME"),
															'beneacct'		=> new Zend_Db_Expr("
																				T.BENEFICIARY_ACCOUNT + ' (' + T.BENEFICIARY_ACCOUNT_CCY + ')  ' + T.BENEFICIARY_ACCOUNT_NAME "),
															
															'ccy'			=>'P.PS_CCY',	
															'traamount'		=>'T.TRA_AMOUNT',	
															'T.NOSTRO_NAME',
															'tramessage'	=>'T.TRA_MESSAGE',	
															'lldid'			=>'T.LLD_CODE',
															'lldcontent'	=>'T.LLD_DESC',
															'TRA_STATUS'	=>'T.TRA_STATUS',	
															'paymenttype'	=>$casePayType,	
															'TRANSFER_TYPE'	=>$caseTransType,	
															'T.NOSTRO_NAME',
															))
										->JOINLEFT(array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())
    
    			//->where("T.TRANSFER_TYPE <= 2");
    	->where("T.TRANSFER_TYPE <= 8");
//    	print_r($fParam);die;
    	if(isSet($fParam['userid']))
    	{
    		$select->where("UPPER(C.CUST_ID) = ".$this->_db->quote($fParam['userid']));
    	}
    	if(isSet($fParam['providerName']))
    	{
    		$PS_BILLER_ID_GET = $this->getName($fParam['providerName'],'PROVIDER_NAME');
    		$datArr = array();
    		foreach ($PS_BILLER_ID_GET as $value) {
    			array_push($datArr, $value['PROVIDER_ID']);
    		}
    		if(count($PS_BILLER_ID_GET) < 1){
    			$select->where("P.PS_BILLER_ID in (?) ", $fParam['providerName']);
    		}
    		else{
    			$select->where("P.PS_BILLER_ID in (?) ", $datArr);
    		}
    	}
    	if(isSet($fParam['traceNo']))
    	{
    		$select->where("UPPER(T.TRACE_NO) LIKE ".$this->_db->quote('%'.$fParam['traceNo'].'%'));
    	}
    	if(isSet($fParam['CCYID']))
    	{
    		$select->where("UPPER(P.PS_CCY) LIKE ".$this->_db->quote('%'.$fParam['CCYID'].'%'));
    	}
    	if(isSet($fParam['serviceCategory']))
    	{
    		$PS_BILLER_ID_GET = $this->getName($fParam['serviceCategory'],'SERVICE_ID');
    		$datArr = array();
    		foreach ($PS_BILLER_ID_GET as $value) {
    			array_push($datArr, $value['PROVIDER_ID']);
    		}
    		if(count($PS_BILLER_ID_GET) < 1){
    			$select->where("P.PS_BILLER_ID in (?) ", $fParam['serviceCategory']);
    		}
    		else{
    			$select->where("P.PS_BILLER_ID in (?) ", $datArr);
    		}
    	}
    
    	if(isSet($fParam['username']))
    	{
    		$select->where("UPPER(U.USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['username'].'%'));
    	}
    
    	if(isSet($fParam['paymentreff']))
    	{
    		$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%'));
    	}
    
    	if(isSet($fParam['transferfrom']))
    	{
    		$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
    	}
    
    	if(isSet($fParam['transferto']))
    	{
    		$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
    	}
    
    	if(isSet($fParam['createdfrom']))
    	{
    		$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
    	}
    
    	if(isSet($fParam['createdto']))
    	{
    		$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
    	}
    
    	if(isSet($fParam['updatedfrom']))
    	{
    		$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
    	}
    
    	if(isSet($fParam['updatedto']))
    	{
    		$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
    	}
    
    	if(isSet($fParam['acctsource']))
    	{
    		$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
    
    		if(isSet($fParam['beneaccount']))
    		{
    			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }
    
    			if(isSet($fParam['paymentstatus']))
    			{
    				$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']);
    			}
    
    			if(isSet($fParam['paymenttype']))
    			{
    				$fPayType 	 	= explode(",", $fParam['paymenttype']);
    				$select->where("P.PS_TYPE in (?) ", $fPayType);
    			}
    
    			if(isSet($fParam['transfertype']))
    			{
    				$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
    			}
    			 	//	echo $select;die;
    			$select->order($sorting);
    			
    			return $this->_db->fetchAll($select);
    }
    
    
    
    public function getPaymentSummary($fParam,$sorting)
    {
    	$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
    	// 		var_dump($this->_paymentType);
    
    	// 		var_dump($paytypesarr);
    	$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
    
    	$casePayType = "(CASE P.PS_TYPE ";
    	foreach($paytypesarr as $key=>$val)
    	{
    		//    			$casePayType .= " WHEN ".$key." THEN '".$val."'";
    	}
    	//   			$casePayType .= " WHEN '3'  THEN P.PS_CATEGORY";
    	$casePayType .= "  WHEN 16 THEN 'Purchase'";
    	$casePayType .= " WHEN 17 THEN 'Payment'";
    	$casePayType .= "  END)";
    		
    	$casePayStatus = "(CASE P.PS_STATUS ";
    	foreach($paystatusarr as $key=>$val)
    	{
    		$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
    	}
    	$casePayStatus .= " ELSE 'N/A' END)";
    
    	$select	= $this->_db->select()
    	->distinct()
    	->from(	array('P' => 'T_PSLIP'),array())
    	->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array())
    							->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array())
    	->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(
    			'TOTAL' =>'COUNT(P.PS_NUMBER)',
    			'P.PS_EFDATE',
    			'P.PS_BILLER_ID',
    			'B.PROVIDER_NAME',
    			'SUM_AMOUNT' =>'SUM(T.TRA_AMOUNT)','T.TRA_AMOUNT','T.TRANSFER_TYPE'))
    
    			//->where("T.TRANSFER_TYPE <= 2");
    	->where("T.TRANSFER_TYPE <= 8")
    	->group('P.PS_BILLER_ID');
    
    	if(isSet($fParam['userid']))
    	{
    		$select->where("UPPER(U.USER_ID) = ".$this->_db->quote($fParam['userid']));
    	}
    	if(isSet($fParam['providerName']))
    	{
    		$PS_BILLER_ID_GET = $this->getName($fParam['providerName'],'PROVIDER_NAME');
    		$datArr = array();
    		foreach ($PS_BILLER_ID_GET as $value) {
    			array_push($datArr, $value['PROVIDER_ID']);
    		}
    		if(count($PS_BILLER_ID_GET) < 1){
    			$select->where("P.PS_BILLER_ID in (?) ", $fParam['providerName']);
    		}
    		else{
    			$select->where("P.PS_BILLER_ID in (?) ", $datArr);
    		}
    	}
    	 
    	if(isSet($fParam['serviceCategory']))
    	{
    		$PS_BILLER_ID_GET = $this->getName($fParam['serviceCategory'],'SERVICE_ID');
    		$datArr = array();
    		foreach ($PS_BILLER_ID_GET as $value) {
    			array_push($datArr, $value['PROVIDER_ID']);
    		}
    		if(count($PS_BILLER_ID_GET) < 1){
    			$select->where("P.PS_BILLER_ID in (?) ", $fParam['serviceCategory']);
    		}
    		else{
    			$select->where("P.PS_BILLER_ID in (?) ", $datArr);
    		}
    	}
    	// 		var_dump($fParam);die;
    	if(isSet($fParam['username']))
    	{
    		$select->where("UPPER(U.USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['username'].'%'));
    	}
    
    	if(isSet($fParam['paymentreff']))
    	{
    		$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%'));
    	}
    
    	if(isSet($fParam['referenceno']))
    	{
    		$select->where("UPPER(T.TRX_ID) LIKE ".$this->_db->quote('%'.$fParam['referenceno'].'%'));
    	}
    
    	if(isSet($fParam['transferfrom']))
    	{
    		$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
    	}
    
    	if(isSet($fParam['transferto']))
    	{
    		$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
    	}
    
    	/*if(isSet($fParam['createdfrom']))
    		{
    	$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
    	}
    
    	if(isSet($fParam['createdto']))
    	{
    	$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
    	}
    
    	if(isSet($fParam['updatedfrom']))
    	{
    	$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
    	}
    
    	if(isSet($fParam['updatedto']))
    	{
    	$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
    	}
    
    	if(isSet($fParam['acctsource']))
    	{
    	$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
    
    	if(isSet($fParam['beneaccount']))
    	{
    	$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }*/
    
    	if(isSet($fParam['paymentstatus']))
    	{
    		$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']);
    	}
    	$select->where("P.PS_STATUS = 5 ");
    
    	if(isSet($fParam['paymenttype']))
    	{
    		$fPayType 	 	= explode(",", $fParam['paymenttype']);
    		$select->where("P.PS_TYPE in (?) ", $fPayType);
    	}
    
    	$select->where("P.PS_TYPE in (16,17)");
    	$select->where("T.TRA_STATUS = 3 ");
    
    	if(isSet($fParam['transfertype']))
    	{
    		$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
    	}
    
    	$select->order($sorting);
    	
    	// 		echo "<pre>";
    	// 		var_dump($select);die;
    	return $this->_db->fetchAll($select);
    }
    
    public function getCCYList()
    {
    	$data = $this->_db->select()
    	->from('M_MINAMT_CCY',array('CCY_ID'))
    	->query()->fetchall();
    	 
    	return $data;
    }
    
   
    public function getPaymentCoverPosition($fParam,$sorting)
    {
    	$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
    	$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
    
    	$casePayType = "(CASE P.PS_TYPE ";
    	foreach($paytypesarr as $key=>$val)
    	{
    		$casePayType .= " WHEN ".$key." THEN '".$val."'";
    	}
    	$casePayType .= " WHEN '11' THEN P.PS_CATEGORY";
    	$casePayType .= " WHEN '12' THEN P.PS_CATEGORY";
    	$casePayType .= " ELSE 'N/A' END)";
    		
    	$casePayStatus = "(CASE P.PS_STATUS ";
    	foreach($paystatusarr as $key=>$val)
    	{
    		$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
    	}
    	$casePayStatus .= " ELSE 'N/A' END)";
    
    
    	$select	= $this->_db->select()
    	->distinct()
    	->from(	array('P' => 'T_PSLIP'),array())
//     	->join(	array('U' => 'M_USER'), 'P.CUST_ID = U.CUST_ID', array())
    	->join(array('U'=>'M_CUSTOMER'),'P.CUST_ID = U.CUST_ID',array())
//     	->join(	array('M' => 'M_BANKTABLE'), 'M.BANK_CODE = P.USER_BANK_CODE', array())
    	->join(	array('C' => 'M_TRANSACTION_CROSSCCY'), 'C.PS_NUMBER = P.PS_NUMBER', array())
    
    	//						->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array('*'))
    	->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'U.CUST_ID',
    			'U.CUST_NAME',
    			'P.PS_NUMBER',
    			'P.PS_CREATED',
    			'P.PS_UPDATED',
    			'P.PS_EFDATE',
//     			'M.BANK_NAME',
    			'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," / ",T.SOURCE_ACCOUNT_NAME," / ",T.SOURCE_ACCOUNT_CCY)'),
    			'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," / ",T.BENEFICIARY_ACCOUNT_NAME," / ",T.BENEFICIARY_ACCOUNT_CCY)'),
    			'PS_STATUS'	=> $casePayStatus,
    			'PS_TYPE'=> $casePayType,
    			'T.TRA_STATUS','T.TRACE_NO','T.BANK_RESPONSE','T.TRA_AMOUNT','T.TRANSFER_FEE','T.BILLER_ORDER_ID','P.PS_BILLER_ID','T.TRX_ID','P.PS_CCY','T.LLD_TRANSACTION_PURPOSE','T.BENEFICIARY_CATEGORY','T.NOSTRO_NAME','C.BUY_CCY','C.BUY_AMOUNT','C.RATE_BUY','C.SELL_CCY','C.SELL_AMOUNT','C.RATE_SELL','T.EQUIVALENT_AMOUNT_IDR','T.TRANSFER_TYPE','C.PS_CREATE'))
    
    			//->where("T.TRANSFER_TYPE <= 2");
    	//->where("T.TRANSFER_TYPE <= 8");
        ->where("T.TRANSFER_TYPE IN (3,4,7,8)")
        ->where("T.SOURCE_ACCOUNT_CCY != T.BENEFICIARY_ACCOUNT_CCY");
    
    	if(isSet($fParam['custname']))
    	{
    		$select->where("UPPER(U.CUST_NAME) LIKE ".$this->_db->quote('%'.$fParam['custname'].'%'));
    	}
    	
    	if(isSet($fParam['custid']))
    	{
    		$select->where("UPPER(U.CUST_ID) LIKE ".$this->_db->quote('%'.$fParam['custid'].'%'));
    	}
    
    	if(isSet($fParam['paymentreff']))
    	{
    		$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%'));
    	}
    
    	if(isSet($fParam['transferfrom']))
    	{
    		$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
    	}
    
    	if(isSet($fParam['transferto']))
    	{
    		$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
    	}
    
    	if(isSet($fParam['createdfrom']))
    	{
    		$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
    	}
    
    	if(isSet($fParam['createdto']))
    	{
    		$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
    	}
    
    	if(isSet($fParam['updatedfrom']))
    	{
    		$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
    	}

	if(isSet($fParam['CCYID']))
    	{
    	    $select->where('P.PS_CCY = ?', $fParam['CCYID']);
    	}
    
    	if(isSet($fParam['updatedto']))
    	{
    		$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
    	}
    
    	if(isSet($fParam['acctsource']))
    	{
    		$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
    
    		if(isSet($fParam['beneaccount']))
    		{
    			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }
    
    			if(isSet($fParam['paymentstatus']))
    			{
    				$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']);
    			}
    
    			if(isSet($fParam['paymenttype']))
    			{
    				$fPayType 	 	= explode(",", $fParam['paymenttype']);
    				$select->where("P.PS_TYPE in (?) ", $fPayType);
    			}
    
    			if(isSet($fParam['transfertype']))
    			{
    				$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
    			}
    
    			$select->order($sorting);
    			 		/* echo "<pre>";
    			 				var_dump($fParam);
     					Zend_Debug::dump($select->query()) ;
     					die; */
//     			echo $select;
//     			die;
    			return $this->_db->fetchAll($select);
    }
    
    public function getPaymentCoverPositionCount($fParam,$sorting)
    {
    	$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
    	$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
    
    	$casePayType = "(CASE P.PS_TYPE ";
    	foreach($paytypesarr as $key=>$val)
    	{
    		$casePayType .= " WHEN ".$key." THEN '".$val."'";
    	}
    	$casePayType .= " WHEN '11' THEN P.PS_CATEGORY";
    	$casePayType .= " WHEN '12' THEN P.PS_CATEGORY";
    	$casePayType .= " ELSE 'N/A' END)";
    
    	$casePayStatus = "(CASE P.PS_STATUS ";
    	foreach($paystatusarr as $key=>$val)
    	{
    		$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
    	}
    	$casePayStatus .= " ELSE 'N/A' END)";
    
    
    	$select	= $this->_db->select()
    	->distinct()
    	->from(	array('P' => 'T_PSLIP'),array())
    	//     	->join(	array('U' => 'M_USER'), 'P.CUST_ID = U.CUST_ID', array())
    	->join(array('U'=>'M_CUSTOMER'),'P.CUST_ID = U.CUST_ID',array())
    	//     	->join(	array('M' => 'M_BANKTABLE'), 'M.BANK_CODE = P.USER_BANK_CODE', array())
    	->join(	array('C' => 'M_TRANSACTION_CROSSCCY'), 'C.PS_NUMBER = P.PS_NUMBER', array())
    
    	//						->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array('*'))
    	->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'U.CUST_ID',
    			'U.CUST_NAME','C.BUY_CCY','SUMBUY'=>'SUM(C.BUY_AMOUNT)','AVGBUY'=>'AVG(CASE WHEN C.RATE_BUY <> 0 THEN C.RATE_BUY ELSE NULL END)','C.SELL_CCY','SUMSELL'=>'SUM(C.SELL_AMOUNT)','AVGSELL'=>'AVG(CASE WHEN C.RATE_SELL <> 0 THEN C.RATE_SELL ELSE NULL END)','SUMEQ'=>'SUM(T.EQUIVALENT_AMOUNT_IDR)'))
    
    			//->where("T.TRANSFER_TYPE <= 2");
    	->where("T.TRANSFER_TYPE <= 8");
    
    	if(isSet($fParam['custname']))
    	{
    		$select->where("UPPER(U.CUST_NAME) LIKE ".$this->_db->quote('%'.$fParam['custname'].'%'));
    	}
    	 
    	if(isSet($fParam['custid']))
    	{
    		$select->where("UPPER(U.CUST_ID) LIKE ".$this->_db->quote('%'.$fParam['custid'].'%'));
    	}
    
    	if(isSet($fParam['paymentreff']))
    	{
    		$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%'));
    	}
    
    	if(isSet($fParam['transferfrom']))
    	{
    		$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
    	}
    
    	if(isSet($fParam['transferto']))
    	{
    		$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
    	}
    
    	if(isSet($fParam['createdfrom']))
    	{
    		$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
    	}
    
    	if(isSet($fParam['createdto']))
    	{
    		$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
    	}
    
    	if(isSet($fParam['updatedfrom']))
    	{
    		$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
    	}
    
    	if(isSet($fParam['updatedto']))
    	{
    		$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
    	}
    
    	if(isSet($fParam['acctsource']))
    	{
    		$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
    
    		if(isSet($fParam['beneaccount']))
    		{
    			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }
    
    			if(isSet($fParam['paymentstatus']))
    			{
    				$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']);
    			}
    
    			if(isSet($fParam['paymenttype']))
    			{
    				$fPayType 	 	= explode(",", $fParam['paymenttype']);
    				$select->where("P.PS_TYPE in (?) ", $fPayType);
    			}
    
    			if(isSet($fParam['transfertype']))
    			{
    				$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
    			}
    
    			$select->order($sorting);
    			// 		echo "<pre>";
    			// 				var_dump($select);die;
//     			    					Zend_Debug::dump($select->query()) ;
//                      echo $select;
//      			    					die;
    			return $this->_db->fetchAll($select);
    }
    
    

    public function getPaymentCoverFund($psNumber,$sorting)
    {
    	$select	= $this->_db->select()
    	->from(	array('P' => 'T_TRANSACTION'),array('*','TOTAL' =>'SUM(P.TRA_AMOUNT)','COUNTDATA' =>'COUNT(P.PS_NUMBER)'))
    	
//     	->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array('U.USER_FULLNAME'))
    	->join(	array('L' => 'T_PSLIP'), 'L.PS_NUMBER = P.PS_NUMBER', array('L.PS_CCY','L.PS_TOTAL_AMOUNT'))
    	->join(array('U'=>'M_CUSTOMER'),'L.CUST_ID = U.CUST_ID',array())
    	->join(	array('T' => 'M_NOSTRO_BANK'), 'T.NOSTRO_SWIFTCODE = P.NOSTRO_CODE', array('T.NOSTRO_NAME','T.NOSTRO_SWIFTCODE','NOSTRO_CCY'=>'T.CCY'))
    	->group(array('P.NOSTRO_CODE' , 'L.PS_CCY'));
    	 $select = "SELECT *,SUM(TRA_AMOUNT) AS `TOTAL`,COUNT(PS_NUMBER) AS `COUNTDATA` FROM (
SELECT 
  `P`.*,
  
  
  `L`.`PS_CCY`,
  `L`.`PS_TOTAL_AMOUNT`,
  
  `T`.`NOSTRO_SWIFTCODE`,
  `T`.`CCY` AS `NOSTRO_CCY` 
FROM
  `T_TRANSACTION` AS `P` 
  INNER JOIN `T_PSLIP` AS `L` 
    ON L.PS_NUMBER = P.PS_NUMBER 
  INNER JOIN `M_CUSTOMER` AS `U` 
    ON L.CUST_ID = U.CUST_ID 
  INNER JOIN `M_NOSTRO_BANK` AS `T` 
    ON T.NOSTRO_SWIFTCODE = P.NOSTRO_CODE 
 WHERE DATE(L.`PS_CREATED`) = DATE(NOW())    	     
GROUP BY P.PS_NUMBER
) j
GROUP BY `PS_CCY`,`NOSTRO_CODE`";
$select .= ' ORDER BY '.$sorting;
     	 
//    	return $this->_db->fetchAll($select);
return $this->_db->fetchAll($select);
    }
    
    public function getPaymentCoverFundCetak($psNumber,$sorting)
    {
    	$select	= $this->_db->select()
    	->from(	array('P' => 'T_TRANSACTION'),array())
    	 
    	//     	->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array('U.USER_FULLNAME'))
    	->join(	array('L' => 'T_PSLIP'), 'L.PS_NUMBER = P.PS_NUMBER', array())
    	->join(array('U'=>'M_CUSTOMER'),'L.CUST_ID = U.CUST_ID',array())
    	->join(	array('T' => 'M_NOSTRO_BANK'), 'T.NOSTRO_ID = P.NOSTRO_CODE', array('T.NOSTRO_NAME','T.NOSTRO_SWIFTCODE','NOSTRO_CCY'=>'T.CCY','TOTAL' =>'SUM(P.TRA_AMOUNT)'))
    	->group('P.NOSTRO_CODE');
    	$select = "SELECT *,SUM(TRA_AMOUNT) AS `TOTAL`,COUNT(PS_NUMBER) AS `COUNTDATA` FROM (
SELECT 
  `P`.*,
  
  
  `L`.`PS_CCY`,
  `L`.`PS_TOTAL_AMOUNT`,
  
  `T`.`NOSTRO_SWIFTCODE`,
  `T`.`CCY` AS `NOSTRO_CCY` 
FROM
  `T_TRANSACTION` AS `P` 
  INNER JOIN `T_PSLIP` AS `L` 
    ON L.PS_NUMBER = P.PS_NUMBER 
  INNER JOIN `M_CUSTOMER` AS `U` 
    ON L.CUST_ID = U.CUST_ID 
  INNER JOIN `M_NOSTRO_BANK` AS `T` 
    ON T.NOSTRO_SWIFTCODE = P.NOSTRO_CODE 
    	    WHERE DATE(L.`PS_CREATED`) = DATE(NOW())
GROUP BY P.PS_NUMBER
) j
GROUP BY `PS_CCY`,`NOSTRO_CODE`";

$select .= ' ORDER BY '.$sorting;
//     	    	 print_r($select->query());die;
    	return $this->_db->fetchAll($select);
    }
    
    
public function getPayment($fParam,$sorting)
    {
		$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
		
		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($paytypesarr as $key=>$val)
  		{
   			$casePayType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayType .= " WHEN '11' THEN P.PS_CATEGORY";
  			$casePayType .= " WHEN '12' THEN P.PS_CATEGORY";
  			$casePayType .= " ELSE 'N/A' END)";
  			
  		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE 'N/A' END)";
		
		$select	= $this->_db->select()
						->distinct()
						->from(	array('P' => 'T_PSLIP'),array())
						->join(	array('U' => 'M_USER'), 'P.CUST_ID = U.USER_ID', array())
//						->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array('*'))
						->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'U.USER_ID',
																									'U.USER_FULLNAME',
																									'P.PS_NUMBER',																							  	
																									'P.PS_CREATED',
																									'P.PS_UPDATED',
																									'P.PS_EFDATE',
																									'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," (",T.SOURCE_ACCOUNT_CCY,") ",T.SOURCE_ACCOUNT_NAME)'),
																									'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," (",T.BENEFICIARY_ACCOUNT_CCY,") ",T.BENEFICIARY_ACCOUNT_NAME)'),									
																									'PS_STATUS'	=> $casePayStatus,
																									'PS_TYPE'=> $casePayType,
																									'T.TRA_STATUS','T.TRACE_NO','T.BANK_RESPONSE','T.TRA_AMOUNT','T.TRANSFER_FEE','T.BILLER_ORDER_ID','P.PS_BILLER_ID','T.TRX_ID'))
						
						//->where("T.TRANSFER_TYPE <= 2");
						->where("T.TRANSFER_TYPE <= 8");
						
    	if(isSet($fParam['userid']))
    	{ 
    		$select->where("UPPER(U.USER_ID) = ".$this->_db->quote($fParam['userid'])); 
    	}
    	if(isSet($fParam['providerName']))
		{ 
			$PS_BILLER_ID_GET = $this->getName($fParam['providerName'],'PROVIDER_NAME');
			$datArr = array();
			foreach ($PS_BILLER_ID_GET as $value) {
				array_push($datArr, $value['PROVIDER_ID']);
			}
			if(count($PS_BILLER_ID_GET) < 1){
				$select->where("P.PS_BILLER_ID in (?) ", $fParam['providerName']);	
			}
			else{
				$select->where("P.PS_BILLER_ID in (?) ", $datArr);	
			}
		}
    	if(isSet($fParam['traceNo']))
		{ 
			$select->where("UPPER(T.TRACE_NO) LIKE ".$this->_db->quote('%'.$fParam['traceNo'].'%')); 
		}
    	if(isSet($fParam['CCYID']))
		{ 
			$select->where("UPPER(P.PS_CCY) LIKE ".$this->_db->quote('%'.$fParam['CCYID'].'%')); 
		}
    	if(isSet($fParam['serviceCategory']))
		{ 
			$PS_BILLER_ID_GET = $this->getName($fParam['serviceCategory'],'SERVICE_ID');
			$datArr = array();
			foreach ($PS_BILLER_ID_GET as $value) {
				array_push($datArr, $value['PROVIDER_ID']);
			}
			if(count($PS_BILLER_ID_GET) < 1){
				$select->where("P.PS_BILLER_ID in (?) ", $fParam['serviceCategory']);	
			}
			else{
				$select->where("P.PS_BILLER_ID in (?) ", $datArr);
			}
		}
		
		if(isSet($fParam['username']))
		{ 
			$select->where("UPPER(U.USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['username'].'%')); 
		}
		
		if(isSet($fParam['paymentreff']))
		{ 
			$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%')); 
		}
		
		if(isSet($fParam['transferfrom']))
		{
			$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
		}
		
		if(isSet($fParam['transferto']))
		{
			$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
		}
		
		if(isSet($fParam['createdfrom']))
		{
			$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
		}
		
		if(isSet($fParam['createdto']))
		{
			$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
		}
		
		if(isSet($fParam['updatedfrom']))
		{
			$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
		}
		
		if(isSet($fParam['updatedto']))
		{
			$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
		}
		
		if(isSet($fParam['acctsource']))
		{ 
			$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
		
		if(isSet($fParam['beneaccount']))	
		{ 
			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }
		
		if(isSet($fParam['paymentstatus']))	
		{ 
			$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']); 
		}
		
		if(isSet($fParam['paymenttype']))
		{ 
			$fPayType 	 	= explode(",", $fParam['paymenttype']);
			$select->where("P.PS_TYPE in (?) ", $fPayType);		
		}
		
		if(isSet($fParam['transfertype']))
		{
			$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
		}
// 		echo $select;die;
		$select->order($sorting);
		
		return $this->_db->fetchAll($select);
    }
    
    public function getPaymentDetail($psNumber)
    {
    	$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
		$transtypesarr = array_combine(array_values($this->_transferType['code']),array_values($this->_transferType['desc']));
		$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
		
    	$casePayType = "(CASE P.PS_TYPE ";
  		foreach($paytypesarr as $key=>$val)
  		{
   			$casePayType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayType .= " WHEN '11' OR '12' THEN PS_CATEGORY";
  			$casePayType .= " ELSE 'N/A' END)";
  			
  		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE 'N/A' END)";
  			
  		$caseTransType = "(CASE T.TRANSFER_TYPE ";
  		foreach($transtypesarr as $key=>$val)
  		{
   			$caseTransType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransType .= " ELSE 'N/A' END)";
  			
  		$caseTransType = "(CASE T.TRANSFER_TYPE ";
  		foreach($transtypesarr as $key=>$val)
  		{
   			$caseTransType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransType .= " ELSE 'N/A' END)";
  			
  		$caseTransStatus = "(CASE T.TRA_STATUS ";
  		foreach($transstatusarr as $key=>$val)
  		{
   			$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransStatus .= " ELSE 'N/A' END)";
  			
		$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP'),array())
// 						->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array())
						->join(array('U'=>'M_CUSTOMER'),'P.CUST_ID = U.CUST_ID',array())
						->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'P.*','U.*','T.*',
																									'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," (",T.SOURCE_ACCOUNT_CCY,") ",T.SOURCE_ACCOUNT_NAME)'),									
																									'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," (",T.BENEFICIARY_ACCOUNT_CCY,") ",T.BENEFICIARY_ACCOUNT_NAME)'),
																									'PS_STATUS'	=> $casePayStatus,
																									'CEK_PS_TYPE' => 'P.PS_TYPE',
																									'PS_TYPE' => $casePayType,
																									'TRANSFER_TYPE' => $caseTransType,
																									'TRANSFER_STATUS' => $caseTransStatus,
																									'T.TRA_STATUS'))
						->where("P.PS_NUMBER = ? ", $psNumber);
		return $this->_db->fetchAll($select);
    }
    
    function getCityCode($cityCode)
    {
    	$select = $this->_db->select()
    	->from(array('A' => 'M_CITY'),array('*'));
    	//		$select->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('A.PROVIDER_ID','A.PROVIDER_NAME'));
    	$select->where('A.CITY_CODE = ?',$cityCode);
    	//		$select ->order('CITY_NAME ASC');
    	return $this->_db->fetchall($select);
    }
    
    
    public function getPslipDetailLld($psNumber)
    {
    	$select	= $this->_db->select()
    	->from(	array('P' => 'T_TRANSACTION'),array('*'))
//     	->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array('U.USER_FULLNAME'))
    	
    	->join(	array('L' => 'T_PSLIP'), 'L.PS_NUMBER = P.PS_NUMBER', array('L.PS_CCY','L.PS_TOTAL_AMOUNT'))
    	->join(array('U'=>'M_CUSTOMER'),'L.CUST_ID = U.CUST_ID',array('U.*'))
//     	->join(	array('T' => 'M_USER_ACCT'), 'T.USER_ID = U.USER_ID', array('T.*'))
    	->join(	array('T' => 'M_CUSTOMER_ACCT'), 'T.ACCT_NO = P.SOURCE_ACCOUNT', array('T.*'))
    	->where("P.PS_NUMBER = ? ", $psNumber);
	//echo $select;die;
    	return $this->_db->fetchAll($select);
    }
    
    
	public function getPslipDetail($psNumber)
    {
    	$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP_DETAIL'),array('*'))
						->where("P.PS_NUMBER = ? ", $psNumber);
		return $this->_db->fetchAll($select);
    }
    
	public function getHistory($psNumber)
    {
    	$historyarr = array_combine(array_values($this->_historystatus['code']),array_values($this->_historystatus['desc']));
    	
    	$caseHistory = "(CASE P.HISTORY_STATUS ";
  		foreach($historyarr as $key=>$val)
  		{
   			$caseHistory .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseHistory .= " ELSE 'N/A' END)";
    	
    	$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP_HISTORY'),array('*','HISTORY_STATUS' => $caseHistory))
						->where("P.PS_NUMBER = ? ", $psNumber);
    	$select -> order('DATE_TIME DESC');
		return $this->_db->fetchAll($select);
    }
}