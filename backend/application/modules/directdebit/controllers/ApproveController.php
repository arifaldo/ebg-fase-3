<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class directdebit_ApproveController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
		
		//select m_user - begin
		$userData = $CustomerUser->getUser($this->_userIdLogin);
		
		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $aliasname,
											   'sortable' => true),*/
						// 'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
						// 					   'label' => $beneficiaryaccount,
						// 					   'sortable' => true),
						// 'comp_id'  => array('field' => 'BENEFICIARY_NAME',
						// 					   'label' => $this->language->_('Company ID'),
						// 					   'sortable' => true),
						'suggest_data'  => array('field' => 'DIR_STATUS',
											   'label' => $this->language->_('Suggestion Data '),
											   'sortable' => true),
						'comp_name'  => array('field' => 'company',
											   'label' => $this->language->_('Company Name'),
											   'sortable' => true),
						'client_reff'  => array('field' => 'CLIENT_REFF',
											   'label' => $this->language->_('Client Reff'),
											   'sortable' => true),
						'client_name'  => array('field' => 'CLIENT_NAME',
											   'label' => $this->language->_('Client Name'),
											   'sortable' => true),
						// 'doc_reff'  => array('field' => 'DOCUMENT_REFF',
						// 					   'label' => $this->language->_('Document Reff'),
						// 					   'sortable' => true),
						'debit_acct'   => array('field'    => 'DEBIT_ACCT',
											  'label'    => $this->language->_('Debited Account'),
											  'sortable' => false),						
						'last_suggest'   => array('field'    => 'DIR_SUGESTEDBY',
											  'label'    => $this->language->_('Suggested By'),
											  'sortable' => false),
						// 'last_approved'   => array('field'    => 'DIR_APPROVEDBY',
						// 					  'label'    => $this->language->_('Last Approved'),
						// 					  'sortable' => false),
						// 'account_name'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
						// 					  'label'    => $this->language->_('Account Name'),
						// 					  'sortable' => false),
				);

		$filterlist = array('COMP_ID','COMP_NAME','DEBIT_ACCT');

		$this->view->filterlist = $filterlist;
				      
		$page    = $this->_getParam('page');
		
		// $sortBy  = $this->_getParam('sortby','B.PS_UPDATED');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('T_DIRECTDEBIT');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		// $sortDir = $this->_getParam('sortdir','desc');
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$select2 = $this->_db->select()
					 ->from(array('C' => 'TEMP_DIRECTDEBIT'),array('*','company'	=> new Zend_Db_Expr("CONCAT(COMP_NAME , ' (' , COMP_ID , ')  ' )")))
					 ->join(array('D' => 'T_GLOBAL_CHANGES'), 'D.CHANGES_ID = C.CHANGES_ID',array('D.CHANGES_INFORMATION'))
					//  ->joinleft(array('X'=>'M_DOMESTIC_BANK_TABLE'), 'X.SWIFT_CODE = C.DEBIT_BANK',array('BANK_NAME'))
					//  ->where('DIR_TYPE = 1')
					 ->where('DIR_STATUS in (0,2)')
					 ->order('DIR_SUGESTED DESC');
		
					 
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'COMP_ID'    	=> array('StripTags'),
	                       	'COMP_NAME'  		=> array('StripTags','StringTrim','StringToUpper'),
							'DEBIT_BANK'    => array('StripTags'),
							'DEBIT_ACCT' 		=> array('StripTags','StringTrim','StringToUpper'),
		);

		$dataParam = array('COMP_ID','DEBIT_BANK','COMP_NAME','DEBIT_ACCT');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}
		}
		
	                      
		$validators = array(
		'COMP_ID' 	=> array(),
		'COMP_NAME' 	=> array(),
		'DEBIT_BANK' 	=> array(),	
		'DEBIT_ACCT' 	=> array(),		
		);
	   
	     $zf_filter 	= new Zend_Filter_Input($filterArr,$validator,$dataParamValue);

	     if ($zf_filter->isValid()) {
	     	$filter 		= TRUE;
	     }
	    
	     
	    
		 $fcompid = $zf_filter->getEscaped('COMP_ID');
		$fcompname = $zf_filter->getEscaped('COMP_NAME');
		$fdebitbank = $zf_filter->getEscaped('DEBIT_BANK');
		$fdebitacct = $zf_filter->getEscaped('DEBIT_ACCT');

		if($fcompid) $param['fcompid'] = $fcompid;
        if($fcompname) $param['fcompname'] = $fcompname;
        if($fdebitbank) $param['fdebitbank'] = $fdebitbank;
        if($fdebitacct) $param['fdebitacct'] = $fdebitacct;

		$this->view->custid = $fcompid;
		$this->view->custname = $fcompname;
		$this->view->debit_bank = $fdebitbank;
		$this->view->debit_acct = $fdebitacct;
	 
		 
		 
		if($filter == null)
		{	
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}
		
		if($filter_clear == '1'){
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';
			$datefrom = '';
			$dateto = '';
			
		}
		
		if($filter == null || $filter ==TRUE)
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;	 
		}
		
		if($filter == TRUE)
	    {		
	    	
	    	if($fcompid)              $select->where('UPPER(COMP_ID)='.$this->_db->quote(strtoupper($fcompid)));
			if($fcompname)            $select->where('UPPER(COMP_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fcompname).'%'));
			if($fdebitbank)            $select->where('UPPER(DEBIT_BANK)='.$this->_db->quote(strtoupper($fdebitbank)));
			if($fdebitacct)            $select->where('UPPER(DEBIT_ACCT) LIKE '.$this->_db->quote('%'.strtoupper($fdebitacct).'%'));
		}

		$select2->order($sortBy.' '.$sortDir);
		
		if($this->_request->isPost() )
		{
			
			$direct_id = $this->_getParam('change_id');
			
			if(count($direct_id) > 0)
			{			
			
				if($this->_getParam('submit')=='Approve')
				{	

					foreach($direct_id as $key=>$row){						
						try 
						{
							$this->_db->beginTransaction();		
		
							$dataapprove =  array(					
													'DIR_STATUS' => '1', 
													'DIR_APPROVED' => date('Y-m-d H:i:s'), 
													'DIR_APPROVEDBY' => $this->_userIdLogin
												);							
							
							$where = array('DIRECT_ID = ?' => $row);
							$result = $this->_db->update('T_DIRECTDEBIT',$dataapprove,$where);
							Application_Helper_General::writeLog('APLO','Approve Direct Debit '.$row);	
							
							$this->_db->commit();							
							
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
							
						}
				    }     
				    //$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/directdebit/approve');					
					
					
				}elseif($this->_getParam('submit')=='Reject')
				{
					
					foreach($direct_id as $key=>$row){						
						try 
						{
							$this->_db->beginTransaction();		
		
							$dataapprove =  array(					
													'DIR_STATUS' => '2'
												);							
							
							$where = array('DIRECT_ID = ?' => $row);
							$result = $this->_db->update('T_DIRECTDEBIT',$dataapprove,$where);
							Application_Helper_General::writeLog('RJLO','Reject Direct Debit '.$row);	
							
							$this->_db->commit();							
							
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
							
						}
				    }     
				    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/directdebit/approve');	
					
				}
			
			}else{
				$this->view->error = TRUE;
				$this->view->report_msg = $this->language->_('Error: Please select a direct debit');
			}
			
		}
		
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$arr = $this->_db->fetchAll($select2);
			foreach ($arr as $key => $value)
			{
				unset($arr[$key]['CUST_ID']);
				//echo $key;
				$arr[$key]["PS_REQUESTED"] = Application_Helper_General::convertDate($value["PS_REQUESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

				$arr[$key]["SOURCE_ACCOUNT"] = $arr[$key]["SOURCE_ACCOUNT"]." [".$arr[$key]["SOURCE_ACCOUNT_CCY"]."] / ".$arr[$key]["SOURCE_ACCOUNT_NAME"];
				unset($arr[$key]["SOURCE_ACCOUNT_CCY"]);
				unset($arr[$key]["SOURCE_ACCOUNT_NAME"]);

				$arrProductType = array('1'=>'Cheque','2'=>'Bilyet Giro');
				$arr[$key]["PRODUCT_TYPE"] = $arrProductType[$arr[$key]["PRODUCT_TYPE"]];

				unset($arr[$key]['PS_APPROVEBY']);

				$requestby = $arr[$key]['PS_REQUESTBY'];
				unset($arr[$key]['PS_REQUESTBY']);

				$arr[$key]["ADMIN_FEE"] = "IDR ".Application_Helper_General::displayMoney($arr[$key]["ADMIN_FEE"]);
				unset($arr[$key]["SUGGEST_STATUS"]);

				$arr[$key]["PS_REQUESTBY"] = $requestby;

				
			}
// 			echo "<pre>";
// 			print_r($arr);die;
			$header = Application_Helper_Array::simpleArray($fields, 'label');
			//Zend_Debug::dump($arr);die;
			if($csv)
			{
				Application_Helper_General::writeLog('DTRX','Download CSV Transaction Report');
				//Zend_Debug::dump($arr);die;
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->csv($header,$arr,null,'Executed Transaction');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('DTRX','Download PDF Transaction Report');
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->pdf($header,$arr,null,'Executed Transaction');
			}
			if($this->_request->getParam('print') == 1){
				
	                $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields));
	        }
		}
		else
		{
			Application_Helper_General::writeLog('DTRX','View Transaction Report');
		}
// 		echo '<pre>';
// 		print_r($select2->query());die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($select2);

		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}
}