
(function($) { 
  $.fn.chained = function(parent_selector, options) { 
    return this.each(function() {
      var self   = this;
      var backup = $(self).clone();
      $(parent_selector).each(function() {
        $(this).bind("change", function() {
          $(self).html($(backup).html());
          var selected = "";
          $(parent_selector).each(function() {
            selected += "\\" + $(":selected", this).val();
          });
          selected = selected.substr(1);
          var first = $(parent_selector).first();
          var selected_first = $(":selected", first).val();
          $("option", self).each(function() {
            if(!$(this).hasClass(selected) && !$(this).hasClass(selected_first) && $(this).val() !== "") {
              $(this).remove();
             }                        
          });
          if (1 == $("option", self).size() && $(self).val() == "") {
            $(self).attr("disabled", "disabled");
          } else {
            $(self).removeAttr("disabled");
          }
        });
        $(this).trigger("change");
        showScheme($('#scheme').val());             
      });
    });
  };
  $.fn.chainedTo = $.fn.chained;  
})(jQuery);

//--------------------------- installment Activation ----------------------------------------------------

var temp_di = 0;
var temp_dtp = 0;
var temp_dtf = 0;
var temp_dittp = 0;
var temp_dittf = 0	

var temp_gi = 0;
var temp_gtp = 0;
var temp_gtf = 0;

var temp_ai = 0;
var temp_atp = 0;
var temp_atf = 0;

var temp_pi = 0;
var temp_ptp = 0;
var temp_ptf = 0;



// proses
function proc_installment_di(v_ccy,v_rate,v_err_mess)
{
	
	if((v_ccy) || (v_rate))
	{
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
	
	}else{
		var temp_rate = document.getElementById('ratedi').value;
		var e = document.getElementById("fccydi");
		var temp_ccy = e.options[e.selectedIndex].value;	
	}
	
	
	$("#detaild_type1").show();
		var intId = $("#detail_proc_d div").length + 1;
        var fieldWrapper = $('<div class=\'fieldwrapper\' id=\'field' + intId + '\'/>');
//        var fType = $('<select readonly="readonly" class=\'fieldtype\' name=\'ccy_di['+temp_di+']\'><option value=\'' + temp_ccy +'\'>'+temp_ccy+' </option></select>');
        var fType = $('<input type="text" class="fieldtype" name=\'ccy_di['+temp_di+']\' style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        var fName = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input type=\'text\' id=\'rate_di['+temp_di+']\' style="width:125px"  onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" name=\'rate_di['+temp_di+']\' size=\'10\' maxlength=\'6\' class=\'fieldname\' value=\'' +temp_rate+ '\' />');
        var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
		
		removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(fType);
        fieldWrapper.append(fName);
		
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			// var err_message = $("<input type=\"text\" class=\"fielderr\" size=\"40\" readonly=\"true\" value=\""+v_err_mess+"\" />");
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
        if((temp_rate && temp_ccy) || (v_err_mess)){
			$("#detail_proc_d").append(fieldWrapper);
			temp_di++;
		}
		document.addForm.ratedi.value = "" ; 
	var e = document.getElementById("fccydi");
	e.value = "";
	//setArray_di();
	//addMore_di();
}


function proc_installment_dtp(v_ccy,v_rate,v_min,v_max,v_err_mess)
{
	$("#detaild_type2").show();
		var intId = $("#detail_proc_dtp div").length + 1;
	
	if((v_ccy) || (v_rate))
	{

		$("#label_mod_d").show();
		
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
		var temp_max = v_max;
		var temp_min = v_min;
	
	}else{
		
		var temp_rate = document.getElementById('ratedtp').value;
		var e = document.getElementById("fccydtp");
		var temp_ccy = e.options[e.selectedIndex].value;
		var temp_max = document.getElementById('maxdtp').value;
		var temp_min = document.getElementById('mindtp').value;
	}
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_dtp = $('<select class=\'fieldtype\' name=\'ccy_dtp['+temp_dtp+']\'><option value=\'' + temp_ccy +'\'>'+temp_ccy+'</option></select>');
        var val_ccy_dtp = $('<input type="text" class="fieldtype" name=\'ccy_dtp['+temp_dtp+']\' style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        
		var val_rate_dtp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'rate_dtp['+temp_dtp+']\' id=\'rate_dtp['+temp_dtp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'6\' maxlength=\'6\' class=\'fieldname\' value=\'' +temp_rate+ '\' />');
        var val_min_dtp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'min_dtp['+temp_dtp+']\' style="width:125px"   id=\'min_dtp['+temp_dtp+']\'  onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' class=\'fieldname\' maxlength=\'16\' value=\'' +temp_min+ '\' />');
        var val_max_dtp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'max_dtp['+temp_dtp+']\' style="width:125px"   id=\'max_dtp['+temp_dtp+']\'  onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)"type=\'text\' maxlength=\'16\' class=\'fieldname\' value=\'' +temp_max+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_dtp);
        fieldWrapper.append(val_rate_dtp);
		 fieldWrapper.append(val_min_dtp);
        fieldWrapper.append(val_max_dtp);
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 //var err_message = $("<input type=\"text\" class=\"fielderr\" size=\"56\" readonly=\"true\" value=\""+v_err_mess+"\" />");
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		
		if((temp_rate && temp_ccy && temp_min && temp_max) || (v_err_mess)){
			 $("#detail_proc_dtp").append(fieldWrapper);
			temp_dtp++;
		}
       
		document.addForm.ratedtp.value = "" ; 
		var e = document.getElementById("fccydtp");
		e.value = "";
		document.addForm.maxdtp.value = "" ;
		document.addForm.mindtp.value = "" ;
	
	//setArray_dtp();
	//addMore_dtp();
}

function proc_installment_dtf(v_ccy,v_amount,v_err_mess)
{
	$("#detaild_type3").show();
	
	var intId = $("#detail_proc_dtf div").length + 1;
	if((v_ccy) || (v_amount))
	{
		var temp_amount = v_amount;
		var temp_ccy = v_ccy;
	
		
		
	}else{
	var temp_amount = document.getElementById('amountdtf').value;
	var e = document.getElementById("fccydtf");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_dtf = $('<select class=\'fieldtype\' name=\'ccy_dtf['+temp_dtf+']\'><option  value=\'' + temp_ccy +'\'>'+temp_ccy+'</option></select>');
        var val_ccy_dtf = $('<input type="text" class="fieldtype" name=\'ccy_dtf['+temp_dtf+']\' style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        var val_amount_dtf = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'amount_dtf['+temp_dtf+']\' style="width:125px" id=\'amount_dtf['+temp_dtf+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'8\' maxlength=\'16\'class=\'fieldname\' value=\'' +temp_amount+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_dtf);
        fieldWrapper.append(val_amount_dtf);
		
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		
		 if((temp_amount && temp_ccy) || (v_err_mess)){
			  $("#detail_proc_dtf").append(fieldWrapper);
		temp_dtf++;
		}
      
		
		document.addForm.amountdtf.value = "" ; 
		var e = document.getElementById("fccydtf");
		e.value = "";
		
	
	//setArray_dtf();
	//addMore_dtf();
}

/*function proc_installment_dittp_temp()
{
	$("#detaildit_type1").show();
	var intId = $("#detail_proc_dittp div").length + 1;
	var temp_rate = document.getElementById('ratedittp').value;
	var e = document.getElementById("fccydittp");
	var temp_ccy = e.options[e.selectedIndex].value;
	var temp_max = document.getElementById('maxdittp').value;
	var temp_min = document.getElementById('mindittp').value;
	
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
        var val_ccy_dittp = $("<select class=\"fieldtype\" name=\"ccy_dittp["+temp_dittp+"]\"><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
		var val_rate_dittp = $(" &nbsp; &nbsp; &nbsp; &nbsp;<input name=\"rate_dittp["+temp_dittp+"]\" type=\"text\" size=\"6\" maxlength=\"5\"class=\"fieldname\" value=\"" +temp_rate+ "\" />");
        var val_min_dittp = $(" &nbsp; &nbsp; &nbsp; &nbsp;<input name=\"min_dittp["+temp_dittp+"]\" type=\"text\" size=\"6\" maxlength=\"5\"class=\"fieldname\" value=\"" +temp_min+ "\" />");
        var val_max_dittp = $(" &nbsp; &nbsp; &nbsp; &nbsp;<input name=\"max_dittp["+temp_dittp+"]\" type=\"text\" size=\"6\" maxlength=\"5\" class=\"fieldname\" value=\"" +temp_max+ "\" />");
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_dittp);
        fieldWrapper.append(val_rate_dittp);
		 fieldWrapper.append(val_min_dittp);
        fieldWrapper.append(val_max_dittp);
        fieldWrapper.append(removeButton);
       
		
		
		if((temp_rate && temp_ccy && temp_min && temp_max) || (v_err_mess)){
			 $("#detail_proc_dittp").append(fieldWrapper);
			temp_dittp++;
		}
		document.addForm.ratedittp.value = "" ; 
		var e = document.getElementById("fccydittp");
		e.value = "";
		document.addForm.maxdittp.value = "" ;
		document.addForm.mindittp.value = "" ;
		
}

function proc_installment_dittf_temp()
{
	$("#detaildit_type2").show();
	
	var intId = $("#detail_proc_dittf div").length + 1;
	var temp_amount = document.getElementById('amountdittf').value;
	var e = document.getElementById("fccydittf");
	var temp_ccy = e.options[e.selectedIndex].value;
	
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
        var val_ccy_dittf = $("<select name=\"ccy_dittf["+temp_dittf+"]\" class=\"fieldtype\"><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
		var val_amount_dittf = $(" &nbsp; &nbsp; &nbsp; &nbsp;<input name=\"amount_dittf["+temp_dittf+"]\" type=\"text\" size=\"8\" maxlength=\"5\"class=\"fieldname\" value=\"" +temp_amount+ "\" />");
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_dittf);
        fieldWrapper.append(val_amount_dittf);
		
		if((temp_amount && temp_ccy) || (v_err_mess)){
			  $("#detail_proc_dittf").append(fieldWrapper);
			temp_dtf++;
		}
      
        fieldWrapper.append(removeButton);
        
		
		
		document.addForm.amountdittf.value = "" ; 
		var e = document.getElementById("fccydittf");
		e.value = "";
		
		
	
}
*/


function proc_installment_dittp(v_ccy,v_rate,v_min,v_max,v_err_mess)
{
	//alert("sss");
	$("#detaildit_type1").show();
		var intId = $("#detail_proc_dittp div").length + 1;
	
	if((v_ccy) || (v_rate))
	{
		//var t = document.getElementById("charges_type_d");
		//t.value = "2";
		//var m = document.getElementById("charges_mod_d");
		//m.value = "0";
		$("#label_mod_d").show();
		
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
		var temp_max = v_max;
		var temp_min = v_min;
	
	}else{
		
			var temp_rate = document.getElementById('ratedittp').value;
		var e = document.getElementById("fccydittp");
		var temp_ccy = e.options[e.selectedIndex].value;
		var temp_max = document.getElementById('maxdittp').value;
		var temp_min = document.getElementById('mindittp').value;
	}
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_dittp = $("<select class=\"fieldtype\" name=\"ccy_dittp["+temp_dittp+"]\"><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
        var val_ccy_dittp = $('<input type="text" class="fieldtype" name=\"ccy_dittp['+temp_dittp+']\" style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        var val_rate_dittp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'rate_dittp['+temp_dittp+']\' id=\'rate_dittp['+temp_dittp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'6\' maxlength=\'6\' class=\'fieldname\' value=\'' +temp_rate+ '\' />');
        var val_min_dittp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'min_dittp['+temp_dittp+']\' id=\'min_dittp['+temp_dittp+']\' style="width:125px" onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" maxlength=\'16\' type=\'text\' class=\'fieldname\' value=\'' +temp_min+ '\' />');
        var val_max_dittp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'max_dittp['+temp_dittp+']\' id=\'max_dittp['+temp_dittp+']\' style="width:125px" onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" maxlength=\'16\' type=\'text\' class=\'fieldname\' value=\'' +temp_max+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_dittp);
        fieldWrapper.append(val_rate_dittp);
		 fieldWrapper.append(val_min_dittp);
        fieldWrapper.append(val_max_dittp);
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
      
		
		if((temp_rate && temp_ccy && temp_min && temp_max) || (v_err_mess)){
			  $("#detail_proc_dittp").append(fieldWrapper);
			temp_dittp++;
		}
		
		document.addForm.ratedittp.value = "" ; 
		var e = document.getElementById("fccydittp");
		e.value = "";
		document.addForm.maxdittp.value = "" ;
		document.addForm.mindittp.value = "" ;
	
}

function proc_installment_dittf(v_ccy,v_amount,v_err_mess)
{
	$("#detaildit_type2").show();
	
	var intId = $("#detail_proc_dittf div").length + 1;
	if((v_ccy) || (v_amount))
	{
		var temp_amount = v_amount;
		var temp_ccy = v_ccy;
	
		/*var t = document.getElementById("charges_type_d");
		t.value = "2";
		var m = document.getElementById("charges_mod_d");
		m.value = "0";*/
		$("#label_mod_d").show();
		
	}else{
	var temp_amount = document.getElementById('amountdittf').value;
	var e = document.getElementById("fccydittf");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_dittf = $('<select class=\'fieldtype\' name=\'ccy_dittf['+temp_dittf+']\'><option  value=\'' + temp_ccy +'\'>'+temp_ccy+'</option></select>');
        var val_ccy_dittf = $('<input type="text" class="fieldtype" name=\'ccy_dittf['+temp_dittf+']\' style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
		var val_amount_dittf = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'amount_dittf['+temp_dittf+']\' id=\'amount_dittf['+temp_dittf+']\' onkeypress="return forceDecimalOCBC(event)" style="width:125px" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'8\' maxlength=\'16\'class=\'fieldname\' value=\'' +temp_amount+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_dittf);
        fieldWrapper.append(val_amount_dittf);
		
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		
		if((temp_ccy && temp_amount) || (v_err_mess)){
			$("#detail_proc_dittf").append(fieldWrapper);
			temp_dittf++;
		}	
		document.addForm.amountdittf.value = "" ; 
		var e = document.getElementById("fccydittf");
		e.value = "";
		
}

//----------------------------------- Grace Period ---------------------------------------------------------------------
function proc_installment_gi(v_ccy,v_rate,v_err_mess)
{
	
	if((v_ccy) || (v_rate))
	{
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
	
	}else{

	var temp_rate = document.getElementById('rategi').value;
	var e = document.getElementById("fccygi");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
	$("#detailg_type1").show();
	
		var intId = $("#detail_proc_g div").length + 1;
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
        var val_rate_g = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'rate_gi['+temp_gi+']\' style="width:125px" id=\'rate_gi['+temp_gi+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'10\' maxlength=\'6\' class=\'fieldname\' value=\'' +temp_rate+ '\' />');
//        var val_ccy_g = $('<select class=\'fieldtype\' name=\'ccy_gi['+temp_gi+']\'><option value=\'' + temp_ccy +'\'>'+temp_ccy+'</option></select>');
        var val_ccy_g = $('<input type="text" class="fieldtype" name=\'ccy_gi['+temp_gi+']\' style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_g);
        fieldWrapper.append(val_rate_g);
       
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		if((temp_rate && temp_ccy) || (v_err_mess)){
			 $("#detail_proc_g").append(fieldWrapper);
			temp_gi++;
		}
       
		document.addForm.rategi.value = "" ; 
	var e = document.getElementById("fccygi");
	e.value = "";
	

}


function proc_installment_gtf(v_ccy,v_amount,v_err_mess)
{
	$("#detailg_type3").show();
	
	var intId = $("#detail_proc_gtf div").length + 1;
	if((v_ccy) || (v_amount))
	{
		var temp_amount = v_amount;
		var temp_ccy = v_ccy;
	
		
	}else{
	var temp_amount = document.getElementById('amountgtf').value;
	var e = document.getElementById("fccygtf");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
		
		var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_gtf = $("<select class=\"fieldtype\" name=\"ccy_gtf["+temp_gtf+"]\"><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
        var val_ccy_gtf = $('<input type="text" class="fieldtype" name=\"ccy_gtf['+temp_gtf+']\" style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        var val_amount_gtf = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'amount_gtf['+temp_gtf+']\' style="width:125px" id=\"amount_gtf['+temp_gtf+']\" onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'8\' maxlength=\'16\'class=\'fieldname\' value=\'' +temp_amount+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_gtf);
        fieldWrapper.append(val_amount_gtf);
		
        fieldWrapper.append(removeButton);
		//----
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		
		 if((temp_amount && temp_ccy) || (v_err_mess)){
			$("#detail_proc_gtf").append(fieldWrapper);
			temp_gtf++;
		}
      
		
		document.addForm.amountgtf.value = "" ; 
		var e = document.getElementById("fccygtf");
		e.value = "";
		
	
	//setArray_dtf();
	//addMore_dtf();
}

function proc_installment_gtp(v_ccy,v_rate,v_min,v_max,v_err_mess)
{
	
	
	if((v_ccy) || (v_rate))
	{
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
		var temp_max = v_max;
		var temp_min = v_min;
		
		//var t = document.getElementById("charges_type_g");
		//t.value = "1";
		//var m = document.getElementById("charges_mod_g");
		//m.value = "0";
		$("#label_mod_g").show();
		
	
	}else{
		var temp_rate = document.getElementById('rategtp').value;
	var e = document.getElementById("fccygtp");
	var temp_ccy = e.options[e.selectedIndex].value;
	var temp_max = document.getElementById('maxgtp').value;
	var temp_min = document.getElementById('mingtp').value;
	}
	
	$("#detailg_type2").show();
	var intId = $("#detail_proc_gtp div").length + 1;
	
	
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_gtp = $('<select name=\'ccy_gtp['+temp_gtp+']\' class=\'fieldtype\'><option value=\'' + temp_ccy +'\'>'+temp_ccy+'</option></select>');
        var val_ccy_gtp = $('<input type="text" class="fieldtype" name=\'ccy_gtp['+temp_gtp+']\' style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
		var val_rate_gtp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'rate_gtp['+temp_gtp+']\'  id=\'rate_gtp['+temp_gtp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'6\' maxlength=\'6\'class=\'fieldname\' value=\'' +temp_rate+ '\' />');
        var val_min_gtp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'min_gtp['+temp_gtp+']\' style="width:125px" id=\'min_gtp['+temp_gtp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' class=\'fieldname\'  maxlength=\'16\' value=\'' +temp_min+ '\' />');
        var val_max_gtp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'max_gtp['+temp_gtp+']\' style="width:125px" id=\'max_gtp['+temp_gtp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' class=\'fieldname\' maxlength=\'16\' value=\'' +temp_max+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_gtp);
        fieldWrapper.append(val_rate_gtp);
		 fieldWrapper.append(val_min_gtp);
        fieldWrapper.append(val_max_gtp);
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		
		if((temp_rate && temp_ccy && temp_min && temp_max) || (v_err_mess)){
			   $("#detail_proc_gtp").append(fieldWrapper);
			temp_gtp++;
		}
       
		
		
		document.addForm.rategtp.value = "" ; 
		var e = document.getElementById("fccygtp");
		e.value = "";
		document.addForm.maxgtp.value = "" ;
		document.addForm.mingtp.value = "" ;
		
	
}
/*
function proc_installment_gtf_temp(v_ccy,v_amount,v_err_mess)
{
	
	if((v_ccy) || (v_amount))
	{
		
		var temp_amount = v_amount;
		var temp_ccy = v_ccy;
	
		
		var t = document.getElementById("charges_type_g");
		t.value = "1";
		var m = document.getElementById("charges_mod_g");
		m.value = "1";
		$("#label_mod_g").show();
		
	}else{
	var temp_amount = document.getElementById('amountgtf').value;
	var e = document.getElementById("fccygtf");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
	
	$("#detailg_type3").show();
	
	var intId = $("#detail_proc_gtf div").length + 1;
	
	if((v_ccy) || (v_amount))
	{
		var temp_amount = v_amount;
		var temp_ccy = v_rate;
	
	}else{
	var temp_amount = document.getElementById('amountgtf').value;
	var e = document.getElementById("fccygtf");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
        var val_ccy_gtf = $("<select class=\"fieldtype\" name=\"ccy_gtf["+temp_gtf+"]\" ><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
		var val_amount_gtf = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input type=\'text\' name=\'amount_gtf['+temp_gtf+']\' size=\'8\' maxlength=\'16\'class=\'fieldname\' value=\'' +temp_amount+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_gtf);
        fieldWrapper.append(val_amount_gtf);
		
        fieldWrapper.append(removeButton);
		
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
       
		
		if((temp_amount && temp_ccy) || (v_err_mess)){
			  $("#detail_proc_gtf").append(fieldWrapper);
			temp_gtf++;
		}
      
		
		document.addForm.amountgtf.value = "" ; 
		var e = document.getElementById("fccygtf");
		e.value = "";
		
		
	
}

*/
function installment_type_g()
{    
     var installment_type = $("[name=charges_type_g]").val();
	
     
     if(installment_type == "1")
     {  
    	 
    	 
    	$("#label_mod_g").show();
	 	$("#charges_mod_g").val('1');
		$("#detailg_1").hide();
		$("#detailg_type1").hide();
		$("#detailg_2").show();
		//$("#detailg_3").hide();
		//$("#detailg_type3").hide();
		
		//var selObj = document.getElementById('label_type_g');
		//selObj.selectedIndex = 1;
		
		//$("#label_type_g").show();
		//$("#label_type_g2").hide();
		
		
		$("#label_type_g1").hide();
		$("#label_type_g0").show();
		installment_mod_g();
    	
     }
     else if(installment_type == "0")
     {
    	
    	$("#detailg_type1").show();
    	$("#label_mod_g").hide();
    	$("#detailg_1").show();
		$("#detailg_type2").hide();
		$("#detailg_type3").hide();
		$("#detailg_2").hide();
		$("#detailg_3").hide();
		
		
		
		$("#label_type_g1").show();
		$("#label_type_g0").hide();
     }     
}


function installment_mod_g()
{    
     var installment_mod = $("[name=charges_mod_g]").val();
	 var installment_type = $("[name=charges_type_g]").val();
    
     if(installment_mod == "2" && installment_type == "1")
     {  
	 	$("#detailg_2").hide();
		$("#detailg_type2").hide();
		$("#detailg_3").show();
		$("#detailg_type3").show();
		
    		
     }
     else if(installment_mod == "1" && installment_type == "1")
     {
    	
		
		$("#detailg_2").show();
		$("#detailg_type2").show();
		$("#detailg_3").hide();
		$("#detailg_type3").hide();
     }     
}

//-------------------------------------------Addtional tenor---------------------------------------------------------------------------

function proc_installment_ai(v_ccy,v_rate,v_err_mess)
{
	
	if((v_ccy) || (v_rate))
	{
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
		
		var t = document.getElementById("charges_type_a");
		t.value = "0";
		var m = document.getElementById("charges_mod_a");
		m.value = "0";
		$("#label_mod_a").hide();
		
	
	}else{
	var temp_rate = document.getElementById('rateai').value;
	var e = document.getElementById("fccyai");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	$("#detaila_type1").show();
	
		var intId = $("#detail_proc_a div").length + 1;
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
        var val_rate_a = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input type=\'text\' id=\'rate_ai['+temp_ai+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" style="width:125px" name=\'rate_ai['+temp_ai+']\' size=\'10\' maxlength=\'6\' class=\'fieldname\' value=\'' +temp_rate+ '\' />');
//        var val_ccy_a = $('<select class=\'fieldtype\' name=\'ccy_ai['+temp_ai+']\' ><option value=\'' + temp_ccy +'\'>'+temp_ccy+'</option></select>');
        var val_ccy_a = $('<input type="text" class="fieldtype" name=\'ccy_ai['+temp_ai+']\' id=\'ccy_ai['+temp_ai+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_a);
        fieldWrapper.append(val_rate_a);
       
        fieldWrapper.append(removeButton);
		
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		if((temp_rate && temp_ccy) || (v_err_mess)){
			  $("#detail_proc_a").append(fieldWrapper);
		temp_ai++;
		}
       
		document.addForm.rateai.value = "" ; 
	var e = document.getElementById("fccyai");
	e.value = "";
	

}


function proc_installment_atp(v_ccy,v_rate,v_min,v_max,v_err_mess)
{
	//alert(v_min);
	$("#detaila_type2").show();
	var intId = $("#detail_proc_atp div").length + 1;
	if((v_ccy) || (v_rate))
	{
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
		var temp_max = v_max;
		var temp_min = v_min;
	
	}else{
		var temp_rate = document.getElementById('rateatp').value;
	var e = document.getElementById("fccyatp");
	var temp_ccy = e.options[e.selectedIndex].value;
	var temp_max = document.getElementById('maxatp').value;
	var temp_min = document.getElementById('minatp').value;
	}
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_atp = $("<select class=\"fieldtype\" name=\"ccy_atp["+temp_atp+"]\" ><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
        var val_ccy_atp = $('<input type="text" class="fieldtype" name=\"ccy_atp['+temp_atp+']\" style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
		var val_rate_atp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'rate_atp['+temp_atp+']\' id=\'rate_atp['+temp_atp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'6\' maxlength=\'6\'class=\'fieldname\' value=\'' +temp_rate+ '\' />');
        var val_min_atp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input  name=\'min_atp['+temp_atp+']\' style="width:125px" id=\'min_atp['+temp_atp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' class=\'fieldname\' maxlength=\'16\' value=\'' +temp_min+ '\' />');
        var val_max_atp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'max_atp['+temp_atp+']\' style="width:125px" id=\'max_atp['+temp_atp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' maxlength=\'16\'  class=\'fieldname\' value=\'' +temp_max+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_atp);
        fieldWrapper.append(val_rate_atp);
		 fieldWrapper.append(val_min_atp);
        fieldWrapper.append(val_max_atp);
        fieldWrapper.append(removeButton);
		
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
       
		
		
		if((temp_rate && temp_ccy && temp_min && temp_max) || (v_err_mess)){
			    $("#detail_proc_atp").append(fieldWrapper);
			temp_atp++;
		}
       
		
		document.addForm.rateatp.value = "" ; 
		var e = document.getElementById("fccyatp");
		e.value = "";
		document.addForm.maxatp.value = "" ;
		document.addForm.minatp.value = "" ;
		
	
}

function proc_installment_atf(v_ccy,v_amount,v_err_mess)
{
	$("#detaila_type3").show();
	
	var intId = $("#detail_proc_atf div").length + 1;
	if((v_ccy) || (v_amount))
	{
		var temp_amount = v_amount;
		var temp_ccy = v_ccy;
		
		
		
	}else{
	var temp_amount = document.getElementById('amountatf').value;
	var e = document.getElementById("fccyatf");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_atf = $("<select class=\"fieldtype\" name=\"ccy_atf["+temp_atf+"]\"><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
        var val_ccy_atf = $('<input type="text" class="fieldtype" name=\"ccy_atf['+temp_atf+']\" style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
		var val_amount_atf = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'amount_atf['+temp_atf+']\' style="width:125px" id=\'amount_atf['+temp_atf+']\' type=\'text\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" size=\'8\' maxlength=\'16\'class=\'fieldname\' value=\'' +temp_amount+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_atf);
        fieldWrapper.append(val_amount_atf);
		
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		
		
		if((temp_amount && temp_ccy) || (v_err_mess)){
			$("#detail_proc_atf").append(fieldWrapper);
			temp_atf++;
		}
        
		
		
		document.addForm.amountatf.value = "" ; 
		var e = document.getElementById("fccyatf");
		e.value = "";
		
		
		
	
}


function installment_type_a()
{    
     var installment_type = $("[name=charges_type_a]").val();

     if(installment_type == "1")
     {  
    	
	 	$("#label_mod_a").show();
	 	$("#charges_mod_a").val('1');
		$("#detaila_1").hide();
		$("#detaila_type1").hide();
		$("#detaila_2").show();		 
		//$("#label_type_a").show();
		//document.getElementById("charges_mod_a").value = '1';	
		$("#label_type_a0").show();
		$("#label_type_a1").hide();
		installment_mod_a(); 
		
    	
     }
     else if(installment_type == "0")
     {
    	$("#detaila_type1").show();
    	$("#label_mod_a").hide();
		$("#detaila_1").show();
		$("#detaila_type2").hide();
		$("#detaila_2").hide();
		$("#detaila_3").hide();
		$("#detaila_type3").hide();
		$("#label_type_a").hide();
		$("#label_type_a1").show();
		$("#label_type_a0").hide();
		
     }     
}


function installment_mod_a()
{    
     var installment_type = $("[name=charges_mod_a]").val();

    
     if(installment_type == "2")
     {  
	 	$("#detaila_2").hide();
		$("#detaila_type2").hide();
		$("#detaila_3").show();
		$("#detaila_type3").show();
		
    		
     }
     else if(installment_type == "1")
     {
    	
		
		$("#detaila_2").show();
		$("#detaila_type2").show();
		$("#detaila_3").hide();
		$("#detaila_type3").hide();
     }     
}

//-----------------------------------------Penalty Period-----------------------------------------------------------
function proc_installment_pi(v_ccy,v_rate,v_err_mess)
{
	if((v_ccy) || (v_rate))
	{
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
		
		
		var t = document.getElementById("charges_type_p");
		t.value = "0";
		var m = document.getElementById("charges_mod_p");
		m.value = "0";
		$("#label_mod_p").hide();
		
		
	
	}else{
	var temp_rate = document.getElementById('ratepi').value;
	var e = document.getElementById("fccypi");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
	$("#detailp_type1").show();
	
		var intId = $("#detail_proc_p div").length + 1;
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
        var val_rate_p = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'rate_pi['+temp_pi+']\' style="width:125px" id=\"rate_pi['+temp_pi+']\" onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'10\' class=\'fieldname\' maxlength=\'6\' value=\'' +temp_rate+ '\' />');
//        var val_ccy_p = $("<select class=\"fieldtype\" name=\"ccy_pi["+temp_pi+"]\" ><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
        var val_ccy_p = $('<input type="text" class="fieldtype" name=\"ccy_pi['+temp_pi+']\"  style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_p);
        fieldWrapper.append(val_rate_p);
       
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		if((temp_rate && temp_ccy) || (v_err_mess)){
			 $("#detail_proc_p").append(fieldWrapper);
		temp_pi++;
		}
       
       
		document.addForm.ratepi.value = "" ; 
	var e = document.getElementById("fccypi");
	e.value = "";
	
	
}


function proc_installment_ptp(v_ccy,v_rate,v_min,v_max,v_err_mess)
{
	if((v_ccy) || (v_rate))
	{
		var temp_rate = v_rate;
		var temp_ccy = v_ccy;
		var temp_max = v_max;
		var temp_min = v_min;
		
		
		//var t = document.getElementById("charges_type_p");
		//t.value = "1";
		//var m = document.getElementById("charges_mod_p");
		//m.value = "0";
		$("#label_mod_p").show();
		
	}else{
		var temp_rate = document.getElementById('rateptp').value;
	var e = document.getElementById("fccyptp");
	var temp_ccy = e.options[e.selectedIndex].value;
	var temp_max = document.getElementById('maxptp').value;
	var temp_min = document.getElementById('minptp').value;
	
	}
		
	$("#detailp_type2").show();
	var intId = $("#detail_proc_ptp div").length + 1;
	
	
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_ptp = $("<select class=\"fieldtype\" name=\"ccy_ptp["+temp_ptp+"]\" ><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
        var val_ccy_ptp = $('<input type="text" class="fieldtype" name=\"ccy_ptp['+temp_ptp+']\" style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        
		var val_rate_ptp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'rate_ptp['+temp_ptp+']\' id=\'rate_ptp['+temp_ptp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'6\' maxlength=\'6\'class=\'fieldname\' value=\'' +temp_rate+ '\' />');
        var val_min_ptp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input type=\'text\' name=\'min_ptp['+temp_ptp+']\' style="width:125px" id=\'min_ptp['+temp_ptp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" maxlength=\'16\' class=\'fieldname\' value=\'' +temp_min+ '\' />');
        var val_max_ptp = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input type=\'text\' name=\'max_ptp['+temp_ptp+']\' style="width:125px" id=\'max_ptp['+temp_ptp+']\' onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" maxlength=\'16\' class=\'fieldname\' value=\'' +temp_max+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_ptp);
        fieldWrapper.append(val_rate_ptp);
		 fieldWrapper.append(val_min_ptp);
        fieldWrapper.append(val_max_ptp);
        fieldWrapper.append(removeButton);
		
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
       
		
		if((temp_rate && temp_ccy && temp_min && temp_max) || (v_err_mess)){
			     $("#detail_proc_ptp").append(fieldWrapper);
			temp_ptp++;
		}
       
		document.addForm.rateptp.value = "" ; 
		var e = document.getElementById("fccyptp");
		e.value = "";
		document.addForm.maxptp.value = "" ;
		document.addForm.minptp.value = "" ;
		
	
}

function proc_installment_ptf(v_ccy,v_amount,v_err_mess)
{

	
	if((v_ccy) || (v_amount))
	{
		var temp_amount = v_amount;
		var temp_ccy = v_ccy;
		
	}else{
	var temp_amount = document.getElementById('amountptf').value;
	var e = document.getElementById("fccyptf");
	var temp_ccy = e.options[e.selectedIndex].value;
	}
	
	$("#detailp_type3").show();
	
	var intId = $("#detail_proc_ptf div").length + 1;
		
        var fieldWrapper = $("<div class=\"fieldwrapper\" id=\"field" + intId + "\"/>");
      
//        var val_ccy_ptf = $("<select class=\"fieldtype\" name=\"ccy_ptf["+temp_ptf+"]\"><option value=\"" + temp_ccy +"\">"+temp_ccy+"</option></select>");
        var val_ccy_ptf = $('<input type="text" class="fieldtype" name=\"ccy_ptf['+temp_ptf+']\"   style="width:25px" readonly="readonly" value=\'' + temp_ccy +'\'>');
        var val_amount_ptf = $(' &nbsp; &nbsp; &nbsp; &nbsp;<input name=\'amount_ptf['+temp_ptf+']\' style="width:125px" id=\"amount_ptf['+temp_ptf+']\" onkeypress="return forceDecimalOCBC(event)" onfocus="iNumFrmtUS(this.value,this.id)" onblur="NumFrmtUS(this.value,this.id)" type=\'text\' size=\'8\' maxlength=\'16\'class=\'fieldname\' value=\'' +temp_amount+ '\' />');
        
		var removeButton = $("<input type=\"button\" class=\"remove\" value=\"Remove\" />");
        removeButton.click(function() {
            $(this).parent().remove();
        });
		fieldWrapper.append(val_ccy_ptf);
        fieldWrapper.append(val_amount_ptf);
		
        fieldWrapper.append(removeButton);
		if(v_err_mess)
		{
			 var err_message = $("<label class=\"fielderr\">"+v_err_mess+"</label>");
			 fieldWrapper.append(err_message);
		
		}
		
		
		if((temp_amount && temp_ccy) || (v_err_mess)){
			 $("#detail_proc_ptf").append(fieldWrapper);
			temp_ptf++;
		}
		
		document.addForm.amountptf.value = "" ; 
		var e = document.getElementById("fccyptf");
		e.value = "";
		
		
	
}


function installment_type_p()
{    
     var installment_type = $("[name=charges_type_p]").val();

    
     if(installment_type == "1")
     {  
    	 
    	$("#label_mod_p").show();
	 	$("#charges_mod_p").val('1');
		$("#detailp_1").hide();
		$("#detailp_type1").hide();
		$("#detailp_2").show();
		
			//var selObj = document.getElementById('label_type_p');
		//selObj.selectedIndex = 0;
		
		$("#label_type_p0").show();
		$("#label_type_p1").hide();
		installment_mod_p();
     }
     else if(installment_type == "0")
     {
    	$("#detailp_type1").show();
    	$("#label_mod_p").hide();
		$("#detailp_1").show();
		$("#detailp_type2").hide();
		$("#detailp_2").hide();
		$("#detailp_3").hide();
		$("#detailp_type3").hide();
		
			//var selObj = document.getElementById('label_type_p');
		//selObj.selectedIndex = 1;
		
		$("#label_type_p0").hide();
		$("#label_type_p1").show();
		
     }     
}

	
function installment_mod_p()
{    
     var installment_type = $("[name=charges_mod_p]").val();

     if(installment_type == "2")
     {  
	 	$("#detailp_2").hide();
		$("#detailp_type2").hide();
		$("#detailp_3").show();
		$("#detailp_type3").show();
		
    		
     }
     else if(installment_type == "1")
     {
    	
		
		$("#detailp_2").show();
		$("#detailp_type2").show();
		$("#detailp_3").hide();
		$("#detailp_type3").hide();
     }     
}

//--------------------------------------------------------------------------------------------------------------------------
//menyembunyikan field installment period & frequency
function hide_start()
{
	$("#label_mod").hide();
	$("#view_transactionfee").hide();
	$(".view_transactionfee").hide();
	$("#detaild_2").hide();
	$("#detaild_type2").hide();
	$("#detaild_3").hide();
	$("#detaild_type3").hide();
	
	$("#detaild_type1").hide();	
	$("#fullDisb").hide();
	$("#detaildit_1").show();
	$("#detaildit_type1").hide();
	$("#detaildit_2").hide();
	$("#detaildit_type2").hide();
	//$("#type_mod_di").hide();
	$('select#label_type_d').html('<option value="1">On Disbursement (Upfront)</option>');
	$('select#label_type_d').val('1');
	
	//--grace
	$("#label_mod_g").hide();
	$("#detailg_2").hide();
	$("#detailg_type2").hide();
	$("#detailg_3").hide();
	$("#detailg_type3").hide();
	
	$("#detailg_type1").hide();	
	
	//--addtional
	//$("#label_mod_a").hide();
	$("#detaila_1").hide();
	$("#detaila_type2").hide();
	$("#detaila_3").hide();
	$("#detaila_type3").hide();
	
	$("#detaila_type1").hide();	
		
	//--penalty
	//$("#label_mod_p").hide();
	$("#detailp_2").hide();
	$("#detailp_type2").hide();
	$("#detailp_1").hide();
	$("#detailp_type1").hide();
	
	$("#detailp_type1").hide();	
	$("#hidep").hide();
	$("#submitbut").hide();	
}

function show_start()
{
	
	var selObj = document.getElementById('charges_type_a');
	selObj.selectedIndex = 1;
	
	var f = document.getElementById("charges_mod_a");
	f.value = "1";
	
	var selObj3 = document.getElementById('charges_type_p');
	selObj3.selectedIndex = 1;
	
	
	var m = document.getElementById("charges_mod_p");
	m.value = 2;
	
	
	$("#label_mod_a").show();
	$("#detaila_2").show();
	
	$("#label_type_g1").show();
	$("#label_type_g0").hide();
	
	$("#label_type_p0").show();
	$("#label_type_p1").hide();
	
	$("#label_type_a0").show();
	$("#label_type_a1").hide();		
	
	

}

// akan di onload pertama kali
/*$(document).ready(function()
{
	 // menyembunyikan baris Installment Period  & Installment Frequency

		
	   hide_start()
	   
	   show_start()       
});*/
function loadpage()
{
	 // menyembunyikan baris Installment Period  & Installment Frequency
	   hide_start();
	   
	   show_start();
	//dialog_conf()

		//-------------
		
	
	

}

function dialog_conf()
{
	var default_message_for_dialog = 'Do You Want Continue Submit data ?';
	$("#dialog").dialog({
		modal: true,
		bgiframe: true,
		width: 300,
		height: 200,
		autoOpen: false,
		title: 'Confirm Message'
		});

	// LINK
	$("a.confirm").click(function(link) {
        link.preventDefault();
        var theHREF = $(this).attr("href");
		var theREL = $(this).attr("rel");
		var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
		var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
		// set windows content
		$('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');
		
		$("#dialog").dialog('option', 'buttons', {
                "Confirm" : function() {
                    window.location.href = theHREF;
                    },
                "Cancel" : function() {
                    $(this).dialog("close");
                    }
                });
		$("#dialog").dialog("open");
		});

	
	// FORMS
	$('input.confirm').click(function(theINPUT){
		theINPUT.preventDefault();
		var theFORM = $(theINPUT.target).closest("form");
		var theREL = $(this).attr("rel");
		var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
		var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
		$('#dialog').html('<P>' + theICON + theMESSAGE + '</P>');
		$("#dialog").dialog('option', 'buttons', {
                "Confirm" : function() {
					addact();
                    },
                "Cancel" : function() {
                    $(this).dialog("close");
                    }
                });
		$("#dialog").dialog("open");
		});
}
function installment_type_d()
{    
     var installment_type = $("[name=charges_type_d]").val();
     
     if(installment_type == "2")
     {
    	$("#detaild_type3").show();
    	$("#label_mod").hide();
		$("#detaild_1").show();
		$("#detaild_2").hide();
		$("#detaild_3").hide();
		$("#detaild_type2").hide();
		$("#detaild_type3").hide();
		$("#view_transactionfee").show();
		$(".view_transactionfee").show();
		$("#type_mod_di").show();
		//document.getElementById("label_type_d").value = '0';
		//$("#type_mod_di").hide();
		$('select#label_type_d').html('<option value="1">On Disbursement (Upfront)</option>');
		$('select#label_type_d').val('1');
		$("#detaild_type1").show();
	   	 if($("#charges_mod_d2").val()==1){
			 $("#detaildit_type1").show();
		 }else{
			 $("#detaildit_type2").show();
		 }
		 installment_mod_d2();
     }
	 else if(installment_type == "1")
     {  
		$("#detaild_type2").show();
		$("#label_mod").show();
	 	$("#charges_mod_d").val('1');
		$("#detaild_1").hide();
		$("#detaild_type1").hide();
		$("#view_transactionfee").hide();
		$(".view_transactionfee").hide();
		$("#detaild_2").show();
		$("#detaild_3").hide();
		$("#detaild_type3").hide();
		$("#type_mod_di").show();
		$('select#label_type_d').html('<option value="1">On Disbursement (Upfront)</option><option value="2">On Maturity Date (After)</option>');
		$('select#label_type_d').val('1');
		$('#detaildit_1').hide();
		$('#detaildit_type1').hide();
		$('#detaildit_2').hide();
		$('#detaildit_type2').hide();
    	
     }
     else if(installment_type == "0")
     {
    	$("#detaild_type1").show();
    	$("#label_mod").hide();
		$("#detaild_1").show();
		$("#detaild_type2").hide();
		$("#detaild_type3").hide();
		$("#detaild_2").hide();
		$("#detaild_3").hide();
		$("#view_transactionfee").hide();
		$(".view_transactionfee").hide();
		//document.getElementById("label_type_d").value = '0';
		
		//$("#type_mod_di").hide();
		$('select#label_type_d').html('<option value="1">On Disbursement (Upfront)</option>');
		$('select#label_type_d').val('1');
		$('#detaildit_1').hide();
		$('#detaildit_type1').hide();
		$('#detaildit_2').hide();
		$('#detaildit_type2').hide();
		
     }
}


function installment_mod_d()
{    
     var installment_mod = $("[name=charges_mod_d]").val();
      var installment_type = $("[name=charges_type_d]").val();
     if(installment_mod == "2" && installment_type == "1")
     {  
	 	$("#detaild_2").hide();
		$("#detaild_type2").hide();
		$("#detaild_3").show();
		$("#detaild_type3").show();
		
    		
     }
     else if(installment_mod == "1" && installment_type == "1")
     {
    	
		
		$("#detaild_2").show();
		$("#detaild_type2").show();
		$("#detaild_3").hide();
		$("#detaild_type3").hide();
     }     
}


function installment_mod_d2()
{    
     var installment_type = $("[name=charges_mod_d2]").val();

     if(installment_type == "2")
     {  
	 	$("#detaildit_1").hide();
		$("#detaildit_type1").hide();
		$("#detaildit_2").show();
		$("#detaildit_type2").show();
		
    		
     }
     else if(installment_type == "1")
     {
		
		$("#detaildit_1").show();
		$("#detaildit_type1").show();
		$("#detaildit_2").hide();
		$("#detaildit_type2").hide();
     }     
}

function confirmSubmit()
{
var agree=confirm("Do you want to continue submit data ?");
if (agree)
	return true ;
else
	return false ;
}


function addact()
{	

	$("#dialog").dialog("close");
	$('#submit').click();


}

function validasii()
{
	 $("#ratedi").keypress(function(event) {
        keyvalue = event.charCode;
    });
	
	if (keyvalue!=8){ //if the key isn't the backspace key (which we should allow)
		if (keyvalue<48||keyvalue>57) //if not a number
			return false //disable key press
		}
	

}

function ValidateValue(str)
{
  var regexPattern = /(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/i;
  if(!str.test(regexPattern))
  {
    alert("value out of range or too much decimal");
  } 
}

