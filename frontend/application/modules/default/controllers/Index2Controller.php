<?php
require_once 'General/Settings.php';
require_once 'General/CustomerUser.php';
require_once 'General/Login.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Gclient.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/Getest/lib/class.geetestlib.php';
require_once 'SGO/Extendedmodule/Getest/config/config.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class Index2Controller extends Application_Main
{

  public function getUserIP()
  {
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
      $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
      $ip = $forward;
    } else {
      $ip = $remote;
    }

    return $ip;
  }

  public function get_browser_name($user_agent)
  {
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
    elseif (strpos($user_agent, 'Edge')) return 'Edge';
    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
    elseif (strpos($user_agent, 'Safari')) return 'Safari';
    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

    return 'Other';
  }

  public function initController()
  {
    //$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

    $setting = new Settings();
    $this->_maxRow = $setting->getSetting('max_import_bulk');
  }

  public function testrabbitAction()
  {



    $selectTrx1  = $this->_db->select()
      //->from(	array('TEMP_BULKPSLIP'),array('*'))
      ->from(array('P' => 'TEMP_BULKPSLIP'), array('P.*'))
      ->join(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID')
      ->order('P.VALIDATION DESC')
      ->where('P.PS_CATEGORY = ?', 'BULK PAYMENT')
      ->where('DATE(P.PS_CREATED) = DATE(NOW())');

    $pslipTrxData1 = $this->_db->fetchAll($selectTrx1);

    foreach ($pslipTrxData1 as $key) {
      $PS_FILE = $key['PS_FILE'];

      $imgsrc = LIBRARY_PATH . '/data/uploads/document/temp/' . $PS_FILE;
      $file_contents = file_get_contents($imgsrc);


      $file = explode(PHP_EOL, $file_contents);
      $extension = 'txt';
      $delimitedWith = '|';
      $newFileName = $imgsrc;
      $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);
      unset($data[0]);

      echo "<pre>";
      var_dump($data);
      die();

      $totalRecords = count($data);

      if ($totalRecords) {

        if ($totalRecords <= $this->_maxRow) {
          $rowNum = 0;

          $paramPayment = array(
            "CATEGORY"        => "BULK PAYROLL",
            "FROM"           => "I",
            "PS_NUMBER"       => "",
            "PS_SUBJECT"        => $key['PS_SUBJECT'],
            "PSFILEID"      => $key['FILE_ID'],
            "PS_EFDATE"       => $key['PS_EFDATE'],
            "PS_FILE"         => $key['PS_FILE'],
            "_dateFormat"      => $this->_dateDisplayFormat,
            "_dateDBFormat"      => $this->_dateDBFormat,
            "_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
            "_beneLinkage"      => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
            "_createPB"       => $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
            "_createDOM"      => $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
            "_createREM"      => false,        // cannot create REM trx
          );

          $paramTrxArr = array();
          // Zend_Debug::dump($fixData); die;

          foreach ($data as $row) {
            // if(count($row)==4)
            // {
            // var_dump($row);die;
            $rowNum++;
            $benefAcct     = trim($row[0]);
            //$benefName 		= trim($row[1]);
            $ccy       = "IDR";
            $amount     = trim($row[1]);
            $message     = trim($row[2]);
            $reference     = trim($row[3]);
            $sms_notif     = trim($row[4]);
            $email_notif   = trim($row[5]);
            $addMessage   = '';
            //$email 		= trim($row[6]);
            // 											$phoneNumber	= trim($row[7]);
            $type       = 'PB';
            // 											$bankCode 		= trim($row[6]);
            //$bankName 		= trim($row[10]);
            //										$bankCity = trim($row[10]);
            // 											$benefAdd		= trim($row[9]);
            // 											$citizenship	= strtoupper(trim($row[10]));
            //$resident = strtoupper(trim($row[11]));

            /*
							 * Change parameter into document
							 */
            $fullDesc = array(
              'BENEFICIARY_ACCOUNT'     => $benefAcct,
              'BENEFICIARY_NAME'       => '',
              'BENEFICIARY_ACCOUNT_CCY'   => $ccy,
              'TRA_AMOUNT'         => $amount,
              'TRA_MESSAGE'        => $message,
              'REFNO'           => $addMessage,
              'SMS_NOTIF'          => $sms_notif,
              'EMAIL_NOTIF'         => $email_notif,
              'BENEFICIARY_EMAIL'     => '',
              'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
              'TRANSFER_TYPE'       => 'PB',
              //'CLR_CODE' 					=> $bankCode,
              //'BENEFICIARY_BANK_NAME' 	=> $bankName,
              //											'BENEFICIARY_CITY' => $bankCity,
              'BENEFICIARY_ADDRESS'    => '',
              'BENEFICIARY_CITIZENSHIP'   => ''
              //'BENEFICIARY_RESIDENT' => $resident
            );



            $filter = new Application_Filtering();

            $SMS_NOTIF     = $filter->filter($sms_notif, "SMS");
            $EMAIL_NOTIF     = $filter->filter($email_notif, "EMAIL");

            $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
            $TRA_MESSAGE     = $filter->filter($message, "TRA_MESSAGE");
            $TRA_REFNO       = $filter->filter($addMessage, "TRA_REFNO");
            $ACBENEF       = $filter->filter($benefAcct, "ACCOUNT_NO");
            $ACBENEF_BANKNAME   = $filter->filter('', "ACCOUNT_NAME");
            $ACBENEF_ALIAS     = $filter->filter('', "ACCOUNT_ALIAS");
            $ACBENEF_EMAIL     = $filter->filter('', "EMAIL");
            $ACBENEF_PHONE     = $filter->filter('', "MOBILE_PHONE_NUMBER");
            $ACBENEF_CCY     = $filter->filter($ccy, "SELECTION");
            $ACBENEF_ADDRESS  = $filter->filter('', "ADDRESS");
            $ACBENEF_CITIZENSHIP = $filter->filter('', "SELECTION");
            //$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
            //$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
            //$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
            //$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
            $TRANSFER_TYPE     = $filter->filter($type, "SELECTION");

            $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

            if ($TRANSFER_TYPE == 'RTGS') {
              $chargeType = '1';
              $select = $this->_db->select()
                ->from('M_CHARGES_OTHER', array('*'))
                ->where("CUST_ID = ?", $this->_custIdLogin)
                ->where("CHARGES_TYPE = ?", $chargeType);
              $resultSelecet = $this->_db->FetchAll($select);
              $chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
            } else if ($TRANSFER_TYPE == 'SKN') {
              $chargeType1 = '2';
              $select1 = $this->_db->select()
                ->from('M_CHARGES_OTHER', array('*'))
                ->where("CUST_ID = ?", $this->_custIdLogin)
                ->where("CHARGES_TYPE = ?", $chargeType1);
              $resultSelecet1 = $this->_db->FetchAll($select1);
              $chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
            } else {
              $chargeAmt = '0';
            }


            $filter->__destruct();
            unset($filter);

            $paramTrx = array(
              "TRANSFER_TYPE"       => $TRANSFER_TYPE,
              "TRA_AMOUNT"         => $TRA_AMOUNT_num,
              "TRANSFER_FEE"         => $chargeAmt,
              "TRA_MESSAGE"         => $TRA_MESSAGE,
              "TRA_REFNO"         => $TRA_REFNO,
              "ACCTSRC"           => $key['SOURCE_ACCOUNT'],
              "ACBENEF"           => $ACBENEF,
              "ACBENEF_CCY"         => $ACBENEF_CCY,
              "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
              "ACBENEF_PHONE"       => $ACBENEF_PHONE,
              "SMS_NOTIF"          => $SMS_NOTIF,
              "EMAIL_NOTIF"        => $EMAIL_NOTIF,
              // for Beneficiary data, except (bene CCY and email), must be passed by reference
              "ACBENEF_BANKNAME"       => $ACBENEF_BANKNAME,
              "ACBENEF_ALIAS"       => $ACBENEF_ALIAS,
              "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // W: WNI, N: WNA
              //	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
              "ACBENEF_ADDRESS1"       => $ACBENEF_ADDRESS,
              "REFERENCE"          => $reference,
              //	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
              //	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

              //	"ORG_DIR" 					=> $ORG_DIR,
              //"BANK_CODE" 				=> $CLR_CODE,
              //	"BANK_NAME" 				=> $BANK_NAME,
              //	"BANK_BRANCH" 				=> $BANK_BRANCH,
              //	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
              //	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
              //	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
            );

            array_push($paramTrxArr, $paramTrx);
          }
        }

        $validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
        $resultVal  = $validate->checkCreatePayroll($paramPayment, $paramTrxArr);
        $payment   = $validate->getPaymentInfo();
      }
    }
  }

  private function convertFileToArray($newFileName, $extension, $delimitedWith)
  {

    $file_contents = file_get_contents($newFileName);

    //if csv occured
    if ($extension === 'csv') {

      if (!empty($delimitedWith)) {
        $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
      }
      //if fix length
      else if ($fixLength == 1) {

        $fileContents = file($newFileName);

        $contentOrder = $fixLengthContent;

        // if with header
        if ($fixLengthType == 1) {
          //karena yg diambil hanya order dri row ke 2 saja
          $startArrIndex = 1;
          $surplusIndex = 0;
        } else if ($fixLengthType == 3) {
          //karena yg diambil hanya order dri row ke 1 saja
          $startArrIndex = 0;
          $surplusIndex = 1;
        }

        $contentOrderArr = explode(',', $contentOrder);

        if (count($contentOrderArr) > 1) {
          foreach ($fileContents as $key => $value) {
            if ($key >= $startArrIndex) {
              foreach ($contentOrderArr as $key2 => $value2) {
                //first order, startIndex from 0
                if ($key2 == 0) {
                  $startIndex = 0;
                  $endIndex = (int) $value2 + 1;

                  $data[$key + $surplusIndex][] = substr($value, $startIndex, $endIndex);
                } else {
                  $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                  $endIndex = (int) ($value2 - ($startIndex - 1));

                  $data[$key + $surplusIndex][] = substr($value, $startIndex, $endIndex);
                }
              }
            }
          }
        }
      } else {
        $data = $this->_helper->parser->parseCSV($newFileName);
      }
    }
    //if txt occured
    else if ($extension === 'txt') {

      $lines = file($newFileName);

      $checkMt940 = false;
      $checkMt101 = false;
      foreach ($lines as $line) {
        if (strpos($line, '{1:') !== false) {
          $checkMt940 = true;
        } else if (strpos($line, ':20:') !== false) {
          $checkMt101 = true;
        }
      }

      //if mt940 format
      if ($checkMt940) {

        $data = $this->_helper->parser->mt940($newFileName);
        $mtFile = true;
      } else if ($checkMt101) {
        $data = $this->_helper->parser->mt101($newFileName);
        $mtFile = true;
      } else {
        //parse csv jg bs utk txt
        if (!empty($delimitedWith)) {
          $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
        }
        //if fix length
        else if ($fixLength == 1) {

          $fileContents = file($newFileName);

          $contentOrder = $fixLengthContent;

          // if with header
          if ($fixLengthType == 1) {
            //karena yg diambil hanya order dri row ke 2 saja
            $startArrIndex = 1;
            $surplusIndex = 0;
          } else if ($fixLengthType == 3) {
            //karena yg diambil hanya order dri row ke 1 saja
            $startArrIndex = 0;
            $surplusIndex = 1;
          }

          $contentOrderArr = explode(',', $contentOrder);

          if (count($contentOrderArr) > 1) {
            foreach ($fileContents as $key => $value) {
              if ($key >= $startArrIndex) {
                foreach ($contentOrderArr as $key2 => $value2) {
                  //first order, startIndex from 0
                  if ($key2 == 0) {
                    $startIndex = 0;
                    $endIndex = (int) $value2 + 1;

                    $data[$key + $surplusIndex][] = substr($value, $startIndex, $endIndex);
                  } else {
                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                    $endIndex = (int) ($value2 - ($startIndex - 1));

                    $data[$key + $surplusIndex][] = substr($value, $startIndex, $endIndex);
                  }
                }
              }
            }
          }
        } else {
          $data = $this->_helper->parser->parseCSV($newFileName, '|');
        }
      }
    }
    //if json occured
    else if ($extension === 'json') {

      $datajson = json_decode($file_contents, 1);
      $i = 0;
      foreach ($datajson as $key => $value) {
        if ($i == 0) {
          $data[$i] = array_keys($value);
          $data[$i + 1] = array_values($value);
        } else {
          $data[$i + 1] = array_values($value);
        }

        $i++;
      }
    }
    //if xml occured
    else if ($extension === 'xml') {

      $xml = (array) simplexml_load_string($file_contents);

      $i = 0;
      foreach ($xml as $key => $value) {
        foreach ($value as $key2 => $value2) {
          if ($i == 0) {
            $data[$i] = array_keys((array) $value2);
            $data[$i + 1] = array_values((array) $value2);
          } else {
            $data[$i + 1] = array_values((array) $value2);
          }

          $i++;
        }
      }

      for ($i = 0; $i < count($data); $i++) {
        if ($i > 0) {
          foreach ($data[$i] as $key => $value) {
            if (empty($value)) {
              $data[$i][$key] = null;
            }
          }
        }
      }

      // print_r($data);die();
    } else if ($extension === 'xls' || $extension === 'xlsx') {
      try {
        $inputFileType = IOFactory::identify($newFileName);
        $objReader = IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($newFileName);
      } catch (Exception $e) {
        die('Error loading file "' . pathinfo($newFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
      }

      $sheet = $objPHPExcel->getSheet(0);
      $highestRow = $sheet->getHighestRow();
      $highestColumn = $sheet->getHighestColumn();

      for ($row = 1; $row <= $highestRow; $row++) {
        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

        $data[$row - 1] = $rowData[0];
      }
    }

    return $data;
  }

  public function indexAction()
  {

    $this->_helper->layout()->setLayout('newlayout');
    if (isset($_GET['changepw'])) {
      $this->_redirect('/');
    }
    $ns = new Zend_Session_Namespace('language');
    // var_dump($ns);die;

    $this->view->gClient = Gclient::getGClient();

    $set = $this->_getParam('lang');
    // $ns = new Zend_Session_Namespace('language');
    // var_dump($set);
    // var_dump($this->_getAllParams ());
    if ($set) {
      // var_dump($set);die;
      //// Start Multi Language
      $defaultlanguage = $set;
      $ns = new Zend_Session_Namespace('language');
      $sessionLanguage = $ns->langCode;



      if (!is_null($sessionLanguage)) {
        $setlang = $sessionLanguage;
      } else {
        $setlang = $locale->getLanguage();
      }
      $ns->langCode = $lang;
      // var_dump($ns);
      if (!empty($setlang) && isset($setlang)) {
        $defaultlanguage = $setlang;
        $ns->langCode = $defaultlanguage;
      }
      Zend_Loader::loadClass('Zend_Translate');

      $frontendOptions = array(
        'cache_id_prefix' => 'MyLanguage',
        'lifetime' => 86400,
        'automatic_serialization' => true
      );
      $backendOptions = array('cache_dir' => APPLICATION_PATH . '/../../library/data/cache/language/frontend');
      $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
      Zend_Translate::setCache($cache);

      $translate = new Zend_Translate('tmx', APPLICATION_PATH . '/../../library/data/languages/frontend/language.tmx', $defaultlanguage);
      // Zend_Debug::dump($translate);die;
      Zend_Registry::set('language', $translate);
      //// End Multi Language

      $lang = $set;
      $ns->langCode = $lang;
      $this->_redirect('/');
    }
    $ns = new Zend_Session_Namespace('language');
    // var_dump($ns);die;
    // print_r($defaultlanguage);die;

    $translate = new Zend_Translate('tmx', APPLICATION_PATH . '/../../library/data/languages/frontend/language.tmx', $defaultlanguage);
    // var_dump($ns);die;
    // 		Zend_Debug::dump($translate);
    Zend_Registry::set('language', $translate);

    // print_r($this->language);die;

    //---------------------- GENERATE CAPTCHA IMAGE  --------------------------------------
    $captcha = Application_Captcha::generateCaptcha();
    $this->view->captchaId = $captcha['id']; //returns the ID given to session &amp; image
    $this->view->captImgDir = $captcha['imgDir'];
    //---------------------	END GENERATE CAPTCHA IMAGE -------------------------------------
    $confirmPage = $this->_getParam('confirmPage', 1);
    $tokenAuth = $this->_getParam('tokenAuth', 0);

    $challengeCode = NULL;
    $challengeCode = new Service_Token(NULL, NULL);
    $challengeCode = $challengeCode->generateChallengeCode();

    $sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');

    $app = Zend_Registry::get('config');
    if ($app['captcha']['emulate'] == 1) {
      $this->view->capchahide = 'block';
    } else {
      $this->view->capchahide = 'none';
    }

    if ($this->_request->isPost()) {
      $token = '';

      $filters = array(
        'userId' => array('StripTags', 'StringTrim', 'StringToUpper'),
        'custId' => array('StripTags', 'StringTrim', 'StringToUpper'),
        'password' => array(),
        'challengeCode' => array('StripTags', 'StringTrim', 'StringToUpper'),
        'responseCode' => array('StripTags', 'StringTrim', 'StringToUpper'),
      );

      $validators = array(
        'userId'         => array(
          'NotEmpty',
          //					'Alnum',
          'messages' => array(
            $this->language->_('User ID cannot be empty'),
            //						$this->language->_('Invalid User ID Format'),
          ),
        ),
        'custId'         => array(
          'NotEmpty',
          //					'Alnum',
          'messages' => array(
            $this->language->_('Customer ID cannot be empty'),
            //						$this->language->_('Invalid Customer ID Format'),
          ),
        ),
        'password'         => array(
          'NotEmpty',
          'messages' => array(
            $this->language->_('Password cannot be empty'),
          ),
        ),
        'challengeCode'         => array(),
        'responseCode'         => array(
          'Digits',
          'messages' => array(
            $this->language->_('Invalid Response Token Format'),
          ),
        ),
      );

      $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_getAllParams(), $this->_optionsValidator);
      // ========================== END FILTERING =======================================

      //================================BYPASSCAPCAY (|| true========================================//

      $sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
      $sessionNameRand->randomTransact = $this->_getParam('randomTransact');
      if ($this->_getParam('randomTransact') == $sessionNameRand->randomTransact) {
        try {
          $passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
          $blocksize = 256;  // can be 128, 192 or 256
          $custidEnc =  $this->_getParam('custId');
          $useridEnc =  $this->_getParam('userId');
          $authkeysEnc =   $this->_request->getParam('password');
          // 					print_r($custidEnc);die;
        } catch (Exception $e) {
          $custidEnc = '';
          $useridEnc = '';
          $authkeysEnc = '';
        }
      } else {
        $this->view->sessionExpired = $this->language->_('Session expired');
        $this->_redirect('/');
      }
      // 			print_r($custidEnc);die;
      $randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
      $this->view->randomTransact = $randomTransact;
      $sessionNameRand->randomTransact = $randomTransact;
      $GtSdk = new GeetestLib(CAPTCHA_ID, PRIVATE_KEY);

      if ($app['captcha']['emulate'] == 1) {
        $captchaValid = true;
      } else {

        if ($_SESSION['gtserver'] == 1) {   //服务器正常
          $data = array();
          $resultcapca = $GtSdk->success_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'], $data);
          if ($resultcapca) {

            $captchaValid = true;
            //echo '{"status":"success"}';
          } else {
            $captchaValid = false;
            // echo 'here';
            //echo '{"status":"fail"}';
          }
        } else {  //服务器宕机,走failback模式
          if ($GtSdk->fail_validate($_POST['geetest_challenge'], $_POST['geetest_validate'], $_POST['geetest_seccode'])) {
            $captchaValid = true;
            //echo '{"status":"success1"}';
          } else {
            $captchaValid = false;
            // echo 'here1';
            // echo '{"status":"fail1"}';
          }
        }
      }
      //var_dump($confirmPage);
      //var_dump($captchaValid);die;
      if ($confirmPage == 1 && $captchaValid) {

        $compId = $this->_getParam('custId');
        // 			    echo $cud;
        //cek company id dan user id match
        // 				$compId = $custidEnc;//$this->_getParam('custId');
        $userIdPost = $useridEnc; //$this->_getParam('userId');
        $userStatusPost = 3;

        $dataCompId = $this->_db->select()
          ->from(
            array('M_USER')
            //				array('CUST_TOKEN_AUTH' => "IFNULL(CUST_TOKEN_AUTH,'N')")
          )
          ->where('CUST_ID = ?', $compId)
          ->where('USER_ID = ?', $userIdPost)
          ->where('USER_STATUS != ?', $userStatusPost)
          ->limit(1);
        // echo $dataCompId;die;
        $dataComp = $this->_db->fetchRow($dataCompId);

        if (!$zf_filter_input->isValid()) {
          $errors = '';
          $listMsg = $zf_filter_input->getMessages();
          foreach ($listMsg as $key) {
            foreach ($key as $msg) {
              $errors .= $msg . PHP_EOL;
            }
          }
        } elseif (!$dataComp) {
          $errors = $this->language->_('Invalid Company ID or User ID');
          Application_Helper_General::writeLogAnonymous('FLGN', 'Failed Login into System Invalid Company ID or User ID' . $user_ip = $this->getUserIP(), $userId, $custid);
        }

        $custId = ($custidEnc);
        $tokenAuth = $this->_db->select()
          ->from(
            array('M_CUSTOMER'),
            array('CUST_TOKEN_AUTH' => "IFNULL(CUST_TOKEN_AUTH,'N')")
          )
          ->where('CUST_ID = ?', (string) $custId)
          ->where('CUST_TOKEN_AUTH = ?', 'Y')
          ->limit(1);

        $tokenAuth = $this->_db->fetchRow($tokenAuth);
        $tokenAuth = (isset($tokenAuth['CUST_TOKEN_AUTH']) && $tokenAuth['CUST_TOKEN_AUTH'] == 'Y' ? 1 : 0);






        $cekpass   = false;
        $userId = ($useridEnc); //($zf_filter_input->userId);
        $custId = ($custidEnc); //($zf_filter_input->custId);
        $blocksize = 256;
        $passwordRand = $sessionNameRand->randomTransact;
        // 				$custId = SGO_Helper_AES::decrypt( $this->_getParam('custId'), $passwordRand, $blocksize );
        $pass = ($authkeysEnc); //($zf_filter_input->password);
        // print_r($tokenAuth);die;
        if ($tokenAuth == '1') {
          $responseCode = $zf_filter_input->getEscaped('responseCode');
          if (!(empty($responseCode))) {
            $tokenIdUser = $this->_db->select()
              ->from(
                array('M_USER'),
                array('TOKEN_ID')
              )
              ->where('USER_ID = ?', $userId)
              ->limit(1);
            $tokenIdUser = $this->_db->fetchRow($tokenIdUser);
            $tokenIdUser = $tokenIdUser['TOKEN_ID'];

            if (!(empty($tokenIdUser))) {
              $cekToken = new Service_Token($custId, $userId, $tokenIdUser);
              $cekToken = $cekToken->verify($zf_filter_input->getEscaped('challengeCode'), $responseCode);

              if ($cekToken['ResponseCode'] == '00') {
                $Login     = new Login();
                $result     = $Login->authenticate($custId, $userId . '@HOST' . $_SERVER['REMOTE_ADDR'], $pass);

                $cekpass  = ($result['responseCode'] == '00') ? true : false;
                $errors    = ($result['responseCode'] == '00') ?   "" : $result['responseDesc'];
              } else {
                $CustUser = new CustomerUser($custId, $userId);
                $CustUser->setCustId($custId);
                $CustUser->setUserId($userId);

                // 								$CustUser->setFailedTokenMustLogout();
                $CustUser->setUserFailToken();
                $CustUser->setUserLock();
                $CustUser->setUserLogout();

                $cekpass = false;
                $errors = 'Token ' . $cekToken['ResponseDesc'];
              }
            } else {
              $cekpass = false;
              $errors = 'Token has not been enabled';
            }
          } else {
            $cekpass = false;
            $errors = 'Response token cannot be empty ';
          }
        } else {
          $tokenIdUser = $this->_db->select()
            ->from(
              array('M_USER'),
              array('USER_FAILEDATTEMPT')
            )
            ->where('USER_ID = ?', $userId)
            ->limit(1);
          $capcalock = $this->_db->fetchRow($tokenIdUser);
          if ($capcalock['USER_FAILEDATTEMPT'] == '3') {
            $this->view->reqcap = 1;
          }

          if ($capcalock['USER_FAILEDATTEMPT'] >= 3 && Application_Captcha::validateCaptcha($this->_request->getPost('captcha')) === true) {

            $Login     = new Login();

            // 						print_r($pass);die;
            $result     = $Login->authenticate($custId, $userId . '@HOST' . $_SERVER['REMOTE_ADDR'], $pass);

            $cekpass  = ($result['responseCode'] == '00') ? true : false;
            $errors    = ($result['responseCode'] == '00') ?   "" : $result['responseDesc'];
          } else if ($capcalock['USER_FAILEDATTEMPT'] < 2) {
            // die('re');

            $Login     = new Login();

            // 						print_r($pass);die;
            $result     = $Login->authenticate($custId, $userId . '@HOST' . $_SERVER['REMOTE_ADDR'], $pass);

            $cekpass  = ($result['responseCode'] == '00') ? true : false;
            $errors    = ($result['responseCode'] == '00') ?   "" : $result['responseDesc'];
          } else if ($capcalock['USER_FAILEDATTEMPT'] = 2) {
            // die;

            $Login     = new Login();

            // 						print_r($pass);die;
            $result     = $Login->authenticate($custId, $userId . '@HOST' . $_SERVER['REMOTE_ADDR'], $pass);

            $cekpass  = ($result['responseCode'] == '00') ? true : false;
            $errors    = ($result['responseCode'] == '00') ?   "" : $result['responseDesc'];
            $this->view->reqcap = 1;
          } else {
            $this->view->reqcap = 1;
            $errors = $this->language->_('Invalid Captcha');

            // $cekpass = true;
          }
        }

        if ($cekpass) {
          try {
            $this->_db->delete('TEMP_SESSION_USER', array(
              'CUST_ID = ?' => $custId,
              'USER_ID = ?' => $userId,
            ));
            $this->_db->insert('TEMP_SESSION_USER', array(
              'CUST_ID' => $custId,
              'USER_ID' => $userId,
              'USER_LASTACTIVITY' => new Zend_Db_expr('now()'),
              'TOKEN_WS' => $token,
            ));
          } catch (Exception $e) {
            Application_Helper_General::exceptionLog($e);
          }


          $frontendOptions = array(
            'lifetime' => 86400,
            'automatic_serialization' => true
          );
          $backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
          $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
          $cacheID = 'USERLIST';


          $userlist = $cache->load($cacheID);

          if (empty($userlist)) {
            $selectuser = $this->_db->select()
              ->from(
                'M_USER',
                array('*')
              );

            $userlist = $this->_db->fetchAll($selectuser);
            $listuser = array();
            foreach ($userlist as $keyuser => $valuser) {
              $listuser[$valuser['USER_ID']] = $valuser['USER_FULLNAME'];
            }
            $cache->save($listuser, $cacheID);
          }


          // echo "<pre>";
          // var_dump($userlist);die;

          $Settings = new Settings();
          $allSetting = $Settings->getAllSetting();

          $minDiff = $allSetting['timeoutafter'];

          $objSessionNamespace = new Zend_Session_Namespace('Zend_Auth');
          $timeExprSession = 600 + $minDiff * 60; //tambah 600 karena scheduler force logout 10 menit sekali
          $objSessionNamespace->setExpirationSeconds($timeExprSession);
          // set keterangan login ke auth
          $CustomerUser  = new CustomerUser(strtoupper($custId), strtoupper($userId));
          $listAccount   = $CustomerUser->getAccounts();
          $accountList   = Application_Helper_Array::simpleArray($listAccount, 'ACCT_NO');

          $listPrivilege = $CustomerUser->getPrivileges();
          $privilege = Application_Helper_Array::simpleArray($listPrivilege, 'FPRIVI_ID');

          $privilege[] = 'systemPrivilege';

          // set keterangan login ke auth
          Zend_Session::regenerateId();
          $auth = Zend_Auth::getInstance();
          $data = new stdClass();


          $select = $this->_db->select()
            ->from(
              'M_NOTIFICATION',
              array('*')
            );
          $select->where("DATE (EFDATE) >= DATE(NOW())");
          $select->where("TIME (EFDATE) <= TIME(NOW())");
          $select->where("DATE (EXP_DATE) >= DATE(NOW())");
          $select->where("TARGET IN ('1','3')");
          #$select->where("TIME (EXP_TIME) >= TIME(NOW())");
          // echo $select;die;
          $notif = $this->_db->fetchAll($select);

          $data->userIdLogin = $username;
          $data->userNameLogin = $login['BUSER_NAME'];
          $data->userBranch = $login['BRANCH_NAME'];
          $data->userBranchId = $login['ID'];
          $data->userBranchCode = $login['BRANCH_CODE'];
          $data->priviIdLogin = $priviId;
          $data->userGroupLogin = $groupArr;
          if (!empty($notif)) {
            $data->notifCount = count($notif);
            $data->notif = $notif;
          }
          //var_dump($login);die;
          // print_r($result);die;
          $data->isCommLinkage = false;
          $data->userIdLogin    = $userId;
          $data->userNameLogin  = ucwords(strtolower($result['userNameLogin']));
          $data->custNameLogin  = ucwords(strtolower($result['custNameLogin']));
          $data->priviIdLogin   = $privilege;
          $data->allSetting     = $allSetting;
          // $data->userGroupLogin = $groupArr;
          $data->custIdLogin    = strtoupper($custId);
          $data->tokenIdLogin    = $result['tokenId'];
          $data->accountList    = $accountList;
          $data->lastLogin = $result['lastLogin'];
          $data->custNameLogin = $result['custNameLogin'];
          $data->custSameUser = $result['custSameUser'];

          //theme
          $data->_userTheme = $dataComp['USER_THEME'];

          // cek userSessionHash before login
          $userSessionHash  = $this->_db->fetchrow(
            $this->_db->SELECT('SESSION_HASH')->FROM('M_USER')
              ->WHERE('USER_ID = ?', $userId)
              ->WHERE('CUST_ID =?', $custId)
          );
          //


          // ---------------------------------------- HASH -----------------------------------------------  //
          $hash = $this->hashKey($userId, $custId);
          $data->userHash     = $hash;

          try {
            $auth->getStorage()->write($data);
          } catch (Exception $e) {
            var_dump($e);
            die;
          }


          // End set keterangan login ke auth
          //$browser = substr($_SERVER['HTTP_USER_AGENT'], 0,24);
          $browser = $_SERVER['HTTP_USER_AGENT'];
          $sessionHash = md5($_SERVER['REMOTE_ADDR'] . $browser);

          //update USER_ISLOGIN
          $updateArr['USER_ISLOGIN'] = 1;
          $updateArr['USER_ISLOCKED'] = 0;
          $updateArr['USER_LASTLOGIN'] = new Zend_Db_Expr('now()');
          $updateArr['USER_LASTACTIVITY'] = new Zend_Db_Expr('now()');
          $updateArr['USER_FAILEDATTEMPT'] = 0;
          $updateArr['USER_ISNEW'] = 0;
          $updateArr['USER_HASH'] = $hash;
          $updateArr['SESSION_HASH'] = $sessionHash;
          $whereArr = array(
            'CUST_ID = ?' => (string) $custId,
            'USER_ID = ?' => (string) $userId
          );
          $customerupdate = $this->_db->update('M_USER', $updateArr, $whereArr);


          if ($userSessionHash['SESSION_HASH'] != $sessionHash) {

            //send email
            $EmailLoginNotif         = $allSetting['femailtemplate_loginnotif'];
            $templateEmailMasterBankName  = $allSetting['master_bank_name'];
            $templateEmailMasterBankAppName = $allSetting['master_bank_app_name'];
            $templateEmailMasterBankEmail  = $allSetting['master_bank_email'];
            $templateEmailMasterBankTelp  = $allSetting['master_bank_telp'];
            $templateEmailMasterBankWapp   = $allSetting['master_bank_wapp'];
            $url_fo   = $allSetting['url_fo'];
            $userData  = $this->_db->fetchrow(
              $this->_db->SELECT()->FROM('M_USER')
                ->WHERE('USER_ID = ?', $userId)
                ->WHERE('CUST_ID =?', $custId)
            );

            $lastlogin = Application_Helper_General::convertDate($userData['USER_LASTLOGIN'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

            // masking user id
            $lengthUserId = strlen($userId);
            $maskchar = substr($userId, 1, $lengthUserId - 3);
            $lengthMaskchar = strlen($maskchar);
            $mask = str_repeat("*", $lengthMaskchar);
            $userIdMask = str_replace($maskchar, $mask, $userId) . "<br>";
            // end masking						
            // echo "<pre>";
            $paramip = var_export(unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $_SERVER['REMOTE_ADDR'])));

            $data = array(
              '[[user_fullname]]' => $userData['USER_FULLNAME'],
              '[[user_login]]' => $userIdMask,
              '[[user_email]]' => $userData['USER_EMAIL'],
              '[[user_lastlogin]]' => $lastlogin,
              '[[user_agent]]' => $this->get_browser_name($_SERVER['HTTP_USER_AGENT']), //$browser,
              '[[comp_accid]]' => $userData['CUST_ID'],
              '[[ip_address]]' => $_SERVER['REMOTE_ADDR'],
              '[[app_url_link]]' => $url_fo . '/tools/forgotpassword/',
              '[[city]]' => $paramip['geoplugin_city'],
              '[[country]]' => $paramip['geoplugin_countryName'],
              '[[master_bank_name]]' => $templateEmailMasterBankName,
              '[[master_bank_app_name]]' => $templateEmailMasterBankAppName,
              '[[master_bank_email]]' => $templateEmailMasterBankEmail,
              '[[master_bank_telp]]'   => $templateEmailMasterBankTelp,
              '[[master_bank_wapp]]'   => $templateEmailMasterBankWapp
            );

            $EmailLoginNotif = strtr($EmailLoginNotif, $data);
            Application_Helper_Email::sendEmail($userData['USER_EMAIL'], 'Login Notification', $EmailLoginNotif);
            //end send email
          }


          //update latest transaction cache
          $this->latestTransaction($custId, $userId);
          // 	print_r($cekpass);
          // die;
          Application_Helper_General::writeLog('FLGN', 'Login Success ' . $user_ip = $this->getUserIP());
          $this->_redirect('/home/dashboard');
        } else {
          $confirmPage = 1;
        }
      } else {
        if ($captchaValid == false) {
          $errors = $this->language->_('Invalid Captcha');
        }
      }
    } // END REQUEST IS POST
    else {
      $randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
      $this->view->randomTransact = $randomTransact;
      $sessionNameRand->randomTransact = $randomTransact;
    }

    $this->view->challengeCode = $challengeCode;
    $this->view->confirmPage = $confirmPage;
    $this->view->tokenAuth = $tokenAuth;

    if (isset($errors)) {
      $this->view->errors = $errors;
    }

    //		$this->view->userId = (isset($zf_filter_input->userId)) ? $zf_filter_input->userId : $this->_getParam('userId');
    $this->view->userId = (isset($useridEnc)) ? $useridEnc : $useridEnc;

    //		$this->view->custId = (isset($zf_filter_input->custId)) ? $zf_filter_input->custId : $this->_getParam('custId');
    $this->view->custId = (isset($custidEnc)) ? $custidEnc : $custidEnc;
  }

  public function hashKey($userId, $custId)
  {
    do {
      $key = null;
      for ($i = 0; $i < 3; $i++)
        $key .= rand(0, 9);

      $browser = $_SERVER['HTTP_USER_AGENT'];
      $hash = md5($_SERVER['REMOTE_ADDR'] . $browser . $key);
      $checkHash = null;
      // CHECK HASH
      $checkHash = $this->_db->fetchOne(
        $this->_db->SELECT()
          ->FROM('M_USER', array('USER_HASH'))
          ->WHERE('USER_ID =?', $userId)
          ->WHERE('CUST_ID =?', $custId)
          ->WHERE('USER_HASH =?', $hash)
      );
    } while (!empty($checkHash));

    return $hash;
  }

  private function latestTransaction($custId, $userId)
  {
    $arrPayType   = array_combine($this->_paymenttype["code"], $this->_paymenttype["desc"]);

    $casePayType = "(CASE P.PS_TYPE ";
    foreach ($arrPayType as $key => $val) {
      $casePayType .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $casePayType .= " END)";

    $frontendOptions = array(
      'lifetime' => 900,
      'automatic_serialization' => true
    );
    $backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
    $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

    $cacheID = 'LT' . $custId . $userId;

    if (!$result = $cache->load($cacheID)) {
      $select = $this->_db->select()
        ->from(array('P' => 'T_PSLIP'), array())
        ->join(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
        ->joinLeft(array('M' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = M.PROVIDER_ID', array('P.PS_NUMBER', 'PAYMENT_TYPE' => $casePayType, 'T.BENEFICIARY_ACCOUNT', 'P.PS_CCY', 'P.PS_TOTAL_AMOUNT', 'M.PROVIDER_NAME'))
        ->where('P.CUST_ID = ?', $custId)
        ->where('P.PS_RELEASER_USER_LOGIN = ?', $userId)
        ->where('P.PS_STATUS = 5')
        ->where('T.TRA_STATUS = 3')
        ->order('P.PS_CREATED DESC')
        ->limit(5);

      $result = $this->_db->fetchAll($select);

      $cache->save($result, $cacheID);
    }
  }

  public function langAction()
  {
    $set = $this->_getParam('set');

    //// Start Multi Language
    $defaultlanguage = $set;
    $ns = new Zend_Session_Namespace('language');
    $sessionLanguage = $ns->langCode;

    if (!is_null($sessionLanguage)) {
      $setlang = $sessionLanguage;
    } else {
      $setlang = $locale->getLanguage();
    }
    if (!empty($setlang) && isset($setlang)) {
      $defaultlanguage = $setlang;
      $ns->langCode = $defaultlanguage;
    }
    Zend_Loader::loadClass('Zend_Translate');

    $frontendOptions = array(
      'cache_id_prefix' => 'MyLanguage',
      'lifetime' => 86400,
      'automatic_serialization' => true
    );
    $backendOptions = array('cache_dir' => APPLICATION_PATH . '/../../library/data/cache/language/frontend');
    $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
    Zend_Translate::setCache($cache);

    $translate = new Zend_Translate('tmx', APPLICATION_PATH . '/../../library/data/languages/frontend/language.tmx', $defaultlanguage);

    Zend_Registry::set('language', $translate);
    //// End Multi Language

    $lang = $set;
    $ns->langCode = $lang;
  }

  public function logoutAction()
  {
    Application_Helper_General::writeLog('FLGT', 'Logout From System ' . $user_ip = $this->getUserIP());
    if (Zend_Auth::getInstance()->hasIdentity()) {
      $auth = Zend_Auth::getInstance()->getIdentity();
      $this->_userIdLogin   = $auth->userIdLogin;
      $this->_custIdLogin   = $auth->custIdLogin;
      Zend_Auth::getInstance()->clearIdentity();
      $CustomerUser  = new CustomerUser(strtoupper($this->_custIdLogin), strtoupper($this->_userIdLogin));
      $CustomerUser->forceLogout();

      $forceBySession = $this->_request->getParam('session');

      if ($forceBySession == '1') {
        $this->_redirect('/authorizationacl/index/disableuser');
      }
    }
    $this->_redirect('/');

    //		$this->_redirector->gotoUrl('login/');
  }


  public function notifrefreshAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();
    $select = $this->_db->select()
      ->from(
        'M_NOTIFICATION',
        array('*')
      );
    $select->where("DATE (EFDATE) >= DATE(NOW())");
    $select->where("TIME (EFDATE) <= TIME(NOW())");
    $select->where("DATE (EXP_DATE) >= DATE(NOW())");
    $select->where("TARGET IN ('1','3')");
    #$select->where("TIME (EXP_TIME) >= TIME(NOW())");
    // echo $select;die;
    $notif = $this->_db->fetchAll($select);
    if (!empty($notif)) {
      $notifCount = count($notif);
      $notif = $notif;
    }
    $string = '';
    foreach ($notif as $key => $value) {
      $val = $value['SUBJECT'];
      $string  .= '<a class="dropdown-item" href="#">' . $val . '</a><div class="dropdown-divider"></div>';
    }
    echo $string;
  }


  public function updatenotifAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');

    $insertArr = array(
      'NOTIF_ID'      => $tblName,
      'USER_ID'       => $this->_userIdLogin
    );
    $this->_db->insert('M_NOTICEPOPUP', $insertArr);
  }





  public function notifpopAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $select = $this->_db->select()
      ->from(
        array('L' =>  'M_NOTIFICATION'),
        array('*')
      );
    // $select1->joinleft(array('T' => 'M_NOTICEPOPUP'), 'T.NOTIF_ID = L.ID', array('T.USER_ID'));
    $select->where("DATE (EFDATE) >= DATE(NOW())");
    $select->where("TIME (EFDATE) <= TIME(NOW())");
    $select->where("DATE (EXP_DATE) >= DATE(NOW())");
    $select->where("TARGET IN ('1','3')");
    // $select->where("USER_ID = ? ", $this->_userIdLogin);
    // echo $select;die;
    #$select->where("TIME (EXP_TIME) >= TIME(NOW())");
    // echo $select;die;
    $notif = $this->_db->fetchAll($select);
    if (!empty($notif)) {
      // $data = array();
      foreach ($notif as $key => $value) {
        $selectdata = $this->_db->select()
          ->from(
            array('A' =>  'M_NOTICEPOPUP'),
            array('*')
          );
        $selectdata->where("NOTIF_ID = ?", $value['ID']);
        $selectdata->where("USER_ID = ?", $this->_userIdLogin);
        $data = $this->_db->fetchAll($selectdata);
        if (!empty($data)) {
          // echo 'here';
          unset($notif[$key]);
        }
        unset($data);
      }
    }

    if (!empty($notif)) {
      $notifCount = count($notif);
      $notif = $notif;
    }
    echo json_encode($notif);
    // $string = '';
    // foreach ($notif as $key => $value) {
    // 	$val = $value['SUBJECT'];
    // 	$string  .= '<a class="dropdown-item" href="#">'.$val.'</a><div class="dropdown-divider"></div>';
    // }
    // ?echo $string;

  }

  public function verifyldapAction()
  {

    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $username = $this->_request->getPost('username');
    $password = $this->_request->getPost('password');

    $ldap = ldap_connect("btn.co.id");
    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

    if ($ldap) {
      $userldap = $username . "@btn.co.id";
      // $userldap = $username;
      // $username = explode("@", $userldap)[0];
      // var_dump($userldap);

      $bind = @ldap_bind($ldap, $userldap, $password);
      if ($bind) {
        $filter = '(&(objectCategory=person)(sAMAccountName=' . strtoupper($username) . '))';
        $dn = "DC=btn,DC=co,DC=id";

        $res = ldap_search($ldap, $dn, $filter);
        $first = ldap_first_entry($ldap, $res);
        if ($first) {
          $data = ldap_get_dn($ldap, $first);
          $race = ldap_get_entries($ldap, $res);

          $response = array();
          $response['fullName'] = $race[0]['cn'][0];
          $response['name_front'] = $race[0]['givenname'][0];
          $response['userAD'] = $race[0]['samaccountname'][0];
          $response['nip'] = $race[0]['initials'][0];
          $response['email'] = $race[0]['mail'][0];
          $response['status'] = '1';
          echo json_encode($response);
        } else {
          echo json_encode(array('status' => '0'));
        }
      } else {
        echo json_encode(array('status' => '0'));
      }
    } else {
      echo json_encode(array('status' => '0'));
    }
  }
}
