<?php

class Emailconf_IndexController extends Application_Main {
	
	protected $_moduleDB  = 'WLT';
	
	public function indexAction() {
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->msg = $msg;
     		}	
    	}
		$data_settingid = "'tmpl_new_cust_act_print','tmpl_new_cust_act_email','tmpl_cust_user_pass_print','tmpl_cust_user_pass_email',
							'tmpl_backend_usr_act_print','tmpl_backend_usr_act_email','tmpl_backend_user_reset_pass_email','tmpl_backend_user_reset_pass_print'";				
		$suggestedText = $this->_db->fetchAll(
							$this->_db->select()->from (
							 	array ('M_SETTING'), 
							 	array ('SETTING_VALUE'))
							 	->where('LOWER(SETTING_ID) IN ('.$data_settingid.')')
							);
		
		$isPendingAvailable = $this->_db->fetchOne(
									$this->_db->select()->from (
									 	array ('TEMP_SETTING'), 
									 	array ('SETTING_VALUE'))
									 	->where('LOWER(SETTING_ID) = ?','tmpl_new_cust_act_print')
							 );
							 
		$this->view->disableFlag = ($isPendingAvailable==true) ? true :  false;
		
		
		for($i=0;$i<8;$i++)
		{
			if(!empty($suggestedText[$i]["SETTING_VALUE"]))
			{
				$suggesttext[$i] = $suggestedText[$i]["SETTING_VALUE"];
			}else{
				$suggesttext[$i] = " ";
			}
		
		}
		$this->view->VarSuggestedText = $suggesttext;
		//$suggestedText1 =  	$suggestedText;
		//Zend_Debug::dump($suggestedText1);
		//echo $suggestedText1[0]["SETTING_VALUE"];
		//insert log
		try {
	  		$this->_db->beginTransaction();
	  		$this->backendLog('V',$this->_moduleDB,$this->_moduleDB,null,null);
	  		$this->_db->commit();
		}
    	catch(Exception $e){
 	  		$this->_db->rollBack();
	  		SGO_Helper_GeneralLog::technicalLog($e);
		}
	}		
}

?>