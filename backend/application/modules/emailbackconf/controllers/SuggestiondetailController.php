<?php
require_once 'Zend/Controller/Action.php';

class Welcome_SuggestiondetailController extends Application_Main {
	protected $_changesID;
	protected $_changesInfo;
	protected $_ccyArr=array();
	protected $_groupStatusCode  = array();
	protected $_moduleDB  = 'WLT';
	
	public function initVar(){
		$this->_groupStatus =  $this->_masterStatus;
		$this->view->groupStatus = $this->_masterStatus;
		$this->_helper->layout()->setLayout('popup');
	}
	public function indexAction() {
		$this->_helper->viewRenderer->setNoRender();
		
		$filters = array('changes_id' => array('StringTrim'));
							 
		$validators =  array(
			'changes_id' => array('NotEmpty','Digits', //group code may not be empty
									array('Db_RecordExists',array('table'=>'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID')),
									array('Db_RecordExists',array('table'=>'TEMP_SETTING', 'field' => 'CHANGES_ID')),
								   'messages' => array(	$this->getErrorRemark('01','Suggestion ID'), 
								   						$this->getErrorRemark('04','Suggestion ID'),
								   						$this->getErrorRemark('22','Suggestion ID'),
								   						$this->getErrorRemark('22','Suggestion ID'),
								   					   ) 
								   ),
			);
					
		if (array_key_exists('changes_id',$this->_request->getParams())){

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
		
			if($zf_filter_input->isValid()) {
				
				$this->_changesID  = ($zf_filter_input->changes_id)? $zf_filter_input->changes_id : $this->_request->getParam('changes_id');
			
        		$this->_changesInfo = 
							$this->_db->fetchRow(
								$this->_db->select()
												->from(array('G' => 'T_GLOBAL_CHANGES'), '*')
												->where("G.CHANGES_ID=?", $this->_changesID)
							);
							
//				if($this->_changesInfo['CHANGES_TYPE']==$this->_changeType['code']['new']){
//					$this->_redirect('/'.$this->_request->getModuleName()."/suggestiondetail/detailnew/changes_id/$this->_changesID");
				if($this->_changesInfo['CHANGES_TYPE']==$this->_changeType['code']['edit']){
					$this->_redirect('/'.$this->_request->getModuleName()."/suggestiondetail/detailedit/changes_id/$this->_changesID");
//				}elseif($this->_changesInfo['CHANGES_TYPE']==$this->_changeType['code']['activate']){
//					$this->_redirect('/'.$this->_request->getModuleName()."/suggestiondetail/detailactivate/changes_id/$this->_changesID");
//				}elseif($this->_changesInfo['CHANGES_TYPE']==$this->_changeType['code']['deactivate']){
//					$this->_redirect('/'.$this->_request->getModuleName()."/suggestiondetail/detaildeactivate/changes_id/$this->_changesID");
				}else{
					$errorMsg = $this->getErrorRemark('22','Suggestion ID');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
					$this->_redirect('/'.$this->_request->getModuleName().'/suggestiondetail/failed');
				}
			}
			else{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error){
					foreach($error as $key2 => $errorMsg){
						$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
					}					
				}	
				$this->_redirect('/'.$this->_request->getModuleName().'/suggestiondetail/failed');
			}
		}
		else{
			$errorMsg = $this->getErrorRemark('01','Suggestion ID');
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
			$this->_redirect('/'.$this->_request->getModuleName().'/suggestiondetail/failed');
		}
	}
	
	public function detailnewAction(){
	}
	
	public function detaileditAction(){
		//$this->_helper->layout()->setLayout('popup');
		$filters = array('changes_id' => array('StringTrim'));
							 
		$validators =  array(
					'changes_id' => array(
										'NotEmpty', //group code may not be empty
										'Digits', //group code may not be empty
										array('Db_RecordExists',array('table'=>'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID')),
								   		array('Db_RecordExists', array('table' => 'TEMP_SETTING', 'field' => 'CHANGES_ID')),
								   		'messages' => array(
								   							$this->getErrorRemark('01','Suggestion ID'), 
								   							$this->getErrorRemark('04','Suggestion ID'),
								   					   		$this->getErrorRemark('22','Suggestion ID'),
								   					   		$this->getErrorRemark('22','Suggestion ID'),
								   					   )
								   ),
						);
					
		if (array_key_exists('changes_id',$this->_request->getParams())){
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid()) {
				
				$this->_changesID  = ($zf_filter_input->changes_id)? $zf_filter_input->changes_id : $this->_request->getParam('changes_id');
		
				$this->_changesInfo = 
							$this->_db->fetchRow(
								$this->_db->select()
										->from('T_GLOBAL_CHANGES')
										->where("CHANGES_TYPE='E'")
										->where("CHANGES_FLAG='B'")
                             			->where('CHANGES_ID=?', $this->_changesID)
										->where("KEY_FIELD='MODULE_ID'")
										->where("KEY_VALUE='WLT'")
							);
				if(!count($this->_changesInfo)){
					$errorMsg = $this->getErrorRemark('22','Suggestion ID');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
					$this->_redirect('/'.$this->_request->getModuleName().'/suggestiondetail/failed');
				}
				
			$db = Zend_Db_Table::getDefaultAdapter();
        
			$this->view->inputParam = $this->getRequest()->getParams(); 
			        $select = $this->_db->select()
					             ->from(array('TEMP_SETTING'))
								->where("CHANGES_ID = ?",$this->_changesID)
								->where("MODULE_ID = ?", 'WLT')
                    			->query()->fetchAll();
                    			
						$this->view->suggestedText  = $select[0]['SETTING_VALUE'];
								
				//insert log
				try {
	  				$this->_db->beginTransaction();
	  				$fulldesc = 'CHANGES_ID:'.$this->_changesID;
	  				$this->backendLog('V',$this->_moduleDB,$this->_moduleDB,$fulldesc,null);
	  				$this->_db->commit();
				}
    			catch(Exception $e){
 	  				$this->_db->rollBack();
	  				SGO_Helper_GeneralLog::technicalLog($e);
				}	
			}else{
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')	->addMessage('F');
				foreach($errors as $key => $error){
					foreach($error as $key2 => $errorMsg)
						$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
				}	
				$this->_redirect('/'.$this->_request->getModuleName().'/suggestiondetail/failed');
			}
		}else{
			$errorMsg = $this->getErrorRemark('01','Suggestion ID');
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage("$errorMsg");
			$this->_redirect('/'.$this->_request->getModuleName().'/suggestiondetail/failed');
		}
	}
	
	public function detailactivateAction(){
	}
	
	public function detaildeactivateAction(){
	}
	
	public function failedAction(){
		//$this->_redirect($this->_backURL);
		$this->_redirect('/popuperror/index/index');
	}
}