<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';

require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_requestmddetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');
		$this->view->systemType = $system_type;

		$stamp_fee = $settings->getSetting('stamp_fee');
		$this->view->stamp_fee = $stamp_fee;
		$adm_fee = $settings->getSetting('adm_fee');
		$this->view->adm_fee = $adm_fee;

		// $Settings = new Settings();
		// $claim_period = $Settings->getSetting('max_claim_period');
		// $this->view->BG_CLAIM_PERIOD = $claim_period;

		$this->view->systemType = $system_type;
		$this->view->ProvFee = 200000;

		$conf = Zend_Registry::get('config');
		$this->_bankName = $conf['app']['bankname'];
		$this->view->masterbankname = $this->_bankName;

		$toc_ind = $settings->getSetting('ftemplate_bg_ind');
		$this->view->toc_ind = $toc_ind;
		$toc_eng = $settings->getSetting('ftemplate_bg_eng');
		$this->view->toc_eng = $toc_eng;

		$this->view->googleauth = true;

		$numb = $this->_getParam('bgnumb');

		// decrypt numb
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token     = $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));
		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
		$numb = $BG_NUMBER;

		$checkOthersAttachment = $this->_db->select()
			->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
			->where("BG_REG_NUMBER = '$numb'")
			->order('A.INDEX ASC')
			->query()->fetchAll();

		if (count($checkOthersAttachment) > 0) {
			$this->view->othersAttachment = $checkOthersAttachment;
		}

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*', 'CUST_ID_PEMOHON' => 'A.CUST_ID', 'CUST_EMAIL_PRINCIPAL' => 'A.CUST_EMAIL'))
				->joinRight(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID')
				->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->where('A.BG_STATUS IN (21, 24)')
				->query()->fetchAll();

			if (!empty($bgdata[0]['BG_OLD'])) {
				$tbgdata = $this->_db->select()
					->from(["A" => "T_BANK_GUARANTEE"], ["*"])
					->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
					->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
					->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
					->query()->fetch();

				$this->view->tbgdata = $tbgdata;

				// Marginal Deposit Eksisting ---------------
				$bgdatamdeks = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					// ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
					// ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
					->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : $numb)
					->query()->fetchAll();

				$this->view->bgdatamdeks = $bgdatamdeks;
				// Marginal Deposit Eksisting ---------------

				// Top Up Marginal Deposit ---------------
				$bgdatamd = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
					// ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
					// ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
					->where('A.BG_REG_NUMBER = ?', $numb)
					->query()->fetchAll();

				$this->view->bgdatamd = $bgdatamd;
			}

			if ($bgdata[0]['BG_OLD']) {
				$bgOld = $this->_db->select()
					->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
					->where('BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
					->query()->fetch();

				$this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
				$this->view->prevProv = $bgOld['PROVISION_FEE'];
			}

			$bgdatadetail = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$this->view->bgdatadetail = array_combine(array_map('strtolower', array_column($bgdatadetail, 'PS_FIELDNAME')), array_column($bgdatadetail, 'PS_FIELDVALUE'));

			if (!empty($bgdata)) {

				$data = $bgdata['0'];
				$this->view->data = $data;

				// Guaranted Transaction -----------------------

				$getGuarantedTransanctions = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_UNDERLYING')
					->where('BG_REG_NUMBER = ?', $numb)
					->query()->fetchAll();

				$this->view->guarantedTransanctions = $getGuarantedTransanctions;

				if (!empty($tbgdata['BG_REG_NUMBER'])) {
					$checkOthersAttachmentT = $this->_db->select()
						->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
						->where("BG_REG_NUMBER = ?", $tbgdata['BG_REG_NUMBER'])
						->order('A.INDEX ASC')
						->query()->fetchAll();

					$this->view->othersAttachmentT = $checkOthersAttachmentT;

					$getGuarantedTransanctionsT = $this->_db->select()
						->from('T_BANK_GUARANTEE_UNDERLYING')
						->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
						->query()->fetchAll();

					$this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;

					$bgdatadetailT = $this->_db->select()
						->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
						->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
						->query()->fetchAll();

					$this->view->bgdatadetailT = array_combine(array_map('strtolower', array_column($bgdatadetailT, 'PS_FIELDNAME')), array_column($bgdatadetailT, 'PS_FIELDVALUE'));
				}

				$bgdatamd = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
					// ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER', ['B.ACCT', 'B.NAME', 'B.AMOUNT'])
					->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER', ['B.ACCT', 'B.NAME', 'B.AMOUNT', 'IS_TRANSFER'])
					->where('A.BG_REG_NUMBER = ?', $numb)
					->where('B.IS_TRANSFER = ?', '1')
					->query()->fetchAll();

				$this->view->bgdatamd = $bgdatamd;

				// check apakah ada data dokumen awal di TEMP
				$checkPremDoc = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_PRELIMINARY')
					->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
					->query()->fetch();

				$needRepair = false;

				$this->view->needRepair = $needRepair;
				// end check

				switch ($bgdata['0']["CHANGE_TYPE"]) {
					case '0':
						$this->view->suggestion_type = "Pengajuan Baru";
						break;
					case '1':
						$this->view->suggestion_type = "Amendment Isi";
						break;
					case '2':
						$this->view->suggestion_type = "Amendment Format";
						break;
				}

				$this->view->bankFormatNumber = $data['BG_FORMAT'];

				$selectcomp = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
					->join(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID')
					->join(array('C' => 'M_CITYLIST'), 'B.CUST_CITY = C.CITY_CODE', ['CITY_NAME' => 'C.CITY_NAME'])
					->where('A.CUST_ID =' .  $this->_db->quote((string)$this->_custIdLogin))
					->query()->fetchAll();

				$this->view->compinfo = $selectcomp[0];

				$policyBoundary = $this->findPolicyBoundary(38, $bgdata['0']['BG_AMOUNT']);

				$this->view->policyBoundary = $policyBoundary;

				$approverUserList = $this->findUserBoundary(38, $bgdata['0']['BG_AMOUNT']);

				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->BG_PUBLISH = $arrbgpublish[$data['BG_PUBLISH']];

				if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];
					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();

					$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
					$this->view->insuranceAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];
				}

				if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
					$bgdatasplit = $this->_db->select()
						->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
						->where('A.BG_NUMBER = ?', $numb)
						->query()->fetchAll();

					$this->view->fullmember = $bgdatasplit;
				}

				$selectHistory	= $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY')
					->where("BG_REG_NUMBER = ?", $BG_NUMBER);

				$history = $this->_db->fetchAll($selectHistory);

				$cust_approver = 1;
				$bg_submission_hisotrys = $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY')
					->where("BG_REG_NUMBER = ?", $BG_NUMBER)
					->where("CUST_ID IN (?) ", [$selectcomp[0]["CUST_ID"], $bgdata[0]["BG_INSURANCE_CODE"]]);
				$bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

				foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {
					// maker
					if ($bg_submission_hisotry['HISTORY_STATUS'] == 1) {
						$makerStatus = 'active';
						$makerIcon = '<i class="fas fa-check"></i>';
						$custlogin = $bg_submission_hisotry['USER_LOGIN'];
						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);
						$custFullname = $customer[0]['USER_ID'];
						$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));
						$align = 'align="center"';

						$this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
					}

					// approver
					if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
						$approverStatus = 'active';
						$approverIcon = '<i class="fas fa-check"></i>';
						$custlogin = $bg_submission_hisotry['USER_LOGIN'];
						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);
						$custFullname = $customer[0]['USER_ID'];
						$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));
						$align = 'align="center"';

						$this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

						$this->view->approverStatus = $approverStatus;
					}

					if ($bg_submission_hisotry['HISTORY_STATUS'] == 5 || $bg_submission_hisotry['HISTORY_STATUS'] == 10) {
						$releaserStatus = 'active';
						$releaserIcon = '<i class="fas fa-check"></i>';
						$custlogin = $bg_submission_hisotry['USER_LOGIN'];
						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);
						$custFullname = $customer[0]['USER_ID'];
						$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));
						$align = 'align="center"';

						$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
						$this->view->releaserStatus = $releaserStatus;
					}
				}

				foreach ($history as $row) {
					if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
						if ($row['HISTORY_STATUS'] == 8) {
							$makerStatus = 'active';
							$insuranceIcon = '<i class="fas fa-check"></i>';
							$insuranceStatus = 'active';
							$reviewStatus = '';
							$makerOngoing = '';
							$reviewerOngoing = '';
							$approverOngoing = '';
							$custlogin = $row['USER_LOGIN'];
							$selectCust    = $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin);

							$customer = $this->_db->fetchAll($selectCust);
							$custFullname = $customer[0]['USER_ID'];
							$insuranceApprovedBy = $custFullname;
							$align = 'align="center"';
							$marginLeft = '';
							if ($cust_reviewer == 0 && $cust_approver == 0) {
								$align = '';
								$marginLeft = 'style="margin-left: 15px;"';
							}
							$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
							$this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
						}
					}

					if ($row['HISTORY_STATUS'] == 15) {
						$verifyStatus = 'active';
						$verifyIcon = '<i class="fas fa-check"></i>';
						$verifyOngoing = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
							$reviewerOngoing = '';
							$verifyOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						} else {
							$reviewerOngoing = '';
							$verifyOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						}
						$custlogin = $row['USER_LOGIN'];
						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);
						$custFullname = $customer[0]['USER_FULLNAME'];
						$custEmail 	  = $customer[0]['USER_EMAIL'];
						$custPhone	  = $customer[0]['USER_PHONE'];
						$verifyApprovedBy = $custFullname;
						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						$align = 'align="center"';
						$marginRight = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginRight = 'style="margin-right: 15px;"';
						}

						$this->view->verifyApprovedBy = '';
						// $this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
					}

					//if approver done
					if ($row['HISTORY_STATUS'] == 16) {
						$makerStatus = 'active';
						$approveStatus = '';
						$reviewStatus = '';
						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = 'ongoing';
						$releaserOngoing = '';
						$custlogin = $row['USER_LOGIN'];
						$selectuserapp	= $this->_db->select()
							->from(array('C' => 'T_APPROVAL'), array(
								'*'
							))
							->where("C.PS_NUMBER = ?", $BG_NUMBER)
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin);

						$userapprove = $this->_db->fetchAll($selectuserapp);
						//tampung data user yang sudah approve
						if (!empty($userapprove) && empty($userid)) {
							foreach ($userapprove as $vl) {
								$userid[] = $vl['USER_ID'];
							}
						}
						$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
					}

					//if releaser done
					if ($row['HISTORY_STATUS'] == 5) {
						$makerStatus = 'active';
						$approveStatus = '';
						$reviewStatus = '';
						$releaseStatus = '';
						$releaseIcon = '';
						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						$releaserOngoing = '';
						$custlogin = $row['USER_LOGIN'];
						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);
						$custFullname = $customer[0]['USER_FULLNAME'];
						$releaserApprovedBy = $custFullname;
						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}
						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
					}
				}

				$REPAIR_NOTE = $this->_db->select()
					->from(
						array('D' => 'T_BANK_GUARANTEE_HISTORY'),
						array(
							'BG_REASON'      => 'D.BG_REASON'
						)
					)
					->where('D.HISTORY_STATUS IN (20, 22)')
					->where("D.BG_REG_NUMBER = ?", $numb)
					->limit(1)
					->order('D.DATE_TIME DESC')
					->query()->fetchAll();

				$this->view->reasonhistory = $REPAIR_NOTE[0]["BG_REASON"];

				//approvernamecircle jika sudah ada yang approve
				if (!empty($userid)) {

					$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

					$flipAlphabet = array_flip($alphabet);

					$approvedNameList = array();
					$i = 0;
					//var_dump($userid);die;
					foreach ($userid as $key => $value) {

						//select utk nama dan email
						$selectusername = $this->_db->select()
							->from(array('M_USER'), array(
								'*'
							))
							->where("USER_ID = ?", (string) $value)
							->where("CUST_ID = ?", (string) $this->_custIdLogin);

						$username = $this->_db->fetchAll($selectusername);

						//select utk cek user berada di grup apa
						$selectusergroup	= $this->_db->select()
							->from(array('C' => 'M_APP_GROUP_USER'), array(
								'*'
							))
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							->where("C.USER_ID 	= ?", (string) $value);

						$usergroup = $this->_db->fetchAll($selectusergroup);

						$groupuserid = $usergroup[0]['GROUP_USER_ID'];
						$groupusername = $usergroup[0]['USER_ID'];
						$groupuseridexplode = explode("_", $groupuserid);

						if ($groupuseridexplode[0] == "S") {
							$usergroupid = "SG";
						} else {
							$usergroupid = $alphabet[$groupuseridexplode[2]];
						}

						array_push($approvedNameList, $username[0]['USER_FULLNAME']);

						$efdate = $approveEfDate[$i];

						$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
						$i++;
					}
					//var_dump($approverApprovedBy);die;
					$this->view->approverApprovedBy = $approverApprovedBy;

					//kalau sudah approve semua
					if (!$checkBoundary) {
						$approveStatus = '';
						$approverOngoing = '';
						$approveIcon = '';
						$releaserOngoing = 'ongoing';
					}
				}

				$selectsuperuser = $this->_db->select()
					->from(array('C' => 'T_APPROVAL'))
					->where("C.PS_NUMBER 	= ?", $numb)
					->where("C.GROUP 	= 'SG'");

				$superuser = $this->_db->fetchAll($selectsuperuser);
				if (!empty($superuser)) {
					$userid = $superuser[0]['USER_ID'];
					//select utk nama dan email
					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("USER_ID = ?", (string) $userid)
						->where("CUST_ID = ?", (string) $this->_custIdLogin);

					$username = $this->_db->fetchAll($selectusername);

					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

					$approveStatus = 'active';
					$approverOngoing = '';
					$approveIcon = '<i class="fas fa-check"></i>';
					$releaserOngoing = 'ongoing';
				}

				//define circle
				$makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '</button>';
				foreach ($reviewerList as $key => $value) {
					$textColor = '';
					if ($value == $reviewerApprovedBy) {
						$textColor = 'text-white-50';
					}
					$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				$reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '</button>';
				$groupNameList = $approverUserList['GROUP_NAME'];
				unset($approverUserList['GROUP_NAME']);
				if ($approverUserList != '') {
					foreach ($approverUserList as $key => $value) {
						$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
						$i = 1;
						foreach ($value as $key2 => $value2) {
							$textColor = '';
							if (in_array($value2, $approvedNameList)) {
								$textColor = 'text-white-50';
							}
							if ($i == count($value)) {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
							} else {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
							}
							$i++;
						}
					}
				} else {
					$approverListdata = 'There is no Approver User';
				}

				$spandata = '';
				if (!empty($approverListdata) && !$error_msg2) {
					$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
				}
				$approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '
				' . $spandata . '
				</button>';

				foreach ($releaserList as $key => $value) {
					$textColor = '';
					if ($value == $releaserApprovedBy) {
						$textColor = 'text-white-50';
					}
					$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				$releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span> </button>';

				$this->view->policyBoundary = $policyBoundary;
				$this->view->makerNameCircle = $makerNameCircle;
				$this->view->reviewerNameCircle = $reviewerNameCircle;
				$this->view->approverNameCircle = $approverNameCircle;
				$this->view->releaserNameCircle = $releaserNameCircle;
				$this->view->makerStatus = $makerStatus;
				$this->view->approveStatus = $approveStatus;
				$this->view->reviewStatus = $reviewStatus;
				$this->view->releaseStatus = $releaseStatus;

				// status bg -------------------------------------------------------------
				$bgType 		= $conf["bg"]["type"]["desc"];
				$bgCode 		= $conf["bg"]["type"]["code"];
				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));
				$this->view->arrbgType = $arrbgType;
				// status bg -------------------------------------------------------------

				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();

					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				$config    		= Zend_Registry::get('config');
				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

				$this->view->arrStatus = $arrStatus;

				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

				$param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
				$AccArr = $CustomerUser->getAccounts($param);
				//var_dump($AccArr);die;

				if (!empty($AccArr)) {
					$this->view->src_name = $AccArr['0']['ACCT_NAME'];
				}

				$arrBankFormat = array(
					1 => 'Bank Standard',
					2 => 'Special Format (with bank approval)'
				);

				$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

				$arrLang = array(
					1 => 'Indonesian',
					2 => 'English',
					3 => 'Billingual',
				);

				$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

				$arrWaranty = array(
					1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
					2 => 'Bank Guarantee Line Facility',
					3 => 'Insurance'

				);
				$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

				//  select account type 1 -------------------------------------------------------
				$type = array('D', 'T', '20', '30', 'S', '10');
				$paramA = array('ACCT_TYPE' => $type);
				$AccArrA = $CustomerUser->getAccountsBG($paramA);
				$newArr2 = [];

				$getAllProduct = $this->_db->select()
					->from("M_PRODUCT")
					->query()->fetchAll();

				foreach ($AccArrA as $keyArr => $arrAcct) {
					$accountTypeCheck = false;
					$productTypeCheck = false;
					$svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
					$result = $svcAccount->inquiryAccountBalance();

					if ($result['response_code'] == '0000') {
						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					} else {
						$result = $svcAccount->inquiryDeposito();
						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					}

					foreach ($getAllProduct as $key => $value) {
						if ($value['PRODUCT_CODE'] == $accountType) {
							$accountTypeCheck = true;
						};
						if ($value['PRODUCT_PLAN'] == $productType) {
							$productTypeCheck = true;
						};
					}
					if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
				}
				$this->view->AccArr = $newArr2;
				//  select account -------------------------------------------------------

				// preliminary Member ---------------------------------------------------
				$selectMP = $this->_db->select()
					->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
				$selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
				$preliminaryMemberArr = $this->_db->fetchAll($selectMP);
				if (empty($preliminaryMemberArr)) {
					$this->view->disableChecklist = true;
				}
				$this->view->preliminaryMemberArr = $preliminaryMemberArr;
				// preliminary Member ---------------------------------------------------

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];

				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->contact_person = $data['RECIPIENT_CP'];
				$this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
				$this->view->contact_email = $data['RECIPIENT_EMAIL'];

				$this->view->fileName = $data['BG_UNDERLYING_DOC'];
				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];

				$get_name = new Service_Account($data['FEE_CHARGE_TO'], "IDR");
				$get_name = $get_name->inquiryAccontInfo();
				$this->view->chargeto_name = $get_name["account_name"];

				$this->view->status = $data['BG_STATUS'];

				$Settings = new Settings();
				$claim_period = $Settings->getSetting('claim_period');
				$this->view->claim_period = $claim_period;

				$this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
				$bgdocType 		= $conf["bgdoc"]["type"]["desc"];
				$bgdocCode 		= $conf["bgdoc"]["type"]["code"];

				$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

				$this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];
				$this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];
				$this->view->usage_purpose = $data['USAGE_PURPOSE'];
				$this->view->comment = $data['SERVICE'];

				if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || $data['BG_STATUS'] == '24' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$selectQuery  = "SELECT
					a.USER_LOGIN,
					b.`USER_FULLNAME` AS u_name,
					c.`BUSER_NAME` AS b_name,
					a.DATE_TIME,
					a.BG_REASON,
					a.HISTORY_STATUS,
					a.BG_REG_NUMBER
					FROM
					T_BANK_GUARANTEE_HISTORY AS a
					LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
					LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
					WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
					$result =  $this->_db->fetchAll($selectQuery);
					if (!empty($result)) {
						$data['REASON'] = $result[count($result) - 1]['BG_REASON'];
					}
					$this->view->reqrepair = true;
					$this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
				}

				if (!empty($bgdatadetail)) {
					$save_bg_data_detail = [];
					foreach ($bgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}

						$save_bg_data_detail[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
					}

					$this->view->bg_data_detail = $save_bg_data_detail;
				}

				$conf = Zend_Registry::get('config');
				$this->view->bankname = $conf['app']['bankname'];

				$download = $this->_getParam('download');
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				// data modal Persetujuan Asuransi -----------------------
				$getBgDataDetail = $this->_db->select()
					->from("TEMP_BANK_GUARANTEE_DETAIL")
					->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
					->query()->fetchAll();

				if (!empty($getBgDataDetail)) {
					foreach ($getBgDataDetail as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Principle Agreement Number') {
							$this->view->principleAgreementNumber =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Principle Agreement Granted Date') {
							$this->view->principleAgreementDate =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Principle Insurance Premium') {
							$this->view->principleInsurancePremium =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Principle Insurance Administration') {
							$this->view->principleInsuranceAdm =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Principle Insurance Stamp') {
							$this->view->principleInsuranceStamp =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Counter Guarantee Number') {
							$this->view->counterGuaranteeNumber =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Counter Guarantee Document') {
							$this->view->counterGuaranteeDocument =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Counter Guarantee Granted Date') {
							$this->view->counterGuaranteeGrantedDate =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
							$ins_branch_code =   $value['PS_FIELDVALUE'];
							$sqlinsurancebranch = $this->_db->select()
								->from("M_INS_BRANCH", ["INS_BRANCH_ACCT"])
								->where("INS_BRANCH_CODE = ?", $ins_branch_code);
							$get_norekinsurance = ($sqlinsurancebranch)
								->query()->fetchAll();
							$this->view->norekCabang =   $get_norekinsurance[0]['INS_BRANCH_ACCT'];
						}
					}
				}

				$get_cg_doc_number = $this->_db->select()
					->from("T_BANK_GUARANTEE_DETAIL")
					->where("BG_REG_NUMBER = ?", $data['BG_REG_NUMBER'])
					->where("PS_FIELDNAME = ?", "Counter Guarantee Number")
					->query()->fetchAll();

				$this->view->get_cg_doc_number = $get_cg_doc_number[0]['PS_FIELDVALUE'];

				if (!$get_cg_doc_number[0]['PS_FIELDVALUE']) {
					$get_cg_doc_number = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE_DETAIL")
						->where("BG_REG_NUMBER = ?", $data['BG_REG_NUMBER'])
						->where("PS_FIELDNAME = ?", "Counter Guarantee Number")
						->query()->fetchAll();

					$this->view->get_cg_doc_number = $get_cg_doc_number[0]['PS_FIELDVALUE'];
				}
				// data modal Persetujuan Asuransi -----------------------

				// check limit asuransi ----------------------------------

				$get_linefacilityINS = $this->_db->select()
					->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT", "STATUS"])
					->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
					->query()->fetchAll();


				$check_all_detail = $this->_db->select()
					->from("T_BANK_GUARANTEE_DETAIL")
					->where("PS_FIELDNAME = ?", "Insurance Name")
					->where("PS_FIELDVALUE = ?", $data["BG_INSURANCE_CODE"])
					->query()->fetchAll();

				$total_bgamount_on_risk = 0;

				if (count($check_all_detail) > 0) {
					$save_bg_reg_number = [];
					foreach ($check_all_detail as $value) {
						array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
					}

					$get_bgamount_on_risks = $this->_db->select()
						->from(["TBG" => "T_BANK_GUARANTEE"], ["BG_AMOUNT", "BG_OLD", "BG_OLD_AMOUNT" => new Zend_Db_Expr('(SELECT BG_AMOUNT FROM T_BANK_GUARANTEE X WHERE X.BG_NUMBER = TBG.BG_OLD LIMIT 1)')])
						->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
						->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
						->query()->fetchAll();

					foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
						$tempCountAmount = ($get_bgamount_on_risk["BG_AMOUNT"] - $get_bgamount_on_risk['BG_OLD_AMOUNT']) >= 0 ?  ($get_bgamount_on_risk["BG_AMOUNT"] - $get_bgamount_on_risk['BG_OLD_AMOUNT']) : 0;
						$total_bgamount_on_risk += $tempCountAmount;
					}
				}

				$check_all_detail = $this->_db->select()
					->from("TEMP_BANK_GUARANTEE_DETAIL")
					->where("PS_FIELDNAME = ?", "Insurance Name")
					->where("PS_FIELDVALUE = ?", $data["BG_INSURANCE_CODE"])
					->query()->fetchAll();

				$total_bgamount_on_temp = 0;

				if (count($check_all_detail) > 0) {

					$save_bg_reg_number = [];
					foreach ($check_all_detail as $value) {
						array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
					}

					$get_bgamount_on_temps = $this->_db->select()
						->from(["TMBG" => "TEMP_BANK_GUARANTEE"], ["BG_AMOUNT", "BG_REG_NUMBER", "BG_OLD_AMOUNT" => new Zend_Db_Expr('(SELECT BG_AMOUNT FROM T_BANK_GUARANTEE X WHERE X.BG_NUMBER = TMBG.BG_OLD LIMIT 1)')])
						->where("COUNTER_WARRANTY_TYPE = '3'")
						->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
						->where("BG_STATUS IN (?)", ["5", "6", "7", '10', "14", "17", "20", '21', '22', '23', '24'])
						->query()->fetchAll();

					foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
						$tempCountAmount = ($get_bgamount_on_temp["BG_AMOUNT"] - $get_bgamount_on_temp['BG_OLD_AMOUNT']) >= 0 ? ($get_bgamount_on_temp["BG_AMOUNT"] - $get_bgamount_on_temp['BG_OLD_AMOUNT']) : 0;
						$total_bgamount_on_temp += $tempCountAmount;
					}
				}

				$current_limitINS = $get_linefacilityINS[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;

				if ($current_limitINS < 0) {
					$errLimitIns = true;

					$errmsg = 'Limit asuransi tidak tersedia';
					$this->view->errorLimitIns = $errmsg;
				}

				// end check limit asuransi ---------------------------------

				// Top Up Marginal Deposit ---------------
				$bgdatamdedit = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
					// ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
					->where('A.BG_REG_NUMBER = ?', $bgdata[0]['BG_REG_NUMBER'])
					->where('A.IS_TRANSFER != ?', '1')
					->query()->fetchAll();

				// var_dump($bgdata[0]['BG_REG_NUMBER']); die;

				$this->view->bgdatamdedit = $bgdatamdedit;

				// action form ----------------------------
				if ($this->_request->isPost()) {
					$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
					$approve = $this->_getParam('release');
					$BG_NUMBER = $this->_getParam('bgnumb');
					$reject = $this->_getParam('reject');
					$repair = $this->_getParam('repair');
					$filter             = new Application_Filtering();

					// update data TEMP_BANK_GUARANTEE_SPLIT --------------------
					$datas = $this->_request->getParams();
					// var_dump($datas);
					// die;

					// hapus data sebelumnya
					$cekmdsplit = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE_SPLIT")
						->where("BG_REG_NUMBER = ?", $data['BG_REG_NUMBER'])
						->query()->fetchAll();

					if (!empty($cekmdsplit)) {
						$this->_db->delete("TEMP_BANK_GUARANTEE_SPLIT", ["BG_REG_NUMBER = ?" => $data['BG_REG_NUMBER'], 'IS_TRANSFER != ?' => '1']);
					}
					// end hapus data sebelumya

					$splitArr = array();
					foreach ($datas['flselect_own'] as $keyOwn => $valOwn) {
						if ($valOwn == '0') {
							if (!empty($datas['flselect_account_manual'])) {
								foreach ($datas['flselect_account_manual'] as $ky => $vl) {
									if ($vl == '') {
										$nameAcct = explode('|', $datas['flselect_account_number'][$ky]);
										$splitArr[$ky]['acct'] = $nameAcct[0];
									} else {
										$splitArr[$ky]['acct'] = $vl;
									}
									if ($datas['flname_manual'][$ky] == '') {
										$splitArr[$ky]['acct_name'] = $datas['flname'][$ky];
									} else {
										$splitArr[$ky]['acct_name'] = $datas['flname_manual'][$ky];
									}
									$splitArr[$ky]['amount'] = $datas['flamount'][$ky];
									$splitArr[$ky]['flag'] = $datas['flselect_own'][$ky];
									$splitArr[$ky]['bank_code'] = '999';
								}

								foreach ($splitArr as $key => $value) {
									$temp_save = $this->_db->select()
										->from("M_CUSTOMER_ACCT")
										->where("ACCT_NO = ?", $value["acct"])
										->query()->fetchAll();

									$splitArr[$key]["currency"] = $datas['currency'][$key]; //'IDR';//$temp_save[0]["CCY_ID"];
									$splitArr[$key]["type"] = $datas['type_desc'][$key]; //$temp_save[0]["ACCT_DESC"];
								}
							}
						} else {
							if (!empty($datas['flselect_account_number'])) {
								foreach ($datas['flselect_account_number'] as $ky => $vl) {
									if ($vl == '') {
										$splitArr[$ky]['acct'] = $vl;
									} else {
										$nameAcct = explode('|', $datas['flselect_account_number'][$ky]);
										$splitArr[$ky]['acct'] = $nameAcct[0];
									}
									if ($datas['flname_manual'][$ky] == '') {
										$splitArr[$ky]['acct_name'] = $datas['flname'][$ky];
									} else {
										$splitArr[$ky]['acct_name'] = $datas['flname_manual'][$ky];
									}
									$splitArr[$ky]['amount'] = $datas['flamount'][$ky];
									$splitArr[$ky]['flag'] = $datas['flselect_own'][$ky];
									$splitArr[$ky]['bank_code'] = '999';
								}

								foreach ($splitArr as $key => $value) {
									$temp_save = $this->_db->select()
										->from("M_CUSTOMER_ACCT")
										->where("ACCT_NO = ?", $value["acct"])
										->query()->fetchAll();
									$splitArr[$key]["currency"] = $datas['currency'][$key]; //'IDR';//$temp_save[0]["CCY_ID"];
									$splitArr[$key]["type"] = $datas['type_desc'][$key]; //$temp_save[0]["ACCT_DESC"];

								}
							}
						}
					}

					foreach ($splitArr as $ky => $vl) {
						$tmparrDetail = array(
							'BG_REG_NUMBER' => $data['BG_REG_NUMBER'],
							'ACCT' => $vl['acct'],
							'BANK_CODE' =>  $vl['bank_code'],
							'NAME' => $vl['acct_name'],
							'AMOUNT' => Application_Helper_General::convertDisplayMoney(rtrim($vl['amount'], ",")),
							'FLAG' => $vl['flag'],
							'ACCT_TYPE' => $vl['type'],
							'CCY_ID' => $vl['currency']
						);
						$this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
					}
					// update data TEMP_BANK_GUARANTEE_SPLIT --------------------

					// update data TEMP_BANK_GUARANTEE --------------------
					$status = 22;

					$getCustApprover = $this->_db->select()
						->from('M_CUSTOMER', ['CUST_APPROVER'])
						->where('CUST_ID = ?', $bgdata[0]['CUST_ID_PEMOHON'])
						->query()->fetch();

					if (intval($getCustApprover['CUST_APPROVER']) !== 1) $status = 23;

					$dataupdate = [
						// 'PRINCIPLE_APPROVE' => '1',
						'BG_STATUS' => $status,
						'BG_UPDATED' => new Zend_Db_Expr("now()"),
						'BG_UPDATEDBY' => $this->_userIdLogin,
					];
					$where['BG_REG_NUMBER = ?'] = $numb;
					$this->_db->update('TEMP_BANK_GUARANTEE', $dataupdate, $where);
					// update data TEMP_BANK_GUARANTEE --------------------

					// insert data T_BANK_GUARANTEE_HISTORY --------------------
					$historyInsert = array(
						'DATE_TIME'         => new Zend_Db_Expr("now()"),
						'BG_REG_NUMBER'     => $numb,
						'CUST_ID'           => $this->_custIdLogin,
						'USER_LOGIN'        => $this->_userIdLogin,
						// 'HISTORY_STATUS'    => 19,
						'BG_REASON'         => "",
					);

					if ($bgdata[0]['BG_STATUS'] == '24') {
						$historyInsert['HISTORY_STATUS'] = 20;
					} else {
						$historyInsert['HISTORY_STATUS'] = 19;
					}

					$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
					// insert data T_BANK_GUARANTEE_HISTORY --------------------
					Application_Helper_General::writeLog('MDBG', 'Menyetujui Penambahan Marginal Deposit Prinsipal BG No Reg : ' . $data['BG_REG_NUMBER'] . '');

					// hapus T_APPROVAL
					$this->_db->delete('T_APPROVAL', ['PS_NUMBER = ?' => $numb]);

					// $this->_db->commit();
					$this->setbackURL('/' . $this->_request->getModuleName() . '/requestmd/');
					$this->_redirect('/notification/success/index');

					// if ($approve) {

					// 	// start validasi jaminan
					// 	$errValidasi = false;

					// 	if ($data["COUNTER_WARRANTY_TYPE"] == '2') {

					// 		$cust_id = $this->_custIdLogin;
					// 		// $jenisjaminan = strtoupper($arrbgType[$data['USAGE_PURPOSE']]);
					// 		$jenisjaminan = ($data['USAGE_PURPOSE'] == 'F4299') ? strtoupper($data['USAGE_PURPOSE_DESC']) : strtoupper($arrbgType[$data['USAGE_PURPOSE']]);

					// 		$check_grup_bumn = $this->_db->select()
					// 			->from("M_CUSTOMER")
					// 			->where("CUST_STATUS = 1")
					// 			->where("CUST_ID = ?", $cust_id)
					// 			->query()->fetchAll();

					// 		$check_charges_bg = $this->_db->select()
					// 			->from("M_CHARGES_BG")
					// 			->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'LFBUMN' : 'LFNONBUMN') . '%')
					// 			->where("CHARGES_NAME LIKE ?", '' . 'LF ' . $jenisjaminan . '%')
					// 			->query()->fetchAll();

					// 		$check_lf_detail = $this->_db->select()
					// 			->from("M_CUST_LINEFACILITY_DETAIL")
					// 			->where("OFFER_TYPE = ?", $jenisjaminan)
					// 			//->where("FLAG = 1")
					// 			->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
					// 			->where("CUST_ID = ?", $cust_id)
					// 			->query()->fetchAll();
					// 		$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];

					// 		$check_lf_detail[0]['FLAG'] == '1' && $check_lf_detail[0]['ACTIVE'] == 'Y' ? $errValidasi = false : $errValidasi = true;
					// 	}

					// 	if ($data["COUNTER_WARRANTY_TYPE"] == '3') {

					// 		$cust_id = $bgdata[0]["BG_INSURANCE_CODE"];

					// 		// $jenisjaminan = strtoupper($arrbgType[$bgdata[0]['USAGE_PURPOSE']]);
					// 		$jenisjaminan = ($data['USAGE_PURPOSE'] == 'F4299') ? strtoupper($data['USAGE_PURPOSE_DESC']) : strtoupper($arrbgType[$data['USAGE_PURPOSE']]);

					// 		//echo $jenisjaminan;die;

					// 		$check_grup_bumn = $this->_db->select()
					// 			->from("M_CUSTOMER")
					// 			->where("CUST_STATUS = 1")
					// 			->where("CUST_ID = ?", $cust_id)
					// 			->query()->fetchAll();

					// 		$check_charges_bg = $this->_db->select()
					// 			->from("M_CHARGES_BG")
					// 			->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'INSBUMN' : 'INSNONBUMN') . '%')
					// 			->where("CHARGES_NAME LIKE ?", '' . 'ASURANSI ' . $jenisjaminan . '%')
					// 			->query()->fetchAll();

					// 		$check_lf_detail = $this->_db->select()
					// 			->from("M_CUST_LINEFACILITY_DETAIL")
					// 			->where("OFFER_TYPE = ?", $jenisjaminan)
					// 			//->where("FLAG = 1")
					// 			->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
					// 			->where("CUST_ID = ?", $cust_id)
					// 			->query()->fetchAll();

					// 		$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];

					// 		$check_lf_detail[0]['FLAG'] == '1' && $check_lf_detail[0]['ACTIVE'] == 'Y' ? $errValidasi = false : $errValidasi = true;
					// 	}

					// 	if (empty($errValidasi)) {

					// 		$datas = $this->_request->getParams();

					// 		// $attachmentDestination   = UPLOAD_PATH . '/document/submit/';
					// 		// $adapter = new Zend_File_Transfer_Adapter_Http();
					// 		// $files = $adapter->getFileInfo();

					// 		// upload document ------------------------------------------
					// 			// $uploadDocument = $files["uploadDocument"];
					// 			// $counterGuaranteeFile = $files["counterGuaranteeFile"];
					// 			// $InsuranceSpecialFormatApprovalDocument = $files["InsuranceSpecialFormatApprovalDocument"];

					// 			// if ($uploadDocument["size"] != NULL) {
					// 			// 	if (($uploadDocument['size'] + 0) > 0) {
					// 			// 		$uploadDocumentName = $uploadDocument["name"];
					// 			// 		$adapter->setDestination($attachmentDestination);

					// 			// 		$date = date("dmy");
					// 			// 		$time = date("his");

					// 			// 		$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $uploadDocumentName;

					// 			// 		if (file_exists($attachmentDestination . $newFileName2)) {
					// 			// 			$newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $uploadDocumentName;
					// 			// 		}

					// 			// 		$adapter->addFilter('Rename', $newFileName2, "uploadDocument");
					// 			// 		$adapter->receive("uploadDocument");
					// 			// 	}
					// 			// } else {
					// 			// 	$newFileName2 = $save_bg_data_detail["Principle Insurance Document"];
					// 			// }

					// 			// if ($counterGuaranteeFile["size"] != NULL) {
					// 			// 	if (($counterGuaranteeFile['size'] + 0) > 0) {
					// 			// 		$counterGuaranteeFileName = $counterGuaranteeFile["name"];
					// 			// 		$adapter->setDestination($attachmentDestination);

					// 			// 		$date = date("dmy");
					// 			// 		$time = date("his");

					// 			// 		$newFileName3 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $counterGuaranteeFileName;

					// 			// 		if (file_exists($attachmentDestination . $newFileName3)) {
					// 			// 			$newFileName3 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $counterGuaranteeFileName;
					// 			// 		}

					// 			// 		$adapter->addFilter('Rename', $newFileName3, "counterGuaranteeFile");
					// 			// 		$adapter->receive("counterGuaranteeFile");
					// 			// 	}
					// 			// } else {
					// 			// 	$newFileName3 = $save_bg_data_detail["Counter Guarantee Document"];
					// 			// }
					// 		// end of upload document ---------------------------------

					// 		$get_detail_lf = $this->_db->select()
					// 			->from(["A" => "M_CUST_LINEFACILITY"], ["FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
					// 			->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
					// 			->query()->fetchAll();

					// 		$provisionFee = number_format(($get_detail_lf[0]["FEE_PROVISION"] + 0) * $data["BG_AMOUNT"] / 100, 2);
					// 		$provisionFee = str_replace(",", "", $provisionFee);
					// 		$admFee = number_format(($get_detail_lf[0]["FEE_ADMIN"] + 0), 2);
					// 		$admFee = str_replace(",", "", $admFee);
					// 		$stampFee = number_format(($get_detail_lf[0]["FEE_STAMP"] + 0), 2);
					// 		$stampFee = str_replace(",", "", $stampFee);

					// 	// ---------- lokasi ------------

					// 		$principleDetails = [
					// 			"Principle Agreement Number" => $datas["principleAgreementNumber"],
					// 			"Principle Agreement Granted Date" => $datas["grantedDate"],
					// 			"Principle Insurance Premium" => (str_replace(",", "", $datas["insurancePremium"]) + 0),
					// 			"Principle Insurance Administration" => (str_replace(",", "", $datas["insuranceAdministration"]) + 0),
					// 			"Principle Insurance Stamp" => (str_replace(",", "", $datas["insuranceMaterai"]) + 0),
					// 			"Principle Insurance Document" => $newFileName2,
					// 		];

					// 		if ($datas['pilih_kontra_garansi'] == 1) {
					// 			if (!empty($datas["counterGuaranteeNumber"])) {
					// 				$principleDetails["Counter Guarantee Number"] = $datas["counterGuaranteeNumber"];
					// 				$principleDetails["Counter Guarantee Granted Date"] = $datas["counterGuaranteeDate"];
					// 				$principleDetails["Counter Guarantee Document"] = $newFileName3;
					// 			}
					// 		}

					// 		if ($data['BG_FORMAT'] == "2") {
					// 			$principleDetails["Insurance Special Format Approval Document"] = $newFileName4;
					// 		}

					// 		if ($datas["pilih_marginal_deposit_principal"] == 1) {
					// 			if (!empty($datas["marginalDepositPercentage"])) {
					// 				$principleDetails["Marginal Deposit Percentage"] = (str_replace(",", "", $datas["marginalDepositPercentage"]) + 0);
					// 			}
					// 		}

					// 		foreach ($principleDetails as $key => $value) {
					// 			$this->_db->delete("TEMP_BANK_GUARANTEE_DETAIL", [
					// 				"PS_FIELDNAME = ?" => $key,
					// 				"BG_REG_NUMBER = ?" => $numb
					// 			]);
					// 		}

					// 		$this->_db->delete("TEMP_BANK_GUARANTEE_DETAIL", [
					// 			"PS_FIELDNAME = ?" => "Counter Guarantee Number",
					// 			"BG_REG_NUMBER = ?" => $numb
					// 		]);

					// 		foreach ($principleDetails as $key => $principleDetail) {
					// 			$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', [
					// 				"BG_REG_NUMBER" => $numb,
					// 				"CUST_ID" => $this->_custIdLogin,
					// 				"USER_ID" => $this->_userIdLogin,
					// 				"PS_FIELDNAME" => $key,
					// 				"PS_FIELDTYPE" => 1,
					// 				"PS_FIELDVALUE" => $principleDetail,
					// 			]);
					// 		}

					// 		$this->_db->insert('T_FACTIVITY', array(
					// 			'LOG_DATE'         => new Zend_Db_Expr('now()'),
					// 			'CUST_ID'           => $this->_custIdLogin,
					// 			'USER_ID'           => $this->_userIdLogin,
					// 			'USER_NAME'           => $this->_userNameLogin,
					// 			'ACTION_DESC'       => 'MDBG',
					// 			'ACTION_FULLDESC'   => "Lihat Daftar Permintaan Tambah Marginal Deposit",
					// 		));

					// 		$conf = Zend_Registry::get('config');

					// 		$counterWarrantyType = $conf["bgcg"]["type"]["desc"];
					// 		$counterWarrantyCode = $conf["bgcg"]["type"]["code"];

					// 		$counterWarranty = array_combine(array_values($counterWarrantyCode), array_values($counterWarrantyType));

					// 		$bgStatusType = $conf["bg"]["status"]['desc'];
					// 		$bgStatusCode = $conf["bg"]["status"]["code"];

					// 		$bgStatus = array_combine(array_values($bgStatusCode), array_values($bgStatusType));

					// 		$getNamaPemohon = $this->_db->select()
					// 			->from('M_CUSTOMER', ['CUST_NAME'])
					// 			->where('CUST_ID = ?', $bgdata[0]['CUST_ID'])
					// 			->query()->fetch();

					// 		$getNamaPemohon = $getNamaPemohon['CUST_NAME'];

					// 		$getAllBgroup = $this->_db->select()
					// 			->from('M_BPRIVI_GROUP')
					// 			->where('BPRIVI_ID  = ?', 'VNCS')
					// 			->query()->fetchAll();

					// 		$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');

					// 		$getAllBuser = $this->_db->select()
					// 			->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
					// 			->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
					// 			->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
					// 			->where('MB.BGROUP_ID IN (?)', $saveBgroup)
					// 			->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
					// 			->query()->fetchAll();

					// 		$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';

					// 		$allSetting = $setting->getAllSetting();
					// 		foreach ($getAllBuser as $buser) {
					// 			$getEmailTemplate = $allSetting['bemailtemplate_verification_notif'];

					// 			$data = [
					// 				'[[user_name]]' => $buser['BUSER_NAME'],
					// 				'[[group_name]]' => $buser['BGROUP_DESC'],
					// 				'[[master_bank_name]]' => $allSetting["master_bank_name"],
					// 				'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
					// 				'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
					// 				'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
					// 				'[[recipient_name]]' => $getNamaPemohon,
					// 				// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
					// 				'[[is_amandment]]' => $tipePengajuan,
					// 				'[[bg_status]]' => $bgStatus[5],
					// 				'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
					// 			];

					// 			$getEmailTemplate = strtr($getEmailTemplate, $data);
					// 			Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], 'BG Verification Notif', $getEmailTemplate);
					// 		}

					// 		$this->setbackURL('/' . $this->_request->getModuleName() . '/requestmd/');
					// 		$this->_redirect('/notification/success/index');
					// 	} else {
					// 		$this->view->err = true;
					// 		$this->view->errMsg = 'Jaminan tidak tersedia';
					// 	}
					// }

					// if ($reject) {
					// 	$data = array('BG_STATUS' => '9');
					// 	$where['BG_REG_NUMBER = ?'] = $numb;
					// 	$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);
					// 	$notes = $this->_getParam('PS_REASON_REJECT');
					// 	$historyInsert = array(
					// 		'DATE_TIME'         => new Zend_Db_Expr("now()"),
					// 		'BG_REG_NUMBER'         => $numb,
					// 		'CUST_ID'           => $this->_custIdLogin,
					// 		'USER_LOGIN'        => $this->_userIdLogin,
					// 		'HISTORY_STATUS'    => 4,
					// 		'BG_REASON'         => $notes,
					// 	);

					// 	$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

					// 	$this->_db->insert('T_FACTIVITY', array(
					// 		'LOG_DATE'         => new Zend_Db_Expr('now()'),
					// 		'CUST_ID'           => $this->_custIdLogin,
					// 		'USER_ID'           => $this->_userIdLogin,
					// 		'USER_NAME'           => $this->_userNameLogin,
					// 		'ACTION_DESC'       => 'MDBG',
					// 		'ACTION_FULLDESC'   => "Pengajuan ditolak oleh Asuransi (Nomor Registrasi : " . $numb . ")",
					// 	));
					// 	$this->setbackURL('/' . $this->_request->getModuleName() . '/requestmd/');
					// 	$this->_redirect('/notification/success/index');
					// }

					// if ($repair) {
					// 	$data = array('BG_STATUS' => '19');
					// 	$where['BG_REG_NUMBER = ?'] = $numb;
					// 	$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

					// 	$notes = $this->_getParam('PS_REASON_REPAIR');
					// 	$historyInsert = array(
					// 		'DATE_TIME'         => new Zend_Db_Expr("now()"),
					// 		'BG_REG_NUMBER'         => $numb,
					// 		'CUST_ID'           => $this->_custIdLogin,
					// 		'USER_LOGIN'        => $this->_userIdLogin,
					// 		'HISTORY_STATUS'    => 15,
					// 		'BG_REASON'         => $notes,
					// 	);

					// 	$this->_db->insert('T_FACTIVITY', array(
					// 		'LOG_DATE'         => new Zend_Db_Expr('now()'),
					// 		'CUST_ID'           => $this->_custIdLogin,
					// 		'USER_ID'           => $this->_userIdLogin,
					// 		'USER_NAME'           => $this->_userNameLogin,
					// 		'ACTION_DESC'       => 'MDBG',
					// 		'ACTION_FULLDESC'   => "Pengajuan ditolak oleh Asuransi (Nomor Registrasi : " . $numb . ")",
					// 	));

					// 	$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

					// 	$this->_db->delete('TEMP_BANK_GUARANTEE_PRELIMINARY', ["BG_REG_NUMBER = ?" => $numb]);
					// 	$this->_db->delete('TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER', ["BG_REG_NUMBER = ?" => $numb]);

					// 	$getAllVRpbgPriv = $this->_db->select()
					// 		->from(['MFPU' => 'M_FPRIVI_USER'], [
					// 			'*',
					// 			'EMAIL' => new Zend_Db_Expr("(SELECT USER_EMAIL FROM M_USER WHERE USER_ID = (SELECT REPLACE(MFPU.FUSER_ID, '" . $bgdata['0']['CUST_ID'] . "', '')) AND CUST_ID = '" . $bgdata['0']['CUST_ID'] . "' limit 1)")
					// 		])
					// 		->where('FUSER_ID LIKE ?', '%' . $bgdata['0']['CUST_ID'] . '%')
					// 		->where('FPRIVI_ID = ?', 'RPBG')
					// 		->query()->fetchAll();

					// 	$emailTemplate = $settings->getSetting('femailtemplate_repairasuransitoprincipal');

					// 	$bgcgType         = $conf["bgcg"]["type"]["desc"];
					// 	$bgcgCode         = $conf["bgcg"]["type"]["code"];

					// 	$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

					// 	$dataEmail = array(
					// 		'[[namaCustomerPrinsipal]]' => $selectcomp[0]['CUST_NAME'],
					// 		'[[nomorRegis]]' => $bgdata[0]['BG_REG_NUMBER'],
					// 		'[[subjekBG]]' => $bgdata[0]['BG_SUBJECT'],
					// 		'[[kontraGaransi]]' => $arrbgcg[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
					// 		'[[notes]]'    => $notes,
					// 	);

					// 	$emailTemplate = strtr($emailTemplate, $dataEmail);

					// 	// send email 
					// 	if (count($getAllVRpbgPriv) > 0) {
					// 		foreach ($getAllVRpbgPriv as $value) {
					// 			Application_Helper_Email::sendEmail($value['EMAIL'], 'Permintaan Perbaikan Bank Garansi', $emailTemplate);
					// 		}
					// 	}

					// 	Application_Helper_Email::sendEmail($selectcomp[0]['CUST_EMAIL'], 'Permintaan Perbaikan Bank Garansi', $emailTemplate);

					// 	$this->setbackURL('/' . $this->_request->getModuleName() . '/principle/');
					// 	$this->_redirect('/notification/success/index');
					// }

					$back = $this->_getParam('back');
					if ($back) {
						$this->_redirect('/eformworkflow/requestmd');
					}
				}
				// action form ----------------------------
			}
		}

		Application_Helper_General::writeLog('MDBG', 'Lihat Detail BG No Reg : ' . $data['BG_REG_NUMBER'] . '');
	}

	public function getstatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$temp_status_line_facility = [
			"1" => "Approve",
			"2" => "Terminated",
			"3" => "Expired",
			"4" => "Freeze Submission",
		];

		$select_line_facility	= $this->_db->select()
			->from(array('A' => 'M_CUST_LINEFACILITY'), array(
				'STATUS' 	=> 'A.STATUS'
			))
			->where("A.CUST_ID 	= ?", $this->_custIdLogin);

		$select_line_facility = $this->_db->fetchAll($select_line_facility);

		$select_line_facility = $select_line_facility[0]['STATUS'];

		if (!empty($select_line_facility)) {
			if ($select_line_facility == 1) {
				echo json_encode(["status" => "YES"]);
			} else {
				echo json_encode(["status" => "NO", "msg" => "Tidak dapat melanjutkan proses (Status Perjanjian : " . $temp_status_line_facility[$select_line_facility] . ")"]);
			}
		} else {
			echo json_encode(["status" => "NO", "msg" => "Line Facility tidak ditemukan"]);
		}
	}

	public function checkgrantedAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// untuk merefresh limit user tersebut
		$app      = Zend_Registry::get('config');
		$bankCode = $app['app']['bankcode'];

		$custID = $this->_custIdLogin;

		$currentGuarantee = 0;

		$dataDetail = $this->_db->select()
			->from(
				array('M_MARGINALDEPOSIT_DETAIL'),
				array('*')
			)
			->where('CUST_ID = ?', $custID);

		$dataDetail = $this->_db->fetchAll($dataDetail);
		foreach ($dataDetail as $row) {
			$acctNo  = $row['MD_ACCT'];
			$acctCcy = Application_Helper_General::getCurrNum($row['MD_ACCT_CCY']);
			$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
			$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
			if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
				$currentGuarantee = (float)$result['balance_active'];
				$dataUpdate  = ['GUARANTEE_AMOUNT' => $result['balance_active']];
				$whereUpdate = ['MD_ACCT = ?' => $acctNo];

				try {
					$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
				} catch (Exception $error) {
					$this->_db->rollBack();
				}
			} else {

				$svcAccount = new Service_Account($acctNo, $ccy, $bankCode);
				$result = $svcAccount->inquiryDeposito('AB', TRUE);
				//var_dump($result);die;
				if ($result['response_code'] == '0000') {
					$dataUpdate  = ['GUARANTEE_AMOUNT' => $result['balance']];
					$whereUpdate = ['MD_ACCT = ?' => $acctNo];
					$this->_db->update('M_MARGINALDEPOSIT_DETAIL', $dataUpdate, $whereUpdate);
				}
			}
		}

		$check_cg_completeness = $this->_db->select()
			->from("T_BANK_GUARANTEE_DETAIL")
			->where("PS_FIELDNAME = ?", "Insurance Name")
			->where("PS_FIELDVALUE = ?", $custID)
			->query()->fetchAll();

		$cg_completeness = 0;
		if (count($check_cg_completeness) > 0) {
			$checker = 0;
			foreach ($check_cg_completeness as $key => $value) {
				$check_cgins_status = $this->_db->select()
					->from("T_BANK_GUARANTEE")
					->where("BG_REG_NUMBER = ?", $value["BG_REG_NUMBER"])
					->where("CGINS_STATUS = ?", "1");

				$check_cgins_status = $this->_db->fetchRow($check_cgins_status);
				if ($check_cgins_status) {
					$checker += 1;
				}
			}
			if ($checker == count($check_cg_completeness)) {
				$cg_completeness = 1;
			}
		}

		$this->_db->update('M_MARGINALDEPOSIT', [
			"CG_COMPLETENESS" => $cg_completeness,
		], [
			"CUST_ID = ?" => $custID
		]);

		$dataUpdate2  = ['LAST_CHECK' => new Zend_Db_Expr('now()')];
		$whereUpdate2 = ['CUST_ID = ?' => $custID];

		try {
			$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
		} catch (Exception $error) {
			$this->_db->rollBack();
		}



		// start validasi top up kurang dari marginal deposit
		$cek_min_md = $this->_db->select()
			->from("M_CUST_LINEFACILITY")
			->where("CUST_ID = ?", $custID)
			->query()->fetchAll();

		$filter = true;
		$filterParam['fCustID'] = $custID;

		$selectData = $this->_db->select()
			->from(
				array('A' => 'M_MARGINALDEPOSIT'),
				array('*')
			)
			->joinLeft(
				array('B' => 'M_MARGINALDEPOSIT_DETAIL'),
				'B.CUST_ID = A.CUST_ID',
				array('*')
			)
			->joinLeft(
				array('C' => 'M_CUSTOMER'),
				'C.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			)
			->joinLeft(
				array('D' => 'M_CUST_LINEFACILITY'),
				'D.CUST_ID = A.CUST_ID',
				array('PLAFOND_LIMIT', 'MARGINAL_DEPOSIT')
			);

		if ($filter == TRUE) {
			if ($filterParam['fCustID']) {
				$selectData->where('A.CUST_ID LIKE ' . $this->_db->quote('%' . $filterParam['fCustID'] . '%'));
			}
			if ($filterParam['fStatus']) {
				$selectData->where('A.PKS_STATUS LIKE ' . $this->_db->quote('%' . $filterParam['fStatus'] . '%'));
			}
		}

		$data = $this->_db->fetchAll($selectData);

		$dataDetail = $this->_db->select()
			->from(
				array('M_MARGINALDEPOSIT_DETAIL'),
				array('*')
			)
			->where('CUST_ID = ?', $custID);

		$dataDetail = $this->_db->fetchAll($dataDetail);
		foreach ($dataDetail as $row) {
			$GUARANTEE_AMOUNT += $row['GUARANTEE_AMOUNT'];
		}

		foreach ($data as $row) {
			$COUNTING_DEADLINE_TOPUP += $row['COUNTING_DEADLINE_TOPUP'];
		}

		if ($GUARANTEE_AMOUNT <= $cek_min_md[0]['MARGINAL_DEPOSIT']) {
			$mdData = $this->_db->select()
				->from("M_MARGINALDEPOSIT")
				->where("CUST_ID = ?", $custID);

			$mdData = $this->_db->fetchRow($mdData);
			if ($mdData['LAST_CHECK_MINUS'] == null) {

				$dataUpdate2  = ['LAST_CHECK_MINUS' => new Zend_Db_Expr('now()'), 'COUNTING_DEADLINE_TOPUP' => $COUNTING_DEADLINE_TOPUP + 1];
				$whereUpdate2 = ['CUST_ID = ?' => $custID];
				try {
					$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
				} catch (Exception $error) {
					$this->_db->rollBack();
				}
			}
		} else {
			$dataUpdate2  = ['LAST_CHECK_MINUS' => null];
			$dataUpdate2  = ['COUNTING_DEADLINE_TOPUP' => null];
			$whereUpdate2 = ['CUST_ID = ?' => $custID];
			try {
				$this->_db->update('M_MARGINALDEPOSIT', $dataUpdate2, $whereUpdate2);
			} catch (Exception $error) {
				$this->_db->rollBack();
			}
		}

		$temp_status_line_facility = [
			"1" => "Approve",
			"2" => "Terminated",
			"3" => "Expired",
			"4" => "Freeze Submission",
		];

		$plafon_limit = $this->_db->select()
			->from(["A" => "M_CUST_LINEFACILITY"], ["PLAFOND_LIMIT", "STATUS"])
			->where("CUST_ID = ?", $this->_custIdLogin)
			->query()->fetchAll();

		if (!empty($plafon_limit)) {
			if ($plafon_limit[0]['STATUS'] == 1) {
				$check_all_detail = $this->_db->select()
					->from("T_BANK_GUARANTEE_DETAIL")
					->where("PS_FIELDNAME = ?", "Insurance Name")
					->where("PS_FIELDVALUE = ?", $this->_custIdLogin)
					->query()->fetchAll();

				$total_bgamount_on_risk = 0;

				if (count($check_all_detail) > 0) {
					$save_bg_reg_number = [];
					foreach ($check_all_detail as $value) {
						array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
					}

					$get_bgamount_on_risks = $this->_db->select()
						->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
						->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
						->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
						->query()->fetchAll();

					foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
						$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
					}
				}

				$check_all_detail = $this->_db->select()
					->from("TEMP_BANK_GUARANTEE_DETAIL")
					->where("PS_FIELDNAME = ?", "Insurance Name")
					->where("PS_FIELDVALUE = ?", $this->_custIdLogin)
					->query()->fetchAll();

				$total_bgamount_on_temp = 0;

				if (count($check_all_detail) > 0) {

					$save_bg_reg_number = [];
					foreach ($check_all_detail as $value) {
						array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
					}

					$get_bgamount_on_temps = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
						->where("COUNTER_WARRANTY_TYPE = '3'")
						->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
						->where("BG_STATUS IN (?)", ["5", "6", "7", "14", "17", "20"])
						->query()->fetchAll();

					foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
						$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
					}
				}

				if ((intval($plafon_limit[0]['PLAFOND_LIMIT']) - intval($total_bgamount_on_risk) - intval($total_bgamount_on_temp)) >= 0) {
					echo json_encode(["status" => "YES"]);
				} else {
					echo json_encode(["status" => "NO", "msg" => "Pengajuan belum dapat diproses. Limit " . $insurance_name . " saat ini belum sesuai"]);
				}
			} else {
				echo json_encode(["status" => "NO", "msg" => "Tidak dapat melanjutkan proses (Status Perjanjian : " . $temp_status_line_facility[$plafon_limit[0]['STATUS']] . ")"]);
			}
		} else {
			echo json_encode(["status" => "NO", "msg" => "Tidak ada limit plafon yang tersedia"]);
		}
	}

	public function findPolicyBoundary($transfertype, $amount)
	{


		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	public function findUserBoundary($transfertype, $amount)
	{

		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
		//	echo '<pre>';
		//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	public function checktypebglfAction()
	{

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$get_offer_type = $this->_request->getParam("offertype");
		$warranty_type = $this->_request->getParam("warranty_type");

		if ($warranty_type == 2) {
			$cust_id = $this->_request->getParam("cust_id");

			$check_grup_bumn = $this->_db->select()
				->from("M_CUSTOMER")
				->where("CUST_STATUS = 1")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();


			$check_charges_bg = $this->_db->select()
				->from("M_CHARGES_BG")
				->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'LFBUMN' : 'LFNONBUMN') . '%')
				->where("CHARGES_NAME LIKE ?", '' . 'LF ' . $get_offer_type . '%')
				->query()->fetchAll();


			$check_lf_detail = $this->_db->select()
				->from("M_CUST_LINEFACILITY_DETAIL")
				->where("OFFER_TYPE = ?", $get_offer_type)
				//->where("FLAG = 1")
				->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();
			$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($check_lf_detail[0]);
	}

	public function checktypebginsAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$get_offer_type = $this->_request->getParam("offertype");
		$warranty_type = $this->_request->getParam("warranty_type");

		if ($warranty_type == 3) {
			$cust_id = $this->_request->getParam("cust_id");

			$check_grup_bumn = $this->_db->select()
				->from("M_CUSTOMER")
				->where("CUST_STATUS = 1")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();


			$check_charges_bg = $this->_db->select()
				->from("M_CHARGES_BG")
				->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'INSBUMN' : 'INSNONBUMN') . '%')
				->where("CHARGES_NAME LIKE ?", '' . 'ASURANSI ' . $get_offer_type . '%')
				->query()->fetchAll();


			$check_lf_detail = $this->_db->select()
				->from("M_CUST_LINEFACILITY_DETAIL")
				->where("OFFER_TYPE = ?", $get_offer_type)
				//->where("FLAG = 1")
				->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();
			$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($check_lf_detail[0]);
	}

	public function inquiryaccountbalanceAction()
	{
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccountBalance('AB', TRUE);
		$info = $svcAccount->inquiryAccontInfo("AB");

		if ($result["status"] != 1 && $result["status"] != 4) {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return 0;
		}

		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
		$filterBy = $result['account_number']; // or Finance etc.
		$check_prod_type = $this->_db->select()
			->from("M_PRODUCT_TYPE")
			->where("PRODUCT_CODE = ?", $result["account_type"])
			->query()->fetch();

		$result["check_product_type"] = 0;
		if (!empty($check_prod_type)) {
			$result["check_product_type"] = 1;
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($result);
		die();

		$data['data'] = false;
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($result);
	}

	public function inquirydepositoAction()
	{
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		$bg_reg_number = $this->_getParam('bg_reg_number');
		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryDeposito('AB', TRUE);

		$info = $svcAccount->inquiryAccontInfo("AB");

		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
		$result2 = $svcAccountCIF->inquiryCIFAccount();

		if ($result["response_code"] != "0000") {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return 0;
		}

		if ($result["status"] != 1 && $result["status"] != 4) {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return 0;
		}

		$sqlRekeningJaminanExist = $this->_db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
			->where('A.ACCT = ?', $result['account_number'])
			->query()->fetchAll();

		if ($bg_reg_number && $sqlRekeningJaminanExist) {
			foreach ($sqlRekeningJaminanExist as $key => $value) {
				if ($value["BG_REG_NUMBER"] == $bg_reg_number) {
					$result["check_exist_split"] = 0;
				} else {
					$result["check_exist_split"] = 1;
				}
			}
		} else {
			$result["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;
		}

		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
		$result2 = $svcAccountCIF->inquiryCIFAccount();
		$filterBy = $result['account_number']; // or Finance etc.
		$check_prod_type = $this->_db->select()
			->from("M_PRODUCT_TYPE")
			->where("PRODUCT_CODE = ?", $result["account_type"])
			->query()->fetch();

		$result["check_product_type"] = 0;
		if (!empty($check_prod_type)) {
			$result["check_product_type"] = 1;
		}

		$data['data'] = false;
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($result);
	}

	public function inquiryaccountinfoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		if ($result['response_desc'] == 'Success') //00 = success
		{
			$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
			$result2 = $svcAccountCIF->inquiryCIFAccount();

			$filterBy = $result['account_number']; // or Finance etc.
			$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
				return ($var['account_number'] == $filterBy);
			});

			$singleArr = array();
			foreach ($new as $key => $val) {
				$singleArr = $val;
			}
		} else {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return 0;
		}

		if ($singleArr['type_desc'] == 'Deposito') {
			$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountDeposito->inquiryDeposito();

			if ($result3["response_code"] != "0000") {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode(array_merge($result3, $singleArr, $result));
				return 0;
			}

			if ($result3["status"] != 1 && $result3["status"] != 4) {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode(array_merge($result3, $singleArr, $result));
				return 0;
			}

			$sqlRekeningJaminanExist = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
				->where('A.ACCT = ?', $result['account_number'])
				->query()->fetchAll();

			$result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;

			$get_product_type = current($new)["type"];

			$check_prod_type = $this->_db->select()
				->from("M_PRODUCT_TYPE")
				->where("PRODUCT_CODE = ?", $result3["account_type"])
				->query()->fetch();

			$result3["check_product_type"] = 0;
			if (!empty($check_prod_type)) {
				$result3["check_product_type"] = 1;
			}
		} else {
			$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountBalance->inquiryAccountBalance();

			if ($result3["status"] != 1 && $result3["status"] != 4) {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($result3);
				return 0;
			}

			$get_product_type = current($new)["type"];
			$check_prod_type = $this->_db->select()
				->from("M_PRODUCT_TYPE")
				->where("PRODUCT_CODE = ?", $result3["account_type"])
				->query()->fetch();

			$result3["check_product_type"] = 0;
			if (!empty($check_prod_type)) {
				$result3["check_product_type"] = 1;
			}
		}

		$return = array_merge($result, $singleArr, $result3);

		$data['data'] = false;
		is_array($return) ? $return :  $return = $data;

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($return);
	}
}
