<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class customer_UserprintpdfController extends Application_Main
{
	public function initController(){
		  $this->_helper->layout()->setLayout('popup');
	 }
	public function indexAction()
	{
		$set = new Settings();
		
		$templateEmailMasterBankAddress = $set->getSetting('master_bank_address');
		$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
		$templateEmailMasterBankAppUrl = $set->getSetting('master_bank_app_url');
		$templateEmailMasterBankCity = $set->getSetting('master_bank_city');
		$templateEmailMasterBankCountry = $set->getSetting('master_bank_country');
		$templateEmailMasterBankEmail = $set->getSetting('master_bank_email');
		$templateEmailMasterBankEmail1 = $set->getSetting('master_bank_email1');
		$templateEmailMasterBankFax = $set->getSetting('master_bank_fax');
		$templateEmailMasterBankName = $set->getSetting('master_bank_name');
		$templateEmailMasterBankProvince = $set->getSetting('master_bank_province');
		$templateEmailMasterBankTelp = $set->getSetting('master_bank_telp');
		$templateEmailMasterBankWebsite = $set->getSetting('master_bank_website');
	
		$FEmailResetPass = $set->getSetting('ftemplate_resetpwd');
		$user_id = $this->_getParam('user_id');
		$cust_id = $this->_getParam('cust_id');
		
		$isi = $this->_db->SELECT ()
						->FROM ('M_USER', array('USER_ID','CUST_ID','USER_EMAIL','USER_FULLNAME','USER_CLEARTEXT_PWD'))
						->WHERE ('USER_ID = ? ', $user_id)
						->WHERE ('CUST_ID = ? ',$cust_id);
							
		$isi = $this->_db->fetchrow($isi);
		$email = ($isi['USER_EMAIL'] != null || $isi['USER_EMAIL'] != '' ) ? $isi['USER_EMAIL'] : 'No Email Registered';
		$newPassword = substr(base64_decode($isi['USER_CLEARTEXT_PWD']),4, -4);

		$FEmailResetPass = str_ireplace('[[user_fullname]]',$isi['USER_FULLNAME'],$FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[comp_accid]]',$isi['CUST_ID'],$FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[user_login]]',$isi['USER_ID'],$FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[user_email]]',$email,$FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[user_cleartext_password]]',$newPassword,$FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$FEmailResetPass);
		$FEmailResetPass = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$FEmailResetPass);
		Application_Helper_General::writeLog('CSRL','Downloading Print Pdf : Cust Id ( '.$this->_custIdLogin.' ) ,User Id ( '.$user_id.' )');
		$data = array(
									'USER_RPWD_ISPRINTED' 			=> 1,
								);
		$where =  array();
		$where['CUST_ID 		 = ?'] 	= $cust_id;
		$where['USER_ID 		 = ?'] 	= $user_id;
		$this->_db->update('M_USER',$data,$where);
		$this->view->forms = $FEmailResetPass;
	}
	
}
?>