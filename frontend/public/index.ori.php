<?php 

date_default_timezone_set('Asia/Jakarta');

$beforeexecute = date("Y-m-d H:i:s");

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
   defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));


//define('SHARE_UPLOAD_PATH',APPLICATION_PATH . '/../../data/uploads');
define('LIBRARY_PATH',APPLICATION_PATH.'/../../library');
define('UPLOAD_PATH',LIBRARY_PATH . '/data/uploads');
define('HELPER_PATH',LIBRARY_PATH . '/data/uploads');
define('SHARE_UPLOAD_PATH',LIBRARY_PATH . '/data/uploads/submit');

define('DEBUG_MODE',1);    
    
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(LIBRARY_PATH)
  // realpath('/usr/local/zend/apache2/htdocs/coolPayBackend/library/')
)));

//get_include_path()

/** Zend_Application */
require_once 'Zend/Application.php';
require_once 'Zend/Config/Ini.php';
//require_once 'Zend/Debug.php';


//Create a Zend_Config_Ini object as the main config
$config = new Zend_Config_Ini(LIBRARY_PATH.'/configs/application.ini',
							  APPLICATION_ENV,
							  array('allowModifications'=>true));
							  
//uses php5 SPL DirectoryIterator class 
$dir = new DirectoryIterator(LIBRARY_PATH.'/configs');
foreach($dir as $file) 
{
	if(!$file->isDot() && $file->isFile() && $file->getFileName() != 'application.ini') 
	{
		// WARNING : All loaded ini files must have the same sections from main ini file
		if(substr($file->getFileName(),-4)=='.ini'){
			$cfg = new Zend_Config_Ini(LIBRARY_PATH.'/configs/'.$file->getFileName(),
									   APPLICATION_ENV, 
									   array('allowModifications'=>true));
			//merge into main config
			$config->merge($cfg);
		}
	}
}



// Create application, bootstrap, and run
$application = new Zend_Application( APPLICATION_ENV, $config);
$application->bootstrap()
            ->run();

$afterexecute = date("Y-m-d H:i:s"); 

if ($_REQUEST['csv'] == false){

	echo "<!-- executed time : $beforeexecute - $afterexecute -->";
}
