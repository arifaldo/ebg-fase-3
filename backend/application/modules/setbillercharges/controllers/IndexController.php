<?php


require_once 'Zend/Controller/Action.php';


class setbillercharges_IndexController extends Application_Main
{
	public function indexAction() 
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
	
		$this->_helper->layout()->setLayout('newlayout');
		$model = new setbillercharges_Model_Setbillercharges();
		
		$providerArr = $model->getServiceProvider();
		
		$this->view->var = $providerArr;	
		
		$fields = array	(
							'biller_code'  			=> array	(
																	'field' => 'PROVIDER_CODE',
																	'label' => $this->language->_('Biller Code'),
																	'sortable' => true
																),
							'biller_name'  			=> array	(
																	'field' => 'PROVIDER_NAME',
																	'label' => $this->language->_('Biller Name'),
																	'sortable' => true
																),
							'biller_type'  			=> array	(
																	'field' => 'PROVIDER_TYPE',
																	'label' => $this->language->_('Biller Type'),
																	'sortable' => true
																),
							'suggest_date'  		=> array	(
																	'field' => 'CHARGES_SUGGESTED',
																	'label' => $this->language->_('Suggested Date'),
																	'sortable' => true
																),
							'suggestor'  		=> array	(
																	'field' => 'CHARGES_SUGGESTEDBY',
																	'label' => $this->language->_('Suggester'),
																	'sortable' => true
																),
						);

		$filterlist = array("BILLER_CODE","BILLER_NAME","SUGESTBY","SUGEST_DATE");
		
		$this->view->filterlist = $filterlist;
						
		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'BILLER_CODE'    	=> array('StripTags','StringTrim'),
	                       'BILLER_NAME'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'SUGESTBY'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'SUGEST_DATE'  	=> array('StripTags','StringTrim'),
						   'SUGEST_DATE_END'  		=> array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array('filter' 		=> array(),
	                       'BILLER_CODE'    	=> array(),
	                       'BILLER_NAME'  	=> array(),
						   'SUGESTBY'  	=> array(),
						   'SUGEST_DATE'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'SUGEST_DATE_END'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );
	    
	    $dataParam = array("BILLER_CODE","BILLER_NAME","BILLER_STATUS","SUGESTBY","APPROVEBY","SERVICE_CATEGORY");
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "SUGEST_DATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		// print_r($dataParamValue);die;
			if(!empty($this->_request->getParam('sugestdate'))){
				$createarr = $this->_request->getParam('sugestdate');
					$dataParamValue['SUGEST_DATE'] = $createarr[0];
					$dataParamValue['SUGEST_DATE_END'] = $createarr[1];
			}
			                 


		$fParam = array();	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    // $filter = $zf_filter->getEscaped('filter');
	    $filter 		= $this->_getParam('filter');

	    $billerCode = $fParam['billerCode'] = html_entity_decode($zf_filter->getEscaped('BILLER_CODE'));
		$billerName = $fParam['billerName'] = html_entity_decode($zf_filter->getEscaped('BILLER_NAME'));
		$suggestor = $fParam['suggestor'] = html_entity_decode($zf_filter->getEscaped('SUGESTBY'));
		$datefrom = $fParam['datefrom'] = html_entity_decode($zf_filter->getEscaped('SUGEST_DATE'));
		$dateto	= $fParam['dateto'] = html_entity_decode($zf_filter->getEscaped('SUGEST_DATE_END'));
		
		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');
		
		$page = (Zend_Validate::is($page,'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		if($filter = TRUE)
		{
			if(!empty($datefrom))
			{
				$this->view->fDateTo    = $dateto;
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$fParam['datefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($dateto))
			{
				$this->view->fDateFrom  = $datefrom;
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
			}
			
		 	if($billerCode)
		    {
	       		$this->view->billerCode = $billerCode;
		    }
		    
			if($billerName)
			{
	       		$this->view->billerName = $billerName;
			}
			
			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
		    }
		}
		
		$data = $model->getData($fParam,$sortBy,$sortDir,$filter);
		
    	$this->paging($data);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;
		$this->setbackURL('/'.$this->_request->getModuleName());
    	if($csv || $pdf)
    	{
    		$arr = $data;
    		
    		foreach($arr as $key=>$value)
			{
				unset($arr[$key]["PROVIDER_ID"]);
				$arr[$key]["CHARGES_SUGGESTED"] = Application_Helper_General::convertDate($value["CHARGES_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
	    	if($csv)
			{
				Application_Helper_General::writeLog('VWBH','Download CSV Biller Charges');
				$this->_helper->download->csv(array('Biller Code','Biller Name','Biller Type','Suggested Date', 'Suggester'),$arr,null,'Biller Charges');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('VWBH','Download PDF Biller Charges');
				$this->_helper->download->pdf(array('Biller Code','Biller Name','Biller Type' , 'Suggested Date', 'Suggester'),$arr,null,'Biller Charges');
			}
			
			
    	}else if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Biller Charges', 'data_header' => $fields));
			}
    	else
    	{
    		Application_Helper_General::writeLog('VWBH','View Biller Charges');
    	}

    	unset($dataParamValue['SUGEST_DATE_END']);
    	// unset($dataParamValue['APPROVE_DATE_END']);
    	if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}