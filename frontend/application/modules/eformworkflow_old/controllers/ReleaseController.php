<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_ReleaseController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg Number / Subject'),
      ),
      'obligee_name'     => array(
        'field'    => 'BG_SUBJECT',
        'label'    => $this->language->_('Obligee Name'),
      ),
      'startdate' => array(
        'field' => 'TIME_PERIOD_START',
        'label' => $this->language->_('Start Date'),
      ),
      'enddate'   => array(
        'field'    => 'TIME_PERIOD_END',
        'label'    => $this->language->_('End Date'),
      ),
      'bank_branch'   => array(
        'field'    => 'BRANCH_NAME',
        'label'    => $this->language->_('Bank Branch'),
      ),
      'counter_type'   => array(
        'field'    => 'BG_FORMAT',
        'label'    => $this->language->_('Counter Type'),
      ),
      'bgamount'  => array(
        'field'    => 'BG_AMOUNT',
        'label'    => $this->language->_('BG Amount')
      ),
      'type' => array(
        'field' => 'TIME_PERIOD_START',
        'label' => $this->language->_('Type'),
      ),
    );

    $bgstats = array('3'); // waiting release
    $selectbg = $this->_db->select()
    ->from(
      array('A' => 'TEMP_BANK_GUARANTEE'),
        // array('REG_NUMBER'=>'BG_REG_NUMBER',
        //         'SUBJECT'=>'BG_SUBJECT',
        //         'CREATED'=>'BG_CREATED', 
        //         'TIME_PERIOD_START',
        //         'TIME_PERIOD_END',
        //         'CREATEDBY'=>'BG_CREATEDBY',
        //         'AMOUNT'=>'BG_AMOUNT',
      array(
        'REG_NUMBER'                    => 'BG_REG_NUMBER',
        'SUBJECT'                       => 'BG_SUBJECT',
        'RECIPIENT_NAME'                => 'RECIPIENT_NAME',
        'TIME_PERIOD_START'             => 'TIME_PERIOD_START',
        'TIME_PERIOD_END'               => 'TIME_PERIOD_END',
        'COUNTER_WARRANTY_TYPE'         => 'COUNTER_WARRANTY_TYPE',
        'COUNTER_WARRANTY_ACCT_NO'      => 'COUNTER_WARRANTY_ACCT_NO',
        'COUNTER_WARRANTY_ACCT_NAME'    => 'COUNTER_WARRANTY_ACCT_NAME',
        'IS_AMENDMENT'                  => 'CHANGE_TYPE',
        'AMOUNT'                        => 'BG_AMOUNT',
        'FULLNAME'                      => 'T.USER_FULLNAME',
        'BRANCH_NAME'                   => 'C.BRANCH_NAME',

      )
    )
    ->join(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID')
    ->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE')
    ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
    ->where('A.BG_STATUS IN (?)', $bgstats)
    ->order('A.BG_CREATED DESC');

    $selectlc = $this->_db->select()
    ->from(array('A' => 'T_LC'), array(
      'REG_NUMBER' => 'LC_REG_NUMBER',
      'SUBJECT' => 'LC_CREDIT_TYPE',
      'CREATED' => 'LC_CREATED',
      'CCYID' => 'LC_CCY',
        //'TYPE' => (string)'TYPE',
      'TIME_PERIOD_END' => 'LC_EXPDATE',
      'CREATEDBY' => 'LC_CREATEDBY',
      'AMOUNT' => 'LC_AMOUNT',
      'FULLNAME' => 'T.USER_FULLNAME'
    ))
    ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
    ->where('A.LC_CUST =' . $this->_db->quote((string)$this->_custIdLogin))
    ->where('A.LC_STATUS = 3')
    ->order('A.LC_CREATED DESC');

    // filter ----------------------------------------------------------

    $filterlist = array('Reg Number'=>'REG_NUMBER', 'Subject'=>'BG_SUBJECT', 'Obligee Name' => 'OBLIGEE_NAME', 'Bank Cabang'=>'BRANCH_NAME','Counter Type' => 'COUNTER_TYPE');

    $this->view->filterlist = $filterlist;
	
	$filterArr = array(
      'REG_NUMBER'  => array('StringTrim', 'StripTags'),
      'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
      'OBLIGEE_NAME'  => array('StringTrim', 'StripTags'),
      'BRANCH_NAME'   => array('StringTrim', 'StripTags'),
      'COUNTER_TYPE'   => array('StringTrim', 'StripTags')
    );

    $dataParam = array('REG_NUMBER', 'BG_SUBJECT', 'OBLIGEE_NAME','BRANCH_NAME','COUNTER_TYPE');

    $dataParamValue = array();

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {

            if (empty($dataParamValue[$dtParam])) {
              $dataParamValue[$dtParam] = [];
            }
            array_push($dataParamValue[$dtParam], $dataval[$key]);
          }
        }
      }
    }

    if($this->_request->getParam('createdate')){
      $dataParamValue['TIME_PERIOD_START'] = $this->_request->getParam('createdate')[0];
      $dataParamValue['TIME_PERIOD_END'] = $this->_request->getParam('createdate')[1];
    }

    $options = array('allowEmpty' => true);
    $validators = array(
      'REG_NUMBER' => array(),
      'BG_SUBJECT' => array(),
      'OBLIGEE_NAME' => array(),
      'BRANCH_NAME' => array(),
      'COUNTER_TYPE'=> array(),
    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

    $fRegNumber = $zf_filter->getEscaped('REG_NUMBER');
    $fSubject = $zf_filter->getEscaped('BG_SUBJECT');
    $fObligateName = $zf_filter->getEscaped('OBLIGEE_NAME');
    $fBranchName = $zf_filter->getEscaped('BRANCH_NAME');
    $fCounterType = $zf_filter->getEscaped('COUNTER_TYPE');

    if ($fRegNumber) {
      $selectbg->where("A.BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $fRegNumber[0] . '%'));
    }
    if ($fSubject) {
      $selectbg->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $fSubject[0] . '%'));
    }
    if ($fObligateName) {
      $selectbg->where("A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $fObligateName[0] . '%'));
    }
    
    if ($fBranchName) {
      $selectbg->where("BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fBranchName[0]) . '%'));
    }
	
    if ($fCounterType) {
      $selectbg->where("A.COUNTER_WARRANTY_TYPE = ?", $fCounterType[0]);
    }

    // -----------------------------------------------------------------

    $selectbg = $this->_db->fetchAll($selectbg);
    $selectlc = $this->_db->fetchAll($selectlc);

    $result = array_merge($selectbg, $selectlc);

    $setting = new Settings();
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $pw_hash = md5($enc_salt . $enc_pass);
    $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
    $sessionNamespace->token   = $rand;
    $this->view->token = $sessionNamespace->token;

    foreach ($result as $key => $value) {
      $get_reg_number = $value["REG_NUMBER"];

      $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
      $encpayreff = urlencode($encrypted_payreff);

      $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
    }

    $this->paging($result);

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];

    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;


    if (!empty($dataParamValue)) {

      $this->view->createdStart = $dataParamValue['TIME_PERIOD_START'];
      $this->view->createdEnd = $dataParamValue['TIME_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {
          foreach ($duparr as $ss => $vs) {
            $wherecol[] = $key;
            $whereval[] = $vs[0];
          }
        } else {
          $wherecol[] = $key;
          $whereval[] = $value[0];
        }
      }

      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }
  }

  public function getcountertypeAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');

    $optHtml = "<option value='' disabled>-- ".$this->language->_('Please Select')." --</option>";

    $arrWarrantyType = array(
      '1' => 'FC',
      '2' => 'LF',
      '3' => 'INS'
    );

    foreach($arrWarrantyType as $key => $row){
      if($tblName==$key){
        $select = 'selected';
      }else{
        $select = '';
      }
      $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
    }

    echo $optHtml;
  }


}
