<?php

require_once 'Zend/Controller/Action.php';

class user_IndexController extends Application_Main
{

  public function indexAction()
  {

  	$this->_moduleDB = strtoupper($this->_moduleID['user']);
  	$cust_id = strtoupper($this->_getParam('cust_id'));

  	$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    // var_dump($cust_id); die;
  	if($cust_id)
  	{
  	  $select = $this->_db->select()
  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'))
  	                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $result = $this->_db->fetchRow($select);
      if($result['CUST_ID'])
      {
      	$this->view->cust_name = $result['CUST_NAME'];
      }
      else{ $cust_id = null; }
  	}

  	if(!$cust_id)
  	{
  	  $error_remark = $this->getErrorRemark('22','Customer ID');
      //insert log
      try
      {
	    $this->_db->beginTransaction();
	    $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
        $this->_db->commit();
	  }
	  catch(Exception $e)
	  {
	    $this->_db->rollBack();
  	    SGO_Helper_GeneralLog::technicalLog($e);
	  }

	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_helper->url->url(array('module'=>'customer','controller'=>'index','action'=>'index')));
  	}

  	$fields = array('userid'       => array('field'    => 'USER_ID',
                                           'label'    => 'User ID',
                                           'sortable' => true),

                    'userstatus'   => array('field'    => 'USER_STATUS',
                                           'label'    => 'Status',
                                           'sortable' => true),

  	                'userislocked'  => array('field'   => 'USER_ISLOCKED',
                                            'label'    => 'User is locked',
                                            'sortable' => true),

  	                'userisemail'   => array('field'   => 'USER_ISEMAIL',
                                            'label'    => 'User is Email',
                                            'sortable' => true),

                    'username'    => array('field'   => 'USER_FULLNAME',
                                          'label'    => 'Name',
                                          'sortable' => true),
                    );

    $page = $this->_getParam('page');
    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    $sortBy = $this->_getParam('sortby');
    $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
	$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

    $filterArr = array('filter' => array('StripTags','StringTrim'),
                       'uid'    => array('StripTags','StringTrim','StringToUpper'),
                       'uname'  => array('StripTags','StringTrim','StringToUpper'),
                       'status' => array('StripTags','StringToUpper')
                      );
    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
    $filter = $zf_filter->getEscaped('filter');
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;

  	$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
	if(count($temp)>1){
      	if($temp[0]=='F' || $temp[0]=='S'){
      		if($temp[0]=='F')
        		$this->view->error = 1;
        	else
        		$this->view->success = 1;
      		$msg = ''; unset($temp[0]);
      		foreach($temp as $value)
      		{
      			if(!is_array($value))
      				$value = array($value);
      			$msg .= $this->view->formErrors($value);
      		}
        	$this->view->user_msg = $msg;
     	}
    }

    if($filter=='Filter')
    {
      $uid     = html_entity_decode($zf_filter->getEscaped('uid'));
      $uname   = html_entity_decode($zf_filter->getEscaped('uname'));
      $status  = $zf_filter->getEscaped('status');

      $haystack_status = array(strtoupper($this->_masteruserStatus['code']['approved']),
                               strtoupper($this->_masteruserStatus['code']['delete']),
                               strtoupper($this->_masteruserStatus['code']['suspended']));
      $status = (Zend_Validate::is($status,'InArray',array('haystack'=>$haystack_status)))? $status : null;

      $this->view->uid = $uid;
      $this->view->uname = $uname;
      $this->view->status = $status;
    }

    if($filter)
    {
	  $select = $this->_db->select()
	 				         ->from(array('u'=>'M_USER'),array('USER_ID','USER_FULLNAME','USER_STATUS','USER_ISLOCKED','USER_ISEMAIL'))
		  				     //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
		  				     ->where('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id));
		  				     //->where('USER_STATUS!=3');
    }else{ $select = array(); }

    if($filter=='Filter')
    {
      if($uid)    $select->where('UPPER(u.USER_ID) LIKE '.$this->_db->quote('%'.$uid.'%'));
      if($uname)  $select->where('UPPER(u.USER_FULLNAME) LIKE '.$this->_db->quote('%'.$uname.'%'));



      if($status) $select->where('u.USER_STATUS='.$status);
    }

    if($filter)$select->order($sortBy.' '.$sortDir);

    $this->paging($select);
    $this->view->fields = $fields;
    $this->view->filter = $filter;

  	$this->view->status_type = $this->_masteruserStatus;
    $this->view->token_type = $this->_masterhasStatus;
  	$this->view->cust_id = $cust_id;
  	$this->view->modulename = $this->_request->getModuleName();



    //insert log
    try
    {
	  $this->_db->beginTransaction();

	  Application_Helper_General::writeLog('CSLS','');

      $this->_db->commit();
	}
	catch(Exception $e)
	{
	  $this->_db->rollBack();
  	  SGO_Helper_GeneralLog::technicalLog($e);
	}

  }


}
