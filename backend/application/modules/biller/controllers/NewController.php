<?php


require_once 'Zend/Controller/Action.php';


class biller_NewController extends Application_Main
{
	public function indexAction() 
	{
		// min max untuk provider Code and Name Length
  		$min = 4;
		$max = 30;
		$max_billercode = 10;
		
		$model 			 = new biller_Model_Biller();
		$getServiceType 	 = $model->getServiceType();
		if(!empty($getServiceType))
		{
			foreach($getServiceType as $value)
			{
				$arrProvider[$value['SERVICE_OF']][$value['SERVICE_ID']] = $value['SERVICE_NAME'];
			}
		}
		$arrProviderType = array(1=>'Payment',2=>'Purchase');
		$this->view->arrProviderType = array('0'=> '--- Please Select ---')+$arrProviderType;
		$this->view->arrServiceType = $arrProvider;
		$this->view->providerType = 0;// set default
		
		if($this->_request->isPost())
		{
			$providerName = $this->_getParam('providerName');
			$providerCode = $this->_getParam('providerCode');
			$providerType = $this->_getParam('providerType');

			$filters = array(
							   'providerCode'    	=> array('StripTags','StringTrim','StringToUpper'),
							   'providerName'  	=> array('StripTags','StringTrim'),
							   'providerType'  	=> array('StripTags','StringTrim'),
							  );
			$validators = array(
										'providerName' 	=> array(	
																'NotEmpty',
																//'Alnum',
																array	(
																			'StringLength', 
																			array	(
																						'min' => $min,
																						'max' => $max
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'M_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_NAME'
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'TEMP_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_NAME'
																					)
																		),
																'messages' => array(
																						"Error: Biller Name Can not be empty.",
																						//"Error: Invalid Biller Name.",
																						"Error: Invalid Biller Name length( $min - $max character allowed).",
																						"Error: $providerName is already registered.",
																						"Error: $providerName is already taken."
																					)
															),
										'providerCode' 	=> array(	
																'NotEmpty',
																'Alnum',
																array	(
																			'StringLength', 
																			array	(
																						'min' => $min,
																						'max' => $max_billercode
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'M_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_CODE'
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'TEMP_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_CODE'
																					)
																		),
																'messages' => array(
																						"Error: Biller Code Can not be empty.",
																						"Error: Invalid Biller Code.",
																						"Error: Invalid Biller Code length( $min - $max_billercode character allowed).",
																						"Error: $providerCode is already registered.",
																						"Error: $providerCode is already taken."
																					)
															),
										'providerType' 	=> array(
																'NotEmpty',
																array('InArray',array_keys($arrProviderType)),
																'messages' => array(																																								
																						'Error: Please Select from the List.',
																						'Error: Please Select from the List.'
																					)
															),																																
									);

			if(in_array($providerType,array_keys($arrProviderType)))
			{
				$filters 		+= array('serviceType'.$providerType =>  array('StripTags','StringTrim') );
				$validators += array(
													'serviceType'.$providerType 	=> array(
																															'NotEmpty',
																															array('InArray',array_keys($arrProvider[$providerType])),
																															'messages' => array(																																								
																																					'Error: Please Select from the List.',
																																					'Error: Please Select from the List.'
																																				)
																														)
													);
				
			}

			$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());
			if($zf_filter->isValid())
			{
				$param['PROVIDER_CODE'] 	= html_entity_decode($zf_filter->getEscaped('providerCode'));
				$param['PROVIDER_NAME'] 	= html_entity_decode($zf_filter->getEscaped('providerName'));
				$providerType	= $param['PROVIDER_TYPE'] 	= html_entity_decode($zf_filter->getEscaped('providerType'));
				$param['SERVICE_TYPE'] 	= html_entity_decode($zf_filter->getEscaped('serviceType'.$providerType));
				
				$this->_db->beginTransaction();
				try
				{
					$providerID 	 = $this->_getParam('providerID');
					$providerName 	 = $this->_getParam('providerName');
					  
					$info = 'Service Provider';
					$info2 = 'Add New Biller';
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_SERVICE_PROVIDER','TEMP_SERVICE_PROVIDER',$param['PROVIDER_CODE'],$param['PROVIDER_NAME'],$param['PROVIDER_CODE']);

					$data= $param+array(
																		'CHANGES_ID' 			=> $change_id,
																		'PROVIDER_STATUS' 			=> 1,
																		'PROVIDER_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
																		'PROVIDER_SUGGESTEDBY' 	=> $this->_userIdLogin
																	);
					$model->insertTemp($data);
					Application_Helper_General::writeLog('CBIL','Add New Biller. Provider ID : '.$providerID.'.Provider Name : '.$providerName);
					
					if($error == 0)
					{
						$this->_db->commit();
						$this->setbackURL('/biller');
						$this->_redirect('/notification/submited/index');
					}
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
			else
			{
				$this->view->providerCode = $this->_getParam('providerCode');
				$this->view->providerName = $this->_getParam('providerName');
				$this->view->providerType = $this->_getParam('providerType');
				$serviceID = 'serviceType'.$providerType;
				$this->view->$serviceID = $this->_getParam($serviceID);

				$listMsg = $zf_filter->getMessages();
				foreach($listMsg as $key => $arrMsg)
				{
					foreach($arrMsg as $msg)
					{
						$keyID = $key.'_errMsg';
						$this->view->$keyID = $msg;
					}
				}
			}
		}

    	Application_Helper_General::writeLog('CBIL','View Add Biller');
	}
}