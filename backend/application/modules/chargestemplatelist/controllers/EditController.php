<?php


require_once 'Zend/Controller/Action.php';


class Chargestemplatelist_EditController extends Application_Main
{
    public function indexAction()
	{
		$template_id = $this->_getParam('template_id');
		$mData = $this->_db->select()
			->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'), array('*'))
			->join(array('TCO' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = TCO.TEMPLATE_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
			->where("A.TEMPLATE_ID = ".$this->_db->quote($template_id));
		$arr = $this->_db->fetchAll($mData);
		
		$tempname =  $arr[0]['TEMPLATE_NAME'];
		$tempid = $arr[0]['TEMPLATE_ID'];
		
		$ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$this->view->ccyArr = Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		
		if(is_array($ccyArr))
		{
	        	$ccyArrValidate = Application_Helper_Array::simpleArray($ccyArr,'CCY_ID');
	    }
		
		$chargertgs = $this->_getParam('charge_rtgs');
		$chargeskn 	= $this->_getParam('charge_skn');
			
		$tempchargertgs = Application_Helper_General::convertDisplayMoney($chargertgs);
		$tempchargeskn = Application_Helper_General::convertDisplayMoney($chargeskn);
			
		$chargertgs = str_replace('.','',$tempchargertgs);
		$chargeskn = str_replace('.','',$tempchargeskn);
			
		$this->_setParam('charge_rtgs',$chargertgs);
		$this->_setParam('charge_skn',$chargeskn);
	    
		$this->view->templatedetail = $arr;
		
		$filters = array(
							 'charge_skn_curr' => array('StringTrim','StripTags'),
							 'charge_rtgs_curr'    => array('StringTrim','StripTags'),
							 'charge_rtgs'    => array('StringTrim','StripTags'),
							 'charge_skn'    => array('StringTrim','StripTags')
							);
		 		
			$validators = array(
								'charge_skn_curr' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>3)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 3 chars)'),
			                                                             )
													),					
								'charge_rtgs_curr' => array('NotEmpty',
													  new Zend_Validate_StringLength(array('max'=>3)),
													  'messages' => array(
													                     $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 3 chars)'),
																         )
														),
								'charge_rtgs'      => array('NotEmpty',
													  'Digits',
													  new Zend_Validate_StringLength(array('max'=>15)),
													  'messages' => array(
													                     $this->language->_('Can not be empty'),
													 					 $this->language->_('Entry Must be Numbers'),
																         $this->language->_('Data too long (max 15 chars)'),
																         )
														),
								'charge_skn'      => array('NotEmpty',
													 'Digits',
													  new Zend_Validate_StringLength(array('max'=>15)),
													 'messages' => array(
													                     $this->language->_('Can not be empty'),
													 					 $this->language->_('Entry Must be Numbers'),
																         $this->language->_('Data too long (max 15 chars)'),
																         )
														),
							   );
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			$re = '%^[a-zA-Z0-9+\-]*$%';		
			$cekspecialchar = preg_match($re, $zf_filter_input->template_name)? 1 : 0;
			
			$cektemp = $this->_db->select()
					->from(array('A' => 'TEMP_TEMPLATE_CHARGES_OTHER'), array('*'))
					->where("A.TEMPLATE_NAME = ".$this->_db->quote($tempname));
				$cektemp = $this->_db->fetchAll($cektemp);
		
			if($cektemp)
			{
				$this->view->disabled = 'disabled';
			}
			
			$submit = $this->_getParam('submit');
			
			if($submit && $zf_filter_input->isValid() && !$cektemp && $this->view->hasPrivilege('CTUD'))
			{
				Application_Helper_General::writeLog('CTUD','Editing Charge Template ('.$tempid.')');
				$this->_db->beginTransaction();
				
				try
				{
					if($zf_filter_input->isValid())
					{
			        	$changeInfo= "Edit Template Charge Other";
				    	$changeType = $this->_changeType['code']['edit'];
						$keyField = $tempid;
				    	$keyValue = $tempname;
					}
				
					$masterTable = 'M_TEMPLATE_CHARGES_OTHER,M_TEMPLATE_CHARGES_OTHER_DETAIL';
					$tempTable =  'TEMP_TEMPLATE_CHARGES_OTHER,TEMP_TEMPLATE_CHARGES_OTHER_DETAIL';
				
					$displayTableName = 'Charges Template';
					//var_dump($dataforinsert);exit;					
					$changesId = $this->suggestionWaitingApproval($displayTableName, $changeInfo, $changeType,'',$masterTable,$tempTable,$keyField,$keyValue);
				
	  				$content = array(
							'TEMPLATE_NAME' => $tempname,
	  						'TEMPLATE_ID'	=> $tempid,
							'SUGGESTED' 	=> date("Y-m-d H:i:s"),
							'SUGGESTEDBY' 	=> $this->_userIdLogin,
							'CHANGES_ID' 	=> $changesId,
	  						'ISEDITED'		=> '1',
	  						'STATUS'		=> '1'
						    );
					if ($zf_filter_input->isValid())
					{
				    	//insert into temp
						$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER',$content);
					}
				
			    	$lastIdTemp = $this->_db->fetchOne('select @@identity');
				
					$content2 = array(
				            'TEMP_ID' 		=> 	$lastIdTemp,
							'TEMPLATE_NAME' => 	$tempname,
							'TEMPLATE_ID'	=> 	$tempid,
							'CHARGES_TYPE' 	=> 	1,
							'CHARGES_CCY' 	=>	$zf_filter_input->charge_rtgs_curr,
							'CHARGES_AMT' 	=> 	$tempchargertgs,
							'CHANGES_ID' 	=> 	$changesId
						    );
					
					if ($zf_filter_input->isValid())
					{
				    	$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content2);	
					}

                	$content3 = array(
				            'TEMP_ID' 		=>	$lastIdTemp,
							'TEMPLATE_NAME' => 	$tempname,
                			'TEMPLATE_ID'	=> 	$tempid,
							'CHARGES_TYPE' 	=> 	2,
							'CHARGES_CCY' 	=>	$zf_filter_input->charge_skn_curr,
							'CHARGES_AMT' 	=> 	$tempchargeskn,
							'CHANGES_ID' 	=> 	$changesId
						    );
					if ($zf_filter_input->isValid())
					{
				   	 	$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content3);
					}

					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
					$this->_redirect('/notification/submited/index');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();					
				}	
			}
			
			if($submit && !$zf_filter_input->isValid())
			{
				$this->view->error = true;
				$docErr = $this->language->_("Error in processing form values. Please correct values and re-submit.");
				$this->view->errorMsg = $docErr;
				
				foreach($zf_filter_input->getMessages() as $key=>$err)
				{
					$xxx = 'x'.$key;
					$this->view->$xxx = $this->displayError($err);
					if (!$cekspecialchar)
					{		
						$this->view->xtemplate_name = $this->language->_('Must not contain special characters');
						$this->view->template_name	= '';
					}
				}
			}
		Application_Helper_General::writeLog('CTUD','View Edit Charge Template ('.$tempid.') page');
	}
}