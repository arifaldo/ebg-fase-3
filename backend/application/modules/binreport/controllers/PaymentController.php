<?php

require_once 'Zend/Controller/Action.php';

class binreport_PaymentController extends Application_Main
{


    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newlayout');
        $fields = array(
            'transaction_id' => array(
                'field'    => 'CUST_NAME',
                'label'    => $this->language->_('Transaction ID'),
                'sortable' => true
            ),

            'customer'     => array(
                'field'    => 'CUST_CITY',
                'label'    => $this->language->_('Customer'),
                'sortable' => true
            ),

            'bin'   => array(
                'field'    => 'CUST_STATUS',
                'label'    => $this->language->_('BIN'),
                'sortable' => true
            ),

            'va_number'     => array(
                'field'    => 'BIN_SUGGESTED',
                'label'    => $this->language->_('VA Number'),
                'sortable' => true
            ),

            'va_id'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('VA ID'),
                'sortable' => true
            ),
            'transaction_time'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Transaction Time'),
                'sortable' => true
            ),
            'ccy'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('CCY'),
                'sortable' => true
            ),
            'amount'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Amount'),
                'sortable' => true
            ),
            'channel'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Channel'),
                'sortable' => true
            ),
            'status'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Status'),
                'sortable' => true
            )
        );

        //validasi sort, jika input sort bukan ASC atau DESC
        $sortBy  = $this->_getParam('sortby');
        $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('transaction_time' => array('asc', 'desc')))) ? $sortDir : 'asc';

        $this->view->currentPage = $page;
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        $this->view->fields = $fields;
    }
}
