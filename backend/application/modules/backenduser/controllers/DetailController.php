<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';
require_once 'Crypt/AESMYSQL.php';
class backenduser_DetailController extends customer_Model_Customer
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password));

		$suspend = $this->_getParam('suspend');
		$unsuspend = $this->_getParam('unsuspend');
		$delete = $this->_getParam('delete');
		$forcelogout = $this->_getParam('forcelogout');

		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']), array_values($this->_masterglobalstatus['desc']));
		$yesno = array_combine(array_values($this->_masterglobalauto['code']), array_values($this->_masterglobalauto['desc']));

		$hide = false;
		if ($user_id == $this->_userIdLogin) {
			$this->view->hide = true;
			$hide = true;
		}

		if ($user_id) {

			$caseStatus = "(CASE A.BUSER_STATUS ";
			foreach ($statusarr as $key => $val) {
				$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
			}
			$caseStatus .= " END)";

			$select = $this->_db->select()
				->from(array('A' => 'M_BUSER'), [])
				->joinleft(array('C' => 'M_BRANCH'), 'A.BUSER_BRANCH = C.ID', array('*'))
				->joinleft(array('B' => 'M_BGROUP'), 'A.BGROUP_ID = B.BGROUP_ID', array("A.*", 'B.BGROUP_DESC', 'STATUS' => $caseStatus));
			$select->where("A.BUSER_ID LIKE " . $this->_db->quote($user_id));
			$resultdata = $this->_db->fetchRow($select);

			$this->view->result = $resultdata;

			$select2 = $this->_db->select()
				->from(array('TEMP_BUSER'));
			$select2->where("BUSER_ID LIKE " . $this->_db->quote($user_id));
			$result2 = $select2->query()->FetchAll();
			//Zend_Debug::dump($this->_changeType['code']['suspend']);die;



			//modal privil

			$privil = $this->_db->select()
				->FROM('M_BPRIVILEGE')
				->ORDER('BPRIVI_DESC ASC')
				->query()->fetchall();

			$select4 = $this->_db->select()
				->from(array('M_BPRIVI_GROUP'));
			$select4->where("BGROUP_ID LIKE " . $this->_db->quote($resultdata['BGROUP_ID']));
			$result4 = $select4->query()->FetchAll();

			foreach ($privil as $row) {
				// print_r($result4);die;
				foreach ($result4 as $row2) {
					if ($row2['BPRIVI_ID'] == $row['BPRIVI_ID']) {
						$this->view->$row['BPRIVI_ID'] = '1';
						// break;
					}
				}
			}

			// echo"<pre>";
			// print_r($privil);die;

			$this->view->privil = $privil;
			$this->view->prev = $result4;

			//end modal privil




			if (!$result2) {
				if ($suspend && $resultdata['BUSER_STATUS'] == '1' && $resultdata['BUSER_ISLOGIN'] != '1' && !$hide && $this->view->hasPrivilege('ADSP')) {
					$this->_db->beginTransaction();
					try {
						Application_Helper_General::writeLog('ADSP', 'Suspend Backend User (' . $resultdata['BUSER_ID'] . ')');
						$info = "Suspend Backend User";
						$change_id = $this->suggestionWaitingApproval('Backend User', $info, $this->_changeType['code']['suspend'], null, 'M_BUSER', 'TEMP_BUSER', $resultdata['BUSER_ID'], $resultdata['BUSER_NAME']);
						$data = array(
							'CHANGES_ID' 		=> $change_id,
							'BUSER_ID'			=> $resultdata['BUSER_ID'],
							'BUSER_BRANCH'		=> $resultdata['BUSER_BRANCH'],
							'BUSER_NAME' 		=> $resultdata['BUSER_NAME'],
							'BUSER_NIK' 		=> $resultdata['BUSER_NIK'],
							'BGROUP_ID' 		=> $resultdata['BGROUP_ID'],
							'BUSER_DESC' 		=> $resultdata['BUSER_DESC'],
							'BUSER_EMAIL'		=> $resultdata['BUSER_EMAIL'],
							'CREATED'			=> $resultdata['CREATED'],
							'CREATEDBY'			=> $resultdata['CREATEDBY'],
							'BUSER_STATUS'		=> '2'
						);
						$this->_db->insert('TEMP_BUSER', $data);
						$temp = 1;
					} catch (Exception $e) {
						$temp = 0;
						$this->_db->rollBack();
					}

					if ($temp == 1) {
						$this->_db->commit();
						$this->setbackURL('/' . $this->_request->getModuleName() . '/index/index/');
						$this->_redirect('/notification/submited/index');
					}
					if ($temp == 0) {
						$this->_db->rollBack();
					}
				}
				//Zend_Debug::dump($resultdata[0]);die;
				if ($unsuspend && $resultdata['BUSER_STATUS'] == '2' && $resultdata['BUSER_ISLOGIN'] != '1' && !$hide && $this->view->hasPrivilege('ADSP')) {

					$this->_db->beginTransaction();
					try {
						Application_Helper_General::writeLog('ADSP', 'Unsuspend Backend User (' . $resultdata['BUSER_ID'] . ')');
						$info = "Unsuspend Backend User";
						$change_id = $this->suggestionWaitingApproval('Backend User', $info, $this->_changeType['code']['unsuspend'], null, 'M_BUSER', 'TEMP_BUSER', $resultdata['BUSER_ID'], $resultdata['BUSER_NAME']);
						$data = array(
							'CHANGES_ID' 		=> $change_id,
							'BUSER_ID'			=> $resultdata['BUSER_ID'],
							'BUSER_BRANCH'		=> $resultdata['BUSER_BRANCH'],
							'BUSER_NAME' 		=> $resultdata['BUSER_NAME'],
							'BUSER_NIK' 		=> $resultdata['BUSER_NIK'],
							'BGROUP_ID' 		=> $resultdata['BGROUP_ID'],
							'BUSER_DESC' 		=> $resultdata['BUSER_DESC'],
							'BUSER_EMAIL'		=> $resultdata['BUSER_EMAIL'],
							'CREATED'			=> $resultdata['CREATED'],
							'CREATEDBY'			=> $resultdata['CREATEDBY'],
							'BUSER_STATUS'		=> '1'
						);
						$this->_db->insert('TEMP_BUSER', $data);
						$temp = 1;
					} catch (Exception $e) {
						$temp = 0;
						$this->_db->rollBack();
					}

					if ($temp == 1) {
						$this->_db->commit();
						$this->setbackURL('/' . $this->_request->getModuleName() . '/index/index/');
						$this->_redirect('/notification/submited/index');
					}
					if ($temp == 0) {
						$this->_db->rollBack();
					}
				}

				if ($delete && $resultdata['BUSER_ISLOGIN'] != '1' && !$hide && $this->view->hasPrivilege('ADUD')) {
					$this->_db->beginTransaction();
					try {
						Application_Helper_General::writeLog('ADUD', 'Delete Backend User (' . $resultdata['BUSER_ID'] . ')');
						$info = "Delete Backend User";
						$change_id = $this->suggestionWaitingApproval('Backend User', $info, $this->_changeType['code']['delete'], null, 'M_BUSER', 'TEMP_BUSER', $resultdata['BUSER_ID'], $resultdata['BUSER_NAME']);
						$data = array(
							'CHANGES_ID' 		=> $change_id,
							'BUSER_ID'			=> $resultdata['BUSER_ID'],
							'BUSER_BRANCH'		=> $resultdata['BUSER_BRANCH'],
							'BUSER_NAME' 		=> $resultdata['BUSER_NAME'],
							'BGROUP_ID' 		=> $resultdata['BGROUP_ID'],
							'BUSER_DESC' 		=> $resultdata['BUSER_DESC'],
							'BUSER_EMAIL'		=> $resultdata['BUSER_EMAIL'],
							'BUSER_STATUS'		=> '3'
						);
						$this->_db->insert('TEMP_BUSER', $data);
						$temp = 1;
					} catch (Exception $e) {
						$temp = 0;
						$this->_db->rollBack();
					}

					if ($temp == 1) {
						$this->_db->commit();
						$this->setbackURL('/' . $this->_request->getModuleName() . '/index/index/');
						$this->_redirect('/notification/submited/index');
					}
					if ($temp == 0) {
						$this->_db->rollBack();
					}
				}

				if ($forcelogout && $resultdata['BUSER_ISLOGIN'] == '1' && !$hide && $this->view->hasPrivilege('ADFL')) {
					$updateArr['BUSER_ISLOGIN']   = '0';
					$whereArr = array('BUSER_ID = ?' => $user_id);
					$userupdate = $this->_db->update('M_BUSER', $updateArr, $whereArr);
					$this->view->successmsg = '<span class="successmsg">User is Logged Out</span><br /><br />';

					$select = $this->_db->select()
						->from(array('A' => 'M_BUSER'), array())
						->joinleft(array('C' => 'M_BRANCH'), 'A.BUSER_BRANCH = C.ID', array('*'))
						->joinleft(array('B' => 'M_BGROUP'), 'A.BGROUP_ID = B.BGROUP_ID', array('A.*', 'B.BGROUP_DESC', 'STATUS' => $caseStatus));
					$select->where("A.BUSER_ID LIKE " . $this->_db->quote($user_id));
					$resultdata = $this->_db->fetchRow($select);
					$this->view->result = $resultdata;

					Application_Helper_General::writeLog('ADFL', 'Force Backend user Logout');
				}
			}

			if ($resultdata['BUSER_ID']) {
				$this->view->user_id      		= strtoupper($resultdata['BUSER_ID']);
				$this->view->group     			= $resultdata['BGROUP_DESC'];
				$this->view->branch     	    = $resultdata['BRANCH_NAME'];
				$this->view->status    			= $resultdata['STATUS'];
				$this->view->name    			= $resultdata['BUSER_NAME'];
				$this->view->email 				= $resultdata['BUSER_EMAIL'];
				$this->view->nik 				= $resultdata['BUSER_NIK'];
				$this->view->description    	= $resultdata['BUSER_DESC'];;
				$this->view->failed     		= $resultdata['BUSER_FAILEDATTEMPT'];
				$this->view->lockreason     	= $resultdata['BUSER_LOCKREASON'];
				$this->view->userislogoin 		= isset($yesno[$resultdata['BUSER_ISLOGIN']]) ? $yesno[$resultdata['BUSER_ISLOGIN']] : '';
				$this->view->userislocked  		= $yesno[$resultdata['BUSER_ISLOCKED']];
				$this->view->createdate    		= Application_Helper_General::convertDate($resultdata['CREATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$this->view->creator      		= $resultdata['CREATEDBY'];
				$this->view->last_suggestion    = Application_Helper_General::convertDate($resultdata['SUGGESTED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$this->view->last_suggestor    	= $resultdata['SUGGESTEDBY'];
				$this->view->last_approval  	= Application_Helper_General::convertDate($resultdata['UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$this->view->last_approver  	= $resultdata['UPDATEDBY'];
				$this->view->rreset				= $resultdata['BUSER_RRESET'];
				$this->view->login				= $resultdata['BUSER_ISLOGIN'];
			} else $user_id = null;
		} // End if cust_id == true

		$this->view->user_id = $user_id;
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('ADLS', 'View Backend User List Detail [' . $user_id . ']');
		}
		if ($result2) {
			$change = "disabled";
			$this->view->change = $change;
		}
	}
}
