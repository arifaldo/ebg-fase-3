<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';

$config    	= Zend_Registry::get('config');
$bankName    = $config['app']['bankname'];
     $appName     = $config['app']['name'];
     $appBankCode   = $config['app']['bankcode'];

	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	
$template = file_get_contents(LIBRARY_PATH.'/email template/emailreport.html');
$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
// print_r($template);die;
$transFT   = array( '[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
					'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
					'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl													
				  );

$FOOTER_ID = strtr($FOOTER_ID, $transFT);
$FOOTER_EN = strtr($FOOTER_EN, $transFT);

$db = Zend_Db_Table::getDefaultAdapter();

$mailedTo = 'No Email Sent';

// $settings 	= new Settings();


	
// 	SEND FILE STATUS = 0
$data = array();
$data_content = '';
$status = 0;
$counter = 0;




 $data =  $db->fetchAll(
						$db->select()->distinct()
							 ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
							 ->WHERE('A.REPORT_SCHEDULE = ?','weekly')	
							 ->WHERE('A.REPORT_EMAIL != ?','')		
               ->WHERE('A.REPORT_DAY = WEEKDAY(NOW())')
               ->WHERE('HOUR(A.REPORT_TIME) = HOUR(NOW())')    
               // ->where('A.REPORT_EMAIL = ?','benny@sgo.co.id')	
							 ->order('A.ID DESC')
					  );

$receiver = '';
$subject = '';
$tableHeader = $string = '';
// echo $data;die;
foreach ($data as $key => $value) {
	// $listReceiver = $Settings->getSetting('email_exception');
	$receiver = explode(";", $value['REPORT_EMAIL']);
	$subject = 'Scheduler Report '.$value['REPORT_NAME'];
	// print_r($receiver);
	$selectcolomn = $db->select()
                            ->from('T_REPORT_COLOMN', array('COLM_NAME','COLM_FIELD','COLM_TYPE'))
                            ->where('COLM_REPORD_ID = ?', $value['ID']);

        $ColomnList = $db->fetchAll($selectcolomn);

        $CustomerUser   = new CustomerUser($value['REPORT_CUST'], $value['REPORT_CREATEDBY']);
        
        $arrAccount     = $CustomerUser->getAccounts();
        $acc  = Application_Helper_Array::simpleArray($arrAccount, "ACCT_NO");
         $paramPayment = array("WA"              => false,
                              "ACCOUNT_LIST"    => $acc,
                              "_beneLinkage"    => 0,
                             );
        
        // get payment query
        $select   = $CustomerUser->getPayment($paramPayment);
        // $selectdata = $db->select()->from(array('A' => $select),
        //         array(
        //             'A.*'
        //         ));
        // echo $selectdata;die;
                 if(!empty($data[$key]['REPORT_LIMIT'])){
            $select->limit($data[$key]['REPORT_LIMIT']);
        }
        if(!empty($data[$key]['REPORT_WHERE'])){
            $whereArr = explode(';', $data[$key]['REPORT_WHERE']);
            foreach ($whereArr as $key => $value) {
                // print_r($whereArr);die;
                $searchkey = ( explode( ' ', $value ));
                if($searchkey['0'] == 'PS_EFDATE' || $searchkey['0'] == 'PS_CREATED' || $searchkey['0'] == 'PS_UPDATED'){
                    $var = $searchkey['2'];
                    $searchkey['2'] = date("Y-m-d", strtotime($var) );
                    $value = 'DATE('.$searchkey['0'].') '.$searchkey['1'].' "'.$searchkey['2'].'"';
                }
                $select->where($value);
            }
            
        }

        if(!empty($data[$key]['REPORT_SCHEDULE'])){
            // $sortby = $reportdata['REPORT_SORT_DESC'];
            if($data[$key]['REPORT_SCHEDULE']=='monthly'){
                $select->where('MONTH(PS_CREATED) = ?' , 'MONTH(NOW())');
                $select->where('YEAR(PS_CREATED) = ?' , 'YEAR(NOW())');

            }
            // $sortdesdata = explode(',', $reportdata['REPORT_SORT_DESC']);
            // $sortdirdesc = 'DESC';
            //  if(!empty($sortdesdata)){
            //     foreach ($sortdesdata as $key => $value) {
            //             // 4
            //             $select->order($value.' DESC');
            //     }
            // }
        }
        // echo $select;die;
        // $selectdata = $selectdata->order('TRA_AMOUNT DESC');
        // $selectdata = $selectdata->order('PS_NUMBER DESC');

        // $selectdata->order($sortby.' '.$sortdir);
        if(!empty($data[$key]['REPORT_SORT_ASC'])){

            $sortascdata = explode(',', $data[$key]['REPORT_SORT_ASC']);
                    // print_r($sortascdata);die;
            if(!empty($sortascdata)){
                // die;
                foreach ($sortascdata as $key => $value) {
                        // 4
                            // die;
                        $select->order($value.' ASC');
                }
            }
            
        }

        if(!empty($data[$key]['REPORT_SORT_DESC'])){
            // $sortby = $reportdata['REPORT_SORT_DESC'];
            $sortdesdata = explode(',', $data[$key]['REPORT_SORT_DESC']);
            $sortdirdesc = 'DESC';
             if(!empty($sortdesdata)){
                foreach ($sortdesdata as $key => $value) {
                        // 4
                        $select->order($value.' DESC');
                }
            }
        }

        $datareport = $db->fetchAll($select);

$numFields = count($ColomnList);
		$string = '<style>table.blueTable {
  border: 1px solid #59A489;
  background-color: #ECEEED;
  width: 100%;
  text-align: left;
  border-collapse: collapse;
}
table.blueTable td, table.blueTable th {
  border: 1px solid #AAAAAA;
  padding: 3px 2px;
}
table.blueTable tbody td {
  font-size: 13px;
}
table.blueTable tr:nth-child(even) {
  background: #9CFF88;
}
table.blueTable thead {
  background: #1A8E1B;
  background: -moz-linear-gradient(top, #53aa54 0%, #319931 66%, #1A8E1B 100%);
  background: -webkit-linear-gradient(top, #53aa54 0%, #319931 66%, #1A8E1B 100%);
  background: linear-gradient(to bottom, #53aa54 0%, #319931 66%, #1A8E1B 100%);
  border-bottom: 2px solid #444444;
}
table.blueTable thead th {
  font-size: 15px;
  font-weight: bold;
  color: #FFFFFF;
  text-align: center;
  border-left: 2px solid #DBE0CC;
}
table.blueTable thead th:first-child {
  border-left: none;
}

table.blueTable tfoot {
  font-size: 14px;
  font-weight: bold;
  color: #FFFFFF;
  background: #683AF5;
  background: -moz-linear-gradient(top, #8e6bf7 0%, #774df6 66%, #683AF5 100%);
  background: -webkit-linear-gradient(top, #8e6bf7 0%, #774df6 66%, #683AF5 100%);
  background: linear-gradient(to bottom, #8e6bf7 0%, #774df6 66%, #683AF5 100%);
  border-top: 2px solid #444444;
}
table.blueTable tfoot td {
  font-size: 14px;
}
table.blueTable tfoot .links {
  text-align: right;
}
table.blueTable tfoot .links a{
  display: inline-block;
  background: #1C6EA4;
  color: #FFFFFF;
  padding: 2px 8px;
  border-radius: 5px;
}</style>';
		
		foreach ($ColomnList as $key => $value) {
                    $tableHeader  .= '<th data-field="'.$value['COLM_FIELD'].'">'.$value['COLM_NAME'].'</th>';
                }

        foreach ($datareport as $key => $dataRow) {
        	$string .= '<tr>';
        	for ($i=0; $i < $numFields; $i++) { 

                                if($ColomnList[$i]['COLM_FIELD'] == 'PS_STATUS'){
                                    $ColomnList[$i]['COLM_FIELD']  = 'payStatus';
                                }
                                if($ColomnList[$i]['COLM_TYPE']=='1'){
                                    //type text
                                    $string .= '<td>'.$dataRow[$ColomnList[$i]['COLM_FIELD']].'</td>';
                                }elseif($ColomnList[$i]['COLM_TYPE']=='2'){
                                    //type number
                                    // die;
                                    $string .= '<td>'.Application_Helper_General::displayNumber($dataRow[$ColomnList[$i]['COLM_FIELD']]).'</td>';
                                }elseif($ColomnList[$i]['COLM_TYPE']=='3'){
                                    //type date
                                    $string .= '<td>'.Application_Helper_General::convertDate($dataRow[$ColomnList[$i]['COLM_FIELD']],'dd MMMM yyyy','dd MMM yyyy').'</td>';
                                    
                                }elseif($ColomnList[$i]['COLM_TYPE']=='4'){
                                    //type date time
                                    $string .= '<td>'.Application_Helper_General::convertDate($dataRow[$ColomnList[$i]['COLM_FIELD']],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</td>';
                                    
                                }else{
                                    $string .= '<td>'.$dataRow[$ColomnList[$i]['COLM_FIELD']].'</td>';
                                }

                                

                                // $i++;
                                

                 }
                 $string .= '</tr>';



        	# code...
        }
        $string .= '</table>';
        // echo $string;;die;
        // foreach ($ as $key => $value) {
        // 	# code...
        // }




$destination = LIBRARY_PATH.'/data/temp/';
      $htmlTemplate = '<!DOCTYPE html><html>
            <head>
                    <title>Report PDF</title>
              <style type="text/css">
                  body {
                    margin:0px 10px 10px 10px;
                    padding:0;
                    background-color:#ffffff;
                  }

                  th {
                    background-color: #000;
                    font-family: Arial, Helvetica, sans-serif;
                    color:#FFF;
                    font-size:11px;
                    font-weight: bold;
                    padding: 5px; text-align: left;
                    border: solid 1px #A9A9A9;
                  }

                  table, td, select, textarea, input {
                    font-size: 11px;
                    font-family:  Arial, Helvetica, sans-serif;
                  }

            .tbl-headercontent {
              color:#FFF;
              background-color: #000;
              font-weight: bold;
              padding: 4px 4px 4px 4px;
              vertical-align: top;
              font-size: 12px;
            }
          
            .tbl-evencontent {
              padding: 4px 4px 4px 4px;
              vertical-align: top;
              font-size: 10px;
              background-color: #f2f2f2;
              border: 1px solid #A9A9A9
            }
            
            .tbl-oddcontent {
              padding: 4px 4px 4px 4px;
              vertical-align: top;
              font-size: 10px;
              border: 1px solid #A9A9A9
            }
            
            .tableform { 
              border-collapse: collapse;
              border: 2px solid #CCCCCC
            }
                
            thead { display: table-header-group }
            tfoot { display: table-row-group }
            tr { page-break-inside: avoid }
    </style>
  </head>
  
  <body>

            <table border="0" cellpadding="0" cellspacing="0" width="100%">{TABLE_CONTENT}</table>
  </body>
</html>';

//            <div>
//              <table border="0" cellpadding="0" cellspacing="0" width="100%">
//              <tr>
//                <td><img src="file://'.$destination.'../../../frontend/public/images/logo/logo_pdf'.$appBankCode.'.jpg"></td>
//                <td align="center" style="float:center"><strong>'.$bankName.' - '.$appName.'</strong></td>
//                <td align="right" style="float:right"><strong>'.date("d F Y H:i:s").'</strong></td>
//              </tr>
//              </table>  
//            </div>
            
      // $string .= ;
      $tableContent =  '<table class="blueTable" style="widht:90%"><tr>'.$tableHeader.'</tr>'.$string;
      // echo $tableContent;die;

      // print_r($tableContent);die;
      
        $htmlPdf =  str_ireplace('{TABLE_CONTENT}', $tableContent, $htmlTemplate);
      
      // print_r($htmlPdf);die;
      $generateUUID = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) );   
        
    
      $generateName = date('Y-m-d'). '-' .$generateUUID;

          $file = file_put_contents($destination.$generateName.'.html',$htmlPdf);

//          shell_exec('/usr/local/bin/wkhtmltopdf '. 'file://'.$destination.$generateName.'.html '. $destination.$generateName.'.pdf');
//          
//          header('Content-Description: File Transfer');
//          header('Content-Type: application/octet-stream');
//          header('Content-Disposition: attachment; filename="'.$fileName.'.pdf"');
//          header('Content-Transfer-Encoding: binary');
//          header('Expires: 0');
//          header('Cache-Control: must-revalidate');
//          header('Pragma: public');
//          header('Content-Length: ' . filesize($destination.$generateName.'.pdf'));
//
//          readfile($destination.$generateName.'.pdf');
//          
//          @unlink($destination.$generateName.'.html');
//          @unlink($destination.$generateName.'.pdf');
//     }

          
      $fileHeader = '<!DOCTYPE html><html><head><title>Header PDF</title></head><body><div style="margin-top:250px;font-size: 12px;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td valign="middle"><img src="file:///app/permatacms/frontend/public/images/logo/logo_pdf'.$appBankCode.'.jpg"></td>
            <td align="center" valign="middle"><strong>Generate Report Scheduler</strong></td>
            <td align="right" valign="middle"><strong>'.date("d F Y H:i:s").'</strong></td>
          </tr>
        </table>
            </div></body></html>';
      $fileHeader = file_put_contents($destination.$generateName.'-header.html',$fileHeader);
          $webkitOpt = '--footer-font-size 10 --footer-right "Page [page]/[topage]   " --margin-top 30mm --header-spacing 5 '.
            '--header-html file://'.$destination.$generateName.'-header.html';
          $shell = '/bin/wkhtmltopdf '.$webkitOpt.' file://'.$destination.$generateName.'.html '. $destination.$generateName.'.pdf';
          #shell_exec('/usr/local/bin/wkhtmltopdf '.$webkitOpt.' file://'.$destination.$generateName.'.html '. $destination.$generateName.'.pdf');
          shell_exec($shell);
// print_r($temporary);die;
          
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$fileName.'.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($destination.$generateName.'.pdf'));
                // $file = $destination.$generateName.'.pdf';
                readfile($destination.$generateName.'.pdf');
        





if($string != ''){  
  $translate = array( 
    '[[DATA_CONTENT]]' => '',
    '[[FOOTER_ID]]'     => $FOOTER_ID,
    '[[FOOTER_EN]]'     => $FOOTER_EN,
    '[[APP_NAME]]'      => $templateEmailMasterBankAppName,
    '[[BANK_NAME]]'     => $templateEmailMasterBankName,
    '[[APP_URL]]'       => $templateEmailMasterBankAppUrl,
    '[[master_bank_name]]'  => $templateEmailMasterBankName,
    '[[master_bank_fax]]'   => $templateEmailMasterBankFax,
    '[[master_bank_address]]'   => $templateEmailMasterBankAddress,
    '[[master_bank_telp]]'  => $templateEmailMasterBankTelp,    
  );
  
  $mailContent = strtr($template,$translate);
  // echo $mailContent;die;
  if( count ($receiver)>0 ){
    foreach($receiver as $counter => $email){
    
      $name = "ReportScheduler.pdf";
      // $status = Application_Helper_Email::sendEmail($email,$subject,$mailContent);
      $status = Application_Helper_Email::sendEmailAttachement($email,'E-Statement',$mailContent,$generateName.'.pdf',$name);
      echo "\n\n".$status.' Email sent to ';
      @unlink($destination.$generateName.'-header.html');
//        @unlink($destination.$generateName.'.html');
        @unlink($destination.$generateName.'.pdf');
      $mailedTo .= 'Email sent to '.$email.' with status : '.$status.';';
    }
  }
}



	# code...
}
// print_r($data);die;



	
// 	SEND FILE STATUS = 1

	


$filename = basename(__FILE__);
Application_Helper_General::cronLog($filename,$mailedTo,1);

?>