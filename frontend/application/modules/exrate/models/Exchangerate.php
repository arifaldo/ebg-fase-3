<?php
class exrate_Model_Exchangerate
{
 	protected $_db;

    // constructor
 
 	public function __construct() {
	     $config = array(
	       'host'      	=> '192.168.88.28',
	       'username'  	=> 'ib',
	       'password'  	=> '590ib',
	       'dbname'    	=> 'bankmayapada',
	       'port'  		=> '3306'
	     );
	     $this->_db = new Zend_Db_Adapter_Pdo_Mysql($config);
    }
    
	public function getData()
	{
		$result = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('C' => 'GLDCRT'),array('*'))
					//->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
		
	}
	
	public function getDataUpdate()
	{
		$result = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('C' => 'GLDCRT_UPDATED'),array('*'))
					//->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
		
	}
	
	public function getDataInterestRate($tipe = null)
	{
		
		$result = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('C' => 'INTEREST_RATE'),array('*'))
					->where("TIPE = ?",$tipe)
			);
		
		return $result;
		
	}
	
	public function getDataInterestRateOther()
	{		
		$result = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('C' => 'INTEREST_RATE'),array('*'))
					//->where("TIPE <> ".$paramna[TABUNGANKU]. " AND TIPE <> ".$paramna[MYDOLLAR]. " AND TIPE <> ".$paramna[DEPOSITO])
					->order("URUTAN")
			);
		
		return $result;
		
	}
	
	public function getDataInterestRateUpdate()
	{
		$result = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('C' => 'INTEREST_RATE_UPDATED'),array('*'))
					//->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
		
	}
}
    