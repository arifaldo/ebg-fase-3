<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class exceptionmanagementnew_MessageexceptionlistController extends Application_Main
{
	public function indexAction()
	{		
		$paymenttype = $this->_paymenttype;
		$paymenttypecode = $paymenttype['code'];
		$paymenttypedesc = $paymenttype['desc'];
		$this->view->paymenttypecode = array_flip($paymenttypecode);
		$this->view->paymenttypedesc = $paymenttypedesc;
		$this->view->paymenttype = $paymenttype;
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
		
		$approvalstatus = $this->_aprovalstatus;
		$approvalstatuscode = $approvalstatus['code'];
		$approvalstatusdesc = $approvalstatus['desc'];
		$this->view->approvalstatuscode = $approvalstatuscode;
		$this->view->approvalstatusdesc = $approvalstatusdesc;
		$this->view->approvalstatus = $approvalstatus;
		
		$select = $this->_db->select()->distinct()
							->from(array('A' => 'M_CUSTOMER'),array('CUST_ID'))
							->order('CUST_ID ASC')
				 			-> query()->fetchAll();
		$this->view->var=$select;

		$fields = array(
						'release'      		=> array('field' => 'release',
											      'label' => 'Release Date',
											      'sortable' => true),
						'payref'      		=> array('field' => 'payref',
											      'label' => 'Payment Ref#',
											      'sortable' => true),
						'transid'           => array('field' => 'custid',
											      'label' => 'Transaction ID',
											      'sortable' => true),
						'custid'           	=> array('field' => 'custid',
											      'label' => 'Company Code',
											      'sortable' => true),
						'custname'      	=> array('field' => 'custname',
											      'label' => 'Company Name',
											      'sortable' => true),
						// 'community'  		=> array('field' => 'comid',
											      // 'label' => 'Community',
											      // 'sortable' => true),
						// 'member'    		=> array('field' => 'memberid',
											      // 'label' => 'Member',
											      // 'sortable' => true),
						'sourceacct'    	=> array('field' => 'sourceacct',
											      'label' => 'Source Account',
											      'sortable' => true),
						'ccy'    			=> array('field' => 'ccy',
											      'label' => 'CCY / Amount',
											      'sortable' => true),
						);
				      
		
		$page    = $this->_getParam('page');
		
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		// $caseType = "(CASE B.PS_TYPE ";
  		// foreach($typearr as $key=>$val)
  		// {
   			// $caseType .= " WHEN ".$key." THEN '".$val."'";
  		// }
  			// $caseType .= " END)";

  		$select2 = $this->_db->select()->distinct()
					        ->from(array('B' => 'T_PSLIP'),array())
					        ->join(array('C' => 'M_CUSTOMER'), 'B.CUST_ID = C.CUST_ID',array())
					        ->join(array('D' => 'T_TRANSACTION'), 'B.PS_NUMBER = D.PS_NUMBER',array('release'			=>	'B.PS_EFDATE', 
						        																	'payref'			=>	'B.PS_NUMBER',
							        																'transid'			=>	'D.TRANSACTION_ID',
							        																'custid'			=>	'C.CUST_ID',
							        																'custname'			=>	'C.CUST_NAME',
					        																		'sourceacct' 		=>	'D.SOURCE_ACCOUNT',
					        																		'sourcename' 		=>	'D.SOURCE_ACCOUNT_NAME',
					        																		'sourcealname' 		=>	'D.SOURCE_ACCOUNT_ALIAS_NAME',
					        																		'ccy' 				=>	'D.SOURCE_ACCOUNT_CCY',
					        																		'amount' 			=>	'D.TRA_AMOUNT',
							        																'bussiness'			=>	new Zend_Db_Expr("'cashm'")));
		$select2 -> where("D.EFT_STATUS = '9'");
		$select2 -> where("B.PS_TYPE NOT LIKE '1'");
		$select2 -> where("D.TRA_STATUS = '3'");

					        
		$filterArr = array(	'filter' 			=> array('StripTags','StringTrim'),					   	
						   	'paymenref' 		=> array('StripTags','StringTrim','StringToUpper'),
							'transid' 			=> array('StripTags','StringTrim'),
	                       	'custid'    		=> array('StripTags','StringTrim','StringToUpper'),
	                       	'custname'  		=> array('StripTags','StringToUpper'),
							'source' 			=> array('StripTags','StringTrim','StringToUpper'),
							'fDateFrom' 		=> array('StripTags','StringTrim'),
							'fDateTo' 			=> array('StripTags','StringTrim'),
	                      );
	            
	    $validator = array(	'filter' 			=> array(),					   	
						   	'paymenref' 		=> array(),
							'transid' 			=> array(),
	                       	'custid'    		=> array(),
	                       	'custname'  		=> array(),
							'source' 			=> array(),
							'fDateFrom' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
							'fDateTo' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );
	    
	    $zf_filter 		= new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter 		= $zf_filter->getEscaped('filter');
	    $paymenref 		= html_entity_decode($zf_filter->getEscaped('paymenref'));
	    $transid 		= html_entity_decode($zf_filter->getEscaped('transid'));
	    $custid 		= html_entity_decode($zf_filter->getEscaped('custid'));
		$custname 		= html_entity_decode($zf_filter->getEscaped('custname'));
	    $comid 			= html_entity_decode($zf_filter->getEscaped('comid'));
	    $comname 		= html_entity_decode($zf_filter->getEscaped('comname'));
	    $memberid 		= html_entity_decode($zf_filter->getEscaped('memberid'));
	    $membercust 	= html_entity_decode($zf_filter->getEscaped('membercust'));
	    $membercustname = html_entity_decode($zf_filter->getEscaped('membercustname'));
	    $source 		= html_entity_decode($zf_filter->getEscaped('source'));
	    $datefrom 		= html_entity_decode($zf_filter->getEscaped('fDateFrom'));
	    $dateto 		= html_entity_decode($zf_filter->getEscaped('fDateTo'));

		if($filter == null)
		{	
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}
		
	    if($filter == null || $filter =='Set Filter')
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;
			
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
	            }
		
		
		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(B.PS_EFDATE) >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.PS_EFDATE) <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(B.PS_EFDATE) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}
		
		if($filter == 'Set Filter')
		{
			if($paymenref){
	       		$this->view->paymenref = $paymenref;
				$paymenref = '%'.$paymenref.'%';
	       		$select2->where("B.PS_NUMBER LIKE ".$this->_db->quote($paymenref));}
	       		
			if($transid){
	       		$this->view->transid = $transid;
	       		$select2->where("D.TRANSACTION_ID LIKE ".$this->_db->quote($transid));}
	       
			if($custid){
	       		$this->view->custid = $custid;
	       		$select2->where("C.CUST_ID LIKE ".$this->_db->quote($custid));}
	     
			if($custname){
	       		$this->view->custname = $custname;
	       		$select2->where("C.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));}

			if($source){
	       		$this->view->source = $source;
	       		$select2->where("D.SOURCE_ACCOUNT LIKE ".$this->_db->quote($source));}
		}
		$select2->order($sortBy.' '.$sortDir);   
		// Zend_Debug::dump($select2->__tostring());die;
		if($csv || $pdf)
		{
			$arr = $this->_db->fetchAll($select2);
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			
			foreach ($arr as $key => $value)
			{
				$arr[$key]["release"]= Application_Helper_General::convertDate($value["release"],"dd MMM yyyy",$this->view->defaultDateFormat);
				$arr[$key]["ccy"]= $value["ccy"].' '.Application_Helper_General::displayMoney($value["amount"]);

				$arr[$key]["sourceacct"] = $value['sourceacct'].' ['.$value['ccy'].'] - '.$value['sourcename'].' / '.$value['sourcealname'];
				unset($arr[$key]["amount"]);
				unset($arr[$key]["sourcename"]);
				unset($arr[$key]["sourcealname"]);
				unset($arr[$key]["bussiness"]);
			}
			
			if($csv)
			{
				Application_Helper_General::writeLog('EFLS','Download CSV Message Exception List');
				$this->_helper->download->csv($header,$arr,null,'Message Exception List');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('EFLS','Download PDF Message Exception List');
				$this->_helper->download->pdf($header,$arr,null,'Message Exception List');
			}
		}
		else
		{
			Application_Helper_General::writeLog('EFLS','Viewing Message Exception List');
		}

		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($select2);
	}
}
?>
