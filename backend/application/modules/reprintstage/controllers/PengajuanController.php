<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class Reprintstage_PengajuanController extends Application_Main
{
    public function cekAction()
    {
        $this->_helper->layout()->setLayout('newpopup');
        $flashMessanger = $this->_helper->getHelper('FlashMessenger');

        if (count($flashMessanger->getMessages()) > 0) {
            $getFlashMessage = $flashMessanger->getMessages()[0];
            $getError = $getFlashMessage["error"];
            if ($getError) $this->view->error = $getError;

            $getStatus = $getFlashMessage["status"];

            if ($getStatus) $this->view->status = $getStatus;
        }

        if ($this->_request->isPost()) {
            $bg_number = $this->_request->getParam("bg_number");

            $checkBgNumber = $this->_db->select()
                ->from(["A" => "T_BANK_GUARANTEE"])
                ->joinLeft(array('TU' => 'M_BUSER'), 'A.BG_UPDATEDBY = TU.BUSER_ID')
                ->joinLeft(array('BR' => 'M_BRANCH'), 'BR.ID = TU.BUSER_BRANCH', [
                    "BRANCH_RELEASE" => "BR.BRANCH_NAME",
                    "BRANCH_RELEASE_CODE" => "BR.BRANCH_CODE"
                ])
                ->joinLeft(array('BP' => 'M_BRANCH'), 'A.BG_BRANCH_PUBLISHER = BP.BRANCH_CODE', [
                    "BRANCH_PUBLISHER" => "BP.BRANCH_NAME",
                    "BRANCH_PUBLISHER_CODE" => "BP.BRANCH_CODE"
                ])
                ->where("A.BG_NUMBER = ?", $bg_number)
                ->where("A.BG_STATUS IN (?)", ["15", "16"]);
            // ->where("COMPLETION_STATUS = 3");

            $checkBgNumber = $this->_db->fetchRow($checkBgNumber);

            $get_branch_login = $this->_db->select()
                ->from(["A" => "M_BUSER"], [])
                ->joinLeft(["B" => "M_BRANCH"], "A.BUSER_BRANCH = B.ID", ["BUSER_BRANCH_CODE" => "B.BRANCH_CODE"])
                ->where("BUSER_ID = ?", $this->_userIdLogin)
                ->query()->fetch();

            if (empty($checkBgNumber)) {
                $flashMessanger->addMessage(["error" => "BG tidak ditemukan"]);
                return $this->_redirect("/reprintstage/pengajuan/cek");
            }

            if (!empty($checkBgNumber)) {
                if ($checkBgNumber["BG_STATUS"] == 16) {
                    $flashMessanger->addMessage(["error" => "BG sudah selesai penjaminan"]);
                    return $this->_redirect("/reprintstage/pengajuan/cek");
                } else if ($get_branch_login["BUSER_BRANCH_CODE"] != $checkBgNumber["BG_BRANCH"]) {
                    $flashMessanger->addMessage(["error" => "Kantor cabang user tidak sama dengan kantor cabang penerbit BG"]);
                    return $this->_redirect("/reprintstage/pengajuan/cek");
                }
                // else if ($get_branch_login["BUSER_BRANCH_CODE"] != $checkBgNumber["BG_BRANCH"]) {
                //     $flashMessanger->addMessage(["error" => "User tidak diijinkan melakukan usulan cetak ulang BG Kantor Penerbit cabang lain"]);
                //     return $this->_redirect("/reprintstage/pengajuan/cek");
                // }
                else if ($checkBgNumber["BG_PUBLISH"] != 1) {
                    $flashMessanger->addMessage(["error" => "Bentuk penerbitan BG bukan sertifikat"]);
                    return $this->_redirect("/reprintstage/pengajuan/cek");
                } else if ($checkBgNumber['COMPLETION_STATUS'] == 2) {
                    $flashMessanger->addMessage(["error" => "Tidak dapat melakukan Cetak Ulang. Bank Garansi masih dalam Tahap Pencetakkan."]);
                    return $this->_redirect("/reprintstage/pengajuan/cek");
                }
            }

            if (!empty($checkBgNumber["REPRINT"]) && ($checkBgNumber["REPRINT"] == 1 || $checkBgNumber["REPRINT"] == 2)) {
                $flashMessanger->addMessage(["error" => "BG dalam tahapan siap cetak"]);
                return $this->_redirect("/reprintstage/pengajuan/cek");
            }

            $flashMessanger->addMessage(["bg_number" => $bg_number]);
            return $this->_redirect("/reprintstage/pengajuan/detail");
        }
    }

    public function detailAction()
    {
        $this->_helper->layout()->setLayout('newpopup');

        $flashMessanger = $this->_helper->getHelper('FlashMessenger');

        if ($this->_request->isGet()) {

            if (count($flashMessanger->getMessages()) < 1) {
                return $this->_redirect("/reprintstage/pengajuan/cek");
            }

            $bg_number = $flashMessanger->getMessages()[0]["bg_number"];
            $this->view->bg_number = $bg_number;

            $getBgDetail = $this->_db->select()
                ->from(["TBG" => "T_BANK_GUARANTEE"])
                ->joinLeft(["BG_BRANCH" => "M_BRANCH"], "BG_BRANCH.BRANCH_CODE = TBG.BG_BRANCH", [
                    "BG_BRANCH_NAME" => "BG_BRANCH.BRANCH_NAME"
                ])
                ->joinLeft(["PRINSIPAL" => "M_CUSTOMER"], "PRINSIPAL.CUST_ID = TBG.CUST_ID", [
                    "PRINSIPAL" => "PRINSIPAL.CUST_NAME"
                ])
                ->where("BG_NUMBER = ?", $bg_number);

            $getBgDetail = $this->_db->fetchRow($getBgDetail);
            $getBgDetail["BG_NUMBER"] = substr($getBgDetail["BG_NUMBER"], 0, 3) . "-" . substr($getBgDetail["BG_NUMBER"], 3, 6) . "-" . substr($getBgDetail["BG_NUMBER"], 9, 5) . "-" . substr($getBgDetail["BG_NUMBER"], 14);

            $getTabBankDetail = $this->_db->select()
                ->from("T_BANK_GUARANTEE_DETAIL")
                ->where("BG_REG_NUMBER = ?", $getBgDetail["BG_REG_NUMBER"])
                ->query()->fetchAll();

            $getCurrency = array_search("Currency", array_column($getTabBankDetail, "PS_FIELDNAME"));
            $getBgDetail["CURRENCY"] = $getTabBankDetail[$getCurrency]["PS_FIELDVALUE"];

            $this->view->bgDataDetail = $getBgDetail;

            $configCompletion = Zend_Registry::get('config');
            $getCompletionCode = $configCompletion["bg"]["complation"]["code"];
            $getCompletionDesc = $configCompletion["bg"]["complation"]["desc"];

            $getCompletion = array_combine(array_values($getCompletionCode), array_values($getCompletionDesc));

            $this->view->completionStatus = $getCompletion;
        }

        if ($this->_request->isPost()) {
            $bg_number = $this->_request->getParam("bg_number");
            $getBgRegNumber = $this->_db->select()
                ->from("T_BANK_GUARANTEE", "BG_REG_NUMBER")
                ->where("BG_NUMBER = ?", $bg_number);

            $getBgRegNumber = $this->_db->fetchRow($getBgRegNumber);

            $reason = $this->_request->getParam("catatan_cetak_ulang");

            $path = UPLOAD_PATH . '/document/submit/';
            $save_file_name = [];
            foreach ($_FILES as $key => $value) {
                $filename = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $value['name']));
                $path_old_pks = $value['tmp_name'];
                $path_new_pks = $path . $filename;
                move_uploaded_file($path_old_pks, $path_new_pks);
                $save_file_name[$key] = $filename;
            }

            foreach ($save_file_name as $key => $value) {
                $this->_db->insert("T_BANK_GUARANTEE_DETAIL", [
                    "BG_REG_NUMBER" => $getBgRegNumber["BG_REG_NUMBER"],
                    "CUST_ID" => $this->_custIdLogin,
                    "USER_ID" => $this->_userIdLogin,
                    "PS_FIELDNAME" => $key,
                    "PS_FIELDTYPE" => "1",
                    "PS_FIELDVALUE" => $value
                ]);
            }

            $this->_db->insert("T_BANK_GUARANTEE_DETAIL", [
                "BG_REG_NUMBER" => $getBgRegNumber["BG_REG_NUMBER"],
                "CUST_ID" => $this->_custIdLogin,
                "USER_ID" => $this->_userIdLogin,
                "PS_FIELDNAME" => "Catatan Cetak Ulang",
                "PS_FIELDTYPE" => "1",
                "PS_FIELDVALUE" => $reason
            ]);

            $this->_db->update("T_BANK_GUARANTEE", [
                "REPRINT" => 1
            ], [
                "BG_NUMBER = ?" => $bg_number
            ]);

            $flashMessanger->addMessage(["status" => "success"]);
            return $this->_redirect("/reprintstage/pengajuan/cek");
        }
    }
}
