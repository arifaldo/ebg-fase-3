<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class userdailylimit_ViewController extends userdailylimit_Model_Userdailylimit{


  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $user_id = $AESMYSQL->decrypt($this->_getParam('user_id'), $password);
//    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $user_id : null;

    $ccy_id = $AESMYSQL->decrypt($this->_getParam('ccy_id'), $password);
    $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;

    $error_remark = null;

    if($cust_id)
    {
  	  $select = $this->_db->select()
  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'))
  	                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $result = $this->_db->fetchRow($select);
      if(!$result['CUST_ID']){ $cust_id = null; }
      else{ $this->view->cust_name = $result['CUST_NAME']; }
  	}


    if(!$cust_id)
    {
      $error_remark = $this->getErrorRemark('22','Customer ID');
      //insert log
      try
      {
	    $this->_db->beginTransaction();
        $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,null,$error_remark); 
        $this->_db->commit();
	  }
	  catch(Exception $e)
      {
	    $this->_db->rollBack();
  	    SGO_Helper_GeneralLog::technicalLog($e);
	  }

	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark); 
      $this->_redirect($this->view->backURL);
    }


    if($user_id == true && $ccy_id == true)
    {
       $select = $this->_db->select()
                             ->from(array('d'=>'M_DAILYLIMIT'))
                             ->join(array('u'=>'M_USER'),'d.USER_LOGIN=u.USER_ID',array('USER_FULLNAME'))
		  				     ->where('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id));
                             //->where('d.DAILYLIMIT_STATUS!=3');

      $resultdata = $this->_db->fetchRow($select);

      if($resultdata['DAILYLIMIT'])
      {
        $this->view->user_id     = $user_id;
        $this->view->cust_id     = $cust_id;
        $this->view->ccy_id      = $ccy_id;
        $this->view->user_name   = $resultdata['USER_FULLNAME'];
        $this->view->daily_limit =  Application_Helper_General::displayMoney($resultdata['DAILYLIMIT']);
        $this->view->status      =  $resultdata['DAILYLIMIT_STATUS'];


        $this->view->daily_created     = $resultdata['CREATED'];
        $this->view->daily_createdby   = $resultdata['CREATEDBY'];
        $this->view->daily_suggested   = $resultdata['SUGGESTED'];
        $this->view->daily_suggestedby = $resultdata['SUGGESTEDBY'];
        $this->view->daily_updated     = $resultdata['UPDATED'];
        $this->view->daily_updatedby   = $resultdata['UPDATEDBY'];


        //cek apakah ada di temp atau tidak
        $select = $this->_db->select()
                               ->from('TEMP_DAILYLIMIT',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                               ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id));
                               //->where('d.DAILYLIMIT_STATUS!=3');

        $result = $this->_db->fetchOne($select);
        if($result){ $temp = 0; }else{ $temp = 1; }
        $this->view->daily_temp = $temp;

       /* echo $this->view->acct_temp;
        die;*/

      }
      else{ $acct_no = null; }
    }

/*    if(!$user_id)
    {
      $error_remark = $this->getErrorRemark('22','User ID');
  	  //insert log
	  try
	  {
	  	$this->_db->beginTransaction();
	    $fulldesc = 'CUST_ID:'.$cust_id;
	    $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
		$this->_db->commit();
	  }
      catch(Exception $e){
 		$this->_db->rollBack();
		SGO_Helper_GeneralLog::technicalLog($e);
	  }

	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
    }*/

    $this->view->cust_id     = $cust_id;
    $this->view->change_type = $this->_changeType;
    $this->view->status_type = $this->_masterglobalstatus;
    $this->view->login_type  = $this->_masterhasStatus;
    $this->view->modulename  = $this->_request->getModuleName();


    //cek apakah ada di temp atau tidak




    //insert log
	try
	{
	  $this->_db->beginTransaction();

	  Application_Helper_General::writeLog('DLLS','View User Daily Limit Detail [' .$user_id.' ]' );

	  $this->_db->commit();
	}
    catch(Exception $e)
    {
 	  $this->_db->rollBack();
	  SGO_Helper_GeneralLog::technicalLog($e);
	}


  }
}
