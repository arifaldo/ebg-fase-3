<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class bgreport_KlaimController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$bg = $this->_getParam('bg');
		$this->view->bg = $bg;

		$sessionToken = new Zend_Session_Namespace('Tokenenc');
		$token = $sessionToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		// $decryption = urldecode($bg);
		$numb = $AESMYSQL->decrypt($bg, $token);

		$settings = new Settings();
		$conf = Zend_Registry::get('config');

		$this->view->token = $token;
		$this->view->numb = $numb;

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				->join(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID')
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if (!empty($bgdata)) {
				$data = $bgdata['0'];
				$this->view->data = $data;

				$bgdatadetail = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $numb)
					->query()->fetchAll();

				// ----------------------------- Pengajuan Kalim Bank Garansi -----------------------------
				// nomor bg
				$this->view->bgNumber = $data['BG_NUMBER'];

				// bentuk penerbitan
				$bgpublishType = $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode = $conf["bgpublish"]["type"]["code"];
				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
				$this->view->publishDesc = $arrbgpublish[$data['BG_PUBLISH']];

				if ($data['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $data['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();
					$totalKertas = count($getPaperPrint);
					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// subjek
				$this->view->bgSubject = $data['BG_SUBJECT'];

				// obligee
				$this->view->recipientName = $data['RECIPIENT_NAME'];

				// bank cabang penerbit
				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					$this->view->branchName = $selectbranch[0]['BRANCH_NAME'];
				}

				// jenis pemeriksaan
				$dataCheck = [
					'VDK' => 'Verifikasi Dokumen Klaim',
					'PADK' => 'Pemeriksaan Awal Dokumen Klaim'
				];

				if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
					if ($this->view->hasPrivilege('VPCC')) {
						$this->view->typeCheck = $dataCheck['VDK'];
					}
				} else {
					if ($this->view->hasPrivilege('VPNC') && $this->view->hasPrivilege('PADK')) {
						$this->view->typeCheck = $dataCheck['VDK'];
					} else {
						if ($this->view->hasPrivilege('PADK')) {
							$this->view->typeCheck = $dataCheck['PADK'];
						} elseif ($this->view->hasPrivilege('VPNC')) {
							$this->view->typeCheck = $dataCheck['VDK'];
						}
					}
				}

				// BG TYPE
				$bgType         = $conf["bg"]["type"]["desc"];
				$bgCode         = $conf["bg"]["type"]["code"];
				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));

				$this->view->arrbgType = $arrbgType;

				// ----------------------------- Nilai dan Masa Berlaku bank Garansi -----------------------------
				// currency / ccy
				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') $this->view->currency = $value['PS_FIELDVALUE'];
					}
				}
				// nominal bank garansi
				$this->view->bankAmount = $data['BG_AMOUNT'];

				// berlaku sejak
				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				// batas tanggal klaim
				$this->view->claimPeriod = $settings->getSetting('claim_period');
				$this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];

				// ----------------------------- Kontra Garansi -----------------------------
				// jenis kontra garansi
				$bgcgType = $conf["bgcg"]["type"]["desc"];
				$bgcgCode = $conf["bgcg"]["type"]["code"];
				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
				$this->view->counterWarrantyType = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

				// counter warranty type = 2
				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {
						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName = $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
								$insBranchCode =   $value['PS_FIELDVALUE'];
							}
						} else {
							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}

				// counter warranty type = 3
				$this->view->insuranceName = $data["CUST_NAME"];

				if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceName = array_search("Insurance Name", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];
					$getInsuranceName = $bgdatadetail[$getInsuranceName];

					$insurance = $this->_db->select()
						->from('M_CUSTOMER', array('CUST_NAME'))
						->where('CUST_ID = ?', $getInsuranceName['PS_FIELDVALUE'])
						->query()->fetch();

					$this->view->namaAsuransi = $insurance['CUST_NAME'];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();

					$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];

					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $getInsuranceName["PS_FIELDVALUE"];
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
					// end get linefacility
				}

				// ----------------------------- Rekening Tujuan Pembayaran Klaim -----------------------------
				// bank penerima
				$domesticBanks = $this->_db->select()->from(array('A' => 'M_DOMESTIC_BANK_TABLE'), array('*'))->order('BANK_NAME', 'ASC')->query()->fetchAll();
				$this->view->domesticBanks = $domesticBanks;
			}
		}

		// ----------------------------- Kirim -----------------------------
		if ($this->_request->isPost()) {
			$datasession = $this->_request->getParams();

			$AESMYSQL = new Crypt_AESMYSQL();
			$encrypted = $AESMYSQL->encrypt($numb, $token);
			$encrypted_id = urlencode($encrypted);
			$datasession['bg'] = $encrypted_id;

			$datasession['nominal_pembayaran'] = str_replace(',', '', $datasession['nominal_pembayaran']);

			$attachmentDestination = UPLOAD_PATH . '/document/closing/';
			// $attachmentDestination   = UPLOAD_PATH . '/document/bgreport/klaim/';

			$files = $_FILES;
			$jumlahFile = count($files['dokumen_klaim']['name']);

			for ($i = 0; $i < $jumlahFile; $i++) {
				$namaFile = $files['dokumen_klaim']['name'][$i];
				$lokasiTmp = $files['dokumen_klaim']['tmp_name'][$i];

				# kita tambahkan uniqid() agar nama gambar bersifat unik
				$date = date("dmy");
				$time = date("his");
				$namaBaru = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . '_' . $namaFile;

				if ($files['dokumen_klaim']['size'][$i] > 0) {
					$prosesUpload = move_uploaded_file($lokasiTmp, $attachmentDestination . $namaBaru);
					$datasession['lampiran'][$i] = $namaBaru;
				}
			}

			$sessionReportKlaim = new Zend_Session_Namespace('bgreportklaim');
			$sessionReportKlaim->content = $datasession;

			$this->_redirect('/bgreport/klaim/confirm');
		}
	}

	public function confirmAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$settings = new Settings();
		$conf = Zend_Registry::get('config');

		$sessionReportKlaim = new Zend_Session_Namespace('bgreportklaim');
		$dataSession = $sessionReportKlaim->content;

		$numb = $dataSession['bg'];
		$this->view->bgparam = $numb;

		$bg_number = urldecode($numb);
		$sessionToken = new Zend_Session_Namespace('Tokenenc');
		$token = $sessionToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$numb = $AESMYSQL->decrypt($bg_number, $token);

		$this->view->token = $token;
		$this->view->numb = $numb;

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				->join(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID')
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if (!empty($bgdata)) {
				$data = $bgdata['0'];
				$this->view->data = $data;

				$bgdatadetail = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $numb)
					->query()->fetchAll();

				// ----------------------------- Pengajuan Kalim Bank Garansi -----------------------------
				// nomor bg
				$this->view->bgNumber = $data['BG_NUMBER'];

				// bentuk penerbitan
				$bgpublishType = $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode = $conf["bgpublish"]["type"]["code"];
				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
				$this->view->publishDesc = $arrbgpublish[$data['BG_PUBLISH']];

				if ($data['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $data['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();
					$totalKertas = count($getPaperPrint);
					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// subjek
				$this->view->bgSubject = $data['BG_SUBJECT'];

				// obligee
				$this->view->recipientName = $data['RECIPIENT_NAME'];

				// bank cabang penerbit
				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					$this->view->branchName = $selectbranch[0]['BRANCH_NAME'];
				}

				// jenis pemeriksaan
				$this->view->typeCheck = $dataSession['jenis_pemeriksaan'];

				// BG TYPE
				$bgType         = $conf["bg"]["type"]["desc"];
				$bgCode         = $conf["bg"]["type"]["code"];
				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));

				$this->view->arrbgType = $arrbgType;

				// ----------------------------- Nilai dan Masa Berlaku bank Garansi -----------------------------
				// currency / ccy
				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') $this->view->currency = $value['PS_FIELDVALUE'];
					}
				}
				// nominal bank garansi
				$this->view->bankAmount = $data['BG_AMOUNT'];
				$this->view->nominalPembayaran = $dataSession['nominal_pembayaran'];

				// berlaku sejak
				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				// batas tanggal klaim
				$this->view->claimPeriod = $settings->getSetting('claim_period');
				$this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];

				// ----------------------------- Kontra Garansi -----------------------------
				// jenis kontra garansi
				$bgcgType = $conf["bgcg"]["type"]["desc"];
				$bgcgCode = $conf["bgcg"]["type"]["code"];
				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
				$this->view->counterWarrantyType = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

				// counter warranty type = 2
				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {
						if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
								$insBranchCode =   $value['PS_FIELDVALUE'];
							}
						} else {
							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}

				// counter warranty type = 3
				$this->view->insuranceName = $data["CUST_NAME"];

				if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceName = array_search("Insurance Name", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];
					$getInsuranceName = $bgdatadetail[$getInsuranceName];

					$insurance = $this->_db->select()
						->from('M_CUSTOMER', array('CUST_NAME'))
						->where('CUST_ID = ?', $getInsuranceName['PS_FIELDVALUE'])
						->query()->fetch();

					$this->view->namaAsuransi = $insurance['CUST_NAME'];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();
					$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];

					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $getInsuranceName["PS_FIELDVALUE"];
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
					// end get linefacility
				}

				// ----------------------------- Lampiran Dokumen Klaim -----------------------------
				$this->view->pernyataanKlaim = $dataSession['lampiran'][0];
				$this->view->pengantarKlaim = $dataSession['lampiran'][1];

				// ----------------------------- Rekening Tujuan Pembayaran Klaim -----------------------------
				$domesticBanks = $this->_db->select()->from(array('A' => 'M_DOMESTIC_BANK_TABLE'), array('*'))->order('BANK_NAME', 'ASC')->query()->fetchAll();
				foreach ($domesticBanks as $domesticBank) {
					if ($domesticBank['SWIFT_CODE'] == $dataSession['bank_penerima']) $this->view->bankPenerima = $domesticBank['BANK_NAME'];
				}
				$this->view->rekeningPenerima = $dataSession['rekening_penerima'];
				$this->view->namaPenerima = $dataSession['nama_penerima'];
				$this->view->alamatPenerima = $dataSession['alamat_penerima'];
				$this->view->berita = $dataSession['berita'];
			}
		}

		// ----------------------------- Konfirmasi -----------------------------
		if ($this->_request->isPost()) {
			$kalimat = $dataSession['jenis_pemeriksaan'];
			$words = explode(" ", $kalimat);
			$jenisPemeriksaan = implode('', array_map(fn ($w) => $w[0], $words));

			$generateCloseRefNumber = $this->generateKodePenutupan();

			while (true) {
				$cekOnTBGClose = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_CLOSE')
					->where('CLOSE_REF_NUMBER = ?', $generateCloseRefNumber)
					->query()->fetch();

				$cekOnTBG = $this->_db->select()
					->from('T_BANK_GUARANTEE')
					->where('CLOSE_REF_NUMBER = ?', $generateCloseRefNumber)
					->query()->fetch();

				$cekOnSlip = $this->_db->select()
					->from('T_BG_PSLIP')
					->where('CLOSE_REF_NUMBER = ?', $generateCloseRefNumber)
					->query()->fetch();

				if (!$cekOnTBG && !$cekOnSlip && !$cekOnTBGClose) break;

				$generateCloseRefNumber = $this->generateKodePenutupan();
			}

			// (Bila sebelumnya BG Number tersebut terdapat data dalam proses pengajuan Penutupan Permintaan Principal)
			$checkBgClose = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_CLOSE')
				->where('BG_NUMBER = ?', $data['BG_NUMBER'])
				->where('CHANGE_TYPE = 1')
				->query()->fetchAll();

			if (!empty($checkBgClose)) {
				// Maka lakukan juga update data lama (usulan Penutupan Permintaan Principal) pada table TEMP_BANK_GUARANTEE_CLOSE
				$dataClose = [
					'SUGGESTION_STATUS' => '11',
					'LASTUPDATED' => $checkBgClose[0]['LASTUPDATED'],
					'LASTUPDATEDFROM' => '',
					'LASTUPDATEDBY' => 'SYSTEM',
				];
				$where['CLOSE_REF_NUMBER = ?'] = $checkBgClose[0]['CLOSE_REF_NUMBER'];

				$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataClose, $where);

				// Insert juga history untuk data lama (usulan Penutupan Permintaan Principal) ke table T_BANK_GUARANTEE_HISTORY_CLOSE
				$historyClose = [
					'DATE_TIME' => $checkBgClose[0]['DATE_TIME'],
					'CLOSE_REF_NUMBER' => $checkBgClose[0]['CLOSE_REF_NUMBER'],
					'BG_NUMBER' => $data['BG_NUMBER'],
					'CUST_ID' => '',
					'USER_FROM' => 'SYSTEM',
					'USER_LOGIN' => '',
					'BG_REASON' => 'Terdapat Pengajuan Klaim dengan NoRef Pengajuan : ' . $generateCloseRefNumber . ' dari Pengajuan Klaim',
					'HISTORY_STATUS' => 23,
				];

				$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyClose);
			}

			// Insert data ke table TEMP_BANK_GUARANTEE_CLOSE
			if ($jenisPemeriksaan == 'VDK') {
				$suggestionStatus = 5;
				$claimFirstVerified = new Zend_Db_Expr("now()");
			}

			if ($jenisPemeriksaan == 'PADK') {
				$suggestionStatus = 4;
				$claimFirstVerified = '';
			}

			$dataClose = [
				'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
				'BG_NUMBER' => $data['BG_NUMBER'],
				'CHANGE_TYPE' => 3,
				'SUGGESTION_STATUS' => $suggestionStatus,
				'CLAIM_FIRST_VERIFIED' => $claimFirstVerified,
				'LASTUPDATED' => new Zend_Db_Expr("now()"),
				'LASTUPDATEDFROM' => 'BANK',
				'LASTUPDATEDBY' => $this->_userIdLogin,
			];

			$this->_db->insert('TEMP_BANK_GUARANTEE_CLOSE', $dataClose);

			// Insert data Lampiran Dokumen Klaim ke table TEMP_BANK_GUARANTEE_FILE_CLOSE
			$dokPernyataan = [
				"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
				"FILE_NOTE" => 'Surat Pernyataan Klaim Obligee',
				"BG_FILE" => $dataSession['lampiran'][0],
			];

			$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', $dokPernyataan);

			if ($dataSession['lampiran'][1]) {
				$dokPengantar = [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"FILE_NOTE" => 'Surat Pengantar Klaim Obligee',
					"BG_FILE" => $dataSession['lampiran'][1],
				];

				$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', $dokPengantar);
			}

			// Insert data Detail Rekening Tujuan Pembayaran Klaim ke table TEMP_BANK_GUARANTEE_DETAIL_CLOSE
			if (strtolower($dataSession['bank_penerima']) != 'btanidja') { // cek apakah rekening bukan btn (pihak ketiga)
				!empty($dataSession['alamat_penerima']) // cek jika inputan alamat tidak diisi
					? $beneficiaryAddress = $dataSession['alamat_penerima']
					: $beneficiaryAddress = '-';
			} else {
				$beneficiaryAddress = $dataSession['alamat_penerima']; // jika rekening btn: kosong
			}

			$dataFields = [
				[
					'name' => 'Beneficiary is own account',
					'value' => 'T',
				],
				[
					'name' => 'SWIFT CODE',
					'value' => $dataSession['bank_penerima'],
				],
				[
					'name' => 'Beneficiary Account CCY',
					'value' => $dataSession['valuta_rekening_penerima'],
				],
				[
					'name' => 'Beneficiary Account Number',
					'value' => $dataSession['rekening_penerima'],
				],
				[
					'name' => 'Beneficiary Account Name',
					'value' => $dataSession['nama_penerima'],
				],
				[
					'name' => 'Beneficiary Address',
					'value' => $beneficiaryAddress,
				],
				[
					'name' => 'Transaction CCY',
					'value' => $dataSession['valuta_transaksi'],
				],
				[
					'name' => 'Transaction Amount',
					'value' => $dataSession['nominal_pembayaran'],
				],
				[
					'name' => 'Remark 1',
					'value' => $dataSession['berita'] ?: '',
				],
			];

			foreach ($dataFields as $key => $dataField) {
				$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"ACCT_INDEX" => 1,
					"PS_FIELDNAME" => $dataField['name'],
					"PS_FIELDVALUE" => $dataField['value'],
				]);
			}

			// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
			if ($jenisPemeriksaan == 'VDK') $historyStatus = 21;
			if ($jenisPemeriksaan == 'PADK') $historyStatus = 22;

			$historyClose = [
				'DATE_TIME' => new Zend_Db_Expr("now()"),
				'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
				'BG_NUMBER' => $data['BG_NUMBER'],
				'CUST_ID' => '-',
				'USER_FROM' => 'BANK',
				'USER_LOGIN' => $this->_userIdLogin,
				'BG_REASON' => 'NoRef Pengajuan : ' . $generateCloseRefNumber . '',
				'HISTORY_STATUS' => $historyStatus,
			];

			$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyClose);

			// Simpan log aktifitas user ke table T_BACTIVITY
			if ($jenisPemeriksaan == 'VDK') {
				if ($this->view->hasPrivilege('VPCC')) Application_Helper_General::writeLog('VPCC', "Verifikasi Dokumen Klaim untuk BG No. " . $data['BG_NUMBER'] . ", Principal : " . $data['CUST_ID'] . ", NoRef Pengajuan : " . $generateCloseRefNumber);
				if ($this->view->hasPrivilege('VPNC')) Application_Helper_General::writeLog('VPNC', "Verifikasi Dokumen Klaim untuk BG No. " . $data['BG_NUMBER'] . ", Principal : " . $data['CUST_ID'] . ", NoRef Pengajuan : " . $generateCloseRefNumber);
			}

			if ($jenisPemeriksaan == 'PADK') {
				if ($this->view->hasPrivilege('PADK')) Application_Helper_General::writeLog('PADK', "Pemeriksaan Awal Dokumen Klaim untuk BG No. " . $data['BG_NUMBER'] . ", Principal : " . $data['CUST_ID'] . ", NoRef Pengajuan : " . $generateCloseRefNumber);
			}

			// ----------------------------- Email notification -----------------------------
			$bgClose = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_CLOSE')
				->where('BG_NUMBER = ?', $data['BG_NUMBER'])
				->query()->fetch();

			$bgclosingType = $conf["bgclosing"]["changetype"]["desc"];
			$bgclosingCode = $conf["bgclosing"]["changetype"]["code"];
			$arrbgclosingType = array_combine(array_values($bgclosingCode), array_values($bgclosingType));

			$tipeUsulan = $arrbgclosingType[$bgClose['CHANGE_TYPE']];

			$bgclosingStatus = $conf["bgclosing"]["status"]["desc"];
			$bgclosingstatusCode = $conf["bgclosing"]["status"]["code"];
			$arrbgclosingStatus = array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));

			$statusUsulan = $arrbgclosingStatus[$bgClose['SUGGESTION_STATUS']];

			$mSetting = $settings->getAllSetting();
			$contractName = $this->handleDokumenUnderlying($numb);

			$dataEmail = [
				'[[title_change_type]]' => 'KLAIM',
				'[[close_ref_number]]' => $generateCloseRefNumber,
				'[[bg_number]]' => $data["BG_NUMBER"],
				'[[contract_name]]' => $contractName,
				'[[usage_purpose]]' => $data["USAGE_PURPOSE_DESC"],
				'[[cust_cp]]' => $data['CUST_CP'],
				'[[cust_name]]' => $data["CUST_NAME"],
				'[[recipient_cp]]' => $data['RECIPIENT_CP'],
				'[[recipient_name]]' => $data['RECIPIENT_NAME'],
				'[[bg_amount]]' => $this->view->currency . ' ' . number_format($data["BG_AMOUNT"], 2),
				'[[counter_warranty_type]]' => $arrbgcg[$data['COUNTER_WARRANTY_TYPE']],
				'[[change_type]]' => $tipeUsulan,
				'[[suggestion_status]]' => $arrbgclosingStatus[5],
				'[[repair_notes]]' => '-',
				'[[transaction_amount]]' => $dataSession['valuta_rekening_penerima'] . ' ' . number_format($dataSession["nominal_pembayaran"], 2),
				'[[bank_name]]' => $this->view->bankPenerima,
				'[[beneficiary_account_number]]' => $dataSession['rekening_penerima'],
				'[[beneficiary_account_name]]' => $dataSession['nama_penerima'],
				'[[master_bank_email]]' => $mSetting["master_bank_email"],
				'[[master_bank_telp]]' => $mSetting["master_bank_telp"],
				'[[master_app_name]]' => $mSetting["master_bank_app_name"],
				'[[master_bank_name]]' => $mSetting["master_bank_name"],
			];

			if ($jenisPemeriksaan == 'VDK') {
				// ----- Notifikasi email ke Reviewer - Backend BUSER -----
				$subjectToReviewer = $arrbgclosingStatus[5] . ' BG NO. ' . $data["BG_NUMBER"];
				$emailTemplateToReviewer = $mSetting['bemailtemplate_6A'];

				try {
					$data['COUNTER_WARRANTY_TYPE'] == '1'
						? $bprivi = ['RPCC'] // cash collateral
						: $bprivi = ['RPNC']; // non cash collateral

					$busers = $this->buserEmail($bprivi, $data['BG_BRANCH']);

					if (!empty($busers)) {
						foreach ($busers as $buser) {
							$emailContentToReviewer = strtr($emailTemplateToReviewer, $dataEmail);
							Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], $subjectToReviewer, $emailContentToReviewer);
						}
					}

					// ----- Notifikasi email ke Nasabah/Prinsipal -----
					$subjectToPrinciple = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
					$emailTemplateToPrinciple = $mSetting['bemailtemplate_4A'];

					$emailContentToPrinciple = strtr($emailTemplateToPrinciple, $dataEmail);
					Application_Helper_Email::sendEmail($data['CUST_EMAIL'], $subjectToPrinciple, $emailContentToPrinciple);

					// ----- Notifikasi email ke Obligee -----
					$subjectToObligee = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
					$emailTemplateToObligee = $mSetting['bemailtemplate_4B'];

					$emailContentToObligee = strtr($emailTemplateToObligee, $dataEmail);
					Application_Helper_Email::sendEmail($data['RECIPIENT_EMAIL'], $subjectToObligee, $emailContentToObligee);

					// ----- Notifikasi email ke Asuransi -----
					if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
						$subjectToObligee = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
						$emailTemplateToObligee = $mSetting['bemailtemplate_4C'];
						
						$branchFullName = $this->_db->select()
							->from('M_CUSTOMER')
							->where('CUST_ID = ?', $insuranceBranch[0]["CUST_ID"])
							->query()->fetchAll();

						$dataEmailInsurance = array_merge($dataEmail, [
							'[[insurance_name]]' => $branchFullName[0]['CUST_NAME'],
							'[[insurance_branch_name]]' => $insuranceBranch[0]["INS_BRANCH_NAME"],
						]);

						$emailContentToObligee = strtr($emailTemplateToObligee, $dataEmailInsurance);
						Application_Helper_Email::sendEmail($insuranceBranch[0]["INS_BRANCH_EMAIL"], $subjectToObligee, $emailContentToObligee);
					}

					// ----- Notifikasi email ke Kacab Penerbitan -----
					$subjectToBranch = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
					$emailTemplateToBranch = $mSetting['bemailtemplate_4D'];

					$emailContentToBranch = strtr($emailTemplateToBranch, $dataEmail);
					Application_Helper_Email::sendEmail($selectbranch[0]['BRANCH_EMAIL'], $subjectToBranch, $emailContentToBranch);

					// ----- Notifikasi email ke Group  -----
					switch (true) {
						case $bgdata['COUNTER_WARRANTY_TYPE'] == '1':
							// SEND EMAIL TO GROUP CASH COL
							if ($mSetting['email_group_cashcoll']) Application_Helper_Email::sendEmail($mSetting['email_group_cashcoll'], $subjectToBranch, $emailContentToBranch);
							break;

						case $bgdata['COUNTER_WARRANTY_TYPE'] == '2':
							// SEND EMAIL TO GROUP LINE FACILITY
							if ($mSetting['email_group_linefacility']) Application_Helper_Email::sendEmail($mSetting['email_group_linefacility'], $subjectToBranch, $emailContentToBranch);
							break;

						case $bgdata['COUNTER_WARRANTY_TYPE'] == '3':
							// SEND EMAIL TO GROUP ASURANSI
							if ($mSetting['email_group_insurance']) Application_Helper_Email::sendEmail($mSetting['email_group_insurance'], $subjectToBranch, $emailContentToBranch);
							break;

						default:
							break;
					}

				} catch (\Throwable $th) {
					//throw $th;
				}
			} else {
				// ----- Notifikasi email ke Verifikator (petugas BTS Cabang yang Cover Area) -----
				$subjectToBts = $arrbgclosingStatus[5] . ' BG NO. ' . $data["BG_NUMBER"];
				$emailTemplateToBts = $mSetting['bemailtemplate_6A'];

				try {
					$bpriviGroupBts = $this->_db->select()
						->from('M_BPRIVI_GROUP')
						->where('BPRIVI_ID IN (?)', ['VPNC'])
						->query()->fetchAll();

					$getBranchCoverage = $this->_db->select()
						->from('M_BRANCH_COVERAGE', ['BRANCH_COVERAGE_CODE', 'BRANCH_CODE'])
						->where('BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();

					if (!empty($bpriviGroupBts)) {
						$busersBts = $this->_db->select()
							->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
							->join(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', ['BRANCH_EMAIL'])
							->where('MB.BGROUP_ID IN (?)', array_column($bpriviGroupBts, 'BGROUP_ID'))
							->where('MBR.BRANCH_CODE IN (?)', array_column($getBranchCoverage, 'BRANCH_COVERAGE_CODE'))
							->query()->fetchAll();

						if (!empty($busersBts)) {
							foreach ($busersBts as $buserBts) {
								$emailContentToBts = strtr($emailTemplateToBts, $dataEmail);
								Application_Helper_Email::sendEmail($buserBts['BUSER_EMAIL'], $subjectToBts, $emailContentToBts);
							}
						}
					}
				} catch (\Throwable $th) {
					//throw $th;
				}
			}
			// ----------------------------- End Email notification -----------------------------

			unset($sessionReportKlaim->content);

			$this->setbackURL('/' . $this->_request->getModuleName());
			return $this->_redirect('/notification/success');
		}
	}

	function handleDokumenUnderlying($bgRegNumber) {
		$dokumenUnderlying = $this->_db->select()
			->from('T_BANK_GUARANTEE_UNDERLYING')
			->where('BG_REG_NUMBER = ?', $bgRegNumber)
			->query()->fetchAll();
		
		if (empty($dokumenUnderlying)) {
			return '-';
		}
		
		$docName = array_column($dokumenUnderlying, 'DOC_NAME');
		$combinedDocNames = implode(', ', $docName);

		if (strlen($combinedDocNames) > 90) {
			$combinedDocNames = substr($combinedDocNames, 0, 90) . '...';
		}

		$combinedDocNames .= ' (total ' . count($dokumenUnderlying) . ' dokumen)';
		return $combinedDocNames;
	}

	function buserEmail($bprivi, $bgBranch) {
		$bpriviGroup = $this->_db->select()
			->from('M_BPRIVI_GROUP')
			->where('BPRIVI_ID IN (?)', $bprivi)
			->query()->fetchAll();

		$bpriviGroups = array_column($bpriviGroup, 'BGROUP_ID');

		$busers = $this->_db->select()
			->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
			->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
			->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
			->where('MB.BGROUP_ID IN (?)', $bpriviGroups)
			->where('MBR.BRANCH_CODE = ?', $bgBranch)
			->query()->fetchAll();
		
		return $busers;
	}

	function generateKodePenutupan()
	{
		$kode_penutupan = "DK";
		$tanggal = date("ymd");
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$characters_length = strlen($characters);
		$random_string = '';
		for ($i = 0; $i < 4; $i++) {
			$random_string .= $characters[rand(0, $characters_length - 1)];
		}
		$kode = $kode_penutupan . $tanggal . $random_string;
		return $kode;
	}

	public function inquiryaccountbalanceAction()
	{
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');


		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccountBalance('AB', TRUE);
		$info = $svcAccount->inquiryAccontInfo("AB");

		if ($result["status"] != 1 && $result["status"] != 4) {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return 0;
		}

		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
		$filterBy = $result['account_number']; // or Finance etc.
		$check_prod_type = $this->_db->select()
			->from("M_PRODUCT_TYPE")
			->where("PRODUCT_CODE = ?", $result["account_type"])
			->query()->fetch();

		$result["check_product_type"] = 0;
		if (!empty($check_prod_type)) {
			$result["check_product_type"] = 1;
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($result);
		die();

		$data['data'] = false;
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($result);
	}

	public function inquirydepositoAction()
	{
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		$bg_reg_number = $this->_getParam('bg_reg_number');
		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryDeposito('AB', TRUE);

		$info = $svcAccount->inquiryAccontInfo("AB");

		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
		$result2 = $svcAccountCIF->inquiryCIFAccount();

		if ($result["response_code"] != "0000") {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return 0;
		}

		if ($result["status"] != 1 && $result["status"] != 4) {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return 0;
		}

		$sqlRekeningJaminanExist = $this->_db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
			->where('A.ACCT = ?', $result['account_number'])
			->query()->fetchAll();

		if ($bg_reg_number && $sqlRekeningJaminanExist) {
			foreach ($sqlRekeningJaminanExist as $key => $value) {
				if ($value["BG_REG_NUMBER"] == $bg_reg_number) {
					$result["check_exist_split"] = 0;
				} else {
					$result["check_exist_split"] = 1;
				}
			}
		} else {
			$result["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;
		}

		$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $info['cif']);
		$result2 = $svcAccountCIF->inquiryCIFAccount();
		$filterBy = $result['account_number']; // or Finance etc.
		$check_prod_type = $this->_db->select()
			->from("M_PRODUCT_TYPE")
			->where("PRODUCT_CODE = ?", $result["account_type"])
			->query()->fetch();

		$result["check_product_type"] = 0;
		if (!empty($check_prod_type)) {
			$result["check_product_type"] = 1;
		}

		$data['data'] = false;
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($result);
	}

	public function inquiryaccountinfoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		$swiftcode = $this->_request->getParam('swiftcode');
		$acct_name = $this->_request->getParam('acct_name');

		if (strtolower($swiftcode) == 'btanidja') {
			if ($result['response_desc'] == 'Success') //00 = success
			{
				$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
				$result2 = $svcAccountCIF->inquiryCIFAccount();

				$filterBy = $result['account_number']; // or Finance etc.
				$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
					return ($var['account_number'] == $filterBy);
				});

				$singleArr = array();
				foreach ($new as $key => $val) {
					$singleArr = $val;
				}
			} else {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($result);
				return 0;
			}

			if ($singleArr['type_desc'] == 'Deposito') {
				$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountDeposito->inquiryDeposito();

				if ($result3["response_code"] != "0000") {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				$sqlRekeningJaminanExist = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
					->where('A.ACCT = ?', $result['account_number'])
					->query()->fetchAll();

				$result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;

				$get_product_type = current($new)["type"];

				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			} else {
				$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountBalance->inquiryAccountBalance();

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode($result3);
					return 0;
				}

				$get_product_type = current($new)["type"];
				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			}

			$return = array_merge($result, $singleArr, $result3);

			$data['data'] = false;
			is_array($return) ? $return :  $return = $data;

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($return);
		} else {
			$clientUser  =  new SGO_Soap_ClientUser();

			$getInfoBank = $this->_db->select()
				->from('M_DOMESTIC_BANK_TABLE')
				->where('SWIFT_CODE = ?', $swiftcode)
				->query()->fetch();

			$request = [
				'account_number' => $acct_no,
				'bank_code' => $getInfoBank['BANK_CODE']
			];

			$clientUser->callapi('otherbank', NULL, $request);
			$responseService = $clientUser->getResult();

			$result = [
				'valid' => true,
				'account_name' => $responseService['account_name']
			];

			if ($responseService['response_code'] != '0000') {
				$result = [
					'valid' => false,
					'message' => $responseService['response_desc']
				];
			}

			if (strtolower($responseService['account_name']) !== strtolower($acct_name)) {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Nama dan Nomor Rekening tidak sesuai')
				];
			}

			$result = array_merge($result, array_intersect_key($responseService, ['response_code' => '']));
			echo json_encode($result);
			return;
		}
	}
}
