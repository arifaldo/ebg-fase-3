<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
class userdailylimit_DeleteController extends userdailylimit_Model_Userdailylimit 
{

  public function indexAction() 
  {
   
  	 $cust_id = strtoupper($this->_getParam('cust_id'));
     $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
     $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;  


    $AESMYSQL = new Crypt_AESMYSQL();
    $PS_NUMBER      = urldecode($this->_getParam('user_id'));
    $user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    $user_id = strtoupper($user_id);
//     $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $user_id : null;
    
     $ccy_id = strtoupper($this->_getParam('ccy_id'));
     $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
     
     $error_remark = null;
    
    
  
     
    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_STATUS','CUST_ID'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $cust_data = $this->_db->fetchRow($select);
      if(!$cust_data['CUST_ID'])$cust_id = null;
    }
    
    
    if(!$cust_id)
    { 
      $error_remark = 'Customer ID is not found.';
      Application_Helper_General::writeLog('UDLU','Update User Daily Limit');
        
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }
    
   
    
    
    
    if($user_id && $ccy_id)
    {
      $select = $this->_db->select()
                             ->from(array('d'=>'M_DAILYLIMIT'))
                             ->join(array('u'=>'M_USER'),'d.USER_LOGIN=u.USER_ID and d.CUST_ID=u.CUST_ID',array('USER_FULLNAME'))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id))
                             ->where('d.DAILYLIMIT_STATUS!=3');
      $dailyLimitData = $this->_db->fetchRow($select);
      
     
      if($dailyLimitData['DAILYLIMIT'])
      {
      	
      	  $select = $this->_db->select()
                               ->from('TEMP_DAILYLIMIT',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                               ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id));
          $result = $this->_db->fetchOne($select);
          
          if(!$result)
          {
             $info = 'Cust ID = '.$cust_id.', User Id = '.$user_id.', Ccy Id = '.$ccy_id;
             
             $dailyLimitData['DAILYLIMIT_STATUS'] = 3;
             $dailyLimitData['SUGGESTED']    = new Zend_Db_Expr('now()');
		         $dailyLimitData['SUGGESTEDBY']  = $this->_userIdLogin;
             
             try 
             {
  		        $this->_db->beginTransaction();
    		  	    $change_id = $this->suggestionWaitingApproval('User Daily Limit',$info,strtoupper($this->_changeType['code']['delete']),null,'M_DAILYLIMIT','TEMP_DAILYLIMIT',$user_id,$dailyLimitData['USER_FULLNAME'],$cust_id);
    			    $this->insertTempDailyLimit($change_id,$dailyLimitData);
    			    
    			    //log CRUD
    		      Application_Helper_General::writeLog('UDLU','User Daily Limit has been Updated (delete), User Id : '.$user_id. ' CCY : '.$dailyLimitData['CCY_ID'].' Change id : '.$change_id);
    			    
    			    $this->_db->commit();
    			    
    			    //$this->_redirect('/notification/submited/index');
    			    $this->_redirect('/notification/success/index');
  		      }
  		      catch(Exception $e) 
  		      {
    			    $this->_db->rollBack();
    			    $error_remark = $this->language->_('An Error Occured. Please Try Again');
    			    SGO_Helper_GeneralLog::technicalLog($e);
  		      }

            if(!$error_remark)
            { 
              Application_Helper_General::writeLog('UDLU','Update User Daily Limit');
                
              $this->_helper->getHelper('FlashMessenger')->addMessage('F');
              $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
              $this->_redirect($this->view->backURL);
            }
          }
          else
          { 
              $user_id = null;
              $error_remark = $this->language->_('No changes allowed for this record while awaiting approval for previous change.');
          }
      	}
      	else
      	{
            $user_id = null;
      	    $error_remark = 'Daily limit data is not found.';
      	}
     
    }
    else{
      $user_id = null;
    }
    
   
    
    if(!$user_id)
    {
      $error_remark = 'Customer ID is not found.';
      Application_Helper_General::writeLog('UDLU','Update User Daily Limit');
        
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }
    
  }
}
