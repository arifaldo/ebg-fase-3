<?php

Class airlines_Model_Airlines extends Application_Main
{
   
  public function getAirlines($param = null, $where = null)
  {

    if ($param == null) {
      $param = array('*');
    }

    $select = $this->_db->select()
                 ->from('M_AIRLINES', $param);

    if ($where != null) {

      foreach ($where as $key => $value) {
        $select->where($key.' = '.$this->_db->quote($value));
      }

      
    }

    $select = $this->_db->fetchAll($select);
     
    return $select;
  }

  public function getAccountAlias($bank_code, $source_account)
  {
    $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_APIKEY'))
                         ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                          ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                         ->where("A.CUST_ID  = ?", $this->_custIdLogin)
                         ->where("A.BANK_CODE  = ?", $bank_code)
                         ->order('A.APIKEY_ID ASC')
    );

    foreach ($acctlist as $key => $value) {
      $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
      $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
      if ($value['FIELD'] == 'account_number' && $value['VALUE'] == $source_account) {
        $APIKEY_ID = $value['ID'];
      }
    }

    if (empty($account[$APIKEY_ID]['account_currency'])) {
      $ccy = 'IDR';
    }
    else{
      $ccy = $account[$APIKEY_ID]['account_currency'];
    }

    if (empty($account[$APIKEY_ID]['account_alias'])) {
       $alias = $source_account.' - '.$account[$APIKEY_ID]['BANK_NAME'].' ('.$ccy.')';
    }
    else{
      $alias = $source_account.' - '.$account[$APIKEY_ID]['BANK_NAME'].' ('.$ccy.') '.' - '.$account[$APIKEY_ID]['account_alias'];
    }

    return $alias;

  }

  public function getAccountList()
  {

    $path = '/assets/themes/assets/newlayout/img/'; 

    $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_APIKEY'))
                         ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                          ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                         ->where("A.CUST_ID  = ?", $this->_custIdLogin)
                         ->order('A.APIKEY_ID ASC')
    );
    
    $account = array();
    foreach ($acctlist as $key => $value) {
        $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
        $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
        $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];

        if($value['BANK_CODE'] == '014'){
            $account[$value['ID']]['BANK_IMG'] = $path.'bca.png';
        }
        else if($value['BANK_CODE'] == '013'){
            $account[$value['ID']]['BANK_IMG'] = $path.'permata.png';
        }
        else if($value['BANK_CODE'] == '009'){
            $account[$value['ID']]['BANK_IMG'] = $path.'bni.png';
        }
        else if($value['BANK_CODE'] == '008'){
            $account[$value['ID']]['BANK_IMG'] = $path.'mandiri.png';
        }
        else if($value['BANK_CODE'] == '032'){
            $account[$value['ID']]['BANK_IMG'] = $path.'chase.png';
        }
        else if($value['BANK_CODE'] == '031'){
            $account[$value['ID']]['BANK_IMG'] = $path.'citibank.png';
        }
        else if($value['BANK_CODE'] == '153'){
            $account[$value['ID']]['BANK_IMG'] = $path.'sinar.png';
        }
        else if($value['BANK_CODE'] == '002'){
            $account[$value['ID']]['BANK_IMG'] = $path.'bri.png';
        }
        else if($value['BANK_CODE'] == '022'){
            $account[$value['ID']]['BANK_IMG'] = $path.'cimb.png';
        }
        else{
          $account[$value['ID']]['BANK_IMG'] = $path.'default-image.png';
        }
    }

    return $account;
  }


  public function getAirlinesDeposit($param = null, $where = null)
  {

    $depositData = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('T' => 'M_AIRLINES_TRAVELAGENT'), array('TRAVELAGENT_ID'))
                         ->join(array('D' => 'M_AIRLINES_DEPOSIT'), 'T.DEPOSIT_ID = D.DEPOSIT_ID', array('*'))
                         ->join(array('B' => 'M_BANK_TABLE'), 'D.BANK_CODE = B.BANK_CODE', array('BANK_NAME'))
                         ->where("T.CUST_ID  = ?", $this->_custIdLogin)
                         ->order('D.DEPOSIT_ID', 'ASC')
    );
     
    return $depositData;
  }

  public function getAirlinesSettings($airlines) 
  {

    $select = $this->_db->select()
            ->from(array('A' => 'M_AIRLINES_SETTINGS'),array('*'));

    $select->where('A.AIRLINE_CODE =?' , (string) $airlines);
    $select->where('A.CUST_ID =?' , $this->_custIdLogin);

    $airlinesdata = $this->_db->fetchall($select);
     
    return $airlinesdata;
  }

  public function getAllAirlinesSettings($status = 1)
  {

    $select = $this->_db->select()
            ->from(array('A' => 'M_AIRLINES_SETTINGS'),array('*'));

    $select->where('A.CUST_ID =?' , $this->_custIdLogin);
    $select->where('A.STATUS =?' , (string)$status);

    $airlinesdata = $this->_db->fetchall($select);
     
    return $airlinesdata;
  }

  public function getAirlinesLog($param = null, $where = null)
  {

    if ($param == null) {
      $param = array('*');
    }

    $select = $this->_db->select()
                 ->from('T_AIRLINES_LOG', $param);

    if ($where != null) {

      foreach ($where as $key => $value) {
        $select->where($key.' = '.$this->_db->quote($value));
      }
    }

    $select->order('DATE_TIME DESC');

    $select = $this->_db->fetchAll($select);
     
    return $select;
  }

  public function isRequestChange($airlines)
  {
    $cek = $this->_db->fetchRow(
        $this->_db->select()
            ->FROM('TEMP_AIRLINES_SETTINGS')
            //->WHERE("PROVIDER_ID = ?",$this->_db->quote($providerID))
            ->WHERE("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
            ->WHERE("AIRLINE_CODE = ".$this->_db->quote($airlines))
          );
    return $cek;
  }

}