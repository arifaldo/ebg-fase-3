<?php

require_once 'Zend/Controller/Action.php';

class realtimecharges_SuggestiondetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$changeid = $this->_getParam('changes_id');
  	
	$select = $this->_db->select()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
	$select -> where("A.CHANGES_ID = ?", $changeid);
	$select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
	$result = $select->query()->FetchAll();
	$this->view->result=$result;
	
  	if(!$result)
	{
		$this->_redirect('/notification/invalid/index');
	}
	
	$custid = $result[0]['KEY_FIELD'];
	
	//$selectprk = $this->_db->select()
		//			        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		//$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		//$selectprk = $selectprk->__toString();
		//$selectnoprk = $selectnoprk->__toString();
		//$selectnoprk2 = $selectnoprk2->__toString();
		
		//$unionquery = $this->_db->select()
								//->union(array($selectprk,$selectnoprk));
		
		//$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		//$selectunion = $this->_db->select()
		//					->from (($unionquery),array('*'));
		//$resultunion = $selectunion->query()->FetchAll();
		//$this->view->resultaccount = $resultunion;
		//Zend_Debug::dump($resultunion);die;
		
		/*$chargeaccount = $this->_db->select()
								->union(array($selectnoprk2,$selectnoprk));*/
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;
	
	$select2 = $this->_db->select()
					        ->from(array('B' => 'TEMP_CHARGES_WITHIN'),array('*'));
					        //->joinright(array('D' => $selectprk), 'B.ACCT_NO = D.ACCT_NO',array('*'));
	$select2 -> where("B.CHANGES_ID LIKE ".$this->_db->quote($changeid));
	$result2 = $select2->query()->FetchAll();
	$this->view->result2=$result2;
	
	$select2other = $this->_db->select()
					        ->from(array('B' => 'TEMP_CHARGES_OTHER'),array('*'));
					        
	$select2other -> where("B.CHANGES_ID LIKE ".$this->_db->quote($changeid));
	$result2other = $select2other->query()->FetchAll();
	$this->view->result2other = $result2other;
	
	//Zend_Debug::dump($result2);die;
	
	$select3 = $this->_db->select()
					        ->from(array('B' => 'M_CHARGES_WITHIN'),array('*'));
					        //->joinright(array('D' => $selectprk), 'B.ACCT_NO = D.ACCT_NO',array('*'));
	$select3 -> where("B.CUST_ID LIKE ".$this->_db->quote($custid));
	$select3 -> where("B.BUSINESS_TYPE LIKE '1'");
	$result3 = $select3->query()->FetchAll();
	$this->view->result3=$result3;
	
	$select3other = $this->_db->select()
					        ->from(array('B' => 'M_CHARGES_OTHER'),array('*'));
					        //->joinright(array('D' => $selectprk), 'B.ACCT_NO = D.ACCT_NO',array('*'));
	$select3other -> where("B.CUST_ID = ? ",'GLOBAL');
	//$select3 -> where("B.BUSINESS_TYPE LIKE '1'");
	$result3other = $select3other->query()->FetchAll();
	$this->view->result3other=$result3other;
	
	$pstypeArr = array(1 => 'RTGS',2 => 'SKN',8 => 'Domestic Online',0 => 'Inhouse');
		
	$this->view->pstype = $pstypeArr;
		
	
	if($result3)
	{
		$this->view->enable = true;
	}
	//Zend_Debug::dump($result3);die;
	
	$this->view->changes_id = $changeid;
    $this->view->typeCode = array_flip($this->_changeType['code']);
    $this->view->typeDesc = $this->_changeType['desc'];
    $this->view->modulename = $this->_request->getModuleName();
	//echo $select2; die;
	Application_Helper_General::writeLog('CHCL','View company charges changes list');
  }
}