<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class Holiday_SuggestionDetailController extends Application_Main
{
	public function initController(){
		$this->_helper->layout()->setLayout('newpopup');
	}
	public function indexAction()
	{
		$params = $this->_request->getParams();


  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

		$status = $this->_changeStatus;
		$options = array_combine(array_values($status['code']),array_values($status['desc']));

		$changesid = $this->_getparam('changes_id');

		$select = $this->_db->SELECT()
												->FROM(array('A' => 'T_GLOBAL_CHANGES'),array('*'))
												->WHERE("A.CHANGES_ID = ?",$changesid)
												->WHERE("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$result = $this->_db->fetchRow($select);
		if(!$result)
		{
			$this->_redirect('/notification/invalid/index');
		}

  		if(is_array($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;

  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
  					array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
					'messages' => array($this->language->_('Changes ID not Found'),
													$this->language->_('Invalid Changes ID'),
													$this->language->_('Invalid Changes ID'),
													),
  				),
  			);

  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{

				$select	= $this->_db->FetchRow("SELECT CHANGES_STATUS , CREATED , CREATED_BY  FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = ".$this->_db->quote($changesid));

				$this->view->created = $select['CREATED'];
				$this->view->createdby = $select['CREATED_BY'];
				foreach($options as $codestat=>$descstat)
				{
					if($codestat == $select['CHANGES_STATUS'])
					{
						$this->view->status = $descstat;
					}
				}

	  			$year = $this->_db->fetchOne(
	  				$this->_db->select()
	  				->from(array('A' => 'TEMP_HOLIDAY'), array('HOLIDAY_YEAR'))
	  				->where('CHANGES_ID = ?', $changesid)
	  			);

				$newdata = $this->_db->fetchAll(
							$this->_db->select()
							->from('TEMP_HOLIDAY')
							->where('HOLIDAY_YEAR = ?', $year)
				);
	  			$this->view->newdata 	= $newdata;
			}
			$this->view->changes_id 	= $changesid;
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('HDCL','View Suggestion Detail Holiday Setting');
		}
	}
}
