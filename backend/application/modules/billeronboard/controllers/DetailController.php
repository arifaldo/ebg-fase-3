<?php


require_once 'Zend/Controller/Action.php';


class billeronboard_DetailController extends Application_Main
{
	public function indexAction() 
	{
		$providerID 	= $this->_getParam('providerid');
		if(empty($providerID))
		{
			header('location: /billeronboard');exit();
		}
		$model 			 = new billeronboard_Model_Billeronboard();
		$this->view->providerData 	 = $model->getDetailProvider($providerID);
		$this->view->changestatus 	 = $model->isRequestChange($providerID);
		$param['PROVIDER_ID'] = $providerID;
		$this->view->billerAcc 	 = $model->getBillerAcc($param);
		$this->setbackURL('/'.$this->_request->getModuleName().'/detail/index/providerid/'.$providerID);
		Application_Helper_General::writeLog('VBIL','View Detail Biller');
	}
	
	public function activedeactiveAction() 
	{
		
		
		$providerID 	= $this->_getParam('providerid');
		$status 			= $this->_getParam('status');

		if(empty($providerID) || !in_array($status,array(1,2)))
		{
			header('location: /billeronboard');exit();
		}
		$model 			 = new billeronboard_Model_Billeronboard();
		$providerData 	 = $model->getDetailProvider($providerID);
		if(empty($providerData))
		{
			header('location: /billeronboard');exit();
		}
		else
		{
			if($status == 2)
			{
				$code = strtoupper($this->_changeType['code']['suspend']);
				$info2 = 'Suspend Biller';
			}
			elseif($status == 1)
			{
				$code = strtoupper($this->_changeType['code']['unsuspend']);
				$info2 = 'Unsuspend Biller';
			}
			$this->_db->beginTransaction();
			try
			{
				$providerID 	 = $this->_getParam('providerID');
				$providerName 	 = $this->_getParam('providerName');
				$info = 'E-Commerce Payment';
				$change_id = $this->suggestionWaitingApproval($info,$info2,$code,null,'M_SERVICE_PROVIDER','TEMP_SERVICE_PROVIDER',$providerData['PROVIDER_CODE'],$providerData['PROVIDER_NAME'],$providerData['PROVIDER_CODE']);
				
				unset($providerData['CHARGES']);
				$data= $providerData+array(
																	'CHANGES_ID' 			=> $change_id,
																	'PROVIDER_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
																	'PROVIDER_SUGGESTEDBY' 	=> $this->_userIdLogin
																);

				$getServiceType 	 = $model->getServiceType();
				if(!empty($getServiceType))
				{
					foreach($getServiceType as $value)
					{
						$arrServiceType[trim($value['SERVICE_NAME'])] = $value['SERVICE_ID'];
					}
				}

				$arrProviderType = array('Payment' => 1,'Purchase' =>2);
				$data['PROVIDER_TYPE']	= $arrProviderType[$providerData['PROVIDER_TYPE']];
				$data['SERVICE_TYPE']		= $arrServiceType[trim($providerData['SERVICE_TYPE'])];
				$data['PROVIDER_STATUS'] = $status;
				
				$model->insertTemp($data);
				Application_Helper_General::writeLog('UBIL',''.$info2. ' '. $providerData['PROVIDER_CODE']);
				
				if($error == 0)
				{
					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName());
					$this->_redirect('/notification/submited/index');
				}
			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
			}
		}
	}
	
	public function deleteAction()
	{
	
	
		$providerID 	= $this->_getParam('providerid');
		$status 			= $this->_getParam('status');
		
		if(empty($providerID) || !in_array($status,array(3)))
		{
			header('location: /billeronboard');exit();
		}
		$model 			 = new billeronboard_Model_Billeronboard();
		$providerData 	 = $model->getDetailProvider($providerID);
// 		var_dump($providerData);die;
		if(empty($providerData))
		{
			header('location: /billeronboard');exit();
		}
		else
		{
			if($status == 3)
			{
				$code = strtoupper($this->_changeType['code']['delete']); //strtoupper('DEL'); 
				$info2 = 'Delete E-Commerce Payment';
			}
			elseif($status == 1)
			{
				$code = strtoupper($this->_changeType['code']['unsuspend']);
				$info2 = 'Unsuspend Biller';
			}
			$this->_db->beginTransaction();
			try
			{
				$providerID 	 = $this->_getParam('providerID');
				$providerName 	 = $this->_getParam('providerName');
				$info = 'E-Commerce Payment';
				$change_id = $this->suggestionWaitingApproval($info,$info2,$code,null,'M_SERVICE_PROVIDER','TEMP_SERVICE_PROVIDER',$providerData['PROVIDER_CODE'],$providerData['PROVIDER_NAME'],$providerData['PROVIDER_CODE']);
				
				$updatesugStatus['PROVIDER_STATUS'] = '3';
				$wheresugStatus['PROVIDER_ID = ?'] = $this->_getParam('providerid');
				
				//$query = $this->_db->update ( "M_SERVICE_PROVIDER", $updatesugStatus, $wheresugStatus );
				
				unset($providerData['CHARGES']);
				$data= $providerData+array(
						'CHANGES_ID' 			=> $change_id,
						'PROVIDER_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
						'PROVIDER_SUGGESTEDBY' 	=> $this->_userIdLogin
				);
	
				$getServiceType 	 = $model->getServiceType();
				if(!empty($getServiceType))
				{
					foreach($getServiceType as $value)
					{
						$arrServiceType[trim($value['SERVICE_NAME'])] = $value['SERVICE_ID'];
					}
				}
	
				$arrProviderType = array('Payment' => 1,'Purchase' =>2);
				$data['PROVIDER_TYPE']	= $arrProviderType[$providerData['PROVIDER_TYPE']];
				$data['SERVICE_TYPE']		= $arrServiceType[trim($providerData['SERVICE_TYPE'])];
				$data['PROVIDER_STATUS'] = $status;
	
				$model->insertTemp($data);
				Application_Helper_General::writeLog('UECL',''.$info2. ' '. $providerData['PROVIDER_CODE']);
	
				if($error == 0)
				{
					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName());
					$this->_redirect('/notification/submited/index');
				}
			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
			}
		}
	}

}