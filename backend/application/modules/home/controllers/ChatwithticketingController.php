<?php
require_once 'Zend/Controller/Action.php';

class home_ChatwithticketingController extends Application_Main
{
  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $sessTicketId  = new Zend_Session_Namespace('sessTicketId');
    if (isset($sessTicketId->content)) {
      Zend_Session::namespaceUnset('sessTicketId');
    }

    $ticketId    = $this->language->_('No Tiket');
    $category    = $this->language->_('Category');
    $subject    = $this->language->_('Subject');
    $createdDate = $this->language->_('Created Date');
    $createdBy    = $this->language->_('Created By');
    $status    = $this->language->_('Status');

    $fields = [
      'ticketId' => [
        'field'   => 'TICKET_ID',
        'label'    => $ticketId,
        'sortable'  => true
      ],
      'category' => [
        'field'   => 'CATEGORY',
        'label'    => $category,
        'sortable'  => true
      ],
      'subject' => [
        'field'   => 'SUBJECT',
        'label'    => $subject,
        'sortable'  => true
      ],
      'createdDate' => [
        'field'   => 'CREATED_DATE',
        'label'    => $createdDate,
        'sortable'  => true
      ],
      'createdBy' => [
        'field'   => 'CREATED_BY',
        'label'    => $createdBy,
        'sortable'  => true
      ],
      'status' => [
        'field'   => 'STATUS',
        'label'    => $status,
        'sortable'  => true
      ]
    ];

    //get page, sortby, sortdir
    $detail       = $this->_getParam('detail');
    $submit     = $this->_getParam('submit');
    $page       = $this->_getParam('page');
    $sortBy     = $this->_getParam('sortby');
    $sortDir     = $this->_getParam('sortdir');
    $filter_clear   = $this->_getParam('filter_clear');

    //validate parameters before passing to view and query
    $page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

    $sortBy = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

    
    $data = $this->_db->select()
      ->from(
        array('T' => 'TICKETING'),
        array(
          'TICKET_ID'    => 'T.TICKET_ID',
          'CATEGORY'    => new Zend_Db_Expr("(CASE T.CATEGORY
					WHEN '1' THEN 'Transaksi'
					WHEN '2' THEN 'Administrasi User'
					WHEN '3' THEN 'Laporan Permasalahan'
					WHEN '4' THEN 'Lainnya'
					END)"),
          'SUBJECT'    => 'T.SUBJECT',
          'CREATED_DATE'  => 'T.CREATED_DATE',
          'CREATED_BY'  => 'T.CREATED_BY',
          'CUST_CODE'    => 'T.CUST_CODE',
          'STATUS'    => new Zend_Db_Expr("(CASE T.STATUS
					WHEN '0' THEN 'Close'
					WHEN '1' THEN 'Open'
					END)")
        )
      )
      ->joinLeft(
        array('TD' => 'TICKETING_DETAIL'),
        'TD.TICKET_ID = T.TICKET_ID',
        array()
      )
      ->group('T.TICKET_ID');
	
	// advance filter ----------------------------------------------------------
        $filterlist = array("Category" => "CATEGORY", "Status" => "STATUS");

        $this->view->filterlist = $filterlist;


        $filterArr = array(
            'filter'    =>  array('StripTags'),
            'CATEGORY'    =>  array('StringTrim', 'StripTags'),
            'STATUS' =>  array('StringTrim', 'StripTags'),
        );

        $validator = array(
            'filter'                 => array(),
            'CATEGORY' => array(),
            'STATUS' => array(),
        );


        $dataParam = array("CATEGORY", "STATUS");
        $dataParamValue = array();

        $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
        $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
        // print_r($this->_request->getParam('wherecol'));
        foreach ($dataParam as $no => $dtParam) {

            if (!empty($this->_request->getParam('wherecol'))) {
                $dataval = $this->_request->getParam('whereval');
                // print_r($dataval);
                $order = 0;
                foreach ($this->_request->getParam('wherecol') as $key => $value) {
                    if ($dtParam == $value) {
                        $dataParamValue[$dtParam] = $dataval[$order];
                    }
                    $order++;
                }
            }
        }


       

       
        $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
        // $filter 	= $zf_filter->getEscaped('filter');
        $filter         = $this->_getParam('filter');
        $CATEGORY     = html_entity_decode($zf_filter->getEscaped('CATEGORY'));

        $STATUS     = html_entity_decode($zf_filter->getEscaped('STATUS'));
        
        if ($filter == TRUE) {

           
          
            if ($CATEGORY != null) {
               $this->view->CATEGORY = $CATEGORY;
               $data->where('T.CATEGORY = ?', $CATEGORY);
            }
			
			if ($STATUS != null) {
                $STATUS = ($STATUS == 1) ? 1 : 0;
				$data->where('T.STATUS = ?', $STATUS);
            }
        }

        unset($dataParamValue['PAPER_UPDATED_END']);
        if (!empty($dataParamValue)) {
            foreach ($dataParamValue as $key => $value) {
                $wherecol[]    = $key;
                $whereval[] = $value;
            }

            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }
	
    $data->order($sortBy . ' ' . $sortDir);
    $data = $this->_db->fetchAll($data);
    // var_dump($data);
    // die();
    $this->paging($data);

    $this->view->currentPage  = $page;
    $this->view->sortby     = $sortBy;
    $this->view->sortDir     = $sortDir;
    $this->view->fields     = $fields;
    $this->view->filter     = $filter;
    $this->view->fCategory     = $fCategory;
    $this->view->fSubject     = $fSubject;
    $this->view->fCreatedDate   = $fCreatedDate;
    $this->view->fCreatedBy   = $fCreatedBy;
    $this->view->fStatus     = $fStatus;


    if ($submit) {
      //get filtering param
      $filters = array(
        'category'  =>  array('StringTrim', 'StripTags'),
        'subject'  =>  array('StringTrim', 'StripTags'),
        'comment'  =>  array('StringTrim', 'StripTags'),
      );

      $validators = array(
        'category'   => array(
          'NotEmpty',
          'messages' => array($this->language->_('Error: Category cannot be empty'))
        ),
        'subject'   => array(
          'NotEmpty',
          'messages' => array($this->language->_('Error: Subject cannot be empty'))
        ),
        'comment'   => array(
          'NotEmpty',
          'messages' => array($this->language->_('Error: Comment cannot be empty'))
        ),
      );

      $params['category']  = $this->_getParam('category');
      $params['subject']  = $this->_getParam('subject');
      $params['comment']  = $this->_getParam('comment');

      $zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

      if ($zf_filter_input->isValid()) {
        try {
          $this->_db->beginTransaction();

          $data = [
            'TICKET_ID'    => date('ymdHis'),
            'CUST_CODE'    => $this->_custIdLogin,
            'CREATED_DATE'  => date('Y-m-d H:i:s'),
            'CREATED_BY'  => $this->_userIdLogin,
            'CATEGORY'    => $zf_filter_input->category,
            'SUBJECT'    => $zf_filter_input->subject
          ];
          $this->_db->insert('TICKETING', $data);

          $dataDetail = [
            'TICKET_ID'    => date('ymdHis'),
            'USER_ID'    => $this->_userIdLogin,
            'USER_TYPE'    => 2,
            'COMMENT_DATE'  => date('Y-m-d H:i:s'),
            'COMMENT'    => $zf_filter_input->comment
          ];
          $this->_db->insert('TICKETING_DETAIL', $dataDetail);

          $this->_db->commit();

          Application_Helper_General::writeLog('ACSQ', 'Customer Service Create a Ticketing. #' . $data['TICKET_ID'], $data['TICKET_ID']);

          $this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
          $this->_redirect('/notification/success');
        } catch (Exception $error) {
          $this->_db->rollBack();
          $errMsg = $this->language->_('Data Fail To Insert');
          $this->view->error = true;
          $this->view->errMsg = $errMsg;
          Application_Helper_General::writeLog('ACSQ', 'Customer Service Failed to Create a Ticketing.');
        }
      } else {
        $errors = $zf_filter_input->getMessages();
        $errDesc['category'] = (isset($errors['category'])) ? $errors['category'] : null;
        $errDesc['subject']  = (isset($errors['subject'])) ? $errors['subject'] : null;
        $errDesc['comment']  = (isset($errors['comment'])) ? $errors['comment'] : null;

        $this->view->error = true;
        $this->view->errDesc  = $errDesc;
        $this->view->category = $params['category'];
        $this->view->subject  = $params['subject'];
        $this->view->comment  = $params['comment'];
      }
    } elseif ($detail) {
      $sessTicketId  = new Zend_Session_Namespace('sessTicketId');
      $sessTicketId->content = $detail;
      Application_Helper_General::writeLog('VCSQ', 'Customer Service View Ticketing Detail. #' . $detail, $detail);
      $this->_redirect('home/chatwithticketing/detail');
    } else {
      Application_Helper_General::writeLog('VCSQ', 'Customer Service View Ticketing');
    }
  }

  public function detailAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $submit = $this->_getParam('submit');

    if (!$this->view->hasPrivilege('ACSQ')) {
      $errMsg = $this->language->_('You don\'t have permission to  this action.');
      $this->view->error  = true;
      $this->view->errMsg = $errMsg;
    }

    $sessTicketId  = new Zend_Session_Namespace('sessTicketId');
    $ticketId = $sessTicketId->content;

    if (isset($ticketId)) {
      $data = $this->_db->select()
        ->from(
          array('T' => 'TICKETING'),
          array(
            'TICKET_ID'    => 'T.TICKET_ID',
            'CATEGORY'    => new Zend_Db_Expr("(CASE T.CATEGORY
						WHEN '1' THEN 'Transaction'
						WHEN '2' THEN 'User Administration'
						WHEN '3' THEN 'Report Problem'
						END)"),
            'SUBJECT'    => 'T.SUBJECT',
            'CREATED_DATE'  => 'T.CREATED_DATE',
            'CREATED_BY'  => 'T.CREATED_BY',
            'CUST_CODE'    => 'T.CUST_CODE',
            'CLOSED_DATE'  => 'T.CLOSED_DATE',
            'CLOSED_BY'    => 'T.CLOSED_BY',
            'STATUS'    => new Zend_Db_Expr("(CASE T.STATUS
						WHEN '0' THEN 'Close'
						WHEN '1' THEN 'Open'
						END)")
          )
        )
        ->where('T.TICKET_ID = ?', $ticketId);
      $data = $this->_db->fetchRow($data);

      $dataDetail = $this->_db->select()
        ->from(
          array('TD' => 'TICKETING_DETAIL'),
          array('*')
        )
        ->where('TD.TICKET_ID = ?', $ticketId);
      $dataDetail = $this->_db->fetchAll($dataDetail);

      $this->view->data     = $data;
      $this->view->dataDetail = $dataDetail;

      if ($submit) {
        //get filtering param
        $filters = array('comment' => array('StringTrim', 'StripTags'));

        $validators = array(
          'comment' => array(
            'NotEmpty',
            'messages' => array($this->language->_('Error: Comment cannot be empty'))
          )
        );

        $params['comment']  = $this->_getParam('comment');

        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

        if ($zf_filter_input->isValid()) {
          try {
            $this->_db->beginTransaction();

            $dataDetail = [
              'TICKET_ID'    => $ticketId,
              'USER_ID'    => $this->_userIdLogin,
              'USER_TYPE'    => 2,
              'COMMENT_DATE'  => date('Y-m-d H:i:s'),
              'COMMENT'    => $zf_filter_input->comment
            ];
            $this->_db->insert('TICKETING_DETAIL', $dataDetail);
            $this->_db->commit();

            Application_Helper_General::writeLog('ACSQ', 'Customer Service Create a Comment on Ticketing Detail. #' . $ticketId, $ticketId);

            $this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/detail');
            $this->_redirect('/notification/success');
          } catch (Exception $error) {
            $this->_db->rollBack();
            $errMsg = $this->language->_('Failed to send a comment');

            $this->view->error  = true;
            $this->view->errMsg = $errMsg;
            Application_Helper_General::writeLog('ACSQ', 'Customer Service Failed to Create a Comment on Ticketing Detail. #' . $ticketId, $ticketId);
          }
        } else {
          $errors = $zf_filter_input->getMessages();
          $errDesc['comment']  = (isset($errors['comment'])) ? $errors['comment'] : null;

          $this->view->errDesc = $errDesc;
          $this->view->comment = $params['comment'];
        }
      } else {
        Application_Helper_General::writeLog('VCSQ', 'Customer Service View Ticketing Detail. #' . $ticketId, $ticketId);
      }
    }
  }

  public function ajaxAction()
  {
    $this->_helper->viewRenderer->setNoRender(true);
    $this->_helper->layout->disableLayout();

    if (!$this->view->hasPrivilege('ACSQ')) {
      echo json_encode(array('message' => $this->language->_('You don\'t have permission to this action.')), 401);
      return false;
    }

    $request = $this->getRequest();
    $ticketId = $request->ticketId;

    $data = [
      'CLOSED_DATE'  => date('Y-m-d H:i:s'),
      'CLOSED_BY'    => $this->_userIdLogin,
      'STATUS'    => 0
    ];
    $where = ['TICKET_ID = ?' => $ticketId];
    $this->_db->update('TICKETING', $data, $where);

    Application_Helper_General::writeLog('ACSQ', 'Customer Service Close Ticketing Detail. #' . $ticketId, $ticketId);

    $this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/detail');

    echo json_encode(array('redirect' => '/notification/success'));
  }
  
  public function listcategoryAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

        // $ongoingStatusArr = array(
        //     1 => 'Ongoing',
        //     2 => 'Canceled'
        // );
		
		$categoryArr = [
		  '1'  => 'Transaction',
		  '2'  => 'User Administration',
		  '3'  => 'Report Problem'
		];


        foreach ($categoryArr as $key => $row) {
            if ($tblName == $key) {
                $select = 'selected';
            } else {
                $select = '';
            }
            $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
        }

        echo $optHtml;
    }
	
	public function liststatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

        // $ongoingStatusArr = array(
        //     1 => 'Ongoing',
        //     2 => 'Canceled'
        // );
		
		$statusArr = [
		  '1'  => 'Open',
		  '2'  => 'Close'
		];


        foreach ($statusArr as $key => $row) {
            if ($tblName == $key) {
                $select = 'selected';
            } else {
                $select = '';
            }
            $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
        }

        echo $optHtml;
    }
}
