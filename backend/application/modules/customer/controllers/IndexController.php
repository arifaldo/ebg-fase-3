<?php

require_once 'Zend/Controller/Action.php';

class customer_IndexController extends customer_Model_Customer
{

	public function initController()
	{
		//$statusArr = Application_Helper_Array::globalvarArray($this->_masteruserStatus);
		$statusArr = array('' => '-- ' . $this->language->_('Any Value') . ' --', '1' => $this->language->_('Approved'), '2' => $this->language->_('Suspended'), '3' => $this->language->_('Deleted'));
		$this->view->statusArr = $statusArr;

		$custArr  = Application_Helper_Array::listArray($this->getAllCustomer(), 'CUST_ID', 'CUST_ID');
		$custArr  = array_merge(array('' => '-- ' . $this->language->_('Any Value') . ' --'), $custArr);
		$this->view->custArr = $custArr;

		//utk value filter
		$countryArr  = Application_Helper_Array::listArray($this->getCountry(), 'COUNTRY_CODE', 'COUNTRY_NAME');
		//$countryArr  = array_merge(array(''=>'-- Any Value --'),$countryArr);
		$this->view->countryArr  = $countryArr;


		$conf = Zend_Registry::get('config');
		$custmodelType 		= $conf["cust"]["model"]["desc"];
		$custmodelCode 		= $conf["cust"]["model"]["code"];
		$custmodelArr = array_combine(array_values($custmodelCode), array_values($custmodelType));
		//Zend_Debug::dump($custmodelArr);
		$this->view->custmodelArr = $custmodelArr;
		/*
		Zend_Debug::dump($custmodelArr);
		die;*/

		$this->view->signArr = array('EQ' => '=', 'NE' => '!=', 'LT' => '<', 'GT' => '>', 'LE' => '<=', 'GE' => '>=');


		//format display date
		$this->view->dateDisplayFormat = $this->_dateDisplayFormat;
	}



	public function indexAction()
	{
		if (!$this->view->hasPrivilege("CCLS")) {
			return $this->_redirect($this->view->url(["module" => "notification", "controller" => "notauthorized"]));
		}

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');
		$auth = Zend_Auth::getInstance()->getIdentity();
		$fields = array(
			// 'custid'   => array('field'    => 'CUST_ID',
			//                                'label'    => $this->language->_('Company Code'),
			//                                'sortable' => true),

			'custname' => array(
				'field'    => 'COMPANY',
				'label'    => $this->language->_('Company'),
				'sortable' => true
			),
			'model'     => array(
				'field'    => 'CUST_MODEL',
				'label'    => $this->language->_('Model'),
				'sortable' => true
			),

			'city'     => array(
				'field'    => 'CUST_CITY',
				'label'    => $this->language->_('City'),
				'sortable' => true
			),

			'country'     => array(
				'field'  => 'COUNTRY_CODE',
				'label'    => $this->language->_('Country'),
				'sortable' => true
			),

			'status'   => array(
				'field'    => 'CUST_STATUS',
				'label'    => $this->language->_('Status'),
				'sortable' => true
			),

			//'latestSuggestion'     => array('field'    => 'CUST_SUGGESTED',
			//                           'label'    => $this->language->_('Last Suggestion Date'),
			//                          'sortable' => true),

			'latestSuggestor'   => array(
				'field'  => 'CUST_SUGGESTEDBY',
				'label'    => $this->language->_('Last Suggested'),
				'sortable' => true
			),

			//'latestApproval'    => array('field'  => 'CUST_UPDATED',
			//                          'label'    => $this->language->_('Last Approval Date'),
			//                         'sortable' => true),

			'latestApprover'  => array(
				'field'   => 'CUST_UPDATEDBY',
				'label'    => $this->language->_('Last Approved'),
				'sortable' => true
			)
		);

		//$filterlist = array('Last Suggestion Date','Last Approval Date','LATEST_SUGGESTER','LATEST_APPROVER','COMP_ID','COMP_NAME','CITY','COUNTRY_CODE','STATUS');
		$filterlist = array("Company ID" => "COMP_ID", "Company" => "COMP_NAME", "Model" => "CUST_MODEL", "City" => "CITY", "Country" => "COUNTRY_CODE", "Status" => "STATUS", "Last Suggested" => "Last Suggestion Date", "Last Approved" => "Last Approval Date");

		$this->view->filterlist = $filterlist;

		//validasi page, jika input page bukan angka
		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');


		$page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;

		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';

		$filterArr = array(
			'filter'  => array('StripTags', 'StringTrim'),
			'COMP_ID'     => array('StripTags', 'StringTrim', 'StringToUpper'),
			'COMP_NAME'   => array('StripTags', 'StringTrim', 'StringToUpper'),
			'STATUS'  => array('StripTags', 'StringTrim'),
			'COUNTRY_CODE' => array('StripTags', 'StringTrim'),
			'CITY'    => array('StripTags', 'StringTrim'),

			'Last Suggestion Date' => array('StripTags', 'StringTrim', 'StringToUpper'),
			'Last Suggestion Date_END'     => array('StripTags', 'StringTrim'),
			'LATEST_SUGGESTER'      => array('StripTags', 'StringTrim', 'StringToUpper'),
			'Last Approval Date'   => array('StripTags', 'StringTrim', 'StringToUpper'),
			'Last Approval Date_END'       => array('StripTags', 'StringTrim'),
			'LATEST_APPROVER'       => array('StripTags', 'StringTrim', 'StringToUpper'),
		);


		$validator = array(
			'filter'  => array(),
			'COMP_ID'     => array(),
			'COMP_NAME'   => array(),
			'STATUS'  => array(),
			'COUNTRY_CODE' => array(),
			'CITY'    => array(),

			'Last Suggestion Date' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'Last Suggestion Date_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'LATEST_SUGGESTER'      => array(),
			'Last Approval Date'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'Last Approval Date_END'       => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'LATEST_APPROVER'       => array(),
		);

		$dataParam = array('LATEST_SUGGESTER', 'LATEST_APPROVER', 'COMP_ID', 'COMP_NAME', 'CITY', 'COUNTRY_CODE', 'STATUS');
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				$order = 0;

				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
					$order++;
				}
			}
			/*
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if($value == "Last Approval Date" || $value == "Last Suggestion Date"){
						$order--;
					}
					if($dtParam==$value){
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}

			}*/
		}
		// print_r($dataParamValue);
		// die;
		// print_r($this->_request->getParam('whereval'));die;
		if (!empty($this->_request->getParam('lsdate'))) {
			$lsarr = $this->_request->getParam('lsdate');
			$dataParamValue['Last Suggestion Date'] = $lsarr[0];
			$dataParamValue['Last Suggestion Date_END'] = $lsarr[1];
		}
		if (!empty($this->_request->getParam('ladate'))) {
			$laarr = $this->_request->getParam('ladate');
			$dataParamValue['Last Approval Date'] = $laarr[0];
			$dataParamValue['Last Approval Date_END'] = $laarr[1];
		}

		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);


		// $filter 		= $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->customer_msg = $msg;
			}
		}


		// proses pengambilan data filter,display all,sorting
		$select = array();

		$select = $this->_db->select()
			->from(array('G' => 'M_CUSTOMER'), array(
				'CUST_ID', 'CUST_MODEL', 'CUST_CITY', 'CUST_NAME', 'CUST_STATUS', 'COUNTRY_CODE', 'CUST_SUGGESTED', 'CUST_SUGGESTEDBY', 'CUST_UPDATED', 'CUST_UPDATEDBY',
				'COMPANY'	=> new Zend_Db_Expr("CONCAT(CUST_NAME , ' (' , CUST_ID , ')  ' )"),
			))
			->joinLeft(array('B' => 'M_BUSER'), 'G.CUST_CREATEDBY = B.BUSER_ID', array('B.BUSER_BRANCH'))
			->joinLeft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH', array('C.BRANCH_NAME', 'C.STATUS'))
			->join(array('D' => 'M_CITYLIST'), 'G.CUST_CITY = D.CITY_CODE', array('D.CITY_NAME'));
		//->where('C.ID="'.$auth->userBranchId.'"');

		$auth = Zend_Auth::getInstance()->getIdentity();
		if ($auth->userHeadQuarter == "NO") {
			$select->where('C.ID = ?', $auth->userBranchId); // Add Bahri
		}
		// ->joinleft(array('BG' => 'M_BGROUP'),'BG.BGROUP_ID = B.BGROUP_ID');
		//->where('CUST_STATUS <> 3');
		//->where("CONVERT(VARCHAR(20), CUST_SUGGESTED, 111) = '2012/04/12'")
		// ->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($cid)));
		//  CONVERT(VARCHAR(8), now(), 112) AS [YYYYMMDD]
		// ->query()->fetchAll();
		// echo $select;die();
		//saat pertama kali klik, lgsg filter by date today
		if ($filter == '') {
			/*$today = date('d/m/Y');

		        $this->view->latestSuggestionFrom = $today;
			    $this->view->latestSuggestionTo   = $today;

			    //konversi date agar dapat dibandingkan
			    $latestSuggestionFrom   = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
								 	       new Zend_Date($today,$this->_dateDisplayFormat):
								 	       false;

			    $latestSuggestionTo     = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
								 	       new Zend_Date($today,$this->_dateDisplayFormat):
								 	       false;

	           if($latestSuggestionFrom)  $select->where("CONVERT(date,CUST_SUGGESTED) >= CONVERT(DATE,".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
	           if($latestSuggestionTo)    $select->where("CONVERT(date,CUST_SUGGESTED) <= CONVERT(DATE,".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");*/
		}


		if ($filter == TRUE || $csv || $pdf || $this->_request->getParam('print')) {
			$conf = Zend_Registry::get('config');
			$custmodelType 		= $conf["cust"]["model"]["desc"];
			$custmodelCode 		= $conf["cust"]["model"]["code"];
			$custmodelArr = array_combine(array_values($custmodelCode), array_values($custmodelType));

			$cid       = html_entity_decode($zf_filter->getEscaped('COMP_ID'));
			$cname     = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
			$city      = html_entity_decode($zf_filter->getEscaped('CITY'));
			$country   = html_entity_decode($zf_filter->getEscaped('COUNTRY_CODE'));
			$status    = html_entity_decode($zf_filter->getEscaped('STATUS'));
			$latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('Last Suggestion Date'));
			$latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('Last Suggestion Date_END'));
			$latestSuggestor        = html_entity_decode($zf_filter->getEscaped('LATEST_SUGGESTER'));
			$latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('Last Approval Date'));
			$latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('Last Approval Date_END'));
			$latestApprover         = html_entity_decode($zf_filter->getEscaped('LATEST_APPROVER'));

			//konversi date agar dapat dibandingkan
			$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestSuggestionFrom, $this->_dateDisplayFormat) :
				false;

			$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestSuggestionTo, $this->_dateDisplayFormat) :
				false;

			$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestApprovalFrom, $this->_dateDisplayFormat) :
				false;

			$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestApprovalTo, $this->_dateDisplayFormat) :
				false;

			//if($cid)            $select->where('UPPER(CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cid).'%'));
			if ($cid)              $select->where('UPPER(CUST_ID)=' . $this->_db->quote(strtoupper($cid)));
			if ($cname)            $select->where('UPPER(CUST_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($cname) . '%'));
			if ($city)             $select->where('UPPER(D.CITY_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($city) . '%'));
			//if($country)        $select->where('UPPER(COUNTRY_CODE) LIKE '.$this->_db->quote('%'.strtoupper($country).'%'));
			if ($country)          $select->where('UPPER(COUNTRY_CODE)=' . $this->_db->quote(strtoupper($country)));
			if ($status)           $select->where('CUST_STATUS=?', $status);
			if ($latestSuggestor)  $select->where('UPPER(CUST_SUGGESTEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestSuggestor) . '%'));
			if ($latestApprover)   $select->where('UPPER(CUST_UPDATEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestApprover) . '%'));

			if ($latestSuggestionFrom)  $select->where("DATE(CUST_SUGGESTED) >= DATE(" . $this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)) . ")");
			if ($latestSuggestionTo)    $select->where("DATE(CUST_SUGGESTED) <= DATE(" . $this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)) . ")");
			if ($latestApprovalFrom)    $select->where("DATE(CUST_UPDATED) >= DATE(" . $this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)) . ")");
			if ($latestApprovalTo)      $select->where("DATE(CUST_UPDATED) <= DATE(" . $this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)) . ")");

			#			if($latestSuggestionFrom)  $select->where("CONVERT(date,CUST_SUGGESTED) >= CONVERT(DATE,".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
			#			if($latestSuggestionTo)    $select->where("CONVERT(date,CUST_SUGGESTED) <= CONVERT(DATE,".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");
			#			if($latestApprovalFrom)    $select->where("CONVERT(date,CUST_UPDATED) >= CONVERT(DATE,".$this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)).")");
			#			if($latestApprovalTo)      $select->where("CONVERT(date,CUST_UPDATED) <= CONVERT(DATE,".$this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)).")");

			$this->view->cid     = $cid;
			$this->view->cname   = $cname;
			$this->view->city    = $city;
			$this->view->status  = $status;
			$this->view->country = $country;
			$this->view->latestSuggestor  = $latestSuggestor;
			$this->view->latestApprover   = $latestApprover;

			if ($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
			if ($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
			if ($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
			if ($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
		}



		//utk sorting
		$select->order($sortBy . ' ' . $sortDir);

		// END proses pengambilan data filter,display all,sorting


		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$statusCode = array_flip($this->_masterglobalstatus['code']);
		$this->view->statusCode = $statusCode;
		$statusDesc = $this->_masterglobalstatus['desc'];
		$this->view->statusDesc = $statusDesc;
		$this->view->modulename = $this->_request->getModuleName();

		$dataSQL = $this->_db->fetchAll($select);

		if (!empty($dataParamValue)) {
			$filterlistdata = array("Filter");
			foreach ($dataParamValue as $fil => $val) {
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
			}
		} else {
			$filterlistdata = array("Filter - None");
		}

		$this->view->data_filter = $filterlistdata;

		if ($csv) {
			$countryArr  = Application_Helper_Array::listArray($this->getCountry(), 'COUNTRY_CODE', 'COUNTRY_NAME');

			$data = $this->_db->fetchall($select);

			/* foreach ($select as $key => $value) {
					unset($select[$key]['CUST_ID']);
					//unset($select[$key]['CUST_MODEL']);
					unset($select[$key]['CUST_CITY']);
					unset($select[$key]['CUST_NAME']);
					unset($select[$key]['CUST_STATUS']);
					unset($select[$key]['COUNTRY_CODE']);
					unset($select[$key]['CUST_SUGGESTED']);
					unset($select[$key]['CUST_SUGGESTEDBY']);
					unset($select[$key]['CUST_UPDATED']);
					unset($select[$key]['CUST_UPDATEDBY']);
					unset($select[$key]['BUSER_BRANCH']);
					unset($select[$key]['STATUS']);
				} */


			foreach ($data as $p => $pTrx) {
				$paramTrx = array(

					"COMPANY"  				=> $pTrx['COMPANY'],
					"MODEL" 			=> $custmodelArr[$pTrx['CUST_MODEL']],
					"CITY"  			=> $pTrx['CITY_NAME'],
					"COUNTRY"  		=> $countryArr[$pTrx['COUNTRY_CODE']],
					"STATUS"  		=> $statusDesc[$statusCode[$pTrx['CUST_STATUS']]],
					"LAST_SUGGESTED"  		=> $pTrx['CUST_SUGGESTED'] . '(' . $pTrx['CUST_SUGGESTEDBY'] . ')',
					"LAST_APROVED"  		=> $pTrx['CUST_UPDATED'] . '(' . $pTrx['CUST_UPDATEDBY'] . ')',
				);
				$newData[] = $paramTrx;
			}

			$header  = Application_Helper_Array::simpleArray($fields, "label");


			$this->_helper->download->csv($header, $newData, null, $this->language->_('Customer Report'));
			// Application_Helper_General::writeLog('RPPY','Download CSV Customer Report');
		} else if ($this->_request->getParam('print') == 1) {

			$countryArr  = Application_Helper_Array::listArray($this->getCountry(), 'COUNTRY_CODE', 'COUNTRY_NAME');
			$conf = Zend_Registry::get('config');
			$custmodelType 		= $conf["cust"]["model"]["desc"];
			$custmodelCode 		= $conf["cust"]["model"]["code"];
			$custmodelArr = array_combine(array_values($custmodelCode), array_values($custmodelType));

			$filterlistdatax = $this->_request->getParam('data_filter');

			if (count($filterlistdatax) > 1) {
				unset($filterlistdatax[0]);
			}


			//var_dump($filterlistdatax);die;
			//echo $filterlistdatax;die;

			if ($filterlistdatax != "Filter - None") {
				foreach ($filterlistdatax as $key => $val) {
					//echo $val;
					$arr = explode("-", $val);
					$colmn = ltrim(rtrim($arr[0]));
					$value = ltrim(rtrim($arr[1]));

					$arrFilter[$colmn] = $value;


					//echo $colmn;die;
					//echo $this->_db->quote($value->toString($this->_dateDBFormat));die;
					/*if($colmn == 'COMP_ID') $select->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($value)));
						if($colmn == 'COMP_NAME')            $select->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($value).'%'));
						if($colmn == 'CITY')             $select->where('UPPER(CUST_CITY) LIKE '.$this->_db->quote('%'.strtoupper($value).'%'));
					
						if($colmn == 'COUNTRY_CODE')          $select->where('UPPER(COUNTRY_CODE)='.$this->_db->quote(strtoupper($value)));
						if($colmn == 'STATUS')           $select->where('CUST_STATUS=?',$value);
						if($colmn == 'LATEST_SUGGESTER')  $select->where('UPPER(CUST_SUGGESTEDBY) LIKE '.$this->_db->quote('%'.strtoupper($value).'%'));
						if($colmn == 'LATEST_APPROVER')   $select->where('UPPER(CUST_UPDATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($value).'%'));
						
						//if (count($filterlistdatax) > 2){
							if($colmn == 'Last Suggestion Date')  
							$value   = (Zend_Date::isDate($value,$this->_dateDisplayFormat))?
							new Zend_Date($value,$this->_dateDisplayFormat):
							false;

							
								$select->where("DATE(CUST_SUGGESTED) >= DATE(".$this->_db->quote($value->toString($this->_dateDBFormat)).")");
							
							if($colmn == 'Last Suggestion Date_END')    
								$value     = (Zend_Date::isDate($value,$this->_dateDisplayFormat))?
								new Zend_Date($value,$this->_dateDisplayFormat):
								false;
								
								$select->where("DATE(CUST_SUGGESTED) <= DATE(".$this->_db->quote($value->toString($this->_dateDBFormat)).")");
						
						//}
						
					echo $select;die;
						//if($colmn == 'Last Approval Date')    $select->where("DATE(CUST_UPDATED) >= DATE(".$this->_db->quote($value->toString($this->_dateDBFormat)).")");
						//if($colmn == 'Last Approval Date_END')      $select->where("DATE(CUST_UPDATED) <= DATE(".$this->_db->quote($value->toString($this->_dateDBFormat)).")");
						//$data = $this->_db->fetchall($select);
						
						*/

					//
				}

				$cid       = html_entity_decode($arrFilter['COMP_ID']);
				$cname     = html_entity_decode($arrFilter['COMP_NAME']);
				$city      = html_entity_decode($arrFilter['CITY']);
				$country   = html_entity_decode($arrFilter['COUNTRY_CODE']);
				$status    = html_entity_decode($arrFilter['STATUS']);
				$latestSuggestionFrom   = html_entity_decode($arrFilter['Last Suggestion Date']);
				$latestSuggestionTo     = html_entity_decode($arrFilter['Last Suggestion Date_END']);
				$latestSuggestor        = html_entity_decode($arrFilter['LATEST_SUGGESTER']);
				$latestApprovalFrom     = html_entity_decode($arrFilter['Last Approval Date']);
				$latestApprovalTo       = html_entity_decode($arrFilter['Last Approval Date_END']);
				$latestApprover         = html_entity_decode($arrFilter['LATEST_APPROVER']);

				//konversi date agar dapat dibandingkan
				$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom, $this->_dateDisplayFormat)) ?
					new Zend_Date($latestSuggestionFrom, $this->_dateDisplayFormat) :
					false;

				$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo, $this->_dateDisplayFormat)) ?
					new Zend_Date($latestSuggestionTo, $this->_dateDisplayFormat) :
					false;

				$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom, $this->_dateDisplayFormat)) ?
					new Zend_Date($latestApprovalFrom, $this->_dateDisplayFormat) :
					false;

				$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo, $this->_dateDisplayFormat)) ?
					new Zend_Date($latestApprovalTo, $this->_dateDisplayFormat) :
					false;

				//if($cid)            $select->where('UPPER(CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cid).'%'));
				if ($cid)              $select->where('UPPER(CUST_ID)=' . $this->_db->quote(strtoupper($cid)));
				if ($cname)            $select->where('UPPER(CUST_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($cname) . '%'));
				if ($city)             $select->where('UPPER(CUST_CITY) LIKE ' . $this->_db->quote('%' . strtoupper($city) . '%'));
				//if($country)        $select->where('UPPER(COUNTRY_CODE) LIKE '.$this->_db->quote('%'.strtoupper($country).'%'));
				if ($country)          $select->where('UPPER(COUNTRY_CODE)=' . $this->_db->quote(strtoupper($country)));
				if ($status)           $select->where('CUST_STATUS=?', $status);
				if ($latestSuggestor)  $select->where('UPPER(CUST_SUGGESTEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestSuggestor) . '%'));
				if ($latestApprover)   $select->where('UPPER(CUST_UPDATEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestApprover) . '%'));

				if ($latestSuggestionFrom)  $select->where("DATE(CUST_SUGGESTED) >= DATE(" . $this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)) . ")");
				if ($latestSuggestionTo)    $select->where("DATE(CUST_SUGGESTED) <= DATE(" . $this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)) . ")");
				if ($latestApprovalFrom)    $select->where("DATE(CUST_UPDATED) >= DATE(" . $this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)) . ")");
				if ($latestApprovalTo)      $select->where("DATE(CUST_UPDATED) <= DATE(" . $this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)) . ")");
			}

			$data = $this->_db->fetchall($select);
			$field_export = array(

				'custname' => array(
					'field'    => 'COMPANY',
					'label'    => $this->language->_('Company'),
					'sortable' => true
				),
				'model'     => array(
					'field'    => 'MODEL',
					'label'    => $this->language->_('Model'),
					'sortable' => true
				),

				'city'     => array(
					'field'    => 'CITY',
					'label'    => $this->language->_('City'),
					'sortable' => true
				),

				'country'     => array(
					'field'  => 'COUNTRY',
					'label'    => $this->language->_('Country'),
					'sortable' => true
				),

				'status'   => array(
					'field'    => 'STATUS',
					'label'    => $this->language->_('Status'),
					'sortable' => true
				),

				//'latestSuggestion'     => array('field'    => 'CUST_SUGGESTED',
				//                           'label'    => $this->language->_('Last Suggestion Date'),
				//                          'sortable' => true),

				'latestSuggestor'   => array(
					'field'  => 'LAST_SUGGESTED',
					'label'    => $this->language->_('Last Suggested'),
					'sortable' => true
				),

				//'latestApproval'    => array('field'  => 'CUST_UPDATED',
				//                          'label'    => $this->language->_('Last Approval Date'),
				//                         'sortable' => true),

				'latestApprover'  => array(
					'field'   => 'LAST_APROVED',
					'label'    => $this->language->_('Last Approved'),
					'sortable' => true
				)
			);


			foreach ($data as $p => $pTrx) {
				$paramTrx = array(

					"COMPANY"  				=> $pTrx['COMPANY'],
					"MODEL" 			=> $custmodelArr[$pTrx['CUST_MODEL']],
					"CITY"  			=> $pTrx['CITY_NAME'],
					"COUNTRY"  		=> $countryArr[$pTrx['COUNTRY_CODE']],
					"STATUS"  		=> $statusDesc[$statusCode[$pTrx['CUST_STATUS']]],
					"LAST_SUGGESTED"  		=> $pTrx['CUST_SUGGESTED'] . '(' . $pTrx['CUST_SUGGESTEDBY'] . ')',
					"LAST_APROVED"  		=> $pTrx['CUST_UPDATED'] . '(' . $pTrx['CUST_UPDATEDBY'] . ')',
				);
				$newData[] = $paramTrx;
			}

			$this->_forward('print', 'index', 'widget', array('data_content' => $newData, 'data_caption' => 'Customer Report', 'data_header' => $field_export, 'data_filter' => $filterlistdatax));


			//$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Customer Report', 'data_header' => $fields));
		}

		//insert log
		try {
			$this->_db->beginTransaction();

			Application_Helper_General::writeLog('CCLS', 'View Customer Setup Customer List');

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollBack();
		}

		if (!empty($dataParamValue)) {
			$this->view->lsdateStart = $dataParamValue['Last Suggestion Date'];
			$this->view->lsdateEnd = $dataParamValue['Last Suggestion Date_END'];
			$this->view->ladateStart = $dataParamValue['Last Approval Date'];
			$this->view->ladateEnd = $dataParamValue['Last Approval Date_END'];

			unset($dataParamValue['Last Suggestion Date_END']);
			unset($dataParamValue['Last Approval Date_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
			// print_r($whereval);die;
		}
	}
}
